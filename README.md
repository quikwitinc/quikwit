**CHANGE LOG** 

Majority of front-end is now using ProUI custom skin

**READ ME**

You can use git to clone the repo onto your computer. 

Our system uses PhalconPHP for backend, Twitter Bootstrap for frontend. Current Js library is Jquery. Composer for dependency management. Xampp for SQL & Apache server.
Currently database is hosted at Amazon instance.

Front End of the app also incorporates 47admin custom skin & Coming-soon pages are using ME custom skin licensed (both contained in public/third-party)

Current file structure follows mvc pattern and is as follow:

      tute
	 |
	 -app
	    |
	    -cache
	    -components
            -config
            -controllers
            -forms
            -models
            -views
		 |
		 -templates (contain all of the templates which are extended into other folders in views; some are not used)
	 -database
	 -public
	       |
	       -css
	       -img
               -js
               -server
               -third-party (contains third-party scripts which is not a part of any packages in packagist for composer)
	 -vendor (contains dependencies loaded from composer)

Set up:

1) Install Xampp and set up PhalconPHP

2) Paste this in C:/xampp/apache/conf/extra/httpd-vhosts


<VirtualHost *:80>
    ServerAdmin webmaster@dummy-host.example.com
    DocumentRoot "C:/xampp/htdocs/quikwit/public"
    <Directory "C:/xampp/htdocs/quikwit/public">
                Options FollowSymLinks
                AllowOverride None
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                Allow from all
    </Directory> 
    ServerName tute.com
    ServerAlias www.tute.com
    ErrorLog "logs/dummy-host.example.com-error.log"
    CustomLog "logs/dummy-host.example.com-access.log" common
    SetEnv APPLICATION_URL www.tute.com    
</VirtualHost>

3) Set up host files

127.0.0.1       localhost (or whatever you want to name it as)
54.165.18.224	www.tute.com (this is the live testing on the server hosted at Amazon)

4) Git clone on the bitbucket and pull from the branch "production" (branch "master" is not updated as often as branch "production" as branch "master" is only for updating the actual website)

5) Install composer and load dependencies

6) Create a folder named "cache" under /app

7) Change maintenance mode to false in config.php under /app/config/environment/development; This needs to be kept false when you are developing so that other pages can be loaded.

8) For development purpose it would be good to turn off ACL under /public/index.php at line 207


**Changes to MySQL db**

Inside the /app/database folder there is the tute-user.sql. You can import that to your own phpmyadmin for local development. 

**Git Workflow** 

You will always be working on your own development branches (ie. dev_ryan and dev_bryan).

-- Starting the development process (i.e. the PULL process):
Each time you start programming, please merge from the "production" branch to your own development branch.

-- Committing changes (i.e. the COMMIT process):
Changes can be committed to you own development branch.

-- End of development cycle (i.e. the PUSH process):
When you are ready for a push, please follow the following steps:

1. PULL & MERGE from the "production" branch first and merge with anything that might have been committed to that branch. PUSH to your remote development branch.

2. Switch to the "production" branch and PULL and MERGE with you remote development branch, resolve conflicts, and push to "production" branch. 

3. Go back to your own branch.