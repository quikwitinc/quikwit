<?php

namespace Component;

class Helper extends \Phalcon\Mvc\User\Component
{
	public function csrf($redirect = false) {

		if ($this->security->checkToken() == false) {
            $this->flash->error('Invalid CSRF Token');
            if ($redirect) {
            	$this->response->redirect($redirect);
            	return true;
            }
            return;
        }
	}

	public function setJSON(){
		$this->view->disable();
	}

	public function outputJSON($status, $message="", $code=1, $content=null){
		//$this->view->disable();
		$params = array("status" => $status);
		if($message){
			$params['message'] = $message;
		}
		if($code){
			$params['code'] = $code;
		}
		if($content){
			$params['content'] = $content;
		}
		exit(json_encode($params));
	}

    public function getTimeAgo($time){
      $estimate_time = time() - $time;

        if( $estimate_time < 1 )
        {
            return 'less than 1 second ago';
        }

        $condition = array(
                    12 * 30 * 24 * 60 * 60  =>  'year',
                    30 * 24 * 60 * 60       =>  'month',
                    24 * 60 * 60            =>  'day',
                    60 * 60                 =>  'hour',
                    60                      =>  'minute',
                    1                       =>  'second'
        );

        foreach( $condition as $secs => $str )
        {
            $d = $estimate_time / $secs;

            if( $d >= 1 )
            {
                $r = round( $d );
                return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
            }
        }
    }
}