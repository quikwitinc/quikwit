<?php

namespace Component;

use \Phalcon\Http\Response as Response,
    \Phalcon\Mvc\User\Component;

class Json extends Component
{
	protected $success = false;
	protected $status = 0;
	protected $message = "";
	protected $content = null;
	protected $type = "json";
	protected $callback = '';
	static protected $ALLOWED_TYPES = array("json", "jsonp");
	
	public function __construct($type="json", $callback=''){
		$this->setLayout();
		$this->setType($type, $callback);
		$this->response->setHeader("Content-Type", "application/json");
		ob_start();
	}
	
	public function setType($type, $callback=''){
		if(in_array($type, self::$ALLOWED_TYPES)){
			$this->type = $type;
			$this->callback = $callback;
		}
	}
	
	public function getType(){
		return $this->type;
	}
	
	public function setSuccess($bool){
		$this->success = $bool;
	}
	
	public function setStatus($code){
		$this->status = $code;
	}
	
	public function setMessage($message){
		$this->message = $message;
	}
	
	public function setContent($content){
		$this->content = $content;
	}
	
	public function getStatus(){
		return $this->status;
	}
	
	public function getSuccess(){
		return $this->success;
	}
	
	public function getMessage(){
		return $this->message;
	}
	
	public function getContent(){
		return $this->content;
	}
	
	public function setJSON($success, $message, $status, $content=null)
    {
		$this->setSuccess($success);
		$this->setMessage($message);
		$this->setStatus($status);
		if($content){
			$this->setContent($content);
		}
        return $this;
	}
	
	public function setLayout()
    {
		$this->view->disable();
	}

	public function output($boolOnly=false)
    {
		$params = new \stdClass();
		
		if($boolOnly){
			$params = $this->getSuccess();
		}else{
			$params->success = $this->getSuccess();
			$params->status = $this->getStatus();
			if($message=$this->getMessage()){
				$params->message = $this->getMessage();
			}
			if($content=$this->getContent()){
				$params->content = $content;
			}
			
		}
			
		//$result = json_encode($params);
		$type = $this->getType();
		if($type==="jsonp"){
			$result = $this->request->get('callback') . "(".json_encode($params).");";
		}
		ob_end_clean();
        $this->response->setJsonContent($params);
		$this->response->send();
	}
	
	public function outputBoolean()
    {
		ob_end_clean();
		$result = json_encode($this->getSuccess());
        $this->response->setJsonContent($result);
        $this->response->send();
	}
}