<?php

namespace Component\Mailer;

use Phalcon\Mvc\User\Component,
    Swift_Message,
    Swift_Mailer,
    Swift_SmtpTransport,
    Phalcon\Validation;

class Manager extends Component
{
    protected $mailer;
    protected $template;
    protected $templateExtension = '.volt';
    protected $templateName;
    protected $templateVars = null;
    protected $useTemplate = true;
    protected $result;
    protected $failures;

    public function __construct($host = 'localhost', $port = 465, $security = 'ssl')
    {
        $this->getDI()->get('SwiftMailer');
        $mailer = new Swift_SmtpTransport($host, $port, $security);
        $this->setMailer($mailer);
        $template = $this->getDI()->get('emailTemplate');
        $this->setTemplate($template);
    }

    protected function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return \Phalcon\Mvc\View\Simple
     */
    public function getTemplate()
    {
        return $this->template;
    }

    public function setHost($host)
    {
        $this->getMailer()->setHost($host);
        return $this;
    }

    public function getHost()
    {
        $this->getMailer()->getHost();
    }

    public function setPort($port)
    {
        $this->getMailer()->setPort((int)$port);
        return $this;
    }

    public function getPort()
    {
        return $this->getMailer()->getPort();
    }

    public function setSecurity($security)
    {
        $this->getMailer()->setEncryption($security);
        return $this;
    }

    public function setMailer($mailer)
    {
        $this->mailer = $mailer;
        return $this;
    }

    public function setUsername($username)
    {
        $this->getMailer()->setUsername($username);
        return $this;
    }

    public function setPassword($password)
    {
        $this->getMailer()->setPassword($password);
        return $this;
    }

    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function setFailures($failures)
    {
        $this->failures = $failures;
        return $this;
    }

    public function getFailures()
    {
        return $this->failures;
    }

    /**
     * @return \Swift_SmtpTransport
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /*public function setTemplatePath()
    {
        if (strlen($templateName) > strlen($this->templateExtension)) {
            if (substr($templateName, strlen($this->templateExtension) * -1) === $this->templateExtension) {
                $templateName = $templateName . $this->templateExtension;
            }
        }
    }*/

    public function loadSettingsFromConfig($settings = 'gmail-gateway')
    {
        $gateway = $this->getDI()->get('config')->get($settings);
        $userName = $gateway->username;
        $password = $gateway->password;
        $host = $gateway->host;
        $port = $gateway->port;
        $security = $gateway->security;
        $this->getMailer()
            ->setHost($host)
            ->setPort($port)
            ->setEncryption($security)
            ->setUsername($userName)
            ->setPassword($password);
        return $this;
    }

    public function setTemplateName($templateName)
    {
        if (strlen($templateName) > strlen($this->templateExtension)) {
            if (substr($templateName, strlen($this->templateExtension) * -1) === $this->templateExtension) {
                $templateName = $templateName . $this->templateExtension;
            }
        }
        $this->templateName = $templateName;
        return $this;
    }

    public function getTemplateName()
    {
        return $this->templateName;
    }

    public function setTemplateVars($vars)
    {
        $this->templateVars = $vars;
        return $this;
    }

    public function getTemplateVars()
    {
        return $this->templateVars;
    }

    public function render($templateName = null, $vars = null)
    {
        if (!is_null($templateName)) {
            $this->setTemplateName($templateName);
        }

        if (!is_null($vars)) {
            $this->setTemplateVars($vars);
        }

        $templateName = $this->getTemplateName();
        $vars = $this->getTemplateVars();

        return $this->getTemplate()->render($templateName, $vars);
    }

    public function send($to, $from, $subject, $body = null)
    {
        if (is_null($body)) {
            $body = $this->render();
        }

        $failed = false;
        $failedMessages = [];
        if (!self::validateEmail($from)) {
            $failed = true;
            $failedMessages[] = 'Invalid from email.';
        }

        if ($failed) {
            $this->setFailures($failedMessages);
            return false;
        }
        
        $message = Swift_Message::newInstance();
        $message->setTo($to);
        $message->setFrom($from);
        $message->setSubject($subject);
        $message->setBody($body, 'text/html');
        $mailer = Swift_Mailer::newInstance($this->getMailer());
        $result = $mailer->send($message, $failure);
        $this->setResult($result);
        $this->setFailures($failure);
        return $result;
    }

    public static function validateEmail($email)
    {
        $validation = new Validation();
        $validation->add('email', new Validation\Validator\Email(array('message' => 'Email is invalid.')));
        return $validation->validate(['email' => $email]);


    }
}