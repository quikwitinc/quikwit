<?php

namespace Component;
	
use PrettyDateTime\PrettyDateTime,
    Phalcon\Mvc\User\Component,
    DateTime,
    Component\User;

class PrettyPrint extends Component
{
	
	public static function prettyPrint($dateTime, $refDateTime = 'now')
    {
    	$user = new User();
    	date_default_timezone_set($user->getSessionUserTimeZone());
        $getDateTimeObject = function($dateType) {
            if (is_numeric($dateType)) { //Linux timestamp
                $date = (new DateTime())->setTimestamp((int)$dateType);
            } else if (is_string($dateType)) {
                $date = new DateTime($dateType);
            }
            return $date;
        };
		$date = $getDateTimeObject($dateTime);
		$refDate = $getDateTimeObject($refDateTime);
        if (is_a($date, 'DateTime') && is_a($refDate, 'DateTime')) {
            return PrettyDateTime::parse($date, $refDate);
        }
    }

    public static function excerpt($string, $len = 30)
    {
        return substr($string, 0, $len);
    }

    public static function postExcerpt($string, $len = 1000)
    {
        return substr($string, 0, $len);
    }

    public static function getTimezone($timezone)
    {
        return $timezone;
    }
}