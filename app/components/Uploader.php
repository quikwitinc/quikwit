<?php 
namespace Component;

class Uploader extends \Phalcon\Mvc\User\Component{
	
	protected $uploadDir;
	protected $uploadUri;

	public function __construct(){
		global $config;
		$this->uploadDir = $config["dirs"]["upload"]['path'];
		$this->uploadUri = $config["dirs"]["upload"]["uri"];
	}
	
	public function getUploadDir(){
		return $this->uploadDir;
	}
	
	public function getUploadPath($filePath){
		return $this->uploadDir."/".ltrim($filePath, "/");
	}
	
	public function getFileUrl($filePath){
		global $di;
		$url = $di->get("url");
		return $url->get(ltrim($this->uploadUri."/".ltrim($filePath, "/"), "/"));
	}
	
	public function moveFile($file, $dest){
		
	}
	
	public function moveUploadedFile($file, $dest){
		$base = dirname($dest);
		if(!file_exists($base)){
			mkdir($base, 0777, true);
		}
		if(file_exists($file) && is_uploaded_file($file)){
			return move_uploaded_file($file, $dest);
		}
		return false;	
	}
	
	public function createFolder($path){
		if(!file_exists($path)){
			return mkdir($path, 0777, true);
		}
		return true;
		
	}
	
}

?>