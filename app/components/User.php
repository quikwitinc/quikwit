<?php

namespace Component;

use stdClass,
    Phalcon\Session\Bag,
	Phalcon\DI;

class User extends \Phalcon\Mvc\User\Component
{
	private $_session=null;

	public function __construct(){
		$di = DI::getDefault();
		$this->_session = new Bag(get_class());
		$this->_session->setDI($di);
	}

	public function createUserSession(\User $user)
	{
		$sData = new stdClass();
    	$sData->id = $user->usr_id;
    	$sData->firstName = $user->usr_firstname;
    	$sData->lastName = $user->usr_lastname;
    	$sData->lastLogin = time();
        $sData->role = $user->usr_level;
    	$sData->usr_avatar = $user->usr_avatar;
        $this->_session->user = $sData;
        return $this->_session->user;
	}

	public function destroyUserSession(){
		$this->_session->destroy();
		$this->_session->user = null;
	}

	public function hasSession(){
		return is_object($this->_session->user);
	}

	public function getSessionUser(){
		if(empty($this->_session->user))
			return false;
		return $this->_session->user;
	}

	public function refreshSessionUser(){
		$id = $this->_session->user->id;
		if($id){
			$user = User::findUserById($id);
			return $this->createUserSession($user);
		}else{
			unset($this->_session->user);
			return false;
		}
	}

	public function getSessionUserTimeZone(){
		if(empty($this->_session->timeZoneSet))
			return false;
		return $this->_session->timeZoneSet;
	}

	public function setSessionUserTimeZone($time){
		$this->_session->timeZoneSet = $time;
        return $this->_session->timeZoneSet;
	}
}