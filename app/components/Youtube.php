<?php 

namespace Component;

use \Phalcon\Session\Bag as Bag,
\Phalcon\DI as DI;

require_once "Google/Client.php";
require_once "Google/Service/YouTube.php";

class Youtube extends \Phalcon\Mvc\User\Component{
	
	const MAX_SIZE = 64000000000;
	
	private $_client = null;
	private $_youtube = null;
	private $_session = null;
	private $_creds = null;
	private $_model = null;
	private $_scopes = array(
			"https://www.googleapis.com/auth/youtube",
			"https://www.googleapis.com/auth/youtube.upload",
			"https://www.googleapis.com/auth/youtube.readonly",
			"https://www.googleapis.com/auth/youtubepartner-channel-audit"
			);
	
	public function __construct(){
		//global $api;
		$api = $this->getDI()->get('api');
		$di = DI::getDefault();
		$this->_session = new Bag(get_class());
		$this->_session->setDI($di);
		$this->_setCreds($api->get('youtube'));
		$this->_client = new \Google_Client();
		$this->_client->setApplicationName("quikwit");
		$this->_client->setClientId($this->_getCred("clientId"));
		$this->_client->setClientSecret($this->_getCred("clientSecret"));
		$this->_client->setRedirectUri($this->_getCred("RedirectUri"));
		$this->_client->setAccessType("offline");
		$this->_client->setScopes($this->_scopes);
		$this->_client->setApprovalPrompt("force");
		$this->_youtube = new \Google_Service_YouTube($this->_client);
		$this->_model = new \OAuth();
	}
	
	public function getAuthUrl(){
		return $this->_getClient()->createAuthUrl();
	}
	
	protected function _getSessionAccessToken(){
		return $this->_session->access_token;
		//pull from database.
	}
	
	protected function _setSessionAccessToken($token=null){
		$this->_session->access_token = $token;
	}
	
	protected function _hasSessionAccessToken(){
		$token = $this->_getSessionAccessToken();
		return $token? true: false;
	}
	
	public function setRefreshToken($refreshToken){
		$model = $this->_getModel();
		$model->saveRefreshToken($refreshToken);
	}
	
	public function getRefreshToken(){
		$model = $this->_getModel();
		$token = $model->getToken();
		if($token){
			return $token->auth_refresh_token;
		}
		return null;
	}
	
	public function setAccessToken($accessToken){

		$model = $this->_getModel();
		$model->saveAccessToken($accessToken);
		$this->_setSessionAccessToken($accessToken);

	}
	
	public function getAccessToken(){
		$model = $this->_getModel();
		$token = $model->getToken();
		
		if($token)
			$this->_setSessionAccessToken($token->auth_access_token);
		return $this->_getSessionAccessToken();
	}
	
	public function hasAccessToken(){
		return $this->getAccessToken() ? true : false;
	}
	
	public function refreshAccessToken($refreshToken){
		$client = $this->_getClient();
		$client->refreshToken($refreshToken);
		$this->setAccessToken($client->getAccessToken());
		return $this->checkAccessToken();
	}
	
	/**
	 * 
	 * @param unknown $code
	 * 
	 * e.g. {"access_token":"ya29.aABSfYD_g6xHZBsAAAAwjumdgctSxc1oEip4-cc0FfNgFzsl-VL9CQYqs8uPCA","token_type":"Bearer","expires_in":3599,"created":1408591630}
	 */
	public function setAccessTokenFromAuthCode($code){
		$client = $this->_getClient();
		$client->revokeToken();
		try{
			$client->authenticate($code);
			$accessToken = $client->getAccessToken();
			$token = json_decode($accessToken);
			$this->setAccessToken($accessToken);
			if(isset($token->refresh_token)){
				$this->setRefreshToken($token->refresh_token);
			}	
		}catch(Exception $e){
			return false;
		}
		return $this->checkAccessToken();
	}
	
	
	private function _setCreds($creds){
		$this->_creds = $creds;
	}
	
	private function _getCred($name){
		return $this->_creds->get($name, null);
	}
	
	private function _getClient(){
		return $this->_client;
	}
	
	private function _getYoutube(){
		return $this->_youtube;	
	}
	
	private function _getModel(){
		return $this->_model;
	}
	
	private function _getScopes($name=null){
		return $this->_scopes;
	}
	
	public function checkAccessToken(){
		$client = $this->_getClient();
		
		if($this->hasAccessToken()){
			//print_r($this->getAccessToken());
			//exit;
			$client->setAccessToken($this->getAccessToken());
		}
		
		if($client->isAccessTokenExpired()){
			try{
				$token = $this->getRefreshToken();
				if($token){
					$this->refreshAccessToken($token);
					$this->setAccessToken($client->getAccessToken());
				}
			}catch(Exception $e){
				return false;
			}
		}
		return !$client->isAccessTokenExpired();
	}
	
	public function revokeToken(){
		$client =$this->_getClient();
		if($this->hasAccessToken()){
			$client->setAccessToken($this->getAccessToken());
		}
		if($client->revokeToken()){
			$model = $this->_getModel();
			$model->deleteToken();
			$this->setAccessToken($client->getAccessToken());
			return true;
		}else{
			return false;
		}
	}
	
	public function uploadVideo($file){
		
		$client = $this->_getClient();
		if($this->checkAccessToken()){
		
			$snippet = new \Google_Service_YouTube_VideoSnippet();
			$snippet->setTitle("Demo Video");
			$snippet->setDescription("Demo video for testing upload");
			$snippet->setTags(array("tutorial", "demo"));
			$snippet->setCategoryId(27);
			
			$status = new \Google_Service_YouTube_VideoStatus();
			$status->privacyStatus = "private";
				
			$video = new \Google_Service_YouTube_Video();
			$video->setSnippet($snippet);
			$video->setStatus($status);
				
			$chunkSizeBytes = 1 * 1024 * 1024;
				
			$client->setDefer(true);
				
			$insertRequest = $this->_getYoutube()->videos->insert("status,snippet", $video);
			
			$media = new \Google_Http_MediaFileUpload(
					$client,
					$insertRequest,
					'video/*',
					null,
					true,
					$chunkSizeBytes
			);
			
			$media->setFileSize(filesize($file));
			
			// Read the media file and upload it chunk by chunk.
			$status = false;
			$handle = fopen($file, "rb");
			while (!$status && !feof($handle)) {
				$chunk = fread($handle, $chunkSizeBytes);
				$status = $media->nextChunk($chunk);
			}
			fclose($handle);
			
			// If you want to make other calls after the file upload, set setDefer back to false
			$client->setDefer(false);
			return true;
		}else{
			return false;
		}
	}
	
	public function uploadVideoToYoutube($file, $title, $tags, $content) {
		
		$api = $this->getDI()->get('api');		
		$this->_setCreds($api->get('youtube'));
		$application_name = 'quikwit'; 
		$client_secret = $this->_getCred("clientSecret");
		$client_id = $this->_getCred("clientId");
		$scope = $this->_scopes;
		         
		$videoPath = $file;
		$videoTitle = $title;
		$videoDescription = $content;
		$videoCategory = "22";
		$videoTags = implode(',',$tags);
		$key = $this->getAccessToken();
				 
		try{
		    // Client init
		    $client = new \Google_Client();
		    $client->setApplicationName($application_name);
		    $client->setClientId($client_id);
		    $client->setAccessType('offline');
		    $client->setAccessToken($key);
		    $client->setScopes($scope);
		    $client->setClientSecret($client_secret);
		 
		    if ($client->getAccessToken()) {
		 
		        /**
		         * Check to see if our access token has expired. If so, get a new one and save it to file for future use.
		         */
		        if($client->isAccessTokenExpired()) {
		            $newToken = json_decode($client->getAccessToken());
		            $client->refreshToken($newToken->refresh_token);
		            $accessToken = $client->getAccessToken();		            
					$model = $this->_getModel();
		            $model->saveAccessToken($accessToken);
					$this->_setSessionAccessToken($accessToken);
		        }
		 
		        $youtube = new \Google_Service_YouTube($client);
		 
		 
		 
		        // Create a snipet with title, description, tags and category id
		        $snippet = new \Google_Service_YouTube_VideoSnippet();
		        $snippet->setTitle($videoTitle);
		        $snippet->setDescription($videoDescription);
		        $snippet->setCategoryId($videoCategory);
		        $snippet->setTags($videoTags);
		 
		        // Create a video status with privacy status. Options are "public", "private" and "unlisted".
		        $status = new \Google_Service_YouTube_VideoStatus();
		        $status->setPrivacyStatus('public');
		 
		        // Create a YouTube video with snippet and status
		        $video = new \Google_Service_YouTube_Video();
		        $video->setSnippet($snippet);
		        $video->setStatus($status);
		 
		        // Size of each chunk of data in bytes. Setting it higher leads faster upload (less chunks,
		        // for reliable connections). Setting it lower leads better recovery (fine-grained chunks)
		        $chunkSizeBytes = 1 * 1024 * 1024;
		 
		        // Setting the defer flag to true tells the client to return a request which can be called
		        // with ->execute(); instead of making the API call immediately.
		        $client->setDefer(true);
		 
		        // Create a request for the API's videos.insert method to create and upload the video.
		        $insertRequest = $youtube->videos->insert("status,snippet", $video);
		 
		        // Create a MediaFileUpload object for resumable uploads.
		        $media = new \Google_Http_MediaFileUpload(
		            $client,
		            $insertRequest,
		            'video/*',
		            null,
		            true,
		            $chunkSizeBytes
		        );
		        $media->setFileSize(filesize($videoPath));
		 
		 
		        // Read the media file and upload it chunk by chunk.
		        $status = false;
		        $handle = fopen($videoPath, "rb");
		        while (!$status && !feof($handle)) {
		            $chunk = fread($handle, $chunkSizeBytes);
		            $status = $media->nextChunk($chunk);
		        }
		 
		        fclose($handle);
		 
		        /**
		         * Video has successfully been upload, now lets perform some cleanup functions for this video
		         */
		        if ($status->status['uploadStatus'] == 'uploaded') {
		        	//$htmlBody = '<embed width="400" height="315" src="https://www.youtube.com/embed/'.$status['id'].'"></embed>';
		        	// Actions to perform for a successful upload
		        	return $status['id'];
		        }
		 		
		        // If you want to make other calls after the file upload, set setDefer back to false
		        $client->setDefer(true);
		 
		    } else{
		        // @TODO Log error
		        return false;
		    }
		 
		} catch(Google_Service_Exception $e) {
		    print "Caught Google service Exception ".$e->getCode(). " message is ".$e->getMessage();
		    print "Stack trace is ".$e->getTraceAsString();
		}catch (Exception $e) {
		    print "Caught Google service Exception ".$e->getCode(). " message is ".$e->getMessage();
		    print "Stack trace is ".$e->getTraceAsString();
		}
	}
}
?>