<?php

use Phalcon\Mvc\Dispatcher,
    Phalcon\Events\Event,
    Phalcon\Acl;

/**
 * Permission
 *
 * Prevents User Types from accessing areas they are not allowed in.
 */
class Permission extends \Phalcon\Mvc\User\Plugin
{

    /**
     * Constants to prevent a typo
     */
    const GUEST = 'guest';
    const USER  = 'user';
    const ADMIN = 'admin';

    /**
     * Accessible to everyone
     * @var array
     */
    protected $_publicResources = [
        'index' => ['*'],
        'signin' => ['*'],
        'faq' => ['*'],
        'about' => ['*'],
        'contact-us' => ['*'],
        'search' => ['*'],
        'user' => [      
            'profile',
            'unsubscribeFromEmail',
            'unsubscribed',
            'bye'
        ],
        'terms' => ['*'],
        'suggest' => ['*'],
        'register' => [
            'doRegister'
        ],
        'timezone' => ['*'],
        'post' => ['*'],
        'pricing' => ['*'],
        'how-to-hire' => ['*'],
        'browse' => ['*'],
        'categories' => ['*']
        /* 'blog' => [
            'index',
            'show',
            'category'
        ] */
    ];

    /**
     * Accessible to Users (and Admins)
     * @var array
     */
    protected $_userResources = [
        'signout' => ['*'],
        'user' => ['*'],
        'message' => ['*'],
        'contacts' => ['*'],
        'contract' => ['*'],
        'overview' => ['*'],
        'settings' => ['*'],
        'register' => ['*'],
        'media' => ['*'],
        'videoupload' => ['*'],
        'o' => ['*'],
        'contractreview' => ['*'],
        'abuse' => ['*'],
        'gig' => ['*']
    ];

    /**
     * Accessible to Admins
     * @var array
     */
    protected $_adminResources = [
        'admin' => ['*'],
        'templates-preview' => ['*'],
        'blog' => ['*']
    ];

    // ------------------------------------------------------------------------

    /**
     * Triggers before a route is successfully executed
     *
     * @param  Event      $event
     * @param  Dispatcher $dispatcher
     *
     * @return boolean|void
     */
    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        // Debug:
        //$this->component->user->destroyUserSession();

    	
    	$role = self::GUEST;
    	$user = $this->component->user->getSessionUser();
		if($user){
			$role = $user->role;
		}
        // Get the current role
        //$role = $this->session->get('role');

        // Get the current Controller/Action from the Dispatcher
        $controller = $dispatcher->getControllerName();
        $action     = $dispatcher->getActionName();

        // Get the ACL Rule List
        $acl = $this->_getACL();

        // See if they have permission
        $allowed = $acl->isAllowed($role, $controller, $action);

        if ($allowed != Acl::ALLOW)
        {
            //$this->flash->error('<div data-toggle="notify" data-onload data-message="The area is contaminated so we turn you back for safety reason." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            $this->response->redirect($this->url->get(""));
            // Stop the dispatcher at the current operation
            return false;
        }
        return true;
    }

    // ------------------------------------------------------------------------

    /**
     * Build the Session ACL list one time if it's not set
     *
     * @return object
     */
    protected function _getACL()
    {
    	global $config;

        unset($this->persistent->acl);

        //if (!isset($this->persistent->acl) || $config["environment"]=="staging")
        //{
            $acl = new Acl\Adapter\Memory();
            $acl->setDefaultAction(Acl::DENY);

            $roles = [
                User::ROLE_GUEST => new Acl\Role(User::ROLE_GUEST),
                User::ROLE_USER  => new Acl\Role(User::ROLE_USER),
                User::ROLE_ADMIN => new Acl\Role(User::ROLE_ADMIN),
            ];

            // Place all the roles inside the ACL Object
            foreach ($roles as $role) {
                $acl->addRole($role);
            }

            // Public Resources
            foreach ($this->_publicResources as $resource => $action) {
                $acl->addResource(new Acl\Resource($resource), $action);
            }

            // User Resources
            foreach ($this->_userResources as $resource => $action) {
                $acl->addResource(new Acl\Resource($resource), $action);
            }

            // Admin Resources
            foreach ($this->_adminResources as $resource => $action) {
                $acl->addResource(new Acl\Resource($resource), $action);
            }

            // Allow ALL Roles to access the Public Resources
            foreach ($roles as $role) {
                foreach($this->_publicResources as $resource => $action) {
                    $acl->allow($role->getName(), $resource, '*');
                }
            }

            // Allow User & Admin to access the User Resources
            foreach ($this->_userResources as $resource => $actions) {
                foreach ($actions as $action) {
                    $acl->allow(self::USER, $resource, $action);
                    $acl->allow(self::ADMIN, $resource, $action);
                }
            }

            // Allow Admin to access the Admin Resources
            foreach ($this->_adminResources as $resource => $actions) {
                foreach ($actions as $action) {
                    $acl->allow(self::ADMIN, $resource, $action);
                }
            }

            $this->persistent->acl = $acl;
        //}

        return $this->persistent->acl;
    }

    // ------------------------------------------------------------------------

}