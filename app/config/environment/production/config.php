<?php

return new \Phalcon\Config([
    'contacts' => [
        'coming-soon' => [
            'mailto' => 'quikwithq@gmail.com',
            'from' => 'quikwithq@gmail.com',
            'name' => 'QuikWit Inc.'
        ],
        'contact-us' => [
            'mailto' => 'general@quikwit.co',
            'from' => 'general@quikwit.co',
            'name' => 'QuikWit Inc.'
        ]
    ],
    'gmail-gateway' => [
        'username' => 'general@quikwit.co',
        'password' => 'Freelancer123',
        'host' => 'smtp.gmail.com',
        'port' => 465,
        'security' => 'ssl'
    ],
	'db' => [
		'adapter'     => 'Mysql',
        'host'        => 'tute-central.cosryua6tlkt.us-east-1.rds.amazonaws.com',
        'username'    => 'quikwit',
        'password'    => 'quikwitmasterhq',
        'dbname'      => 'quikwit'
    ],
	'dirs' => [
		'upload' => [
			'path' => APPLICATION_PATH. '/uploads',
			'uri' => "/uploads",
		]
	],
	'webURL' => 'https://www.quikwit.co',
    'maintenance' => [
        'mode' => false,
        'default' => 'maintenance',
        'controllers' => ['maintenance'],
        'actions' => [
            'maintenance' => [
                'index'
            ]
        ],
    ]
    /* 'maintenance' => [
        'mode' => false,
        'default' => 'coming-soon',
        'controllers' => ['coming-soon'],
        'actions' => [
            'coming-soon' => [
                'index',
                'about',
                'timeline',
                'teacher',
                'complete'
            ]
        ],
    ] */
]);