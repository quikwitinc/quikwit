<?php
return array (
         // required
        'base_url' => 'https://www.quikwit.co/signin/social',
        'translation' => array(
            'google'   => 'Google',
            'facebook' => 'Facebook',
            'twitter'  => 'Twitter',
            'linkedin' => 'LinkedIn'
        ),
        // Required
        'providers' => array (
             'Google' => array (
                 'enabled' => True ,
                 'keys'     => array (
                     'ID'      => 'Google Client ID',
                     'secret' => 'Google Secret Key ' )
            ),
            'Facebook' => array (
                'enabled' => True ,
                'keys'     => array (
                    'id'      => '770147199741530',
                    'secret' => 'c0902a5fd950d794e5cf60e691a6a7ac'
                ),
                'scope' => array('email, public_profile, user_friends')
            ),
            'Twitter' => array (
                'enabled' => True ,
                'keys'     => array (
                    'key'     => 'Twitter Client ID' ,
                    'secret' => 'Twitter Secret Key'
                )
            ),
            'LinkedIn' => array (
                'enabled' => True ,
                'keys'     => array (
                    'key'      => '77ry6wvpncdabg',
                    'secret' => 'mOPG2ndfbSi7NuDX'

                ),
                'scope' => array('r_emailaddress', 'r_basicprofile')
            )
        ),
        'debug_mode'    => false,
        'debug_file' => 'hybridauth.log',
        'Http_client' => null
    );