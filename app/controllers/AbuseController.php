<?php

use \Phalcon\Tag,
	\Phalcon\Mvc\Url;

class AbuseController extends ControllerBase
{
    public function initializes()
    {
        parent::initialize();
    }

    public function createAction(){

    	$this->view->disable();

    	if(!$this->component->user->hasSession()){
    		$this->flash->error("Unable to report user. User not logged in.");
    		$this->response->redirect($this->url->get("/"));
    	}

    	$this->component->helper->csrf("/user/profile");
    	$userModel = new User();
    	if($this->request->isPost()){
    		$abuse = (object)$this->request->getPost("abuse");
            $user = $this->component->user->getSessionUser();
            if (isset($abuse->uid)){
                $reportedUser = User::findUserById($abuse->uid);
                if(isset($reportedUser->usr_id)){
                    $user = $this->component->user->getSessionUser();
                    $a = new Abuses();
                    $r=$a->saveAbuse($user->id, $reportedUser->usr_id, $abuse->reason, $abuse->extra);
                    $this->flash->success('<div data-toggle="notify" data-onload data-message="Thanks! We will look into it." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                    //$this->response->redirect('user/profile/'. $abuse->uid);
                //if ends
                }
                    $this->flash->error('<div data-toggle="notify" data-onload data-message="Oops, unable to report user. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                    //$this->response->redirect('user/profile/'. $abuse->uid);
                //else ends
                }
            //if ends
            }else{
               $this->flash->error('<div data-toggle="notify" data-onload data-message="Oops, unable to report user. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                //else ends
            }
    	}else{
    		$this->flash->error('<div data-toggle="notify" data-onload data-message="Oops, unable to report user. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
    	}

    }
}