<?php

use \Phalcon\Tag,
    \Phalcon\Mvc\Url,
    Component\Json;

class AdminController extends ControllerBase
{

    public function indexAction()
    {
        Tag::setTitle('Admin');
        parent::initialize();
    }

    public function feedbackAction()
    {
        Tag::setTitle('Feedbacks');
        $this->retrieveFeedbackAction();
    }

    public function retrieveFeedbackAction()
    {
        $feedbackModel = new Feedback();
        $result = $feedbackModel->retrieveFeedback();

        if($result){
            $this->view->setVar('feedbacks', $result);
        }
    }

    public function userStatsAction()
    {
        Tag::setTitle('User Statistics');
        $this->retrieveNumberofUsers();
    }

    public function retrieveNumberOfUsers()
    {
        $userModel = new User();
        $result = $userModel->countTotalUsers();

        if($result){
            $this->view->setVar('NumberOfUsers', $result);
        }
    }

    public function compareUserAction()
    {
    	Tag::setTitle('Compare Users');
    }

    public function abuseMonitorAction()
    {
    	Tag::setTitle('Abuse Monitoring');
    }

    public function viewAbuseReportAction()
    {
        Tag::setTitle('Abuse');
        $this->retrieveReportAction();
    }

    public function retrieveReportAction()
    {
        $abuseModel = new Abuses();
        $result = $abuseModel->retrieveAbuses();

        if($result){
            $this->view->setVar('abuses', $result);
        }
    }

    public function findUserAction()
    {
        Tag::setTitle('Find User');
    }

    public function findUserByIdAction()
    {
        if($this->request->isPost()){
            $id = $this->request->getPost("id");
            $userModel = new User();
            $user = $userModel->findUserById($id);

            if($user){
                echo json_encode($user);
            }else{
                $this->flash->error("No user found.");
            }
            
        }
    }

    public function findUserByEmailAction()
    {
        if($this->request->isPost()){
            $email = $this->request->getPost("email");
            $userModel = new User();
            $user = $userModel->findUserByEmail($email);

            if($user){
                echo json_encode($user);
            }else{
                $this->flash->error("No user found.");
            }
            
        }
    }

    public function findContractAction()
    {
        Tag::setTitle('Find contract');
    }

    public function retrieveContractAction() 
    {
        if($this->request->isPost()){
            $contractId = $this->request->getPost("contractId");
            $hashids = new Hashids\Hashids(null, 8, null);
            $originalId = $hashids->decode($contractId);
            $id = implode('', $originalId);

            //error_log("<pre>contractId".print_r($contractId,true)."</pre>"); 
            //error_log("<pre>originalId".print_r($id,true)."</pre>"); 

            $contractModel = new Contracts();
            $contract = $contractModel->findContractById($id);

            if($contract){
                echo json_encode($contract);
            }else{
                $this->flash->error("No contract found.");
            }

        }
    }

    public function downtimeAction()
    {
        Tag::setTitle('Downtime Notice');
    }

    public function announcementAction()
    {
        Tag::setTitle('Announcements');
    }

    public function sendAction()
    {
        if($this->request->isPost()){
            $message = $this->request->getPost("message");
            $refId = null;
            $title = isset($message['title']) ? $message['title'] : ' ';
            $content = isset($message['message']) ? $message['message'] : '';
            $user = new User();
            $alluserids= $user->find(array(
                "columns" => "usr_id"
            ))->toArray();

            $targetId = (array_column($alluserids, 'usr_id'));
            foreach($targetId as $target) {
                if ($target && $content) {
                    $userId = $this->user->id;
                    $messageModel = new Messages();
                    $messageModel->saveMessage($userId, $target, $title, $content, $refId, Messages::MESSAGE_TYPE_ANNOUNCEMENT);
                    $system = new MessagesSystem();
                    $system->addMessage($messageModel->id, $userId, $target);
                    $this->flash->success("Announcements sent successfully.");
                    //$json->setJSON(false, 'Announcements sent successfully.', 1);   

                } else {
                    $this->flash->error("Failed to send announcement.");
                    //$json->setJSON(false, 'Failed to send announcement.', -1);             
                } 
            }
                                      
        } else {
            $this->flash->error("invalid method");
            return $this->response->redirect($this->request->getServer('HTTP_REFERER'));
        }
        $this->response->redirect($this->url->get("/admin/announcement"));
    }

    public function importAccountAction()
    {
        Tag::setTitle('Import Account');
    }

    public function addAccountAction()
    {
        if($this->request->isPost()){

            $this->view->disable();
            $failed = false;
             
            $firstname = $this->request->getPost("firstname");
            $lastname = $this->request->getPost("lastname");
            $fname = ucfirst($firstname);
            $lname = ucfirst($lastname);
            $email = $this->request->getPost("email");
            //$tokenKey = $this->request->getPost("tokenKey");
            //$token = $this->request->getPost("token");

            if ($this->security->checkToken()) {

                if(empty($fname)){
                    $this->flash->warning('<div data-toggle="notify" data-onload data-message="User firstname should not be empty!" data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
                    $failed = true;
                }
                if(empty($email)){
                    $this->flash->warning('<div data-toggle="notify" data-onload data-message="User email is required!" data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
                    $failed = true;
                }
                 
                if(!$failed){
                    $user = new User();
                    if(User::userExists($email)){
                        $this->flash->warning('<div data-toggle="notify" data-onload data-message="User already exists!" data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
                        $this->response->redirect($this->url->get(""));
                    }else{
                        $pwd = '';
                        $success = $user->importUser($email, $pwd, $fname, $lname);
                        if($success){
                            $this->flash->success('<div data-toggle="notify" data-onload data-message="User added successfully." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                            $user = User::findUserByEmail($email);
                            $this->component->user->createUserSession($user);
                            $this->response->redirect($this->url->get("overview"));
                        }
                        else{
                            $this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry! Please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                            $this->response->redirect($this->url->get(""));
                        }
                    }
                }else{
                    $this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to add user. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                    $this->response->redirect($this->url->get(""));
                }
                
            }else{
                $this->flash->warning('<div data-toggle="notify" data-onload data-message="Sorry! Please try again." data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
                $this->response->redirect($this->url->get(""));
            } 
            
        }else{
            $this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry! Please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');           
            $this->response->redirect($this->url->get(""));
        } 

        //$this->response->redirect($this->url->get(""));
    }

    public function importRequestAction()
    {
        Tag::setTitle('Import Request');
    }

    public function addRequestAction()
    {
        if($this->request->isPost()) {

            $json  = new Json();
            $tagId = $this->request->getPost('tagId');
            $title = $this->request->getPost('title');
            $task = $this->request->getPost('task');
            $fixed = $this->request->getPost('fixed');
            $price = $this->request->getPost('price');
            $hour = $this->request->getPost('hour');
            $quality = $this->request->getPost('quality');
            $multiple = $this->request->getPost('multiple');
            $datetime = $this->request->getPost('datetime');
            $postalCode = $this->request->getPost('postal');
            $postal = preg_replace('/\s+/', '', $postalCode);
            $cityId = $this->request->getPost('cityId');
            $travel = $this->request->getPost('travel');
            $email = $this->request->getPost('email');
            $firstname = ucfirst($this->request->getPost('firstname'));
            $lastname = ucfirst($this->request->getPost('lastname'));
            $phoneNumber = $this->request->getPost('phone');
            $phone = preg_replace('/\s+/', '', $phoneNumber);

            if ($this->security->checkToken()) {

                if(!isset($title) || !isset($task) || !isset($fixed) || !isset($price) || !isset($hour)) {
                    $json->setJSON(false, 'Please refresh this page and fill in all the fields as we need all the details to give you the best match.', -1);
                }else{
                    $userModel = new User();
                    $requestModel = new Requests();
                    $success = $requestModel->saveRequest(0, $tagId, $title, $task, $fixed, $price, $hour, $quality, $multiple, $datetime, $postal, $cityId, $travel, $phone);
                    if($success){
                        $citySkill = array($cityId, $tagId);
                        $json->setJSON(true, 'Thanks for using QuikWit! Your request will be sent to all qualified freelancers within your area.', 1, $citySkill);
                    }else{
                        $json->setJSON(false, 'Sorry, some error occured. Please try again later.', -1);
                    }
                
                }  
            }else{
                $json->setJSON(false, 'Sorry, please try again.', -1);
            } 

        }else{
            $json->setJSON(false, 'Sorry, some error occured. Please try again later.', -1);
        }

        $json->output();
    }

}