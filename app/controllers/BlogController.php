<?php

use \Phalcon\Tag;

class BlogController extends ControllerBase
{	
    /**
     * We simply pass all the posts created to the view
     */
    public function indexAction()
    {
    	Tag::setTitle('Blog');

        $postsModel = new Posts();

        $page = $this->request->get("page", "int", 0);
        if($page) {
            $page = $page;
        }
        else {
            $page = 1;
        }
        $offset = ($page-1)*Posts::POST_PER_PAGE;
        $totalPostCount = $postsModel->getTotalPostCount();
        $postCountPerPage = (($page*Posts::POST_PER_PAGE) >= $totalPostCount)?$totalPostCount:($page*Posts::POST_PER_PAGE); 
        if($page==1) {
            $prevPage = 1;
            if ($totalPostCount<=Posts::POST_PER_PAGE) {
                $nextPage = 1;
            }
            else {
                $nextPage = $page + 1;
            }
        }
        else if (($page*Posts::POST_PER_PAGE) >= $totalPostCount) {
            $prevPage = $page-1;
            $nextPage = $page;          
        }
        else {
            $prevPage = $page-1;
            $nextPage = $page+1;            
        }
        $this->view->setVar('totalPostCount', $postsModel->getTotalPostCount()); 
        $this->view->setVar('posts', $postsModel->getBlogPosts($offset, Posts::POST_PER_PAGE));         
        $this->view->setVar('postOffset', ($totalPostCount==0)?0:(($page-1)*(Posts::POST_PER_PAGE))+1);
        $this->view->setVar('postCountPerPage', $postCountPerPage);
        $this->view->setVar('prevPage', $prevPage);
        $this->view->setVar('nextPage', $nextPage);
        
    }

    /**
     * Let’s read that record from the database. When using MySQL adapter,
     * like we do in this tutorial, $slug variable will be escaped so
     * we don’t have to deal with it.
     */
    public function showAction($slug)
    {
        $postsModel = new Posts();

        $posts = $postsModel->getBlogPostFromSlug($slug);

        if ($posts === false) {
            $this->flash->error("Sorry, this post was not found.");
            $this->dispatcher->forward(array(
                'controller' => 'posts',
                'action' => 'index'
            ));
        }

        $this->view->setVar('posts', $posts);
    }

    public function createAction()
    {
    	Tag::setTitle('Carete a Post');
    }

    public function saveAction()
    {
        if($this->POSTS->isPost()){
            $categoryId = $this->POSTS->getPost('categoryId');
            $title = $this->POSTS->getPost('title');
            $slug = $this->POSTS->getPost('slug');
            $content = stripslashes($this->POSTS->getPost('content'));

            if($this->POSTS->hasFiles()){
                /* $finfo = new finfo(FILEINFO_MIME_TYPE);
                if (false === $ext = array_search(
                        $finfo->file($_FILES['image']['tmp_name']),
                        array(
                                'jpg' => 'image/jpeg',
                                'png' => 'image/png',
                                'gif' => 'image/gif',
                        ),
                        true
                )) {
                    throw new RuntimeException('Invalid file format.');
                } */
                $uploads = $this->POSTS->getUploadedFiles();
                //error_log("<pre>uploads".print_r($uploads,true)."</pre>");
                foreach($uploads as $upload){
                    #define a “unique” name and a path to where our file must go
                    $fileName = $upload->getName();
                    $filePath = 'public/uploads/post/'. $fileName;
                    $upload->moveTo($filePath);
                    
                }
            }

            $postsModel = new Posts();
            $success = $postsModel->savePost($title, $slug, $content, 1, $categoryId, $fileName);

            if($success){
                $this->flash->success('<div data-toggle="notify" data-onload data-message="Posts saved." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                $this->response->redirect($this->url->get('blog'));
            }else{
               $this->flash->error('<div data-toggle="notify" data-onload data-message="Error occured." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
               $this->response->redirect($this->url->get('blog/create')); 
            }

        }else{
            $this->flash->error('<div data-toggle="notify" data-onload data-message="Invalid method." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            $this->response->redirect($this->url->get('blog/create'));   
        } 
    }

    public function categoryAction($pcSlug)
    {
        Tag::setTitle('Blog');

        $postsModel = new Posts();

        $page = $this->request->get("page", "int", 0);
        if($page) {
            $page = $page;
        }
        else {
            $page = 1;
        }
        $offset = ($page-1)*Posts::POST_PER_PAGE;
        $totalPostCount = $postsModel->getTotalPostCount();
        $postCountPerPage = (($page*Posts::POST_PER_PAGE) >= $totalPostCount)?$totalPostCount:($page*Posts::POST_PER_PAGE); 
        if($page==1) {
            $prevPage = 1;
            if ($totalPostCount<=Posts::POST_PER_PAGE) {
                $nextPage = 1;
            }
            else {
                $nextPage = $page + 1;
            }
        }
        else if (($page*Posts::POST_PER_PAGE) >= $totalPostCount) {
            $prevPage = $page-1;
            $nextPage = $page;          
        }
        else {
            $prevPage = $page-1;
            $nextPage = $page+1;            
        }
        $this->view->setVar('totalPostCount', $postsModel->getTotalPostCount()); 
        $this->view->setVar('posts', $postsModel->getBlogPostsFromCategory($pcSlug, $offset, Posts::POST_PER_PAGE));                          
        $this->view->setVar('postOffset', ($totalPostCount==0)?0:(($page-1)*(Posts::POST_PER_PAGE))+1);
        $this->view->setVar('postCountPerPage', $postCountPerPage);
        $this->view->setVar('prevPage', $prevPage);
        $this->view->setVar('nextPage', $nextPage); 
    }

    public function updateAction()
    {
    }

    public function deleteAction($slug)
    {
    }

}

