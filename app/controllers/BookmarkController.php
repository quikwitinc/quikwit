<?php 

use \Phalcon\Tag;


class BookMarkController extends ControllerBase{
	
	private $_sessionUser;
	
	public function onConstruct()
	{
		parent::initialize();
		$user = $this->component->user->getSessionUser();
		if(!$user){
			$this->response->redirect($this->url->get("signin"));
			exit;
		}
	
		$userModel = new User();
		$userData = $userModel::findUserById($user->id);
		$this->view->userInfo =  $userData;
	
		$this->_sessionUser = $user;
	}
	
	
	public function indexAction(){
		Tag::setTitle('Bookmarks');
		$this->view->bookmarks = (new Bookmarks())->getUserBookmarks($this->_sessionUser->id);
	}

    public function getAction(){
    	$this->view->disable();
    	$result = new stdClass();
    	
    	if(!$this->component->user->hasSession()){
    		$result->success = false;
    		$result->message = "User not logged in.";
    	}else{
    		$user = $this->component->user->getSessionUser();
    		$b = new Bookmarks();
    		$bookmarks = $b->getUserBookmarks($user->id);
    		$result->success = true;
    		$result->content = array("bookmarks" => $bookmarks->toArray());
    	}
    	echo json_encode($result);
    }

    public function createAction(){
    	 
		$this->view->disable();
		
		if(!$this->component->user->hasSession()){
			$this->flash->error("Unable to create contract. User not logged in.");
			$this->response->redirect($this->url->get("/"));
		}
		
		$this->component->helper->csrf("/user/profile");
		 
		if($this->request->isPost()){
			$bookmark = (object)$this->request->getPost("bookmark");
			if(isset($bookmark->owner_id) && isset($bookmark->usr_id) || isset($bookmark->media_id)){
				$user = $this->component->user->getSessionUser();
				$b = new Bookmarks();
				$r=$b->saveBookmark($user->id, $bookmark->owner_id, $bookmark->usr_id, $bookmark->media_id);
				$this->flash->success("Bookmark was created successfully.");
			}else{
				$this->flash->error("Error occured.");
			}
		}else{
			$this->flash->error("Unable to save this item.");
		}

    }

    /**
     * Deletes a review
     *
     * @param int $id
     */
    public function deleteAction($id)
    {
        $review = Review::findFirstById($id);
        if (!$review) {

            $this->flash->error("Bookmark was not found");

            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        }

        if (!$bookmark->delete()) {
            $this->flash->error($user->getMessages());
        } else {
            $this->flash->success("Bookmark was deleted");
        }

        return $this->dispatcher->forward(array(
            'action' => 'index'
        ));
    }

}



?>