<?php

use \Phalcon\Tag,
    \Phalcon\Filter;


class BrowseController extends ControllerBase
{
	private $_user;

	public function onConstruct()
	{
	  parent::initialize();
	  $user = $this->component->user->getSessionUser();
	  /*if(!$user){
	  	$this->response->redirect($this->url->get("signin"));
	  	exit;
	  }*/
	  if($user)
	 		$this->_user = $user;
	}

	public function indexAction()
	{
		Tag::setTitle('Browse Gigs');
	    $requestModel = new Requests();
	    $loginUser = $this->component->user->getSessionUser();

	    if ($loginUser){
	       $userModel = new User();
	       $currentUser = $userModel->findUserById($loginUser->id);
	       $email_verified = $currentUser->usr_email_verified;
	       $email = $currentUser->usr_email;

	       $gps = User::getGPS($loginUser->id);
	       $this->view->setVar('loginUser', $loginUser);
	       $this->view->setVar('userId', $loginUser->id);
	       $this->view->setVar('email', $email);
	       $this->view->setVar('email_verified', $email_verified);
	       $this->view->setVar('lat', $gps['lat']);
	       $this->view->setVar('lng', $gps['lng']);
	    }else{
	       $loginUser = new stdClass;
	       $loginUser->id = '';
	       $this->view->setVar('loginUser', null);
	       $this->view->setVar('userId', 0);
	       $this->view->setVar('email', null);
	       $this->view->setVar('email_verified', 0);
	       $this->view->setVar('lat', 0);
	       $this->view->setVar('lng', 0);
	    } 

	    $requests = $requestModel->getAllAvailableRequests(1, 20);
	    //error_log("<pre>requests".print_r($requests,true)."</pre>");

	    if ($requests) {
	        $userComponent = new \Component\User();
	        $user_tz = $userComponent->getSessionUserTimeZone();
	        if ($user_tz) {
	            $this->view->setVar('timezone', $user_tz);
	        }

	        $this->view->setVar("requests", $requests['list']);
	        $this->view->setVar("total", $requests['total']);
	        $this->view->setVar("page", $requests['page']);
	        $this->view->setVar("numPages", $requests['numPages']);
	        
	    }

	}

	public function searchAction()
	{
	    $this->view->disable();
	    
	    $requestModel = new Requests();
	    $loginUser = $this->component->user->getSessionUser();
	    $this->view->setVar('loginUser', $loginUser);
	    if (!$loginUser) {
	      $loginUser = new stdClass;
	      $loginUser->id = '';
	    }
	    $page = $this->request->get("page");
	    $limit = 20;
	    if (!$page){
	        $page = 1;
	    }

	    $list = $requestModel->getAllAvailableRequests($page, $limit);

	    if ($list){
	        // error_log("<pre>list".print_r($list,true)."</pre>"); 
	        $list['status'] = true;
	    }
	    echo json_encode($list);
	}

	public function viewDetailAction($id)
	{
	    if (!$id) {
	        return $this->response->redirect($this->url->get('gig'));
	    }
	    Tag::setTitle('View Request');

	    $loginUser = $this->component->user->getSessionUser();
	    $userId = $loginUser->id;
	    $userModel = new User();
	    $currentUser = $userModel->findUserById($userId);
	    $email_verified = $currentUser->usr_email_verified;
	    $email = $currentUser->usr_email;
	    $userTeach = $currentUser->usr_teach;

	    if ($loginUser){
	       $gps = User::getGPS($loginUser->id);
	       $this->view->setVar('loginUser', $loginUser);
	       $this->view->setVar('loginUserId', $loginUser->id);
	       $this->view->setVar('email', $email);
	       $this->view->setVar('email_verified', $email_verified);
	       $this->view->setVar('userTeach', $userTeach);
	       $this->view->setVar('lat', $gps['lat']);
	       $this->view->setVar('lng', $gps['lng']);
	    }else{
	       $loginUser = new stdClass;
	       $loginUser->id = '';
	       $this->view->setVar('loginUser', null);
	       $this->view->setVar('loginUserId', 0);
	       $this->view->setVar('email_verified', 0);
	       $this->view->setVar('email', null);
	       $this->view->setVar('userTeach', 0);
	       $this->view->setVar('lat', 0);
	       $this->view->setVar('lng', 0);
	    }

	    $requests = Requests::getLeadsFromRequestId($id);

	    if ($requests) {
	        $userComponent = new \Component\User();
	        $user_tz = $userComponent->getSessionUserTimeZone();
	        if ($user_tz) {
	            $this->view->setVar('timezone', $user_tz);
	        }

	        $this->view->setVar('requests', $requests);
	        $request = $requests[sizeof($requests)-1];
	        $this->view->setVar('requestTag', $request['tag']);
	        $this->view->setVar('requestTitle', $request['req_title']);

	        // Update message status
	        foreach ($requests as $request) {
	            Requests::setLeadRead($request['req_id'], $userId);
	            $this->view->setVar('unreadMessageCount', Messages::getTotalUnreadCount($this->user->id));
	        }
	    }
	    
	}
	
    
}

