<?php 

use \Phalcon\Tag,
    Component\Json;


class ComingSoonController extends ControllerBase
{
    const REQUEST_PARAM_EMAIL = 'email';
    const REQUEST_PARAM_MESSAGE = 'message';

    public function indexAction(){
        /* $this->assets
        ->collection("cssprelaunch")
        ->setTargetPath('prelaunch.css')
        ->setTargetUri('css/prelaunch.css')
        ->addCss('third-party/me/css/bootstrap.css') 
        ->addCss('third-party/me/css/font-awesome/css/font-awesome.css') 
        ->addCss('third-party/me/css/fontello/css/fontello.css') 
        ->addCss('third-party/me/css/styles.css') 
        ->addCss('third-party/me/css/animate.css') 
        ->join(true)
        ->addFilter(new \Phalcon\Assets\Filters\Cssmin());

        $this->assets
        ->collection('jsprelaunch')
        ->setTargetPath('prelaunch.js')
        ->setTargetUri('js/prelaunch.js')
        ->addJs('third-party/me/js/jquery.js')
        ->addJs('third-party/me/js/bootstrap.min.js')
        ->addJs('third-party/me/js/scripts.js')
        ->addJs('third-party/me/js/jquery.parallax-1.1.3.js')
        ->join(true)
        ->addFilter(new \Phalcon\Assets\Filters\Jsmin()); */
        Tag::setTitle('QuikWit Coming Soon');
    }

    public function registerAction()
    {

        $json = new Json();

        //Store and check for errors
        //$name = $this->request->get("name", "string");
        $email = $this->request->get("email", "email");

        $form = new NewLetterForm(new Newsletters());
        if($form->isValid($this->request->get())){
            $newsletter = new Newsletters();
            if($newsletter->addSubscription($email)){
                //$this->flash->success("Thank you for subscribing. We will let you know when we launch ASAP :)");
                //$json->setJSON(true, 'Subscription is completed.', 1)->output();
                $this->response->redirect($this->url->get("coming-soon/complete"));
            }else{
                $this->flash->success("Please try again.");
                //$json->setJSON(false, 'Please Try again.', -1)->output();
                $this->response->redirect($this->url->get("coming-soon"));
            }
        }else{
            /* $messages = $form->getMessages();
            $result = [];
            foreach($messages as $idx => $message){
                $result[] = $message->getMessage();
            }
            $json->setJSON(false, 'Error', -1, $result)->output(); */
            $this->flash->success("Please inlude a '.' anywhere after '@' in the email address.");
            //$json->setJSON(false, 'Please Try again.', -1)->output();
            $this->response->redirect($this->url->get("coming-soon"));
        }
    }

    public function completeAction()
    {
        Tag::setTitle('Registration Completed.');
    }

    public function prizeAction(){

        Tag::setTitle('Rewards');
    }

    public function deleteAction($id)
    {
        $form = NewLetterFom::findFirstById($id);
        if (!$form) {

            $this->flash->error("You were not subscribed.");

            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        }

        if (!$form->delete()) {
            $this->flash->error($user->getMessages());
        } else {
            $this->flash->success("You are now unsubscribed.");
        }

        return $this->dispatcher->forward(array(
            'action' => 'index'
        ));
    }

    public function aboutAction(){

        Tag::setTitle('About Us');
    }

    public function timelineAction(){

        Tag::setTitle('Timeline');
    }

    public function contactAction(){

        Tag::setTitle('Contact Us');
    }

    public function blogAction(){

        Tag::setTitle('Blog');
    }

    public function pressAction(){

        Tag::setTitle('Press');
    }

    public function teacherAction(){

        Tag::setTitle('For Teachers');
    }

    public function jobAction(){

        Tag::setTitle('Jobs');
    }

    public function emailAction()
    {
        if ($this->request->isPost()) {

            $json = new Json();

            $email = $this->request->getPost(self::REQUEST_PARAM_EMAIL, 'email');
            if ($email) {
                $contacts = $this->getDI()->get('config')->get('contacts')->get('coming-soon');
                $gateway = $this->getDI()->get('config')->get('gmail-gateway');
                $mailto = $contacts->mailto;

                $this->getDI()->get('swiftmailer');
                // Create the mail transport configuration
                $transporter = Swift_SmtpTransport::newInstance($gateway->host, $gateway->port, $gateway->security)
                    ->setUsername($gateway->username)
                    ->setPassword($gateway->password);
                $message = Swift_Message::newInstance();
                $message->setTo($mailto);
                $message->setSubject("This email is sent using Swift Mailer");
                $message->setBody($this->request->getPost('message'));
                $message->setFrom($email, $this->request->getPost('name'));
                $mailer = Swift_Mailer::newInstance($transporter);
                $mailer->send($message);

                unset($message);

                $confirmation = Swift_Message::newInstance();
                $confirmation->setFrom($contacts->from, $contacts->name);
                $confirmation->setTo([$email => $this->request->getPost('name')]);
                $confirmation->setSubject("This email is sent using Swift Mailer");
                $confirmation->setBody('Thank you for your email, we will respond soon :).');
                $mailer->send($confirmation);
            }
        }
    }
}
