<?php

use \Phalcon\Tag;

class CommunityController extends ControllerBase
{
	const MAX_SIZE = 20000000;
	
	private $_sessionUser;
	
	public function onConstruct()
	{
		parent::initialize();
		$user = $this->component->user->getSessionUser();
		if(!$user){
			$this->response->redirect($this->url->get("signin"));
			exit;
		}
	
		$userModel = new User();
		$userData = $userModel::findUserById($user->id);
		$this->view->userInfo =  $userData;
	
		$this->_sessionUser = $user;
	}
	

    public function indexAction()
    {
        Tag::setTitle('Community');
        parent::initialize();
    }
    
    public function getPostsAction(){
    	$wall = new Wall();
    	$user = $this->component->user->getSessionUser();
    	if($posts = $wall->getPostsByUserId($user->id));
    	if($posts){
    		$this->component->helper->outputJSON(true, 1, "Posts found.", array("posts" => $posts->toArray()));
    	}else{
    		$this->component->helper->outputJSON(true, 1, "No Posts found.");
    	}
    }
    
    public function createPostAction(){
    	if ( !isset($_FILES['file']['error']) || is_array($_FILES['file']['error'])) {
    		$this->component->helper->outputJSON(false, -1, "Invalid Parameters.");
    	} else{
    		// Check $_FILES['upfile']['error'] value.
    		switch ($_FILES['file']['error']) {
    			case UPLOAD_ERR_OK:
    				break;
    			case UPLOAD_ERR_NO_FILE:
    				$this->component->helper->outputJSON(false, "File not found");
    			case UPLOAD_ERR_INI_SIZE:
    			case UPLOAD_ERR_FORM_SIZE:
    				$this->component->helper->outputJSON(false, "Exceeded file size limit");
    			default:
    				$this->component->helper->outputJSON(false, "Unknown error");
    		}
    	
    		// You should also check filesize here.
    		if ($_FILES['file']['size'] > self::MAX_SIZE) {
    			$this->component->helper->outputJSON(false, "Exceeded system file size limit");
    		}
    			
    		// DO NOT TRUST $_FILES['upfile']['mime'] VALUE !!
    		// Check MIME Type by yourself.
    		$finfo = new finfo(FILEINFO_MIME_TYPE);
    		if (false === $ext = array_search(
    				$finfo->file($_FILES['file']['tmp_name']),
    				array(
    						"mp4" => "video/mp4",
    						'jpg' => 'image/jpeg',
    						'png' => 'image/png',
    						'gif' => 'image/gif',
    				),
    				true
    		)) {
    			$this->component->helper->outputJSON(false, "Allowed formats: mp4, jpg, png, gif");
    		}
    			
    		$user = $this->component->user->getSessionUser();
    		//var_dump($user);
    		
    		$tempPath = $_FILES['file']['tmp_name'];
    		//All tests are passed, now move to upload location.
    		$uploader = new \Component\Uploader();
    		$uri = $user->id."/".$_FILES["file"]["name"];
    		$target = $uploader->getUploadPath($uri);
    		try{   	
    			$moved = $uploader->moveUploadedFile($tempPath, $target);
    	
    			if($moved){
    				//$yt->uploadVideo($target);
    				$wall = new Wall();
    				$url = $uploader->getFileUrl($uri);
    				$post = $wall->addWallPost($user->id, $_REQUEST["title"], $_REQUEST["content"], $_REQUEST["tag"], $url);
    				$this->component->helper->outputJSON(true, "Upload successful", array("post"=>$post));
    			}else{
    				$this->component->helper->outputJSON(false, "Upload failed");
    			}
    	
    		}catch(Exception $e){
    			//print_r($e);
    			$this->component->helper->outputJSON(false, $e->getMessage()." ({$e->getFile()} Line ".$e->getLine().")\n{$e->getTraceAsString()}", $e->getCode());
    			//$this->component->helper->outputJSON(false, $e, $e->getCode());
    		}
    	
    	}
    	
    }

}