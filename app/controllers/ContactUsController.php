<?php

use \Phalcon\Tag,
    \Component\Mailer\Manager as EmailManager,
    Component\Json;

class ContactUsController extends ControllerBase
{
    const REQUEST_PARAM_EMAIL = 'email';

    public function indexAction(){
    	Tag::setTitle('Contact Us');
    }

    public function emailAction()
    {
        if ($this->request->isPost()) {

            $json = new Json();

            $email = $this->request->getPost(self::REQUEST_PARAM_EMAIL, 'email');
            /*if ($email) {
                $contacts = $this->getDI()->get('config')->get('contacts')->get('coming-soon');
                $gateway = $this->getDI()->get('config')->get('gmail-gateway');
                $mailto = $contacts->mailto;

                $this->getDI()->get('swiftmailer');
                // Create the mail transport configuration
                $transporter = Swift_SmtpTransport::newInstance($gateway->host, $gateway->port, $gateway->security)
                    ->setUsername($gateway->username)
                    ->setPassword($gateway->password);
                $message = Swift_Message::newInstance();
                $message->setTo($mailto);
                $message->setSubject("This email is sent using Swift Mailer");
                $message->setBody($this->request->getPost('message'));
                $message->setFrom($email, $this->request->getPost('name'));
                $mailer = Swift_Mailer::newInstance($transporter);
                $mailer->send($message);

                unset($message);

                $confirmation = Swift_Message::newInstance();
                $confirmation->setFrom($contacts->from, $contacts->name);
                $confirmation->setTo([$email => $this->request->getPost('name')]);
                $confirmation->setSubject("This email is sent using Swift Mailer");
                $confirmation->setBody('Thank you for your email, we will respond soon :).');
                $mailer->send($confirmation);
            }*/
            if ($email) {
                $contacts = $this->getDI()->get('config')->get('contacts')->get('contact-us');
                $mailto = $contacts->mailto;
                $mailManager = (new EmailManager())->loadSettingsFromConfig();
                $res1 = $mailManager->send($mailto, $email, 'Email from Contact Form', $this->request->getPost('message', 'string'));
                $res2 = $mailManager->send([$email => $this->request->getPost('name')], $email, 'Hi there, ' . $this->request->getPost('name'), 'Thank you for your email, we will respond soon :)');
                $this->flash->success('<div data-toggle="notify" data-onload data-message="Thanks for reaching out to us! We will reply ASAP :)" data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                $json->setSuccess($res1 && $res2);
            } else {
                $this->flash->success('<div data-toggle="notify" data-onload data-message="Oops, some error occurred. Please email general@quikwit.co for now." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                $json->setSuccess(false);
            }

            $this->response->redirect($this->url->get('contact-us'));
            //$json->output();
        }
    }
}
