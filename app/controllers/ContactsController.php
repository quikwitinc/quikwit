<?php

use \Phalcon\Tag;

class ContactsController extends ControllerBase
{
	private $_sessionUser;

	public function onConstruct()
	{
		parent::initialize();
		$user = $this->component->user->getSessionUser();
		if(!$user){
			$this->response->redirect($this->url->get("signin"));
		}

		$userModel = new User();
		$userData = User::findUserById($user->id);
		$this->view->userInfo =  $userData;

		$this->_sessionUser = $user;
	}

    public function indexAction()
    {
        Tag::setTitle('Contacts');
        parent::initialize();
        $cModel = new Contacts();
        $userInfo = $this->_sessionUser;
        $userInfo->lat = $this->view->userInfo->lat;
        $userInfo->lng = $this->view->userInfo->lng;
        $contacts = $cModel->getUserContacts($this->_sessionUser->id);
        $userModel = new User();
        $currentUser = $userModel->findUserById($this->_sessionUser->id);
        $email = $currentUser->usr_email;
        $email_verified = $currentUser->usr_email_verified;

        $this->view->setVar("contactJson", json_encode($contacts));
        $this->view->setVar("userLogin", json_encode($userInfo));
        //$this->view->setVar("userId", $this->request->get("usr_id"));
        $this->view->setVar('currentUser', $currentUser);
        $this->view->setVar("email", $email);
        $this->view->setVar("email_verified", $email_verified);

    }

    public function deleteContactAction($id)
    {
        $userId = $this->_sessionUser->id;
        $contactModel = new Contacts();
        if ($contactModel->deleteContact($userId, $id)){
            echo json_encode(array('error' => false, 'message' => 'Deleted Contact'));
        }else{
            echo json_encode(array('error' => true, 'message' => 'Deleted Contact Failed'));
        }
    }

    public function userskilltagsAction() {
        $this->view->disable();
        $userid = $this->request->get("usr_id");
          $usertagsmodel = new UserTags();
          $userTagsObj = $usertagsmodel->getUserTags($userid);
          //print_r($userTagsObj);
          $userTags = (array_column($userTagsObj, 'name', 'tag_id'));
          //error_log("<pre>userTags".print_r($userTags,true)."</pre>"); 
          echo Phalcon\Tag::selectStatic(array('contract[tag]', 
                                          $userTags, 
                                          "using" => array("tag_id", "name"),
                                          "class" => "select-chosen form-control",
                                          "type" => "text",
                                          "id" => "usersTags",
                                          "data-role" => "tagsinput"
                                          ));
        exit;
    }
}