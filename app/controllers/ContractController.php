<?php

use \Phalcon\Tag,
	\Phalcon\Mvc\Url;

class ContractController extends ControllerBase
{
    public function onConstruct()
    {
        parent::initialize();
    }

    public function indexAction()
    {
        Tag::setTitle('Postings');
        $contractModel = new Contracts();
        $page = $this->request->get("page");
        $status = $this->request->get("status");
        if (!$page){
            $page = 1;
        }

        if( !$status){
            $status = 'all';
        }
        $limit = 10;
        $user = $this->component->user->getSessionUser();
        $contracts = $contractModel->getContracts($user->id, $page, $limit, $status);
        $this->view->contracts = $contracts;
        $this->view->userId = $user->id;
        $this->view->states = array(
            0 => '<div style="color: blue;">Pending</div>',
            1 => '<div style="color: green;">Active</div>',
            2 => '<div style="color: black;">Completed</div>',
            3 => '<div style="color: red;">Declined</div>',
            4 => '<div style="color: orange;">Under review</div>',
            5 => '<div style="color: grey;">Cancelled</div>'
            );
        $this->view->page = $page;
        $this->view->limit = $limit;
        $this->view->status = $status;
        $this->view->currentPage = $page;
        $this->view->firstPage = ($page - 5 < 0)?1:$page - 5;
        $this->view->lastPage = ($page + 5 > $contracts->total_pages)?$contracts->total_pages:$page + 5;

        $userComponent = new \Component\User();
        $user_tz = $userComponent->getSessionUserTimeZone();
        if ($user_tz) {
            $this->view->setVar('timezone', $user_tz);
        }
        
    }

    public function getAction(){
    	$this->view->disable();
    	$result = new stdClass();

    	if(!$this->component->user->hasSession()){
    		$result->success = false;
    		$result->message = "User not logged in.";
    	}else{
    		$user = $this->component->user->getSessionUser();
    		$c = new Contracts();
    		$contracts = $c->getUserContracts($user->id);
    		$result->success = true;
    		$result->content = array("contracts" => $contracts->toArray());
    	}
    	echo json_encode($result);
    }

    public function createAction(){

    	$this->view->disable();

    	if(!$this->component->user->hasSession()){
    		$this->flash->error("Unable to create contract. User not logged in.");
    		$this->response->redirect($this->url->get("/"));
    	}
    	//$this->component->helper->csrf("/user/profile");
    	$userModel = new User();
        $teacher = new stdClass;
    	if($this->request->isPost()){
            if($this->security->checkToken()){
                $contract = (object)$this->request->getPost("contract");
                $price = $this->request->getPost("price");
                if (isset($contract->uid)){
                    $teacher = User::findUserById($contract->uid);
                }
                $reqId = isset($contract->reqId) ? $contract->reqId : null;
                //error_log("<pre>teacher".print_r($teacher,true)."</pre>"); 
                //error_log("<pre>contract".print_r($contract,true)."</pre>"); 
                //die();
                if(isset($teacher->usr_id) && $teacher->usr_teach == 1){
                    $user = $this->component->user->getSessionUser();
                    $c = new Contracts();
                    $ch = new ContractHistory();
                    $client = $userModel->findUserById($user->id);
                    if ($client->usr_email_verified == 1) {
                        if ($c->hasContacted($user->id, $teacher->usr_id)){
                            $r=$c->saveContract($user->id, $teacher->usr_id,Contracts::PENDING, $price, $contract->tag, $contract->datetime, $contract->reqId);
                            $ch->createNewContract($user->id, $teacher->usr_id, $r,$contract->datetime);                                            
                            $this->flash->success('<div data-toggle="notify" data-onload data-message="&lt;b&gt;Contract was created successfully!&lt;/b&gt; A notification has been sent to the freelancer." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                            $this->response->redirect($this->url->get('contract'));
                            $tagModel = new TagsStandard();
                            $skillObj = $tagModel->findTagBySingleId($c->cnt_tagid);
                            $skill = $skillObj->name;
                            $hashids = new Hashids\Hashids(null, 8, null);
                            $newId = strtoupper($hashids->encode($c->cnt_id));
                            $m = new Messages();
                            $m->saveMessage(0, $teacher->usr_id, 'Confirmation of Booking with ' . $user->firstName .' (Contract ID ' . $newId . ')', "Hello $teacher->usr_firstname, <br><br> <a href='../../user/profile/$user->id'>$user->firstName $user->lastName</a> has sent you a booking for a contract in $skill. <br><br> 
                                                                                                                      The contract is currently priced at $ $price per hour. <br><br> If there are any discrepancies please decline this booking and contact <a href='../../user/profile/$user->id'>$user->firstName $user->lastName</a> for clarification or <a href='../contact-us'>Support</a> for assistance. <br><br> 
                                                                                                                      Please go to <a href='../../contract'>Manage</a> tab to either accept or decline the booking in the bulk options.", null, Messages::MESSAGE_TYPE_NOTIFICATION);
                            if ($teacher->usr_forward == 1) {
                                $nemail = new NotificationsEmail();
                                $nemail->sendConfirmationBooking($teacher->usr_email, $teacher->usr_firstname, $user->firstName, $user->lastName, $c->cnt_id, $skill);
                            }
                                                                                                                      
                        }else{
                            $this->flash->error('<div data-toggle="notify" data-onload data-message="Oops, looks like you did not message the freelancer before. Please send a message before proceeding to booking a contract." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                            $this->response->redirect('user/profile/'. $teacher->usr_id);
                        }  
                    }else{
                        $this->flash->error('<div data-toggle="notify" data-onload data-message="Please confirm your email first before proceeding." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                        $this->response->redirect('user/profile/'. $teacher->usr_id);
                    }                 
                }else if(isset($teacher->usr_id) && $teacher->usr_teach != 1){
                    $this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to book contract because this user is not a freelancer." data-options="{&quot;status&quot;:&quotdanger&quot;}" class="hidden-xs"></div>');
                    $this->response->redirect('user/profile/'. $teacher->usr_id);
                }else{
                    $this->flash->error('<div data-toggle="notify" data-onload data-message="Oops, failed to book a contract. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                    $this->response->redirect('user/profile/'. $contract->uid);
                }
            }
    	}else{
    		$this->flash->error('<div data-toggle="notify" data-onload data-message="Oops, failed to book a contract. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
    	}

    }

    public function updateContractAction(){
		$this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
		$result = new stdClass();

		try{
            $n = $this->request->get("next");
			$contractId = $this->request->get("contractId");
            $next = date('Y-m-j H:i', strtotime($n));
            $user = $this->component->user->getSessionUser();
			if($next && $contractId){
				$contract = new Contracts();
                $teacher = User::findUserById($contractId);
				if($contract->updateContractDate($user->id, $next, $contractId)){
                    $ch = new ContractHistory();
                    $ch->updateContractDate($contractId, $user->id, $next);
                    $reciever = User::findUserById($ch->reciever);
					$result->success = true;
					$result->message = "Next contract schedule has been updated";
                    $this->flash->success('<div data-toggle="notify" data-onload data-message="Next contract scheudle has been updated. A notification has been sent to the other party." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                    $tagModel = new TagsStandard();
                    $skillObj = $tagModel->findTagBySingleId($contract->cnt_tagid);
                    $skill = $skillObj->name;
                    $hashids = new Hashids\Hashids(null, 8, null);
                    $newId = strtoupper($hashids->encode($contractId));
                    $m = new Messages();
                    $m->saveMessage(0, $ch->reciever, 'Date/Time Changed for Contract with ' . $user->firstName .' (Contract ID ' . $newId . ')', "Hello $reciever->usr_firstname, <br><br> 
                                                                                                       This notification is to inform you that <a href='../../user/profile/$user->id'>$user->firstName $user->lastName</a> has rescheduled your $skill contract. <br><br>
                                                                                                       You can veiw this change in <a href='../../contract'>Manage</a> tab.<br><br>
                                                                                                       Please contact <a href='../../user/profile/$user->id'>$user->firstName $user->lastName</a> to resolve any conflict or <a href='../../contact-us'>Support</a> for assistance.", null, Messages::MESSAGE_TYPE_NOTIFICATION);
                    if ($reciever->usr_forward == 1) {
                        $nemail = new NotificationsEmail();
                        $nemail->sendTimeChange($reciever->usr_email, $user->firstName, $reciever->usr_firstname, $user->lastName, $ch->cnt_id, $skill);
                    }

				}else{
					$result->success = false;
					$result->message = "Failed to update next contract's date.";
				}
			}else{
				$result->success = false;
				$result->message = "Invalid parameters.";
			}
		}catch(Exception $e){
			$result->success = false;
			$result->message = "An occur occurred while updating contract";
		}
		$this->component->helper->outputJSON($result->success, $result->message);
	}

    public function acceptAction() {
    	$this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
    	$result = new stdClass();

    	try{
    		$contractId = $this->request->get("id");
            $contractModel = new Contracts();
            $contract = $contractModel->findContractById($contractId);
            $user = $this->component->user->getSessionUser();
            $ch = new ContractHistory();
            //Scenario #1: contract cannot be found or contract state is not in pending
            if (!$contract || $contract->cnt_state != 0){
                $result->success = false;
                $result->message = "Failed to accept contract because the contract is not in status PENDING.";
            }else{
            //Scenario #2: User accepts contract
            // Check contractHistory and see if teacher accepted it
            // if they did, change the status of the contract
            //else save their choice
            //Scenario #3: Teacher accepts contract
            // Check contractHistory and see if teacher accepted it
            // if they did, change the status of the contract
            //else save their choice
            $check = $ch->otherPartyAccept($user->id, $contractId);
                if ($check == ContractHistory::OTHERPARTYACCEPT){
                    //change state
                    $ch->saveParticipantAccept($user->id, $contractId);
                    $contractModel->acceptContract($contractId);
                    $reciever = User::findUserById($ch->reciever);
                    $prompter = User::findUserById($ch->prompter);
                    $result->success = true;
                    $result->message = "Contract " . $contractId . " is now Active.";
                    $this->flash->success('<div data-toggle="notify" data-onload data-message="&lt;b&gt;Contract is now Active!&lt;/b&gt; A notification has been sent to the other party." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                    $tagModel = new TagsStandard();
                    $skillObj = $tagModel->findTagBySingleId($contract->cnt_tagid);
                    $skill = $skillObj->name;
                    $hashids = new Hashids\Hashids(null, 8, null);
                    $newId = strtoupper($hashids->encode($contractId));
                    $m = new Messages();
                    $m->saveMessage(0, $ch->reciever, 'Contract with ' . $prompter->usr_firstname . ' is now Active'.' (Contract ID ' . $newId . ')', "Hello $reciever->usr_firstname, <br><br> 
                                                                                                    <a href='../../user/profile/$user->id'>$user->firstName $user->lastName</a> accepted your booking for a contract in $skill. <br><br> 
                                                                                                    No action is required of you at the moment. <br><br>
                                                                                                    Go to <a href='../../contract'>Manage</a> tab to manage your contract.<br><br>
                                                                                                    Enjoy your contract!", null, Messages::MESSAGE_TYPE_NOTIFICATION);
                    if ($reciever->usr_forward == 1) {
                        $nemail = new NotificationsEmail();
                        $nemail->sendAccept($reciever->usr_email, $prompter->usr_firstname, $reciever->usr_firstname, $prompter->usr_lastname, $ch->cnt_id, $skill);
                    }

                }else if ($check == ContractHistory::ALREADYACCEPTED) {
                    $result->success = false;
                    $result->message = "You already accepted this contract.";
                } else{
                    //save your choice
                    $ch->saveParticipantAccept($user->id, $contractId);
                    $result->success = true;
                    $result->message = "Contract  " . $contractId . "  accepted. Waiting for other party member.";
                    $this->flash->success('<div data-toggle="notify" data-onload data-message="&lt;b&gt;Booking accepted successfully!&lt;/b&gt; A notification has been sent to the other party." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                    $reciever = User::findUserById($ch->reciever);
                    $prompter = User::findUserById($ch->prompter);
                    $m = new Messages();
                    $m->saveMessage(0, $ch->reciever, 'Contract #' . $contractId . ' booking has been accepted.', "Hello $reciever->usr_firstname, <br><br> 
                                                                                                                This notification is to inform you that <a href='../../user/profile/$prompter->usr_id'>$prompter->usr_firstname $prompter->usr_lastname</a> has accepted your contract # $ch->cnt_id <br><br> 
                                                                                                                Please go to your <a href='../../contract'>Manage</a> tab to either accept or decline the contract in the bulk options.", null, Messages::MESSAGE_TYPE_NOTIFICATION); 
                    //$this->response->redirect($this->url->get('contract'));
                } 
            }
    	}catch(Exception $e){
    		$result->success = false;
    		$result->message = "An occur occurred while accepting contract " . $contractId ;
    	}
    	$this->component->helper->outputJSON($result->success, $result->message);
    }

    public function declineAction() {
    	$this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
    	$result = new stdClass();

    	try{
    		$contractId = $this->request->get("id");
            $contractModel = new Contracts();
            $contract = $contractModel->findContractById($contractId);
            $user = $this->component->user->getSessionUser();
            if (!$contract || $contract->cnt_state != 0){
                $result->success = false;
                $result->message = "Failed to decline contract because the contract is not in status PENDING.";
            }else{
                 $contractModel->setReject($contractId);
                 $ch = new ContractHistory();
                 $ch->declinecontract($contractId, $user->id);
                 $result->success = true;
                 $result->message = "Contract  " . $contractId . " declined.";
                 $this->flash->success('<div data-toggle="notify" data-onload data-message="&lt;b&gt;Booking declined successfully!&lt;/b&gt; A notification has been sent to the other party." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                 $reciever = User::findUserById($ch->reciever);
                 $prompter = User::findUserById($ch->prompter);
                 $tagModel = new TagsStandard();
                 $skillObj = $tagModel->findTagBySingleId($contract->cnt_tagid);
                 $skill = $skillObj->name;
                 $hashids = new Hashids\Hashids(null, 8, null);
                 $newId = strtoupper($hashids->encode($contractId));
                 $m = new Messages();
                 $m->saveMessage(0, $ch->reciever, 'Your Booking with ' . $prompter->usr_firstname . ' was Declined'.' (Contract ID ' . $newId . ')', "Hello $reciever->usr_firstname, <br><br> 
                                                                                              <a href='../../user/profile/$user->id'>$user->firstName $user->lastName</a> declined your booking for a contract in $skill. <br><br> 
                                                                                              No action is required of you at the moment. <br><br>
                                                                                              Please contact <a href='../../user/profile/$user->id'>$user->firstName $user->lastName</a> to resolve any conflict or contact <a href='../../contact-us'>Support</a> for assistance.<br><br>
                                                                                              You can manage your all your contracts on the <a href='../../contract'>Manage</a> tab.", null, Messages::MESSAGE_TYPE_NOTIFICATION);
                if ($reciever->usr_forward == 1) {
                    $nemail = new NotificationsEmail();
                    $nemail->sendDecline($reciever->usr_email, $prompter->usr_firstname, $reciever->usr_firstname, $prompter->usr_lastname, $ch->cnt_id, $skill);
                }
                 
            }
    	}catch(Exception $e){
    		$result->success = false;
    		$result->message = "An occur occurred while declining contract " . $contractId;
    	}
    	$this->component->helper->outputJSON($result->success, $result->message);
    }

    public function completeAction() {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $result = new stdClass();

        try{
            $contractId = $this->request->get("id");
            $contractModel = new Contracts();
            $contract = $contractModel->findContractById($contractId);
            $user = $this->component->user->getSessionUser();
            $ch = new ContractHistory();
            //Scenario #1: contract cannot be found or contract state is not in pending
            if (!$contract || $contract->cnt_state != Contracts::ACTIVE){
                $result->success = false;
                $result->message = "Failed to complete contract because the contract is not ACTIVE.";
            }else{
            /* //Scenario #2: User click complete
            // Check contractHistory and see if teacher has completed it
            // if they did, change the status of the contract
            //else save their complete status
            //Scenario #3: Teacher clicks complete
            // Check contractHistory and see if user completed it
            // if they did, change the status of the contract
            //else save their complete status
            $check = $ch->otherPartyComplete($user->id, $contractId);
                if ($check == ContractHistory::OTHERPARTYCOMPLETE){ */
                    //change state
                    $ch->saveParticipantComplete($user->id, $contractId);
                    $contractModel->completecontract($contractId);
                    $result->success = true;
                    $result->message = "Contract " . $contractId . " is completed.";
                    $this->flash->success('<div data-toggle="notify" data-onload data-message="&lt;b&gt;Contract is now Completed!&lt;/b&gt; " data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                    $searchUserModel = new SearchUsers();
                    $searchUserModel->updatecontract($contract->cnt_teachid);
                    $client = User::findUserById($contract->cnt_user);
                    $teacher = User::findUserById($contract->cnt_teachid);
                    $contract = $contractModel->findContractById($contractId);
                    $tagModel = new TagsStandard();
                    $skillObj = $tagModel->findTagBySingleId($contract->cnt_tagid);
                    $skill = $skillObj->name;
                    $hashids = new Hashids\Hashids(null, 8, null);
                    $newId = strtoupper($hashids->encode($contractId));
                    $m = new Messages();
                    $m->saveMessage(0, $contract->cnt_user, 'Contract with ' . $teacher->usr_firstname . ' is Completed'.' (Contract ID ' . $newId . ')', "Hello $client->usr_firstname, <br><br>
                                                                                                      We saw that you successfully completed your $skill contract with <a href='../../user/profile/$teacher->usr_id'>$teacher->usr_firstname $teacher->usr_lastname</a>. <br><br>
                                                                                                      We hope you had a great contract! <br><br> 
                                                                                                      Sharing your experience with the QuikWit community could help promote the freelancer, initiate improvements and assist other users in making great choices. <br><br> 
                                                                                                      We will love to hear about your experience! <br><br> 
                                                                                                      You can leave a review by clicking the review button under your contract's Completed status in <a href='../../contract'>Manage</a> tab.
                                                                                                      If you did not complete your contract and this notification is incorrect please contact <a href='../../contact-us'>Support</a> for assistance.", null, Messages::MESSAGE_TYPE_NOTIFICATION);
                    if ($client->usr_forward == 1) {
                        $nemail = new NotificationsEmail();
                        $nemail->sendComplete($client->usr_email, $teacher->usr_firstname, $client->usr_firstname, $teacher->usr_lastname, $ch->cnt_id, $skill);
                    }

                /* }else if ($check == ContractHistory::ALREADYCOMPLETE){
                    $result->success = false;
                    $result->message = "You already marked this contract as completed.";
                }else{
                    //save your choice
                    $ch->saveParticipantComplete($user->id, $contractId);
                    $result->success = true;
                    $result->message = "contract  " . $contractId . "  completed. Waiting for other party member.";
                    $this->flash->success('<div data-toggle="notify" data-onload data-message="&lt;b&gt;contract is now marked with completion by you!&lt;/b&gt; A notification has been sent to the other party." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                    $reciever = User::findUserById($ch->reciever);
                    $prompter = User::findUserById($ch->prompter);
                    $m = new Messages();
                    $m->saveMessage(0, $ch->reciever, 'Confirmation for contract with ' . $prompter->usr_firstname . ' Completion', "Hello $reciever->usr_firstname, <br><br> 
                                                                                                                <a href='../../user/profile/$prompter->usr_id'>$prompter->usr_firstname $prompter->usr_lastname</a> has marked the completion of the contract # $ch->cnt_id with you on $contract->cnt_next . <br><br> 
                                                                                                                If this information is inaccurate please contact <a href='../../user/profile/$prompter->usr_id'>$prompter->firstname $prompter->usr_lastname</a> for clarification. <br><br>
                                                                                                                You can confirm the contract completion or report the contract by selecting the option complete or report in <a href='/contract'>contracts</a>. <br><br>
                                                                                                                Note that by reporting this contract the contract status will be switched to <strong>Under Review</strong> and will be further investigated by QuikWit.", null, Messages::MESSAGE_TYPE_NOTIFICATION);
                    $this->response->redirect($this->get->url('contract'));
                } */
            }
        }catch(Exception $e){
            $result->success = false;
            $result->message = "An occur occurred while accepting contract " . $contractId ;
        }
        $this->component->helper->outputJSON($result->success, $result->message);
    }

    /* public function denyAction() {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $result = new stdClass();

        try{
            $contractId = $this->request->get("id");
            $contractModel = new Contracts();
            $contract = $contractModel->findContractById($contractId);
            $user = $this->component->user->getSessionUser();
            $ch = new ContractHistory();
            //Scenario #1: contract cannot be found or contract state is not in active
            if (!$contract || $contract->cnt_state != Contracts::ACTIVE){
                $result->success = false;
                $result->message = "Failed to deny contract because the contract is not in status ACTIVE.";
            }else{
            //Scenario #2: User click deny
            // Check contractHistory and see if teacher has completed it
            // if they did, save the entry and delete the other persons entry
            //Scenario #3: Teacher clicks deny
            // Check contractHistory and see if user completed it
            // if they did, save entry and delete otehr person entry
            $check = $ch->otherPartyComplete($user->id, $contractId);
                if ($check == ContractHistory::OTHERPARTYCOMPLETE){
                    //change state
                    $ch->saveParticipantDeny($user->id, $contractId);
                    $ch->removeCompletedEntry($contractId);
                    $result->success = true;
                    $result->message = "contract " . $contractId . " is denied completion.";
                    $this->flash->success('<div data-toggle="notify" data-onload data-message="&lt;b&gt;Thanks for letting us know!&lt;/b&gt; A notification has been sent to the other party." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                    $reciever = User::findUserById($ch->reciever);
                    $prompter = User::findUserById($ch->prompter);
                    $m = new Messages();
                    $m->saveMessage(0, $ch->reciever, 'Completion of contract with ' . $prompter->usr_firstname . ' is Denied', "Hello $reciever->usr_firstname, <br><br> 
                                                                                                            This notification is to inform you that the status of your contract # $ch->cnt_id on $contract->cnt_next has changed from Active to <strong>Under Review</strong> status. <br><br> 
                                                                                                            No action is required of you at the moment and this contract will be further investigated by QuikWit. <br><br>
                                                                                                            Please contact support with any issue regarding this notification. We are always happy to help.", null, MESSAGE_TYPE_NOTIFICATION);
                }else{
                    $result->success = false;
                    $result->message = "Failed to Deny contract.";
                }
            }
        }catch(Exception $e){
            $result->success = false;
            $result->message = "An occur occurred while accepting contract " . $contractId ;
        }
        $this->component->helper->outputJSON($result->success, $result->message);
    } */

    public function reviewAction() {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $result = new stdClass();

        try{
            $contractId = $this->request->getPost("id");
            $reason = $this->request->getPost('reason');
            $text = $this->request->getPost('text');
            $contractModel = new Contracts();
            $contract = $contractModel->findContractById($contractId);
            $user = $this->component->user->getSessionUser();
            $ch = new ContractHistory();
            //Scenario #1: contract cannot be found or contract state is not in active
            if (!$contract || $contract->cnt_state == Contracts::PENDING){
                $result->success = false;
                $result->message = "Failed to report contract because the contract is in status PENDING.";
            }else{
            //Scenario #2: User click review
            // Check contractHistory and see if teacher has completed it
            // if they did, save the entry and delete the other persons entry
            //Scenario #3: Teacher clicks review
            // Check contractHistory and see if user completed it
            // if they did, save entry and delete otehr person entry
            //$check = $ch->hasDeny($contractId);
                //if ($check){
                    //change state
                    $ch->saveParticipantReview($user->id, $contractId);
                    $success = $contractModel->reviewcontract($contractId);
                    $contractsReportedModel = new ContractsReported();
                    $contractsReportedModel->saveReport($user->id, $contractId, $reason, $text);
                    $result->success = true;
                    $result->message = "Contract " . $contractId . " is now under review.";
                    $this->flash->success('<div data-toggle="notify" data-onload data-message="&lt;b&gt;Thanks for letting us know!&lt;/b&gt; A notification has been sent to the other party." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                    $reciever = User::findUserById($ch->reciever);
                    $prompter = User::findUserById($ch->prompter);
                    $tagModel = new TagsStandard();
                    $skillObj = $tagModel->findTagBySingleId($c->cnt_tagid);
                    $skill = $skillObj->name;
                    $hashids = new Hashids\Hashids(null, 8, null);
                    $newId = strtoupper($hashids->encode($contractId));
                    $m = new Messages();
                    $m->saveMessage(0, $ch->reciever, 'Contract with ' . $prompter->usr_firstname . ' is now Under Review.' . ' (Contract ID ' . $newId . ')', "Hello $reciever->usr_firstname, <br><br> 
                                                                                                        Your $skill contract with <a href='../../user/profile/$teacher->usr_id'>$teacher->usr_firstname $teacher->usr_lastname</a> is under review. <br><br> 
                                                                                                        This is due to different reports received reagarding the completion of the contract.<br><br>
                                                                                                        This contract will be further investigated by the QuikWit Team. <br><br>
                                                                                                        No action is required of you at the moment.<br><br> 
                                                                                                        Please contact <a href='../../contact-us'>Support</a> for more details. We are always happy to help.", null, MESSAGE_TYPE_NOTIFICATION);
                    if ($reciever->usr_forward == 1) {
                        $nemail = new NotificationsEmail();
                        $nemail->sendUnderReview($reciever->usr_email, $prompter->usr_firstname, $reciever->usr_firstname, $prompter->usr_lastname, $ch->cnt_id, $skill);
                    }

                //}else{
                    //$result->success = false;
                    //$result->message = "Failed to put contract under review.";
                //}
            }
        }catch(Exception $e){
            $result->success = false;
            $result->message = "An occur occurred while putting contract " . $contractId . " under review";
        }
        $this->component->helper->outputJSON($result->success, $result->message);
    }

    public function reviewTeacherAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $result = new stdClass();

        try{
            $title = $this->request->getPost("title");
            $comment = $this->request->getPost("comment");
            $overall = $this->request->getPost("overall");
            $punctuality = $this->request->getPost("punctuality");
            $friendliness = $this->request->getPost("friendliness");
            $teacher = $this->request->getPost("teacher");
            $knowledge = $this->request->getPost("knowledge");
            $teaching = $this->request->getPost("teaching");
            $contract_date_month_year = $this->request->getPost("contract_date_month_year");
            $contractId = $this->request->getPost("contractId");
            $parts = explode(',', $contract_date_month_year);
            $conditions = (strlen($title) > 0) &&
                          (strlen($comment) > 0) &&
                          ($overall > 0 && $overall < 6) &&
                          ($punctuality > 0 && $punctuality < 6) &&
                          ($friendliness > 0 && $friendliness < 6) &&
                          ($knowledge > 0 && $knowledge < 6) &&
                          ($teaching > 0 && $teaching < 6) &&
                          (count($parts) == 2);
            $user = $this->component->user->getSessionUser();
            if($contractId && $conditions){
                $review = new Reviews();
                $month = (int)$parts[0];
                $year = (int)$parts[1];
                $reviewResult = $review->saveReview(array(
                    'title' => $title,
                    'comment' => $comment,
                    'overall' => $overall,
                    'punctuality' => $punctuality,
                    'friendliness' => $friendliness,
                    'knowledge' => $knowledge,
                    'teaching' => $teaching,
                    'month' => $month,
                    'year' => $year,
                    'contractId' => $contractId,
                    'user' => $user->id,
                    'teacher' => $teacher
                    ));
                if($reviewResult){
                    $result->success = true;
                    $result->message = "Review Saved.";
                    $this->flash->success('<div data-toggle="notify" data-onload data-message="&lt;b&gt;Review is posted succesfully!&lt;/b&gt; It will be posted within 24 hours." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                    $searchUserModel = new SearchUsers();
                    $searchUserModel->updateNumRating($teacher);
                    $searchUserModel->updateRating($teacher);
                    //$n = new Notifications();
                    //$n->add('contract', 'reviewTeacherAction', $teacher, 'A client has added an review for contract ' . $contractId . '.', '/contract');

                }else{
                    $result->success = false;
                    $result->message = "Failed to save review.";
                }
            }else{
                $result->success = false;
                $result->message = "Sorry some of the fields are missing.";
            }
        }catch(Exception $e){
            $result->success = false;
            $result->message = "Failed to save review";
        }
        $this->component->helper->outputJSON($result->success, $result->message);
    }

    public function cancelAction() {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $result = new stdClass();

        try{
            $contractId = $this->request->get("id");
            $contractModel = new Contracts();
            $contract = $contractModel->findContractById($contractId);
            $user = $this->component->user->getSessionUser();
            $ch = new ContractHistory();
            //Scenario #1: contract cannot be found or contract state is not in active
            if (!$contract || $contract->cnt_state != Contracts::ACTIVE){
                $result->success = false;
                $result->message = "Failed to cancel contract because the contract is not in status ACTIVE.";
            }else{
                $ch->cancelcontract($contractId, $user->id);
                $contractModel->cancelcontract($contractId);
                $result->success = true;
                $result->message = "contract " . $contractId . " is cancelled.";
                $this->flash->success('<div data-toggle="notify" data-onload data-message="&lt;b&gt;contract cancelled successfully!&lt;/b&gt; A notification has been sent to the other party." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                $reciever = User::findUserById($ch->reciever);
                $prompter = User::findUserById($ch->prompter);
                $tagModel = new TagsStandard();
                $skillObj = $tagModel->findTagBySingleId($c->cnt_tagid);
                $skill = $skillObj->name;
                $hashids = new Hashids\Hashids(null, 8, null);
                $newId = strtoupper($hashids->encode($contractId));
                $m = new Messages();
                $m->saveMessage(0, $ch->reciever, 'contract with ' . $prompter->usr_firstname . ' is Cancelled' . ' (contract ID ' . $newId . ')', "Hello $reciever->usr_firstname, <br><br> 
                                                                                            This notification is to inform you that <a href='../../user/profile/$prompter->usr_id'>$prompter->usr_firstname $prompter->usr_lastname</a> has cancelled your scheduled $skill contract.<br><br> 
                                                                                            No action is required of you at the moment. <br><br>
                                                                                            Please contact <a href='../../user/profile/$prompter->usr_id'>$prompter->usr_firstname $prompter->usr_lastname</a> to resolve any conflict or <a href='../../contact-us'>Support</a> for assistance. We are always happy to help.", null, Messages::MESSAGE_TYPE_NOTIFICATION);
            }
        }catch(Exception $e){
            $result->success = false;
            $result->message = "An occur occurred while cancelling contract " . $contractId ;
        }
        $this->component->helper->outputJSON($result->success, $result->message);
    }

    public function noActionReplyAction(){

        if($this->request->isPost()){

            $this->view->disable();
            $noAction = $this->request->getPost("noAction");

            if($noAction == 0){
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Please select an action to perform." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            }

        }else{
            $this->response->redirect('contract');
        }
    }

    public function noSelectReplyAction(){

        if($this->request->isPost()){

            $this->view->disable();
            $noSelect = $this->request->getPost("noSelect");

            if($noSelect == 0){
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Please select at least one contract to process." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            }

        }else{
            $this->response->redirect('contract');
        }
    }

    public function cannotAcceptReplyAction(){

        if($this->request->isPost()){

            $this->view->disable();
            $cannot = $this->request->getPost("cannot");

            if($cannot == 0){
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Please check the contract(s) selected as only contracts with status Pending can be accepted." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            }

        }else{
            $this->response->redirect('contract');
        }
    }

    public function cannotDeclineReplyAction(){

        if($this->request->isPost()){

            $this->view->disable();
            $cannot = $this->request->getPost("cannot");

            if($cannot == 0){
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Please check the contract(s) selected as only contracts with status Pending can be declined." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            }

        }else{
            $this->response->redirect('contract');
        }
    }

    public function cannotCompleteReplyAction(){

        if($this->request->isPost()){

            $this->view->disable();
            $cannot = $this->request->getPost("cannot");

            if($cannot == 0){
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Please check the contract(s) selected as only contracts with status Active can be completed." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            }

        }else{
            $this->response->redirect('contract');
        }
    }

    /* public function cannotDenyReplyAction(){

        if($this->request->isPost()){

            $this->view->disable();
            $cannot = $this->request->getPost("cannot");

            if($cannot == 0){
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Please check the contract(s) selected as only contracts with status Active(client/teacher completed) can be denied completion." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            }

        }else{
            $this->response->redirect('contract');
        }
    } */

    public function cannotReportReplyAction(){

        if($this->request->isPost()){

            $this->view->disable();
            $cannot = $this->request->getPost("cannot");

            if($cannot == 0){
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Please check the contract(s) selected as only contracts with status Active or Completed can be put under Review." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            }

        }else{
            $this->response->redirect('contract');
        }
    }

    public function cannotCancelReplyAction(){

        if($this->request->isPost()){

            $this->view->disable();
            $cannot = $this->request->getPost("cannot");

            if($cannot == 0){
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Please check the contract(s) selected as only contracts with status Active can be cancelled. If this contract is tied to one of your Requests, please go to your Requests tab to cancel the request." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            }

        }else{
            $this->response->redirect('contract');
        }
    }

}