<?php

use \Phalcon\Tag;

class ContractreviewController extends ControllerBase
{
	public function initializes()
	{
	    parent::initialize();
	}

    public function getAction(){
    	$this->view->disable();
    	$result = new stdClass();
    	
    	if(!$this->component->user->hasSession()){
    		$result->success = false;
    		$result->message = "User not logged in.";
    	}else{
    		$user = $this->component->user->getSessionUser();
    		$r = new Reviews();
    		$reviews = $r->getUserReviews($user->id);
    		$result->success = true;
    		$result->content = array("reviews" => $reviews->toArray());
    	}
    	echo json_encode($result);
    }

    public function countAction(){
        
        $this->view->disable();
        $result = new stdClass();
        if(!$this->component->user->hasSession()){
            $result->success = false;
            $result->message = "User not logged in.";
        }else{
            $user = $this->component->user->getSessionUser();
            $count = Reviews::getUnreadCount($user->id);
            $result->success = true;
            $result->content = array("count" => $count);
        }
        echo json_encode($result);
    }

    public function createAction(){
    	 
		$this->view->disable();
		
		if(!$this->component->user->hasSession()){
			$this->flash->error("Unable to create contract. User not logged in.");
			$this->response->redirect($this->url->get("/"));
		}
		
		$this->component->helper->csrf("/user/profile");
		 
		if($this->request->isPost()){
			$contract = (object)$this->request->getPost("review");
			if(isset($contract->cnt_user) && isset($contract->cnt_teachid)){
				$user = $this->component->user->getSessionUser();
				$r = new Reviews();
				$j=$r->saveReview($user->id, $review->rvs_user, $review->rvs_title, $review->rvs_trust, $review->rvs_friend, $review->rvs_quality, $review->rvs_time, $review->rvs_comment);
				$this->flash->success("Review was created successfully.");
			}else{
				$this->flash->error("Not enough info given.");
			}
		}else{
			$this->flash->error("Unable to create review.");
		}

    }

    /**
     * Edits an existing review
     *
     * @param int $id
     */
    public function editAction($id)
    {
        $review = Review::findFirstById($id);
        if (!$review) {
            $this->flash->error("Review was not found");
            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        }

        if ($this->request->isPost()) {

        	if($review) {
        		$review = new Review();
        		$review->assign(array(
        		    'title' => $this->request->getPost('title', 'striptags'),
        		    'comment' => $this->request->getPost('comment')
        		));
        	}

        	if($trust){
        		if($review->setTrust($this->_sessionUser->id, $trust)){
        			$result->success = true;
        		}elseif($review->setFriend($this->_sessionUser->id, $friend)){
        			$result->success = true;
        			
        		}elseif($review->setQuality($this->_sessionUser->id, $quality)){
        			$result->success = true;
        		}elseif($review->setTime($this->_sessionUser->id, $time)){
        			$result->success = true;
        		}else{
        			$result->success = false;
        			$result->message = "Failed to update ratings.";
        		}
        	}else{
        		$result->success = false;
        		$result->message = "Invalid parameters.";
        	}

            if (!$user->save()) {
                $this->flash->error($user->getMessages());
            } else {
                $this->flash->success("Review was updated successfully");
            }

            Tag::resetInput();
        }

        $this->view->review = $review;
    }

    /**
     * Deletes a review
     *
     * @param int $id
     */
    public function deleteAction($id)
    {
        $review = Review::findFirstById($id);
        if (!$review) {

            $this->flash->error("Review was not found");

            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        }

        if (!$review->delete()) {
            $this->flash->error($user->getMessages());
        } else {
        	$review->rvs_deleted = time();
            $this->flash->success("Review was deleted");
        }

        return $this->dispatcher->forward(array(
            'action' => 'index'
        ));
    }

    public function reportReviewAction($id)
    {
        $reviewModel = new Reviews();
        $review = $reviewModel->findReviewById($id);
        $reviewId = $review->rvs_id;
        if ($review) {
            $reviewModel->report($reviewId);
            $this->flash->success('<div data-toggle="notify" data-onload data-message="Thanks! We will look into it." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
            $json->setJSON(true, 'Report is completed.', 1);
        } else {
            $this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to report review. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            $json->setJSON(false, 'Failed to report review.', -1);
        }
    
    }


}