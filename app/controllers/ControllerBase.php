<?php

use Phalcon\Tag;

class ControllerBase extends \Phalcon\Mvc\Controller
{

	protected $user = null;

	public function beforeDispatchLoop($dispatcher){

		/*print "Before Dispatch Loop";
		 print_r($dispatcher);
		exit;
		print "hello world";
		exit;
		global $config;

		if($config->get("maintenance")){
		print "hello world";
		print_r($this->getParams());
		exit;
		$this->dispatcher->forward(array(
				"controller" => "coming-soon",
				"action" => "index"));
		}*/
	}

	public function beforeDispatch($dispatcher){

		/*print "Before Dispatch";
		 print_r($dispatcher);
		exit; */
	}

	public function beforeExecuteRoute($dispatcher){

        $config = $this->getDI()->getShared('config');

		$maintenance = $config->get("maintenance");

		if($maintenance->get("mode")){
			$controllers = $maintenance->get("controllers")->toArray();
            $actions  =  $maintenance->get("actions")->toArray();


            $controller = $dispatcher->getControllerName();
            $action = $dispatcher->getActionName();

			if (!in_array($controller, $controllers) || !isset($actions[$controller]) || !in_array($action, $actions[$controller])) {
                $this->response->redirect($maintenance->get('default'));
            }
		}
	}

	public function afterExecuteRoute($dispatcher){

		/*print "After ExecuteRoute";
		 print_r($dispatcher);
		exit; */
	}

	public function beforeNotFoundAction($dispatcher){

		/*print "Before Not Found Action";
		 print_r($dispatcher);
		exit; */
	}

	public function beforeException($dispatcher){

		/*print "Before Exception";
		 print_r($dispatcher);
		exit; */
	}

	public function afterDispatch($dispatcher){

		/*print "After Dispatch";
		 print_r($dispatcher);
		exit; */
	}

	public function afterDispatchLoop($dispatcher){

		/* print "After Dispatch Loop";
		 print_r($dispatcher);
		exit; */
	}

	public function initialize()
	{
		Tag::prependTitle('QuikWit |');
		if ($this->component->user->hasSession()) {
			$this->user = $this->component->user->getSessionUser();
			$this->view->setVar('user', $this->user);
			$userData = User::findUserById($this->user->id);
			$userTeach = $userData->usr_teach;
			$this->view->setVar('userTeach', $userTeach);
			$this->view->setVar('firstname', $this->user->firstName);
            $this->view->setVar('unreadMessageCount', Messages::getTotalUnreadCount($this->user->id));
            $this->view->setVar('unreadGigCount', RequestsLead::getUnreadRequestCount($this->user->id));
		}

		$di = \Phalcon\DI\FactoryDefault::getDefault();
		$security = $di->get('security');
		$tokenKey = $security->getTokenKey();
		$token = $security->getToken();
		$this->view->setVar("tokenKey", $tokenKey);
		$this->view->setVar("token", $token);
		
		$tagsStandard = new TagsStandard();
		$tags = $tagsStandard->find(
		   array(
		       "status = '1'",
		       "order" => "name"
		     )
		 )->toArray();
		$tagsColumn = array_column($tags, 'name');
		$this->view->setVar('tagsColumn', $tagsColumn);

		if (! function_exists('array_column')) {
		    function array_column(array $input, $columnKey, $indexKey = null) {
		        $array = array();
		        foreach ($input as $value) {
		            if ( ! isset($value[$columnKey])) {
		                trigger_error("Key \"$columnKey\" does not exist in array");
		                return false;
		            }
		            if (is_null($indexKey)) {
		                $array[] = $value[$columnKey];
		            }
		            else {
		                if ( ! isset($value[$indexKey])) {
		                    trigger_error("Key \"$indexKey\" does not exist in array");
		                    return false;
		                }
		                if ( ! is_scalar($value[$indexKey])) {
		                    trigger_error("Key \"$indexKey\" does not contain scalar value");
		                    return false;
		                }
		                $array[$value[$indexKey]] = $value[$columnKey];
		            }
		        }
		        return $array;
		    }
		}

		//$this->fastCache();
		
	}

	/* protected function fastCache()
	{	
	    $ExpireDate = new \DateTime();
	    $ExpireDate->modify('+1 hour');
	    $response = $this->di['response'];
	    $response->setExpires($ExpireDate);
	    $response->setHeader('Cache-Control', 'public, max-age=3600, s-max=3600, must-revalidate, proxy-revalidate');
	} */

    public function getSessionUser()
    {
        return $this->component->user->getSessionUser();
    }

    public function hasSession()
    {
        return $this->component->user->hasSession();
    }

    public function createSession($user)
    {
        return $this->component->user->createUserSession($user);
    }

    public function reCreateSession()
    {
        return $this->component->user->refreshSessionUser();
    }

}