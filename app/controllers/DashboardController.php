<?php

use \Phalcon\Tag;

class DashboardController extends ControllerBase
{
	private $_sessionUser;
	
	public function onConstruct()
	{
		parent::initialize();
		
		if(!$this->component->user->hasSession()){
			$this->response->redirect($this->url->get("signin"));
			return;
		}
		$user = $this->component->user->getSessionUser();
		$userModel = new User();
		$userData = User::findUserById($user->id);
		$this->view->setVar('userInfo', $userData);
		$this->view->setVar('profileUrl', $this->url->get('profile/'.$user->id));
		
		$this->_sessionUser = $user;
	}

	public function indexAction()
	{
		Tag::setTitle('dashboard');
	}
}