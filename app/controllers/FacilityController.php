<?php

use \Phalcon\Tag;

class FacilityController extends ControllerBase
{
    public function indexAction()
    {
        Tag::setTitle('Add Your Facility');
        if($this->component->user->hasSession()){
        	$this->view->setVar("user", $this->user);
        }
        //parent::initialize();
    }

    public function signupFacilityAction()
    {
        Tag::setTitle('Add Your Facility');
    }

}