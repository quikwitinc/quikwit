<?php

use \Phalcon\Tag,
    Phalcon\Mvc\Url,
    \DateTime,
    Component\Json as JSON;

class GigController extends ControllerBase
{
    public function onConstruct()
    {
        parent::initialize();
    }

    public function indexAction()
    {
    	Tag::setTitle('Client Requests');
        $this->initializeLeadStats();
    }

    protected function initializeLeadStats()
    {
        $requests = new Requests();
        $userId = $this->user->id;
        $userModel = new User();
        $user = $userModel->findUserById($userId);
        $page = $this->request->get("page", "int", 0);
        if($page) {
            $page = $page;
        }
        else {
            $page = 1;
        }
        $offset = ($page-1)*Requests::REQUEST_PER_PAGE;
        $totalLeadCount = Requests::getUserLeadCount($userId);
        $leadCountPerPage = (($page*Requests::REQUEST_PER_PAGE) >= $totalLeadCount)?$totalLeadCount:($page*Requests::REQUEST_PER_PAGE); 
        if($page==1) {
            $prevPage = 1;
            if ($totalLeadCount<=Requests::REQUEST_PER_PAGE) {
                $nextPage = 1;
            }
            else {
                $nextPage = $page + 1;
            }
        }
        else if (($page*Requests::REQUEST_PER_PAGE) >= $totalLeadCount) {
            $prevPage = $page-1;
            $nextPage = $page;          
        }
        else {
            $prevPage = $page-1;
            $nextPage = $page+1;            
        }
        $this->view->setVar('totalLeadCount', Requests::getUserLeadCount($userId)); 
        $this->view->setVar('leads', $requests->getUserLeads($userId, $offset, Requests::REQUEST_PER_PAGE));

        //error_log("<pre>leads".print_r($requests->getUserLeads($userId, $offset, Requests::REQUEST_PER_PAGE),true)."</pre>"); 

        $this->view->setVar('leadOffset', ($totalLeadCount==0)?0:(($page-1)*(Requests::REQUEST_PER_PAGE))+1);
        $this->view->setVar('leadCountPerPage', $leadCountPerPage);
        $this->view->setVar('prevPage', $prevPage);
        $this->view->setVar('nextPage', $nextPage);
        $this->view->setVar('userId', $this->user->id); 
        $this->view->setVar('userTeach' , $user->usr_teach);
    
        $userComponent = new \Component\User();
        $user_tz = $userComponent->getSessionUserTimeZone();
        if ($user_tz) {
            $this->view->setVar('timezone', $user_tz);
        }
    }

    public function viewRequestAction($id)
    {
        if (!$id) {
            return $this->response->redirect($this->url->get('gig'));
        }
        Tag::setTitle('View Request');

        $loginUser = $this->component->user->getSessionUser();
        $userId = $loginUser->id;
        $userModel = new User();
        $currentUser = $userModel->findUserById($userId);
        $email_verified = $currentUser->usr_email_verified;
        $email = $currentUser->usr_email;
        $userTeach = $currentUser->usr_teach;

        if ($loginUser){
           $gps = User::getGPS($loginUser->id);
           $this->view->setVar('loginUser', $loginUser);
           $this->view->setVar('loginUserId', $loginUser->id);
           $this->view->setVar('email', $email);
           $this->view->setVar('email_verified', $email_verified);
           $this->view->setVar('userTeach', $userTeach);
           $this->view->setVar('lat', $gps['lat']);
           $this->view->setVar('lng', $gps['lng']);
        }else{
           $loginUser = new stdClass;
           $loginUser->id = '';
           $this->view->setVar('loginUser', null);
           $this->view->setVar('loginUserId', 0);
           $this->view->setVar('email_verified', 0);
           $this->view->setVar('email', null);
           $this->view->setVar('userTeach', 0);
           $this->view->setVar('lat', 0);
           $this->view->setVar('lng', 0);
        }

        $requests = Requests::getUserLeadsFromRequestIds($userId, $id);

        if ($requests) {
            $userComponent = new \Component\User();
            $user_tz = $userComponent->getSessionUserTimeZone();
            if ($user_tz) {
                $this->view->setVar('timezone', $user_tz);
            }

            $this->view->setVar('requests', $requests);
            $request = $requests[sizeof($requests)-1];
            $this->view->setVar('requestTag', $request['tag']);
            $this->view->setVar('requestTitle', $request['req_title']);

            // Update message status
            foreach ($requests as $request) {
                Requests::setLeadRead($request['req_id'], $userId);
                $this->view->setVar('unreadMessageCount', Messages::getTotalUnreadCount($this->user->id));
            }
        }
        
    }


}