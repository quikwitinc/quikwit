<?php

use \Phalcon\Tag;

class IndexController extends ControllerBase
{
    public function indexAction()
    {
        Tag::setTitle('Home');
        
        if($this->component->user->hasSession()){
            $user = $this->component->user->getSessionUser();
            $this->view->setVar("user", $user);
            //$this->view->setVar('messageCount', Messages::getMessageCount($user->id));
            //error_log("<pre>server".print_r($this->request->getServer('HTTP_REFERER'),true)."</pre>"); 
            $loginUser = $user;
            if ($loginUser){
               $userModel = new User();
               $currentUser = $userModel->findUserById($loginUser->id);
               $email_verified = $currentUser->usr_email_verified;
               $email = $currentUser->usr_email;

               $gps = User::getGPS($loginUser->id);
               $this->view->setVar('loginUser', $loginUser);
               $this->view->setVar('userId', $loginUser->id);
               $this->view->setVar('email', $email);
               $this->view->setVar('email_verified', $email_verified);
               $this->view->setVar('lat', $gps['lat']);
               $this->view->setVar('lng', $gps['lng']);
            }
        }else{
           $loginUser = new stdClass;
           $loginUser->id = '';
           $this->view->setVar('loginUser', null);
           $this->view->setVar('userId', 0);
           $this->view->setVar('email', null);
           $this->view->setVar('email_verified', 0);
           $this->view->setVar('lat', 0);
           $this->view->setVar('lng', 0);
        } 

        $requestModel = new Requests();
        $requests = $requestModel->getAllAvailableRequests(1, 4);
        //error_log("<pre>requests".print_r($requests,true)."</pre>");

        if ($requests) {
            $userComponent = new \Component\User();
            $user_tz = $userComponent->getSessionUserTimeZone();
            if ($user_tz) {
                $this->view->setVar('timezone', $user_tz);
            }

            $this->view->setVar("requests", $requests['list']);
            $this->view->setVar("total", $requests['total']);
            $this->view->setVar("page", $requests['page']);
            $this->view->setVar("numPages", $requests['numPages']);
            
        }

        /* $tagModel = new TagsStandard();
        $tags = $tagModel->find(array(
            "status = 1",
            "order" => "name",
            "limit" => "50"
        ))->toArray();  
        $tagName = array_column($tags, 'name');
        $tag = implode('</a> <a href="" class="label label-info">', $tagName);
        $tagList = '<a href="" class="label label-info">' . $tag . '</a> and more';
        //error_log("<pre>tag".print_r($tag,true)."</pre>"); 
        $this->view->setVar('tagList', $tagList); */

        //parent::initialize();
    }

    public function signoutAction()
    {
       // $this->session->destroy();
        //$this->response->redirect('index/');
    }

    public function show404Action()
    {
        Tag::setTitle('Error 404');
        $this->response->setStatusCode(404, 'Not Found');
        $this->view->pick('index/show404');    
    }

    public function maintenanceAction()
    {
        Tag::setTitle('Maintenance');   
    }
    
    /* public function retrieveUnreadCount()
    {
        if ($this->request->isAjax()) {

            if ($this->component->user->hasSession()) {
                $this->user = $this->component->user->getSessionUser();

                $unreadMessageCount = Messages::getTotalUnreadCount($this->user->id);
                echo json_encode(['newMessages' => $unreadMessageCount]);
                exit;
            }
        }

    } */

    // Bootstrap Data Below
    // --------------------------------

    /* public function generatePasswordAction($password)
    {
       echo $this->security->hash($password);
    }

    public function startSessionAction()
    {
        $this->session->set('user', [
            'name' => 'Ted',
            'age' => 55,
            'soOn' => 'soForth'
        ]);
        $this->session->set('name', 'Jesse');
    }

    public function getSessionAction()
    {
        $user = $this->session->get('user');
        print_r($user);
        echo $this->session->get('name');
    }

    public function removeSessionAction()
    {
        echo $this->session->remove('name');
    }

    public function destroySessionAction()
    {
        echo $this->session->destroy();
    }

    public function route404Action()
    {
        echo "fuked";
    } */

}