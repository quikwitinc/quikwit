<?php

use \Phalcon\Tag,
	Component\Json;

class MediaController extends ControllerBase
{
	public function initialize()
	{
		parent::initialize();
	}

	public function createAction(){
		
		$json = new Json();
		$hasError = false;
		
		//Supports single upload.
		if ( !isset($_FILES['file']['error']) || is_array($_FILES['file']['error'])) {
			$json->setJSON(false, "Invalid Parameters.", -1);
			$hasError = true;
 		} else{
			// Check $_FILES['upfile']['error'] value.
			switch ($_FILES['file']['error']) {
				case UPLOAD_ERR_OK:
					break;
				case UPLOAD_ERR_NO_FILE:
					$json->setJSON(false, "File not found", 1);
					$hasError = true;
					break;
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:
					$json->setJSON(false, "Exceeded file size limit", -1);
					$hasError = true;
					break;
				default:
					$json->setJSON(false, "Unknown error.", -1);
					$hasError = true;
					break;
			}
			 
			// You should also check filesize here.
			/*if ($_FILES['file']['size'] > self::MAX_SIZE) {
				$this->component->helper->outputJSON(false, "Exceeded system file size limit");
			} */
			 
			$finfo = new finfo(FILEINFO_MIME_TYPE);
			if (false === $ext = array_search(
					$finfo->file($_FILES['file']['tmp_name']),
					array(
							"mp4" => "video/mp4",
							//'jpg' => 'image/jpeg',
							//'png' => 'image/png',
							//'gif' => 'image/gif',
					),
					true
			)) {
				$this->component->helper->outputJSON(false, "Allowed formats: mp4");
			}
			 
			$user = $this->component->user->getSessionUser();
			$tempPath = $_FILES['file']['tmp_name'];
			//All tests are passed, now move to upload location.
			$uploader = new \Component\Uploader();
			$uri = $user->id."/".$_FILES["file"]["name"];
			$target = $uploader->getUploadPath($uri);
			try{				 
				$yt = new \Component\Youtube();				 
				$moved = $uploader->moveUploadedFile($tempPath, $target);				 
				if($moved){
					$yt->uploadVideo($target);
					$url = $uploader->getFileUrl($uri);
					$this->component->helper->outputJSON(true, "Upload successful", array("url"=>$url));
				}else{
					$this->component->helper->outputJSON(false, "Upload failed");
				}
				 
			}catch(Exception $e){
				//print_r($e);
				$this->component->helper->outputJSON(false, $e->getMessage()." ({$e->getFile()} Line ".$e->getLine().")\n{$e->getTraceAsString()}", $e->getCode());
				//$this->component->helper->outputJSON(false, $e, $e->getCode());
			}
		}
	}
	
	public function doMediaUploadAction()
	{

		if($this->request->isPost()==true){
			 
			$this->view->disable();
			$failed = false;
			$vtitle = $this->request->getPost("title", "string");
			$vtags = $this->request->getPost("user-skills-input");
			$type = $this->request->getPost("media_type");
			$content = $this->request->getPost("media_content");
			
			if(empty($vtitle)){
				$this->flash->warning('<div data-toggle="notify" data-onload data-message="Please put in a title and type." data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
				$failed = true;
			}
			$mediaFile = '';	 
			if(!$failed){
				if ($this->request->hasFiles() == true) {
					$uploads = $this->request->getUploadedFiles();					
					if($type == 1) {
					  	$isUploaded = false;
					  	#do a loop to handle each file individually
					  	foreach($uploads as $upload){
							if(trim($upload->getname()) != "") {
							   	#define a “unique” name and a path to where our file must go
							   	$fileName = $upload->getname();
							   	$actual_image_name = md5(uniqid(rand(), true)).'-'.strtolower($upload->getname());
							   	
							   	$api = $this->getDI()->get('api');
								$bucket 		= $api->amazon->bucket;
								$awsAccessKey 	= $api->amazon->awsAccessKey;
								$awsSecretKey 	= $api->amazon->awsSecretKey;
								
								$s3 = new \Component\AmazonS3($awsAccessKey, $awsSecretKey);
								$s3->putBucket($bucket, $s3::ACL_PUBLIC_READ);
								
								if($s3->putObjectFile($upload->getTempName(), $bucket , $actual_image_name, $s3::ACL_PUBLIC_READ)) {
									$mediaFile='http://'.$bucket.'.s3.amazonaws.com/'.$actual_image_name;
								}
								else {
									$this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to add media in S3, please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
									$this->response->redirect($this->url->get("overview"));
								}
							}
					 	}
					}else {
						$yt = new \Component\Youtube();
						foreach($uploads as $upload){				  		
							if(trim($upload->getname()) != "") {						
								$mediaFile = $yt->uploadVideoToYoutube($upload->getTempName(), $vtitle, $vtags, $content);
							}
						}
					}

					if($mediaFile){
						$media = new Media(); 
						$user = $this->component->user->getSessionUser();
						$status = $media->addMedia($user->id, $vtitle, $vtags, $fileName, $mediaFile, $type, $content);
						$this->flash->success('<div data-toggle="notify" data-onload data-message="Media has been added to your profile successfully." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
						$this->response->redirect($this->url->get("overview"));
					}else{
						$this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to add media, please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
					}
				}
				
			}else{
				$this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to register media." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
			}
		}else{
			$this->flash->error('<div data-toggle="notify" data-onload data-message="Invalid request method." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
		}
		$this->response->redirect($this->url->get("overview"));
	}
	
	function doDeleteAction() {
		$media = new Media();
		$id = $this->request->getQuery("id", "int"); 
		$status = $media->deleteMedia($id);
		if($status) {
			$this->flash->success('<div data-toggle="notify" data-onload data-message="Media has been removed from your profile successfully." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
		}
		else {
			$this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to remove media." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
		}
		$this->response->redirect($this->url->get("overview"));
	}
}