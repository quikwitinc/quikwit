<?php

use Phalcon\Tag,
    Phalcon\Mvc\Url,
    \DateTime,
    Component\Json as JSON;

class MessageController extends ControllerBase
{
    public function onConstruct()
    {
        parent::initialize();
        $user = $this->getSessionUser();
        if(!$user){
            $this->response->redirect($this->url->get("signin"));
            $this->response->send();
        } else {
            $userData = User::findUserById($user->id);
            $this->view->userInfo =  $userData;
        }
    }

    public function indexAction()
    {
        Tag::setTitle('Messages');
        $this->initializeLeftMenuStats();
        $this->initializeMessageStats(1);
    }

    public function announcementsAction()
    {
        Tag::setTitle('Announcements');
        $this->initializeLeftMenuStats();
        $this->initializeMessageStats(3);
    }

    public function notificationsAction()
    {
        Tag::setTitle('Notifications');
        $this->initializeLeftMenuStats();
        $this->initializeMessageStats(2);
    }

    public function viewAnnouncementAction($id)
    {

        if (!$id) {
            return $this->response->redirect($this->url->get('message/announcements'));
        }

        Tag::setTitle('View announcements');
        $userId = $this->user->id;

        $messageIds = Messages::getTargetedAnnouncementIds($id);
        $messages = Messages::getUserAnnouncementsFromMessageIds($userId, $messageIds);

        if ($messages) {
            $userComponent = new \Component\User();
            $user_tz = $userComponent->getSessionUserTimeZone();
            if ($user_tz) {
                $this->view->setVar('timezone', $user_tz);
            }

            $this->view->setVar('announcements', $messages);
            $message = $messages[sizeof($messages)-1];
            $this->view->setVar('announcementTitle', $message['title']);

            // Update message status
            foreach ($messages as $message) {
                Messages::setMessageRead($message['id'], $userId);
                $this->view->setVar('unreadMessageCount', Messages::getTotalUnreadCount($this->user->id));
            }
        }
        
        $this->initializeLeftMenuStats();
    }
    

    public function viewNotificationAction($id)
    {

        if (!$id) {
            return $this->response->redirect($this->url->get('message/notifications'));
        }

        Tag::setTitle('View notifications');
        $userId = $this->user->id;

        $messageIds = Messages::getTargetedNotificationIds($id);
        $messages = Messages::getUserNotificationsFromMessageIds($userId, $messageIds);

        if ($messages) {
            $userComponent = new \Component\User();
            $user_tz = $userComponent->getSessionUserTimeZone();
            if ($user_tz) {
                $this->view->setVar('timezone', $user_tz);
            }

            $this->view->setVar('notifications', $messages);
            $message = $messages[sizeof($messages)-1];
            $this->view->setVar('notificationTitle', $message['title']);

            // Update message status
            foreach ($messages as $message) {
                Messages::setMessageRead($message['id'], $userId);
                $this->view->setVar('unreadMessageCount', Messages::getTotalUnreadCount($this->user->id));
            }
        }
        
        $this->initializeLeftMenuStats();
    }

    public function viewMessageAction($id)
    {

        if (!$id) {
            return $this->response->redirect($this->url->get('message'));
        }

        Tag::setTitle('View messages');
        $userId = $this->user->id;
        $this->view->userId = $userId;

        $messageIds = Messages::getTargetedContactMessageIds($id);
        $messages = Messages::getUserContactMessagesFromMessageIds($userId, $messageIds);

        if ($messages) {
            $userComponent = new \Component\User();
            $user_tz = $userComponent->getSessionUserTimeZone();
            if ($user_tz) {
                $this->view->setVar('timezone', $user_tz);
            }

            $this->view->setVar('messages', $messages);
            $message = $messages[sizeof($messages)-1];
            $this->view->setVar('messageTitle', $message['title']);
            $this->view->setVar('messageRequestId', $message['request_id']);
            $this->view->setVar('requestUserId', $message['request_userid']);
            $this->view->setVar('replyTo', $message['id']);
            $uid = $message['target_id'];
            $this->view->setVar('uid', $uid);

            // Update message status
            foreach ($messages as $message) {
                //if ($message['created_by'] != $this->user->id){
                    Messages::setMessageRead($message['id'], $userId);
                    $this->view->setVar('unreadMessageCount', Messages::getTotalUnreadCount($this->user->id));
                //}
            }
        }
        
        $this->initializeLeftMenuStats();
    }

    public function getAction(){
        $json = new JSON();

        $offset = $this->request->get("offset", "int", 0);
        $count = $this->request->get("count", "int", 0);

        if(!$this->component->user->hasSession()){
            $json->setJSON(false, "User not logged in.", -1);
        }else{
            $user = $this->component->user->getSessionUser();
            $m = new Messages();
            $msgs = $m->getNewMessages($user->id, 0, $offset, $count);
            $json->setJSON(true, "", 1, ["messages" => $msgs->toArray()]);
            //$result->success = true;
            //var_dump($msgs);
            //$result->content = array("messages" => $msgs->toArray());
        }
        $json->output();
    }

    public function countAction(){
        $json = new JSON();
        $result = new stdClass();
        if(!$this->component->user->hasSession()){
            $json->setJSON(false, "User not logged in.", -1);
        }else{
            $user = $this->component->user->getSessionUser();
            $count = Messages::getUnreadCount($user->id);
            $json->setJSON(true, "", 1, ["count" => $count]);
        }
        $json->output();
    }

    public function retrieveUnreadCount()
    {
        if ($this->component->user->hasSession()) {
            $this->user = $this->component->user->getSessionUser();
            $this->view->setVar('user', $this->user);
            $this->view->setVar('unreadMessageCount', Messages::getTotalUnreadCount($this->user->id));
        }
    }

    public function sendAction()
    {
        //$this->component->helper->csrf("/user/profile");
        if($this->request->isPost()){
            //error_log("<pre>post".print_r($_POST,true)."</pre>"); 
            //die();
            if($this->security->checktoken()){
                $message = $this->request->getPost("message");
                $refId = null;
                $title = isset($message['title']) ? $message['title'] : ' ';
                $content = isset($message['message']) ? $message['message'] : '';
                $targetId = isset($message['uid']) ? $message['uid'] : null;
                $requestId = isset($message['requestId']) ? $message['requestId'] : null;
                if (isset($message['reply_to'])) {
                    $srcMessage = Messages::getMessageById($message['reply_to']);
                    if ($srcMessage) {
                        $refId = $srcMessage->getRefId();
                    }
                    $title = !empty($title) ? $srcMessage->getTitle() : $srcMessage->getTitle();
                }

                if ($targetId && $content && (int)$targetId != (int)$this->user->id) {
                    $userModel = new User();
                    $targetUser = $userModel->findUserById($targetId);
                    if ($targetUser->usr_deleted == 0) {
                        $sender = $userModel->findUserById($this->user->id);
                        if ($sender->usr_email_verified == 1) {
                            $userId = $this->user->id;
                            $messageModel = new Messages();
                            $success = $messageModel->saveMessage($userId, $targetId, $title, $content, $refId, Messages::MESSAGE_TYPE_CONTACT, $requestId);
                            $this->flash->success('<div data-toggle="notify" data-onload data-message="Message is sent succesfully!" data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');

                            if ($targetUser->usr_forward == 1) {
                                if (!empty($targetUser->usr_password)) {
                                    $nemail = new NotificationsEmail();
                                    $nemail->sendGotMessage($targetUser->usr_email, $targetUser->usr_firstname, $this->user->firstName, $this->user->lastName);
                                }
                            }

                            if ($success) {
                                if (!Contacts::isContact($userId, $targetId)) {
                                    (new Contacts())->addContact($userId, $targetId);
                                }
                            }

                            if ($this->request->isAjax()) {
                                if ($success) {
                                    $json = new JSON();
                                    $json->setSuccess($success);
                                    $json->setMessage($success ? 'Message is sent succesfully' : 'Failed to sent message.');
                                    $json->setStatus($success ? 1 : -1);
                                    $json->output();
                                }
                            } /*else {
                                if (isset($message['redirect']) && ($redirect = $message['redirect'])) {
                                    return $this->response->redirect($redirect);
                                } else {
                                    return $this->response->redirect($this->url->get('message/viewMessage/' . $messageModel->getRefId()));
                                }
                            }*/ 
                        }else{
                            $this->flash->error('<div data-toggle="notify" data-onload data-message="Please confirm your email first before proceeding." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                        }       
                    }else{
                        $this->flash->error('<div data-toggle="notify" data-onload data-message="This user account has been deactivated at the request of the account owner. Please contact us if you need assistance." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                    }        
                }
            }else{
               $this->flash->warning('<div data-toggle="notify" data-onload data-message="Invalid CSRF token." data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>'); 
            }
            
        }

        if ($this->request->isAjax()) {
            $json = new JSON();
            $json->setJSON(false, 'Bad request.', -1);
            $json->output();
        } else {
            return $this->response->redirect($this->request->getServer('HTTP_REFERER'));
        }
    }

    protected function initializeLeftMenuStats()
    {
        $userId = $this->user->id;
        $this->view->setVar('totalUnreadAnnouncementCount', Messages::getUnreadCount($userId, Messages::MESSAGE_TYPE_ANNOUNCEMENT));
        $this->view->setVar('totalUnreadMessageCount', Messages::getUnreadCount($userId, Messages::MESSAGE_TYPE_CONTACT));
        $this->view->setVar('totalUnreadNotificationCount', Messages::getUnreadCount($userId, Messages::MESSAGE_TYPE_NOTIFICATION));
    }

    protected function initializeMessageStats($type = 1)
    {
        $messages = new Messages();
        $userId = $this->user->id;
        $page = $this->request->get("page", "int", 0);
        if($page) {
        	$page = $page;
        }
        else {
        	$page = 1;
        }
    	$offset = ($page-1)*Messages::MESSAGE_PER_PAGE;
    	switch ($type) {
    		case 1:
        		$totalMessageCount = Messages::getMessageCount($userId, Messages::MESSAGE_TYPE_CONTACT);
        		break;
    		case 2:
        		$totalMessageCount = Messages::getMessageCount($userId, Messages::MESSAGE_TYPE_NOTIFICATION);
        		break;
    		case 3:
        		$totalMessageCount = Messages::getMessageCount($userId, Messages::MESSAGE_TYPE_ANNOUNCEMENT);
        		break;
    	}
        $messageCountPerPage = (($page*Messages::MESSAGE_PER_PAGE) >= $totalMessageCount)?$totalMessageCount:($page*Messages::MESSAGE_PER_PAGE);
        if($page==1) {
        	$prevPage = 1;
        	if ($totalMessageCount<=Messages::MESSAGE_PER_PAGE) {
        		$nextPage = 1;
        	}
        	else {
        		$nextPage = $page + 1;
        	}
        }
        else if (($page*Messages::MESSAGE_PER_PAGE) >= $totalMessageCount) {
        	$prevPage = $page-1;
        	$nextPage = $page;        	
        }
        else {
        	$prevPage = $page-1;
        	$nextPage = $page+1;        	
        }
        $this->view->setVar('totalMessageCount', Messages::getMessageCount($userId, Messages::MESSAGE_TYPE_CONTACT));
        $this->view->setVar('totalNotificationCount', Messages::getMessageCount($userId, Messages::MESSAGE_TYPE_NOTIFICATION));
        $this->view->setVar('totalAnnouncementCount', Messages::getMessageCount($userId, Messages::MESSAGE_TYPE_ANNOUNCEMENT));
        $this->view->setVar('messages', $messages->getUserContactMessages($userId, $offset, Messages::MESSAGE_PER_PAGE));
        $this->view->setVar('notifications', $messages->getUserNotifications($userId, $offset, Messages::MESSAGE_PER_PAGE));
        $this->view->setVar('announcements', $messages->getUserAnnouncements($userId, $offset, Messages::MESSAGE_PER_PAGE));
        $this->view->setVar('messageOffset', ($totalMessageCount==0)?0:(($page-1)*(Messages::MESSAGE_PER_PAGE))+1);
        $this->view->setVar('messageCountPerPage', $messageCountPerPage);
        $this->view->setVar('notificationCountPerPage', $messageCountPerPage);
        $this->view->setVar('announcementCountPerPage', $messageCountPerPage);
        $this->view->setVar('prevPage', $prevPage);
        $this->view->setVar('nextPage', $nextPage);
        $this->view->setVar('userId', $this->user->id);
 
        $userComponent = new \Component\User();
        $user_tz = $userComponent->getSessionUserTimeZone();
        if ($user_tz) {
            $this->view->setVar('timezone', $user_tz);
        }
    }

    public function deleteMessageAction() {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $result = new stdClass();

        try{
            $id = $this->request->get("id");
            $messageModel = new Messages();
            $message = $messageModel->getMessageById($id);
            $user = $this->component->user->getSessionUser();
            if (!$message || $message->status != Messages::STATUS_DEFAULT){
                $result->success = false;
                $result->message = "Failed to delete message";
            }else{
                $messageModel->deleteMessageById($id);
                $result->success = true;
                $result->message = "Deleted message successfully";
            }
        }catch(Exception $e){
            $result->success = false;
            $result->message = "An occur occurred while deleteing message" . $id ;
        }
        $this->component->helper->outputJSON($result->success, $result->message);
    }

}