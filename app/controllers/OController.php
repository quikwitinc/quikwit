<?php

use Phalcon\Tag as Tag,
    Phalcon\Http\Response as Response,
    Phalcon\Session\Bag as Bag,
    Component\Youtube;

require_once "Google/Client.php";
require_once "Google/Service/YouTube.php";

class OController extends ControllerBase
{
	private $_session;

	public function initialize(){
		$this->_session = new Bag(get_class());
	}
	
	public function indexAction(){
		Tag::setTitle('OAuth Token Management Page');
		if(!$this->component->user->hasSession()){
			return $this->response->redirect("index");
		}
		$sessionUser = $this->getSessionUser();
		
		$userInfo = User::findUserById($sessionUser->id);
		if($userInfo->usr_level !== user::ROLE_ADMIN){
			exit("You do not have permission to access this page.");
		}
		
		$yt = new Youtube();
		$accessToken = $yt->getAccessToken();
		$refreshToken = $yt->getRefreshToken();
		
		$valid = $yt->checkAccessToken();
		
		$this->view->setVar("tokenExpired", !$valid);
		if(!$valid){
			$this->view->setVar("callback", urlencode("http:".$this->url->getStatic("o")));
		}
		
		if($accessToken){
			$accessToken = json_decode($accessToken);
			$this->view->setVar("accessToken", $accessToken->access_token);
			$this->view->setVar("tokenType", $accessToken->token_type);
			$this->view->setVar("expires", date(DATE_RFC2822, $accessToken->created+$accessToken->expires_in));
			$this->view->setVar("refreshToken", $refreshToken);
			$this->view->setVar("created", date(DATE_RFC2822, $accessToken->created));
		}
		$this->view->setVar("userInfo", $userInfo);
	}
	
	public function ytAuthAction(){
		
		$yt = new Youtube();
		$authUrl = $yt->getAuthUrl();
		if($callback=$this->request->get("callback")){
			$this->_session->callback = $callback;
		}
		
		return $this->response->redirect($authUrl, true, 301);
		
	}
	
	public function ytAuthUrlAction(){
		$yt = new Youtube();
		
	}
	
	public function ytOAuthCallbackAction(){
		$yt = new Youtube();
		$callback = $this->_session->callback;
		unset($this->_session->callback);
		
		$error = $this->request->get("error", null, false);
		
		if($error){
			if($callback)
				return $this->response->redirect($callback);
			else
				$this->flash->error("Access denied.");
		}else{
			$code= $this->request->get("code");
			if($yt->setAccessTokenFromAuthCode($code)){
				$this->flash->success("Obtained token successfully.");
			}else{
				$this->flash->error("Failed to obtain token.");
			}
			//print "Got token successfully";
			$callback = $this->_session->callback;
			
			//print $callback;
			if(isset($callback)){
				return $this->response->redirect($callback);
			}
		}
		return $this->response->redirect($this->url->getStatic("o"));
	}
	
	public function ytRefreshTokenAction(){
		$yt = new Youtube();
		$token = $yt->getRefreshToken();

		if($token){
			if($yt->refreshAccessToken($token)){
				$this->flash->success("Token renewed successfully.");
			}else{
				$this->flash->error("Failed to renew access token.");
			}
		}else{
			$this->flash->error("Refresh Token Not Found!");	
		}
		return $this->response->redirect($this->url->getStatic("o"));
	}
	
	public function ytRevokeTokenAction(){
		$yt = new Youtube();
		$callback = $this->request->get("callback");
		try{
			if($yt->revokeToken()){
				$this->flash->success("Token has been removed.");
				if($callback)
					return $this->response->redirect($callback);
			}else{
				$this->flash->error("Failed to remove token or token is invalid.");
			}
		}catch(Exception $e){
			$this->flash->error("Error: {$e->getCode()} - {$e->getMessage()}");
		}
		return $this->response->redirect($this->url->getStatic("o"));
	}

	public function oauthAction(){
		$api = $this->getDI()->get('api');
		$OAUTH2_CLIENT_ID = $api->youtube->clientId;
		$OAUTH2_CLIENT_SECRET = $api->youtube->clientSecret;
		$REDIRECT = $api->youtube->RedirectUri;;
		$APPNAME = "QuikWit";
		
		 
		 
		$client = new Google_Client();
		$client->setClientId($OAUTH2_CLIENT_ID);
		$client->setClientSecret($OAUTH2_CLIENT_SECRET);
		$client->setScopes('https://www.googleapis.com/auth/youtube');
		$client->setRedirectUri($REDIRECT);
		$client->setApplicationName($APPNAME);
		$client->setAccessType('offline');
		 
		 
		// Define an object that will be used to make all API requests.
		$youtube = new Google_Service_YouTube($client);
		//print_r($youtube);
		if (isset($_GET['code'])) {
		    $client->authenticate($_GET['code']);
		    $_SESSION['token'] = $client->getAccessToken();
		 
		}
		 
		if (isset($_SESSION['token'])) {
		    $client->setAccessToken($_SESSION['token']);
		    echo '<code>' . $_SESSION['token'] . '</code>';
		}
		 
		// Check to ensure that the access token was successfully acquired.
		if ($client->getAccessToken()) {
		    try {
		        // Call the channels.list method to retrieve information about the
		        // currently authenticated user's channel.
		        $channelsResponse = $youtube->channels->listChannels('contentDetails', array(
		            'mine' => 'true',
		        ));
		 
		        $htmlBody = '';
		        foreach ($channelsResponse['items'] as $channel) {
		            // Extract the unique playlist ID that identifies the list of videos
		            // uploaded to the channel, and then call the playlistItems.list method
		            // to retrieve that list.
		            $uploadsListId = $channel['contentDetails']['relatedPlaylists']['uploads'];
		 
		            $playlistItemsResponse = $youtube->playlistItems->listPlaylistItems('snippet', array(
		                'playlistId' => $uploadsListId,
		                'maxResults' => 50
		            ));
		 
		            $htmlBody .= "<h3>Videos in list $uploadsListId</h3><ul>";
		            foreach ($playlistItemsResponse['items'] as $playlistItem) {
		                $htmlBody .= sprintf('<li>%s (%s)</li>', $playlistItem['snippet']['title'],
		                    $playlistItem['snippet']['resourceId']['videoId']);
		            }
		            $htmlBody .= '</ul>';
		        }
		    } catch (Google_ServiceException $e) {
		        $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
		            htmlspecialchars($e->getMessage()));
		    } catch (Google_Exception $e) {
		        $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
		            htmlspecialchars($e->getMessage()));
		    }
		 
		    $_SESSION['token'] = $client->getAccessToken();
		} else {
		    $state = mt_rand();
		    $client->setState($state);
		    $_SESSION['state'] = $state;
		 
		    $authUrl = $client->createAuthUrl();
		    $htmlBody = '<h3>Authorization Required</h3>
		  <p>You need to <a href="'.$authUrl.'">authorise access</a> before proceeding.<p>';
			echo $htmlBody;
		}
	}
	
}