<?php

use \Phalcon\Tag,
    \Phalcon\Mvc\Url,
    \Component\Uploader as Uploader,
    Component\Json,
    Component\User as SessionUser;



class OverviewController extends ControllerBase
{

	private $_sessionUser;

	public function onConstruct()
	{
		parent::initialize();
		$user = $this->getSessionUser();
		if(!$user){
			$this->response->redirect($this->url->get("signin"));
		}

		$userData = User::findUserById($user->id);
        if (!$userData->lat || !$userData->lng){
            $userData->lat = 9999;
            $userData->lng = 9999;
        }
		$this->view->userInfo =  $userData;

		$this->_sessionUser = $user;
	}

    public function indexAction()
    {
        Tag::setTitle('Overview');
        $skills = array();
        for($i = 0; $i < count($this->view->userInfo->getUserTags()); $i++){
            $skills[] = $this->view->userInfo->getUserTags()[$i]->getTagsStandard()->name;
        }
        if (count($skills) < 1){
            $skills[] = 'No skills selected';
        }
        foreach ($skills as $key => $value) {
            $this->view->userSkillTags .= '<a href="javascript:void(0)" class="label label-info">' . $value . '</a> ';
        }
        $this->view->userTagsComma = ($skills[0] == 'No skills selected')?[]:$skills;

        $languages = array();
        for($i = 0; $i < count($this->view->userInfo->getUserLanguages()); $i++){
            $languages[] = $this->view->userInfo->getUserLanguages()[$i]->getLanguages()->name;
        } 
        if (count($languages) < 1){
            $languages[] = 'No languages selected';
        }
        $this->view->userLangs = implode(", ", $languages);
        $this->view->userLangsComma = ($languages[0] == 'No languages selected')?[]:$languages;

        $exp = $this->view->userInfo->getUsersExperience(array('order' => 'end_year DESC'));
        $exp = $exp->toArray();
        $presentJobs = array();
        foreach ($exp as $key => $value) {
            if ($value['end_year'] == 9999){
                $presentJobs[] = $value;
                unset($exp[$key]);
            }
        }
        $exp = array_merge($presentJobs, $exp);
        $this->view->userExperiences = $exp;
        $reviews = $this->view->userInfo->getReviews(array('order' => 'rvs_created DESC'));
        $this->view->reviews = $reviews;
        $this->view->viewHelper = $this->component->helper;
        $reviewModel = new Reviews();
        $this->view->stats = $reviewModel->getStats($this->view->userInfo->usr_id)->toArray();
        $contractModel = new Contracts();
        $this->view->contractCompleted = $contractModel->getNumCompletedContracts($this->view->userInfo->usr_id)->toArray();
        $userConnectionModel = new UsersConnections();
        $facebook = $userConnectionModel->retrieveConnect($this->view->userInfo->usr_id, 'facebook');
        $linkedin = $userConnectionModel->retrieveConnect($this->view->userInfo->usr_id, 'linkedin');
        $socialMedia = array();
        $this->view->connections = "No Social Media";
        if ($facebook){
            $socialMedia[] = "<a href='$facebook->usr_profile' target='__blank'>facebook</a> ";
        }
        if ($linkedin){
            $socialMedia[] = "<a href='$linkedin->usr_profile' target='__blank'>linkedin</a>";
        }
        if (count($socialMedia) > 0){
            $this->view->connections = implode(' ', $socialMedia);
        }
		$video = new Video();
        $this->view->videos = $video->getUserVideos($this->view->userInfo->usr_id)->toArray();
		$video = new UserTags();
        $this->view->userTags = $video->getUserTags($this->view->userInfo->usr_id);
        $media = new Media();
        $this->view->medias = $media->getUserMedia($this->view->userInfo->usr_id)->toArray();

        $user = $this->getSessionUser();
        $userModel = new User();
        $currentUser = $userModel->findUserById($user->id);
        $this->view->setVar('userCity', $currentUser->usr_city);
    }

    public function setprofileimageAction(){

        try{
            if (!isset($_FILES['image']['error']) || is_array($_FILES['image']['error'])) {
                throw new RuntimeException('Invalid parameters.');
            }

            // Check $_FILES['upfile']['error'] value.
            switch ($_FILES['image']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            // You should also check filesize here.
            /*if ($_FILES['upfile']['size'] > 1000000) {
            throw new RuntimeException('Exceeded filesize limit.');
            } */

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            if (false === $ext = array_search(
                    $finfo->file($_FILES['image']['tmp_name']),
                    array(
                            'jpg' => 'image/jpeg',
                            //'png' => 'image/png',
                            //'gif' => 'image/gif',
                    ),
                    true
            )) {
                throw new RuntimeException('Invalid file format.');
            }
           
            $uploads = $this->request->getUploadedFiles();
            if($this->request->getPost("uplloadAvatar")==2) {
            	foreach($uploads as $upload){
					if(trim($upload->getname()) != "") {
					   	#define a “unique” name and a path to where our file must go
					   	$fileName = $upload->getname();
					   	$actual_image_name = md5(uniqid(rand(), true)).'-'.strtolower($upload->getname());
					   	$filePath = 'public/uploads/avatar/'.$actual_image_name;
					   	$upload->moveTo($filePath);
					}
            	}
            }
            else {          	
	            $crop = $this->request->get("crop");
				$actual_image_name = $this->saveCroppedImages($crop["x"], $crop["y"], $crop["w"], $crop["h"], $uploads);				
        	} 
        	if(User::setUserAvatar($this->_sessionUser->id, $actual_image_name)){
                $this->flash->success('<div data-toggle="notify" data-onload data-message="Your avatar is updated successfully." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
			}else{
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to update user avatar. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
			}       	
        }catch(Exception $e){
            //$this->flash->error($e->getMessage());
        }
        $this->response->redirect($this->url->get("overview"));
    }
    
    public function saveCroppedImages($x, $y, $w, $h, $uploads){
		$data = array();
		foreach($uploads as $upload){
			if(trim($upload->getname()) != "") {
				$actual_image_name = md5(uniqid(rand(), true)).'-'.strtolower($upload->getname());					   	
				$filename = $upload->getTempName();	
				$image_info = getimagesize($filename);
				if($upload->getRealType()=='image/png'){
					$image = imagecreatefrompng($filename);
				}
				else if($upload->getRealType()=='image/gif'){
					$image = imagecreatefromgif($filename);
				}
				else{
					$image = imagecreatefromjpeg($filename);
				}
				$targ_w = $targ_h = 150;
				$jpeg_quality = 90;
			
				$src = $filename;
				$img_r = imagecreatefromjpeg($src);
				$dst_r = imagecreatetruecolor( $targ_w, $targ_h );
//				$resized_width = ((int)$x2) - ((int)$x1);
//				$resized_height = ((int)$y2) - ((int)$y1);
			
				imagecopyresampled($dst_r, $img_r,0,0, $x, $y,
				$targ_w, $targ_h, $w, $h);
				$new_file_name = 'public/uploads/avatar/'.$actual_image_name;
				imagejpeg($dst_r,$new_file_name,$jpeg_quality);
			}
		}
		return $actual_image_name;
	}

    /* public function setcoverimageAction(){

        try{
            if (!isset($_FILES['image']['error']) || is_array($_FILES['image']['error'])) {
                throw new RuntimeException('Invalid parameters.');
            }

            // Check $_FILES['upfile']['error'] value.
            switch ($_FILES['image']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            // You should also check filesize here.
            // if ($_FILES['upfile']['size'] > 1000000) {
            // throw new RuntimeException('Exceeded filesize limit.');
            // }

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            if (false === $ext = array_search(
                    $finfo->file($_FILES['image']['tmp_name']),
                    array(
                            'jpg' => 'image/jpeg',
                            'png' => 'image/png',
                            'gif' => 'image/gif',
                    ),
                    true
            )) {
                throw new RuntimeException('Invalid file format.');
            }

            $filePath = "/{$this->_sessionUser->id}/avatar/avatar.jpg";
            $imgSrc = $_FILES["image"]["tmp_name"];
            $uploader = new Uploader();
            $imgDest = $uploader->getUploadPath($filePath);

            $imgR = '';
            if($ext==="png"){
                $imgR = imagecreatefrompng($imgSrc);
            }else if($ext==="jpg"){
                $imgR = imagecreatefromjpeg($imgSrc);
            }else{
                $imgR = imagecreatefrompng($imgSrc);
            }
            $width = imagesy($imgR);
            $height = imagesx($imgR);
            $destR = imagecreatetruecolor($width, $height);


            $crop = $this->request->get("crop");
            if($crop["x"] && $crop["y"] && $crop["w"] && $crop["h"]){

                $uploader->createFolder(dirname($imgDest));

                $widthT = $crop["w"];
                $heightT = $crop["h"];
                $xT = $crop["x"];
                $yT = $crop["y"];

                imagecopyresampled($destR,$imgR,0,0,$xT,$yT, $width,$height,$widthT,$heightT);
                imagejpeg($destR, $imgDest, 100);
            }else{
                imagecopyresampled($destR,$imgR,0,0,0,0, $width,$height,$width,$height);
                imagejpeg($destR, $imgDest, 100);
            }

            if(User::setUserAvatar($this->_sessionUser->id, $uploader->getFileUrl($filePath))){
                $this->flash->success("User avatar was updated successfully");
            }else{
                $this->flash->success("User avatar was not updated.");
            }
        }catch(Exception $e){
            var_dump($e);
            exit;
            $this->flash->error($e->getMessage());
        }
        $this->response->redirect($this->url->get("overview"));
    } */

}
