<?php

use \Phalcon\Tag,
    Phalcon\Mvc\Url,
    Component\Json;

class PostController extends ControllerBase
{
    public function onConstruct()
    {
        parent::initialize();
    }

    public function indexAction()
    {
    	Tag::setTitle('Find a Freelancer');
        if($this->component->user->hasSession()){
            $user = $this->getSessionUser();
            $this->view->setVar('user', $user);
        }
    	 
    }

    public function doRequestAction()
    {
        if($this->request->isPost()) {

            $json  = new Json();
            $tagId = $this->request->getPost('tagId');
            $title = $this->request->getPost('title');
            $task = $this->request->getPost('task');
            $fixed = $this->request->getPost('fixed');
            $price = $this->request->getPost('price');
            $hour = $this->request->getPost('hour');
            $quality = $this->request->getPost('quality');
            $multiple = $this->request->getPost('multiple');
            $datetime = $this->request->getPost('datetime');
            $postalCode = $this->request->getPost('postal');
            $postal = preg_replace('/\s+/', '', $postalCode);
            $cityId = $this->request->getPost('cityId');
            $travel = $this->request->getPost('travel');
            $email = $this->request->getPost('email');
            $firstname = ucfirst($this->request->getPost('firstname'));
            $lastname = ucfirst($this->request->getPost('lastname'));
            $phoneNumber = $this->request->getPost('phone');
            $phone = preg_replace('/\s+/', '', $phoneNumber);


            if ($this->security->checkToken()) {

                if(!isset($title) || !isset($task) || !isset($fixed) || !isset($price) || !isset($hour) || !isset($postal)) {
                    $json->setJSON(false, 'Please refresh this page and fill in all the fields as we need all the details to give you the best match.', -1);
                    //$this->flash->warning('<div data-toggle="notify" data-onload data-message="Please fill in all the fields as we need all the details to give you best match." data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
                    //$this->response->redirect($this->url->get("request"));  
                }else{
                    $user = $this->getSessionUser();
                    if(!$user){
                        $userModel = new User();
                        $exist = $userModel->findUserByEmail($email);
                        if(!$exist){
                            $password = "";
                            $send = $userModel->addUser($email, $password, $firstname, $lastname);
                            if($send){
                                $newUser = $userModel->findUserByEmail($email);
                                $requestModel = new Requests();
                                $success = $requestModel->saveRequest($newUser->usr_id, $tagId, $title, $task, $fixed, $price, $hour, $quality, $multiple, $datetime, $postal, $cityId, $travel, $phone); 
                                if($success){
                                    $json->setJSON(true, 'Thanks for using QuikWit! A confirmation link has been sent to your email. Your request will be sent to available freelancers once you confirm your email.', 1);
                                    //$this->flash->success('<div data-toggle="notify" data-onload data-message="Thanks for using QuikWit! A confirmation link has been sent to your email. Your request will be sent to available freelancers once you confirm your email." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                                    $user = User::findUserByEmail($email);
                                    $this->component->user->createUserSession($user);
                                    //$this->response->redirect($this->url->get(""));
                                }else{
                                    $json->setJSON(false, 'Sorry, please try again.', -1);
                                    //$this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry! Please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                                    //$this->response->redirect($this->url->get("request"));
                                }
                            }else{
                                $json->setJSON(false, 'Sorry, some error occured. Please try again later.', -1);
                                //$this->flash->warning('<div data-toggle="notify" data-onload data-message="Sorry! Some error occured. Please try again later." data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
                                //$this->response->redirect($this->url->get("request"));
                            }
                        }else{
                            $json->setJSON(false, 'This email is already registered. Please login first to continue.', -1);
                        }
                        
                    }else{
                        $userModel = new User();
                        $currentUser = $userModel->findUserById($user->id);
                        $requestModel = new Requests();
                        $success = $requestModel->saveRequest($currentUser->usr_id, $tagId, $title, $task, $fixed, $price, $hour, $quality, $multiple, $datetime, $postal, $cityId, $travel, $phone);
                        if($success){
                            $citySkill = array($cityId, $tagId);
                            $json->setJSON(true, 'Thanks for using QuikWit! Your request will be sent to all qualified freelancers within your area.', 1, $citySkill);
                            /* $request = $requestModel->getUserLatestPendingRequest($currentUser->usr_id);
                            if($request){
                                ignore_user_abort(true);
                                $user = new User();
                                $alluserids= $user->getAllSkillCityUserIds($cityId, $tagId);
                                $targetId = (array_column($alluserids, 'usr_id'));
                                //error_log("<pre>targetId".print_r($targetId,true)."</pre>"); 
                                foreach($targetId as $target) {
                                    if($target == $currentUser->usr_id){
                                        continue;
                                    }
                                    $lead = new RequestsLead();
                                    $lead->addRequest($request->req_id, $request->req_userid, $target);

                                }
                                $requestModel->setRequestSent($request->req_id);
                            } */
                            
                            //$this->flash->success('<div data-toggle="notify" data-onload data-message="Thanks for using our service! Your request will be send to all interested freelancers within your area." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                            //$this->response->redirect($this->url->get("request"));
                        }else{
                            $json->setJSON(false, 'Sorry, some error occured. Please try again later.', -1);
                            //$this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry! Please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                            //$this->response->redirect($this->url->get("request"));
                        }
                    }
                }  
            }else{
                $json->setJSON(false, 'Sorry, please try again.', -1);
                //$this->flash->warning('<div data-toggle="notify" data-onload data-message="Invalid CSRF token." data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
                //$this->response->redirect($this->url->get("request"));
            } 

        }else{
            $json->setJSON(false, 'Sorry, some error occured. Please try again later.', -1);
            //$this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry! Please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');           
            //$this->response->redirect($this->url->get("request"));
        }
        $json->output();
    }

    public function sendRequestAction()
    {
        ignore_user_abort(true);
        if($this->request->isPost()){

            $cityId = $this->request->getPost('cityId');
            $tagId = $this->request->getPost('tagId');

            $user = $this->getSessionUser();
            $userModel = new User();
            $currentUser = $userModel->findUserById($user->id);
            $requestModel = new Requests();
            $request = $requestModel->getUserLatestPendingRequest($currentUser->usr_id);
            if($request){
                $user = new User();
                if(!empty($cityId) && !empty($tagId)){
                    $alluserids= $user->getAllSkillCityUserIds($cityId, $tagId);
                    $targetId = (array_column($alluserids, 'usr_id'));
                    //error_log("<pre>targetId".print_r($targetId,true)."</pre>"); 
                    foreach($targetId as $target) {
                        if($target == $currentUser->usr_id){
                            continue;
                        }
                        $lead = new RequestsLead();
                        $lead->addRequest($request->req_id, $request->req_userid, $target);

                    } 
                }else{
                    $alluserids= $user->getAllSkillCityUserIds($request->req_city, $request->req_tagid);
                    $targetId = (array_column($alluserids, 'usr_id'));
                    //error_log("<pre>targetId".print_r($targetId,true)."</pre>"); 
                    foreach($targetId as $target) {
                        if($target == $currentUser->usr_id){
                            continue;
                        }
                        $lead = new RequestsLead();
                        $lead->addRequest($request->req_id, $request->req_userid, $target);

                    }
                }
                
                $requestModel->setRequestSent($request->req_id);
            }else{
                $requestAdmin = $requestModel->getUserLatestPendingRequest(0);
                if($requestAdmin){
                    $user = new User();
                    if(!empty($cityId) && !empty($tagId)){
                        $alluserids= $user->getAllSkillCityUserIds($cityId, $tagId);
                        $targetId = (array_column($alluserids, 'usr_id'));
                        //error_log("<pre>targetId".print_r($targetId,true)."</pre>"); 
                        foreach($targetId as $target) {
                            if($target == $currentUser->usr_id){
                                continue;
                            }
                            $lead = new RequestsLead();
                            $lead->addRequest($requestAdmin->req_id, $requestAdmin->req_userid, $target);

                        } 
                    }else{
                        $alluserids= $user->getAllSkillCityUserIds($requestAdmin->req_city, $requestAdmin->req_tagid);
                        $targetId = (array_column($alluserids, 'usr_id'));
                        //error_log("<pre>targetId".print_r($targetId,true)."</pre>"); 
                        foreach($targetId as $target) {
                            if($target == $currentUser->usr_id){
                                continue;
                            }
                            $lead = new RequestsLead();
                            $lead->addRequest($requestAdmin->req_id, $requestAdmin->req_userid, $target);

                        }
                    }
                    
                    $requestModel->setRequestSent($requestAdmin->req_id);
                }  
            }  
            
        }
        
    }

    public function postingListAction(){
        Tag::setTitle('My Postings');
        $this->initializeRequestListStats();
    }

    protected function initializeRequestListStats()
    {
        $requests = new Requests();
        $userId = $this->user->id;
        $userModel = new User();
        $user = $userModel->findUserById($userId);
        $page = $this->request->get("page", "int", 0);
        if($page) {
            $page = $page;
        }
        else {
            $page = 1;
        }
        $offset = ($page-1)*Requests::REQUEST_PER_PAGE;
        $totalRequestCount = Requests::getUserRequestCount($userId);
        $requestCountPerPage = (($page*Requests::REQUEST_PER_PAGE) >= $totalRequestCount)?$totalRequestCount:($page*Requests::REQUEST_PER_PAGE); 
        if($page==1) {
            $prevPage = 1;
            if ($totalRequestCount<=Requests::REQUEST_PER_PAGE) {
                $nextPage = 1;
            }
            else {
                $nextPage = $page + 1;
            }
        }
        else if (($page*Requests::REQUEST_PER_PAGE) >= $totalRequestCount) {
            $prevPage = $page-1;
            $nextPage = $page;          
        }
        else {
            $prevPage = $page-1;
            $nextPage = $page+1;            
        }
        $this->view->setVar('totalRequestCount', Requests::getUserRequestCount($userId)); 
        $this->view->setVar('requests', $requests->getUserPosts($userId, $offset, Requests::REQUEST_PER_PAGE));
        $this->view->setVar('requestOffset', ($totalRequestCount==0)?0:(($page-1)*(Requests::REQUEST_PER_PAGE))+1);
        $this->view->setVar('requestCountPerPage', $requestCountPerPage);
        $this->view->setVar('prevPage', $prevPage);
        $this->view->setVar('nextPage', $nextPage);
        $this->view->setVar('userId', $this->user->id); 
    
        $userComponent = new \Component\User();
        $user_tz = $userComponent->getSessionUserTimeZone();
        if ($user_tz) {
            $this->view->setVar('timezone', $user_tz);
        }
    }

    public function viewPostingAction($id)
    {
        if (!$id) {
            return $this->response->redirect($this->url->get('request'));
        }
        Tag::setTitle('View Posting');

        $loginUser = $this->component->user->getSessionUser();
        $userId = $loginUser->id;

        $requests = Requests::getUserPostsFromRequestIds($userId, $id);
        //error_log("<pre>requests".print_r($requests,true)."</pre>"); 

        if ($requests) {
            $userComponent = new \Component\User();
            $user_tz = $userComponent->getSessionUserTimeZone();
            if ($user_tz) {
                $this->view->setVar('timezone', $user_tz);
            }

            $this->view->setVar('requests', $requests);
            $request = $requests[sizeof($requests)-1];
            $this->view->setVar('requestTag', $request['tag']);
            $this->view->setVar('requestTitle', $request['req_title']);

        }
        
    }

    public function cancelPostAction()
    {
        $this->view->disable();

        if($this->request->isPost()){
            $reqId = $this->request->getPost('reqId');

            $requestModel = new Requests();
            $success = $requestModel->setRequestCancel($reqId);

            if($success){
                $this->flash->success('<div data-toggle="notify" data-onload data-message="This post is now taken off." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                $this->response->redirect($this->url->get("request/postingList"));
            }else{
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry! Please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                $this->response->redirect($this->url->get("request/postingList"));
            } 
        }
    }

}