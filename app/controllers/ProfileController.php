<?php

use \Phalcon\Tag;

class ProfileController extends ControllerBase
{
    public function onConstruct()
    {
    	parent::initialize();
    	$user = $this->component->user->getSessionUser();
    	if(!$user){
    		$this->response->redirect($this->url->get("signin"));
    		exit;
    	}
    }

    public function indexAction($id)
    {
    	Tag::setTitle('User Profile');
    	 
    }

}