<?php


use \Phalcon\Tag,
    Component\Json;

class RegisterController extends ControllerBase
{
	protected $isAjax;
	
	public function initialize()
	{
		/*if($this->component->user->hasSession()){
			$this->flash->error("You are already logged in.");
			$this->response->redirect($this->url->get("/"));
		}*/
		parent::initialize();
		
		$this->isAjax =  $this->request->isAjax();
	}

	public function indexAction()
	{
		if($this->component->user->hasSession()){
			$this->response->redirect($this->url->get("overview"));
		}
	}

	/* public function checkAction($type){
		$json = new Json();

		$bool = $this->request->get("boolean");
		$bool = ($bool==='true')? true : false;
		
		switch($type){
			case "username":
				$uname = $this->request->get("username", "string");
				if($uname){
					$exists = User::userNameExists($uname);
					if($exists===false){
						$json->setJSON(true, "Username is valid", 1);
					}else{
						$json->setJSON(false, "Username already exists", 0);
					}
				}else{
					$json->setJSON(false, "Invalid parameter", -1);
				}
				break;
			case "email":
				$email = $this->request->get("email", "email");
				if($email){
					$exists = User::userEmailExists($email);
					if($exists===false){
						$json->setJSON(true, "Email is valid", 1);
					}else{
						$json->setJSON(false, "Email already exists", 0);
					}
				}else{
					$json->setJSON(false, "Invalid parameter", -1);
				}
				break;
			default:
				$json->setJSON(false, "Invalid search type '$type'", -1);
				break;
		}
		if($bool){
			$json->outputBoolean();
		}else{
			$json->output();
		}
		
	}
	
	public function getFormAction(){
		 
	} */

	public function doRegisterAction()
	{
		if($this->request->isPost()){

			$this->view->disable();
			$failed = false;
			 
			$firstname = $this->request->getPost("firstname");
			$lastname = $this->request->getPost("lastname");
			$fname = ucfirst($firstname);
			$lname = ucfirst($lastname);
			$email = $this->request->getPost("email");
			$pwd = $this->request->getPost("password");
			$rPwd  = $this->request->getPost("rpassword");
			//$tokenKey = $this->request->getPost("tokenKey");
			//$token = $this->request->getPost("token");

			if ($this->security->checkToken()) {

				if(empty($fname) || empty($lname)){
					$this->flash->warning('<div data-toggle="notify" data-onload data-message="User first and last name should not be empty!" data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
					$failed = true;
				}
				if(empty($email)){
					$this->flash->warning('<div data-toggle="notify" data-onload data-message="User email is required!" data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
					$failed = true;
				}
				
				if(empty($pwd) || empty($rPwd)){
					$this->flash->warning('<div data-toggle="notify" data-onload data-message="Password cannot be empty!" data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
					$failed = true;
				}else if($pwd !== $rPwd){
					$this->flash->warning('<div data-toggle="notify" data-onload data-message="Password does not match!" data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
					$failed = true;
				}
				 
				if(!$failed){
					$user = new User();
					if(User::userExists($email)){
						/* $check = $user->checkAccountMerge($email);
						if($check === true){
							$send = $user->mergeUser($email, $pwd, $fname, $lname);
							if($send){
								//$this->flash->success('<div data-toggle="notify" data-onload data-message="Thanks for signing up! A confirmation email has been sent to your email." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
								$user = User::findUserByEmail($email);
								$this->component->user->createUserSession($user);
								$this->response->redirect($this->url->get("register/confirmYourEmail"));
							}
							else{
								$this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry! Please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
								$this->response->redirect($this->url->get(""));
							}
						}else{ */
							$this->flash->warning('<div data-toggle="notify" data-onload data-message="User already exists!" data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
							$this->response->redirect($this->url->get(""));	
						//} 		
					}else{
						$send = $user->addUser($email, $pwd, $fname, $lname);
						if($send){
							//$this->flash->success('<div data-toggle="notify" data-onload data-message="Thanks for signing up! A confirmation email has been sent to your email." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
							$user = User::findUserByEmail($email);
							$this->component->user->createUserSession($user);
							$this->response->redirect($this->url->get("register/confirmYourEmail"));
						}
						else{
							$this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry! Please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
							$this->response->redirect($this->url->get(""));
						}
					}
				}else{
					$this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to register user. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
					$this->response->redirect($this->url->get(""));
				}
				
			}else{
				$this->flash->warning('<div data-toggle="notify" data-onload data-message="Sorry! Please try again." data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
				$this->response->redirect($this->url->get(""));
			} 
			
		}else{
			$this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry! Please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');			
			$this->response->redirect($this->url->get(""));
		} 

		//$this->response->redirect($this->url->get(""));
	}

	public function introAction()
	{
		Tag::setTitle('Introduction');
		if($this->component->user->hasSession()){
			$user = $this->component->user->getSessionUser();
			$userModel = new User();
			$userInfo = $userModel->findUserById($user->id);
			$this->view->setVar("firstname", $userInfo->usr_firstname);
			$this->view->setVar("lastname", $userInfo->usr_lastname);
			$this->view->setVar("email", $userInfo->usr_email);

			//if($userInfo->usr_email_verified == 1){
				//$this->response->redirect($this->url->get("overview"));
			//}
		}
	}

	public function confirmEmailAction()
    {
        $code = $this->request->getQuery('code');
        $email = $this->request->getQuery('email');
        $confirmation = EmailConfirmations::findFirstByCode($code);
        
        if (!$confirmation) {
            return $this->dispatcher->forward(array(
                    'controller' => 'index',
                    'action' => 'index'
            ));
        }
        if ($confirmation->confirmed <> 0 ) {
            $this->flash->success('<div data-toggle="notify" data-onload data-message="This email is already confirmed." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
            return $this->dispatcher->forward(array(
                    'controller' => 'signin',
                    'action' => 'index'
            ));
        }
        //confirmation
        $user = new User();
        $user->afterConfirmation($email);
        /**
             * Change the confirmation to 'confirmed' and update the user to 'active'
        */
        $confirmation->confirmed = 1;
        if ($confirmation->save()) {
        	$currentUser = $user->findUserByEmail($email);
        	if(empty($currentUser->usr_password)){
	        	/* $requestModel = new Requests();
	        	$request = $requestModel->getUserLatestPendingRequest($currentUser->usr_id);

	        	if($request){
	        		ignore_user_abort(true);
		        	$user = new User();
	                $alluserids= $user->getAllSkillCityUserIds($request->req_city, $request->req_tagid);
		        	$targetId = (array_column($alluserids, 'usr_id'));
		        	foreach($targetId as $target) {
		                if($target == $currentUser->usr_id){
		                    continue;
		                }
		                $lead = new RequestsLead();
		                $lead->addRequest($request->req_id, $request->req_userid, $target);

		            }
		            $requestModel->setRequestSent($request->req_id);
	        	} */

	        	$this->component->user->createUserSession($currentUser);

	        	$this->flash->success('<div data-toggle="notify" data-onload data-message="Thanks, your email is successfully confirmed. Please set your password below." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
	        	return $this->dispatcher->forward(array(
	        	        'controller' => 'register',
	        	        'action' => 'setPassword'
	        	));	
        	}else{
        		$this->component->user->createUserSession($currentUser);
        		$this->flash->success('<div data-toggle="notify" data-onload data-message="Thanks, your email is successfully confirmed." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
        		return $this->dispatcher->forward(array(
        		        'controller' => 'register',
        		        'action' => 'intro'
        		));	
        	}     	
            
        }
        else{
            foreach ($confirmation->getMessages() as $message) {
                    $this->flash->error($message);
            }
        }
    } 

    public function tourAction()
    {
        Tag::setTitle('Tour');
    }

    public function confirmYourEmailAction()
    {
        Tag::setTitle('Please Confirm Your Email');
        if($this->component->user->hasSession()){
        	$user = $this->component->user->getSessionUser();
        	$userModel = new User();
        	$userInfo = $userModel->findUserById($user->id);
        	$this->view->setVar("email", $userInfo->usr_email);

        	if($userInfo->usr_email_verified == 1){
        		$this->response->redirect($this->url->get("overview"));
        	}
        }else{
        	$this->response->redirect($this->url->get(""));
        }
    }

    public function setPasswordAction()
    {
    	Tag::setTitle('Set Your Password');
    	$user = $this->getSessionUser();
		if(!$user){
			$this->response->redirect($this->url->get("signin"));
		}
    }

    public function insertPasswordAction()
    {	
        if ($this->request->isPost()) {
            $json  = new Json();
            $password = $this->request->getPost('password');
            $cPassword = $this->request->getPost('cPassword');
            if ($password && $password === $cPassword) {
            	$user = $this->getSessionUser();
            	$userModel = new User();
            	$currentUser = $userModel->findUserById($user->id);
                if($currentUser) {
                	if(empty($currentUser->usr_password)){
                		if ($userModel->changePassword($user->id, $password)) {
                		    $json->setJSON(true, 'Your password is successfully set. Once again thank you for signing up on QuikWit and your request is now sent to all available freelancers around your area.', 1);
                		    //$this->flash->success('<div data-toggle="notify" data-onload data-message="You password is succesfully set." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                		    //$this->response->redirect($this->url->get('request'));
                		} else {
                		    $json->setJSON(false, 'Failed to set user password. Please try again later.', -1);
                		    //$this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to set user password." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                		    //$this->response->redirect($this->url->get('register/setPassword'));
                		}
                	}
                } else {
                    $json->setJSON(false, 'This user is not found in our database.', -1);
                    //$this->flash->error('<div data-toggle="notify" data-onload data-message="User is not found." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                    //$this->response->redirect($this->url->get('register/setPassword'));
                }
            } else {
                $json->setJSON(false, 'Your passwords do not match. Please check your passwords.', -1);
                //$this->flash->error('<div data-toggle="notify" data-onload data-message="Passwords do not match. Please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                //$this->response->redirect($this->url->get('register/setPassword'));
            }

        }else{
        	$json->setJSON(false, 'Sorry, some error occured. Please try again later.', -1);
			//$this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry! Please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');			
			//$this->response->redirect($this->url->get('register/setPassword'));
		} 
		$json->output();
    }

}