<?php

use \Phalcon\Tag;

class ReviewController extends ControllerBase
{
    public function indexAction()
    {
        Tag::setTitle('Reviews');
        if($this->component->user->hasSession()){
        	$this->view->setVar("user", $this->user);
        }
        //parent::initialize();
    }

    public function addFacilityAction()
    {
        Tag::setTitle('Add a Facility');
    }

    public function addTeacherAction()
    {
        Tag::setTitle('Add a Teacher');
    }

    public function reviewTeacherAction()
    {
        Tag::setTitle('Review Teacher');
    }

    public function thankyouAction()
    {
        Tag::setTitle('Thank you!');
    }



}