<?php

use \Phalcon\Tag,
    \Phalcon\Filter;


class SearchController extends ControllerBase
{
	private $_user;

  public function onConstruct()
  {
      parent::initialize();
      $user = $this->component->user->getSessionUser();
      /*if(!$user){
      	$this->response->redirect($this->url->get("signin"));
      	exit;
      }*/
      if($user)
     		$this->_user = $user;
  }

  public function indexAction()
  {
  	 Tag::setTitle('Search Results');
	   $q = $this->request->get("q");
     //$q = $this->filter->sanitize($q, 'alphanum');
     if (preg_match('/^[a-z0-9 ]+$/i', $q) || preg_match('/^$/', $q)){
        $searchUserModel = new SearchUsers();
        $loginUser = $this->component->user->getSessionUser();
        $userModel = new User();

        if ($loginUser){
           $currentUser = $userModel->findUserById($loginUser->id);
           if($currentUser){
             $email_verified = $currentUser->usr_email_verified;
             $email = $currentUser->usr_email;
           }
           $gps = User::getGPS($loginUser->id);
           $this->view->setVar('loginUser', $loginUser);
           $this->view->setVar('userId', $loginUser->id);
           $this->view->setVar('email', $email);
           $this->view->setVar('email_verified', $email_verified);
           $this->view->setVar('lat', $gps['lat']);
           $this->view->setVar('lng', $gps['lng']);
        }else{
           $loginUser = new stdClass;
           $loginUser->id = '';
           $this->view->setVar('loginUser', null);
           $this->view->setVar('userId', 0);
           $this->view->setVar('email', null);
           $this->view->setVar('email_verified', 0);
           $this->view->setVar('lat', 0);
           $this->view->setVar('lng', 0);
        }

        $sort = $this->request->get("sort");
        $users = $searchUserModel->findFreelancers($q, $loginUser->id, ($loginUser->id)?true:false, 1, 10, $sort);

        $this->view->setVar("users", $users['list']);
        $this->view->setVar("total", $users['total']);
        $this->view->setVar("page", $users['page']);
        $this->view->setVar("q", $q);
        $this->view->setVar("sort", ($sort)?$sort:'relevance');
        $this->view->setVar("numPages", $users['numPages']);
     }else{
        $this->flash->error('<div data-toggle="notify" data-onload data-message="Please put in a valid skill." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
        $this->response->redirect("search");
     }
  }

  public function searchAction()
  {
      $this->view->disable();
      $q = $this->request->get("q");
      $sort = $this->request->get("sort");
      
      $minPrice = $this->request->get("minPrice");
      $maxPrice = $this->request->get("maxPrice");
      $mapRadius = $this->request->get("mapRadius");
      $mapRadiusOption = $this->request->get("mapRadiusOption");
      
      $searchUserModel = new SearchUsers();
      $loginUser = $this->component->user->getSessionUser();
      $this->view->setVar('loginUser', $loginUser);
      if (!$loginUser) {
        $loginUser = new stdClass;
        $loginUser->id = '';
      }
      $page = $this->request->get("page");
      $limit = 10;
      if (!$page){
          $page = 1;
      }

      $list = $searchUserModel->findFreelancers($q, $loginUser->id, ($loginUser->id)?true:false, $page, $limit, $sort, $minPrice, $maxPrice, $mapRadiusOption, $mapRadius);

      if ($list){
          // error_log("<pre>list".print_r($list,true)."</pre>"); 
          $list['status'] = true;
      }
      echo json_encode($list);
  }
  
  public function userskilltagsAction() {
  	$this->view->disable();
  	$userid = $this->request->get("usr_id");
      $usertagsmodel = new UserTags();
      $userTagsObj = $usertagsmodel->getUserTags($userid);
      //print_r($userTagsObj);
      $userTags = (array_column($userTagsObj, 'name', 'tag_id'));
      //error_log("<pre>userTags".print_r($userTags,true)."</pre>"); 
      echo Phalcon\Tag::selectStatic(array('contract[tag]', 
                                        $userTags, 
                                        "using" => array("tag_id", "name"),
                                        "class" => "select-chosen",
                                        "type" => "text",
                                        "id" => "usersTags",
                                        "data-role" => "tagsinput"
                                        ));
  	exit;
  }
    
}

