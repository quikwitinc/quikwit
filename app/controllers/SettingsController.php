<?php

use \Phalcon\Tag,
\Phalcon\Mvc\Url,
\Component\Uploader as Uploader,
\Phalcon\Validation,
\Phalcon\Validation\Validator\Email;

class SettingsController extends ControllerBase
{
	private $_sessionUser;

	public function onConstruct()
	{
		parent::initialize();
		$user = $this->getSessionUser();
		if(!$user){
			$this->response->redirect($this->url->get("signin"));
		}
		$userData = User::findUserById($user->id);
		$this->view->userInfo =  $userData;
		$this->_sessionUser = $user;
	}

	/**
	 * Default action, shows the search form
	 */
	public function indexAction()
	{
		Tag::setTitle('Settings');
		$this->persistent->conditions = null;
		$userConnection = new UsersConnections();	

		$socialMedia = $userConnection->hasConnection($this->_sessionUser->id);
		$this->view->profile = User::findUserById($this->_sessionUser->id);
		$this->view->hasLinkedIn = in_array('linkedin', $socialMedia);
		$this->view->hasFacebook = in_array('facebook', $socialMedia);

		$userSession = $this->getSessionUser();
		$userModel = new User();
		$user = $userModel->findUserById($userSession->id);
		$this->view->setVar("subscribeStatus", $user->usr_subscribe);
		$this->view->setVar("email", $user->usr_email);
		$this->view->setVar("forwardStatus", $user->usr_forward);
		$this->view->setVar("email_verified", $user->usr_email_verified);
	}

	/* public function changeSettingsAction()
	{
		//$this->view->form = new ProfileForm();
		$userModel = new User();
		
		//$profile = $this->request->getPost("profile");
		$firstname = $this->request->getPost("firstname");
		$lastname = $this->request->getPost("lastname");
		$email = $this->request->getPost("email");
		$lang = $this->request->getPost("language");
		$postcode = $this->request->getPost("postalcode");
		$lat = $this->request->getPost("lat");
		$lng = $this->request->getPost("lng");

		/* if($profile){
			$profile = (Object)$profile;
			$firstname = $profile->firstname;
			$lastname = $profile->lastname;
			$email = $profile->email; 

			$exists = User::userEmailExists($email);
			$user = $userModel->findUserById($this->_sessionUser->id);
			$oldEmail = $user->usr_email;
			if($exists===true && $oldEmail != $email){
				$this->flash->error('<div data-toggle="notify" data-onload data-message="Someone is using this email already." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
			} 

			//$lang = $profile->language;
			//$country = $profile->country;
			//$postcode = $profile->postalcode;
			//$lat = $profile->lat;
			//$lng = $profile->lng;
			//if (preg_match("/^\w+$", $firstname))
			$success = $userModel->updateUserInfo($this->_sessionUser->id, $firstname, $lastname, $email, $lang, $postcode, $lat, $lng);
			//error_log("<pre>oldEmail" . print_r($oldEmail,true) . "</pre>");
			//error_log("<pre>email" . print_r($email,true) . "</pre>");
			//die();
			if($success){	
				$this->flash->success('<div data-toggle="notify" data-onload data-message="Information updated!" data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
				if($oldEmail != $email){	
					$emailChange = new UsersEmailChange();
					$confirmation = $emailChange->addEmailChange($this->_sessionUser->id, $oldEmail, $email);				
					$emailChange->sendEmailChangeConfirmation($oldEmail, $email);
				}
			}else{
				$this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to update! Please edit and try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
			}

		$this->response->redirect($this->url->get("settings"));
		//}
	} */

	public function updateEmailAction()
	{
		$userModel = new User();
		
		$email = $this->request->getPost("email");

		$exists = User::userEmailExists($email);
		$user = $userModel->findUserById($this->_sessionUser->id);
		$oldEmail = $user->usr_email;
		if($exists===true && $oldEmail != $email){
			$this->flash->error('<div data-toggle="notify" data-onload data-message="Someone is using this email already." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
		} 

		$success = $userModel->changeEmail($this->_sessionUser->id, $email);

		if($success){	
			$this->flash->success('<div data-toggle="notify" data-onload data-message="Information updated!" data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
			if($oldEmail != $email){	
				$emailChange = new UsersEmailChange();
				$confirmation = $emailChange->addEmailChange($this->_sessionUser->id, $oldEmail, $email);				
				$emailChange->sendEmailChangeConfirmation($oldEmail, $email);
			}
		}else{
			$this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to update! Please edit and try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
		}

		$this->response->redirect($this->url->get("settings"));
	}

	/**
	 * Edits an existing Profile
	 *
	 * @param int $id
	 */
	public function editAction($id)
	{
		$user = User::findFirstById($id);
		if (!$user) {
			$this->flash->error("User was not found");
			return $this->dispatcher->forward(array(
					'action' => 'index'
			));
		}

		if ($this->request->isPost()) {

			$user->assign(array(
					'firstname' => $this->request->getPost('firstname', 'striptags'),
					'lastname' => $this->request->getPost('lastname', 'striptags'),
					'email' => $this->request->getPost('lastname', 'striptags')
			));

			if (!$user->save()) {
				$this->flash->error($user->getMessages());
			} else {
				$this->flash->success("User was updated successfully");
			}

			Tag::resetInput();
		}

		$this->view->form = new ProfileForm($user, array(
				'edit' => true
		));

		$this->view->user = $user;
	}

	public function updatebioAction(){
		$this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
		$result = new stdClass();

		try{
			$about = stripslashes($this->request->get('about'));

			if($about){
				if(User::updateUserBio($this->_sessionUser->id, $about)){
					//$result->success = true;
					//$result->message = "User information updated.";
					$this->flash->success('<div data-toggle="notify" data-onload data-message="Updated Successfully!" data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
					$this->response->redirect($this->url->get('overview'));
				}else{
					//$result->success = false;
					//$result->message = "Failed to update user information.";
					$this->flash->error('<div data-toggle="notify" data-onload data-message="Updated failed! Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
					$this->response->redirect($this->url->get('overview'));
				}
			}else{
				//$result->success = false;
				//$result->message = "Invalid parameters.";
				$this->flash->error('<div data-toggle="notify" data-onload data-message="Updated failed! Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
				$this->response->redirect($this->url->get('overview'));
			}
		}catch(Exception $e){
			//$result->success = false;
			//$result->message = "An occur occurred while updating user information";
			$this->flash->error('<div data-toggle="notify" data-onload data-message="Updated failed! Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
			$this->response->redirect($this->url->get('overview'));
		}
		//$this->component->helper->outputJSON($result->success, $result->message);
	}

	public function changePasswordAction() {

		if($this->request->isPost()){

			$this->view->disable();
			$failed = false;

			$cpwd = $this->request->getPost("cpassword");
			$pwd = $this->request->getPost("password");
			$rPwd  = $this->request->getPost("rpassword");

			if(empty($pwd) || empty($rPwd)){
				$this->flash->warning('<div data-toggle="notify" data-onload data-message="Passwords cannot be empty." data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
				$failed = true;
			}else if($pwd !== $rPwd){
				$this->flash->warning('<div data-toggle="notify" data-onload data-message="Passwords do not match." data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
				$failed = true;
			}

			if(!$failed){
				$userModel = new User();
				$user = $userModel->findUserById($this->_sessionUser->id);
				$success = $userModel->validateUser($user->usr_email, $cpwd);
				if($success){
					$user = new User();
					$user->changePassword($this->_sessionUser->id, $pwd);
					$this->flash->success('<div data-toggle="notify" data-onload data-message="Your password is changed successfully." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
					$this->response->redirect($this->url->get("settings"));
				}else{
					$this->flash->error('<div data-toggle="notify" data-onload data-message="Current password is incorrect. Please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
					$this->response->redirect($this->url->get("settings"));
				}			
			}else{
				$this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to change password. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
				$this->response->redirect($this->url->get("settings"));
			}

		}else{

			$this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to change password. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
			$this->response->redirect($this->url->get("settings"));
		}

		$this->response->redirect($this->url->get("settings"));
	} 

	/* public function callAction() {
		require("/public/third-party/vendor/twilio/Services/Twilio.php");

		$this->view->disable();

		if(!$this->component->user->hasSession()){
			$this->flash->error("Unable to verify. User not logged in.");
			$this->response->redirect($this->url->get("/"));
		}

		$this->component->helper->csrf("/user/profile");

		if($this->request->isPost()){
			$number = (object)$this->request->getPost("number");
			if(isset($number->usr_id) && isset($number->phone_number)){
				$user = $this->component->user->getSessionUser();
				$n = new Numbers();
				$r=$n->saveNumber($user->id, $number->usr_id, $number->phone_number);
				$this->flash->success("Phone number was saved successfully.");
			}else{
				$this->flash->error("Error occured.");
			}
		}else{
			$this->flash->error("Unable to save phone number.");
		}

		// initiate phone call via Twilio REST API
		// Set our AccountSid and AuthToken
		$AccountSid = "ACxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
		$AuthToken = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
		// Instantiate a new Twilio Rest Client
		$client = new Services_Twilio($AccountSid, $AuthToken);
		try {
			// make call
			$call = $client->account->calls->create(
			'+18881234567', // Verified Outgoing Caller ID or Twilio number
			$number, // The phone number you wish to dial
			'/public/third-party/vendor/twilio/Services/Twilio/Twiml.php' // The URL of twiml.php on your server
		);
		} catch (Exception $e) {
			echo 'Error starting phone call: ' . $e->getMessage();
		}
		// return verification code as JSON
		$json = array();
		$json["verification_code"] = $code;
		header('Content-type: application/json');
		echo(json_encode($json));
	} */

}
