<?php

use Phalcon\Tag,
	Phalcon\Mvc\Url,
    Component\Json,
    Phalcon\Validation,
    Phalcon\Validation\Validator\Email;


class SigninController extends ControllerBase
{
    public function initialize()
    {
        if($this->component->user->hasSession()){
        	//$this->flash->error("You are already logged in");
            $this->response->redirect($this->url->get(""));
        } 
        parent::initialize();
    }

    public function authAction($provider){
        try{
            switch ($provider) {
                case 'facebook':
                    $ch = curl_init();
                    curl_setopt_array ( $ch , array(
                        CURLOPT_SSL_VERIFYHOST => false,
                        CURLOPT_SSL_VERIFYPEER => false,
                    ) );
                    $adapter = $this->hybridauth->authenticate( "Facebook" );
                    break;
                case 'linkedin':
                    $adapter = $this->hybridauth->authenticate( "Linkedin" );
                    break;
                default:
                    # code...
                    break;
            }
              if (!empty($adapter))
              {
                    // Request User Profile
                    $user_profile = $adapter->getUserProfile();
                    error_log("<pre>".print_r($user_profile,true)."</pre>");
                    $userModel = new User();
                    $userConnection = new UsersConnections();
                    $profile = '';
                    if ($provider == 'facebook'){
                        $profile = $user_profile->profileURL;
                    }
                    error_log($profile);
                    //from setting, attach another connection
                    if($this->component->user->hasSession()){ error_log('has session');
                        $hybridauth_session_data = $this->hybridauth->getSessionData();
                        $saveConnection = $userConnection->addConnection(
                            $this->component->user->getSessionUser()->id,
                                                       $provider,
                                         $hybridauth_session_data);
                        $this->response->redirect($this->url->get("settings"));
                    //logining in
                    }elseif (User::userEmailExists($user_profile->email)) { error_log('email exist');
                        $user = User::findUserByEmail($user_profile->email);
                        $hasConnection = $userConnection->retrieveConnect($user->usr_id, $provider);
                        $hybridauth_session_data = $this->hybridauth->getSessionData();
                        if (!$hasConnection){
                            // save session
                            $saveConnection = $userConnection->addConnection($user->usr_id,
                                                                                  $provider,
                                                                  $hybridauth_session_data,
                                                                  $profile);
                        }else{
                            //update session
                            $saveConnection = $userConnection->updateConnection($hasConnection, $hybridauth_session_data);
                        }
                        $success = User::activate($user->usr_id);
                        if ($success) {
                            $this->component->user->createUserSession($user);
                            $this->response->redirect($this->url->get(""));
                        }else{
                            $this->flash->error('<div data-toggle="notify" data-onload data-message="Some error occurred. Please contact support at general@quikwit.co" data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                            $this->response->redirect($this->url->get("signin")); 
                        }
                    }else{ error_log('add user');
                        //new user
                        $save = $userModel->addUser($user_profile->email,
                                           sha1(time()),
                                           $user_profile->firstName,
                                           $user_profile->lastName);
                        if ($save){
                            $user = User::findUserByEmail($user_profile->email);
                            $hybridauth_session_data = $this->hybridauth->getSessionData();
                            $saveConnection = $userConnection->addConnection($user->usr_id,
                                                                                  $provider,
                                                                  $hybridauth_session_data,
                                                                  $profile);
                            $this->component->user->createUserSession($user);
                            $this->response->redirect($this->url->get(""));
                        }
                    }
              }
            }catch(\Exception $e){
                echo '<pre>'.print_r($e,true).'</pre>';
                // Display the recived error,
                // to know more please refer to Exceptions handling section on the userguide
                switch( $e->getCode() ){
                  case 0 : error_log( "Unspecified error."); break;
                  case 1 : error_log( "Hybriauth configuration error."); break;
                  case 2 : error_log( "Provider not properly configured."); break;
                  case 3 : error_log( "Unknown or disabled provider."); break;
                  case 4 : error_log( "Missing provider application credentials."); break;
                  case 5 : $this->response->redirect($this->url->get("signin"));
                           break;
                  case 6 : error_log( "User profile request failed. Most likely the user is not connected "
                              . "to the provider and he should authenticate again.");
                           $twitter->logout();
                           break;
                  case 7 : error_log("User not connected to the provider.");
                           $twitter->logout();
                           break;
                  case 8 : error_log("Provider does not support this feature."); break;
            }
                    // well, basically your should not display this to the end user, just give him a hint and move on..
            error_log("<br /><br /><b>Original error message:</b> " . $e->getMessage());

        }
    }
    public function socialAction(){
        require_once( APPLICATION_VENDOR . '/hybridauth/hybridauth/hybridauth/Hybrid/Auth.php' );
        require_once( APPLICATION_VENDOR . '/hybridauth/hybridauth/hybridauth/Hybrid/Endpoint.php' );
        $endpoint = new Hybrid_Endpoint();
        $endpoint->process();
    }
    public function indexAction()
    {
        Tag::setTitle('Signin');
        //$this->assets->collection('additional')->addCss('css/signin.css');
    }

    public function doSigninAction()
    {
        $this->view->disable();
        //$this->component->helper->csrf("/signin/index");

        if($this->request->isPost()){

        	if($this->security->checkToken()){
        		$username = $this->request->getPost("username", "email");
        		$password = $this->request->getPost("password", "string");

        		$userModel = new User();

        		$user = $userModel->validateUser($username, $password);
        		if ($user)
        		{
                    $success = $userModel->activate($user->usr_id);
                    if ($success) {
                        $this->component->user->createUserSession($user);
                        if($user->usr_email_verified == 0){
                            $this->flash->success('<div data-toggle="notify" data-onload data-message="&lt;b&gt;Welcome!&lt;/b&gt; Please confirm your email in your inbox before you continue! If you did not receive one, you can go to Settings in your account tab to resend the confirmation." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                            $this->response->redirect($this->url->get("overview"));
                        }else{
                            //$this->flash->success('<div data-toggle="notify" data-onload data-message="&lt;b&gt;Welcome!&lt;/b&gt; You have signed in successfully!" data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                            $this->response->redirect($this->url->get("overview"));
                        }
                    }else{
                        $this->flash->error('<div data-toggle="notify" data-onload data-message="Some error occurred. Please contact support at general@quikwit.co" data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                        $this->response->redirect($this->url->get("signin")); 
                    }

        		}
        		else{
        			$this->flash->error('<div data-toggle="notify" data-onload data-message="Please check your email or password." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
        			$this->response->redirect($this->url->get("signin"));
        			//exit("Email or Password is incorrect.");
        		}
        	}else{
                //$this->flash->warning('<div data-toggle="notify" data-onload data-message="Invalid CSRF token." data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
                $this->response->redirect($this->url->get("signin"));
            }

        }else{
        	$this->flash->error("Invalid method, please try again!");
        	$this->response->redirect($this->url->getStatic("signin"));
        }
    }

    public function resetPasswordAction()
    {
        Tag::setTitle('Password Reset Page');
        $hash = $this->request->getQuery('hash');
        $recovery = UsersPasswordRecovery::getUserRecovery($hash);
        $this->view->setVar('isValid', '');
        $this->view->setVar('isExpired' , '');
        $this->view->setVar('isCompleted', '');
        if ($recovery) {
            if ($this->request->isPost()) {
                $json  = new Json();
                $password = $this->request->getPost('password');
                $cPassword = $this->request->getPost('password_confirmation');
                if ($password && $password === $cPassword) {
                    $user = User::findUserById($recovery->user_id);
                    if ($user) {
                        if ($user->resetPassword($password)) {
                            $recovery->setRecoveryCompleted();
                            $json->setJSON(true, 'Password is successfully updated.', 1);
                            $this->flash->success('<div data-toggle="notify" data-onload data-message="Password is succesfully updated." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                        } else {
                            $json->setJSON(false, 'Failed to update user password. Please try again later.', 0);
                            $this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to update user password." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                        }
                    } else {
                        $json->setJSON(false, 'User is not found.', 0);
                        $this->flash->error('<div data-toggle="notify" data-onload data-message="User is not found." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                    }
                } else {
                    $json->setJSON(false, 'Passwords do not match. Please check your passwords.', 0);
                    $this->flash->error('<div data-toggle="notify" data-onload data-message="Passwords do not match. Please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                }
                $json->output();
            }
            $this->view->setVar('isValid', $recovery->isValid());
            $this->view->setVar('isExpired' , $recovery->isExpired());
            $this->view->setVar('isCompleted', $recovery->isCompleted());
        }
    }

    public function recoverAction()
    {
        if ($this->request->isPost()) {
            $json = new Json();
            $validation = new Validation();
            $validation->add('email', new Email(array(
                'message' => 'The e-mail is invalid'
            )));
            if ($validation->validate($this->request->getPost())) {
                $email = $this->request->getPost('email');
                if ($user = User::findUserByEmail($email)) {                	
                    $recovery = new UsersPasswordRecovery();
                    
                    //get hash from recovery model
                    $hash = $recovery->addPasswordRecovery($user->usr_id, $user->usr_password);
                    
                    //send recovery email
                    $usename = $user->usr_firstname.' '.$user->usr_lastname;
                    $recovery->sendPasswordRecoveryEmail($email, $usename, $hash);

                    $json->setJSON(true, 'The email containing reset instructions has been sent.', 1);
                    //$this->flash->success('<div data-toggle="notify" data-onload data-message="The email containing reset instructions has been sent. If problem persists, please feel free to email general@quikwit.co for support." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                    $this->response->redirect($this->url->get('signin/emailSent'));
                } else {
                    $json->setJSON(false, 'Email is not found.', 0);
                    $this->flash->error('<div data-toggle="notify" data-onload data-message="This email is not found in our system. Please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                    $this->response->redirect($this->url->get('signin/recover'));
                }
            } else {
                $json->setJSON(false, 'This email address is invalid. Please check your email address.', 0);
                $this->response->redirect($this->url->get('signin/recover'));
            }

            $json->output();
        } else {
            Tag::setTitle('Recover password');
        }
    }


    public function emailSentAction()
    {
        Tag::setTitle('Email Sent');
       
    }

    public function passwordChangedAction()
    {
        Tag::setTitle('Password Changed');
       
    }



}