<?php

class SignoutController extends ControllerBase
{

	private $_session;
	
	public function initialize(){
		$this->_session = $this->component->user;
	}
	
	public function indexAction(){
		if($this->_session->hasSession()){
			$this->_session->destroyUserSession();
		}
		//$this->flash->success('<div data-toggle="notify" data-onload data-message="&lt;b&gt;Bye Bye!&lt;/b&gt; You have signed out successfully!" data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
		return $this->response->redirect($this->url->get("signin"));
	}
}

?>