<?php

use \Phalcon\Tag,
    \Phalcon\Mvc\Url,
    Component\Json;


class SuggestController extends ControllerBase
{
    public function indexAction()
    {
        tag::setTitle("Suggest a Service");
    }

    public function suggestSkillAction() 
    {
        //$json = new Json();
        $skills = $this->request->get('skills', 'string');

        if($skills){
            $skillSet = array_map(function($str){
                    return ucwords(strtolower($str));
                }, explode(",", $skills));
            $tagstandard = new TagsStandard();
            foreach($skillSet as $skill){
                $tag = $tagstandard->findTagByName($skill);
                if($tag->count()===0){
                    $tagsuggest = (new Tags())->add($skill);
                    $this->flash->success('<div data-toggle="notify" data-onload data-message="Thanks for the suggestion. This service category is now under review." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                    $this->response->redirect($this->url->get('suggest'));
                }
                else{
                    $this->flash->error('<div data-toggle="notify" data-onload data-message="One of the service categories you try to add already exists." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                    $this->response->redirect($this->url->get('suggest'));
                    //$json->setJSON(false, "Skill already exists.", 1);
                } 
            }
        }else{
            $this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry, some error occured. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            $this->response->redirect($this->url->get('suggest'));
            //$json->setJSON(false, "Invalid parameter.", -1);
        }
        //$json->output();
    } 

}