
<?php

use \Phalcon\Tag;

class TemplatesPreviewController extends ControllerBase
{

    public function activationAction()
    {
       Tag::setTitle('Activation');
       
    }

    public function launchAction()
    {
       Tag::setTitle('Launch');
       
    }

    public function newsletterAction()
    {
       Tag::setTitle('Newsletter');
       
    }

    public function passwordResetAction()
    {
       Tag::setTitle('Password Reset');
       
    }

    public function WelcomeAction()
    {
       Tag::setTitle('Welcome');
       
    }

    public function confirmBookingAction()
    {
       Tag::setTitle('Confirmation of Booking');
       
    }

    public function completeLessonAction()
    {
       Tag::setTitle('Completion of Lesson');
       
    }

    public function pendingChangeAction()
    {
       Tag::setTitle('Lesson Accepted');
       
    }

    public function dateChangeAction()
    {
       Tag::setTitle('Lesson Date/Time Changed');
       
    }
    
    public function cancelLessonAction()
    {
       Tag::setTitle('Cancelling Lesson');
       
    }

    public function underReviewAction()
    {
       Tag::setTitle('Lesson Status Changed to Under Review');
       
    }

    public function reviewLessonAction()
    {
    	Tag::setTitle('Review of Lesson');
    }

    public function rejectChangeAction()
    {
    	Tag::setTitle('Lesson Rejected');
    }

}