<?php

use \Phalcon\Tag;

class TermsController extends ControllerBase
{

    public function indexAction()
    {
       Tag::setTitle('Terms of Service');
       
    }

    public function privacyAction()
    {
       Tag::setTitle('Privacy Policy');
       
    }

}