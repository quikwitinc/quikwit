<?php 

use \Phalcon\Tag,
    \Component\PrettyPrint as PrettyPrint,
    Component\User;
    
class TimezoneController extends ControllerBase
{
		
    public function getTimezoneAction()
    {

    	if($this->request->isPost()){

            $tz = $this->request->getPost("tz");

            $this->component->user->setSessionUserTimeZone($tz);
            //$prettyPrint = new PrettyPrint();
            //$prettyPrint->getTimezone($tz);

        }
    }

}