<?php

use \Phalcon\Tag,
	\Phalcon\Mvc\Url,
	Component\Json,
	Component\User as SessionUser;

class UserController extends ControllerBase
{
    public function initialize()
    {
        //if($this->component->user->hasSession()){
        //	$this->flash->error("Already logged in");
        //	$this->response->redirect($this->url->get("/"));
        //}
        parent::initialize();
    }

    /**
     * Default action, shows the search form
     */
    public function indexAction()
    {
        Tag::setTitle('Profile');
        $this->persistent->conditions = null;
        $this->view->form = new ProfileForm();
    }

    public function profileAction($id){
    	Tag::setTitle("User Profile");
    	$this->view->userId = $id;
        //$this->view->userLink = "http://www.tute.com/user/profile/" . $id;
        $loginUser = $this->component->user->getSessionUser();
        $userModel = new User();
        $currentUser = $userModel->findUserById($loginUser->id);
        $email_verified = $currentUser->usr_email_verified;
        $email = $currentUser->usr_email;

        if ($loginUser){
           $gps = User::getGPS($loginUser->id);
           $this->view->setVar('loginUser', $loginUser);
           $this->view->setVar('loginUserId', $loginUser->id);
           $this->view->setVar('email', $email);
           $this->view->setVar('email_verified', $email_verified);
           $this->view->setVar('lat', $gps['lat']);
           $this->view->setVar('lng', $gps['lng']);
        }else{
           $loginUser = new stdClass;
           $loginUser->id = '';
           $this->view->setVar('loginUser', null);
           $this->view->setVar('loginUserId', 0);
           $this->view->setVar('email_verified', 0);
           $this->view->setVar('email', null);
           $this->view->setVar('lat', 0);
           $this->view->setVar('lng', 0);
        }

    	if($id){
    		$user = User::findUserById($id);
    		if($user){
                if($user->usr_deleted == 0){
                    if (!$user->lat || !$user->lng){
                        $user->lat = 9999;
                        $user->lng = 9999;
                    }
                    $this->view->userInfo = $user;

                    $skills = array();
                    for($i = 0; $i < count($user->getUserTags()); $i++){
                        $skills[] = $user->getUserTags()[$i]->getTagsStandard()->name;
                    }
                    if (count($skills) < 1){
                        $skills[] = 'No skills selected';
                    }
                    foreach ($skills as $key => $value) {
                        $this->view->userTags .= '<a href="javascript:void(0)" class="label label-info">' . $value . '</a> ';
                    }
                    $this->view->userTagsComma = ($skills[0] == 'No skills selected')?'':implode(',', $skills);
                    $languages = array();
                    for($i = 0; $i < count($this->view->userInfo->getUserLanguages()); $i++){
                        $languages[] = $this->view->userInfo->getUserLanguages()[$i]->getLanguages()->name;
                    } 
                    if (count($languages) < 1){
                        $languages[] = 'No languages selected';
                    }
                    $this->view->userLangs = implode(", ", $languages);
                    $this->view->userLangsComma = ($languages[0] == 'No languages selected')?[]:$languages;
                    $reviews = $this->view->userInfo->getReviews(array('order' => 'rvs_created DESC'));
                    $this->view->reviews = $reviews;
                    $this->view->viewHelper = $this->component->helper;
                    $reviewModel = new Reviews();
                    $this->view->stats = $reviewModel->getStats($this->view->userInfo->usr_id)->toArray();
                    $contractModel = new Contracts();
                    $this->view->contractCompleted = $contractModel->getNumCompletedContracts($this->view->userInfo->usr_id)->toArray();
                    $exp = $this->view->userInfo->getUsersExperience(array('order' => 'end_year DESC'));
                    $exp = $exp->toArray();
                    $userConnectionModel = new UsersConnections();
                    $facebook = $userConnectionModel->retrieveConnect($id, 'facebook');
                    $linkedin = $userConnectionModel->retrieveConnect($id, 'linkedin');
                    $socialMedia = array();
                    $this->view->connections = "No Social Media";
                    if ($facebook){
                        $socialMedia[] = "<a href='$facebook->usr_profile' target='__blank'>facebook</a> ";
                    }
                    if ($linkedin){
                        $socialMedia[] = "<a href='$linkedin->usr_profile' target='__blank'>linkedin</a>";
                    }
                    if (count($socialMedia) > 0){
                        $this->view->connections = implode(' ', $socialMedia);
                    }

                    $presentJobs = array();
                    foreach ($exp as $key => $value) {
                        if ($value['end_year'] == 9999){
                            $presentJobs[] = $value;
                            unset($exp[$key]);
                        }
                    }
                    $exp = array_merge($presentJobs, $exp);
                    $this->view->userExperiences = $exp;
                    $video = new Video();
                    $this->view->videos = $video->getUserVideos($this->view->userInfo->usr_id)->toArray();
                    $media = new Media();
                    $this->view->medias = $media->getUserMedia($this->view->userInfo->usr_id)->toArray();   
                }else{
                    //$this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry, this account has been deactivated at the request of the account owner." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                }          
    		}else{
                //$this->flash->error('<div data-toggle="notify" data-onload data-message="This user is not found." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
    		}
    	}else{
            $this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry, some error occured. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
    	}
    }

    public function deactivateAction()
    {
        if($this->request->isPost()) {
            //$json = new Json();
            $loginUser = $this->component->user->getSessionUser();
            $user = new User();
            $user->setTeach($loginUser->id, false);
            $success = $user->deactivate($loginUser->id);

            if($success) {
                $this->_session = $this->component->user;
                $this->_session->destroyUserSession();
                //$json->setJSON(true, 'User deactivated.', 1);   
            }else{
                //$json->setJSON(false, 'Error Occurred.', 0); 
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry, some error occured. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            }

        }
    }

    /**
     * Creates a new Profile
     */
    public function createAction()
    {
        if ($this->request->isPost()) {

            $user = new User();

            $user->assign(array(
                'firstname' => $this->request->getPost('firstname', 'striptags'),
                'lastname' => $this->request->getPost('lastname', 'striptags'),
                'email' => $this->request->getPost('lastname', 'striptags')
            ));

            if (!$user->save()) {
                $this->flash->error($user->getMessages());
            } else {
                $this->flash->success("Profile was created successfully");
            }

            Tag::resetInput();
        }

        $this->view->form = new ProfileForm(null);
    }

    /**
     * Edits an existing Profile
     *
     * @param int $id
     */
    public function editAction($id)
    {
        $user = User::findFirstById($id);
        if (!$user) {
            $this->flash->error("User was not found");
            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        }

        if ($this->request->isPost()) {

            $user->assign(array(
                'firstname' => $this->request->getPost('firstname', 'striptags'),
                'lastname' => $this->request->getPost('lastname', 'striptags'),
                'email' => $this->request->getPost('lastname', 'striptags')
            ));

            if (!$user->save()) {
                $this->flash->error($user->getMessages());
            } else {
                $this->flash->success("User was updated successfully");
            }

            Tag::resetInput();
        }

        $this->view->form = new ProfileForm($user, array(
            'edit' => true
        ));

        $this->view->user = $user;
    }

    /**
     * Deletes a Profile
     *
     * @param int $id
     */
    public function deleteAction($id)
    {
        $user = User::findFirstById($id);
        if (!$user) {

            $this->flash->error("User was not found");

            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        }

        if (!$user->delete()) {
            $this->flash->error($user->getMessages());
        } else {
            $this->flash->success("User was deleted");
        }

        return $this->dispatcher->forward(array(
            'action' => 'index'
        ));
    }

    public function byeAction()
    {
        Tag::setTitle('Sorry to see you go :(');
    }

    public function updateTeachAction() {
       	$json = new Json();

        try{
            $teach = $this->request->get("teach", 'string');
            if($teach){
                $sessionUser = (new SessionUser())->getSessionUser();

                $userModel = new User();
                $user = $userModel->findUserById($sessionUser->id);

                if($user->usr_email_verified == 1) {
                    if(User::setTeach($sessionUser->id, $teach==='true'? true : false)){
                        if($teach==='true'){
                           $this->flash->success('<div data-toggle="notify" data-onload data-message="Great! You are now a freelancer. Now you should set your details in your profile." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>'); 
                        }else{
                            $this->flash->success('<div data-toggle="notify" data-onload data-message="You are not a freelancer anymore! But you can still take contracts from other freelancers." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>'); 
                        }                   
                        $json->setJSON(true, "Updated successfully.", ($teach == 'true')?'Switch to Client account':'Become a Freelancer');
                    }else{
                        $this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry, please try again." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                        $json->setJSON(false, "Updated failed.", -1);
                    }
                }else{
                    $this->flash->error('<div data-toggle="notify" data-onload data-message="Please confirm your email first before proceeding." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                }
            }else{
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry, an error has occured. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                $json->setJSON(false, "Invalid parameters.", -1);
            }
        }catch(Exception $e){
            $this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry, an error has occured. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
        	$json->setJSON(false, "An occur has occurred.", -1);
        }
       	$json->output();
    }

    public function updateSubscribeAction() {

        $this->view->disable();
        $user = $this->component->user->getSessionUser();

        if ($this->request->isPost()) {

            $value = $this->request->getPost('value');

            if($value == 1 || $value == 0) {
                $userModel = new User();
                $success = $userModel->subscribeUser($user->id, $value);

                if($success) {
                    $this->flash->success('<div data-toggle="notify" data-onload data-message="Your subscription status has been updated." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>'); 
                }else{
                    $this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to update subscription status." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>'); 
                }
            }else{
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Oops, error occured. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>'); 
            }

        }
    }

    public function unsubscribeFromEmailAction($newId) {

        $this->view->disable();

        $hashids = new Hashids\Hashids(null, 8, null);
        $originalId = $hashids->decode($newId);
        $id = implode('', $originalId);

        $userModel = new User();
        $success = $userModel->subscribeUser($id, 0);

        if($success) {
            $this->flash->success('<div data-toggle="notify" data-onload data-message="Your subscription status has been updated." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
            $this->response->redirect($this->url->get('user/unsubscribed')); 
        }else{
            $this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to subsubscribe. Please login to change your preference." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>'); 
            $this->response->redirect($this->url->get('signin'));
        }

    }

    public function unsubscribedAction()
    {
        Tag::setTitle('Unsubscribed');   
    }

    public function updateForwardAction() {

        $this->view->disable();
        $user = $this->component->user->getSessionUser();

        if ($this->request->isPost()) {

            $value = $this->request->getPost('value');

            if($value == 1 || $value == 0) {
                $userModel = new User();
                $success = $userModel->forwardEmail($user->id, $value);

                if($success) {
                    $this->flash->success('<div data-toggle="notify" data-onload data-message="Your email forwarding status has been updated." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>'); 
                }else{
                    $this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to update email forwarding status." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>'); 
                }
            }else{
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Oops, error occured. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>'); 
            }

        }
    }

    public function updateSkillAction() {

    	$json = new Json();
        $userModel = new User();
    	$user = $this->component->user->getSessionUser();
        $skills = $this->request->get('skills', 'string');

    	if($user && $skills){
    		$userTags = new UserTags();
            $tagsFound = $userTags->getUserTags($user->id);
    		$userSkills = array_map(function($s){
    			return $s['name'];
    		}, $userTags->getUserTags($user->id));     
    		$newSkills = array_diff($skills, $userSkills);
    		$removedSkills = array_diff($userSkills, $skills);
    		$tags = new TagsStandard();
    		foreach($newSkills as $skill){
    			$tag = $tags->findTagByName($skill);
    			if($tag->count()===1){
    				$tag = $tag->getFirst();
    				$userTags->add($user->id, $tag->id);
    			}
                else{
                    $json->setJSON(false, "Skill not available.", 1);
                    $this->flash->warning('<div data-toggle="notify" data-onload data-message="This skill is not available." data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
    			} 
    		}
    		foreach($removedSkills as $skill){
    			$tag = $tags->findTagByName($skill);
    			if($tag->count()===1){
    				$tag = $tag->getFirst();
    				if($tag && $tag->id) {
    					$userTags->deleteTag($user->id, $tag->id);
                    }
    			}
    		}
    		$json->setJSON(true, "User skills updated.", 1);
            $this->flash->success('<div data-toggle="notify" data-onload data-message="Your skills have been updated successfully!" data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
    	}else if ($user && strlen($skills) == 0){
            $tags = new TagsStandard();
            $userTags = new UserTags();
            $userSkills = array_map(function($s){
                return $s['name'];
            }, $userTags->getUserTags($user->id));
            foreach($userSkills as $skill){
                $tag = $tags->findTagByName($skill);
                if($tag->count()===1){
                    $tag = $tag->getFirst();
                    if($tag && $tag->id)
                        $userTags->deleteTag($user->id, $tag->id);
                }
            }
            $json->setJSON(true, "User skills updated.", 1);
            $this->flash->success('<div data-toggle="notify" data-onload data-message="Your skills have been updated successfully!" data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
        }else{
    		$json->setJSON(false, "Invalid parameter.", -1);
    	}
    	$json->output();
    }

    public function updateLanguageAction() {

        $json = new Json();
        $userModel = new User();
        $user = $this->component->user->getSessionUser();
        $languages = $this->request->get('languages', 'string');

        if($user && $languages){
            $userLangs = new UserLanguages();
            $langsFound = $userLangs->getUserLanguages($user->id);
            $userLanguages = array_map(function($s){
                return $s['name'];
            }, $userLangs->getUserLanguages($user->id));     
            $newLanguages = array_diff($languages, $userLanguages);
            $removedLanguages = array_diff($userLanguages, $languages);
            $langs = new Languages();
            foreach($newLanguages as $language){
                $lang = $langs->findLangByName($language);
                if($lang->count()===1){
                    $lang = $lang->getFirst();
                    $userLangs->add($user->id, $lang->id);
                }
                else{
                    $json->setJSON(false, "Language not available.", 1);
                    $this->flash->warning('<div data-toggle="notify" data-onload data-message="This language is not available." data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
                } 
            }
            foreach($removedLanguages as $language){
                $lang = $langs->findLangByName($language);
                if($lang->count()===1){
                    $lang = $lang->getFirst();
                    if($lang && $lang->id) {
                        $userLangs->deleteLanguages($user->id, $lang->id);
                    }
                }
            }
            $json->setJSON(true, "User languages updated.", 1);
            $this->flash->success('<div data-toggle="notify" data-onload data-message="Your languages have been updated successfully!" data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
        }else if ($user && strlen($languages) == 0){
            $langs = new Languages();
            $userLangs = new UserLanguages();
            $userLanguages = array_map(function($s){
                return $s['name'];
            }, $userLangs->getUserLanguages($user->id));
            foreach($userLanguages as $language){
                $lang = $langs->findLangByName($language);
                if($lang->count()===1){
                    $lang = $lang->getFirst();
                    if($lang && $lang->id)
                        $userLangs->deleteLanguages($user->id, $lang->id);
                }
            }
            $json->setJSON(true, "User languages updated.", 1);
            $this->flash->success('<div data-toggle="notify" data-onload data-message="Your languages have been updated successfully!" data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
        }else{
            $json->setJSON(false, "Invalid parameter.", -1);
        }
        $json->output();
    }

    public function updateRateAction()
    {
        $json = new Json();
        $userModel = new User();
        $user = $this->component->user->getSessionUser();
        $rate = $this->request->get('rate');
        if($user && $rate && is_numeric($rate && $rate >= 0)){
            $userModel->saveTeachingRate($user->id, $rate);
            $json->setJSON(true, "User rate updated.", 1);
        }
        $json->output();
    }

    public function updateNameAction()
    {
        $json = new Json();
        $userModel = new User();
        $user = $this->component->user->getSessionUser();
        $firstname = $this->request->get('firstname');
        $lastname = $this->request->get('lastname');
        if($user && $firstname && $lastname){
            $userModel->saveName($user->id, $firstname, $lastname);
            $json->setJSON(true, "User name updated.", 1);
        }
        $json->output();
    }

    public function updatePostalAction()
    {
        $json = new Json();
        $userModel = new User();
        $user = $this->component->user->getSessionUser();
        $postal = $this->request->get('postalcode');
        $lat = $this->request->get('lat');
        $lng = $this->request->get('lng');
        $cityId = $this->request->get('cityId');

        if($user && $postal && $cityId){
            $success = $userModel->savePostalCode($user->id, $postal, $lat, $lng, $cityId);

            if($success){
                $this->flash->success('<div data-toggle="notify" data-onload data-message="Your location is updated successfully!" data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                $json->setJSON(true, "Postal Code updated.", 1);
                $this->response->redirect($this->url->get('overview'));
            }else{
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry! Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                $this->response->redirect($this->url->get('overview'));
            }
            
        }else{
            $this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry! Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            $this->response->redirect($this->url->get('overview'));
        }
        //$json->output();
    }

    public function addExperienceAction()
    {
        $json = new Json();
        $user = $this->component->user->getSessionUser();
        $title = $this->request->getPost('title');
        $content = $this->request->getPost('content');
        $start_month = $this->request->getPost('startMonth');
        $start_year = $this->request->getPost('startYear');
        $end_month = $this->request->getPost('endMonth');
        $end_year = $this->request->getPost('endYear');
        // $jobTag = $this->request->getPost('jobTag');
        $condition = strlen($title) > 0 &&
        strlen($content) > 0 &&
        $start_month > 0 && $start_month < 13 &&
        $end_month > 0 && $end_month < 13 &&
        is_numeric($start_year) && strlen($start_year) == 4 &&
        is_numeric($end_year) && strlen($end_year) == 4;
         // && strlen($jobTag) > 0;
        if($user && $condition){
            $userExperienceModel = new UsersExperience();
            $result = $userExperienceModel->addExperience(
                $user->id,
                $title,
                $content,
                $start_month,
                $start_year,
                $end_year,
                $end_month
            );

            if ($result){
                $json->setJSON(true, "User Experience Added.", 1);
                $this->flash->success('<div data-toggle="notify" data-onload data-message="Your have added your experience successfully!" data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');

            }else{
                $json->setJSON(false, "Experience not saved.", -1);
                $this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to update! Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
            }
        }else{
            $json->setJSON(false, "Invalid parameter.", -1);
        }
        $json->output();
    }

    public function getReviewAction()
    {
        $this->view->disable();
        $result = new stdClass();
        $userId = $this->request->get('userId');
        $page = $this->request->get('page');
        $result->success = false;
        $result->page = $page?$page:1;
        $result->numPage = 1;
        $result->total = 0;
        $limit = 5;
        $result->list = array();
        if ($userId && $this->component->user->hasSession()){
            $user = User::findUserById($userId);
            if (isset($user->usr_id)){
                $reviewModel = new Reviews();
                $pageResult = $reviewModel->getPaginateReview($userId, $page, $limit);
                if ($pageResult->total_items > 0 && count($pageResult) > 0 && $page <= $pageResult->total_pages){
                    foreach ($pageResult->items as $key => $value) {
                        $result->list[] = array(
                            'id' => $value->rvs_id,
                            'userId' => $value->rvs_user,
                            'avatar' => $value->getReviewer()->usr_avatar,
                            'firstname' => $value->getReviewer()->usr_firstname,
                            'lastname' => $value->getReviewer()->usr_lastname,
                            'createDate' => $this->component->helper->getTimeAgo(strtotime($value->rvs_created)),
                            'title' => $value->rvs_title,
                            'comment' => $value->rvs_comment
                        );
                    }
                    $result->numPage = $pageResult->total_pages;
                    $result->total = $pageResult->total_items;
                }
            }
        }
        echo json_encode($result);
    }

    public function getExperienceAction($id)
    {
        $usersExperienceModel = new UsersExperience();
        $experience = $usersExperienceModel->findExperienceById($id);
        echo json_encode($experience);
    }

    public function editExperienceAction($id)
    {
        $user = UsersExperience::findFirstById($id);
        if (!$user) {
          echo json_encode(array('message' => 'Experience was not saved'));
        }
        if ($this->request->isPost()) {
            $user->assign(array(
                'title' => $this->request->getPost('title', 'striptags'),
                'content' => $this->request->getPost('content', 'striptags'),
                'start_month' => $this->request->getPost('startMonth', 'striptags'),
                'start_year' => $this->request->getPost('startYear', 'striptags'),
                'end_month' => $this->request->getPost('endMonth', 'striptags'),
                'end_year' => $this->request->getPost('endYear', 'striptags')
            ));
            if ($user->save()) {
                echo json_encode(array('message' => 'Experience saved', 'success' => true));
            } else {
                echo json_encode(array('message' => 'Experience was not saved', 'success' => false));
            }
        }
    }

    public function deleteExperienceAction($id)
    {
        $userExperienceModel = new UsersExperience();
        $searchUserModel = new SearchUsers();
        $user = UsersExperience::findFirstById($id);
        $userId = $user->user_id;
        if ($user) {
        if (!$user->delete()) {
            echo json_encode(array('success'=> false));
        } else {
            if (!$userExperienceModel->hasExperience($userId)){
                $searchUserModel->updateExperience($userId, false);
            }
            echo json_encode(array('success'=> true));
        }
        }
    }

    public function clientAction(){

        if($this->request->isPost()){

            $this->view->disable();
            $client = $this->request->getPost("client");

            //if($client == 0){
                //$this->flash->success('<div data-toggle="notify" data-onload data-message="Great! Before you start searching for the right teacher you should set up your profile." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
            //}

        }else{
            $this->response->redirect('overview');
        }
    }

    public function resendAction(){

        $this->view->disable();
        $user = $this->component->user->getSessionUser();
        $userModel = new User();
        $email = $this->request->getPost("email");
        $success = $userModel->sendEmailConfirmation($user->id, $email, $fname, $lname);

        if($success){
            $this->flash->success('<div data-toggle="notify" data-onload data-message="An Email Confirmation has been sent to your inbox." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
        }else{
            $this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to send Email Confirmation. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
        }
        
    }

    public function giveFeedbackAction(){

        if($this->request->isPost()){

            $this->view->disable();
            $feedback = $this->request->getPost("feedback");
            $user = $this->component->user->getSessionUser();

            if($feedback){
                $feedbackModel = new Feedback();
                $success = $feedbackModel->saveFeedback($user->id, $feedback);

                if($success){
                    $this->flash->success('<div data-toggle="notify" data-onload data-message="Thank you for your feedback." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
                }else{
                    $this->flash->error('<div data-toggle="notify" data-onload data-message="Sorry, we did not get your feedback. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
                }
            }
        }
    }
    
}