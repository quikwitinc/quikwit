<?php

use \Phalcon\Tag;

class VideouploadController extends ControllerBase
{
	const MAX_SIZE = 20000000;
	

	public function initialize()
	{
		parent::initialize();
	}

	public function indexAction()
	{
		Tag::setTitle('Videoupload');
	}

	public function getFormAction(){

	}

	public function processUploadAction(){
		//$response =  new Phalcon\Http\Response();
		//$response->setHeader("Content-Type", "application/json");

		//echo json_encode(array("abc" => "1"));
		//exit;
		//http://php.net/manual/en/features.file-upload.php
		 
		// Undefined | Multiple Files | $_FILES Corruption Attack
		// If this request falls under any of them, treat it invalid.

		//Supports single upload.
		if ( !isset($_FILES['file']['error']) || is_array($_FILES['file']['error'])) {
			$this->component->helper->outputJSON(false, -1, "Invalid Parameters.");
		} else{
			// Check $_FILES['upfile']['error'] value.
			switch ($_FILES['file']['error']) {
				case UPLOAD_ERR_OK:
					break;
				case UPLOAD_ERR_NO_FILE:
					$this->component->helper->outputJSON(false, "File not found");
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:
					$this->component->helper->outputJSON(false, "Exceeded file size limit");
				default:
					$this->component->helper->outputJSON(false, "Unknown error");
			}

			// You should also check filesize here.
			if ($_FILES['file']['size'] > self::MAX_SIZE) {
				$this->component->helper->outputJSON(false, "Exceeded system file size limit");
			}
			
			// DO NOT TRUST $_FILES['upfile']['mime'] VALUE !!
			// Check MIME Type by yourself.
			$finfo = new finfo(FILEINFO_MIME_TYPE);
			if (false === $ext = array_search(
					$finfo->file($_FILES['file']['tmp_name']),
					array(
							"mp4" => "video/mp4",
							//'jpg' => 'image/jpeg',
							//'png' => 'image/png',
							//'gif' => 'image/gif',
					),
					true
			)) {
				$this->component->helper->outputJSON(false, "Allowed formats: mp4");
			}
			
			$user = $this->component->user->getSessionUser();
			$tempPath = $_FILES['file']['tmp_name'];
			//All tests are passed, now move to upload location.
			$uploader = new \Component\Uploader();
			$uri = $user->id."/".$_FILES["file"]["name"];
			$target = $uploader->getUploadPath($uri);
			try{
		
				$yt = new \Component\Youtube();
				
				$moved = $uploader->moveUploadedFile($tempPath, $target);
				
				if($moved){
					$yt->uploadVideo($target);
					$url = $uploader->getFileUrl($uri);
					$this->component->helper->outputJSON(true, "Upload successful", array("url"=>$url));
				}else{
					$this->component->helper->outputJSON(false, "Upload failed");
				}
				
			}catch(Exception $e){
				//print_r($e);
				$this->component->helper->outputJSON(false, $e->getMessage()." ({$e->getFile()} Line ".$e->getLine().")\n{$e->getTraceAsString()}", $e->getCode());
				//$this->component->helper->outputJSON(false, $e, $e->getCode());
			}
		
		}

		// You should name it uniquely.
		// DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
		// On this example, obtain safe unique name from its binary data.
		/*if (!move_uploaded_file(
				$_FILES['upfile']['tmp_name'],
				sprintf('./uploads/%s.%s',
						sha1_file($_FILES['upfile']['tmp_name']),
						$ext
				)
		)) {
			exit('Failed to move uploaded file.');
		}

		echo 'File is uploaded successfully.'; */
	}

	public function doVideoUploadAction()
	{

		if($this->request->isPost()==true){
			 
			$this->view->disable();
			$failed = false;
			 
			$vtitle = $this->request->getPost("title", "string");
			$vtags = $this->request->getPost("user-skills-input");
			if(empty($vtitle)){
				$this->flash->warning('<div data-toggle="notify" data-onload data-message="Please put in a title and type." data-options="{&quot;status&quot;:&quot;warning&quot;}" class="hidden-xs"></div>');
				$failed = true;
			}
			$videoFile = '';	 
			if(!$failed){
				if ($this->request->hasFiles() == true) {
					$uploads = $this->request->getUploadedFiles();
				  	$isUploaded = false;
				  	#do a loop to handle each file individually
				  	foreach($uploads as $upload){				  		
						if(trim($upload->getname()) != "") {
						   	#define a “unique” name and a path to where our file must go
						   	$fileName = $upload->getname();
						   	$actual_image_name = md5(uniqid(rand(), true)).'-'.strtolower($upload->getname());
						   	
						   	$api = $this->getDI()->get('api');
							$bucket 		= $api->amazon->bucket;
							$awsAccessKey 	= $api->amazon->awsAccessKey;
							$awsSecretKey 	= $api->amazon->awsSecretKey;
							
							$s3 = new \Component\AmazonS3($awsAccessKey, $awsSecretKey);
							$s3->putBucket($bucket, $s3::ACL_PUBLIC_READ);
							
							if($s3->putObjectFile($upload->getTempName(), $bucket , $actual_image_name, $s3::ACL_PUBLIC_READ)) {
								$mediaFile='http://'.$bucket.'.s3.amazonaws.com/'.$actual_image_name;
							}
							else {
								$this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to add media in S3, please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
								$this->response->redirect($this->url->get("/overview"));
							}
						}
				 	}
				}
				$video = new Video();
				$user = $this->component->user->getSessionUser();
				if($this->request->getPost("intro_video_id")!="") {

					$status = $video->updateVideo($user->id, $vtitle, $vtags, $fileName, $videoFile);
					$this->flash->success('<div data-toggle="notify" data-onload data-message="Video has been updated to your profile successfully." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');

					$this->response->redirect($this->url->get("/overview"));
				}
				else {
					$status = $video->addVideo($user->id, $vtitle, $vtags, $fileName, $mediaFile);
					
					if($status){
						$this->flash->success('<div data-toggle="notify" data-onload data-message="Video has been updated to your profile successfully." data-options="{&quot;status&quot;:&quot;success&quot;}" class="hidden-xs"></div>');
						$this->response->redirect($this->url->get("/overview"));
					}
					else{
						$this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to upload video. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
					}
				}
				
			}else{
				$this->flash->error('<div data-toggle="notify" data-onload data-message="Failed to register video. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');

			}
		}else{
			$this->flash->error('<div data-toggle="notify" data-onload data-message="Invalid request method. Please try again later." data-options="{&quot;status&quot;:&quot;danger&quot;}" class="hidden-xs"></div>');
		}
		$this->response->redirect($this->url->get("/overview"));
	}
}
