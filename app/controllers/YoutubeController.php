<?php

use \Phalcon\Tag;

class YoutubeController extends ControllerBase
{
	
	private $_youtube = null;
	
    public function onConstruct()
    {
    	$this->_youtube = new \Component\Youtube();
        //parent::initialize();
    }

    public function indexAction()
    {
    	 Tag::setTitle('Youtube');
    }
    
    public function checkTokenAction(){
    	$this->_youtube->checkAccessToken();
    	Tag::setTitle("Youtube Check Token");
    }
}