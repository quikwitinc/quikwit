<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;

class NewLetterForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        /*if (isset($options['edit']) && $options['edit']) {
           $id = new Hidden('id');
        } else {
            $id = new Text('id');
        }

        $this->add($id); */

        /* $name = new Text('name', array(
            'placeholder' => 'Name'
        ));

        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Name is required'
            )),
        	new StringLength(array("max" => 112, "min" => 1, "messageMaximum" => "Name exceeds limited of 112 characters.", "messageMinimum" => "Name Cannot be empty.")),
        ));

        $this->add($name); */

        $email = new Text('email', array(
            'placeholder' => 'Email'
        ));

        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'E-mail is required'
            )),
            new Email(array(
                'message' => 'The e-mail is not valid'
            )),
        	new StringLength(array("max" => 112, "min" => 7, "messageMaximum" => "Email exceeds limited of 112 characters.", "messageMinimum" => "Email cannot be less than 7 characters long.")),
        ));
        
        $this->add($email);
    }
}
