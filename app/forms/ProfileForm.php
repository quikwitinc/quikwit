<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;

class ProfileForm extends Form
{

    public function initialize($entity = null, $options = null)
    {
        if (isset($options['edit']) && $options['edit']) {
            $id = new Hidden('id');
        } else {
            $id = new Text('id');
        }

        $this->add($id);

        $firstname = new Text('firstname', array(
            'placeholder' => 'First Name'
        ));

        $firstname->addValidators(array(
            new PresenceOf(array(
                'message' => 'The first name is required'
            ))
        ));

        $lastname = new Text('lastname', array(
            'placeholder' => 'Last Name'
        ));

        $lastname->addValidators(array(
            new PresenceOf(array(
                'message' => 'The last name is required'
            ))
        ));

        $this->add($firstname);
        $this->add($lastname);

        $email = new Text('email', array(
            'placeholder' => 'Email'
        ));

        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'The e-mail is required'
            )),
            new Email(array(
                'message' => 'The e-mail is not valid'
            ))
        ));

        $this->add($email);

    }
}
