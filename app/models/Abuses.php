<?php

class Abuses extends BaseModel{

	public $abs_id;
	public $abs_user;
	public $abs_target;
	public $abs_created;
	public $abs_reason;
	public $abs_text;

	public function initialize()
	{
		$this->setSource("abuses");
	}

	public function saveAbuse($userId, $abuseTargetId, $reason, $text){
		$this->abs_user = $userId;
		$this->abs_target = $abuseTargetId;
		$this->abs_reason= $reason;
		$this->abs_text = $text;
		$this->abs_created = date("Y-m-d H:i", time());
		$this->save();
		User::reportUser($abuseTargetId);
	}

	public function findAbusesById($id){
		$abuse = self::find(array("conditions" => "abs_id = ?1",
				"bind" => array(1 => $id)));
		return $abuse->getFirst();
	}

	public function retrieveAbuses(){
		$sql = 'SELECT * FROM abuses ORDER BY abs_created DESC';

		$params = NULL;

		$result = $this->getResultSet($sql, $params)->toArray();
		return $result;
	}

}