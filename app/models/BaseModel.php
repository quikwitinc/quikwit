<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class BaseModel extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        
    }

    public function beforeCreate()
    {
        //$this->usr_created = $this->_timeStamp();
    }

    public function beforeUpdate()
    {
        //$this->updated_at = date('Y-m-d H:i:s');
    }

    public static function getTimeStamp()
    {
        return time();
    }

    /**
     * @param $sql
     * @param $params
     * @return Resultset
     */
    protected function getResultSet($sql, $params)
    {
        $result = new Resultset(null, $this, $this->getReadConnection()->query($sql, $params));
        return $result;
    }
   /*public function _timeStamp($zone="UTC"){
    	$tz = date_default_timezone_get();
    	date_default_timezone_set($zone);
    	$tp=date("Y-m-d H:i:s");
    	date_default_timezone_set($tz);
    	return $tp;
    }*/

}