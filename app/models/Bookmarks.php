<?php

use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;


class Bookmarks extends BaseModel
{

	public function initialize(){
		parent::initialize();
	}

	public function getUserBookmarks($userId, $count=10, $offset=0){

		$sql = "SELECT * 
				FROM bookmarks 
				WHERE owner_id = ?";

		$result = new Resultset(null, $this, $this->getReadConnection()->query($sql, array($userId)));
		return $result->toArray();
	}
}