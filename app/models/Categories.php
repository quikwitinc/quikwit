<?php

class Categories extends BaseModel
{
	const STATUS_APPROVED = 1;
	const STATUS_PENDING = 0;
	const STATUS_BANNED = 2;

	public $cat_id;
	public $cat_name;
	public $cat_main_id;

	public function initialize()
	{
		$this->setSource("categories");
		parent::initialize();
	}

	public function findCategoryByName($category){
		return self::find(array("conditions" => "cat_name = :category:", "bind" => array("category" => $category)));
	}
	
	public function findCategoryById($id){
		return self::find(array("conditions" => "cat_id = :id:", "bind" => array("id" => $id)));
	}

}