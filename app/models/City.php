<?php

class City extends BaseModel{

	public $id;
	public $name;

	public function initialize()
	{
		$this->setSource("city");
	}

	public function saveCity($city){
		$this->name = $city;
		$success = $this->save();

		return $success;
	}

	public function findCityById($id){
		$city = self::find(array("conditions" => "id = ?1",
				"bind" => array(1 => $id)));
		return $city->getFirst();
	}

}