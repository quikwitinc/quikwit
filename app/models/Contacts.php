<?php

use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;


class Contacts extends BaseModel
{

	public function initialize(){
		parent::initialize();
        $this->setSource("contacts");
	}

	public function getUserContacts($userId, $count=10, $offset=0){

		/*$sql = "SELECT u.usr_id id, u.usr_firstname firstname, u.usr_lastname lastname, u.usr_avatar avatar, u.lat, u.lng FROM
				(SELECT DISTINCT rid FROM messages WHERE rid=? OR sid=?) AS c, users AS u
				WHERE c.rid=u.usr_id";
		*/
		$sql = "
		SELECT contactObject.id, contactObject.firstname, contactObject.lastname, contactObject.avatar, contactObject.rate, contactObject.about, contactObject.lat, contactObject.lng, contactObject.connectTime, GROUP_CONCAT(tags.name) as `tagSkill`, contactObject.teach FROM (
		SELECT cf.id, cf.firstname, cf.lastname, cf.avatar, cf.rate, cf.lat, cf.lng, cf.connectTime, cf.about, IFNULL(tag_id, 0) as user_id, IFNULL(tag_id, 0) as tag_id, cf.teach from
		(SELECT * from  (
		    SELECT u.usr_id id, u.usr_firstname firstname, u.usr_lastname lastname, u.usr_avatar avatar, u.usr_rate rate, u.usr_about about, u.lat, u.lng, u.usr_deleted, c.created     as  connectTime, u.usr_teach as teach
		    FROM contacts c JOIN users u ON c.contact_id = u.usr_id
		    WHERE c.user_id=" . $userId . "
		    AND u.usr_deleted != 1
		) contactFound LEFT OUTER JOIN `user_tags` ut ON contactFound.id=ut.user_id) cf ) contactObject LEFT OUTER JOIN tags on contactObject.tag_id = tags.id WHERE contactObject.id != " . $userId . "
		group by contactObject.id
		";
		$result = new Resultset(null, $this, $this->getReadConnection()->query($sql, array($userId, $userId)));
		$preprocessResult = $result->toArray();
        $reviewModel = new Reviews();
        $contractModel = new Contracts();
		foreach ($preprocessResult as $key => $value) {
			if ($value['teach'] > 0){
 				$stat = $reviewModel->getStats($value['id'])->toArray();
        		$contractCompleted = $contractModel->getNumCompletedContracts($value['id'])->toArray();
 				$preprocessResult[$key]['overall'] = $stat[0]['overall'];
 				$preprocessResult[$key]['num_contract'] = $contractCompleted[0]['num_contract'];
 				$preprocessResult[$key]['num_review'] = $stat[0]['num_contract'];
			}else{
 				$preprocessResult[$key]['overall'] = 0;
 				$preprocessResult[$key]['num_contract'] = 0;
 				$preprocessResult[$key]['num_review'] = 0;

			}
		}
		return $preprocessResult;
	}

	public static function isContact($userId, $targetId){
        $contact = self::findFirst(['user_id = :user_id: AND contact_id = :contact_id:', 'bind' => ['user_id' => $userId, 'contact_id' => $targetId]]);
		//$sql = "SELECT count(*) as num FROM contacts WHERE " . 'user_id = '.$userId . ' and contact_id = '. $targetId;
		//$result = new Resultset(null, $this, $this->getReadConnection()->query($sql, array()));
		//$num = $result->toArray()[0];
		return $contact;
	}

	public function addContact($userId, $targetId)
    {
        if ($userId !== $targetId) {
            $this->user_id = $userId;
            $this->contact_id  = $targetId;
            $this->created = time();
            return $this->save();
        }
        return false;
	}

    public function deleteContact($userId, $id)
    {
        $contact = $this->findContactByUser($userId, $id);
        if ($contact){
            return $contact->delete();
        } else{
            return false;
        }
    }

    public function findContactByUser($userId, $id){
        $search = self::find(array("conditions" => "user_id = $userId AND contact_id = ?1",
                        "bind" => array(1 => $id)));
        return $search->getFirst();
    }
}