<?php

use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;


class ContractHistory extends BaseModel
{

    const CREATE = 0;
    const USERCHANGEDATE = 1;
    const TEACHERCHANGEDATE = 2;
    const USERACCEPT = 3;
    const TEACHERACCEPT = 4;
    const USERCOMPLETED = 5;
    const TEACHERCOMPLETED = 6;
    const USERCONFIRM = 7;
    const TEACHERCONFIRM = 8;
    const TEACHERDENY = 9;
    const USERDENY = 10;
    const USERREVIEW = 11;
    const TEACHERREVIEW = 14;
    const USERDECLINE = 12;
    const TEACHERDECLINE = 13;
    const CANCEL = 15;

    const OTHERPARTYACCEPT = 1;
    const ALREADYACCEPTED = 2;
    const OTHERPARTYCOMPLETE = 3;
    const ALREADYCOMPLETE = 4;
    public $cnt_id;
    public $cnt_rejected;
    public $cnt_user;
    public $cnt_state;
    public $cnt_unitprice;
    public $cnt_teachid;
    public $cnt_created;
    public $cnt_next;
    public $cnt_started;
    public $cnt_completed;

    public function initialize(){
        parent::initialize();
        $this->setSource("contracthistory");
         $this->belongsTo('cnt_id', 'Contracts', 'cnt_id');
         $this->belongsTo('prompter', 'User', 'usr_id', array(
            'alias'=> 'Prompter'));
         $this->belongsTo('reciever', 'User', 'usr_id', array(
            'alias'=> 'Reciever'));
    }

    public function createNewContract($userId, $teacherId, $contractId, $next){
        $this->prompter = $userId;
        $this->reciever = $teacherId;
        $this->cnt_id = $contractId;
        $this->sub_state = self::USERACCEPT;
        $this->cnt_next = date("Y-m-d H:i", strtotime($next));

        $this->save();

        return $contractId;
    }
    public function updateContractDate($contractId, $userId, $next){
        $contractModel = new Contracts();
        $contract = $contractModel->findContractById($contractId);
        $prompter = $contract->cnt_user;
        $reciever = $contract->cnt_teachid;
        $state = self::USERCHANGEDATE;
        if ($userId == $reciever){
            $temp = $reciever;
            $reciever = $prompter;
            $prompter = $temp;
            $state = self::TEACHERCHANGEDATE;
        }
        $this->prompter = $prompter;
        $this->reciever = $reciever;
        $this->cnt_id = $contractId;
        $this->sub_state = $state;
        $this->cnt_next = $next;
        $this->save();
        return false;
    }
    public function declineContract($contractId, $userId){
        $contractModel = new Contracts();
        $contract = $contractModel->findContractById($contractId);
        $prompter = $contract->cnt_user;
        $reciever = $contract->cnt_teachid;
        $state = self::USERDECLINE;
        if ($userId == $reciever){
            $temp = $reciever;
            $reciever = $prompter;
            $prompter = $temp;
            $state = self::TEACHERDECLINE;
        }
        $this->prompter = $prompter;
        $this->reciever = $reciever;
        $this->cnt_id = $contractId;
        $this->sub_state = $state;
        $this->cnt_next = $contract->cnt_next;
        $this->save();
        return false;
    }

    public function otherPartyAccept($userId, $contractId){
        $contractModel = new Contracts();
        $contract = $contractModel->findContractById($contractId);
        $prompter = $contract->cnt_user;
        $reciever = $contract->cnt_teachid;
        if ($userId == $reciever){
            $temp = $reciever;
            $reciever = $prompter;
            $prompter = $temp;
        }
        if ($this->findContractHistoryOtherParty($contractId, $userId)){
            return self::ALREADYACCEPTED;
        } 
        if ($this->findContractHistoryOtherParty($contractId, $reciever)){
            return self::OTHERPARTYACCEPT;
        }
        return -1;
    }

    public function hasDeny($contractId){
        $contract = self::find(array("conditions" => "cnt_id = ?1 AND (sub_state=" . self::TEACHERDENY . " OR sub_state=" . self::USERDENY . ")",
        "bind" => array(1 => $contractId)));
        return $contract->getFirst();
    }

    public function otherPartyComplete($userId, $contractId){
        $contractModel = new Contracts();
        $contract = $contractModel->findContractById($contractId);
        $prompter = $contract->cnt_user;
        $reciever = $contract->cnt_teachid;
        if ($userId == $reciever){
            $temp = $reciever;
            $reciever = $prompter;
            $prompter = $temp;
        }
        if ($this->findContractHistoryOtherPartyComplete($contractId, $userId)){
            return self::ALREADYCOMPLETE;
        }
        if ($this->findContractHistoryOtherPartyComplete($contractId, $reciever)){
            return self::OTHERPARTYCOMPLETE;
        }
        return -1;
    }

    public function saveParticipantAccept($userId, $contractId){
        $contractModel = new Contracts();
        $contract = $contractModel->findContractById($contractId);
        $prompter = $contract->cnt_user;
        $reciever = $contract->cnt_teachid;
        $state = self::USERACCEPT;
        if ($userId == $reciever){
            $temp = $reciever;
            $reciever = $prompter;
            $prompter = $temp;
            $state = self::TEACHERACCEPT;
        }
        $this->prompter = $prompter;
        $this->reciever = $reciever;
        $this->cnt_id = $contractId;
        $this->sub_state = $state;
        $this->cnt_next = $contract->cnt_next;
        $this->save();
        return false;
    }

    public function saveParticipantReview($userId, $contractId){
        $contractModel = new Contracts();
        $contract = $contractModel->findContractById($contractId);
        $prompter = $contract->cnt_user;
        $reciever = $contract->cnt_teachid;
        $state = self::USERREVIEW;
        if ($userId == $reciever){
            $temp = $reciever;
            $reciever = $prompter;
            $prompter = $temp;
            $state = self::TEACHERREVIEW;
        }
        $this->prompter = $prompter;
        $this->reciever = $reciever;
        $this->cnt_id = $contractId;
        $this->sub_state = $state;
        $this->cnt_next = $contract->cnt_next;
        $this->save();
        return false;
    }

    public function saveParticipantComplete($userId, $contractId){
        $contractModel = new Contracts();
        $contract = $contractModel->findContractById($contractId);
        $prompter = $contract->cnt_user;
        $reciever = $contract->cnt_teachid;
        $state = self::USERCOMPLETED;
        if ($userId == $reciever){
            $temp = $reciever;
            $reciever = $prompter;
            $prompter = $temp;
            $state = self::TEACHERCOMPLETED;
        }
        $this->prompter = $prompter;
        $this->reciever = $reciever;
        $this->cnt_id = $contractId;
        $this->sub_state = $state;
        $this->cnt_next = $contract->cnt_next;
        $this->save();
        return false;
    }
    public function saveParticipantDeny($userId, $contractId){
        $contractModel = new Contracts();
        $contract = $contractModel->findContractById($contractId);
        $prompter = $contract->cnt_user;
        $reciever = $contract->cnt_teachid;
        $state = self::USERDENY;
        if ($userId == $reciever){
            $temp = $reciever;
            $reciever = $prompter;
            $prompter = $temp;
            $state = self::TEACHERDENY;
        }
        $this->prompter = $prompter;
        $this->reciever = $reciever;
        $this->cnt_id = $contractId;
        $this->sub_state = $state;
        $this->cnt_next = $contract->cnt_next;
        $this->save();
        return false;
    }

    public function removeCompletedEntry($contractId) {
        $contract = self::find(array("conditions" => "cnt_id = ?1 AND (sub_state = " . self::USERCOMPLETED . " OR sub_state = " . self::TEACHERCOMPLETED . ")",
                        "bind" => array(1 => $contractId)));
        for ($i = 0; $i < count($contract); $i++){
            $contract[$i]->delete();
        }
    }

    public function findContractHistoryOtherParty($id, $otherParty){
        $contract = self::find(array("conditions" => "cnt_id = ?1 AND prompter = ?2 AND sub_state !=" . self::CREATE,
                        "bind" => array(1 => $id, 2 => $otherParty)));
        return $contract->getFirst();
    }

    public function findContractHistoryOtherPartyComplete($id, $otherParty){
        $contract = self::find(array("conditions" => "cnt_id = ?1 AND prompter = ?2 AND (sub_state = " . self::USERCOMPLETED . " OR sub_state = " . self::TEACHERCOMPLETED . ")",
                        "bind" => array(1 => $id, 2 => $otherParty)));
        return $contract->getFirst();
    }

    public function cancelContract($contractId, $userId){
        $contractModel = new Contracts();
        $contract = $contractModel->findContractById($contractId);
        $prompter = $contract->cnt_user;
        $reciever = $contract->cnt_teachid;
        $state = self::CANCEL;
        if ($userId == $reciever){
            $temp = $reciever;
            $reciever = $prompter;
            $prompter = $temp;
        }
        $this->prompter = $prompter;
        $this->reciever = $reciever;
        $this->cnt_id = $contractId;
        $this->sub_state = $state;
        $this->cnt_next = $contract->cnt_next;
        $this->save();
        return false;
    }
}