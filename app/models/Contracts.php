<?php

use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;


class Contracts extends BaseModel
{

	const PENDING = 0;
	const ACTIVE = 1;
	const COMPLETED = 2;
	const REJECTED = 3;
	const REVIEW = 4;
	const CANCEL = 5;

	public $cnt_id;
	public $cnt_rejected;
	public $cnt_user;
	public $cnt_state;
	public $cnt_unitprice;
	public $cnt_tagid;
	public $cnt_teachid;
	public $cnt_created;
	public $cnt_next;
	public $cnt_started;
	public $cnt_completed;
	public $cnt_requestid;

	public function initialize(){
		parent::initialize();
		$this->setSource("contracts");
		 $this->belongsTo('cnt_user', 'User', 'usr_id');
		 $this->belongsTo('cnt_requestid', 'Requests', 'req_id');
		 $this->belongsTo('cnt_teachid', 'User', 'usr_id',  array(
            'alias'=> 'Teacher'));
		 $this->hasMany('cnt_id', 'ContractHistory', 'cnt_id');
		 $this->hasMany('cnt_id', 'Reviews', 'rvs_contractId');
		 $this->belongsTo('cnt_tagid', 'TagsStandard', 'id');
	}

	public function hasContacted($userId, $targetId){
		//$sql = "SELECT count(*) as num FROM messages WHERE " . '(sid = ' . $userId . ' and rid = '. $targetId . ') OR (sid = '.$targetId . ' and rid = '. $userId . ')';
		$sql = "SELECT count(*) as num FROM contacts WHERE " . 'user_id = '.$userId . ' and contact_id = '. $targetId;
		$result = new Resultset(null, $this, $this->getReadConnection()->query($sql, array()));
		$num = $result->toArray()[0];
		return $num['num'] > 0;
	} 

	public function saveContract($userId, $teachId, $state, $unitPrice, $tagid, $next, $requestid){
		$this->cnt_user = $userId;
		$this->cnt_teachid = $teachId;
		$this->cnt_state = $state;
		$this->cnt_unitprice = $unitPrice;
		$this->cnt_tagid = $tagid;
		$this->cnt_next = date("Y-m-d H:i", strtotime($next));
		$this->cnt_created = date("Y-m-d H:i", time());
		$this->cnt_requestid = $requestid;
		$state = 0;
		$this->save();
		if($this->save()){
		    $requestModel = new Requests();
		    $requestModel->setRequestTaken($requestid);
		}
		return $this->cnt_id;
	}

	public function findContractById($id){
		$contract = self::find(array("conditions" => "cnt_id = ?1",
				        "bind" => array(1 => $id)));
		return $contract->getFirst();
	}

	public function findContractByRequestid($reqId){
		$contract = self::find(array("conditions" => "cnt_requestid = ?1",
				        "bind" => array(1 => $reqId)));
		return $contract->getFirst();
	}

	public function getUserContracts($userId, $count=10, $offset=0, $status){
		$sql = "SELECT *
				FROM contracts
				WHERE cnt_user=? ";
		$result = new Resultset(null, $this, $this->getReadConnection()->query($sql, array($userId)));
		return $result->toArray();
	}

	public function acceptContract($contractId){
		$contract = $this->findContractById($contractId);
		if($contract){
			$contract->cnt_state = self::ACTIVE;
			$contract->cnt_started = date("Y-m-d H:i", time());
			$contract->save();
		}
		return false;
	}

	public function completeContract($contractId){
		$contract = $this->findContractById($contractId);
		if($contract){
			$contract->cnt_state = self::COMPLETED;
			$contract->cnt_completed = date("Y-m-d H:i", time());
			$contract->save();
		}
		return false;
	}

	public function reviewContract($contractId){
		$contract = $this->findContractById($contractId);
		if($contract){
			$contract->cnt_state = self::REVIEW;
			$contract->save();
		}
		return false;
	}

	public function setReject($contractId){
		$contract = $this->findContractById($contractId);
		if($contract){
			//error_log('a'.self::REJECTED);
			//error_log('<pre>'.print_r($contract,true).'</pre>');
			$contract->cnt_state = self::REJECTED;
			$contract->save();

			if($contract->save()){
			    $requestModel = new Requests();
			    $requestModel->setRequestSent($contract->cnt_requestid);
			}
		}
		
	}

	public function updateContractDate($userId, $next, $contractId){
		$contract = $this->findContractById($contractId);
		if(count($contract)===1 && ($contract->cnt_user == $userId || $contract->cnt_teachid == $userId)){
			$contract->cnt_next = $next;
			return $contract->save();
		}
		return false;
	}

    public function getUsers($parameters=null)
    {
        return $this->getRelated('User', $parameters);
    }

    public function getRequests($parameters=null)
    {
        return $this->getRelated('Requests', $parameters);
    }
    
	public function getContracts($userId, $page, $limit, $status){
		$sql = '';
		if ($status == 'pending'){
			$sql = $sql . ' AND cnt_state=' . self::PENDING;
		}
		if ($status == 'completed'){
			$sql = $sql . ' AND cnt_state=' . self::COMPLETED;
		}
		if ($status == 'active'){
			$sql = $sql . ' AND cnt_state=' . self::ACTIVE;
		}
		if ($status == 'rejected'){
			$sql = $sql . ' AND cnt_state=' . self::REJECTED;
		}
		if ($status == 'cancel'){
			$sql = $sql . ' AND cnt_state=' . self::CANCEL;
		}
		if ($status == 'review'){
			$sql = $sql . ' AND cnt_state=' . self::REVIEW;
		}
		/* if ($status == 'cancel'){
			$sql = $sql . ' AND cnt_state!=' . self::COMPLETED . ' AND cnt_state!=' . self::REJECTED;
		} */
		$contracts = self::find(array("conditions" => "(cnt_user = ?1 OR cnt_teachid = ?2 )" . $sql,
				        "bind" => array(1 => $userId, 2 => $userId),
				        "order" => "cnt_created DESC"));
		$paginator = new \Phalcon\Paginator\Adapter\Model(
		    array(
		        "data" => $contracts,
		        "limit"=> $limit,
		        "page" => $page
		    )
		);
		return $paginator->getPaginate();
	}

	public function getNumCompletedContracts($userId){
		$sql = "SELECT count(*) as num_contract
				FROM contracts
				WHERE cnt_teachid=? AND ". 'cnt_state=' . self::COMPLETED;

		return new Resultset(null, $this, $this->getReadConnection()->query($sql, array($userId)));
	}

	public function cancelContract($contractId){
		$contract = $this->findContractById($contractId);
		if($contract){
			$contract->cnt_state = self::CANCEL;
			$contract->cnt_started = date("Y-m-d H:i", time());
			$contract->save();

			if($contract->save()){
			    $requestModel = new Requests();
			    $requestModel->setRequestSent($contract->cnt_requestid);
			}
		}

	}
}