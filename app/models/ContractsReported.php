<?php

class ContractsReported extends BaseModel{

	public $rpt_id;
	public $rpt_user;
	public $rpt_contract;
	public $rpt_created;
	public $rpt_reason;
	public $rpt_text;

	public function initialize()
	{
		$this->setSource("contracts_reported");
	}

	public function saveReport($userId, $contractId, $reason, $text){
		$this->rpt_user = $userId;
		$this->rpt_contract = $contractId;
		$this->rpt_reason= $reason;
		$this->rpt_text = $text;
		$this->rpt_created = date("Y-m-d H:i", time());
		$this->save();

		return false;
	}

	public function findContractsReportedById($id){
		$contract = self::find(array("conditions" => "rpt_id = ?1",
				"bind" => array(1 => $id)));
		return $contract->getFirst();
	}

	public function retrieveContractsReported(){
		$sql = 'SELECT * FROM contracts_reported ORDER BY rpt_created DESC';

		$params = NULL;

		$result = $this->getResultSet($sql, $params)->toArray();
		return $result;
	}

}