<?php
require_once __DIR__ . '/../../vendor/swiftmailer/swiftmailer/lib/swift_required.php';
use \Phalcon\Tag,
    Component\Json;
class EmailConfirmations extends BaseModel
{

    const VERIFIED = 1;
    const NOT_VERIFIED = 0;

    public $id;
    public $usersId;
    public $code;
    public $createdAt;
    public $modifiedAt;
    public $confirmed;

	public function initialize(){
		$this->belongsTo('usersId', 'User', 'usr_id', array(
            'alias' => 'user'
        ));
	}

	public function beforeValidationOnCreate(){
		//Timestamp the confirmaton
        $this->createdAt = time();
        //Generate a random confirmation code
        $this->code = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(24)));
        //Set status to non-confirmed
        $this->confirmed = self::NOT_VERIFIED;
	}

	public function beforeValidationOnUpdate(){
		//Timestamp the confirmaton
        $this->modifiedAt = time();
	}

	public function afterCreate(){
		$request = new Phalcon\Http\Request();
		
		// get data from config
        $contacts = $this->getDI()->get('config')->get('contacts')->get('contact-us');
        $gateway = $this->getDI()->get('config')->get('gmail-gateway');
        $subject = "Please confirm your email with QuikWit Inc.";
		$name = 'activation';
		$params = array();
		
		// Create transport using smtp credential
        $transport = Swift_SmtpTransport::newInstance($gateway->host, $gateway->port, $gateway->security);
		$transport->setUsername($gateway->username);
		$transport->setPassword($gateway->password); 
		
		// Create the message
		$message = Swift_Message::newInstance();
		$message->setTo(array(
		  $request->getPost("email")
		));
		$message->setSubject($subject);
		$message->setBody($this->getTemplate($name, $params, $request->getPost("email")),'text/html');
		$message->setFrom($contacts->from, $contacts->name);
		 
		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		$mailer->send($message);   
		return true;     
    }
    /**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplate($name, $params, $email)
	{
		$userModel = new User();
		$user = $userModel->findUserByEmail($email);
		$userfirst = $user->usr_firstname;
		$userlast = $user->usr_lastname;
		if($user->usr_password == null && $user->usr_about != null) {
			$name = 'activationNoPass';
		}
		$view = new \Phalcon\Mvc\View();
 		$view->setViewsDir('app/views/');
 		$view->start();
 		$content = $view->getRender('templates-email', $name,
		 				array('confirmUrl' => $this->getDI()->get('config')->get('webURL').'/register/confirmEmail?code=' . $this->code . '&email=' . $email, 'firstname' => $userfirst, 'lastname' => $userlast)
		 			);
 		$view->finish();
		return $content;
	}

}

?>