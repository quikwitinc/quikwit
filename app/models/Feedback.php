<?php

class Feedback extends BaseModel{

	public $feed_id;
	public $user_id;
	public $feedback;
	public $created;

	public function initialize()
	{
		$this->setSource("feedback");
	}

	public function saveFeedback($userId, $feedback){
		$this->user_id = $userId;
		$this->feedback = $feedback;
		$this->created = date("Y-m-d H:i", time());
		$success = $this->save();

		return $success;
	}

	public function findFeedbackById($id){
		$abuse = self::find(array("conditions" => "feed_id = ?1",
				"bind" => array(1 => $id)));
		return $feedback->getFirst();
	}

	public function retrieveFeedback(){
		$sql = 'SELECT * FROM feedback ORDER BY created DESC';

		$params = NULL;

		$result = $this->getResultSet($sql, $params)->toArray();
		return $result;
	}

}