<?php

class Languages extends BaseModel
{
	const STATUS_APPROVED = 1;
	const STATUS_DISABLED= 0;

	public $id;
	public $name;
	public $status;

	public function initialize()
	{
		$this->setSource("languages");
		parent::initialize();
	}

	public function add($name){
		$lang = $this->findLangByName($name);
		if($lang->count()===0){
			$lang = new Languages();
			$lang->name = $name;
			$lang->status = self::STATUS_APPROVED; /* Temporarily set to APPROVED before implementing TAG APPROVAL SYSTEM */
			return $lang->save()? $lang : false;
		}
		return $lang;
	}

	public function updateLang($id, $lang){
		$olang = $this->findLangById($id);
		if($olang){
			$olang->name = $lang;
			$olang->status = STATUS_APPROVED; /* Temporarily set to APPROVED before implementing TAG APPROVAL SYSTEM */
			$this->save();
		}
		return false;
	}

	public function findLangByName($lang){
		return self::find(array("conditions" => "name = :lang:", "bind" => array("lang" => $lang)));
	}

	public function findLangById($id){
		return self::find(array("conditions" => "id = :id:", "bind" => array("id" => $id)));
	}

}