<?php

use \Phalcon\Mvc\Model\Validator,
	\Phalcon\DI;

use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;


class Media extends BaseModel
{
    const DELETED = 1;
    const NOT_DELETED = 0;

    public $mid_id;
    public $mid_user;
    public $mid_type;
    public $mid_name;
    public $mid_url;
    public $mid_content;
    public $mid_created;


    public function initialize()
    {
        $this->setSource("media");
        parent::initialize();
    }

    public function addMedia($user, $title, $tags, $name, $mediaFile, $type, $content){
    	$this->mid_user = $user;
        $this->mid_type = $type;
        $this->mid_name = $title;
        $this->mid_url = $mediaFile;
        $this->mid_content = $content;        
        $this->mid_created = self::getTimeStamp();
        $this->save();
        $media = $this->mid_id;
        foreach ($tags as $tag) {
        	$t = new MediaTags();
			$t->media_id = $media;
			$t->tag_id = $tag;
            $t->created = self::getTimeStamp();
			$t->save();
        }
        return true;
    }

    public function getUserMedia($userId){
    	$sql = "SELECT *
				FROM media
				WHERE mid_user=?";

   		return new Resultset(null, $this, $this->getReadConnection()->query($sql, array($userId)));
    }

    public function getUrl($mediaId){
    	$media = $self::find(array("condition" => "mid_id = ?1",
    			"bind" => array(1 => $mediaId)));
    	return $media->getFirst();
    }

    public function mediaExists($title){
        $users = self::query()
        ->where("mid_url = :url:")
        ->bind(array("url" => $title))
        ->execute();
        return count($medias) > 0;
    }

    public function validation()
    {
        $this->validate(new Validator\Uniqueness([
            'field' => 'mid_url',
            'message' => 'This url has already been used.'
        ]));

//        $this->validate(new Validator\StringLength([
//            'field' => 'mid_title',
//            'max' => '50',
//            'min' => '10',
//            'messageMaximum' => 'Your title must be under 50 characters.',
//            'messageMinimum' => 'Your title must be atleast 4 characters.'
//        ]));

        if($this->validationHasFailed()){
            return false;
        }

    }

    public function getNumMediaByUser($userId){
        $sql = "SELECT count(*) as num_media
                FROM media
                WHERE mid_user=?";
        return new Resultset(null, $this, $this->getReadConnection()->query($sql, array($userId)));
    }
    
    public function updateMedia($id, $title, $tags, $name, $mediaFile) {
    	$user = self::findMediaByMidUser($id);
		if(count($user)===1){
	        $user->mid_name = $title;
	        if($mediaFile!='') {
	        	$user->mid_url = $mediaFile;
	        }
			$user->save();
        	$t = new UserTags();
			$t->deleteAllTags($id);
			foreach ($tags as $tag) {
        		$t = new UserTags();
				$t->user_id = $id;
				$t->tag_id = $tag;
				$t->save();
	        }
		}else{
			return false;
		}
    }
    
    public static function findMediaByMidUser($id)
    {
		$user = self::find(array("conditions" => "mid_user = ?1",
				"bind" => array(1 => $id)));
		return $user->getFirst();
	}

    public function deleteMedia($id){
        $status = self::find(["mid_id=".$id])->delete();        
        $t = new MediaTags();
        $statusMedia = $t->deleteMediaTags($id); 
        return $status;
    }
}