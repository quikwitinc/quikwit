<?php

use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;


class MediaTags extends BaseModel
{
	protected $media_id;
	protected $tag_id;
	protected $created;

	public function initialize()
	{
		$this->setSource("media_tags");
		parent::initialize();
		$this->belongsTo('media_id', 'Media', 'mid_id');
		$this->belongsTo('tag_id', 'TagsStandard', 'id');
	}

	public function add($mediaId, $tagId){
		$mediaTags = new MediaTags();
		$mediaTags->media_id = $mediaId;
		$mediaTags->tag_id = $tagId;
		$mediaTags->created = time();
		return $mediaTags->save();
	}

	public function deleteTag($mediaId, $tagId){
		$condition = "media_id=?1 AND tag_id=?2";
		$record = self::find([$condition, "bind" => array(1=> $mediaId, 2 => $tagId)]);
		$count = $record->count();
		if($record->count()===1){
			return $record->delete();
		}
		return false;
	}

	public function deleteAllTags($mediaId){
		return self::find(["media_id=".$mediaId])->delete();
	}

	public function deleteMediaTags($mediaId){
		return self::find(["media_id=".$mediaId])->delete();
	}
}