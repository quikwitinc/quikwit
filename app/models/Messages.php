<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Messages extends BaseModel
{

    const MESSAGE_TYPE_ANNOUNCEMENT = 1;
    const MESSAGE_TYPE_CONTACT = 2;
    const MESSAGE_TYPE_NOTIFICATION = 3;
    const MESSAGE_PER_PAGE = 20;

    const STATUS_DELETED = -1;
    const STATUS_DEFAULT = 1;

    protected $id;
    protected $type;
    protected $title;
    protected $body;
    protected $status;
    protected $created;
    protected $created_by;
    protected $ref_id;
    protected $request_id;

    public function initialize()
    {
        parent::initialize();
        $this->setSource("messages");
        //$this->hasMany('id', 'messages_contact', 'message_id');
    }

    public function getId()
    {
        return $this->id;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setCreated()
    {
        $this->created = self::getTimeStamp();
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
        return $this;
    }

    public function getCreatedBy()
    {
        return $this->created_by;
    }

    public function setRequestId($requestId)
    {
        $this->request_id = $requestId;
        return $this;
    }

    public function getRequestId()
    {
        return $this->request_id;
    }

    public function setRefId($refId)
    {
        $this->ref_id = $refId;
        return $this;
    }

    public function getRefId()
    {
        return $this->ref_id;
    }

    public function generateRefId()
    {
        $key = '';
        $id = $this->getId();
        if (!empty($id)) {
            $key = $id . '_' . $this->getTitle() . '_' . $this->getBody() . ' ' . $this->getType() . '_' . $this->getCreated() . '_' . $this->getCreatedBy();
        } else {
            $key = microtime();
        }
        return hash('crc32', $key);
    }

    public function saveMessage($senderId, $receiverId, $title, $message, $refId = null, $type, $requestId)
    {
        $this->setTitle($title);
        $this->setBody($message);
        $this->setType($type);
        $this->setStatus(self::STATUS_DEFAULT);
        $this->setCreated();
        $this->setCreatedBy($senderId);
        $this->setRequestId($requestId);

        if ($this->save()) {
            if (!$refId) {
                $refId = $this->generateRefId();
            }
            $this->setRefId($refId);
            if ($this->save()) {
                switch($type) {
                    case self::MESSAGE_TYPE_CONTACT:
                        return  (new MessagesContact())->addMessage($this->getId(), $senderId, $receiverId);
                        break;
                    //case self::MESSAGE_TYPE_ANNOUNCEMENT:
                        //return  (new MessagesSystem())->addMessage($this->getId(), $senderId, $receiverId);
                        //break;                        
                    case self::MESSAGE_TYPE_NOTIFICATION:
                        return  (new MessagesNotification())->addMessage($this->getId(), $senderId, $receiverId);
                        break;
                }
            }
        }

        $success = $this->save();
        return $success;
    }

    public static function setMessageRead($messageId, $userId)
    {
        $message = self::getMessageById($messageId);
        if ($message) {
            switch ($message->getType())
            {
                case self::MESSAGE_TYPE_CONTACT:
                    return MessagesContact::updateReadStatusByIds($messageId, $userId);
                    break;
                case self::MESSAGE_TYPE_ANNOUNCEMENT:
                    return MessagesSystem::updateReadStatusByIds($messageId, $userId);
                    break;
                case self::MESSAGE_TYPE_NOTIFICATION:
                    return MessagesNotification::updateReadStatusByIds($messageId, $userId);
                    break;
            }
        }
        return false;
    }

    public static function getUnreadCount($userId, $type = self::MESSAGE_TYPE_CONTACT)
    {
        switch($type) {
            case self::MESSAGE_TYPE_CONTACT:
                $count = MessagesContact::getUnreadMessageCount($userId);
                break;
            case self::MESSAGE_TYPE_NOTIFICATION:
                $count = MessagesNotification::getUnreadMessageCount($userId);
                break;
            case self::MESSAGE_TYPE_ANNOUNCEMENT:
                $count = MessagesSystem::getUnreadMessageCount($userId);
                break;
        }
        //error_log("<pre>count" . print_r($count,true) . "</pre>");
        return $count;
    }

    public static function getMessageCount($userId, $type = self::MESSAGE_TYPE_CONTACT){
        switch($type) {
            case self::MESSAGE_TYPE_CONTACT:
                $messages = MessagesContact::getUserMessageCount($userId);
                break;
            case self::MESSAGE_TYPE_NOTIFICATION:
                $messages = MessagesNotification::getUserMessageCount($userId);
                break;
            case self::MESSAGE_TYPE_ANNOUNCEMENT:
                $messages = MessagesSystem::getUserMessageCount($userId);
                break;
        }
        //error_log("<pre>messages" . print_r($messages,true) . "</pre>");
        return $messages;
    }

    public static function getTotalUnreadCount($userId)
    {
        $count1 = MessagesContact::getUnreadMessageCount($userId);
        $count2 = MessagesNotification::getUnreadMessageCount($userId);
        $count3 = MessagesSystem::getUnreadMessageCount($userId);

        $unreadCount = $count1 + $count2 + $count3;

        return $unreadCount;
    }

    public static function getUserContactMessagesFromMessageIds($userId, $messageIds)
    {
        $obj = new self;

        if ($messageIds) {
            if (!is_array($messageIds)) {
                $messageIds = [$messageIds];
            }

            $sql = 'SELECT m.*, mc.target_id,
                    IF (mc.user_type = :user_type_sender, u1.usr_id, u2.usr_id) as user_id,
                    IF (mc.user_type = :user_type_sender, u1.usr_firstname, u2.usr_firstname) as user_firstname,
                    IF (mc.user_type = :user_type_sender, u1.usr_lastname, u2.usr_lastname) as user_lastname,
                    IF (mc.user_type = :user_type_sender, u1.usr_avatar, u2.usr_avatar) as user_avatar,
                    r.req_userid as request_userid
    				FROM messages m
    				LEFT JOIN messages_contact mc on m.id = mc.message_id
    				LEFT JOIN users u1 ON mc.user_id = u1.usr_id
    			    LEFT JOIN users u2 ON mc.target_id = u2.usr_id
                    LEFT JOIN requests r ON m.request_id = r.req_id
                    WHERE m.id = mc.message_id
                      AND mc.user_id = :user_id
                      AND m.id IN (' . implode($messageIds, ',') . ')
                      AND m.status != :status_deleted
                      AND mc.status != :mc_status_deleted
                    ORDER BY m.created DESC';

            $params = ['user_type_sender' => MessagesContact::USER_TYPE_SENDER, 'user_id' => $userId, 'status_deleted' => self::STATUS_DELETED, 'mc_status_deleted' => MessagesContact::STATUS_DELETED];
            $result = $obj->getResultSet($sql, $params)->toArray();
            return $result;
        }
        return false;
    }

    public static function getUserNotificationsFromMessageIds($userId, $messageIds)
    {
        $obj = new self;

        if ($messageIds) {
            if (!is_array($messageIds)) {
                $messageIds = [$messageIds];
            }

            $sql = 'SELECT m.*, mn.target_id
                    FROM messages m
                    LEFT JOIN messages_notification mn on m.id = mn.message_id
                    WHERE m.id = mn.message_id
                      AND mn.target_id = :user_id
                      AND m.id IN (' . implode($messageIds, ',') . ')
                      AND m.status != :status_deleted
                      AND mn.status != :mn_status_deleted
                    ORDER BY m.created DESC';

            $params = ['user_id' => $userId, 'status_deleted' => self::STATUS_DELETED, 'mn_status_deleted' => MessagesContact::STATUS_DELETED];
            $result = $obj->getResultSet($sql, $params)->toArray();
            return $result;
        }
        return false;
    }

    public static function getUserAnnouncementsFromMessageIds($userId, $messageIds)
    {
        $obj = new self;

        if ($messageIds) {
            if (!is_array($messageIds)) {
                $messageIds = [$messageIds];
            }

            $sql = 'SELECT m.*, ms.target_id
                    FROM messages m
                    LEFT JOIN messages_system ms on m.id = ms.message_id
                    WHERE m.id = ms.message_id
                      AND ms.target_id = :user_id
                      AND m.id IN (' . implode($messageIds, ',') . ')
                      AND m.status != :status_deleted
                      AND ms.status != :ms_status_deleted
                    ORDER BY m.created DESC';

            $params = ['user_id' => $userId, 'status_deleted' => self::STATUS_DELETED, 'ms_status_deleted' => MessagesSystem::STATUS_DELETED];
            $result = $obj->getResultSet($sql, $params)->toArray();
            return $result;
        }
        return false;
    }

    public static function getTargetedContactMessageIds($refId)
    {
        $obj = new self;
        $sql = 'SELECT id FROM messages WHERE ref_id = :ref_id AND status != :message_deleted AND type = :message_type';
        $params = ['ref_id' => $refId, 'message_deleted' => self::STATUS_DELETED, 'message_type' => self::MESSAGE_TYPE_CONTACT];
        $result = $obj->getResultSet($sql, $params)->toArray();
        return array_column($result, 'id');
    }

    public function getUserContactMessages($userId, $offset = 0, $count = self::MESSAGE_PER_PAGE)
    {
        $sql = 'SELECT *, mc.modified, IF(mc.read_on IS NULL, 1, 0) AS message_count, u.usr_id as user_id, u.usr_firstname as user_firstname, u.usr_lastname as user_lastname, u.usr_avatar as user_avatar
                FROM (SELECT * FROM messages ORDER BY created DESC) As mf
                LEFT JOIN messages_contact mc on mf.id = mc.message_id
                LEFT JOIN users u ON mc.target_id = u.usr_id
                WHERE mf.status != :deleted_status
                  AND mf.type = :type_contact
                  AND mc.user_id = :user_id 
                  AND mf.created_by != :user_id
                GROUP BY ref_id
                ORDER BY created DESC';

        if (is_numeric($count)) {
            $sql .= ' LIMIT ' . $count;
            if (is_numeric($offset)) {
                $sql .= ' OFFSET ' . $offset;
            }
        }

        $params = ['deleted_status' => self::STATUS_DELETED, 'type_contact' => self::MESSAGE_TYPE_CONTACT, 'user_id' => $userId];
        $result = $this->getResultSet($sql, $params)->toArray();

        //error_log("<pre>result".print_r($result,true)."</pre>");
        return $result;
    }

    public static function getTargetedNotificationIds($refId)
    {
        $obj = new self;
        $sql = 'SELECT id FROM messages WHERE ref_id = :ref_id AND status != :message_deleted AND type = :message_type';
        $params = ['ref_id' => $refId, 'message_deleted' => self::STATUS_DELETED, 'message_type' => self::MESSAGE_TYPE_NOTIFICATION];
        $result = $obj->getResultSet($sql, $params)->toArray();
        return array_column($result, 'id');
    }

    public function getUserNotifications ($userId, $offset = 0, $count = self::MESSAGE_PER_PAGE)
    {

        $sql = 'SELECT m.*, mn.modified, SUM(IF(mn.read_on IS NULL, 1, 0)) AS message_count, u.usr_id as user_id, u.usr_firstname as user_firstname, u.usr_lastname as user_lastname, u.usr_avatar as user_avatar
                FROM messages m
                LEFT JOIN messages_notification mn on m.id = mn.message_id
                LEFT JOIN users u ON mn.target_id = u.usr_id
                WHERE m.status != :deleted_status
                  AND m.type = :type_notification
                  AND mn.target_id = :user_id
                GROUP BY ref_id
                ORDER BY created DESC';

        if (is_numeric($count)) {
            $sql .= ' LIMIT ' . $count;
            if (is_numeric($offset)) {
                $sql .= ' OFFSET ' . $offset;
            }
        }

        $params = ['deleted_status' => self::STATUS_DELETED, 'type_notification' => self::MESSAGE_TYPE_NOTIFICATION, 'user_id' => $userId];
        $result = $this->getResultSet($sql, $params)->toArray();
        return $result;
        $messageIds = MessagesNotification::getUserMessageIds($userId, $offset, $count);
        return $this->getUserNotificationsFromMessageIds($userId, $messageIds);
    }

    public static function getTargetedAnnouncementIds($refId)
    {
        $obj = new self;
        $sql = 'SELECT id FROM messages WHERE ref_id = :ref_id AND status != :message_deleted AND type = :message_type';
        $params = ['ref_id' => $refId, 'message_deleted' => self::STATUS_DELETED, 'message_type' => self::MESSAGE_TYPE_ANNOUNCEMENT];
        $result = $obj->getResultSet($sql, $params)->toArray();
        return array_column($result, 'id');
    }

    public function getUserAnnouncements ($userId, $offset = 0, $count = self::MESSAGE_PER_PAGE)
    {

        $sql = 'SELECT m.*, ms.modified, SUM(IF(ms.read_on IS NULL, 1, 0)) AS message_count, u.usr_id as user_id, u.usr_firstname as user_firstname, u.usr_lastname as user_lastname, u.usr_avatar as user_avatar
                FROM messages m
                LEFT JOIN messages_system ms on m.id = ms.message_id
                LEFT JOIN users u ON ms.target_id = u.usr_id
                WHERE m.status != :deleted_status
                  AND m.type = :type_announcement
                  AND ms.target_id = :user_id
                GROUP BY ref_id
                ORDER BY created DESC';

        if (is_numeric($count)) {
            $sql .= ' LIMIT ' . $count;
            if (is_numeric($offset)) {
                $sql .= ' OFFSET ' . $offset;
            }
        }

        $params = ['deleted_status' => self::STATUS_DELETED, 'type_announcement' => self::MESSAGE_TYPE_ANNOUNCEMENT, 'user_id' => $userId];
        $result = $this->getResultSet($sql, $params)->toArray();
        return $result;
        $messageIds = MessagesSystem::getUserMessageIds($userId, $offset, $count);
        return $this->getUserAnnouncementsFromMessageIds($userId, $messageIds);
    }

    public function getUserMessages($userId, $type = self::MESSAGE_TYPE_CONTACT, $offset = 0, $count = self::MESSAGE_PER_PAGE)
    {
        $messageIds = null;
        switch ($type) {
            case self::MESSAGE_TYPE_CONTACT:
                return $this->getUserContactMessages($userId, $offset, $count);
                break;
            case self::MESSAGE_TYPE_NOTIFICATION:
                return $this->getUserNotifications($userId, $offset, $count);
                break;
        }
        return false;
    }

    public function getNewMessages($userId, $refId=0, $offset=0, $count=null)
    {
        //$msg= self::find(array("conditions" => "rid = ?1 AND isRead = ?2 AND id > ?3",
        //"bind" => array(1 => $userId, 2 => self::NOT_READ, 3 => $refId)));
        return $this->getUserMessages($userId, $refId, $offset, $count);

    }

    public static function hasAccess($id, $userId)
    {
        $message = self::getMessageById($id);
        if ($message) {
            switch ($message->getType()) {
                case self::MESSAGE_TYPE_CONTACT:
                case self::MESSAGE_TYPE_NOTIFICATION:
                    return MessagesContact::hasAccessToMessage($id, $userId);
                    break;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @return  Messages
     */
    public static function getMessageById($id)
    {
        $message = self::findFirst(["id = :id: AND status != :status:", "bind" => ["id" => $id, "status" => self::STATUS_DELETED]]);
        return $message;
    }

    public function deleteMessageById($id){
        $message = $this->getMessageById($id);
        if($message){
            $message->status = self::STATUS_DELETED;
            $message->save();
        }
        return false;
    }

    public function checkIfUserReplyRequest($requestId, $userId){
        $requestIdMessage = $this->findFirst(["request_id = :rid: AND created_by = :uid:", "bind" => ["rid" => $requestId, "uid" => $userId]]);
        if($requestIdMessage){
            return true;
        }else{
            return false;
        }
    }


}

if (! function_exists('array_column')) {
    function array_column(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if ( ! isset($value[$columnKey])) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( ! isset($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }
}