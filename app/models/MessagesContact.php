<?php 

use Phalcon\Mvc\Model\Behavior\Timestampable;

class MessagesContact extends BaseModel{
	
	const STATUS_READ = 1;
	const STATUS_NOT_READ = 0;
    const STATUS_SENT = 2;
    const STATUS_DELETED = -1;

    const USER_TYPE_SENDER = 1;
    const USER_TYPE_RECEIVER = 2;
	
	protected $id;
    protected $message_id;
    protected $user_id;
    protected $user_type;
    protected $target_id;
    protected $read_on;
    protected $status;
    protected $modified;

    public function getId()
    {
        return $this->id;
    }

    public function setMessageId($messageId)
    {
        $this->message_id = $messageId;
        return $this;
    }

    public function getMessageId()
    {
        return $this->message_id;
    }

    public function setUserId($userId)
    {
        $this->user_id = $userId;
        return $this;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserType($userType)
    {
        $this->user_type = $userType;
        return $this;
    }

    public function getUserType()
    {
        return $this->user_type;
    }

    public function setTargetId($targetId)
    {
        $this->target_id = $targetId;
        return $this;
    }

    public function getTargetId()
    {
        return $this->target_id;
    }

    public function setReadOn($readOn = null)
    {
        $readOn = $readOn ? $readOn : self::getTimeStamp();
        $this->read_on = $readOn;
        return $this;
    }

    public function getReadOn()
    {
        return $this->read_on;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setModified($modified = null)
    {
        $modified = $modified ? $modified : self::getTimeStamp();
        $this->modified = $modified;
        return $this;
    }

    public function getModified()
    {
        return $this->modified;
    }

	public function initialize()
	{
		$this->setSource('messages_contact');
        $this->belongsTo('message_id', 'messages', 'id');
        $this->belongsTo('user_id', 'users', 'usr_id');
        $this->belongsTo('target_id', 'users', 'usr_id');
	}

    public function addMessage($messageId, $senderId, $receiverId)
    {
        return self::addSender($messageId, $senderId, $receiverId) && self::addReceiver($messageId, $receiverId, $senderId);
    }

    public function updateReadStatus($readOn = null)
    {
        $this->setReadOn($readOn);
        $this->setStatus(self::STATUS_READ);
        $this->setModified();
        return $this->save();
    }

    protected static function addSender($messageId, $senderId, $receiverId)
    {
        $message = new self;
        $message->setMessageId($messageId);
        $message->setUserId($senderId);
        $message->setTargetId($receiverId);
        $message->setUserType(self::USER_TYPE_SENDER);
        $message->setStatus(self::STATUS_SENT);
        $message->setModified();
        return $message->save();
    }

    protected static function addReceiver($messageId, $receiverId, $senderId)
    {
        $message = new self;
        $message->setMessageId($messageId);
        $message->setUserId($receiverId);
        $message->setTargetId($senderId);
        $message->setUserType(self::USER_TYPE_RECEIVER);
        $message->setStatus(self::STATUS_NOT_READ);
        $message->setModified();
        return $message->save();
    }

    /**
     * @param $messageId
     * @param $userId
     * @return MessagesContact
     */
    public static function getMessageByIds($messageId, $userId)
    {
        return self::findFirst(['message_id = :message_id: AND user_id = :user_id:', 'bind' => ['message_id' => $messageId, 'user_id' => $userId]]);
    }

    public static function getUserMessagesByType($userId, $type)
    {
        $result = self::find(['user_type = :type: AND user_id = :user_id: AND status != :status:', 'bind' => ['type' => $type, 'user_id' => $userId, 'status' => self::STATUS_DELETED]]);
        if ($result) {
            $result = array_column($result->toArray(), 'id');
        }
        $resultSet = self::find(['user_type = :type: AND user_id = :user_id: AND status != :status:', 'bind' => ['type' => $type, 'user_id' => $userId, 'status' => self::STATUS_DELETED]]);
    }

    public static function updateReadStatusByIds($messageId, $userId)
    {
        if ($message = self::getMessageByIds($messageId, $userId)) {
            return $message->updateReadStatus();
        }
        return false;
    }

    public static function getUnreadMessages($userId)
    {
        return self::find(self::getUserUnreadMessageParams($userId));
    }

    public static function getUnreadMessageCount($userId)
    {
        return self::count(self::getUserUnreadMessageParams($userId));
    }

    public static function getUserMessageIds($userId, $offset = 0, $count = Messages::MESSAGE_PER_PAGE)
    {
        $resultSet = self::find(self::getUserMessageParams($userId, $count, $offset));
        if ($resultSet->count() > 0) {
            $resultSet = $resultSet->toArray();
            return array_unique(array_column($resultSet, 'message_id'));
        }
        return null;
    }

    public static function getUserMessageCount($userId)
    {
        return self::count(self::getUserMessageParams($userId));
    }

    public static function hasAccessToMessage($messageId, $userId)
    {
        $message = self::getMessageByIds($messageId, $userId);
        return  $message !== false;
    }

    protected static function getUserUnreadMessageParams($userId)
    {
        return ['user_id = :user_id: AND user_type = :user_type: AND status = :status_not_read: AND status != :status_deleted:',
            'bind' => ['user_id' => $userId, 'user_type' => self::USER_TYPE_RECEIVER, 'status_not_read' => self::STATUS_NOT_READ, 'status_deleted' => self::STATUS_DELETED]
        ];
    }

    protected static function getUserMessageParams($userId, $limit = null, $offset = null)
    {
        $param = ['user_id = :user_id: AND user_type = :user_type: AND status != :status:',
            'bind' => ['user_id' => $userId, 'user_type' => self::USER_TYPE_RECEIVER, 'status' => self::STATUS_DELETED]
        ];

        if (!is_null($limit)) {
            $limitParam = [];
            $limitParam['number'] = $limit;
            if (!is_null($offset)) {
                $limitParam['offset'] = $offset;
            }
            $param['limit'] = $limitParam;
        }
        return $param;
    }
}