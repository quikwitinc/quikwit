<?php

class Newsletters extends BaseModel{
	
	const STATUS_SUBSCRIBED = 1;
	const STATUS_UNSUBSCRIBED = 0;
	
	protected $id;
	protected $name;
	protected $email;
	protected $status;
	protected $created;
	protected $modified;
	

	public function initialize()
	{
		$this->setSource("newsletters");
		parent::initialize();
	}

	public function addSubscription($email){
		$subscription = $this->subscriptionExistsByEmail($email);
		if(!$subscription){
			//$this->name = $name;
			$this->email = $email;
			$tp = time();
			$this->created = $tp;
			$this->modified = $tp;
			$this->status = self::STATUS_SUBSCRIBED;
			return $this->save();
		}else{
			//var_dump($subscription);
			return $this->updateSubscription($subscription->id, $name, $email);
		}
	}
	
	public function updateSubscription($id, $name, $email){
		$subscription = $this->getSubscriptionById($id);
		if($subscription){
			$subscription->name = $name? $name : $user->name;
			$subscription->email = $email? $email : $user->email;
			$subscription->modified = ($name || $email) ? time() : $subscription->modified;
			return $subscription->save();
		}else{
			return $this->addSubscription($name, $email);
		}
	}
	
	public function subscriptionExistsByEmail($email){
		$subscription = self::findFirst(array("conditions" => "email = :email:", "bind" => array("email" => $email)));
		return $subscription;
	}
	
	public function getSubscriptionById($id){
		$subscription = self::findFirst(array("conditions" => "id = :id:", "bind" => array("id" => $id)));
		return $subscription;
	}
	
	

}