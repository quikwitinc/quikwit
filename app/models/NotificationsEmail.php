<?php
require_once __DIR__ . '/../../vendor/swiftmailer/swiftmailer/lib/swift_required.php';
use
\Phalcon\Mvc\Model\Validator,
\Phalcon\Security,
\Phalcon\DI;

class NotificationsEmail extends BaseModel
{

    public function initialize()
    {
        parent::initialize();
    }

    /**
	 * Function use to send notification email
	 *
	 * @param string $email
	 */    
    public function sendConfirmationBooking($email, $teacherFirst, $clientFirst, $clientLast, $id, $skill){		
		// get data from config
        $contacts = $this->getDI()->get('config')->get('contacts')->get('contact-us');
        $gateway = $this->getDI()->get('config')->get('gmail-gateway');
        $subject = "Confirmation of Booking for Contract";
		$name = 'confirmationBooking';

		$hashids = new Hashids\Hashids(null, 8, null);
		$id = $hashids->encode($id);
		
		// Create transport using smtp credential
        $transport = Swift_SmtpTransport::newInstance($gateway->host, $gateway->port, $gateway->security);
		$transport->setUsername($gateway->username);
		$transport->setPassword($gateway->password); 
		
		// Create the message
		$message = Swift_Message::newInstance();
		$message->setTo($email);
		$message->setSubject($subject);
		$message->setBody($this->getTemplateConfirmBooking($name, $teacherFirst, $clientFirst, $clientLast, $id, $skill),'text/html');
		$message->setFrom($contacts->from, $contacts->name);
		 
		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		$mailer->send($message);   
		return true;     
    }
    
    /**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplateConfirmBooking($name, $teacherFirst, $clientFirst, $clientLast, $id, $skill)
	{
		$view = new \Phalcon\Mvc\View();
 		$view->setViewsDir('app/views/');
 		$view->start();
 		$content = $view->getRender('templates-email', $name, array('teacherFirst' => $teacherFirst,
		 					  'clientFirst' => $clientFirst, 'clientLast' => $clientLast, 'id' => $id, 'skill' => $skill));
 		$view->finish();
		return $content;
	}

    /**
	 * Function use to send notification email
	 *
	 * @param string $email
	 */    
    public function sendTimeChange($email, $userFirst, $receiverFirst, $userLast, $id, $skill){		
		// get data from config
        $contacts = $this->getDI()->get('config')->get('contacts')->get('contact-us');
        $gateway = $this->getDI()->get('config')->get('gmail-gateway');
        $subject = "Date/Time Changed for Contract";
		$name = 'timeChange';

		$hashids = new Hashids\Hashids(null, 8, null);
		$id = $hashids->encode($id);
		
		// Create transport using smtp credential
        $transport = Swift_SmtpTransport::newInstance($gateway->host, $gateway->port, $gateway->security);
		$transport->setUsername($gateway->username);
		$transport->setPassword($gateway->password); 
		
		// Create the message
		$message = Swift_Message::newInstance();
		$message->setTo($email);
		$message->setSubject($subject);
		$message->setBody($this->getTemplateTimeChange($name, $userFirst, $receiverFirst, $userLast, $id, $skill),'text/html');
		$message->setFrom($contacts->from, $contacts->name);
		 
		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		$mailer->send($message);   
		return true;     
    }
    
    /**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplateTimeChange($name, $userFirst, $receiverFirst, $userLast, $id, $skill)
	{
		$view = new \Phalcon\Mvc\View();
 		$view->setViewsDir('app/views/');
 		$view->start();
 		$content = $view->getRender('templates-email', $name, array('userFirst' => $userFirst,
		 					  'receiverFirst' => $receiverFirst, 'userLast' => $userLast, 'id' => $id, 'skill' => $skill));
 		$view->finish();
		return $content;
	}

    /**
	 * Function use to send notification email
	 *
	 * @param string $email
	 */    
    public function sendAccept($email, $userFirst, $receiverFirst, $userLast, $id, $skill){		
		// get data from config
        $contacts = $this->getDI()->get('config')->get('contacts')->get('contact-us');
        $gateway = $this->getDI()->get('config')->get('gmail-gateway');
        $subject = "Contract is now Active";
		$name = 'accept';

		$hashids = new Hashids\Hashids(null, 8, null);
		$id = $hashids->encode($id);
		
		// Create transport using smtp credential
        $transport = Swift_SmtpTransport::newInstance($gateway->host, $gateway->port, $gateway->security);
		$transport->setUsername($gateway->username);
		$transport->setPassword($gateway->password); 
		
		// Create the message
		$message = Swift_Message::newInstance();
		$message->setTo($email);
		$message->setSubject($subject);
		$message->setBody($this->getTemplateAccept($name, $userFirst, $receiverFirst, $userLast, $id, $skill),'text/html');
		$message->setFrom($contacts->from, $contacts->name);
		 
		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		$mailer->send($message);   
		return true;     
    }
    
    /**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplateAccept($name, $userFirst, $receiverFirst, $userLast, $id, $skill)
	{
		$view = new \Phalcon\Mvc\View();
 		$view->setViewsDir('app/views/');
 		$view->start();
 		$content = $view->getRender('templates-email', $name, array('userFirst' => $userFirst,
		 					  'receiverFirst' => $receiverFirst, 'userLast' => $userLast, 'id' => $id, 'skill' => $skill));
 		$view->finish();
		return $content;
	}

    public function sendDecline($email, $userFirst, $receiverFirst, $userLast, $id, $skill){		
		// get data from config
        $contacts = $this->getDI()->get('config')->get('contacts')->get('contact-us');
        $gateway = $this->getDI()->get('config')->get('gmail-gateway');
        $subject = "Contract is Declined";
		$name = 'decline';

		$hashids = new Hashids\Hashids(null, 8, null);
		$id = $hashids->encode($id);
		
		// Create transport using smtp credential
        $transport = Swift_SmtpTransport::newInstance($gateway->host, $gateway->port, $gateway->security);
		$transport->setUsername($gateway->username);
		$transport->setPassword($gateway->password); 
		
		// Create the message
		$message = Swift_Message::newInstance();
		$message->setTo($email);
		$message->setSubject($subject);
		$message->setBody($this->getTemplateDecline($name, $userFirst, $receiverFirst, $userLast, $id, $skill),'text/html');
		$message->setFrom($contacts->from, $contacts->name);
		 
		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		$mailer->send($message);   
		return true;     
    }
    
    /**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplateDecline($name, $userFirst, $receiverFirst, $userLast, $id, $skill)
	{
		$view = new \Phalcon\Mvc\View();
 		$view->setViewsDir('app/views/');
 		$view->start();
 		$content = $view->getRender('templates-email', $name, array('userFirst' => $userFirst,
		 					  'receiverFirst' => $receiverFirst, 'userLast' => $userLast, 'id' => $id, 'skill' => $skill));
 		$view->finish();
		return $content;
	}

	public function sendComplete($email, $teacherFirst, $clientFirst, $teacherLast, $id, $skill){		
		// get data from config
        $contacts = $this->getDI()->get('config')->get('contacts')->get('contact-us');
        $gateway = $this->getDI()->get('config')->get('gmail-gateway');
        $subject = "Contract is Completed";
		$name = 'complete';

		$hashids = new Hashids\Hashids(null, 8, null);
		$id = strtoupper($hashids->encode($id));
		
		// Create transport using smtp credential
        $transport = Swift_SmtpTransport::newInstance($gateway->host, $gateway->port, $gateway->security);
		$transport->setUsername($gateway->username);
		$transport->setPassword($gateway->password); 
		
		// Create the message
		$message = Swift_Message::newInstance();
		$message->setTo($email);
		$message->setSubject($subject);
		$message->setBody($this->getTemplateComplete($name, $teacherFirst, $clientFirst, $teacherLast, $id, $skill),'text/html');
		$message->setFrom($contacts->from, $contacts->name);
		 
		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		$mailer->send($message);   
		return true;     
    }
    
    /**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplateComplete($name, $teacherFirst, $clientFirst, $teacherLast, $id, $skill)
	{
		$view = new \Phalcon\Mvc\View();
 		$view->setViewsDir('app/views/');
 		$view->start();
 		$content = $view->getRender('templates-email', $name, array('teacherFirst' => $teacherFirst,
 				 					  'clientFirst' => $clientFirst, 'teacherLast' => $teacherLast, 'id' => $id, 'skill' => $skill));
 		$view->finish();
		return $content;
	}

    public function sendUnderReview($email, $userFirst, $receiverFirst, $userLast, $id, $skill){		
		// get data from config
        $contacts = $this->getDI()->get('config')->get('contacts')->get('contact-us');
        $gateway = $this->getDI()->get('config')->get('gmail-gateway');
        $subject = "Contract is now Under Review";
		$name = 'underReview';
		$hashids = new Hashids\Hashids(null, 8, null);
		$id = $hashids->encode($id);
		
		// Create transport using smtp credential
        $transport = Swift_SmtpTransport::newInstance($gateway->host, $gateway->port, $gateway->security);
		$transport->setUsername($gateway->username);
		$transport->setPassword($gateway->password); 
		
		// Create the message
		$message = Swift_Message::newInstance();
		$message->setTo($email);
		$message->setSubject($subject);
		$message->setBody($this->getTemplateUnderReview($name, $userFirst, $receiverFirst, $userLast, $id, $skill),'text/html');
		$message->setFrom($contacts->from, $contacts->name);
		 
		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		$mailer->send($message);   
		return true;     
    }
    
    /**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplateUnderReview($name, $userFirst, $receiverFirst, $userLast, $id, $skill)
	{
		$view = new \Phalcon\Mvc\View();
 		$view->setViewsDir('app/views/');
 		$view->start();
 		$content = $view->getRender('templates-email', $name, array('userFirst' => $userFirst,
		 					  'receiverFirst' => $receiverFirst, 'userLast' => $userLast, 'id' => $id, 'skill' => $skill));
 		$view->finish();
		return $content;
	}

	public function sendGotMessage($email, $targetFirst, $userFirst, $userLast){		
		// get data from config
        $contacts = $this->getDI()->get('config')->get('contacts')->get('contact-us');
        $gateway = $this->getDI()->get('config')->get('gmail-gateway');
        $subject = "New Message Received";
		$name = 'gotMessage';
		
		// Create transport using smtp credential
        $transport = Swift_SmtpTransport::newInstance($gateway->host, $gateway->port, $gateway->security);
		$transport->setUsername($gateway->username);
		$transport->setPassword($gateway->password); 
		
		// Create the message
		$message = Swift_Message::newInstance();
		$message->setTo($email);
		$message->setSubject($subject);
		$message->setBody($this->getTemplateGotMessage($name, $targetFirst, $userFirst, $userLast),'text/html');
		$message->setFrom($contacts->from, $contacts->name);
		 
		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		$mailer->send($message);   
		return true;     
    }
    
    /**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplateGotMessage($name, $targetFirst, $userFirst, $userLast)
	{
		//$hashids = new Hashids\Hashids(null, 8, null);
		//$newId =  $hashids->encode($targetId);

		$view = new \Phalcon\Mvc\View();
 		$view->setViewsDir('app/views/');
 		$view->start();
 		$content = $view->getRender('templates-email', $name, array('targetFirst' => $targetFirst,
		 					  'userFirst' => $userFirst, 'userLast' => $userLast));
 		$view->finish();
		return $content;
	}

	public function sendGotRequest($email, $targetFirst){		
		// get data from config
        $contacts = $this->getDI()->get('config')->get('contacts')->get('contact-us');
        $gateway = $this->getDI()->get('config')->get('gmail-gateway');
        $subject = "New Gig Received";
		$name = 'gotRequest';
		
		// Create transport using smtp credential
        $transport = Swift_SmtpTransport::newInstance($gateway->host, $gateway->port, $gateway->security);
		$transport->setUsername($gateway->username);
		$transport->setPassword($gateway->password); 
		
		// Create the message
		$message = Swift_Message::newInstance();
		$message->setTo($email);
		$message->setSubject($subject);
		$message->setBody($this->getTemplateGotRequest($name, $targetFirst),'text/html');
		$message->setFrom($contacts->from, $contacts->name);
		 
		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		$mailer->send($message);   
		return true;     
    }
    
    /**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplateGotRequest($name, $targetFirst)
	{
		//$hashids = new Hashids\Hashids(null, 8, null);
		//$newId =  $hashids->encode($targetId);

		$view = new \Phalcon\Mvc\View();
 		$view->setViewsDir('app/views/');
 		$view->start();
 		$content = $view->getRender('templates-email', $name, array('targetFirst' => $targetFirst));
 		$view->finish();
		return $content;
	}

}