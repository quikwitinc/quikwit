<?php

require("/public/third-party/vendor/twilio/Services/Twilio.php");

class Numbers extends BaseModel{
	
	public function saveNumber() {
		// require POST request
		if ($_SERVER['REQUEST_METHOD'] != "POST") die;
		// generate "random" 6-digit verification code
		$code = rand(100000, 999999);
		// save verification code in DB with phone number
		// attempts to delete existing entries first
		$number = mysql_real_escape_string($_POST["phone_number"]);
		db(sprintf("DELETE FROM numbers WHERE phone_number='%s'", $number));
		db(sprintf("INSERT INTO numbers (phone_number, verification_code) VALUES('%s', %d)", $number, $code));
		mysql_close();
	}
	
	public function collect_verify () {
		$response = new Services_Twilio_Twiml();
		if (empty($_POST["Digits"])) {
			$gather = $response->gather(array('numDigits' => 6));
			$gather->say("Please enter your verification code.");
		}
		else {
			// grab db record and check for match
			$result = db(sprintf("select * from numbers where phone_number='%s'", $_POST["Called"]));
			if($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
				if ($_POST["Digits"] === $line["verification_code"]) {
					db(sprintf("UPDATE numbers SET verified = 1 WHERE phone_number = '%s'", $_POST["Called"]));
					$response->say("Thank you! Your phone number has been verified.");
				}
				else {
				// if incorrect, prompt again
				$gather = $response->gather(array('numDigits' => 6));
				$gather->say("Verification code incorrect, please try again.");
				}
			}
			mysql_close();
		}
		print $response;
	}
	

?>