<?php 

use \Phalcon\Mvc\Model\Behavior\SoftDelete,
    \Phalcon\Mvc\Model\Validator,
    \Phalcon\Security,
	\Phalcon\DI,
	\Phalcon\Session\Bag;
	
class OAuth extends BaseModel{
	
	public $auth_id;
	public $auth_type;
	public $auth_access_token;
	public $auth_refresh_token;

	const Bearer = 1;
	
	
	public function initialize(){
		
		$this->setSource("oauth");
		 
		$this->_security = new Security();
		$this->_security->setWorkFactor(14);
	
		parent::initialize();
	}
	
	public function saveAccessToken($token){
		if($accessToken=self::findFirst("auth_type=1")){
			$accessToken->auth_type=1;
			$accessToken->auth_access_token=$token;
			return $accessToken->save();
		}else{
			$this->auth_type=1; //Placeholder, since we dont have an OAuth_Type table yet.
			$this->auth_access_token=$token;
			return $this->save();
		}
	}
	
	public function saveRefreshToken($token){
		if($refreshToken=self::findFirst("auth_type=1")){
			$refreshToken->auth_type=1;
			$refreshToken->auth_refresh_token=$token;
			return $refreshToken->save();
		}else{
			$this->auth_type=1; //Placeholder, since we dont have an OAuth_Type table yet.
			$this->auth_refresh_token=$token;
			return $this->save();
		}
	}

	public function saveTokens($accessToken, $refreshToken){
		$authToken = self::findFirst("auth_type=1");
		$authToken->auth_type=1; //Placeholder, since we dont have an OAuth_Type table yet.
		$authToken->auth_access_token=$accessToken;
		$authToken->auth_refresh_token=$refreshToken;
		return $this->save();
	}
	
	public function getToken(){
		return self::findFirst("auth_type=1");
	}
	
	public function deleteToken(){
		if($authToken = self::findFirst("auth_type=1")){
			$authToken->delete();
		}
	}
}