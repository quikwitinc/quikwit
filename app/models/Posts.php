<?php


class Posts extends BaseModel
{

    const POST_PER_PAGE = 4;

    const STATUS_DELETED = -1;
    const STATUS_DEFAULT = 1;

    /**
     * @var integer
     *
     */
    public $id;

    /**
     * @var string
     *
     */
    public $title;

    /**
     * @var string
     *
     */
    public $slug;

    /**
     * @var string
     *
     */
    public $content;

    /**
     * @var string
     *
     */
    public $created;

    /**
     * @var integer
     *
     */
    public $users_id;

    /**
     * @var integer
     *
     */
    public $categories_id;

    /**
     * @var string
     *
     */
    public $cover_img;

    /**
     * @var integer
     *
     */
    public $status;


    /**
     * Initializer method for model.
     */
    public function initialize()
    {
        $this->setSource('posts');
        $this->belongsTo("users_id", "User", "id");
        $this->belongsTo("categories_id", "PostsCategories", "id");
    }

    public function savePost($title, $slug, $content, $userId, $categoryId, $cover){
        $this->title = $title;
        $this->slug= $slug;
        $this->content = $content;
        $this->created = date("Y-m-d H:i", time());
        $this->users_id = $userId;
        $this->categories_id = $categoryId;
        $this->cover_img = $cover;
        $this->status = self::STATUS_DEFAULT;
        $success = $this->save();

        if($success) {
           return $success; 
        }
    }

    public function getBlogPosts($offset = 0, $count = self::POST_PER_PAGE)
    {
        $sql = 'SELECT *, u.usr_id as user_id, u.usr_firstname as user_firstname, u.usr_lastname as user_lastname, u.usr_avatar as user_avatar, pc.name as category, pc.id as categoryId
                FROM posts p
                LEFT JOIN posts_categories pc on p.categories_id = pc.id
                LEFT JOIN users u ON p.users_id = u.usr_id
                WHERE p.status != :deleted_status
                ORDER BY created DESC';

        if (is_numeric($count)) {
            $sql .= ' LIMIT ' . $count;
            if (is_numeric($offset)) {
                $sql .= ' OFFSET ' . $offset;
            }
        }

        $params = ['deleted_status' => self::STATUS_DELETED];
        $result = $this->getResultSet($sql, $params)->toArray();

        //error_log("<pre>result".print_r($result,true)."</pre>");
        return $result;
    }

    public function getTotalPostCount(){
        $posts = Posts::find();
        $result = count($posts);
        return $result;
    }

    public static function getBlogPostFromSlug($slug)
    {
        $obj = new self;

        $sql = 'SELECT *, u.usr_id as user_id, u.usr_firstname as user_firstname, u.usr_lastname as user_lastname, u.usr_avatar as user_avatar, pc.name as category, pc.id as categoryId
                FROM posts p
                LEFT JOIN posts_categories pc on p.categories_id = pc.id
                LEFT JOIN users u ON p.users_id = u.usr_id
                WHERE p.slug = :slug';

        $params = ['slug' => $slug];
        $result = $obj->getResultSet($sql, $params)->toArray();

        return $result;

    }

    public function getBlogPostsFromCategory($pcSlug, $offset = 0, $count = self::POST_PER_PAGE)
    {
        $sql = 'SELECT *, u.usr_id as user_id, u.usr_firstname as user_firstname, u.usr_lastname as user_lastname, u.usr_avatar as user_avatar, pc.name as category, pc.id as categoryId, pc.reference as categoryRef
                FROM posts p
                LEFT JOIN posts_categories pc on p.categories_id = pc.id
                LEFT JOIN users u ON p.users_id = u.usr_id
                WHERE p.status != :deleted_status 
                    AND pc.reference = :reference
                ORDER BY created DESC';

        if (is_numeric($count)) {
            $sql .= ' LIMIT ' . $count;
            if (is_numeric($offset)) {
                $sql .= ' OFFSET ' . $offset;
            }
        }

        $params = ['deleted_status' => self::STATUS_DELETED, 'reference' => $pcSlug];
        $result = $this->getResultSet($sql, $params)->toArray();

        error_log("<pre>pcslug".print_r($pcSlug,true)."</pre>");
        return $result;
    }



}
