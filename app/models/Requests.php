<?php

use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Requests extends BaseModel{

	public $req_id;
	public $req_userid;
	public $req_tagid;
	public $req_title;
	public $req_text;
	public $req_fixed;
	public $req_price;
	public $req_hour;
	public $req_quality;
	public $req_multiple;
	public $req_datetime;
	public $req_postal;
	public $req_city;
	public $req_travel;
	public $req_phone;
	public $req_created;
	public $req_status;

	const REQUEST_PER_PAGE = 20;
	const STATUS_DELETED = -1;
	const STATUS_PENDING = 0;
	const STATUS_SENT = 1;
	const STATUS_TAKEN = 2;
	const STATUS_CANCEL = 3;

	public function initialize()
	{
		$this->setSource("requests");
	}

	public function saveRequest($userid, $tagid, $title, $task, $fixed, $price, $hour, $quality, $multiple, $datetime, $postal, $cityid, $travel, $phone){
		$this->req_userid = $userid;
		$this->req_tagid = $tagid;
		$this->req_title = $title;
		$this->req_text = $task;
		$this->req_fixed = $fixed;
		$this->req_price = $price;
		$this->req_hour = $hour;
		$this->req_quality = $quality;
		$this->req_multiple = $multiple;
		$this->req_datetime = date("Y-m-d H:i", strtotime($datetime));
		$this->req_postal = $postal; 
		$this->req_city = $cityid;
		$this->req_travel = $travel;
		$this->req_phone = $phone;
		$this->req_created = self::getTimeStamp();
		$this->req_status = self::STATUS_PENDING;
		$success = $this->save();

		return $success;
	}

	public function findRequestById($id){
		$request = self::find(array("conditions" => "req_id = ?1",
				"bind" => array(1 => $id)));
		return $request->getFirst();
	}

	public function setRequestSent($id){
		$request = self::findRequestById($id);
		if($request){
			$request->req_status = self::STATUS_SENT;
			return $request->save();
		}
	}

	public function setRequestTaken($id){
		$request = self::findRequestById($id);
		if($request){
			$request->req_status = self::STATUS_TAKEN;
			return $request->save();
		}

	}

	public function setRequestCancel($id){
		$request = self::findRequestById($id);
		if($request){
			$request->req_status = self::STATUS_CANCEL;
			$request->save();

			if($request->save()){
				$contractModel = new Contracts();
				$contract = $contractModel->findContractByRequestid($id);
				if($contract){
					$contract->cnt_state = Contracts::CANCEL;
					$success = $contract->save();

					return $success;
				}
				
			}
		}

	}

	public function getAllAvailableRequests($page, $limit)
	{
	    $queryCount = "SELECT count(*) as num from requests";
        $result = new Resultset(null, $this, $this->getReadConnection()->query($queryCount, array()));
        $searchTotalResult = $result->toArray();
        $total = $searchTotalResult[0]['num'];
        $offset = ($page-1) * $limit ;
        $limitConditional = ($page !=0 && $limit !=0) ? "LIMIT  $offset , $limit" : '';
        $numPages = ceil($total / $limit);

    	$sql = 'SELECT r.*, u.usr_id as user_id, u.usr_firstname as user_firstname, u.usr_lastname as user_lastname, u.usr_avatar as user_avatar, ts.name as tag, c.name as city
                FROM requests r
                LEFT JOIN users u ON r.req_userid = u.usr_id
                LEFT JOIN tags_standard ts on r.req_tagid = ts.id
                LEFT JOIN city c on r.req_city = c.id
                WHERE r.req_status != :deleted_status 
                	AND r.req_status != :cancelled_status 
                	AND r.req_status != :taken_status
                GROUP BY req_id
                ORDER BY req_created DESC ' . $limitConditional;

        $params = ['deleted_status' => self::STATUS_DELETED, 'cancelled_status' => self::STATUS_CANCEL, 'taken_status' => self::STATUS_TAKEN];
        $requestList = $this->getResultSet($sql, $params)->toArray();



	    return array(
            'total' => $total,
            'numPages' => $numPages,
            'page' => $page,
            'list' => $requestList
        );
	}

	public function getRequestDetailById($requestId)
	{
		$obj = new self;

		if ($requestId) {

		    $contractModel = new Contracts();
		    $contract = $contractModel->findContractByRequestid($requestId);

		    if($contract){
		    	$sql = 'SELECT r.*, u.usr_id as user_id, ts.name as tag, c.cnt_state as state, c.cnt_teachid as teachid, u.usr_firstname as user_firstname, u.usr_lastname as user_lastname, u.usr_avatar as user_avatar, ct.name as city
		    	        FROM requests r
		    	        LEFT JOIN contracts c on r.req_id = c.cnt_requestid
		    	        LEFT JOIN city ct on r.req_city = ct.id
		    	        RIGHT JOIN users u ON c.cnt_teachid = u.usr_id
		    	        LEFT JOIN tags_standard ts on r.req_tagid = ts.id
		    	        WHERE r.req_id = :request_id
		    	          AND r.req_status != :status_deleted
		    	        GROUP BY c.cnt_requestid
		    	        ORDER BY c.cnt_created DESC';

		    	$params = ['status_deleted' => self::STATUS_DELETED, 'request_id' => $requestId];
		    	$result = $obj->getResultSet($sql, $params)->toArray();
		    	return $result;
		    }else{
		    	$sql = 'SELECT r.*, ts.name as tag, ct.name as city
		    	        FROM requests r
		    	        LEFT JOIN city ct on r.req_city = ct.id
		    	        LEFT JOIN tags_standard ts on r.req_tagid = ts.id
		    	        WHERE r.req_id = :request_id
		    	          AND r.req_status != :status_deleted';

		    	$params = ['status_deleted' => self::STATUS_DELETED, 'request_id' => $requestId];
		    	$result = $obj->getResultSet($sql, $params)->toArray();
		    	return $result;
		    }

		}
		return false;
	}

	public function getUserRequests($userId)
	{
		$request = self::find(array(
			"conditions" => "req_userid = ?1 AND req_status = ?2",
			"bind" => array(1 => $userId, 2 => self::STATUS_SENT),
			"order" => "req_created DESC"
		));
		return $request->toArray();
	}

	public static function getUserRequestCount($userId)
	{
	    return self::count(self::getUserRequestParams($userId));
	}

	protected static function getUserRequestParams($userId, $limit = null, $offset = null)
	{
	    $param = ['req_userid = :user_id: AND req_status != :status:',
	        'bind' => ['user_id' => $userId, 'status' => self::STATUS_DELETED]
	    ];

	    if (!is_null($limit)) {
	        $limitParam = [];
	        $limitParam['number'] = $limit;
	        if (!is_null($offset)) {
	            $limitParam['offset'] = $offset;
	        }
	        $param['limit'] = $limitParam;
	    }
	    return $param;
	}

	public function getUserLatestPendingRequest($userId)
	{
		$request = self::findFirst(array(
			"conditions" => "req_userid = ?1 AND req_status = ?2",
			"bind" => array(1 => $userId, 2 => self::STATUS_PENDING),
			"order" => "req_created DESC"
		));
		return $request;
	}

	public function getUserPosts ($userId, $offset = 0, $count = self::REQUEST_PER_PAGE)
	{

	    $sql = 'SELECT r.*, ts.name as tag, c.cnt_state as state, c.cnt_teachid as teachid
	            FROM requests r
	            LEFT JOIN contracts c on r.req_id = c.cnt_requestid
	            LEFT JOIN tags_standard ts on r.req_tagid = ts.id
	            WHERE r.req_status != :deleted_status
	              AND r.req_userid = :user_id
	            GROUP BY req_id
	            ORDER BY req_created DESC';

	    if (is_numeric($count)) {
	        $sql .= ' LIMIT ' . $count;
	        if (is_numeric($offset)) {
	            $sql .= ' OFFSET ' . $offset;
	        }
	    }

	    $params = ['deleted_status' => self::STATUS_DELETED, 'user_id' => $userId];
	    $result = $this->getResultSet($sql, $params)->toArray();
	    return $result;
	    //$requestIds = Requests::getUserRequestIds($userId, $offset, $count);
	    //return $this->getUserPostsFromRequestIds($userId, $requestIds);
	}

	public static function getUserPostsFromRequestIds($userId, $requestId)
	{
	    $obj = new self;

	    if ($requestId) {

	        $contractModel = new Contracts();
	        $contract = $contractModel->findContractByRequestid($requestId);

	        if($contract){
	        	$sql = 'SELECT r.*, u.usr_id as user_id, ts.name as tag, c.cnt_state as state, c.cnt_teachid as teachid, u.usr_firstname as user_firstname, u.usr_lastname as user_lastname, u.usr_avatar as user_avatar, ct.name as city
	        	        FROM requests r
	        	        LEFT JOIN contracts c on r.req_id = c.cnt_requestid
	        	        LEFT JOIN city ct on r.req_city = ct.id
	        	        RIGHT JOIN users u ON c.cnt_teachid = u.usr_id
	        	        LEFT JOIN tags_standard ts on r.req_tagid = ts.id
	        	        WHERE r.req_userid = :user_id
	        	          AND r.req_id = :request_id
	        	          AND r.req_status != :status_deleted
	        	        GROUP BY c.cnt_requestid
	        	        ORDER BY c.cnt_created DESC';

	        	$params = ['user_id' => $userId, 'status_deleted' => self::STATUS_DELETED, 'request_id' => $requestId];
	        	$result = $obj->getResultSet($sql, $params)->toArray();
	        	return $result;
	        }else{
	        	$sql = 'SELECT r.*, ts.name as tag, ct.name as city
	        	        FROM requests r
	        	        LEFT JOIN city ct on r.req_city = ct.id
	        	        LEFT JOIN tags_standard ts on r.req_tagid = ts.id
	        	        WHERE r.req_userid = :user_id
	        	          AND r.req_id = :request_id
	        	          AND r.req_status != :status_deleted';

	        	$params = ['user_id' => $userId, 'status_deleted' => self::STATUS_DELETED, 'request_id' => $requestId];
	        	$result = $obj->getResultSet($sql, $params)->toArray();
	        	return $result;
	        }

	    }
	    return false;
	}

	public function getUserLeads ($userId, $offset = 0, $count = self::REQUEST_PER_PAGE)
	{

	    $sql = 'SELECT r.*, rl.modified, SUM(IF(rl.read_on IS NULL, 1, 0)) AS lead_count, u.usr_id as user_id, u.usr_firstname as user_firstname, u.usr_lastname as user_lastname, u.usr_avatar as user_avatar, ts.name as tag
	            FROM requests r
	            LEFT JOIN requests_lead rl on r.req_id = rl.request_id
	            LEFT JOIN users u ON rl.user_id = u.usr_id
	            LEFT JOIN tags_standard ts on r.req_tagid = ts.id
	            WHERE r.req_status != :deleted_status
	              AND rl.target_id = :user_id
	            GROUP BY req_id
	            ORDER BY req_created DESC';

	    if (is_numeric($count)) {
	        $sql .= ' LIMIT ' . $count;
	        if (is_numeric($offset)) {
	            $sql .= ' OFFSET ' . $offset;
	        }
	    }

	    $params = ['deleted_status' => self::STATUS_DELETED, 'user_id' => $userId];
	    $result = $this->getResultSet($sql, $params)->toArray();
	    return $result;
	    $requestIds = RequestsLead::getUserRequestIds($userId, $offset, $count);
	    return $this->getUserLeadsFromRequestIds($userId, $requestIds);
	}

	public static function getUserLeadsFromRequestIds($userId, $requestIds)
	{
	    $obj = new self;

	    if ($requestIds) {
	        if (!is_array($requestIds)) {
	            $requestIds = [$requestIds];
	        }

	        $sql = 'SELECT r.*, u.usr_id as user_id, u.usr_firstname as user_firstname, u.usr_lastname as user_lastname, u.usr_avatar as user_avatar, ts.name as tag, ct.name as city
	                FROM requests r
	                LEFT JOIN requests_lead rl on r.req_id = rl.request_id
	                LEFT JOIN city ct on r.req_city = ct.id
	                LEFT JOIN users u ON rl.user_id = u.usr_id
	                LEFT JOIN tags_standard ts on r.req_tagid = ts.id
	                WHERE r.req_id = rl.request_id
	                  AND rl.target_id = :user_id
	                  AND r.req_id IN (' . implode($requestIds, ',') . ')
	                  AND r.req_status != :status_deleted
	                  AND rl.status != :rl_status_deleted
	                ORDER BY r.req_created DESC';

	        $params = ['user_id' => $userId, 'status_deleted' => self::STATUS_DELETED, 'rl_status_deleted' => RequestsLead::STATUS_DELETED];
	        $result = $obj->getResultSet($sql, $params)->toArray();
	        return $result;
	    }
	    return false;
	}

	public static function getUserLeadCount($userId){

	    $leads = RequestsLead::getUserRequestCount($userId);

	    return $leads;
	}

	/**
     * @param $id
     * @return  Requests
     */
    public static function getRequestById($id)
    {
        $request = self::findFirst(["req_id = :id: AND req_status != :status:", "bind" => ["id" => $id, "status" => self::STATUS_DELETED]]);
        return $request;
    }

	public static function setLeadRead($requestId, $userId)
	{
	    $request = self::getRequestById($requestId);
	    if ($request) {      
	        return RequestsLead::updateReadStatusByIds($requestId, $userId);
	    }
	    return false;
	}

	public function getLeadsFromRequestId($requestId)
	{
		$obj = new self;

		if ($requestId) {

		    $sql = 'SELECT r.*, u.usr_id as user_id, u.usr_firstname as user_firstname, u.usr_lastname as user_lastname, u.usr_avatar as user_avatar, ts.name as tag, ct.name as city
		            FROM requests r
		            LEFT JOIN city ct on r.req_city = ct.id
		            LEFT JOIN users u ON r.req_userid = u.usr_id
		            LEFT JOIN tags_standard ts on r.req_tagid = ts.id
		            WHERE r.req_id = :request_id
		              AND r.req_status != :status_deleted
		            ORDER BY r.req_created DESC';

		    $params = ['request_id' => $requestId, 'status_deleted' => self::STATUS_DELETED];
		    $result = $obj->getResultSet($sql, $params)->toArray();
		    return $result;
		}
		return false;
	}

}