<?php 

use Phalcon\Mvc\Model\Behavior\Timestampable;

class RequestsLead extends BaseModel{
    
    const STATUS_READ = 1;
    const STATUS_NOT_READ = 0;
    const STATUS_SENT = 2;
    const STATUS_DELETED = -1;
    
    protected $id;
    protected $request_id;
    protected $user_id;
    protected $target_id;
    protected $read_on;
    protected $status;
    protected $modified;

    public function getId()
    {
        return $this->id;
    }

    public function setRequestId($requestId)
    {
        $this->request_id = $requestId;
        return $this;
    }

    public function getRequestId()
    {
        return $this->request_id;
    }

    public function setUserId($userId)
    {
        $this->user_id = $userId;
        return $this;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setTargetId($targetId)
    {
        $this->target_id = $targetId;
        return $this;
    }

    public function getTargetId()
    {
        return $this->target_id;
    }

    public function setReadOn($readOn = null)
    {
        $readOn = $readOn ? $readOn : self::getTimeStamp();
        $this->read_on = $readOn;
        return $this;
    }

    public function getReadOn()
    {
        return $this->read_on;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setModified($modified = null)
    {
        $modified = $modified ? $modified : self::getTimeStamp();
        $this->modified = $modified;
        return $this;
    }

    public function getModified()
    {
        return $this->modified;
    }

    public function initialize()
    {
        $this->setSource('requests_lead');
        $this->belongsTo('request_id', 'requests', 'req_id');
        $this->belongsTo('user_id', 'users', 'usr_id');
        $this->belongsTo('target_id', 'users', 'usr_id');
    }

    public function addRequest($requestId, $senderId, $receiverId)
    {
        self::addReceiver($requestId, $receiverId, $senderId);
    }

    public function updateReadStatus($readOn = null)
    {
        $this->setReadOn($readOn);
        $this->setStatus(self::STATUS_READ);
        $this->setModified();
        return $this->save();
    }

    protected static function addReceiver($requestId, $receiverId, $senderId)
    {
        $request = new self;
        $request->setRequestId($requestId);
        $request->setUserId($senderId);
        $request->setTargetId($receiverId);
        $request->setStatus(self::STATUS_NOT_READ);
        $request->setModified();
        $request->save();

        if($request->save()){
            $userModel = new User();
            $user = $userModel->findUserById($receiverId);
            if ($user->usr_password == null) {
                $userModel->sendEmailConfirmation($receiverId, $user->usr_email);
            }else{
                $nemail = new NotificationsEmail();
                $nemail->sendGotRequest($user->usr_email, $user->usr_firstname);
            }
        }
    }

    /**
     * @param $requestId
     * @param $userId
     * @return RequestsNotification
     */
    public static function getRequestByIds($requestId, $userId)
    {
        return self::findFirst(['request_id = :request_id: AND target_id = :user_id:', 'bind' => ['request_id' => $requestId, 'user_id' => $userId]]);
    }

    public static function updateReadStatusByIds($requestId, $userId)
    {
        if ($request = self::getRequestByIds($requestId, $userId)) {
            return $request->updateReadStatus();
        }
        return false;
    }

    public static function getUnreadRequests($userId)
    {
        return self::find(self::getUserUnreadRequestParams($userId));
    }

    public static function getUnreadRequestCount($userId)
    {
        return self::count(self::getUserUnreadRequestParams($userId));
    }

    public static function getUserRequestIds($userId, $offset = 0, $count = Requests::REQUEST_PER_PAGE)
    {
        $resultSet = self::find(self::getUserRequestParams($userId, $count, $offset));
        if ($resultSet->count() > 0) {
            $resultSet = $resultSet->toArray();
            return array_unique(array_column($resultSet, 'request_id'));
        }
        return null;
    }

    public static function getUserRequestCount($userId)
    {
        return self::count(self::getUserRequestParams($userId));
    }

    public static function hasAccessToRequest($requestId, $userId)
    {
        $request = self::getRequestByIds($requestId, $userId);
        return  $request !== false;
    }

    protected static function getUserUnreadRequestParams($userId)
    {
        return ['target_id = :user_id: AND status = :status_not_read:',
            'bind' => ['user_id' => $userId, 'status_not_read' => self::STATUS_NOT_READ]
        ];
    }

    protected static function getUserRequestParams($userId, $limit = null, $offset = null)
    {
        $param = ['target_id = :user_id: AND status != :status:',
            'bind' => ['user_id' => $userId, 'status' => self::STATUS_DELETED]
        ];

        if (!is_null($limit)) {
            $limitParam = [];
            $limitParam['number'] = $limit;
            if (!is_null($offset)) {
                $limitParam['offset'] = $offset;
            }
            $param['limit'] = $limitParam;
        }
        return $param;
    }
}