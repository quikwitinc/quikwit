<?php
use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class Reviews extends BaseModel{

	const FALSE = 0;
	const TRUE = 1;

	const ACTIVE = 0;
	const FLAGGED = 1;

	public $rvs_id;
	public $rvs_user;
	public $rvs_target;
	public $rvs_overall;
	public $rvs_month;
	public $rvs_year;
	public $rvs_created;
	public $rvs_title;
	public $rvs_trust; // teaching skill
	public $rvs_friend; //friendness
	public $rvs_quality; //knowledge
	public $rvs_time; // puntuality
	public $rvs_comment;
	public $rvs_edited;
	public $rvs_deleted;
	public $rvs_contractId;
	public $rvs_status;

	public function initialize()
	{
		$this->setSource("reviews");
		$this->belongsTo('rvs_contractId', 'Contracts', 'cnt_id');
		$this->belongsTo('rvs_user', 'User', 'usr_id',  array(
            'alias'=> 'Reviewer'));
	}

	public function saveReview($options){
		$this->rvs_user = $options['user'];
		$this->rvs_target = $options['teacher'];
		$this->rvs_title = $options['title'];
		$this->rvs_trust = $options['teaching'];
		$this->rvs_friend = $options['friendliness'];
		$this->rvs_quality = $options['knowledge'];
		$this->rvs_time = $options['punctuality'];
		$this->rvs_comment = $options['comment'];
		$this->rvs_overall = $options['overall'];
		$this->rvs_month = $options['month'];
		$this->rvs_year = $options['year'];
		$this->rvs_contractId = $options['contractId'];
		$this->rvs_created = date("Y-m-d H:i", time());;
		$this->rvs_edited = null;
		$this->rvs_deleted = null;
		$this->rvs_status = self::ACTIVE;
		return $this->save();
	}

	public function findReviewById($id){
		$review = self::find(array("conditions" => "rvs_id = ?1",
				"bind" => array(1 => $id)));
		return $review->getFirst();
	}

	public function getCount($userId){
		$rvw = self::find(array("conditions" => "rvs_id = ?1",
				"bind" => array(1 => $id)));
		return count($rvw);
	}

	public function getReviews($userId, $refId=0, $offset=0, $count=null){
		$sql = "SELECT r.*
				FROM reviews r, users u
				WHERE r.rvs_user=? AND r.rvs_target=u.usr_id AND rvs_id>?";

		if($count){
			$sql.=" LIMIT $offset, $count";
		}
		return new Resultset(null, $this, $this->getReadConnection()->query($sql, array($userId, self::NOT_READ, $refId)));
	}

	public function setTrust($id, $review){
		$oreview = $this->findReviewById($id);
		if($oreview){
			$oreview->rvs_trust = TRUE;
			$ocontract->rvs_edited = time();
			$this->save();
		}
		return false;
	}

	public function setFriend($id, $review){
		$oreview = $this->findReviewById($id);
		if($oreview){
			$oreview->rvs_friend = TRUE;
			$ocontract->rvs_edited = time();
			$this->save();
		}
		return false;
	}

	public function setQuality($id, $review){
		$oreview = $this->findReviewById($id);
		if($oreview){
			$oreview->rvs_quality = TRUE;
			$ocontract->rvs_edited = time();
			$this->save();
		}
		return false;
	}

	public function setTime($id, $review){
		$oreview = $this->findReviewById($id);
		if($oreview){
			$oreview->rvs_time = TRUE;
			$ocontract->rvs_edited = time();
			$this->save();
		}
		return false;
	}

	public function getStats($userId){
		$sql = "SELECT avg(rvs_overall) as overall, avg(rvs_trust) as teaching_skill, avg(rvs_friend) as friendness, avg(rvs_quality) as knowledge, avg(rvs_time) as puntuality, count(*) as num_contract
				FROM reviews
				WHERE rvs_target=?";

		return new Resultset(null, $this, $this->getReadConnection()->query($sql, array($userId)));
	}

	public function getPaginateReview($userId, $page, $limit){
		$reviews = self::find(array(
			"conditions" => "(rvs_target = ?1)",
			"bind" => array(1 => $userId),
			"order" => "rvs_created DESC"
			));
		$paginator = new \Phalcon\Paginator\Adapter\Model(
		    array(
		        "data" => $reviews,
		        "limit"=> $limit,
		        "page" => $page
		    )
		);
		return $paginator->getPaginate();
	}

	public function report($id){
		$review = self::findReviewById($id);
		if($review){
			$review->rvs_status = self::FLAGGED;
			return $review->save();
		}
		return false;
	}

	public static function beenReviewed($userId, $targetId){
        $reviewed = self::findFirst(['rvs_user = :rvs_user: AND rvs_target = :rvs_target:', 'bind' => ['rvs_user' => $userId, 'rvs_target' => $targetId]]);
		return $reviewed;
	}

}