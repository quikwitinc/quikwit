<?php

use \Phalcon\Mvc\Model\Behavior\SoftDelete,
\Phalcon\Mvc\Model\Validator,
\Phalcon\Security,
\Phalcon\DI,
\Phalcon\Session\Bag;
use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class SearchUsers extends BaseModel
{
    public $id;
    public $usr_id;
    public $contract_complete;
    public $num_rating;
    public $rating;
    public $intro_video;
    public $uploads;
    public $experience_filled;
    public $lat;
    public $lng;
    public $price;

    public function initialize()
    {
        $this->setSource("users_search");
        parent::initialize();
        $this->belongsTo('usr_id', 'User', 'usr_id');
    }

    public function addSearch($options){
        $this->usr_id = $options['usr_id'];
        $exist = $this->findSearchByUser($options['usr_id']);
        if ($exist){
            return false;
        }
        $contractModel = new Contracts();
        $contractCompletedObj = $contractModel->getNumCompletedContracts($options['usr_id'])->toArray();
        $contractCompleted = $contractCompletedObj[0]['num_contract'];

        $this->contract_complete = $contractCompleted;

        $reviewModel = new Reviews();
        $statsObj = $reviewModel->getStats($options['usr_id'])->toArray();
        $stats = ($statsObj[0]['num_contract'])?$statsObj[0]['num_contract']:0;

        $this->num_rating = $stats;

        $overall = ($statsObj[0]['overall'])?$statsObj[0]['overall']:0;
        $this->rating = $overall;
        //not sure where we set the intro video
        $this->intro_video = 0;

        $videoModel = new Video();
        $userExperienceModel = new UsersExperience();
        $numVideoObj = $videoModel->getNumVideoByUser($options['usr_id'])->toArray();
        $this->uploads = ($numVideoObj[0]['num_video'] > 99)?1:($numVideoObj[0]['num_video'] * 0.01);
        $this->experience_filled = ($userExperienceModel->hasExperience($options['usr_id']) > 0)?1:0;

        if (($options['lat'] != null && $options['lat'] != 0) &&
                ($options['lng'] != null && $options['lng'] != 0)){
        $this->lat = $options['lat'];
        $this->lng = $options['lng'];
        }else{
        $this->lat = 9999;
        $this->lng = 9999;
        }
        $this->price = $options['rate'];
        $status = $this->save();
        return $status;
    }

    public function findFreelancers($q, $userId, $isUser, $page, $limit, $sort, $minPrice='', $maxPrice='', $mapRadiusOption=0, $mapRadius='')
    {      
    	$whr = '';
    	if($minPrice != '' and $maxPrice != '') {
    		$whr .= ' AND price >= '.$minPrice.' AND price <= '.$maxPrice.' ';
    	}
    	else if($minPrice != '') {
    		$whr .= ' AND price >= '.$minPrice.' ';
    	}
    	else if($maxPrice != '') {
    		$whr .= ' AND price <= '.$maxPrice.' ';
    	}

        //$di = \Phalcon\DI::getDefault();
        //$db = $di->get('db');
        //$sql =  $db->prepare("SELECT id FROM tags_standard WHERE name SOUNDS LIKE '" . $q . "'");
    	
        $sql =  "SELECT id FROM tags_standard WHERE name SOUNDS LIKE :q";

        /* $sql = Tags::find(array(
            'conditions' => 'name LIKE :name:',
            'bind' => array(
                'name' => "%" . $q . "%"
            )
        )); */
        //error_log("<pre>sql".print_r($sql,true)."</pre>"); 
        $result = new Resultset(null, $this,
        $this->getReadConnection()->query($sql, array('q' => $q)));
        $tagResult = $result->toArray();
        $tagList = array();
        foreach ($tagResult as $key => $value) {
            $tagList[] = $value['id'];
        }
        $userIds = implode(',', $tagList);
        $queryDistance = ', 0 as distance ';
        $notSelf = ' 1 ';
        $orderBy = " ORDER BY rank DESC, rating DESC, distance ASC, contract_complete DESC, num_rating DESC, intro_video DESC, experience_filled DESC, uploads DESC ";
        if ($sort == 'distance'){
        $orderBy = " ORDER BY distance ASC, rank DESC, rating DESC, contract_complete DESC, num_rating DESC, intro_video DESC, experience_filled DESC, uploads DESC ";
        }
        if ($sort == 'rating'){
        $orderBy = " ORDER BY rating DESC, rank DESC, distance ASC, contract_complete DESC, num_rating DESC, intro_video DESC, experience_filled DESC, uploads DESC ";
        }
        if ($sort == 'lowprice'){
        $orderBy = " ORDER BY price ASC, rating DESC, rank DESC, distance ASC, contract_complete DESC, num_rating DESC, intro_video DESC, experience_filled DESC, uploads DESC ";
        }
        if ($sort == 'highprice'){
        $orderBy = " ORDER BY price DESC, rating DESC, rank DESC, distance ASC, contract_complete DESC, num_rating DESC, intro_video DESC, experience_filled DESC, uploads DESC ";
        }
        $queryDistanceHaving = '';
        if ($isUser){
            $user = User::findUserById($userId);
            if ($user->lat != null && $user->lat != 0 &&
                $user->lng != null && $user->lng != 0){
	            if($mapRadius!='' and $mapRadiusOption!=0) {
		            $queryDistance = ', (6371 * acos (
						      cos ( radians('.$user->lat.') )
						      * cos( radians( s.lat ) )
						      * cos( radians( s.lng ) - radians('.$user->lng.') )
						      + sin ( radians('.$user->lat.') )
						      * sin( radians(s.lat ) )
						    )
						 ) as distance ';//add distance query
	            
	            	$queryDistanceHaving = ' HAVING distance <= '.$mapRadius.' ';
	            }
            }
                        
            $notSelf = ' usr_id != ' . $userId . ' ';
        }
        if (strlen($userIds) > 0){
            $queryTag = 'SELECT s.*' . $queryDistance . ' FROM `user_tags` t join users_search s on t.user_id = s.usr_id WHERE tag_id in (' . $userIds. ')'.$queryDistanceHaving;
            $queryUser = 'SELECT s.*' . $queryDistance . '  FROM `users` u join users_search s on u.usr_id=s.usr_id WHERE (usr_firstname LIKE "' . $q . '%" OR usr_lastname LIKE "' . $q . '%" OR usr_email LIKE "' . $q. '%") AND usr_level="user" AND usr_teach = 1'.$queryDistanceHaving;
            $queryCount = "SELECT count(*) as num from (($queryTag) union ($queryUser)) ab  WHERE $notSelf $whr";
            $result = new Resultset(null, $this,
            $this->getReadConnection()->query($queryCount, array()));
            $searchTotalResult = $result->toArray();
            $total = $searchTotalResult[0]['num'];
            $offset = ($page-1) * $limit ;
            $limitConditional = ($page !=0 && $limit !=0) ? "LIMIT  $offset , $limit" : '';
            $numPages = ceil($total / $limit);
            $limitConditional = $orderBy . $limitConditional;

            //find rows
            $queryUnion = "SELECT *, ((0.1 * contract_complete) + (0.02 * num_rating) + rating + (0.1 * intro_video) + (0.1 * experience_filled) + uploads  - (0.1 * distance)) as rank from
                (($queryTag) union ($queryUser)) ab WHERE $notSelf $whr $limitConditional";
                
            $result = new Resultset(null, $this,
            $this->getReadConnection()->query($queryUnion, array()));
            $searchRows = $result->toArray();
        } else{
            $queryUser = 'SELECT s.*' . $queryDistance . '  FROM `users` u join users_search s on u.usr_id=s.usr_id WHERE (usr_firstname LIKE "' . $q . '%" OR usr_lastname LIKE "' . $q . '%" OR usr_email LIKE "' . $q. '%") AND usr_level="user" AND usr_teach = 1';
            $queryCount = "SELECT count(*) as num from ($queryUser) ab  WHERE $notSelf $whr";
            $result = new Resultset(null, $this,
            $this->getReadConnection()->query($queryCount, array()));
            $searchTotalResult = $result->toArray();
            $total = $searchTotalResult[0]['num'];
            $offset = ($page-1) * $limit ;
            $limitConditional = ($page !=0 && $limit !=0) ? "LIMIT  $offset , $limit" : '';
            $numPages = ceil($total / $limit);
            $limitConditional = $orderBy . $limitConditional;
            //find rows
            $queryUnion = "SELECT *, ((0.1 * contract_complete) + (0.02 * num_rating) + rating + (0.1 * intro_video) + (0.1 * experience_filled) + uploads  - (0.1 * distance)) as rank from
                ($queryUser) ab WHERE $notSelf $whr $limitConditional";
            $result = new Resultset(null, $this,
            $this->getReadConnection()->query($queryUnion, array()));
            $searchRows = $result->toArray();
        }
        $userList = array();

        foreach ($searchRows as $key => $value) {
            $currentUser = User::findUserById($value['usr_id']);
            $searchRows[$key]['search_id'] = $searchRows[$key]['id'];
            $searchRows[$key]['id'] = $value['usr_id'];
            $searchRows[$key]['firstname'] = $currentUser->usr_firstname;
            $searchRows[$key]['lastname'] = $currentUser->usr_lastname;
            $searchRows[$key]['avatar'] = $currentUser->usr_avatar;
            $searchRows[$key]['lan'] = $currentUser->usr_lang;
            $searchRows[$key]['usr_teach'] = 1;
            $searchRows[$key]['usr_rate'] = $value['price'];
            
            $skills = array();
            $utModel = new UserTags();
            $userTags = $utModel->getUserTags($value['usr_id']);
            for($i = 0; $i < count($userTags); $i++){
                $skills[] = $userTags[$i]['name'];
            }
            /*if (count($skills) < 1){
                $skills[] = 'None';
            } */
            
            $stripq = str_replace(' ', '', $q);
            $stripSkills = str_replace(' ', '', $skills);
            $match = preg_grep("/^$stripq$/i", $stripSkills);
            $skillCount = count($skills);

            if(count($match) > 0) {
	            $skillCountMinus = $skillCount - 1;	            
	            if($skillCountMinus == 0){
	                $skillCountMinus = "";
	            }else{
	                $skillCountMinus = ' and ' . $skillCountMinus . ' more';
	            }
                foreach($match as $k=>$v) {             
                    $skill = $skills[$k];
                }
                $searchRows[$key]['skills'] =  $skill;            
	            $searchRows[$key]['moreskills'] = $skillCountMinus;
            }else if (count($match) == 0) {
                $searchRows[$key]['skills'] =  $skillCount . " skills";    
                $searchRows[$key]['moreskills'] = "";
            }
            else if ($skillCount>0){
            	$skill = $skills[0];
            	$skillCountMinus = $skillCount - 1;	            
	            if($skillCountMinus == 0){
	                $skillCountMinus = "";
	            }else{
	                $skillCountMinus = ' and ' . $skillCountMinus . ' more';
	            }  
	            $searchRows[$key]['moreskills'] =  $skillCountMinus;
            }
            else {
	            $searchRows[$key]['skills'] =  "N/A";            
            }
            
            //$searchRows[$key]['skills'] = implode('</a> <a href="javascript:void(0)" class="label label-info">', $skills);
            $userList[] = $searchRows[$key];
        }
        return array(
            'total' => $total,
            'numPages' => $numPages,
            'page' => $page,
            'list' => $userList
        );
    }

    public function findSearchByUser($id){
        $search = self::find(array("conditions" => "usr_id = ?1",
                        "bind" => array(1 => $id)));
        return $search->getFirst();
    }

    public function updateExperience($usr_id, $status)
    {
    $search = $this->findSearchByUser($usr_id);
    $status = ($status == true)?1:0;
    if($search){
        if ($search->experience_filled != $status){
            $search->experience_filled = $status;
            $search->save();
        }
    }
    return false;
    }

    public function updateVideo($usr_id, $status)
    {
    $search = $this->findSearchByUser($usr_id);
    if($search){
        if ($search->intro_video != $status){
            $search->intro_video = $status;
            $search->save();
        }
    }
    return false;
    }

    public function updateContract($usr_id)
    {
    $search = $this->findSearchByUser($usr_id);
    if($search){
        $contractModel = new Contracts();
        $contractCompletedObj = $contractModel->getNumCompletedContracts($usr_id)->toArray();
        $contractCompleted = $contractCompletedObj[0]['num_contract'];
        if ($search->contract_complete != $contractCompleted){
            $search->contract_complete = $contractCompleted;
            $search->save();
        }
    }
    return false;
    }

    public function updateNumRating($usr_id)
    {
    $search = $this->findSearchByUser($usr_id);
    if($search){
        $reviewModel = new Reviews();
        $statsObj = $reviewModel->getStats($usr_id)->toArray();
        $stats = $statsObj[0]['num_contract'];
        if ($search->num_rating != $stats){
            $search->num_rating = $stats;
            $search->save();
        }
    }
    return false;
    }

    public function updateRating($usr_id)
    {
    $search = $this->findSearchByUser($usr_id);
    if($search){
        $reviewModel = new Reviews();
        $statsObj = $reviewModel->getStats($usr_id)->toArray();
        //error_log("<pre>statsObj".print_r($statsObj,true)."</pre>"); 
        $stats = $statsObj[0]['overall'];
        if ($search->rating != $stats){
            $search->rating = $stats;
            $search->save();
        }
    }
    return false;
    }

    public function updateUploads($usr_id, $status)
    {
    $search = $this->findSearchByUser($usr_id);
    if($search){
        if ($search->uploads != $status){
            $search->uploads = $status;
            $search->save();
        }
    }
    return false;
    }
    public function updateGPS($usr_id, $status)
    {
        $user = User::findUserById($usr_id);
        $search = $this->findSearchByUser($usr_id);
        if(count($user)===1){
            if (($user->lat != null && $user->lat != 0) &&
                ($user->lng != null && $user->lng != 0) &&
                $search){
                $search->lat = $user->lat;
                $search->lng = $user->lng;
                $search->save();
            }else{
                $search->lat = 9999;
                $search->lng = 9999;
                $search->save();
            }
        }
        return false;
    }

    public function deleteSearchUser($userId){
        return self::find(["usr_id=".$userId])->delete();
    }

    public function updatePrice($usr_id, $price)
    {
        $search = $this->findSearchByUser($usr_id);
        if($search){
            if ($search->price != $price){
                $search->price = $price;
                $search->save();
            }
        }
    return false;
    }

    public function updatePostal($usr_id, $lat, $lng)
    {
        $search = $this->findSearchByUser($usr_id);

        if($search){
            $search->lat = $lat;
            $search->lng = $lng;
            $search->save();          
        }

        return false;
    }

}