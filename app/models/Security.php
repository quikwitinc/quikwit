<?php

use \Phalcon\DI;

class Security extends \Phalcon\Security
{
    public function getTokenKey($numberBytes = 13)
    {
        $key = '$PHALCON/CSRF/KEY$';

        $tokenKey = \Phalcon\DI::getDefault()->getShared('session')->get($key);

        if ($tokenKey)
        {
            return $tokenKey;
        }

        return parent::getTokenKey($numberBytes);
    }   

    public function getToken($numberBytes = 32)
    {
        $key = '$PHALCON/CSRF$';

        $token = \Phalcon\DI::getDefault()->getShared('session')->get($key);

        if ($token)
        {
            return $token;
        }

        return parent::getToken($numberBytes);
    } 

    public function changeToken($numberBytes = 32)
    {
        return parent::getToken($numberBytes);
    }

}