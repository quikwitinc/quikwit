<?php

class Tags extends BaseModel
{
	const STATUS_APPROVED = 1;
	const STATUS_PENDING = 0;
	const STATUS_BANNED = 2;

	public $id;
	public $name;
	public $status;
	public $created;

	public function initialize()
	{
		$this->setSource("tags");
		parent::initialize();
	}
	
	public function add($name){
		$tag = $this->findTagByName($name);
		if($tag->count()===0){
			$tag = new Tags();
			$tag->name = $name;
			$tag->status = self::STATUS_PENDING; /* Temporarily set to APPROVED before implementing TAG APPROVAL SYSTEM */
			$tag->created = time();
			return $tag->save()? $tag : false;
		}
		return $tag;
	}
	
	public function updateTag($id, $tag){
		$otag = $this->findTagById($id);
		if($otag){
			$otag->name = $tag;
			$otag->status = STATUS_APPROVED; /* Temporarily set to APPROVED before implementing TAG APPROVAL SYSTEM */
			$this->save();
		}
		return false;
	}

	public function findTagByName($tag){
		return self::find(array("conditions" => "name = :tag:", "bind" => array("tag" => $tag)));
	}
	
	public function findTagById($id){
		return self::find(array("conditions" => "id = :id:", "bind" => array("id" => $id)));
	}

}
