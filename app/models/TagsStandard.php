<?php

class TagsStandard extends BaseModel
{
	const STATUS_APPROVED = 1;
	const STATUS_DISABLED= 0;

	public $id;
	public $name;
	public $status;
	public $category;

	public function initialize()
	{
		$this->setSource("tags_standard");
		parent::initialize();
	}

	public function add($name){
		$tag = $this->findTagByName($name);
		if($tag->count()===0){
			$tag = new TagsStandard();
			$tag->name = $name;
			$tag->status = self::STATUS_APPROVED; /* Temporarily set to APPROVED before implementing TAG APPROVAL SYSTEM */
			$tag->category_id = $category;
			return $tag->save()? $tag : false;
		}
		return $tag;
	}

	public function updateTag($id, $tag, $category){
		$otag = $this->findTagById($id);
		if($otag){
			$otag->name = $tag;
			$otag->status = STATUS_APPROVED; /* Temporarily set to APPROVED before implementing TAG APPROVAL SYSTEM */
			$tag->category_id = $category;
			$this->save();
		}
		return false;
	}

	public function findTagByName($tag){
		return self::find(array("conditions" => "name = :tag:", "bind" => array("tag" => $tag)));
	}

	public function findTagById($id){
		return self::find(array("conditions" => "id = :id:", "bind" => array("id" => $id)));
	}

	public static function findTagBySingleId($id)
    {
		$skill = self::find(array("conditions" => "id = ?1",
				"bind" => array(1 => $id)));
		return $skill->getFirst();
	}

	public static function findTagByCategoryId($categoryId)
	{
		return self::find(array("conditions" => "category_id = :category_id:", "bind" => array("category_id" => $categoryId), "order" => "name"));
	}

	public function findTagByCategoryName($categoryName)
	{
		$sql = 'SELECT ts.*, ct.cat_id, ct.cat_name
		        FROM tags_standard ts
		        LEFT JOIN categories ct on ts.category_id = ct.cat_id
		        WHERE ct.cat_name = :category
		          AND ts.status = :status_approved
		        ORDER BY ts.name ASC';

		$params = ['category' => $categoryName, 'status_approved' => self::STATUS_APPROVED];
		$result = $this->getResultSet($sql, $params)->toArray();
		return $result;
	}

}