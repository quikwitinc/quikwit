<?php

use Phalcon\Mvc\Model\Behavior\SoftDelete,
    Phalcon\Mvc\Model\Validator,
    Phalcon\Security,
    Phalcon\DI;

class User extends BaseModel
{
	const DELETED = 1;
	const NOT_DELETED = 0;
	const TEACH = 1;
	const NOT_TEACH = 0;
	const VERIFIED = 1;
	const NOT_VERIFIED = 0;
	const SUBSCRIBED = 1;
	const NOT_SUBSCRIBED = 0;
	const EMAIL_FORWARD = 1;
	const NO_FORWARD = 0;

	const ROLE_GUEST = 'guest';
	const ROLE_USER = 'user';
	const ROLE_ADMIN = 'admin';

    const LANG_ENGLISH = 'English';
    const DEFAULT_LANG = self::LANG_ENGLISH;

    const USER_REPORTED = 1;

	private $_session, $_security;

	public $usr_id;
	public $usr_deleted;
	public $usr_level;
	public $usr_handle;
	public $usr_password;
	public $usr_salt;
	public $usr_email;
	public $usr_firstname;
	public $usr_lastname;
	public $usr_avatar;
	public $usr_cover;
	public $usr_organization;
	public $usr_address;
	public $usr_city;
	public $usr_country;
	public $usr_created;
	public $usr_about;
	public $usr_postalcode;
	public $user_lang;
	public $lat;
	public $lng;
	public $usr_teach;
	public $usr_verifcation;
    public $usr_email_verified;
	public $usr_reported;
	public $usr_rate;
	public $usr_subscribe;
	public $usr_forward;

	public function initialize()
	{
		$this->setSource("users");

		// local field, referenced model, referenced field.
		$this->hasMany('id', 'Project', 'user_id');
		$this->hasMany('usr_id', 'UserTags', 'user_id');
		$this->hasMany('usr_id', 'UserLanguages', 'user_id');
		$this->hasMany('usr_id', 'UsersExperience', 'user_id');
        $this->hasMany('usr_id', 'UserPasswordRecovery', 'user_id');
		$this->hasMany('usr_id', 'Reviews', 'rvs_target');
		$this->hasMany('usr_id', 'UsersConnections', 'usr_connection_user');
		$this->hasMany('usr_id', 'SearchUsers', 'usr_id');

		$this->addBehavior(new SoftDelete([
				'field' => 'usr_deleted',
				'value' => 1
				]));

		$this->_security = new Security();
		$this->_security->setWorkFactor(14);

		parent::initialize();
	}

	public function login($user)
    {
		//$tp = time(); //UTC timestamp for login time.
		$sData = new stdClass();
		$sData->id = $user->usr_id;
		$sData->firstName = $user->usr_firstname;
		$sData->lastName = $user->usr_lastname;

		//$this->_sess->user = $sData;
		//$this->_sess->lastLogin = $tp;
	}

	public function addUser($email, $password, $firstname, $lastname)
    {
		$this->usr_email = $email;
		if(empty($password)){
			$this->usr_password = $password;
		}else{
			$this->usr_password = $this->_hash($password);
		}
		$this->usr_firstname = $firstname;
		$this->usr_lastname = $lastname;
		$this->usr_level = Permission::USER;
		$this->usr_deleted = User::NOT_DELETED;
		$this->usr_lang = self::DEFAULT_LANG;
		$this->usr_teach = User::NOT_TEACH;
		$this->usr_created = self::getTimeStamp();
		$this->usr_rate = 0;
		$this->usr_reported = 0;
		$this->usr_email_verified = self::NOT_VERIFIED;
		$this->usr_subscribe = self::SUBSCRIBED;
		$this->usr_forward = self::EMAIL_FORWARD;
		$success = $this->save();
		
		if ($success){
			$emailConfirmation = new EmailConfirmations();
			$emailConfirmation->usersId = $this->usr_id;
			$send = $emailConfirmation->save();

			return $send;
		}
	}

	/**
     * Send a confirmation e-mail to the user if the account is not active
     */
    public function sendEmailConfirmation($id, $email)
    {
    	$user = self::findUserById($id);
    	//error_log("<pre>user".print_r($user,true)."</pre>");
    	if($user){
	    	$user->usr_email = $email;
			$save = $user->save();

			if ($save && $user->usr_email_verified == 0) {
				$emailConfirmation = new EmailConfirmations();
			    $emailConfirmation->usersId = $user->usr_id;
			    $success = $emailConfirmation->save();

			    return $success;
			    //error_log("<pre>success".print_r($success,true)."</pre>");
			}
    	}
    }  

    public function afterConfirmation($email)
    {
    	$user = self::findUserByEmail($email);
    	if($user){
    		//$this->usr_email_verified = self::VERIFIED;
    		$user->usr_email_verified = self::VERIFIED;    		
			$user->save();
    	}
    	return false;
    }

	public static function userNameExists($username)
    {
		$user = self::findFirst(array(
			'conditions' => 'usr_handle = ?1',
			'bind' => array(1 => $username)
		));
		return $user === false ? false : true;
	}

	public static function userEmailExists($email)
    {
		$user = self::findFirst(array(
			'conditions' => 'usr_email = ?1',
			'bind' => array(1 => $email)
		));
		return $user === false ? false : true;
	}

	public static function userExists($email)
    {
		$users = self::query()
		->where("usr_email = :email:")
		->bind(array("email" => $email))
		->execute();
        return count($users) > 0;
	}

	public static function findUserByEmail($email)
    {
		$user = User::find(array("conditions" => "usr_email = ?1",
				"bind" => array(1 => $email)));
		return $user->getFirst();
	}

	public static function findUserById($id)
    {
		$user = self::find(array("conditions" => "usr_id = ?1",
				"bind" => array(1 => $id)));
		return $user->getFirst();
	}

	public static function findUserByCritieria($q, $count=10, $offset=0, $columns="usr_id id,usr_firstname firstname,usr_lastname lastname,usr_avatar avatar,lat,lng,usr_lang lan, usr_teach")
    {
		$limit = array("number" => $count, "offset" => $offset);
		$users = self::find(array("conditions" => "(usr_firstname LIKE '$q%' OR usr_lastname LIKE '$q%' OR usr_email LIKE '$q%') AND usr_level='user' AND usr_teach = 1", "bind" => array("q"=>$q),
				"columns" => $columns,
				"limit" => $limit,
				));
		return $users->toArray();
	}

	public static function updateUserInfo($id, $firstname, $lastname, $email, $language, $postalcode, $lat, $lng)
    {
		$user = self::findUserById($id);
		if(count($user)===1){
			$user->usr_firstname = $firstname;
			$user->usr_lastname = $lastname;
			$user->usr_email = $email;
			$user->usr_lang = $language;
			//$user->usr_country = $country;
			$user->usr_postalcode = $postalcode;
			$user->lat = $lat;
			$user->lng = $lng;
			$success = $user->save();
			return $success;
		}else{
			return false;
		}
	}

	public static function changeEmail($id, $email)
    {
		$user = self::findUserById($id);
		if(count($user)===1){
			$user->usr_email = $email;
			$success = $user->save();
			return $success;
		}else{
			return false;
		}
	}

	public static function deactivate($id)
    {
		$user = self::findUserById($id);
		if(count($user)===1){
			$user->usr_deleted = self::DELETED;
			$success = $user->save();
			return $success;
		}else{
			return false;
		}
	}

	public static function activate($id)
    {
		$user = self::findUserById($id);
		if(count($user)===1){
			$user->usr_deleted = self::NOT_DELETED;
			$success = $user->save();
			return $success;
		}else{
			return false;
		}
	}

	public static function setUserAvatar($id, $avatar)
    {
		$user = self::findUserById($id);
		if(count($user)===1){
			$user->usr_avatar = $avatar;
			return $user->save();
		}
		return false;
	}

	public static function setUserCover($id, $cover)
    {
		$user = self::findUserById($id);
		if(count($user)===1){
			$user->usr_cover = $cover;
			return $user->save();
		}
		return false;
	}

	public static function updateUserBio($id, $about)
    {
		$user = self::findUserById($id);
		if(count($user)===1){
			$user->usr_about = $about;
			return $user->save();
		}
		return false;
	}


	public function validateUser($username, $password)
    {
		$user = self::findUserByEmail($username);
		if($user){
			$this->_security = new Security();
			$this->_security->setWorkFactor(14);
			if($this->_security->checkHash($password, $user->usr_password)){
				return $user;
			}
		}
		return false;
	}

	public function validation()
	{
		$this->validate(new Validator\Email([
				'field' => 'usr_email',
				'message' => 'Your email is invalid.'
				]));

		$this->validate(new Validator\Uniqueness([
				'field' => 'usr_email',
				'message' => 'Your email is already in use.'
				]));

		/* $this->validate(new Validator\StringLength([
				'field' => 'usr_password',
				'max' => '112',
				'min' => '4',
				'messageMaximum' => 'Your passowrd must be under 30 characters.',
				'messageMinimum' => 'Your password must be atleast 4 characters.'
				])); */

		if($this->validationHasFailed()){
			return false;
		}
	}

	public static function setTeach($id, $value)
    {
		$user = self::findUserById($id);
		$searchUserModel = new SearchUsers();
		if(count($user)===1){
			$user->usr_teach = ($value === true) ? self::TEACH : self::NOT_TEACH;
			$result = $user->save();
			if ($result){
				if ($user->usr_teach == 1){
					$searchUserModel->addSearch(array(
						'usr_id' => $user->usr_id,
						'lat' => $user->lat,
						'lng' => $user->lng,
						'rate' => ($user->usr_rate)?$user->usr_rate:0
					));
				}else{
					$searchUserModel->deleteSearchUser($user->usr_id);
				}
			}
			return $result;
		}
		return false;
	}

	public function checkVerification($id)
    {
		$this->usr_verification = $verify;
		$this->usr_avatar = $avatar;
		if(!empty($avatar)) {
			$verify++;
		}else{
			$verify--;
		}
	}

	public function changePassword($id, $password)
	{
		$user = self::findUserById($id);
		if($user){
			$this->_security = new Security();
			$this->_security->setWorkFactor(14);
			$user->usr_password = $this->_hash($password);
			return $user->save();
		}
		return false;
		
	}

    public function resetPassword($password)
    {
        $this->usr_password = $this->_hash($password);
        return $this->save();
    }

	public function validatePhone($string)
    {
	   	$numbersOnly = ereg_replace("[^0-9]", "", $string);
	    $numberOfDigits = strlen($numbersOnly);
	    if ($numberOfDigits == 7 or $numberOfDigits == 10) {
	        echo $numbersOnly;
	    } else {
	        echo 'Invalid Phone Number';
	    }
	}

	public static function getGPS($userId)
    {
		$user = self::findUserById($userId);
		if ($user){
			return array(
				'usr_postalcode' => $user->usr_postalcode,
				'lat' => $user->lat,
				'lng' => $user->lng,
				);
		}else{
			return array(
				'usr_postalcode' => '',
				'lat' => 0,
				'lng' => 0,
				);
		}
	}

	public static function reportUser($id)
    {
		$user = User::findFirst($id);
		$user->usr_reported = self::USER_REPORTED;
		//if user username not there, cannot save it, use user->getMessages() to see why it fail to save
		return $user->save();
	}

	public function subscribeUser($id, $status)
    {
		$user = Self::findUserById($id);
		$user->usr_subscribe = $status;
		$success = $user->save();

		return $success;
	}

	public function forwardEmail($id, $status)
    {
		$user = Self::findUserById($id);
		$user->usr_forward = $status;
		$success = $user->save();

		return $success;
	}

    private function _hash($password)
    {
        return $this->_security->hash($password);
    }

    public function saveTeachingRate($userId, $rate){
    	$user = User::findFirst($userId);
		$user->usr_rate = $rate;
		$searchUserModel = new SearchUsers();
		$searchUserModel->updatePrice($userId, $rate);
		//if user username not there, cannot save it, use user->getMessages() to see why it fail to save
		return $user->save();
    }

    public function savePostalCode($userId, $postalcode, $lat, $lng, $cityId){
    	$user = User::findFirst($userId);
		$user->usr_postalcode = $postalcode;
		$user->lat = $lat;
		$user->lng = $lng;
		$user->usr_city = $cityId;
		$success = $user->save();

		if($user->save() && $user->usr_teach == 1){
			$searchUserModel = new SearchUsers();
			$searchUserModel->updatePostal($userId, $lat, $lng);
		}

		return $success;
    }

    public function saveName($userId, $firstname, $lastname){
    	$user = User::findFirst($userId);
		$user->usr_firstname = $firstname;
		$user->usr_lastname = $lastname;
		$success = $user->save();

		return $success;
    }

    public function getAllSkillCityUserIds($cityId, $tagId){
    	$sql = 'SELECT * 
    			FROM users u
    			LEFT JOIN user_tags ut on u.usr_id = ut.user_id
    			WHERE u.usr_city = :city_id
    			  AND ut.tag_id = :tag_id
    			  AND u.usr_teach = :status_teach';

    	$params = ['city_id' => $cityId, 'tag_id' => $tagId, 'status_teach' => self::TEACH];
    	$result = $this->getResultSet($sql, $params)->toArray();

    	return $result;
    }

    public function countTotalUsers(){
    	$users = User::find();
    	$result = count($users);
    	//error_log("<pre>count".print_r($result,true)."</pre>"); 
    	return $result;
    }

	public function importUser($email, $password, $firstname, $lastname)
    {
		$this->usr_email = $email;
		$this->usr_password = $password;
		$this->usr_firstname = $firstname;
		$this->usr_lastname = $lastname;
		$this->usr_level = Permission::USER;
		$this->usr_deleted = User::NOT_DELETED;
		$this->usr_lang = self::DEFAULT_LANG;
		$this->usr_teach = User::TEACH;
		$this->usr_created = self::getTimeStamp();
		$this->usr_rate = 0;
		$this->usr_reported = 0;
		$this->usr_email_verified = self::NOT_VERIFIED;
		$this->usr_subscribe = self::SUBSCRIBED;
		$this->usr_forward = self::EMAIL_FORWARD;
		$success = $this->save();
		
		return $success;
	}

	public function checkAccountMerge($email)
	{
    	$user = User::findUserByEmail($email);
		if(empty($user->usr_password)){
			$check = true;
		}else{
			$check = false;
		}

		return $check;
    }

    public function mergeUser($email, $password, $firstname, $lastname)
    {
    	$user = User::findUserByEmail($email);
    	if($user){
			$user->usr_password = $this->_hash($password);
			$user->usr_firstname = $firstname;
			$user->usr_lastname = $lastname;
			$user->usr_level = Permission::USER;
			$user->usr_deleted = User::NOT_DELETED;
			$user->usr_lang = self::DEFAULT_LANG;
			$user->usr_teach = User::NOT_TEACH;
			$user->usr_created = self::getTimeStamp();
			$user->usr_rate = 0;
			$user->usr_reported = 0;
			$user->usr_email_verified = self::NOT_VERIFIED;
			$user->usr_subscribe = self::SUBSCRIBED;
			$user->usr_forward = self::EMAIL_FORWARD;
			$success = $user->save();
			
			if ($success){
				$emailConfirmation = new EmailConfirmations();
				$emailConfirmation->usersId = $this->usr_id;
				$send = $emailConfirmation->save();

				return $send;
			}
    	}
    		
    }

}
