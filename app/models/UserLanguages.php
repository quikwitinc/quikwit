<?php

use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;


class UserLanguages extends BaseModel
{
	protected $user_id;
	protected $language_id;
	protected $created;

	public function initialize()
	{
		$this->setSource("user_languages");
		parent::initialize();
		$this->belongsTo('user_id', 'User', 'usr_id');
		$this->belongsTo('language_id', 'Languages', 'id');
	}

	public function getUserLanguages($userId) {
		$sql =	'SELECT ulangs.*, l.* FROM user_languages ulangs INNER JOIN languages l ON ulangs.language_id=l.id WHERE ulangs.user_id=? AND l.status=?';
		$result = new Resultset(null, $this, $this->getReadConnection()->query($sql, array($userId, Languages::STATUS_APPROVED)));
		return $result->toArray();
	}

	public function add($userId, $langId){
		$userLangs = new UserLanguages();
		$userLangs->user_id = $userId;
		$userLangs->language_id = $langId;
		$userLangs->created = time();
		return $userLangs->save();
	}

	public function deleteLanguages($userId, $langId){
		$condition = "user_id=?1 AND language_id=?2";
		$record = self::find([$condition, "bind" => array(1=> $userId, 2 => $langId)]);
		$count = $record->count();
		if($record->count()===1){
			return $record->delete();
		}
		return false;
	}

	public function deleteAllLanguages($userId){
		return self::find(["user_id=".$userId])->delete();
	}
}