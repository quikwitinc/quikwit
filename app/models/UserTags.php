<?php

use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;


class UserTags extends BaseModel
{
	protected $user_id;
	protected $tag_id;
	protected $created;

	public function initialize()
	{
		$this->setSource("user_tags");
		parent::initialize();
		$this->belongsTo('user_id', 'User', 'usr_id');
		$this->belongsTo('tag_id', 'TagsStandard', 'id');
	}

	public function getUserTags($userId) {
		$sql =	'SELECT utags.*, t.* FROM user_tags utags INNER JOIN tags_standard t ON utags.tag_id=t.id WHERE utags.user_id=? AND t.status=?';
		$result = new Resultset(null, $this, $this->getReadConnection()->query($sql, array($userId, TagsStandard::STATUS_APPROVED)));
		return $result->toArray();
	}

	public function add($userId, $tagId){
		$userTags = new UserTags();
		$userTags->user_id = $userId;
		$userTags->tag_id = $tagId;
		$userTags->created = time();
		return $userTags->save();
	}

	public function deleteTag($userId, $tagId){
		$condition = "user_id=?1 AND tag_id=?2";
		$record = self::find([$condition, "bind" => array(1=> $userId, 2 => $tagId)]);
		$count = $record->count();
		if($record->count()===1){
			return $record->delete();
		}
		return false;
	}

	public function deleteAllTags($userId){
		return self::find(["user_id=".$userId])->delete();
	}
}