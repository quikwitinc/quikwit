<?php

use \Phalcon\Mvc\Model\Behavior\SoftDelete,
\Phalcon\Mvc\Model\Validator,
\Phalcon\Security,
\Phalcon\DI,
\Phalcon\Session\Bag;


class UsersConnections extends BaseModel
{
    public $usr_id;
    public $usr_connection_user;
    public $usr_provider;
    public $usr_hybridauth_session;
    public $usr_updated_at;
    public $usr_profile;
    public function initialize()
    {
        $this->setSource("users_connections");
        parent::initialize();
    }

    public function addConnection($usr_id, $provider, $hybridauth_session, $profile){
        $this->usr_connection_user = $usr_id;
        $this->usr_provider = $provider;
        $this->usr_hybridauth_session = $hybridauth_session;
        $this->usr_updated_at = date('Y-m-d H:i:s');
        $this->usr_profile = $profile;
        $status = $this->save();
        return $status;
    }

    public function retrieveConnect($usr_id, $provider){
        $connection = self::find(array("conditions" => "usr_connection_user = ?1 AND usr_provider = ?2",
                "bind" => array(1 => $usr_id, 2 => $provider)));
        return $connection->getFirst();
    }

    public function hasConnection($usr_connection_user){
        $connection = self::find(array("conditions" => "usr_connection_user = ?1",
                "bind" => array(1 => $usr_connection_user)));
        $result = $connection->toArray();
        $providers  = array();
        foreach ($result as $key => $value ){
            $providers[] = $value['usr_provider'];
        }
        return $providers;
    }

    public function setupSocialSession($usr_connection_user, $provider){
        require_once( APPLICATION_ROOT . DIRECTORY_SEPARATOR .  "vendor" . DIRECTORY_SEPARATOR . "hybridauth" . DIRECTORY_SEPARATOR . "hybridauth"  . DIRECTORY_SEPARATOR . "hybridauth" . DIRECTORY_SEPARATOR . "Hybrid" . DIRECTORY_SEPARATOR . "Auth.php" );
        $hasConnection = $this->retrieveConnect($usr_connection_user, $provider);
        if (!$hasConnection){
            return null;
        }
        $di = \Phalcon\DI\FactoryDefault::getDefault();
        $hybridauth_session_data = $hasConnection->usr_hybridauth_session;
        $authConfig = include APPLICATION_PATH . '/config/hybridauth.php';
        $di->getShared('hybridauth')->restoreSessionData( $hybridauth_session_data);
        $adapter = $di->getShared('hybridauth')->authenticate( $authConfig['translation'][$provider]);
        return $adapter;
    }

    public function updateConnection($connection, $hybridauth_session){
        $connection->usr_hybridauth_session = $hybridauth_session;
        $connection->usr_updated_at = date('Y-m-d H:i:s');
        $status = $connection->save();
        return $status;
    }
}