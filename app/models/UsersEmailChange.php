<?php
require_once __DIR__ . '/../../vendor/swiftmailer/swiftmailer/lib/swift_required.php';
use
\Phalcon\Mvc\Model\Validator,
\Phalcon\Security,
\Phalcon\DI;

class UsersEmailChange extends BaseModel
{

    public $id;
    public $user_id;
    public $old_email;
    public $new_email;
    public $created;
    public $modified;

    public function initialize()
    {
        $this->setSource("users_email_change");
        parent::initialize();
    }

    public function addEmailChange($userId, $oldEmail, $newEmail)
    {
        $this->user_id = $userId;
        $this->old_email = $oldEmail;
        $this->new_email = $newEmail;
        $this->created = $this->getTimeStamp();
        $this->save();
    }

    /**
	 * Function use to send recovery email
	 *
	 * @param string $email
	 * @param string $usename
	 * @param string $hash
	 */    
    public function sendEmailChangeConfirmation($email, $newEmail){		
		// get data from config
        $contacts = $this->getDI()->get('config')->get('contacts')->get('contact-us');
        $gateway = $this->getDI()->get('config')->get('gmail-gateway');
        $subject = "Your account's email is changed on Quikwit.co";
		$name = 'confirmEmailChange';
		
		// Create transport using smtp credential
        $transport = Swift_SmtpTransport::newInstance($gateway->host, $gateway->port, $gateway->security);
		$transport->setUsername($gateway->username);
		$transport->setPassword($gateway->password); 
		
		// Create the message
		$message = Swift_Message::newInstance();
		$message->setTo($email);
		$message->setSubject($subject);
		$message->setBody($this->getTemplate($name, $email, $newEmail),'text/html');
		$message->setFrom($contacts->from, $contacts->name);
		 
		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		$mailer->send($message);   
		return true;     
    }
    
    /**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplate($name, $oldEmail, $newEmail)
	{
		$userModel = new User();
		$user = $userModel->findUserByEmail($email);
		$userfirst = $user->usr_firstname;
		$userlast = $user->usr_lastname;
		$view = new \Phalcon\Mvc\View();
 		$view->setViewsDir('app/views/');
 		$view->start();
 		$content = $view->getRender('templates-email', $name, array('oldEmail' => $oldEmail,
		 					  'newEmail' => $newEmail, 'firstname' => $userfirst, 'lastname' => $userlast));
 		$view->finish();
		return $content;
	}
}