<?php

use \Phalcon\Mvc\Model\Behavior\SoftDelete,
\Phalcon\Mvc\Model\Validator,
\Phalcon\Security,
\Phalcon\DI,
\Phalcon\Session\Bag;
use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class UsersExperience extends BaseModel
{
    public $id;
    public $title;
    public $content;
    public $start_month;
    public $start_year;
    public $end_month;
    public $end_year;
    public $user_id;
    public $create_date;
    const PRESENT = 1;
    public function initialize()
    {
        $this->setSource("users_experience");
        parent::initialize();
        $this->belongsTo('user_id', 'User', 'usr_id');
    }

    public function addExperience($user_id, $title, $content, $start_month, $start_year,
        $end_year, $end_month){
        $this->user_id = $user_id;
        $this->title = $title;
        $this->content = $content;
        $this->start_month = $start_month;
        $this->start_year = $start_year;
        $this->end_month = $end_month;
        $this->end_year = $end_year;
        $this->create_date = date('Y-m-d H:i:s');
        $status = $this->save();
        $searchUserModel = new SearchUsers();
        $searchUserModel->updateExperience($user_id, true);
        return $status;
    }

    public function findExperienceById($id){
        $review = self::find(array("conditions" => "id = ?1",
                "bind" => array(1 => $id)));
        return $review->getFirst();
    }

    public function hasExperience($userId){
        $sql = "SELECT count(*) as num FROM users_experience WHERE user_id = " . $userId;
        $result = new Resultset(null, $this, $this->getReadConnection()->query($sql, array()));
        $num = $result->toArray()[0];
        return $num['num'] > 0;
    }
}