<?php
require_once __DIR__ . '/../../vendor/swiftmailer/swiftmailer/lib/swift_required.php';
use
\Phalcon\Mvc\Model\Validator,
\Phalcon\Security,
\Phalcon\DI;

class UsersPasswordRecovery extends BaseModel
{
    const STATUS_PENDING = 0;
    const STATUS_COMPLETED = 1;

    public $id;
    public $user_id;
    public $old_password;
    public $hash;
    public $created;
    public $status;
    public $modified;

    public function initialize()
    {
        $this->setSource("users_password_recovery");
        parent::initialize();
    }

    public function addPasswordRecovery($userId, $oldPassword = '')
    {
    	$hash = $this->getHash();
        $this->user_id = $userId;
        $this->hash = $hash;
        $this->old_password = $oldPassword;
        $this->created = $this->getTimeStamp();
        $this->status = self::STATUS_PENDING;
        $this->save();
        return $hash;
    }

    public function isExpired()
    {
        return time() - $this->created > 86400;
    }

    public function isCompleted()
    {
        return (int)$this->status === self::STATUS_COMPLETED;
    }

    public function isPending()
    {
        return (int)$this->status === self::STATUS_PENDING;
    }


    public function isValid()
    {
        $valid = true;
        if ($this->isExpired()) {
            $valid = false;
        } else if (!$this->isPending()){
            $valid = false;
        }
        return $valid;
    }

    public static function getUserRecovery($hash)
    {
        $recovery = self::findFirst([
            'conditions' => 'hash = ?1',
            'bind' => array(1 => $hash),
            'order' => 'created DESC'
        ]);
        return $recovery ? $recovery : false;
    }

    public function setRecoveryCompleted()
    {
        $this->status = self::STATUS_COMPLETED;
        $this->modified = $this->getTimeStamp();
        $status = $this->save();
        return $status;
    }

    protected function getHash()
    {
        return md5($this->generateRandomString() . time());
    }

    protected function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    /**
	 * Function use to send recovery email
	 *
	 * @param string $email
	 * @param string $usename
	 * @param string $hash
	 */    
    public function sendPasswordRecoveryEmail($email, $usename, $hash){		
		// get data from config
        $contacts = $this->getDI()->get('config')->get('contacts')->get('contact-us');
        $gateway = $this->getDI()->get('config')->get('gmail-gateway');
        $subject = "Password recovery email from QuikWit Inc.";
		$name = 'passwordReset';
		$params = array('hash' => $hash);
		
		// Create transport using smtp credential
        $transport = Swift_SmtpTransport::newInstance($gateway->host, $gateway->port, $gateway->security);
		$transport->setUsername($gateway->username);
		$transport->setPassword($gateway->password); 
		
		// Create the message
		$message = Swift_Message::newInstance();
		$message->setTo(array(
		  $email => $usename
		));
		$message->setSubject($subject);
		$message->setBody($this->getTemplate($name, $params, $email),'text/html');
		$message->setFrom($contacts->from, $contacts->name);
		 
		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		$mailer->send($message);   
		return true;     
    }
    
    /**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplate($name, $params, $email)
	{
        $userModel = new User();
        $user = $userModel->findUserByEmail($email);
        $userfirst = $user->usr_firstname;
        $userlast = $user->usr_lastname;
		$view = new \Phalcon\Mvc\View();
 		$view->setViewsDir('app/views/');
 		$view->start();
 		$content = $view->getRender('templates-email', $name,
		 				array('resetUrl' => $this->getDI()->get('config')->get('webURL').'/signin/resetPassword?hash=' . $params['hash'],
		 					  'email' => $email, 'firstname' => $userfirst, 'lastname' => $userlast)
		 			);
 		$view->finish();
		return $content;
	}
}