<?php

use \Phalcon\Mvc\Model\Validator,
	\Phalcon\DI;

use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;


class Video extends BaseModel
{
    const DELETED = 1;
    const NOT_DELETED = 0;

    public $vid_id;
    public $vid_user;
    public $vid_type;
    public $vid_name;
    public $vid_url;
    public $vid_created;


    public function initialize()
    {
        $this->setSource("videos");
        parent::initialize();
    }

    public function addVideo($user, $title, $tags, $name, $videoFile){
    	$this->vid_user = $user;
        $this->vid_type = 0;
        $this->vid_name = $title;
        $this->vid_url = $videoFile;
        $this->vid_created = self::getTimeStamp();
        $this->save();
        $video = $this->vid_id;
        foreach ($tags as $tag) {
        	$t = new VideoTags();
			$t->video_id = $video;
			$t->tag_id = $tag;
            $t->created = self::getTimeStamp();
			$t->save();
        }
        return true;
    }

    public function getUserVideos($userId){
    	$sql = "SELECT *
				FROM videos
				WHERE vid_user=? ORDER BY vid_created DESC LIMIT 1";

   		return new Resultset(null, $this, $this->getReadConnection()->query($sql, array($userId)));
    }

    public function getUrl($videoId){
    	$video = $self::find(array("condition" => "vid_id = ?1",
    			"bind" => array(1 => $videoId)));
    	return $video->getFirst();
    }

    public function videoExists($title){
        $users = self::query()
        ->where("vid_url = :url:")
        ->bind(array("url" => $title))
        ->execute();
        return count($videos) > 0;
    }

    public function validation()
    {
        $this->validate(new Validator\Uniqueness([
            'field' => 'vid_url',
            'message' => 'This url has already been used.'
        ]));

//        $this->validate(new Validator\StringLength([
//            'field' => 'vid_title',
//            'max' => '50',
//            'min' => '10',
//            'messageMaximum' => 'Your title must be under 50 characters.',
//            'messageMinimum' => 'Your title must be atleast 4 characters.'
//        ]));

        if($this->validationHasFailed()){
            return false;
        }

    }

    public function getNumVideoByUser($userId){
        $sql = "SELECT count(*) as num_video
                FROM videos
                WHERE vid_user=?";
        return new Resultset(null, $this, $this->getReadConnection()->query($sql, array($userId)));
    }
    
    public function updateVideo($id, $title, $tags, $name, $videoFile) {
    	$user = self::findVideoByVidUser($id);
		if(count($user)===1){
	        $user->vid_name = $title;
	        if($videoFile!='') {
	        	$user->vid_url = $videoFile;
	        }
			$user->save();
            $video = $this->vid_id;
			foreach ($tags as $tag) {
        		$t = new VideoTags();
                $t->video_id = $video;
                $t->tag_id = $tag;
                $t->created = self::getTimeStamp();
                $t->save();
	        }
		}else{
			return false;
		}
    }
    
    public static function findVideoByVidUser($id)
    {
		$user = self::find(array("conditions" => "vid_user = ?1",
				"bind" => array(1 => $id)));
		return $user->getFirst();
	}
}