<?php

use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;


class VideoTags extends BaseModel
{
	protected $video_id;
	protected $tag_id;
	protected $created;

	public function initialize()
	{
		$this->setSource("video_tags");
		parent::initialize();
		$this->belongsTo('video_id', 'Video', 'vid_id');
		$this->belongsTo('tag_id', 'TagsStandard', 'id');
	}

	public function add($videoId, $tagId){
		$videoTags = new VideoTags();
		$videoTags->video_id = $videoId;
		$videoTags->tag_id = $tagId;
		$videoTags->created = time();
		return $videoTags->save();
	}

	public function deleteTag($videoId, $tagId){
		$condition = "video_id=?1 AND tag_id=?2";
		$record = self::find([$condition, "bind" => array(1=> $videoId, 2 => $tagId)]);
		$count = $record->count();
		if($record->count()===1){
			return $record->delete();
		}
		return false;
	}

	public function deleteAllTags($videoId){
		return self::find(["video_id=".$videoId])->delete();
	}
}