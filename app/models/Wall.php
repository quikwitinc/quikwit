<?php

class Wall extends BaseModel{
	
	const STATUS_SUBSCRIBED = 1;
	const STATUS_UNSUBSCRIBED = 0;
	
	protected $id;
	protected $media;
	protected $title;
	protected $content;
	protected $created;
	protected $modified;
	protected $createdby;

	public function initialize()
	{
		$this->setSource("wall");
		parent::initialize();
	}

	public function addWallPost($user_id, $title, $content, $url){
			$this->title = $title;
			$this->content = $content;
			$tp = time();
			$this->created = $tp;
			$this->modified = $tp;
			$this->createdby = $user_id;
			$this->media = $url;
			return $this->save();
	}
	
	public function getPostsByUserId($user_id){
		$posts = self::find(array("conditions" => "createdby = :id:", "bind" => array("id" => $user_id)));
		return $posts;
	}
	
	
	
	

}