{% extends "templates/home.volt" %}

{% block title %}
About Us | QuikWit
{% endblock %}

{% block content %}
<!-- Media Container -->

    <!-- Intro -->
    <section class="site-section site-section-light site-section-top themed-background">
        <div class="container">
            
            <h2 class="h3 text-center animation-slideUp" style="padding-top:20px"><strong>Why did we start QuikWit?</strong></h2>
     
    </section>
    <!-- END Intro -->

    <!-- Gmaps.js (initialized in js/pages/about.js), for more examples you can check out http://hpneo.github.io/gmaps/examples.html -->
    {#<div id="gmap-top" class="media-map"></div>#}
   

<!-- END Media Container -->

<!-- Company Info -->
<section class="site-content site-section" style="padding-bottom: 200px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="padding-top: 40px; font-size: 16px">
                <div class="site-block">
                    <p>The search for an event photographer or an appliance installation expert that fits your budget can be time-consuming and futile. QuikWit is a platform that connects you to freelancers in your area across Canada. Whether in need of a language tutor, catering services, graphic designer or event planner for your personal or business objectives, we want to be your go to site for the help you need.</p>
                </div>

                <div class="site-block">
                    <p>QuikWit’s co-founders are two recent graduates from University of Toronto who in the past struggled to find programmers, web designers and squash instructors to work with in person. Their checklist included independent freelancers who understood their need for expedient, cost-effective and quality service with a personal touch! This spurred the idea for QuikWit. The great thing about this platform is, it helps you find an expert right in your area and supports local businesses and freelancers.</p>
                </div>             
                
            </div>
        </div>
    </div>
</section>
<!-- END Company Info -->

{% endblock %}

{% block script %}
<!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps -->
<script src="third-party/proui/frontend/js/helpers/gmaps.min.js"></script> 

<!-- Load and execute javascript code used only in this page -->
<script src="third-party/proui/frontend/js/pages/about.js"></script>
<script>$(function(){ About.init(); });</script>
{% endblock %}