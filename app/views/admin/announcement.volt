{% extends "templates/home.volt" %}

{% block head %}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top" style="background-color: black;">
    <div class="container">
        <h1 class="text-center animation-slideDown"><i class="fa fa-arrow-right"></i> <strong>QuikWit Admin</strong></h1>
        <h2 class="h3 text-center animation-slideUp"></h2>
    </div>
</section>
<!-- END Intro -->	

<section class="site-section site-content" style="padding-bottom: 200px;">
	<div class="container">
		<div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-lg-5 col-lg-offset-3 site-block">
		        <legend>Announcement</legend>
				 <!-- <form class="form-horizontal"> -->
				 {{ form("admin/send", "id" : "message-form", "method" : "post", "class" : "form-horizontal") }}
				    <div class="form-group">
				      {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }}
				       <label for = "message-title" class = "col-lg-2 control-label">Title:</label>
				       <div class = "col-lg-10">
				         <!-- <input type = "text" class = "form-control" id = "message-title" placeholder = "[Type Title Here]"> -->
				        {{ text_field('message[title]', "id" : "message-title", "placeholder" : "[Type Title Here]", "class": "form-control") }}
				       </div>
				    </div>
				    <div class = "form-group">
				       <label for = "review-body" class = "col-lg-2 control-label">Message:</label>
				       <div class = "col-lg-10">
				       <!-- <textarea class="form-control" rows="8" placeholder="[Type Message Here]"></textarea> -->
				       {{ text_area('message[message]', "id" : "message-title", "placeholder" : "[Type Message Here]", "class": "form-control", "rows": 8) }}
				       </div>
				    </div>
				    <button type="submit" class="btn btn-success pull-right">Send</button>
				</div>
				{{ end_form() }}
			</div>
		</div>
	</div>
</section>
{% endblock %}

{% block script %}
{% endblock %}