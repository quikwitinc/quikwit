{% extends "templates/home.volt" %}

{% block head %}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background-default">
    <div class="container">
        <h1 class="text-center animation-slideDown"><i class="fa fa-arrow-right"></i> <strong>QuikWit Admin</strong></h1>
        <h2 class="h3 text-center animation-slideUp"></h2>
    </div>
</section>
<!-- END Intro -->	

<section class="site-section site-content">
	<div class="container">
		<div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-lg-5 col-lg-offset-3 site-block">
		        <legend>Compare Two Users (solve relationship problems!)</legend>
				<p>Enter user IDs or email</p>
				<br>
				<form class="form-horizontal">
					<div class ="form-group">
			        	<label class = "col-lg-3 control-label">User 1:</label>
				        <div class="col-lg-8">
				            <input class = "form-control" type="text" name="user1">
				        </div>
			      	</div>
			      	<div class ="form-group">
				        <label class = "col-lg-3 control-label">User 2:</label>
				        <div class="col-lg-8">
				            <input class = "form-control" type="text" name="user2">
				        </div>
			    	</div>
			    	<div class="form-group form-actions">
			    	    <div class="col-xs-9">
			    	    </div>
			    	    <div class="col-xs-3">
			    	        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Find</button>
			    	    </div>
			    	</div>
				</form>
			</div>
		</div>
	</div>
</section>
{% endblock %}