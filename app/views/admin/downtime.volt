{% extends "templates/home.volt" %}

{% block head %}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background-default">
    <div class="container">
        <h1 class="text-center animation-slideDown"><i class="fa fa-arrow-right"></i> <strong>QuikWit Admin</strong></h1>
        <h2 class="h3 text-center animation-slideUp"></h2>
    </div>
</section>
<!-- END Intro -->	

<section class="site-section site-content">
	<div class="container">
		<div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-lg-5 col-lg-offset-3 site-block">
		        <legend>Downtime Notice</legend>
				<textarea class="form-control" rows="6"></textarea>
				<button class="btn btn-info">Set Notice</button>
			</div>
		</div>
	</div>
</section>
{% endblock %}