{% extends "templates/home.volt" %}

{% block head %}
<style>
table {
	border-spacing: 10px;
	border-collapse: separate;
}
</style>
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top" style="background-color: black;">
    <div class="container">
        <h1 class="text-center animation-slideDown"><i class="fa fa-arrow-right"></i> <strong>QuikWit Admin</strong></h1>
        <h2 class="h3 text-center animation-slideUp"></h2>
    </div>
</section>
<!-- END Intro -->	

<section class="site-section site-content" style="padding-bottom: 200px;">
	<div class="container">
		<div class="row">
            <div class="col-sm-8 col-sm-offset-2 site-block">
		        <legend>Feedbacks</legend>
				<div class="table-responsive">
					<table class"table table-vcenter">
						<thead>
							<tr>
								<th>Feedbacks</th>
								<th>User</th>
								<th>Created</th>
							</tr>
						</thead>
						<tbody>
							{% if feedbacks is defined %}
							    {% for feedback in feedbacks %}
								<tr>
									<td name="feedback">{{ feedback['feedback'] }}</td>
									<td name="user"><a href="{{ url('user/profile/' ~ feedback['user_id']) }}">{{ feedback['user_id'] }}</a></td>
									<td name="created">{{ feedback['created'] }}</td>
								</tr>
								{% endfor %}
							{% endif %}
						</tbody>		
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
{% endblock %}

{% block script %}
{% endblock %}