{% extends "templates/home.volt" %}

{% block head %}
<style>
table {
	border-spacing: 50px;
	border-collapse: separate;
}
</style>
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top" style="background-color: black;">
    <div class="container">
        <h1 class="text-center animation-slideDown"><i class="fa fa-arrow-right"></i> <strong>QuikWit Admin</strong></h1>
        <h2 class="h3 text-center animation-slideUp"></h2>
    </div>
</section>
<!-- END Intro -->	

<section class="site-section site-content" style="padding-bottom: 200px;">
	<div class="container">
		<div class="row">
            <div class="site-block">
            	<div class="col-xs-12">
			        <legend>Lessons</legend>
	        		<div class="col-md-6">
		        		<input id='lessonId' placeholder='Lesson Id' class="form-control">
		        	</div>
		        	<button id='btnLessonFind' class="btn btn-info btn-sm col-md-1">Find</button>
	        	</div>
	        	<br>
	        	<div class="col-xs-12">
					<div class="table-responsive">
						<table class"table table-vcenter">
							<thead>
								<tr>
									<th class="text-center">Lesson Id</th>
									<th class="text-center">Skill</th>
									<th class="text-center"><i class="gi gi-user"></i></th>
									<th >Date Created</th>
									<th>Lesson Status</th>
									<th>Price</th>
									<th>Scheduled Date and Time</th>
									<th>Start Date</th>
									<th>Completion Date</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id='id'></td>
									<td id='skill'></td>
									<td id='user'></td>
									<td id='created'></td>
									<td id='state'></td>
									<td id='price'></td>
									<td id='teachid'></td>
									<td id='next'></td>
									<td id='started'></td>
									<td id='completed'></td>
								</tr>
							</tbody>		
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
{% endblock %}

{% block script %}
<script>
$(document).ready(function() {
	$('#btnLessonFind').click(function() {
		var lessonId = $('#lessonId').val();
		$.ajax({
		    url: '/admin/retrieveContract',
		    type: 'POST',
		    dataType: 'json',
		    data:{
		        lessonId : lessonId
		    },
		    success: function(data) {
		        //var result = JSON.parse(data);
		        console.log(data);
		        var user = "<a href='{{ url('user/profile/" + data.cnt_user + "') }}'>" + data.cnt_user + "</a>";
		        $("#id").html(data.cnt_id);
		        $("#skill").html(data.cnt_tagid);
		        $("#user").html(user);
		        $("#created").html(data.cnt_created);
		        $("#state").html(data.cnt_state);
		        $("#price").html(data.cnt_unitprice);
		        $("#teachid").html(data.cnt_teachid);
		        $("#next").html(data.cnt_next);
		        $("#started").html(data.cnt_started);
		        $("#completed").html(data.cnt_completed); 
		    },
		    error: function(xhr, ajaxOptions, thrownError) {
		        alert('Sorry, error occured.');
		    }
		});
	});
});
</script>
{% endblock %}