{% extends "templates/home.volt" %}

{% block head %}
<style>
ul {
	list-style-type: none;
	padding-left: 0px;
}
td {
	padding: 10px;
}
table {
	border-spacing: 10px;
	border-collapse: separate;
}
</style>
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top" style="background-color: black;">
    <div class="container">
        <h1 class="text-center animation-slideDown"><i class="fa fa-arrow-right"></i> <strong>QuikWit Admin</strong></h1>
        <h2 class="h3 text-center animation-slideUp"></h2>
    </div>
</section>
<!-- END Intro -->

<section class="site-section site-content" style="padding-bottom: 200px; padding-top: 200px;">
	<div class="container">
            <div class="col-lg-6 site-block">
				<legend>Find User</legend>
				<div class="col-lg-5">
					<input id="inputId" class="form-control" placeholder="Id">
					<br>
					<br>
					<input id="inputEmail" class="form-control" placeholder="Email">
				</div>
				<div class="col-lg-3">
					<button class="btn btn-info btn-sm" id="btnIdFind">Find</button>
					<br>
					<br>
					<br>
					<button class="btn btn-info btn-sm" id="btnEmailFind">Find</button>
				</div>
			</div>
			<div class="table-responsive col-lg-6">
				<table class"table table-vcenter">
					<tr>
						<th>Id</th>
						<td id="id"></td>
					</tr>
					<tr>
						<th>Deleted</th>
						<td id="deleted"></td>
					</tr>
					<tr>
						<th>Level</th>
						<td id="level"></td>
					</tr>
					<tr>
						<th>Email</th>
						<td id="email"></td>
					</tr>
					<tr>
						<th>Firstname</th>
						<td id="first"></td>
					</tr>
					<tr>
						<th>Lastname</th>
						<td id="last"></td>
					</tr>
					<tr>
						<th>Avatar</th>
						<td id="avatar"></td>
					</tr>
					<tr>
						<th>Postal Code</th>
						<td id="postal"></td>
					</tr>
					<tr>
						<th>Created Date</th>
						<td id="created"></td>
					</tr>
					<tr>
						<th>About</th>
						<td id="about"></td>
					</tr>
					<tr>
						<th>Teacher</th>
						<td id="teach"></td>
					</tr>
					<tr>
						<th>Email Verfied</th>
						<td id="email_ver"></td>
					</tr>
					<tr>
						<th>Reported</th>
						<td id="report"></td>
					</tr>
					<tr>
						<th>Rate</th>
						<td id="rate"></td>
					</tr>
					<tr>
						<th>Subscription</th>
						<td id="subscribe"></td>
					</tr>				
				</table>
			</div>
		</div>
	</div>
</section>
{% endblock %}

{% block script %}
<script>
$(document).ready(function() {
	$('#btnIdFind').click(function() {
		var id = $('#inputId').val();
		$.ajax({
		    url: '/admin/findUserById',
		    type: 'POST',
		    dataType: 'json',
		    data:{
		        id : id
		    },
		    success: function(data) {
		        //var result = JSON.parse(data);
		        console.log(data);
		        $("#id").html(data.usr_id);
		        $("#deleted").html(data.usr_deleted);
		        $("#level").html(data.usr_level);
		        $("#email").html(data.usr_email);
		        $("#first").html(data.usr_firstname);
		        $("#last").html(data.usr_lastname);
		        $("#avatar").html(data.usr_avatar);
		        $("#postal").html(data.usr_postalcode);
		        $("#created").html(data.usr_created);
		        $("#about").html(data.usr_about);
		        $("#teach").html(data.usr_teach);
		        $("#email_ver").html(data.usr_email_verified);
		        $("#report").html(data.usr_reported);
		        $("#rate").html(data.usr_rate);
		        $("#subscribe").html(data.usr_subscribe);
		    },
		    error: function(xhr, ajaxOptions, thrownError) {
		        alert('Sorry, error occured.');
		    }
		});
	});

	$('#btnEmailFind').click(function() {
		var email = $('#inputEmail').val();
		$.ajax({
		    url: '/admin/findUserByEmail',
		    type: 'POST',
		    dataType: 'json',
		    data:{
		        email : email
		    },
		    success: function(data) {
		        //var result = JSON.parse(data);
		        console.log(data);
		        $("#id").html(data.usr_id);
		        $("#deleted").html(data.usr_deleted);
		        $("#level").html(data.usr_level);
		        $("#email").html(data.usr_email);
		        $("#first").html(data.usr_firstname);
		        $("#last").html(data.usr_lastname);
		        $("#avatar").html(data.usr_avatar);
		        $("#postal").html(data.usr_postalcode);
		        $("#created").html(data.usr_created);
		        $("#about").html(data.usr_about);
		        $("#teach").html(data.usr_teach);
		        $("#email_ver").html(data.usr_email_verified);
		        $("#report").html(data.usr_reported);
		        $("#rate").html(data.usr_rate);
		        $("#subscribe").html(data.usr_subscribe);
		    },
		    error: function(xhr, ajaxOptions, thrownError) {
		        alert('Sorry, User not found.');
		    }
		});
	});
});
</script>
{% endblock %}