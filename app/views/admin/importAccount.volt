{% extends "templates/home.volt" %}

{% block title %}
Import Account | QuikWit
{% endblock %}

{% block head %}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        {#<h1 class="text-center animation-slideDown"><strong>Log In</strong></h1>#}
        <h1 class="h3 text-center animation-slideUp" style="font-size: 200%"><strong>Import New Account</strong></h1>
    </div>
</section>
<!-- END Intro -->

<!-- Log In -->
<section class="site-content site-section" style="padding-bottom: 200px;">
    <div class="container">
        <div class="row" style="margin-top: 50px">
            <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 site-block">
                <!-- Sign Up Form -->
                {{ form("admin/addAccount", "method" : "post", "class" : "form-horizontal") }}
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                {{ text_field('firstname', "placeholder" : "First Name", "class" : "form-control input-lg") }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                {{ text_field('lastname', "id" : "rform-lastname", "placeholder" : "Last Name", "class" : "form-control input-lg") }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                {{ email_field('email', "id" : "rform-email", "placeholder" : "Email", "class" : "form-control input-lg") }}
                            </div>
                        </div>
                    </div>
                    </br>
                    <div class="form-group form-actions">
                        <div class="col-xs-12 text-center">
                            <button type="submit" class="btn btn-lg btn-primary" id="signUpButton"><strong>Add</strong></button>
                        </div>
                    </div>
                </div>
                    {# <input id="rform-csrf" type="hidden" name="<?php echo $this->security->getTokenKey() ?>" value="<?php echo $this->security->getTokenKey() ?>"/> 
                    <input id="rform-csrf" type="hidden" name="{{ tokenKey }}" value="{{ token }}"/> #}
                    {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }} 
                {{ end_form() }}
                <!-- END Sign Up Form -->
            </div>
        </div>
    </div>
</section>
<!-- END Log In -->
{% endblock %}
