{% extends "templates/home.volt" %}

{% block title %}
Import Post | QuikWit
{% endblock %}

{% block head %}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        {#<h1 class="text-center animation-slideDown"><strong>Log In</strong></h1>#}
        <h1 class="h3 text-center animation-slideUp" style="font-size: 200%"><strong>Import New Post</strong></h1>
    </div>
</section>
<!-- END Intro -->

<!-- Log In -->
<section class="site-content site-section" style="padding-bottom: 200px;">
    <div class="container">
        <div class="row" style="margin-top: 50px">
            <div class="col-sm-6 col-sm-offset-3 site-block">
                <div class="response-message" style="font-size: 16px;"></div>
                <br>
                <form id="admin-request-form">
                  <div class="form-group">
                     <label style="font-size:16px">What do you need help with today?</label>  
                     <?php 
                     echo Phalcon\Tag::select(array('tagId', 
                                                   TagsStandard::find(array(
                                                      "status = 1",
                                                      "order" => "name")), 
                                                   "using" => array("id", "name"),
                                                   "class" => "select-chosen form-control",
                                                   "type" => "text",
                                                   "data-role" => "tagsinput",
                                                   "useEmpty" => "true",
                                                   "emptyText" => "Select a category"
                                                   )); ?>                    
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">Please provide a title for this gig.</label>
                     {{ text_field('title', 'class' : 'form-control') }}
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">Please provide the details of the gig.</label>
                     {{ text_area('task', 'class' : 'form-control', 'rows' : '8') }}
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">Do you prefer to pay by the hour or a flat rate?</label>
                     <select class="form-control" name="fixed" style="font-size:14px">
                        <option value="0" >Hourly - pay by the hour</option>
                        <option value="1">Flat rate - pay fixed amount</option>
                     </select>
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">Is this gig nonrecurring or recurring?</label>
                     <select class="form-control" name="multiple" style="font-size:14px">
                        <option value="0">Nonrecurring</option>
                        <option value="1">Recurring</option>
                     </select>
                  </div> 
                  <br> 
                  <div class="form-group">
                     <label style="font-size:16px">What is your budget? Please put the amount here in Canadian Dollars.</label>
                     <div class="input-group m-b">
                        <span class="input-group-addon">$</span>
                        {{ text_field('price', 'class' : 'form-control') }}
                     </div>
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">What is your estimation of the hours needed to complete this gig?</label>
                     <div class="input-group m-b">
                        {{ text_field('hour', 'class' : 'form-control') }}
                        <span class="input-group-addon">Hours</span>
                     </div>
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">What sort of quality are you looking for?</label>
                     <select class="form-control" name="quality" style="font-size:14px">
                        <option value="0">As cheap as possible.</option>
                        <option value="1">A mix of value and experience.</option>
                        <option value="2">Price is not the biggest factor. I want the highest quality.</option>
                     </select>
                  </div>
                  <br>
                  <div class="form-group">
                     <label for="datetime-title" style="font-size:16px">When do you need this task done?</label>
                     <div class='input-group date datetimepicker mb-lg'>
                        <input type='text' name="datetime" id="datetimepickers" class="form-control" readOnly />
                        <span class="input-group-addon">
                           <span class="fa fa-calendar"></span>
                        </span>
                     </div>
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">Where do you need this gig done? Please put in the postal code and city.</label>
                     <div class="col-lg-12" style="margin-bottom: 20px; padding-left: 0px;">
                        <div class="col-lg-5">
                           {{ text_field('postal', 'class' : 'form-control', 'placeholder' : 'Enter postal code') }}
                        </div>
                        <div class="col-lg-7">
                           <?php 
                           echo Phalcon\Tag::select(array('cityId', 
                                                      City::find(), 
                                                      "using" => array("id", "name"),
                                                      "class" => "form-control",
                                                      "type" => "text"
                                                      )); ?> 
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">Do you require the freelancer to travel to your location?</label>
                     <select class="form-control" name="travel" style="font-size:14px">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                     </select>
                  </div> 
                  {#<div class="form-group">
                     <label>Within hours, you'll be introduced to interested and available appliance installation specialists who will send you custom quotes based on your answers to the previous questions.</label>
                  </div>#}
                  <div class="form-group">
                     <label style="font-size:16px">To wrap things up, please put in your details.</label>
                  </div>
                  <div class="form-group">
                     {#<label style="font-size:16px">First Name</label>#}
                     {{ text_field('firstname', "placeholder" : "First Name", "class" : "form-control") }}
                  </div>
                  <div class="form-group">
                     {#<label style="font-size:16px">Last Name</label>#}
                     {{ text_field('lastname', "id" : "rform-lastname", "placeholder" : "Last Name", "class" : "form-control") }}
                  </div>
                  <div class="form-group">
                     {#<label style="font-size:16px">Email</label>#}
                     {{ email_field('email', "id" : "rform-email", "placeholder" : "Email", "class" : "form-control") }}         
                  </div>
                  <div class="form-group">
                     {{ text_field('phone', "id" : "rform-phone", "placeholder" : "Phone Number", "class" : "form-control") }}
                  </div>
                  <div class="col-xs-12 text-center" style="padding-top: 20px">
                     <button type="button" id="postButton" class="btn btn-success">Post</button>
                  </div>
                  {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }} 
               </form>
            </div>
        </div>
    </div>
</section>
<!-- END Log In -->
{% endblock %}

{% block script %}
<script type="text/javascript">
   $("#postButton").click(function(e) {

         e.preventDefault();
         var form = $("#admin-request-form");
         if(!form.valid()){
            return false;
         }else{
            $('.response-message').html('Posting...');
            window.scrollTo(0, 0);
         }
         
         $.ajax({
            type : 'POST',
            url : '/admin/addRequest',
            data : form.serialize(),
            success : function(response) {
               if(response.success == true){
                  $('.response-message').css('color', 'rgb(30, 192, 80)');
               }else{
                  $('.response-message').css('color', 'red');
               }
               $('.response-message').html(response.message); 
               //if (response.success) {
                  //form.fadeOut();
               //} 
               window.scrollTo(0, 0);

               if(response.content){
                  var cityId = response.content[0];
                  var tagId = response.content[1];

                  $.ajax({
                     type: 'POST',
                     url : '/post/sendRequest',
                     data : {
                        cityId : cityId,
                        tagId : tagId
                     }
                  });  
               }  
            }
         });

   });
</script>
{% endblock %}
