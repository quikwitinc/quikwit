{% extends "templates/home.volt" %}

{% block head %}
<style>
ul {
	list-style-type: none;
	padding-left: 0px;
}
</style>
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top" style="background-color: black;">
    <div class="container">
        <h1 class="text-center animation-slideDown"><i class="fa fa-arrow-right"></i> <strong>QuikWit Admin</strong></h1>
        <h2 class="h3 text-center animation-slideUp">Manage the site!</h2>
    </div>
</section>
<!-- END Intro -->

<section class="site-section site-content">
	<div class="container">
		<div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-lg-5 col-lg-offset-3 site-block">
				<strong>Users</strong>
				<br>
				<ul>
					<li><a href="{{ url('admin/importRequest') }}">Import Requests</a></li>
					<li><a href="{{ url('admin/importAccount') }}">Import Account</a></li>
					<li><a href="">Find and Play with Users</a></li>
					<li><a href="{{ url('admin/compareUser') }}">Compare Users</a></li>
					<li><a href="{{ url('admin/feedback') }}">List all Feedbacks</a></li>
					<li><a href="{{ url('admin/userStats') }}">User Stats</a></li>
				</ul>
				<strong>Announcements, Reports and Maintenance</strong>
				<br>
				<ul>
					{# <li><a href="">View and create reports</a></li> 
					<li><a href="">Download emails to gzipped CSV file</a></li> #}
					<li><a href="{{ url('admin/announcement') }}">Sending announcements</a></li>
					<li><a href="{{ url('admin/downtime') }}">Manage downtime notice</a></li>
					<li><a href="">View and manage darkmode status</a></li>
				</ul>
				<strong>Abuse and Errors</strong>
					<ul>
					<li><a href="{{ url('admin/abuseMonitor') }}">Abuse monitoring</a></li>
					<li><a href="{{ url('admin/viewAbuseReport') }}">Abuse report logs</a></li>
					<li><a href="">Logged exceptions</a></li>
				</ul>

				{# <strong>Spam moderation</strong>
				<ul>
					<li><a href="">Search statuses by keyword</a></li>
					<li><a href="">Keyword Monitor Spam Tool</a></li>
					<li><a href="">Lookup users</a></li>
					<li><a href="">Spam Reports</a></li>
				</ul> #}

				<strong>API</strong>
				<ul>
					{# <li><a href="">Manage device sources</a></li>
					<li><a href="">Manage whitelisted users</a></li> #}
					<li><a href="">Manage OAuth applications</a></li>
				</ul>

				<strong>Search</strong>
				<ul>
					<li><a href="{{ url('admin/findUser') }}">User search</a></li>
					<li><a href="{{ url('admin/findContract') }}">Contract search</a></li>
					<li><a href="">User Message search</a></li>
				</ul>
			</div>
		</div>
	</div>
</section>
{% endblock %}