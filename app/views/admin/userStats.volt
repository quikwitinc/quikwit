{% extends "templates/home.volt" %}

{% block head %}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top" style="background-color: black;">
    <div class="container">
        <h1 class="text-center animation-slideDown"><i class="fa fa-arrow-right"></i> <strong>QuikWit Admin</strong></h1>
        <h2 class="h3 text-center animation-slideUp"></h2>
    </div>
</section>
<!-- END Intro -->	

<section class="site-section site-content" style="padding-bottom: 200px;">
	<div class="container">
		<div class="row">
            <div class="col-sm-8 col-sm-offset-2 site-block">
		        <legend>Number of Users</legend>
				<div>
					{{ NumberOfUsers }}
				</div>
			</div>
		</div>
	</div>
</section>
{% endblock %}

{% block script %}
{% endblock %}