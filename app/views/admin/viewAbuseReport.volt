{% extends "templates/home.volt" %}

{% block head %}
<style>
table {
	border-collapse: separate;
}
</style>
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top" style="background-color: black;">
    <div class="container">
        <h1 class="text-center animation-slideDown"><i class="fa fa-arrow-right"></i> <strong>QuikWit Admin</strong></h1>
        <h2 class="h3 text-center animation-slideUp"></h2>
    </div>
</section>
<!-- END Intro -->	

<section class="site-section site-content" style="padding-bottom: 200px;">
	<div class="container">
		<div class="row">
            <div class="col-sm-8 col-sm-offset-2 site-block">
		        <legend>Abuses</legend>
				<div class="table-responsive">
					<table class"table table-vcenter">
						<thead>
							<tr>
								<th width="20%">Reason</th>
								<th width="20%">Detail</th>
								<th width="20%">User</th>
								<th width="20%">Target</th>
								<th width="20%">Created</th>
							</tr>
						</thead>
						<tbody>
							{% if abuses is defined %}
							    {% for abuse in abuses %}
								<tr>
									<td>{{ abuse['abs_reason'] }}</td>
									<td>{{ abuse['abs_text'] }}</td>
									<td><a href="{{ url('user/profile/' ~ abuse['abs_user']) }}">{{ abuse['abs_user'] }}</a></td>
									<td><a href="{{ url('user/profile/' ~ abuse['abs_target']) }}">{{ abuse['abs_target'] }}</a></td>
									<td>{{ abuse['abs_created'] }}</td>
								</tr>
								{% endfor %}
							{% endif %}
						</tbody>		
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
{% endblock %}

{% block script %}
{% endblock %}