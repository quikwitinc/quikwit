{% extends "templates/home.volt" %}

{% block title %}
Blog | QuikWit
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h1 class="animation-slideDown"><strong>QuikWit Burrow</strong></h1>
        <h2 class="h3 animation-slideUp">Check out the great articles we dug up just for you</h2>
    </div>
</section>
<!-- END Intro -->

<!-- Content -->
<section class="site-content site-section">
    <div class="container">
        <div class="row">
            <!-- Posts -->
            <div class="col-sm-8 col-md-9">
                {% if posts is defined %}
                    {% for post in posts %}
                    <!-- Blog Post -->
                    <div class="site-block">
                        <div class="row">
                            <div class="col-md-4">
                                <p>
                                    <img src="{{ url("uploads/post/" ~ post['cover_img']) }}" alt="image" class="img-responsive">
                                </p>
                            </div>
                            <div class="col-md-8">
                                <h3 class="site-heading"><strong>{{ post['title'] }}</strong></h3>
                                <?php echo Component\PrettyPrint::postExcerpt($post['content']) . ' ...'; ?>
                            </div>
                        </div>
                        <div class="clearfix">
                            <p class="pull-right">
                                <a href="{{ url('blog/show/' ~ post['slug']) }}" class="label label-primary">Read more..</a>
                            </p>
                            <ul class="list-inline pull-left">
                                <li><i class="fa fa-calendar"></i> {{ post['created'] }}</li>
                                <li><i class="fa fa-user"></i> by {{ post['user_firstname']|capitalize }}&nbsp;{{ post['user_lastname']|capitalize }}</li>
                                <li><i class="fa fa-tag"></i> <a href="{{ url('blog/category/' ~ post['category']|lower) }}">{{ post['category'] }}</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- END Blog Post -->
                    {% endfor %}
                {% endif %}

                <!-- Pagination -->
                <div class="text-center">
                    <ul class="pagination">
                        <li><a href="blog?page={{prevPage}}"><i class="fa fa-angle-left"></i></a></li>
                        <li><a href="blog?page={{nextPage}}"><i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </div>
                <!-- END Pagination -->
            </div>
            <!-- END Posts -->

            <!-- Sidebar -->
            <div class="col-sm-4 col-md-3">
                <aside class="sidebar site-block">

                    <!-- Categories -->
                    <div class="sidebar-block">
                        <h4 class="site-heading"><strong>Blog</strong> Categories</h4>
                        <ul class="fa-ul ul-breath">
                            <li><i class="fa fa-angle-right fa-li"></i> <a href="blog/category/insights">Insights</a></li>
                            <li><i class="fa fa-angle-right fa-li"></i> <a href="blog/category/trends">Trends</a></li>
                            <li><i class="fa fa-angle-right fa-li"></i> <a href="blog/category/diy">DIY</a></li>
                            <li><i class="fa fa-angle-right fa-li"></i> <a href="blog/category/news">News</a></li>
                        </ul>
                    </div>
                    <!-- END Categories -->

                    <!-- About -->
                    <div class="sidebar-block">
                        <h4 class="site-heading"><strong>About</strong> QuikWit</h4>
                        <p>QuikWit is an online platform that connects people looking for services to qualified local freelancers across Canada.</p>
                        <a href="{{ url('index') }}" class="btn btn-info" style="margin-bottom: 10px;">How QuikWit works?</a>
                        <a href="{{ url('become-a-pro') }}" class="btn btn-success">Become a Pro</a>
                    </div>
                    <!-- END About -->
                </aside>
            </div>
            <!-- END Sidebar -->
        </div>
    </div>
</section>
<!-- END Content -->
{% endblock %}