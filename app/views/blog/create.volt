{% extends "templates/home.volt" %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        {#<h1 class="text-center animation-slideDown"><strong>Log In</strong></h1>#}
        <h1 class="h3 text-center animation-slideUp" style="font-size: 200%"><strong>Create a Post</strong></h1>
    </div>
</section>
<!-- END Intro -->

<!-- Create Post -->
<section class="site-content site-section" style="padding-bottom: 200px;">
    <div class="container">
        <div class="row" style="margin-top: 50px">
            <div class="col-sm-10 col-sm-offset-1 site-block">
                <!-- Create Post Form -->
                {{ form("blog/save", "method" : "post", "enctype" : "multipart/form-data", "class" : "form-horizontal") }} 
                    <div class="form-group">
                       <label for="category" class="col-lg-3 control-label">Categroy</label>
                       <div class="col-lg-8">
                         <?php echo Phalcon\Tag::select(array('categoryId', 
                                              PostsCategories::find(array(
                                                 "order" => "name")), 
                                              "using" => array("id", "name"),
                                              "class" => "form-control",
                                              "type" => "text",
                                              "useEmpty" => "true",
                                              "emptyText" => "Select a category"
                                              )); ?>
                       </div>
                    </div>
                    <div class="form-group">
                       <label for="title" class="col-lg-3 control-label">Title</label>
                       <div class="col-lg-8">
                         {{ text_field('title', 'class' : 'form-control') }}
                       </div>
                    </div>
                    <div class="form-group">
                       <label for="reference" class="col-lg-3 control-label">Reference</label>
                       <div class="col-lg-8">
                         {{ text_field('slug', 'class' : 'form-control') }}
                       </div>
                    </div>
                    <div class="form-group">
                       <label for="image" class="col-lg-3 control-label">Cover Image</label>
                       <div class="col-lg-8">
                         <?php echo Phalcon\Tag::fileField("file"); ?>
                       </div>
                    </div>
                    <div class="form-group">
                       <label for="content" class="col-lg-3 control-label">Content</label>
                       <div class="col-lg-8">
                         {{ text_area('content', 'class' : 'form-control ckeditor') }}
                       </div>
                    </div>
                    <div class="form-group form-actions">
                      <div class="col-xs-12 text-center">
                          <button type="submit" class="btn btn-lg btn-primary"><strong>Save</strong></button>
                      </div>
                    </div><br>
                {{ end_form() }}
                <!-- END Create Post Form -->
            </div>
        </div>
    </div>
</section>
<!-- END Create Post -->
{% endblock %}

{% block script %}
<!-- ckeditor.js, load it only in the page you would like to use CKEditor (it's a heavy plugin to include it with the others!) -->
{{ javascript_include('third-party/proui/backend/js/helpers/ckeditor/ckeditor.js') }}
{% endblock %}
