{% extends "templates/home.volt" %}

{% block title %}
Blog Article | QuikWit
{% endblock %}

{% block content %}
{% for post in posts %}
<!-- Media Container -->
<div class="media-container">
    <!-- Intro -->
    <section class="site-section site-section-light site-section-top">
        <div class="container">
            <a href="javascript:void(0)">
                <img src="{{ url('uploads/avatar/' ~ post['user_avatar']) }}" alt="Author" class="site-top-avatar pull-right img-circle animation-fadeIn360">
            </a>
            <h1 class="animation-slideDown"><strong>{{ post['title'] }}</strong></h1>
            <h5 class="animation-slideUp hidden-xs">Posted on <?php $oldDate = strtotime($post['created']); 
                                                                    $newDate = date('Y-m-j', $oldDate);
                                                                    echo $newDate;
                                                                ?></h5>
        </div>
    </section>
    <!-- END Intro -->

    <!-- For best results use an image with a resolution of 2560x279 pixels -->
    <img src="{{ url("uploads/post/" ~ post['cover_img']) }}" alt="Image" class="media-image animation-pulseSlow">
</div>
<!-- END Media Container -->

<!-- Article -->
<section class="site-content site-section">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 site-block">
                <!-- Story -->
                <article>
                    {{ post['content'] }}
                </article>
                <!-- END Story -->
            </div>
        </div>
        <hr>
    </div>
</section>
<!-- END Article -->
{% endfor %}
{% endblock %}