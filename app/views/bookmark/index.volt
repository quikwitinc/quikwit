{% extends "templates/base.volt" %}

{% block head %}
{% endblock %}

{% block content %}
<section style="margin-left:30px; margin-top:60px;">
  <div class="container col-lg-5 col-md-5 col-sm-12 col-xs-12" id="tabs">
  	<ul class="nav nav-tabs">
  		<li class="active"><a href="#people" data-toggle="tab">People</a></li>
  		<li><a href="#posting" data-toggle="tab">Postings</a></li>
        <li><a href="#media" data-toggle="tab">Media</a></li> 
  	</ul>
  <!-- Tab panes -->
  	<div class="tab-content" style="margin-top:10px;">
      	<div class="tab-pane active" id="people">
      	</div>
      	<div class="tab-pane" id="posting">
  		  </div>  
        <div class="tab-pane" id="media">         
        </div>      
  	</div>
  </div>
</section>
{% endblock %}

{% block script %}
{% endblock %}