{% extends "templates/home.volt" %}

{% block title %}
Browse Posts | QuikWit
{% endblock %}

{% block head %}
{% endblock %}

{% block content %}

<section class="main-content">
  <div class="container">
    <div class="col-md-5">
       <h3 style="font-size:18px; color:#1b92ba"><strong>Check out our new gigs!<strong></h3>

       
       <div style="font-size:16px; font-weight: 500;">
          Once a gig is posted it shows up here. If you are tired of scrolling and clicking we can send you relevant gigs based on your skillsets and location once you sign up as a freelancer. <a href="/#signup">Sign Up</a> now for free to get access to cool features. 
          <br><br>

          <a class="btn btn-lg btn-success text-center" href="/post" target="_blank"><b style="font-size: 20px">Post a Gig</b></a>
       </div>
       
       <br>
    </div>
    <div class="col-md-7">
    	<?php if(count($requests) > 0): ?>
    	<?php foreach($requests as $idx => $request): ?>
        <!-- START widget-->
        <div class="widget widget-hover-effect1" data-id="<?php echo $request['req_id'];?>">
            <div class="widget-simple themed-background">
                {# <div class="col-sm-2 hidden-xs" style="padding: 0px;">
                  {% set profile = "user/profile/" ~ request['req_userid'] %}
                  {% if request['req_userid'] != 0 %}
                  <a href="{{ url(profile) }}" target="_blank">
                  {% endif %}
                  {% if request['user_avatar'] is defined %}
                  <img src="{{ url("uploads/avatar/" ~ request['user_avatar']) }}" alt="avatar" class="widget-image pull-left">
                  {% else %} 
                  <img src="/img/user/blank-avatar" style="width: 64px; max-width: 100%" alt="avatar" class="widget-image pull-left">
                  {% endif %}
                  {% if request['req_userid'] != 0 %}
                  </a>
                  {% endif %} 
                </div> #}
                <div class="col-sm-8 col-xs-12" style="padding: 0px; height: 60px;">
                  {# <h5 class="widget-content-light">
                      <strong><?php if(!empty($request['user_firstname']) && !empty($request['user_lastname'])) {
                                          echo ucwords($request['user_firstname']." ".$request['user_lastname']);
                                       }else{
                                          echo 'Posted by an unregistered user';
                                       }
                                  ?></strong>

                  </h5> #}
                  <h4 class="widget-content widget-content-light">
                      <strong>{{ request['req_title'] }}</strong>
                  </h4>  
                </div>  
                <div class="col-sm-4 col-xs-12" style="padding: 0px;">
                  <a class="btn btn-m pull-right" style="border: solid 2px white; background: #1bbae1; color: white;" href="{% if loginUser is defined and request['req_userid'] == userId %} {{ url('post/viewPosting/' ~ request['req_id']) }} {% elseif loginUser is defined %} {{ url('browse/viewDetail/' ~ request['req_id']) }} {% endif %}" {% if loginUser is empty %}data-toggle="modal" data-target="#register" {% else %} target="_blank" {% endif %}><h5 style="color: white;"><b>Contact Details</b></h5></a>
                </div>
            </div>
            <div class="widget-extra">
                <div class="row text-center" style="color: #1b92ba;">
                    <div class="col-xs-4">
                        <h5>
                            <strong>{{request['tag']}}</strong><br>
                        </h5>
                    </div>
                    <div class="col-xs-4">
                        <h5>
                            <strong>
                              <?php   if ($request['req_fixed'] == 0 && $request['req_price'] != 0) {
                                          echo '$' .  number_format($request['req_price'], 2) . ' <p class="text-muted">(per hour)</p>'; 
                                      }elseif ($request['req_fixed'] == 1 && $request['req_price'] != 0) {
                                          echo '$' .  number_format($request['req_price'], 2) . ' <p class="text-muted">(flate rate)</p>'; 
                                      }elseif ($request['req_fixed'] == 0 && $request['req_price'] == 0) {
                                          echo 'Budget not specified <p class="text-muted">(per hour)</p>';
                                      }elseif ($request['req_fixed'] == 1 && $request['req_price'] == 0) {
                                          echo 'Budget not specified <p class="text-muted">(flat rate)</p>';
                                      }
                              ?>
                            </strong><br>
                        </h5>
                    </div>
                    <div class="col-xs-4">
                        <h5>
                            <strong>{{ request['city'] }}</strong><br>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- END widget-->
        <?php endforeach; ?>
        <?php else: ?>
          <h4 style="margin-top: 50px">Sorry, currently there are no posts. Please check again later~</h4>
        <?php endif; ?>
        <?php if ($numPages > 1){ ?>
        <button class="btn btn-block btn-success" id="loadMore">Load More</button>
        <?php } ?>
    </div> 
  </div>
</section>

{% endblock %}

{% block modal %}
<!-- START modal-->
<div id="register" tabindex="-1" role="dialog"
    aria-labelledby="reportLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="reportLabel" class="modal-title">New to our site? Please register below to continue.</h4>
            </div>
            <div class="modal-body">
              <!-- Sign Up Form -->
              {{ form("register/doRegister", "id" : "form-validation", "method" : "post", "class" : "form-horizontal") }}
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-user"></i></span>
                              {{ text_field('firstname', "placeholder" : "First Name", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-user"></i></span>
                              {{ text_field('lastname', "id" : "rform-lastname", "placeholder" : "Last Name", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                              {{ email_field('email', "id" : "rform-email", "placeholder" : "Email", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-lock"></i></span>
                              {{ password_field('password', "id" : "rform-password", "placeholder" : "Password", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-lock"></i></span>
                              {{ password_field('rpassword', "id" : "rform-repassword", "placeholder" : "Retype Password", "class" : "form-control input-lg") }}
                              
                          </div>
                      </div>
                  </div>
                  </br>
                  <div class="form-group form-actions">
                      <div class="checkbox c-checkbox">
                           <label class="text-center">  
                              {{ check_field("confirmation", "id" : "rform-confirmation") }}
                              <span class="fa fa-check"></span>
                              <strong class="text-right"> Yes, I understand and agree to the QuikWit <a href="/terms" target="_blank" class="register-terms"> Terms of Use</a> and <a href="/terms/privacy" target="_blank" class="register-terms">Privacy Policy</a></strong>
                          </label>         
                      </div>
                      <br>
                      <div class="col-xs-12 text-center">
                          <button type="submit" class="btn btn-lg btn-primary" id="signUpButton"><strong>Sign Up</strong></button>
                      </div>
                  </div>
                  {# <input id="rform-csrf" type="hidden" name="<?php echo $this->security->getTokenKey() ?>" value="<?php echo $this->security->getTokenKey() ?>"/> 
                  <input id="rform-csrf" type="hidden" name="{{ tokenKey }}" value="{{ token }}"/> #}
                  {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }} 
              {{ end_form() }}
              <!-- END Sign Up Form -->
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
{% endblock %}

{% block footer %}{% endblock %}

{% block script %}
{{ javascript_include('third-party/async/async.js') }}
{{ javascript_include("third-party/proui/backend/js/pages/formsValidation.js") }}

<script>
$(document).ready(function(){
  Handlebars.registerHelper('equal', function(lvalue, rvalue, options) {
      if (arguments.length < 3)
          throw new Error("Handlebars Helper equal needs 2 parameters");
      if( lvalue!=rvalue ) {
          return options.inverse(this);
      } else {
          return options.fn(this);
      }
  });

  $(function(){ FormsValidation.init(); });
  $(document).on('click', '.searchMessage', function(ev){
    var id = $(this).attr('data-id');
    $('#searchRedirect').val('/user/profile/' + id);
    $('#searchUserId').val(id);
  });

  $(document).on('click', '.report-user', function(ev){
    var id = $(this).attr('data-id');
    $('#abuse_user_id').val(id);
  });

  $("#resend").click(function() {

      var email = "<?php echo $email; ?>";

      $.ajax({
          url: '/user/resend',
          type: 'POST',
          data:{
              email: email
          },
          success: function(res) {
              location.reload();
          },
          error: function(xhr, ajaxOptions, thrownError) {
              location.reload();
          }
      });
  });

  var requestObj = {
    page: {{page}},
    total: {{total}},
    numPages: {{numPages}}
  }

  $('#loadMore').click(function(){
    <?php $loginUserArray = (array) $loginUser;
          if($loginUserArray) {
            $loginUserJSON = json_encode($loginUserArray);
          }else{
            $loginUserJSON = false;
          } 
          
          ?>
    var loginUser_data = '<?php echo $loginUserJSON; ?>';
    if (!loginUser_data) {
      $('#register').modal('show');
    } else {
      processSearch(requestObj);
    }
  });

  function processSearch(requestObj){

    requestObj.page = requestObj.page + 1;
    
    async.waterfall([
      function(cb){
        $.ajax({
            url: '/browse/search?page=' + requestObj.page,
            success: function(res) {
                var result = JSON.parse(res);
                if (result.status){                 
                  $('#loadMore').show();
                    return cb(null, result.list);
                }else{
                    return cb(null, []);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                cb(null, []);
             }
        });
      },
      function(list, cb){
          if (list.length < 1){
              $('#loadMore').hide();
          }
          var self = this;
           $.ajax({
              url: '/js/post/search.js',
              cache: false,
              dataType: 'text',
              success: function(data) {
               var source  = data;
               template  = Handlebars.compile(source);
               var queue = [];
               list.map(function(oneRequest){
                      var price = Number(oneRequest.req_price);
                      $('#loadMore').before(template({
                         requestId: oneRequest.req_id,
                         userId: oneRequest.req_userid,
                         avatar: oneRequest.avatar,
                         price: price.toFixed(2),
                         skill: oneRequest.tag,
                         name: capitalizeFirstLetter(oneRequest.user_firstname + ' ' + oneRequest.user_lastname),
                         title: oneRequest.req_title,
                         city: oneRequest.city,
                         fixed: oneRequest.req_fixed
                      }));

                      return oneRequest;
              });

              //resetFilter(mapObject);
              if (requestObj.page == requestObj.numPages){
                 $('#loadMore').hide();
              }
               return cb(null, true);
              }, 
              error: function(xhr, ajaxOptions, thrownError) {
                return cb(null, true);
              }
          });

      }
    ], function(e, result){

    });
  }

  function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
  }
  

  $(document).on('click', '.report-user', function(ev){
    var id = $(this).attr('data-id');
    $('#abuse_user_id').val(id);
  });

});
</script>
{% endblock %}
