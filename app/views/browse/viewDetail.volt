{% extends "templates/home.volt" %}

{% block head %}
{% endblock %}

{% block title %}
Gig Detail | QuikWit
{% endblock %}

{% block content %}
<section class="main-content">
    <!-- Inbox Content -->
    <div class="row">
        <div class="container">
            <!-- View Request Block -->
            <div class="block full">
                <!-- View Request Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                    </div>
                    <h2><strong>{{ requestTitle|capitalize }}</strong></h2>
                    <h2 class="pull-right"><i class="fa fa-chevron-circle-left text-muted" style="color:white"></i> <a href="javascript:void(0);" style="color:white" onclick="window.history.go(-1)">Go Back</a></h2>
                </div>
                <!-- END View Request Title -->

                {% for request in requests %}
                <!-- Request Meta -->
                <div class="row">
                  <div class="col-sm-2 col-xs-6">
                      <a href="{{ url('user/profile/' ~ request['req_userid']) }}" class="text-center" style="font-size: 18px;">
                          {% if request['user_avatar'] is defined %}
                          <img src="{{ url('uploads/avatar/' ~ request['user_avatar']) }}" alt="{{ request['user_firstname']|capitalize ~ ' ' ~ request['user_lastname']|capitalize }}" class="img-thumbnail" style="width: 100px;" />
                          {% else %}
                          <img src="/img/user/blank-avatar" alt="{{ request['user_firstname']|capitalize ~ ' ' ~ request['user_lastname']|capitalize }}" class="img-thumbnail" style="width: 100px;" />
                          {% endif %} <br> 
                          {% if request['req_userid'] == 0 %}
                          Posted by an unregistered user
                          {% else %}
                          {{ request['user_firstname']|capitalize ~ ' ' ~ request['user_lastname']|capitalize }}
                          {% endif %}
                      </a>
                  </div> 
                  <div class="col-sm-2 col-xs-6">
                    {% if request['req_userid'] == 0 %}
                    <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#message" disabled>Message</button>
                    {% else %}
                    <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#message">Message</button>
                    {% endif %}
                    <?php $messageModel = new Messages(); 
                        $userSent = $messageModel->checkIfUserReplyRequest($request['req_id'], $loginUserId);
                          if($userSent == true){
                            echo '<br><br> <span style="color: #27AE44">You already responded to this gig</span>';
                          }
                    ?>
                    <br><br>
                    Phone #: {% if request['req_phone'] is defined %} {{ request['req_phone'] }} {% else %} Not Available {% endif %}
                  </div>
                  <div class="text-right col-sm-4 col-xs-12">
                  {% if request['req_status'] == 1 %}
                  Gig Status: Available
                  {% elseif request['req_status'] == 2 %}
                  Gig Status: Assigned
                  {% elseif request['req_status'] == 3 %}
                  Gig Status: Cancelled
                  {% endif %}
                  </div>


                  <div class="text-right col-sm-4 col-xs-12"><strong><?php if ($timezone) {
                                                       $userTimezone = new DateTimeZone($timezone); 
                                                       $newDate = new DateTime();
                                                       $newDate->setTimestamp($request['req_created']);
                                                       $newDate->setTimeZone($userTimezone);
                                                       echo $newDate->format('F jS, Y - g:i a'); 

                                                       }else{
                                                           echo date('F jS, Y - g:i a', $request['req_created']) . " UTC";
                                                       }

                                                       ?></strong></div>
                </div>

                <!-- END Request Meta -->

                <form>
                <br>
                <div class="form-group">
                  <hr>
                   <label style="font-size:16px; color: #1b92ba"><strong>Category:</strong></label>
                   <div>  
                     {{ requestTag|capitalize }} 
                   </div>
                    <hr>
                </div>
                   <div class="form-group">
                      <label style="font-size:16px; color: #1b92ba">Description of Task:</label>
                      <div>  
                        <?php echo htmlspecialchars_decode($request['req_text']); ?>   
                      </div>
                       <hr>        
                   </div>
                   <div class="form-group">
                      <label style="font-size:16px; color: #1b92ba">Client's budget:</label>
                      <div>
                        {% if request['req_fixed'] == 0 and request['req_price'] != 0 %}
                        ${{ request['req_price'] }} <p class="text-muted">(per hour)</p>
                        {% elseif request['req_fixed'] == 1 and request['req_price'] != 0 %}
                        ${{ request['req_price'] }} <p class="text-muted">(flat rate)</p> 
                        {% elseif  request['req_fixed'] == 0 and request['req_price'] == 0 %}
                        Budget not specified <p class="text-muted">(per hour)</p>
                        {% elseif  request['req_fixed'] == 1 and request['req_price'] == 0 %}
                        Budget not specified <p class="text-muted">(flat rate)</p>
                        {% endif %}
                      </div>
                       <hr>
                   </div>  
                   <div class="form-group">
                      <label style="font-size:16px; color: #1b92ba">This gig will be:</label>
                      <div style="font-size:14px">
                          {% if request['req_multiple'] == 0 %}
                          Nonrecurring
                          {% elseif request['req_multiple'] == 1 %}
                          Recurring
                          {% endif %}
                      </div>
                       <hr>
                   </div>                                                                                                                                  
                   <div class="form-group">
                      <label style="font-size:16px; color: #1b92ba">Estimated number of hours required to complete this gig:</label>
                      <div>
                        {% if request['req_hour'] != 0 %}
                        {{ request['req_hour'] }}&nbsp;hrs
                        {% else %}
                        Hours not specified
                        {% endif %}
                      </div>
                       <hr>
                   </div>
                   <div class="form-group">
                      <label style="font-size:16px; color: #1b92ba">Client's quality preference:</label>
                      <div style="font-size:14px">
                          {% if request['req_quality'] == 0 %}
                          As cheap as possible.
                          {% elseif request['req_quality'] == 1 %}
                          A mix of value and experience.
                          {% elseif request['req_quality'] == 2 %}
                          Price is not the biggest factor. I want the highest quality.
                          {% endif %}
                      </div>
                       <hr>
                   </div>  
                   {#<div class="form-group">
                      <label for="datetime-title">Date and Time the client want this task to be completed on:</label>
                      <div style="font-size:16px">
                          <?php 
                          if ($timezone) {
                              $userTimezone = new DateTimeZone($timezone); 
                              $newDate = new DateTime();
                              if ($request['req_datetime'] != Null) {
                                  $newDate->setTimestamp(strtotime($request['req_datetime']));
                                  $newDate->setTimeZone($userTimezone);
                                  echo $newDate->format('F jS, Y - g:i a'); 
                              }else{
                                  echo 'Null';
                              }
                          }else if ($request['req_datetime'] != Null) {
                              echo $request['req_datetime'] . " UTC";
                          }else{
                              echo 'Null';
                          }  

                          ?>
                      </div>
                       <hr>
                   </div>#}
                   <div class="form-group">
                      <label style="font-size:16px; color: #1b92ba">Location of this gig:</label>
                      <div>
                        {{ request['city'] }}
                      </div>
                       <hr>
                   </div>
                   <div class="form-group">
                      <label style="font-size:16px; color: #1b92ba">Client's travelling preference:</label>
                      <div>
                        {% if request['req_travel'] == 0 %}
                        Client requires freelancer to travel
                        {% elseif request['req_travel'] == 1 %}
                        Client does not require freelancer to travel
                        {% endif %}
                      </div>
                   </div>
                </form>
                {% endfor %}
            </div>
            <!-- END View Request Block -->
        </div>
        <!-- END Requests List -->
    </div>
    <!-- END Inbox Content -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
</section>
{% endblock %}

{% block modal %}
<!-- START modal-->
<div id="message" tabindex="-1" role="dialog" aria-labelledby="messageLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="messageLabel" class="modal-title">Send Message</h4>
            </div>
            <div class="modal-body">
                {% if email_verified == 1 %}
               <!-- <form class="form-horizontal"> -->
               {{ form("message/send", "id" : "message-form", "method" : "post", "class" : "form-horizontal") }}
                  <div class="form-group">
                    {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }}
                    {# hidden_field("message[redirect]", "value" : userLink) #}
                    {{ hidden_field("message[uid]", "value" : request['req_userid']) }}
                    {{ hidden_field("message[requestId]", "value" : request['req_id']) }}
                    <label for = "message-title" class = "col-lg-2 control-label">Title:</label>
                     <div class = "col-lg-10">
                       <!-- <input type = "text" class = "form-control" id = "message-title" placeholder = "[Type Title Here]"> -->
                       {{ text_field('message[title]', "id" : "message-title", "class" : "form-control", "value" : "Re: " ~ request['req_title'], "readOnly" : true) }}
                     </div>
                  </div>
                  <div class = "form-group">
                     <label for = "review-body" class = "col-lg-2 control-label">Message:</label>
                     <div class = "col-lg-10">
                     <!-- <textarea class="form-control" rows="8" placeholder="[Type Message Here]"></textarea> -->
                     {{ text_area('message[message]', "id" : "message-title", "placeholder" : "Type your message here", "class" : "form-control", "rows": 8) }}
                     </div>
                  </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-info">Send</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
            {{ end_form() }}
            {% else %}
            For security reasons, we require users to confirm their email before messaging other users. Do you want to confirm your email now?
            </div>
            <div class="modal-footer">
                <button id="resend" class="btn btn-info">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
            {% endif %}
        </div>
    </div>
</div>
<!-- END modal-->
{% endblock %}

{% block footer %}
{% endblock %}
