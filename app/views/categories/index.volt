{% extends "templates/home.volt" %}

{% block title %}
Categories | QuikWit
{% endblock %}

{% block head %}
<style>
.cat-list li
{
    list-style-type: none;
    padding: 10px 0;
    display: inline;
    width: 31.49171271%;
    float: left;
}
.cat-title
{
  margin-left: 40px;
  margin-bottom: 30px;
}
</style>
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h1 class="text-center animation-slideDown"> <strong>Categories!</strong></h1>
        <h2 class="h3 text-center animation-slideUp">What do you need help with today?</h2>
    </div>
</section>
<!-- END Intro -->

<section class="site-section site-content">
	<div class="container">
    <?php foreach($categoryName as $name) { ?>
		<div class="row" id="<?php $newName = str_replace(' ', '-', strtolower($name)); echo $newName; ?>">
      <h3 class="cat-title"><strong><?php echo $name; ?></strong></h3>
      <ul class="cat-list">
        <?php $tagModel = new TagsStandard();
              $tags = $tagModel->findTagByCategoryName($name);          
              $tagName = array_column($tags, 'name');

              for($i = 0; $i < sizeof($tagName); $i++) {
                $plusName[$i] = str_replace(' ', '+', $tagName[$i]);
                $seperator1 = '<li><a href="/search?q=' . $plusName[$i] . '">';
                $seperator2 = '</a></li>';
                echo $seperator1 . $tagName[$i] . $seperator2;
              }
              
        ?>
      </ul>
		</div>
    <hr>
    <?php } ?>
  </div>
</section>
{% endblock %}

{% block script %}
{% endblock %}