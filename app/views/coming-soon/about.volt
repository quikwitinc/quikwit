{% extends "templates/coming-soon.volt" %}

{% block title %}
<title>About Us</title>
{% endblock %}

{% block header %}
	style="background-image: url('/optimized/image/smboxing.jpg'); height: 90%; min-height: 600px;"
{% endblock %}

{% block top %}
<h1>About Us</h1> 
{% endblock %}

{% block signup %}
<div class="gotosection">
	<a href="/coming-soon" class='to-signup-anchor btn btn-info'>Get Notified!</a>
</div>
<br>
<!-- View more -->                 
<a href="/coming-soon/about#about"><i class="fa fa-angle-double-down " style="font-size: 40px;"></i></a>
<!-- View more end -->
{% endblock %}

{% block section %}

<!-- Section : About -->
<section id="about" class="section">
	<div class="container">

		<!-- Section Title -->
		<div>
			<h4>What is your favorite thing in the world?</h4>
			<p>Do you love writing, playing guitar, hiking? Have you ever wanted to learn climbing, scuba diving or get better at computer programming? QuikWit provides users with the opportunity to actively pursue interests and develop skills in areas of interest. Enthusiasts can  also share their passion through instruction.</p>
			<p>The co-founders, graduates of the University of Toronto, are passionate about squash badminton, cookery and art. When Asian cuisine and programming piqued their interest, their tight budgets and schedules made it difficult to browse several sites and visit locations to find suitable instructors. At times, it was more affordable and convenient to simply use instructional materials online but they found that nothing beats having a personal instructor or the proximity of a fellow enthusiast to lend support.</p>
			<br>
			<p>This frustration birthed the QuikWit site, an easy, quick way of finding flexible instructors nearby to help develop passions. The founders’ vision is for people to be able to learn anything imaginable through QuikWit and for QuikWit to become well-known for driving people to live their passions.</p>
		</div>
		<!-- Section Title End -->
		
	</div>
</section>
<!-- Section : About end -->

{% endblock %}

{% block script %}
{% endblock %}
