{% extends "templates/coming-soon.volt" %}

{% block title %}
<title>Blog</title>
{% endblock %}

{% block header %}
	style="background-image: url('/img/coming-soon/smskydiving.jpg');"
{% endblock %}

{% block section %}
<!-- Section : Blog -->
	<section id="blog" class="section">
		<div class="container">
			<!-- section title -->
			<div class="title">
				<h1>QuikWit Burrow</h1>
				<span class="border"></span>
				<br>
				<br>
				<h4>Check out the great articles we dug up just for you</h4>
			</div>
			<!-- section title end -->
		<!-- blog post holder -->
			<div class="blog-post-holder">
			{# 		
				<!-- blog post -->
					<div class="blog-post">
               				
						<img src="http://placehold.it/600x427" alt="image">
						
                        <h3 class="blog-title"><a href="#">Standard blog post with image</a></h3>
                        <p class="blog-meta">Posted by <a href="#">Admin</a> | 6 November | <a href="#">5 Comments</a></p>
                        <div class="border"></div>
                        <div class="blog-content">
                            <p>Nullam ornare, sem in malesuada sagittis, quam sapien ornare massa, id pulvinar quam augue vel. Praesent leo orci...</p>  
                        </div>
                        <a href="#" class="btn">Read More</a>
					</div>
				<!-- blog post end -->

				<!-- blog post  quote-->
					<div class="blog-quote">
		                <a href="#">
		                   	<div class="post-quote">
								<h2>I'm not a genius. I'm just a tremendous bundle of experience.</h2>
		                        <P>- R. Buckminster Fuller</P>
		                    </div>
		                </a>
					</div>
				<!-- blog post quote end -->


				<!-- blog post -->
					<div class="blog-post">
               				
		              <div id="blog-carousel" class="owl-carousel owl-theme">
		                <div class="item"><img src="http://placehold.it/800x450" alt=""></div>
		                <div class="item"><img src="http://placehold.it/800x450" alt=""></div>
		                <div class="item"><img src="http://placehold.it/800x450" alt=""></div>
		              </div>
						
                        <h3 class="blog-title"><a href="#">Standard blog post with gallery</a></h3>
                        <p class="blog-meta">Posted by <a href="#">Admin</a> | 6 November | <a href="#">5 Comments</a></p>
                        <div class="border"></div>
                        <div class="blog-content">
                            <p>Nullam ornare, sem in malesuada sagittis, quam sapien ornare massa, id pulvinar quam augue vel. Praesent leo orci...</p>  
                        </div>
                        <a href="#" class="btn">Read More</a>
					</div>
				<!-- blog post end -->

				<!-- blog video post -->
					<div class="blog-post">
                       				
                        <div class="video-holder">
                            <iframe src="http://player.vimeo.com/video/76175542?title=0" width="800" height="450" ></iframe>
                        </div>
						
                        <h3 class="blog-title"><a href="#">Standard blog post with video</a></h3>
                        <p class="blog-meta">Posted by <a href="#">Admin</a> | 6 November | <a href="#">5 Comments</a></p>
                        <div class="border"></div>
                        <div class="blog-content">
                            <p>Nullam ornare, sem in malesuada sagittis, quam sapien ornare massa, id pulvinar quam augue vel. Praesent leo orci...</p>  
                        </div>
                        <a href="#" class="btn">Read More</a>
					</div>
			<!-- blog video post end -->

					<!-- blog post -->
						<div class="blog-post">
	                        <h3 class="blog-title"><a href="#">Blog post with text only</a></h3>
	                        <p class="blog-meta">Posted by <a href="#">Admin</a> | 6 November | <a href="#">5 Comments</a></p>
	                        <div class="border"></div>
	                        <div class="blog-content">
	                            <p>Nullam ornare, sem in malesuada sagittis, quam sapien ornare massa, id pulvinar quam augue vel. Praesent leo orci...</p>  
	                        </div>
	                        <a href="#" class="btn">Read More</a>
						</div>
				<!-- blog post end -->  



				<!-- blog post  quote-->
					<div class="blog-quote">
		                <a href="#">
		                   	<div class="post-quote">
								<h2>Artists themselves are not confined, but their output is.</h2>
		                        <P>- Robert Smithson</P>
		                     </div>
		                </a>
					</div>
				<!-- blog post quote end -->    						
			</div>
		<!-- blog post holder end -->
		#}
		</div>
	</section>
<!-- Section : Blog end-->


{% endblock %}

{% block script %}
{% endblock %}