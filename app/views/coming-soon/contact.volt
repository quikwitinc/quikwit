{% extends "templates/coming-soon.volt" %}

{% block header %}
	style="background-image: url('/img/coming-soon/smcycling.jpg');"
{% endblock %}

{% block top %}
<h1>Contact Us</h1>
{% endblock %}

{% block signup %}
<div class="gotosection">
	<a href="/coming-soon" class='to-signup-anchor btn btn-info'>Get Early Access!</a>
</div>
{% endblock %}


{% block section %}

<!-- Section : Contact -->
<section id="contact" class="section">
	<div class="container">
		<!-- section title -->
		<div class="title">
			<h4>
				We are always happy to hear back from you!
			</h4>
		</div>
		<!-- section title end-->

		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				{# <h3>Contact Us</h3> #}
				<!-- contact from -->
				{{ form("coming-soon/email", "id" : "form", "method" : "post", "class" : "form") }}
					{{ text_field('name', 'required' : true, 'placeholder' : 'Name') }}<br/>
					{{ email_field('email', 'required' : true, 'placeholder' : 'Email', 'type' : 'email') }}
					{{ text_area('message', 'required' : true, 'placeholder' : 'Message', 'rows' : '8') }}
					<button type="submit" class="btn btn-info form-control"  id="submit">Send Message</button>
				{{ end_form() }}
				
							<span class="sending">
								sending...
							</span>
							<div class="mess center">
							</div>	
				<!-- contact from end-->
			</div>			
		</div>
	</div>
</section>
<!-- Section : Contact end -->
{% endblock %}

{% block script %}
{% endblock %}