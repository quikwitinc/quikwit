{% extends "templates/coming-soon.volt" %}

{% block title %}
<title>QuikWit Coming Soon!</title>
{% endblock %}

{% block header %}
	style="background-image: url('/img/coming-soon/mpushup.jpg'); height: 100%; min-height: 600px;"
{% endblock %}

{% block section %}
<!-- Section : Service -->
<section id="what" class="section">
	<div class="container">
		<!-- section title -->
		<div class="title" >
			<h2>What is QuikWit?</h2>
			<span class="border"></span>
			<br>
			<br>
			<h4>
				QuikWit is an online platform connecting teachers with students who need their services
			</h4>
		</div>
		<!-- section title end -->
		<div>
			
			<div class="service-item col-xs-12 col-sm-4 col-md-4">
				<span class="icon" style="color:#1b92ba">
                    <i class="fa fa-search"></i>
                </span>
                <h4><strong>Tailored Learning</strong></h4>
                <p>Scan our database of enthusiastic and flexible instructors in your area and book sessions that fit your availability and budget.</p>				
			</div>

			<div class="service-item col-xs-12 col-sm-4 col-md-4">
				<span class="icon" style="color:#1b92ba">
                    <i class="fa fa-dollar"></i>
                </span>
                <h4><strong>Make Quick Bucks</strong></h4>
                <p>You’re the sensei of your passion. Your expertise in a particular area is enviable. Who said you can’t make money doing what you love? Simply advertise your skill, availability and set your own lesson rates.</p>				
			</div>

			<div class="service-item col-xs-12 col-sm-4 col-md-4">
				<span class="icon" style="color:#1b92ba">
                    <i class="li_lock"></i>
                </span>
                <h4><strong>Safety First</strong></h4>
                <p>Information from instructors will be verified to facilitate a safe platform. Secure payment processing will also be available to users.</p>				
			</div>							

		</div>

		<div class="col-xs-12 text-center">	
			<h4>Find out why you should sign up now as a teacher</h4>
			<br>	
			<a href="/coming-soon/teacher#teacherFAQ" class='btn btn-info'>Learn More</a>
		</div>

	</div>
</section>
<!-- Section : Service end -->

{# <!--Parallax Section -->
  	<div id="wprocess" class="parallax" style="background-color: #1b92ba;" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
		<div class="container">
			<!--Title-->
			<div class="title">
			<h2>How QuikWit Works</h2>
			<span class="border"></span>
			</div>
			<!--Title-->  				
			<div class="row">
				<div class="col-xs-5 col-sm-4 col-md-3">
	            <!-- work_process -->
	            <div class="work_process">
	                <div class="work_process-box">
	                    <i class="fa fa-pencil-square-o"></i>
	                    <h4>Sign Up</h4>
	                </div>
	            </div>
	            <!-- work_process -->
	        </div>
	        
	        <div class="col-xs-5 col-sm-4 col-md-3">
	            <!-- work_process -->
	            <div class="work_process">
	                <div class="work_process-box">
	                    <i class="icon-brush"></i>
	                    <h4>Create Profile</h4>
	                </div>
	            </div>
	            <!-- work_process -->
	        </div>
	        
	        <div class="col-xs-5 col-sm-4 col-md-3">
	            <!-- work_process -->
	            <div class="work_process">
	                <div class="work_process-box">
	                    <i class="fa fa-puzzle-piece"></i>
	                    <h4>Search for an Instructor</h4>
	                </div>
	            </div>
	            <!-- work_process -->
	        </div>
	        
	        <div class="col-xs-5 col-sm-4 col-md-3">
	            <!-- work_process -->
	            <div class="work_process">
	                <div class="work_process-box">
	                    <i class="fa fa-lock"></i>
	                    <h4>Book and Enjoy Your Lesson</h4>
	                </div>
	            </div>
	            <!-- work_process -->
	        </div>
	   
		</div>
		</div> 
 	</div>
<!-- Parallax Section End -->

<!--Parallax Section -->
<div id="status" class="parallax" style="background-image: url('/img/coming-soon/smbasketball.jpg')" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
	<div class="parallax-overlay"> 
		<div class="container" style="margin-top:-50px;">
			<!--Title-->
				<div class="title">
					<h2>Our goals</h2>
					<span class="border"></span>
				</div>
				<!--Title-->
			<!-- Status -->	
				<div class="row count">

					<div class="col-xs-6 col-sm-6 col-md-3 single">
						<div class="total-numbers" data-perc="1300">
							<i class="cicon icon-child"></i>
							<br>
							<br>
							<h4 style="color:white">Link Enthusiasts</h4>
						</div>
					</div>

					<div class="col-xs-6 col-sm-6 col-md-3 single">
						<div class="total-numbers" data-perc="1300">
							<i class="cicon icon-bicycle"></i>
							<br>
							<br>
							<h4 style="color:white">Encourage Activity</h4>
						</div>
					</div>

					<div class="col-xs-6 col-sm-6 col-md-3 single">
						<div class="total-numbers" data-perc="1300">
							<i class="cicon icon-jabber"></i>
							<br>
							<br>
							<h4 style="color:white">Inspire Creativity</h4>
						</div>
					</div>	

					<div class="col-xs-6 col-sm-6 col-md-3 single">
						<div class="total-numbers" data-perc="1300">
							<i class="cicon icon-dollar"></i>
							<br>
							<br>
							<h4 style="color:white">Create Jobs</h4>
						</div>
					</div>	

				</div>
			<!-- Status end-->
		</div> 
	</div>
</div>
<!-- Parallax Section End --> #}

{# <!-- Section : Newsletter -->
<section id="signup" class="section">
	<div class="container">
		<!-- section title -->
		<div class="title">
			<h2>Early Access</h2>
			<span class="border"></span>
			<br>
			<br>
			<h4>
				Become Teacher or Student Now.
			</h4>
		</div>
		<!-- section title end-->

		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<!-- contact from -->
				<div id="messages">{{ flash.output() }}</div>

				{{ form("coming-soon/register", "id" : "fmnewsletter", "method" : "post", "class" : "form") }}
					{{ text_field('name', 'required' : true, 'placeholder' : 'Name') }}<br/>
					{{ email_field('email', 'required' : true, 'placeholder' : 'Email') }}
					<button type="submit" class="btn btn-info form-control"  id="submit">Sign me up!</button>
				{{ end_form() }}

			</div>

		</div>
	</div>
</section>
<!-- Section : Newsletter end --> #}

{% endblock %}

{% block script %}
{{ javascript_include("js/coming-soon/index.js") }}

{% endblock %}
