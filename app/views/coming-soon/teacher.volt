{% extends "templates/coming-soon.volt" %}

{% block title %}
<title>For Teachers</title>
{% endblock %}

{% block header %}
	style="background-image: url('/img/coming-soon/mviolin.jpg'); height: 90%; min-height: 600px;"
{% endblock %}

{% block top %}
<h1>For Teachers</h1> 
{% endblock %}

{% block signup %}
<div class="gotosection">
	<a href="/coming-soon" class='to-signup-anchor btn btn-info'>Get Notified!</a>
</div>
<br>
<!-- View more -->                 
<a href="/coming-soon/teacher#teacherFAQ"><i class="fa fa-angle-double-down " style="font-size: 40px;"></i></a>
<!-- View more end -->
{% endblock %}

{% block section %}
<section id="teacherFAQ" class="section">
	<div class="container">
		<!-- FAQ Content -->
		<div class="row">
		    <div class="col-md-6 col-lg-3 col-lg-offset-1">
		        <div class="block-section">
		            <h3><strong>Sign up now!</strong></h3>
		            <p class="clearfix"><i class="fa fa-clock-o fa-5x pull-left" style="color: #1b92ba;"></i>Sign up today to become one of our invited teachers for our <strong><span style="color: #1b92ba;">Beta launch</span></strong>.</p>
					<a href="/coming-soon" class='to-signup-anchor btn btn-lg btn-info' >Get Notified!</a>
		        </div>
		    </div>
		    <div class="col-md-6 col-lg-6 col-lg-offset-1">
		        <!-- Intro Content -->
		        <h3><strong>Welcome</strong></h3>
		        <div id="faq1" class="panel-group">
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq1" href="#faq1_q1">What  is QuikWit?</a></h4>
		                </div>
		                <div id="faq1_q1" class="panel-collapse collapse">
		                    <div class="panel-body">
		                        <p>QuikWit is an online platform connecting teachers with students who need their services.</p>
		                    </div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq1" href="#faq1_q2">Why should I sign up now?</a></h4>
		                </div>
		                <div id="faq1_q2" class="panel-collapse collapse in">
		                    <div class="panel-body">
		                    	<p>Signing up now allows us to directly notify you when we launch. Teachers who sign up for early access will be able to use this service <strong>free of charge for the first 3 months</strong>. For more information, see "What does it cost to use QuikWit?" below.

		                    	<p>Note that signing up now does not mean that you have created an account. For more information, see “Will I have to sign any contract?” below.</p>
		                    </div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq1" href="#faq1_q3">How can you get me more students?</a></h4>
		                </div>
		                <div id="faq1_q3" class="panel-collapse collapse">
		                    <div class="panel-body">
		                    	<p>It is our intention to build a specialized platform to allow students across Canada to find teachers for any skill. Our marketing efforts will go into attracting students to the site.</p>
		                    	<p>Teachers with pictures, an introduction video (optional) and completed profiles are more likely to be selected by students. Reviews and ratings left by students will also help teachers create stronger profiles for attracting students.</p>
		                    </div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq1" href="#faq1_q4">What will be required of me as a teacher on QuikWit?</a></h4>
		                </div>
		                <div id="faq1_q4" class="panel-collapse collapse">
		                    <div class="panel-body">
		                    	<p>A photo, phone number, postal code and an email will be required to verify your QuikWit account. We will never disclose your personal information to a third party without your consent.</p>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <!-- END Intro Content -->

		        <!-- Subscriptions Content -->
		        <h3 class="sub-header"><strong>Registrations</strong></h3>
		        <div id="faq3" class="panel-group">
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq3" href="#faq3_q1">Will I have to sign any contract?</a></h4>
		                </div>
		                <div id="faq3_q1" class="panel-collapse collapse">
		                    <div class="panel-body">
		                    	<p>Teachers are not required to sign any contracts and are not employees of QuikWit. However, after we launch, all users will have to agree to our Terms of Service when creating their account.</p>
		                    </div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq3" href="#faq3_q3">Am I charged for closing my account?</a></h4>
		                </div>
		                <div id="faq3_q3" class="panel-collapse collapse">
		                    <div class="panel-body">
		                    	<p>Teachers will be able to delete their accounts at any time and are not charged any fees if they decide to do so.</p>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <!-- END Subscriptions Content -->

		        <!-- Features Content -->
		        <h3 class="sub-header"><strong>Payments</strong></h3>
		        <div id="faq2" class="panel-group">
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq2" href="#faq2_q1">What does it cost to use QuikWit?</a></h4>
		                </div>
		                <div id="faq2_q1" class="panel-collapse collapse">
		                    <div class="panel-body">
		                    	<p>Teachers who sign up for early access will be able to use this service free of charge for the first 3 months. Thereafter a 10% service fee will be charged to teachers for every completed lesson through our platform. This will allow us to build better features and maintain the quality of the service we will provide. There will be no additional or hidden fees.</p>
							</div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq2" href="#faq2_q2">How will students pay me?</a></h4>
		                </div>
		                <div id="faq2_q2" class="panel-collapse collapse">
		                    <div class="panel-body">
		                    	<p>To verify the availability of funds, students will be charged via their selected payment option prior to the lesson date. This amount is then deposited into the teacher’s bank account after completion of the lesson.</p>
							</div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq2" href="#faq2_q3">How is QuikWit going to charge me?</a></h4>
		                </div>
		                <div id="faq2_q3" class="panel-collapse collapse">
		                    <div class="panel-body">
		                    	<p>The 10% service fee will be subtracted before payment is deposited into the teacher’s bank account. For example, if a student pays $100 to a teacher, $90 will be deposited into the teacher’s bank account after the completion of a lesson.</p>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <!-- END Features Content -->

		    </div>
		</div>
		<!-- END FAQ Content -->
    </div>
</section>

{% endblock %}

{% block script %}
{% endblock %}
