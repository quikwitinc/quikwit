{% extends "templates/coming-soon.volt" %}

{% block title %}
<title>Timeline</title>
{% endblock %}

{% block header %}
	style="background-image: url('/img/coming-soon/mchef.jpg'); height: 90%; min-height: 600px;"
{% endblock %}

{% block top %}
<h1>Timeline</h1>
{% endblock %}

{% block signup %}
<div class="gotosection">
	<a href="/coming-soon" class='to-signup-anchor btn btn-info'>Get Notified!</a>
</div>
<br>
<!-- View more -->                 
<a href="/coming-soon/timeline#milestone"><i class="fa fa-angle-double-down " style="font-size: 40px;"></i></a>
<!-- View more end -->
{% endblock %}

{% block section %}

<!-- Section : Resume -->
<section id="milestone" class="section">
	<div class="container">
		<!-- section title -->
		<div class="title">
			<h4>
				"To connect people with others around them who share a passion."
			</h4>
		</div>
		<!-- section title end-->
		<!-- Experience Timeline -->
		<div class="row">
			<div class="col-md-12">
				<div class="timeline">
					<!-- Item Head -->
						<div class="timeline-item">
							<div class="timeline-head">
								<span class="fa fa-lightbulb-o"><i></i></span>
							</div>
							<div class="timeline-arrow"><i></i></div>
							<div class="timeline-head-content">
									<h4>Idea</h4>
							</div>
						</div>
					<!-- /Item Head end -->
					<!-- first item of timeline -->
					<div class="timeline-item">
						<div class="timeline-item-date">2014 - now</div><!-- date -->
							<!-- timeline item trigger -->
							<div class="timeline-item-trigger">
								<span class="fa fa-minus" data-toggle="collapse" data-target="#job1"><i></i></span>
							</div>
							<!-- /end of timeline item trigger -->
							<div class="timeline-arrow"><i></i></div>
							<!-- timeline main content -->
							<div class="timeline-item-content">
								<h4 class="timeline-item-title" data-toggle="collapse" data-target="#job1">Building Site</h4>
								
								<div class="collapse in" id="job1">
									<p><small class="muted">from December 2014 to present day</small></p>
									<!-- <h4 class="media-heading primary-color">Lorem ipsum dolor sit amet</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tincidunt diam ac lectus tristique scelerisque. Quisque vitae libero sit amet turpis faucibus auctor eget vitae tortor. Aenean metus erat, ultricies non mattis quis, molestie ac massa. Sed sollicitudin erat ac dui viverra a posuere eros adipiscing. Phasellus nisi lectus, imperdiet sed hendrerit ac, dictum quis sem. Phasellus vel nisi non massa elementum porta. Aliquam erat volutpat.</p>
									<p><a href="#" title="" class="noprint">&rarr; View website</a></p> -->
								</div>
						    </div>
						 	<!-- /end of timeline main content -->  
					</div>
					<!-- /end of first item of timeline -->
				</div>								
		<!-- /Experience Timeline End -->
			</div>
		</div>
	</div>								
</section>
<!-- Section : Resume end -->

{% endblock %}