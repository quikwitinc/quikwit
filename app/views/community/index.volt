{% extends "templates/base.volt" %}

{% block head %}
{% endblock %}

{% block content %}
<section class="main-content">
	{# <!-- START Messages-->
	<div class="panel panel-default col-md-6" style="margin-left:-50px;">
	    <div class="panel-body">
	        <div class="input-group m-b">
	            <div class="input-group-btn">
	                <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">@
	                    <span class="caret"></span>
	                </button>
	                <ul class="dropdown-menu">
						<li><a href="#">@user</a>
						</li>
						<li><a href="#">@community</a>
						</li>
						<li><a href="#">Something else here</a>
						</li>
						<li class="divider"></li>
						<li><a href="#">Separated link</a>
						</li>
	                </ul>
	            </div>
	            <input type="text" class="form-control" style="width:30%">
	            <input type="text" class="form-control" style="width:70%">
	        </div>                                                     
	        <ul class="nav nav-pill nav-stacked col-md-3" role="tablist">
	  			<li class="active"><a href="#inbox" data-toggle="tab" style="color:blue;">Inbox</a></li>
	  			<li><a href="#preferCommunity" data-toggle="tab" style="color:green;">Community</a></li>
	  			<li><a href="#otherCommunity" data-toggle="tab" style="color:orange;">Community</a></li>
			</ul> 
			<!-- Tab panes -->
	        <div class="tab-content col-md-9">
	        	<div class="tab-pane active" id="inbox" style="height:auto; overflow-y:scroll; overflow-x:hidden; padding:none;">
	        		<!-- START widget-->
	        		<div class="panel widget">
	        		   <div class="panel-body">
	        		      <div class="">
	        		         <div class="text-center col-xs-3">
	        		            <img src="/img/user/06.jpg" style="" alt="Image" class="img-thumbnail img-circle">
	        		            <strong>Shirley Osborn</strong>
	        		         </div>
	        		         <div class="col-xs-7">                     
	        		            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> 
	        		         </div>
	        		         <div class="col-xs-2">
	        		         	<p>14:35</p>
	        		         </div>
	        		      </div>
	        		   </div>
	        		</div>
	        		<!-- END widget--> 
	        		<!-- START widget-->
	        		<div class="panel widget">
	        		   <div class="panel-body">
	        		      <div class="">
	        		         <div class="text-center col-xs-3">
	        		            <img src="/img/user/02.jpg" style="" alt="Image" class="img-thumbnail img-circle">
	        		            <strong>Shirley Osborn</strong>
	        		         </div>
	        		         <div class="col-xs-7">                     
	        		            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> 
	        		         </div>
	        		         <div class="col-xs-2">
	        		         	<p>Nov 6</p>
	        		         </div>
	        		      </div>
	        		   </div>
	        		</div>
	        		<!-- END widget--> 
	        		<!-- START widget-->
	        		<div class="panel widget">
	        		   <div class="panel-body">
	        		      <div class="">
	        		         <div class="text-center col-xs-3">
	        		            <img src="/img/user/11.jpg" style="" alt="Image" class="img-thumbnail img-circle">
	        		            <strong>Shirley Osborn</strong>
	        		         </div>
	        		         <div class="col-xs-7">                     
	        		            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> 
	        		         </div>
	        		         <div class="col-xs-2">
	        		         	<p>Jun 4</p>
	        		         </div>
	        		      </div>
	        		   </div>
	        		</div>
	        		<!-- END widget--> 
	            </div>
	            <div class="tab-pane active" id="preferCommunity">
	            </div>
	            <div class="tab-pane active" id="otherCommunity">
	            </div>
	        </div>
	    </div>
	</div>
	<!-- END Messages--> #}
	<div class="panel panel-default col-md-10" id="viewMedia" style="margin-left:50px;">
		{# <button type="button" class="btn btn-default" aria-label="Left Align" data-target="#create-content" data-toggle="modal">Create new</button> #}
		<div class="panel-body">
			<div id="preview">
				<button class="btn btn-sm btn-success pull-right" id="hidePreview" style="">Return to List</button>
				<button class="btn btn-sm btn-info pull-right" style="margin-right:10px;">Bookmark</button>
				<br>
				  <h3> Sunrise on the field</h3>
				  <p> 1100 likes</p>	
			      <img src="/img/bg1.jpg" alt="Image" class="img-responsive">
			      <br>
			      <div>
			      	<p class="col-md-9">I took it during fall in Wisconsin.</p>
			      	<p class="col-md-3">-Jun.29.2014</p>
			      </div>
			   <div class="col-md-12" style="height:200px; overflow-y:scroll; overflow-x:hidden;">
			      <div class="col-md-2" style="margin-left:-30px;">
			         <img src="/img/user/06.jpg" style="width: 50px; height: 50px; max-width: 100%" alt="Image" class="img-thumbnail img-circle">
			      </div>
			      <div class="col-md-10">
			         <a href="#">John Murray</a>
			         <p>It is cool.</p>
			      </div>
			      <br>
			   </div>
			   <div class="col-md-8">
			   		<input type="text" class="form-control" placeholder="Leave a comment..." style="margin-bottom:20px;">
			   	</div>
			   <button class="btn col-md-2">Post!</button>
			   <button class="col-md-1 fa fa-2x fa-thumbs-o-up"></button>
			   <hr>
			   <br>	
			</div>
			<hr>
			<br>
			<div id="mediaList" style="overflow-y:scroll; overflow-x:hidden;">
				<!-- START widget-->
				<div class="col-md-2"> 
				<div class="panel widget pic">
			   		<img src="/img/bg1.jpg" alt="Image" class="img-responsive">
				</div>
				<!-- END widget-->
			</div>
		</div>  
	</div>     
</section>
{% endblock %}

{% block modal %}
{# <!-- START modal-->
   <div id="create-content" tabindex="-1" role="dialog" aria-labelledby="reportLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">X</button>
               <h4 id="reportLabel" class="modal-title">Create a new post</h4>
            </div>
           	<form class = "form-horizontal" id="new-post-form" name="newform" action="/community/createpost" method="post" enctype="multipart/form-data"> 
	            <div class="modal-body">
	                                       
	                  <div class = "form-group">
	                  	<label>Title:</label><input type="text" name="title"><br/>
	                     <label>Upload Media:</label><input type='file' name='file'/><br/>
	                     <label>Body</label><textarea name="content" form="new-post-form"></textarea> 
	                  </div>
	
	            </div>
	            <div class="modal-footer">
	               <input type="submit" value="Submit">
	               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
	            </div>
            </form>
         </div>
      </div>
   </div>
<!-- END modal--> #}
{% endblock %}

{% block footer %}
{% endblock %}


{% block script %}
<script>
$(document).ready(function() {
    $("#preview").hide();
});

$(".pic").click(function() {
	$("#preview").show();
});

$("#hidePreview").click(function() {
	$("#preview").hide();
})
</script> 
{% endblock %}