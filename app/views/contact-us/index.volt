{% extends "templates/home.volt" %}

{% block title %}
Contact Us | QuikWit
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h1 class="text-center animation-slideDown"><i class="fa fa-envelope"></i> <strong>Contact Us</strong></h1>
        <h2 class="h3 text-center animation-slideUp">We are always happy to hear back from you!</h2>
    </div>
</section>
<!-- END Intro -->

<!-- Contact -->
<section class="site-content site-section" style="padding-bottom: 200px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 site-block">
                <div class="site-block">
                    <h3 class="h2 site-heading"><strong>QuikWit</strong> Inc</h3>
                    <address>
                        <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)">general@quikwit.co</a>
                    </address>
                </div>
            </div>
            <div class="col-sm-6 col-md-8 site-block">
                <h3 class="h2 site-heading"><strong>Contact</strong> Form</h3>
                {{ form("contact-us/email", "id" : "", "method" : "post", "class" : "form-horizontal") }}
                    <div class="form-group">
                        {{ text_field('name', "placeholder" : "Name", "class" : "form-control input-lg") }}
                    </div>
                    <div class="form-group">
                        {{ email_field('email', "placeholder" : "Email", "class" : "form-control input-lg") }}
                    </div>
                    <div class="form-group">
                        {{ text_area('message', "placeholder" : "Message", "class" : "form-control input-lg", "rows" : "10") }}
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-primary">Send Message</button>
                    </div>
                {{ end_form() }}
            </div>
        </div>
    </div>
</section>
<!-- END Contact -->
{% endblock %}

{% block script %}
{% endblock %}
