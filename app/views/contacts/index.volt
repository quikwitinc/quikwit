{% extends "templates/home.volt" %}

{% block title %}
User Contacts | QuikWit
{% endblock %}

{% block head %}
<!-- Star Rating -->
<link rel="stylesheet" href="/third-party/star-rating/jquery.rating.css">
{{ stylesheet_link("third-party/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") }}
{% endblock %}

{% block content %}
<section class="main-content" style="min-height: 300px;">
	<div class="form-horizontal">
	   <div class="form-group">
	      <label class="col-lg-2 control-label">Search Contact</label>
	      <div class="col-lg-6">
           <div class="input-group">
               <input id="inputFilter" type="text" placeholder="Search your contact here" class="form-control">
               <div class="input-group-btn">
                   <button type="submit" class="btn btn-primary" style="padding-top: 3px; height: 34px;"><i class="fa fa-search"></i></button>
               </div>
           </div>
	      </div>
          <div class="btn-group col-lg-1">
            <button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle btn-sm" aria-expanded="false">Sort By
               <span class="caret"></span>
            </button>
            <ul id="contact-sort-by" role="menu" class="dropdown-menu">
            <li><a href="javascript:void(0);" data-target="recent" id="contact-recent" style="font-weight: 600;">Recently Added</a>
            </li>
            <li><a href="javascript:void(0);" data-target="location" id="contact-location" style="font-weight: 600;">Location</a>
            </li>
            <li><a href="javascript:void(0);" data-target="name" id="contact-name" style="font-weight: 600;">Name</a>
            </li>
            </ul>
           </div>
	   </div>
	</div>

	<div id="contactList" class="row">

 	</div>
</section>
{% endblock %}

{% block footer %}
{% endblock %}

{% block modal %}
<!-- START modal-->
<div id="message" tabindex="-1" role="dialog" aria-labelledby="messageLabel" aria-hidden="true" class="modal ">
  <div class="modal-dialog">
     <div class="modal-content">
        <div class="modal-header">
           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
           <h4 id="messageLabel" class="modal-title">Send Message</h4>
        </div>
        <div class="modal-body">
        {% if email_verified == 1 %}
           <!-- <form class="form-horizontal"> -->
           {{ form("message/send", "id" : "message-form", "method" : "post", "class" : "form-horizontal") }}
              <div class="form-group">
                {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }}
                {{ hidden_field("message[redirect]", "value" : "http://localhost/user/profile/", "id":"contactRedirect", "data-value":"http://localhost/user/profile/") }}
                {{ hidden_field("message[uid]", "value" : 0, "id":"contactUserId") }}
                 <label for = "message-title" class = "col-lg-2 control-label">Title:</label>
                 <div class = "col-lg-10">
                   <!-- <input type = "text" class = "form-control" id = "message-title" placeholder = "[Type Title Here]"> -->
                  {{ text_field('message[title]', "id" : "message-title", "placeholder" : "Give your message a title", "class": "form-control") }}
                 </div>
              </div>
              <div class = "form-group">
                 <label for = "review-body" class = "col-lg-2 control-label">Message:</label>
                 <div class = "col-lg-10">
                 <!-- <textarea class="form-control" rows="8" placeholder="[Type Message Here]"></textarea> -->
                 {{ text_area('message[message]', "id" : "message-title", "placeholder" : "Type your message here", "class": "form-control", "rows": 8) }}
                 </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-info">Send</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
              </div>
          </div>
          {{ end_form() }}
          {% else %}
          For security reasons, we require users to confirm their email before messaging other users. Do you want to confirm your email now?
          </div>
          <div class="modal-footer">
              <button id="resend" class="btn btn-info">Yes</button>
              <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
          </div>
          {% endif %}
      </div>
  </div>
</div>
<!-- END modal-->
<!-- START modal-->
   <div id="booking" tabindex="-1" role="dialog" aria-labelledby="bookingLabel" aria-hidden="true" class="modal ">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="bookingLabel" class="modal-title">Contract Booking</h4>
            </div>
            <div class="modal-body">
            {% if email_verified == 1 %}
              <!-- START panel-->
              {{ form("contract/create", "id" : "contract-form", "method" : "post", "class" : "form-horizontal") }}
                <div class="form-group container">
                  <p>Please message the teacher first regarding availability before booking a contract.</p>
                </div>
                 <div class="form-group">
                 {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }}
                  {{ hidden_field("contract[uid]", "id" : 'bookId') }}
                    <label for="datetime-title" class="col-lg-3 control-label">Start Date & Time</label>
                    <div class="col-lg-8">
                     <div class='input-group date datetimepicker mb-lg'>
                          <input type='text' name="contract[datetime]" id="datetimepickers" class="form-control" readOnly />
                          <span class="input-group-addon">
                              <span class="fa fa-calendar"></span>
                          </span>
                      </div>
                    </div>
                 </div>
                 <div class="form-group">
                    <label for="price-title" class="col-lg-3 control-label">Price of the contract ($ CDN)</label>
                    <div class="col-lg-8">
                      <div class="input-group m-b">
                         <span class="input-group-addon">$</span>
                         {{ text_field('price', "id" : "contract-price", "class" : "form-control") }}
                      </div>
                    </div>
                 </div>
                 <div class = "form-group">
                    <label for = "sub-skill" class = "col-lg-3 control-label">Skill:</label>
                     <div class = "col-lg-8">
                        <span id="bookingSkillList"></span>
                     </div>
                  </div>
                  <div class = "form-group">
                    <label for = "req-id" class = "col-lg-3 control-label">Post:</label>
                     <div class = "col-lg-8">
                        <?php 
                        $requestModel = new Requests();
                        $requestsObj = $requestModel->getUserRequests($currentUser->usr_id);
                        $requests = (array_column($requestsObj, 'req_title', 'req_id'));
                        //error_log("<pre>request".print_r($requests,true)."</pre>"); 
                        echo Phalcon\Tag::selectStatic(array('contract[reqId]', 
                                                          $requests, 
                                                          "using" => array("req_id", "req_title"),
                                                          "class" => "select-chosen form-control",
                                                          "type" => "text",
                                                          "id" => "myRequests",
                                                          "data-role" => "tagsinput",
                                                          "useEmpty" => true,
                                                          "emptyText" => "Ignore if this is not related to any post"
                                                          )); ?>
                     </div>
            <!-- END panel-->
            </div>
            <div class="modal-footer">
               <input type="submit" value="Book" class="btn btn-info">
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
            {{ end_form() }}
            {% else %}
            For security reasons, we require users to confirm their email before booking a contract. Do you want to confirm your email now?
            </div>
            <div class="modal-footer">
                <button id="resend" data-dismiss="modal" class="btn btn-info">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
            {% endif %}
         </div>
      </div>
   </div>
<!-- END modal-->
<!-- START modal-->
<div id="remove" tabindex="-1" role="dialog"
    aria-labelledby="removeLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="removeLabel" class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">Are you sure you want to remove the
                contact selected?</div>
            <div class="modal-footer">
                <button class="btn btn-info" id="remove-person">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
<!-- START modal-->
<div id="report" tabindex="-1" role="dialog"
    aria-labelledby="reportLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="reportLabel" class="modal-title">Report User</h4>
            </div>
            <div class="modal-body">
                {{ form("abuse/create", "class" : "form-horizontal", "method" :
                "post") }}
                <div class="form-group">
                    <label for="report-title" class="col-lg-2 control-label">Description:</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="report-title"  name="abuse[reason]"
                            placeholder="Please tell us the nature of the issue">
                    </div>
                </div>
                <div class="form-group">
                    <label for="report-body" class="col-lg-2 control-label">More Information:</label>
                    <div class="col-lg-10">
                        <textarea class="form-control" id="report-body" rows="8" name="abuse[extra]"
                            placeholder="Please provide us with more information so we can help"></textarea>
                    </div>
                </div>
                 {{ hidden_field("abuse[uid]", "value" : 0, "id": "abuse_user_id") }}
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-info">Submit</button>
                {{ end_form() }}
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
{% endblock %}

{% block script %}
<script>
var contactOperations = {
    contacts: {{contactJson}},
    user: {{userLogin}},
    addShow: function(){
        var self = this;
        this.contacts = this.contacts.map(function(oneContact){
            oneContact.show = true;
            oneContact.distance = self.distanceGPS(Number(oneContact.lat), Number(oneContact.lng),
                Number(self.user.lat), Number(self.user.lng));
            oneContact.tag = [oneContact.firstname, oneContact.lastname];
            if (oneContact.tagSkill != null){
                oneContact.tagSkill = oneContact.tagSkill.toLowerCase();
                oneContact.tagSkill = oneContact.tagSkill.replace(/,/g, ', ');
                oneContact.tag = oneContact.tag.concat(oneContact.tagSkill.split(','));
            }else{
                oneContact.tagSkill = '';
            }
            if (oneContact.about == null){
                oneContact.about = '';
            }
            if (oneContact.avatar == null){
                oneContact.avatar = '';
            }
            return oneContact;
        });
    },
    sortByRecent: function(a, b){
      if (a.connectTime < b.connectTime){
         return -1;
      }
      if (a.connectTime > b.connectTime){
        return 1;
      }
      if (a.distance < b.distance){
         return -1;
      }
      if (a.distance > b.distance){
        return 1;
      }
      if (a.firstname < b.firstname){
         return -1;
      }
      if (a.firstname > b.firstname){
        return 1;
      }
      return 0;
    },
    sortByLocation: function(a, b){
      if (a.distance < b.distance){
         return -1;
      }
      if (a.distance > b.distance){
        return 1;
      }
      if (a.connectTime < b.connectTime){
         return -1;
      }
      if (a.connectTime > b.connectTime){
        return 1;
      }
      if (a.firstname < b.firstname){
         return -1;
      }
      if (a.firstname > b.firstname){
        return 1;
      }
      return 0;
    },
    sortByName: function(a, b){
      if (a.firstname < b.firstname){
         return -1;
      }
      if (a.firstname > b.firstname){
        return 1;
      }
      if (a.connectTime < b.connectTime){
         return -1;
      }
      if (a.connectTime > b.connectTime){
        return 1;
      }
      if (a.distance < b.distance){
         return -1;
      }
      if (a.distance > b.distance){
        return 1;
      }
      return 0;
    },
    distanceGPS: function(lat1, lon1, lat2, lon2) {
      //calculate distance using metres
     var radlat1 = Math.PI * lat1 / 180;
     var radlat2 = Math.PI * lat2 / 180;
     var radlon1 = Math.PI * lon1 / 180;
     var radlon2 = Math.PI * lon2 / 180;
     var theta = lon1 - lon2;
     var radtheta = Math.PI * theta / 180;
     var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
     dist = Math.acos(dist);
     dist = dist * 180 / Math.PI;
     dist = dist * 60 * 1.1515;
     dist = dist * 1.609344;
     return dist.toFixed(3);
    },
    getContacts: function(){
        return this.contacts;
    },
    render: function(sort){
        if (sort == 'recent'){
            this.contacts = this.contacts.sort(this.sortByRecent);
        }
        if (sort == 'location'){
            this.contacts = this.contacts.sort(this.sortByLocation);
        }
        if (sort == 'name'){
            this.contacts = this.contacts.sort(this.sortByName);
        }
        $('#contactList').html('');
        var self = this;
         $.ajax({
            url: 'js/contact/contactCard',
            cache: false,
            dataType: 'text',
            success: function(data) {
             var source   = data;
             template  = Handlebars.compile(source);
             self.contacts.map(function(oneContact){
                if (oneContact.show == true){
                    var usr_rate = Number(oneContact.rate);
                    $('#contactList').append(template({
                       id: oneContact.id,
                       avatar: oneContact.avatar,
                       firstname: oneContact.firstname,
                       lastname: oneContact.lastname,
                       rate: usr_rate.toFixed(2),
                       tagSkill: oneContact.tagSkill,
                       about: oneContact.about,
                       num_contract: oneContact.num_contract,
                       num_review: oneContact.num_review,
                       num_1: (oneContact.overall > 0)?'checked':false,
                       num_2: (oneContact.overall >= 2)?'checked':false,
                       num_3: (oneContact.overall >= 3)?'checked':false,
                       num_4: (oneContact.overall >= 4)?'checked':false,
                       num_5: (oneContact.overall >= 5)?'checked':false,
                       teach: (oneContact.teach == 1)?true:false,
                       notTeach: (oneContact.teach == 1)?false:true
                    }));
                }
             return oneContact;
            });
            }, error: function(xhr, ajaxOptions, thrownError) {
            }
        });
    },
    filter: function(text){
        var re = new RegExp(text,'gi');
        this.contacts = this.contacts.map(function(oneContact){
            oneContact.show = true;
            var success = false;
            for (var i = 0; i < oneContact.tag.length; i++){
                if (re.test(oneContact.tag[i])){
                    success = true;
                    oneContact.show = true;
                }
                if (!re.test(oneContact.tag[i]) && !success){
                    oneContact.show = false;
                }
            }
            return oneContact;
        });
    }
};

function contactMessage(id){
  $('#contactUserId').val(id);
  $('#contactRedirect').val($('#contactRedirect').attr('data-value') + id);
  $('#contactUserId').val(id);
}

$(document).ready(function() {
  contactOperations.addShow();
  contactOperations.render('recent');
  $("#contact-recent").click(function(){
      contactOperations.render('recent');
  });
  $("#contact-name").click(function(){
      contactOperations.render('name');
  });
  $("#contact-location").click(function(){
      contactOperations.render('location');
  });
	$("#inputFilter").change(function() {
	    var filter = $(this).val();
        contactOperations.filter(filter);
        contactOperations.render('no change');
	});
  $('#remove-person').click(function(ev){
        var id = $('#remove-person').attr('data-id');
        $.ajax('/contacts/deleteContact/' + id , {
            data: {
            },
            success: function(data){
                console.log(data);
                if (!data.error){
                window.location.reload();
                }
            }
            });
  });
  $("#resend").click(function() {

      var email = "<?php echo $email; ?>";

      $.ajax({
          url: '/user/resend',
          type: 'POST',
          data:{
              email: email
          },
          success: function(res) {
              location.reload();
          },
          error: function(xhr, ajaxOptions, thrownError) {
              location.reload();
          }
      });
  });
});

function showBookingModal(usr_id, usr_price) {
  $('#contract-price').val(usr_price);
  $('#bookId').val(usr_id);
  var postData = 'usr_id='+usr_id;  
  $.ajax({
      url: '/contacts/userskilltags',
      type: "POST",
      data: postData,
      dataType: 'text',
      success: function(data) {           
        $('#bookingSkillList').html(data);
        $("#usersTags").chosen();
        $('.chosen-container.chosen-container-single').css('width', '360px');
        $('#booking').modal('show');
      }, 
        error: function(xhr, ajaxOptions, thrownError) {
      }
  }); 
}
</script>
{% endblock %}
