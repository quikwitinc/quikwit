{% extends "templates/home.volt" %}

{% block title %}
Manage Contracts | QuikWit
{% endblock %}

{% block head %}
<!-- Star Rating -->
<link rel="stylesheet" href="/third-party/star-rating/jquery.rating.css">
{% endblock %}

{% block content %}
<section class="main-content">
    <!-- Responsive Full Block -->
    <div class="block">
        <!-- Responsive Full Title -->
        <div class="block-title text-center">
            <h1 style="font-size:24px"><strong>Manage Your Contracts</strong></h1>
            <div>
                <br>
           &nbsp; <input id="contract_pending" class="contract_status" rel="status"
                type="checkbox" value="Pending" 
                <?php if ($status == 'pending'){
                    echo 'checked';
                    }?>
                 >Pending &nbsp;
                <input id="contract_active"
                class="contract_status" rel="status" type="checkbox" value="Active"
                <?php if ($status == 'active'){
                    echo 'checked';
                    }?>
                >Active
            &nbsp; <input id="contract_completed" class="contract_status" rel="status" type="checkbox"
                value="Completed"
                <?php if ($status == 'completed'){
                    echo 'checked';
                    }?>
                 >Completed 
                 &nbsp; <input id="contract_review" class="contract_status" rel="status"
                type="checkbox" value="Under Review"
                <?php if ($status == 'review'){
                    echo 'checked';
                    }?>
                 >Under Review &nbsp;
                <input id="contract_rejected"
                class="contract_status" rel="status" type="checkbox" value="Rejected"
                <?php if ($status == 'rejected'){
                    echo 'checked';
                    }?>
                >Rejected
            &nbsp; <input id="contract_cancel" class="contract_status" rel="status" type="checkbox"
                value="Canceled"
                <?php if ($status == 'cancel'){
                    echo 'checked';
                    }?>
                 >Cancelled
            </div>
            <br>
        </div>
        <!-- END Responsive Full Title -->

        <!-- Responsive Full Content -->
        <div class="row">
            <div class="col-sm-5 col-xs-7">
                <div class="input-group">
                    <!--<input type="search" class="form-control" placeholder="Search" aria-control="example-datatable">
                    <span class="input-group-addon">
                        <i class="fa fa-search"></i>
                    </span> -->
                </div>
            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-5">
                <div class="input-group" >
                    <select class="input-md form-control" id="pendingbulk" >
                        <option value="0" disabled selected>Perform an Action</option>
                        <option value="1">Accept Booking</option>
                        <option value="2">Decline Booking</option>
                        <option value="6">Cancel Booking</option>
                        <option value="3">Mark as Completed</option>
                        <option value="4">Report</option> 
                        {# <option value="5">Deny Completion</option> #}
                        
                    </select> <span class="input-group-btn">
                        <button class="btn btn-default" id="pendingapply"
                            data-toggle="modal" style="padding-top: 3px; height: 34px;">Apply</button>
                    </span>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-vcenter table-striped" style="overflow-x: hidden;">
                <thead>
                    <tr class="first">
                        <th style="width: 7%" class="text-center">Skill <br>(Contract ID)</th>
                        <th style="width: 7%" class="text-center"><i class="gi gi-user"></i></th>
                        <th style="width: 10%" class="hidden-xs hidden-sm hidden-md text-center">Date Created</th>
                        <th style="width: 4%" class="hidden-xs text-center">Status</th>
                        <th style="width: 6%" class="hidden-xs hidden-sm hidden-md text-center">Price</th>
                        <th style="width: 12%" class="hidden-xs text-center">Scheduled Time</th>
                        <th style="width: 10%" class="hidden-xs hidden-sm hidden-md text-center">Date Completed</th>
                        <th style="width: 2%" class="check-all text-center">
                            <div data-toggle="tooltip" data-title="Check All"
                                class="checkbox c-checkbox">
                                <label> <input type="checkbox"> <span class="fa fa-check"></span>
                                </label>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($contracts) > 0): ?>
                    <?php foreach($contracts->items as $idx => $contract): ?>
                    <tr data-toggle="collapse" data-target="#accordion_<?php echo $contract->cnt_id; ?>" class="clickable">
                        <td class="text-center" style="padding-top: 40px;">
                            <?php   $tagModel = new TagsStandard();
                                    $skillObj = $tagModel->findTagBySingleId($contract->cnt_tagid);
                                    $skill = $skillObj->name;
                                    echo $skill; ?>
                            
                            <div style="padding-top: 10px;">
                            <?php   $hashids = new Hashids\Hashids(null, 8, null);
                                    $newId =  $hashids->encode($contract->cnt_id);
                            echo "<div style='color: #1b92ba'>(" .$newId. ")</div>"; 
                            //echo $contract->cnt_id;
                            ?>
                        </div>
                            {# <a href="javascript:void(0)"><?php echo "(" . $contract->cnt_id . ")"; ?> </a> #}
                        </td>
                        <td class="text-center">
                        <a href="<?php
                        if ($userId == $contract->cnt_user){
                            echo '/user/profile/'. $contract->cnt_teachid;
                        }else{
                            echo '/user/profile/'. $contract->cnt_user;
                        }

                         ?>">
                         <img src="<?php
                         $di = \Phalcon\DI::getDefault();
                         $url = $di->get('url');
                         if ($userId == $contract->cnt_user){
                             if ($contract->getTeacher()->usr_avatar){
                                 echo $url->get("uploads/avatar/" . $contract->getTeacher()->usr_avatar);
                             }else{
                                 echo $url->get("img/user/blank-avatar");
                             }                       
                         }else{
                             if ($contract->getUsers()->usr_avatar){
                                 echo $url->get("uploads/avatar/" . $contract->getUsers()->usr_avatar); 
                             }else{
                                 echo $url->get("img/user/blank-avatar");
                             }                     
                         }
                         ?>" alt="avatar" class="img-thumbnail" style="width: 65px;">
                         <br>
                        <?php
                        if ($userId == $contract->cnt_user){
                            echo $contract->getTeacher()->usr_firstname . ' ' . $contract->getTeacher()->usr_lastname;
                        }else{
                            echo $contract->getUsers()->usr_firstname . ' ' . $contract->getUsers()->usr_lastname;
                        }


                        ?>
                        </a>
                        </td>
                        <td class="hidden-xs hidden-sm hidden-md text-center" >{# <?php echo date('F jS, Y', strtotime($contract->cnt_created)); ?> #}
                            <?php 
                            if ($timezone) {
                                $userTimezone = new DateTimeZone($timezone); 
                                $newDate = new DateTime();
                                if ($contract->cnt_created != Null) {
                                    $newDate->setTimestamp(strtotime($contract->cnt_created));
                                    $newDate->setTimeZone($userTimezone);
                                    echo $newDate->format('F jS, Y'); 
                                }else{
                                    echo 'Null';
                                }
                            }elseif ($contract->cnt_created != Null) {
                                echo $contract->cnt_created . " UTC";
                            }else{
                                echo 'Null';
                            }  

                            ?>
                        </td>
                        <td class="hidden-xs text-center">{# <a href="javascript:void(0)" class="label label-info"> #}
                        <?php
                        $reviewStr = '';
                        if ($contract->cnt_state == 0){
                            $history = (!empty($contract->getContractHistory()))?$contract->getContractHistory():array();
                            $newState = false;
                            for ($i = 0; $i < count($history); $i++){
                                if ($history[$i]->sub_state == ContractHistory::USERACCEPT){
                                    echo $states[$contract->cnt_state]; 
                                    $newState = true;
                                }
                                if ($history[$i]->sub_state == ContractHistory::TEACHERACCEPT){
                                    echo $states[$contract->cnt_state];
                                    $newState = true;
                                }
                            }
                            if (!$newState){
                                echo($states[$contract->cnt_state]);
                            }
                        }else if ($contract->cnt_state == 1){
                            $history = (!empty($contract->getContractHistory()))?$contract->getContractHistory():array();
                            $newState = false;
                            $stateStr = '';
                            for ($i = 0; $i < count($history); $i++){
                                /* if ($history[$i]->sub_state == ContractHistory::USERDENY){
                                    $stateStr = $states[$contract->cnt_state] . ' (client deny completion)';
                                    $newState = true;
                                }
                                if ($history[$i]->sub_state == ContractHistory::TEACHERDENY){
                                    $stateStr = $states[$contract->cnt_state] . ' (teacher deny completion)';
                                    $newState = true;
                                } */
                                if ($history[$i]->sub_state == ContractHistory::USERCOMPLETED){
                                    $stateStr = $states[$contract->cnt_state] ;
                                    $newState = true;
                                }
                                if ($history[$i]->sub_state == ContractHistory::TEACHERCOMPLETED){
                                    $stateStr = $states[$contract->cnt_state] ;
                                    $newState = true;
                                }
                            }
                            if (!$newState){
                                $stateStr = $states[$contract->cnt_state];
                            }
                            echo $stateStr;
                        }else if ($contract->cnt_state == 2){
                            $newState = false;
                            if ($userId == $contract->cnt_user){
                                $reviewModel = new Reviews();
                                $reviewed = $reviewModel->beenReviewed($contract->cnt_user, $contract->cnt_teachid);
                                //error_log("<pre>reviewed".print_r($reviewed,true)."</pre>"); 
                                if (empty($reviewed)){
                                    $reviewDone = count($contract->getReviews());
                                    if ($reviewDone < 1){
                                        $reviewStr =  '<button class="label label-success review-teacher" data-toggle="tooltip" title="Leave a Review" data-id="' .
                                        $contract->cnt_id . '" data-teacher="' . $contract->cnt_teachid . '" style="border-color:transparent; padding: 3px 0px 5px 5px"><img src="img/landing/clipboard105.png" height="18" width="18"/></button>';
                                        echo($states[$contract->cnt_state]);
                                    }
                                }else{
                                    echo($states[$contract->cnt_state]);
                                }
                            }

                        }else{
                            echo($states[$contract->cnt_state]);
                        }
                         ?>
                        {# </a> #}
                        <?php echo $reviewStr; ?>
                        </td>
                        <td class="hidden-xs hidden-sm hidden-md text-center"><?php echo '$' . number_format($contract->cnt_unitprice,2); ?></td>
                        <td class="hidden-xs" style="padding-top: 20px;">
                            <?php 
                                if ($contract->cnt_state == 1) {
                                echo "<div class='col-md-2 col-sm-12 pull-right' style='padding-left: 0px;'>
                                        <button class='btn btn-sm btn-default' id='datetime_save_" . $contract->cnt_id . "_lg' style='padding: 3px; border-style: solid; border-width: 1px' disabled>update</button>
                                    </div>" . "<div class='input-group date mb-lg col-md-10' id='datetime_container_" . $contract->cnt_id . "_lg'>" . 
                                    "<input type='text' style='font-size: 14px' name='datetime_" . $contract->cnt_id . "'
                                                                    id='datetime_" . $contract->cnt_id . "'
                                                                    data-contract-id='" . $contract->cnt_id . "'
                                                                    data-contract-date='" . date('F jS, Y - g:i a', strtotime($contract->cnt_next)) . "'
                                                                    class='form-control' readOnly/>" . "<span class='input-group-addon'>
                                    <span class='fa-clock-o fa'></span>
                                </span>
                            </div>";
                            }else{
                                echo "<input type='text' style='font-size: 14px' name='datetime_" . $contract->cnt_id . "'
                                id='datetime_" . $contract->cnt_id . "'
                                data-contract-id='" . $contract->cnt_id . "'
                                value='" . date('M jS, Y - g:i a', strtotime($contract->cnt_next)) . "'
                                class='form-control' readOnly/>";
                            }

                            ?>
                            {# <div>
                                <button class="btn btn-sm btn-default pull-right" id="datetime_save_<?php echo $contract->cnt_id ?>" style="padding: 3px; border-style: solid; border-width: 1px" disabled>update</button>
                            </div>
                            <div class='input-group date mb-lg' id="datetime_container_<?php echo $contract->cnt_id ?>">
                                <input type='text' style="font-size: 14px" name="datetime_<?php echo $contract->cnt_id ?>"
                                id="datetime_<?php echo $contract->cnt_id ?>"
                                data-contract-id="<?php echo($contract->cnt_id); ?>"
                                data-contract-date="<?php echo date('F jS, Y - g:i a', strtotime($contract->cnt_next)); ?>"
                                class="form-control" readOnly/>
                                <span class="input-group-addon">
                                    <span class="fa-clock-o fa"></span>
                                </span>
                            </div> #} 
                        </td>
                        <td class="hidden-xs hidden-sm hidden-md text-center" ><?php   if ($timezone) {
                                                                    $userTimezone = new DateTimeZone($timezone); 
                                                                    $newDate = new DateTime();
                                                                    if ($contract->cnt_completed != Null) {
                                                                        $newDate->setTimestamp(strtotime($contract->cnt_completed));
                                                                        $newDate->setTimeZone($userTimezone);
                                                                        echo $newDate->format('F jS, Y'); 
                                                                    }else{
                                                                        echo 'Not Completed';
                                                                    }
                                                                }elseif ($contract->cnt_completed != Null) {
                                                                    echo $contract->cnt_completed . " UTC";
                                                                }else{
                                                                    echo 'Not Completed';
                                                                }
                                                                
                                                                ?></td>
                        <td>
                            <div class="checkbox c-checkbox">
                                <label> <input type="checkbox" class="contract_checked"
                                    name="chkbx_<?php echo $contract->cnt_id; ?>"
                                    id="chkbx_<?php echo $contract->cnt_id; ?>"
                                    data-state="<?php echo $contract->cnt_state;?>"
                                    data-id="<?php echo $contract->cnt_id;?>"
                                    > <span
                                    class="fa fa-check"></span>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr class="hidden-lg">
                        <td colspan="12">
                            <div id="accordion_<?php echo $contract->cnt_id; ?>" class="collapse" style="font-size: 14px; line-height:2;">

                                <div class="col-xs-12 col-sm-3" style="padding-top:25px; padding-bottom: 25px; margin-left: 15px;">
                                    <p style="font-size: 16px; color: #888;">Date Created</p>
                                    <?php 

                                    if ($timezone) {
                                        $userTimezone = new DateTimeZone($timezone); 
                                        $newDate = new DateTime();
                                        if ($contract->cnt_created != Null) {
                                            $newDate->setTimestamp(strtotime($contract->cnt_created));
                                            $newDate->setTimeZone($userTimezone);
                                            echo $newDate->format('F jS, Y'); 
                                        }else{
                                            echo 'Null';
                                        }
                                    }elseif ($contract->cnt_created != Null) {
                                        echo $contract->cnt_created . " UTC";
                                    }else{
                                        echo 'Null';
                                    }  

                                    ?>
                                </div>

                                <div class="col-xs-12 col-sm-2" style="padding-top:25px; padding-bottom: 25px; margin-left: 15px;">
                                    <p style="font-size: 16px; color: #888;">Price</p> <?php echo '$' . number_format($contract->cnt_unitprice,2); ?>
                                </div>

                                <div class="col-xs-12 hidden-md hidden-sm" style="padding-top:25px; padding-bottom: 25px; margin-left: 15px;">
                                    <p style="font-size: 16px; color: #888;">Status</p>
                                    <?php
                                    $reviewStr = '';
                                    if ($contract->cnt_state == 0){
                                        $history = (!empty($contract->getContractHistory()))?$contract->getContractHistory():array();
                                        $newState = false;
                                        for ($i = 0; $i < count($history); $i++){
                                            if ($history[$i]->sub_state == ContractHistory::USERACCEPT){
                                                echo $states[$contract->cnt_state]; 
                                                $newState = true;
                                            }
                                            if ($history[$i]->sub_state == ContractHistory::TEACHERACCEPT){
                                                echo $states[$contract->cnt_state];
                                                $newState = true;
                                            }
                                        }
                                        if (!$newState){
                                            echo($states[$contract->cnt_state]);
                                        }
                                    }else if ($contract->cnt_state == 1){
                                        $history = (!empty($contract->getContractHistory()))?$contract->getContractHistory():array();
                                        $newState = false;
                                        $stateStr = '';
                                        for ($i = 0; $i < count($history); $i++){
                                            /* if ($history[$i]->sub_state == ContractHistory::USERDENY){
                                                $stateStr = $states[$contract->cnt_state] . ' (client deny completion)';
                                                $newState = true;
                                            }
                                            if ($history[$i]->sub_state == ContractHistory::TEACHERDENY){
                                                $stateStr = $states[$contract->cnt_state] . ' (teacher deny completion)';
                                                $newState = true;
                                            } */
                                            if ($history[$i]->sub_state == ContractHistory::USERCOMPLETED){
                                                $stateStr = $states[$contract->cnt_state] ;
                                                $newState = true;
                                            }
                                            if ($history[$i]->sub_state == ContractHistory::TEACHERCOMPLETED){
                                                $stateStr = $states[$contract->cnt_state] ;
                                                $newState = true;
                                            }
                                        }
                                        if (!$newState){
                                            $stateStr = $states[$contract->cnt_state];
                                        }
                                        echo $stateStr;
                                    }else if ($contract->cnt_state == 2){
                                        $newState = false;
                                        if ($userId == $contract->cnt_user){
                                            $reviewModel = new Reviews();
                                            $reviewed = $reviewModel->beenReviewed($contract->cnt_user, $contract->cnt_teachid);
                                            //error_log("<pre>reviewed".print_r($reviewed,true)."</pre>"); 
                                            if (empty($reviewed)){
                                                $reviewDone = count($contract->getReviews());
                                                if ($reviewDone < 1){
                                                    $reviewStr =  '<button class="label label-success review-teacher" data-toggle="tooltip" title="Leave a Review" data-id="' .
                                                    $contract->cnt_id . '" data-teacher="' . $contract->cnt_teachid . '" style="border-color:transparent; padding: 3px 0px 5px 5px"><img src="img/landing/clipboard105.png" height="18" width="18"/></button>';
                                                    echo($states[$contract->cnt_state]);
                                                }
                                            }else{
                                                echo($states[$contract->cnt_state]);
                                            }         
                                        }

                                    }else{
                                        echo($states[$contract->cnt_state]);
                                    }
                                     ?>
                                    {# </a> #}
                                    <?php echo $reviewStr; ?> 
                                </div>

                                <div class="col-xs-12 hidden-sm hidden-md" style="padding-top:25px; padding-bottom: 25px; margin-left: 15px; padding-right: 35px;">
                                    <p style="font-size: 16px; color: #888;">Scheduled Time</p> 
                                    <?php 
                                        if ($contract->cnt_state == 1) {
                                        echo "     
                                            " . "<div class='input-group date mb-lg' id='datetime_container_" . $contract->cnt_id . "_xs'>" . 
                                            "<input type='text' style='font-size: 14px' name='datetime_" . $contract->cnt_id . "'
                                                                            id='datetime_" . $contract->cnt_id . "'
                                                                            data-contract-id='" . $contract->cnt_id . "'
                                                                            data-contract-date='" . date('F jS, Y - g:i a', strtotime($contract->cnt_next)) . "'
                                                                            class='form-control' readOnly/>" . "<span class='input-group-addon'>
                                            <span class='fa-clock-o fa'></span>
                                        </span>
                                        
                                    </div>
                                    <button class='btn btn-sm btn-default' id='datetime_save_" . $contract->cnt_id . "_xs' style='padding: 3px; border-style: solid; border-width: 1px' disabled>update</button>";
                                    }else{
                                        echo "<input type='text' style='font-size: 14px' name='datetime_" . $contract->cnt_id . "'
                                        id='datetime_" . $contract->cnt_id . "'
                                        data-contract-id='" . $contract->cnt_id . "'
                                        value='" . date('M jS, Y - g:i a', strtotime($contract->cnt_next)) . "'
                                        class='form-control' readOnly/>";
                                    }

                                    ?> 

                                </div>
                                <div class="col-xs-12 col-sm-3" style="padding-top:25px; padding-bottom: 25px; margin-left: 15px;"> 
                                    <p style="font-size: 16px; color: #888;">Date Completed</p> 
                                        <?php   
                                        if ($timezone) {
                                            $userTimezone = new DateTimeZone($timezone); 
                                            $newDate = new DateTime();
                                            if ($contract->cnt_completed != Null) {
                                                $newDate->setTimestamp(strtotime($contract->cnt_completed));
                                                $newDate->setTimeZone($userTimezone);
                                                echo $newDate->format('F jS, Y'); 
                                            }else{
                                                echo 'Not Completed';
                                            }
                                        }elseif ($contract->cnt_completed != Null) {
                                            echo $contract->cnt_completed . " UTC";
                                        }else{
                                            echo 'Not Completed';
                                        }
                                        
                                        ?>  
                                </div>
                                <div class="col-xs-12 col-sm-3" style="padding-top:25px; padding-bottom: 25px; margin-left: 15px;"> 
                                    <p style="font-size: 16px; color: #888;">Gig Related:</p> 
                                        <?php   if($contract->getRequests() && $userId == $contract->cnt_teachid){
                                                    echo '<a href="/gig/viewRequest/' . $contract->cnt_requestid . '">' . $contract->getRequests()->req_title . '</a>';
                                                }elseif ($contract->getRequests() && $userId == $contract->cnt_user){
                                                    echo '<a href="/post/viewPosting/' . $contract->cnt_requestid . '">' . $contract->getRequests()->req_title . '</a>';
                                                }else{
                                                    echo 'None';
                                                }
                                                
                                        ?>  
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
        <!-- END Responsive Full Content -->
        <div class="block-footer">
            <div class="row">
                <div class="col-lg-12 paging-bootstrap clear-fix">
                    <ul class="pagination pagination-sm">
                        <li
                        <?php if ($currentPage == 1){ ?>
                         class="disabled"
                         <?php } ?>
                         ><a href="/contract<?php
                        if ($status != 'all'){
                            echo '?status=' . $status;
                        }
                         ?>"> << </a>
                        </li>
                        <?php for ($i = $firstPage; $i <= $lastPage; $i++) {?>
                        <li
                        <?php if ($i == $currentPage){ ?>
                        class="active"
                        <?php } ?>
                        ><a href="/contract?page=<?php
                        echo $i;
                        if ($status != 'all'){
                            echo '&status=' . $status;
                        }
                        ?>"><?php echo $i;?>
                        <?php if ($i == $currentPage){ ?>
                        <span class="sr-only">(current)</span>
                        <?php } ?>
                        </a>
                        </li>
                        <?php } ?>
                        <li
                        <?php if ($currentPage == $lastPage){ ?>
                         class="disabled"
                         <?php } ?>
                        ><a href="/contract?page=<?php echo $contracts->last;
                        if ($status != 'all'){
                            echo '&status=' . $status;
                        }
                        ?>"> >> </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END Responsive Full Block -->
</section>
{% endblock %}

{% block modal %}
<!-- START modal-->
<div id="accept" tabindex="-1" role="dialog"
    aria-labelledby="acceptLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="acceptLabel" class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">Are you sure you want to accept the
                contract(s) selected?</div>
            <div class="modal-footer">
                <button class="btn btn-info confirm-accept">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
<!-- START modal-->
<div id="cancel" tabindex="-1" role="dialog"
    aria-labelledby="acceptLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="denyLabel" class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">Are you sure you want to cancel the
                contract(s) selected?</div>
            <div class="modal-footer">
                <button class="btn btn-info confirm-cancel">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
{# <!-- START modal-->
<div id="deny" tabindex="-1" role="dialog"
    aria-labelledby="acceptLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="denyLabel" class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">Are you sure you want to deny completion for the
                contract(s) selected?</div>
            <div class="modal-footer">
                <button class="btn btn-danger confirm-deny">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal--> #}
<!-- Start modal -->
<div id="decline" tabindex="-1" role="dialog"
    aria-labelledby="declineLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="declineLabel" class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">Are you sure you want to decline the
                contract(s) selected?</div>
            <div class="modal-footer">
                <button class="btn btn-info confirm-decline" >Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
<!-- START modal-->
<div id="complete" tabindex="-1" role="dialog"
    aria-labelledby="declineLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="completeLabel" class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">Are you sure you want to mark the
                contract(s) selected as completed?</div>
            <div class="modal-footer">
                <button class="btn btn-info confirm-complete">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
<!-- START modal-->
<div id="review-by-admin" tabindex="-1" role="dialog"
    aria-labelledby="declineLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="completeLabel" class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group container">
                        Are you sure you want to report the contract(s) selected to an admin?
                    </div>
                    <div class="form-group">
                        <label for="report-title" class="col-lg-2 control-label">Description:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="report-contract-title"
                                placeholder="Please tell us the nature of the issue">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="report-body" class="col-lg-2 control-label">More Information:</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" id="report-contract-body" rows="8"
                                placeholder="Please provide us with more information so we can help"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info confirm-review-by-admin">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
<!-- START modal-->
<div id="review" tabindex="-1" role="dialog"
    aria-labelledby="reviewLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background:#1b92ba">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close" style="color:white">x</button>
                <h4 id="reviewLabel" class="modal-title" style="color:white"><strong>Leave a Review</strong></h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group text-center" style="margin-bottom: 20px; color:#1b92ba">
                       Please note that you can only review this freelancer once.
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Ratings:</label>
                        <div class="col-sm-10">
                            <table width="100%" cellspacing="10"> <tr>
                              <td valign="top" width="">
                               <table width="100%">
                                <tr>
                                 <td valign="top" width="50%">
                                    <div class="Clear">
                                        Punctuality
                                        <input class="star required" type="radio" name="punctuality" value="1" onchange="calculateAvg()" title="Worst"/>
                                        <input class="star" type="radio" name="punctuality" value="2" onchange="calculateAvg()" title="Bad"/>
                                        <input class="star" type="radio" name="punctuality" value="3" onchange="calculateAvg()" title="OK"/>
                                        <input class="star" type="radio" name="punctuality" value="4" onchange="calculateAvg()" title="Good"/>
                                        <input class="star" type="radio" name="punctuality" value="5" onchange="calculateAvg()" title="Best"/>
                                    </div>
                                    <br/>
                                   <div class="Clear">
                                        Friendliness
                                        <input class="star required" type="radio" name="friendliness" value="1" onchange="calculateAvg()" title="Worst"/>
                                        <input class="star" type="radio" name="friendliness" value="2" onchange="calculateAvg()" title="Bad"/>
                                        <input class="star" type="radio" name="friendliness" value="3" onchange="calculateAvg()" title="OK"/>
                                        <input class="star" type="radio" name="friendliness" value="4" onchange="calculateAvg()" title="Good"/>
                                        <input class="star" type="radio" name="friendliness" value="5" onchange="calculateAvg()" title="Best"/>
                                    </div>
                                    <br/>
                                 </td>
                                 <td valign="top" width="50%">
                                   <div class="Clear">
                                        Knowledge
                                        <input class="star required" type="radio" name="knowledge" value="1" onchange="calculateAvg()" title="Worst"/>
                                        <input class="star" type="radio" name="knowledge" value="2" onchange="calculateAvg()" title="Bad"/>
                                        <input class="star" type="radio" name="knowledge" value="3" onchange="calculateAvg()" title="OK"/>
                                        <input class="star" type="radio" name="knowledge" value="4" onchange="calculateAvg()" title="Good"/>
                                        <input class="star" type="radio" name="knowledge" value="5" onchange="calculateAvg()" title="Best"/>
                                   </div>
                                    <br/>
                                   <div class="Clear">
                                        Service Quality
                                        <input class="star required" type="radio" name="teaching" value="1" onchange="calculateAvg()" title="Worst"/>
                                        <input class="star" type="radio" name="teaching" value="2" onchange="calculateAvg()" title="Bad"/>
                                        <input class="star" type="radio" name="teaching" value="3" onchange="calculateAvg()" title="OK"/>
                                        <input class="star" type="radio" name="teaching" value="4" onchange="calculateAvg()" title="Good"/>
                                        <input class="star" type="radio" name="teaching" value="5" onchange="calculateAvg()" title="Best"/>
                                    </div>
                                    <br/>
                                 </td>
                                </tr>
                               </table>
                              </td>
                             </tr>
                            </table>
                            <br />
                            <div class="Clear" style="margin-left: 30%;">
                                 Overall Rating <input class="form-control textcenter" type="text" name="overall" id="overallRating" disabled style="background: white; width: 30%; text-align: center; color: #1b92ba">
                                 {# <input class="star {split:2} required" type="radio" name="overall" value="0.5" disabled="disabled"/>
                                 <input class="star {split:2}" type="radio" name="overall" value="1.0" disabled="disabled"/>
                                 <input class="star {split:2}" type="radio" name="overall" value="2.5" disabled="disabled"/>
                                 <input class="star {split:2}" type="radio" name="overall" value="2.0" disabled="disabled"/>
                                 <input class="star {split:2}" type="radio" name="overall" value="2.5" disabled="disabled"/>
                                 <input class="star {split:2}" type="radio" name="overall" value="3.0" disabled="disabled"/>
                                 <input class="star {split:2}" type="radio" name="overall" value="3.5" disabled="disabled"/>
                                 <input class="star {split:2}" type="radio" name="overall" value="4.0" disabled="disabled"/>
                                 <input class="star {split:2}" type="radio" name="overall" value="4.5" disabled="disabled"/>
                                 <input class="star {split:2}" type="radio" name="overall" value="5.0" disabled="disabled"/> #}
                             </div>
                             <script>
                             function calculateAvg() {
                                var punctuality = $('input[type="radio"][name="punctuality"]:checked').val();
                                var friendliness = $('input[type="radio"][name="friendliness"]:checked').val();
                                var knowledge = $('input[type="radio"][name="knowledge"]:checked').val();
                                var teaching = $('input[type="radio"][name="teaching"]:checked').val();

                                var overall = (parseInt(friendliness) + parseInt(knowledge) + parseInt(teaching) + parseInt(punctuality))/4;
                                $('#overallRating').val(overall);
                                //$('input[type="radio"][name="overall"]').rating(select, overall);
                             }                           
                             </script>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-6 control-label">When did you book this freelancer?</label>
                        <div class="col-sm-6">
                            <select name="contract_date_month_year" id="contract_date_month_year" class="form-control" size="1">
                                <?php
                                $date = new DateTime();
                                $interval = new DateInterval('P1M');
                                echo '<option value="' . $date->format('m') . ',' . $date->format('Y') . '" selected>' .
                                $date->format('F') . ' ' . $date->format('Y') . '</option>';
                                for ($i=0; $i < 12 ; $i++) {
                                    $date->sub($interval);
                                    echo '<option value="' . $date->format('m') . ',' . $date->format('Y') . '">' .
                                    $date->format('F') . ' ' . $date->format('Y') . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="review-title" class="col-lg-2 control-label">Title:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="review-title"
                                placeholder="Give your review a title" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="review-body" class="col-lg-2 control-label">Comment:</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" rows="8" id="review-comment"
                                placeholder="Tell us about your experience" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                       <div class="col-lg-offset-2 col-lg-10">
                          <div class="checkbox c-checkbox">
                             <label>
                                <input type="checkbox" id="tnc">
                                <span class="fa fa-check"></span>I certify that this review is based on my own experience and is my genuine opinion of this freelancer and that I have not been offered any incentive or payment to write this review. I also understand that QuikWit has a zero-tolerance policy on fake reviews.</label>
                          </div>
                       </div>
                    </div>
                    <input type="hidden" name="review-contractId" id="review-contractId" />
                    <input type="hidden" name="review-teacherId" id="review-teacherId" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info review-teacher-submit">Submit</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
{% endblock %}

{% block footer %}
{% endblock %}

{% block script %}
<!-- Star Rating -->
{{ javascript_include("third-party/star-rating/jquery.rating.js") }}
{{ javascript_include("third-party/star-rating/jquery.MetaData.js") }}

{{ javascript_include('third-party/async/async.js') }}

<script>
var listOfContract = [];
function processContracts(action){
    async.map(listOfContract, function(oneContract, cb){
        $.ajax({
            url: '/contract/' + action,
            data:{
                id: oneContract.id
            },
            success: function(res) {
                console.log(res);
                var result = JSON.parse(res);
                if (!result.status){
                    alert(result.message);
                }
                cb(null,true);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert('Sorry, the Contracts cannot be updated');
                cb(null,true);
             }
        });
    }, function(err, results){
        location.reload();
    });
}

function processReportContracts(action){
    var reason = $('#report-contract-title').val();
    var text = $('#report-contract-body').val();
    async.map(listOfContract, function(oneContract, cb){
        $.ajax({
            url: '/contract/' + action,
            type: 'POST',
            data:{
                id: oneContract.id,
                reason : reason,
                text : text
            },
            success: function(res) {
                console.log(res);
                var result = JSON.parse(res);
                if (!result.status){
                    alert(result.message);
                }
                cb(null,true);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert('Sorry, the Contracts cannot be updated');
                cb(null,true);
             }
        });
    }, function(err, results){
        location.reload();
    });
}

function saveReview(title, comment, overall, punctuality, friendliness,
 knowledge, teaching, contract_date_month_year, contractId, teacher){
        $.ajax({
            url: '/contract/reviewTeacher',
            type: 'POST',
            data:{
                title:title,
                comment:comment,
                overall:overall,
                punctuality:punctuality,
                friendliness:friendliness,
                knowledge:knowledge,
                teaching:teaching,
                teacher: teacher,
                contract_date_month_year:contract_date_month_year,
                contractId: contractId
            },
            success: function(res) {
                var result = JSON.parse(res);
                alert(result.message);
                //$('#review').modal('hide');
                //window.location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert('Sorry, the Contracts review was not saved.');
             }
        });
}
      $(document).ready(function() {
        <?php foreach($contracts->items as $idx => $contract): ?>
         $('#datetime_container_<?php echo $contract->cnt_id ?>_lg').datetimepicker({
            icons: {
                time:   "fa fa-clock-o",
                date:   "fa fa-calendar",
                up:     "fa fa-arrow-up",
                down:   "fa fa-arrow-down"
            },
            defaultDate: moment("<?php echo date('Y-m-j H:i', strtotime($contract->cnt_next)); ?>").format('MMM DD YYYY h:mm a'),
            format: 'MMM DD YYYY h:mm a',
            keepOpen: true,
            autoclose: true,
            todayBtn: false,
            minuteStep: 10,
            minDate: new Date(),
            inline: true,
            sideBySide: true
         });

         $('#datetime_container_<?php echo $contract->cnt_id ?>_xs').datetimepicker({
            icons: {
                time:   "fa fa-clock-o",
                date:   "fa fa-calendar",
                up:     "fa fa-arrow-up",
                down:   "fa fa-arrow-down"
            },
            defaultDate: moment("<?php echo date('Y-m-j H:i', strtotime($contract->cnt_next)); ?>").format('MMM DD YYYY h:mm a'),
            format: 'MMM DD YYYY h:mm a',
            keepOpen: true,
            autoclose: true,
            todayBtn: false,
            minuteStep: 10,
            minDate: new Date(),
            inline: true,
            sideBySide: true
         });

         //console.log($("#datetime_<?php echo $contract->cnt_id ?>").val());
         //console.log($("#datetime_<?php echo $contract->cnt_id ?>").attr("data-contract-date"));
       
         $("#datetime_container_<?php echo $contract->cnt_id ?>_lg").datetimepicker().on('change', function() {
            $("#datetime_save_<?php echo $contract->cnt_id ?>_lg").attr("disabled", false);
            $("#datetime_save_<?php echo $contract->cnt_id ?>_lg").removeClass("btn-default");
            $("#datetime_save_<?php echo $contract->cnt_id ?>_lg").addClass("btn-info");
            //var a = $('#datetime_<?php echo $contract->cnt_id ?>').val();
            //console.log(a);
            //var b = "<?php echo date('Y-m-j H:i', strtotime('a')) ?>";
            //console.log(b);
         });

         $("#datetime_container_<?php echo $contract->cnt_id ?>_xs").datetimepicker().on('change', function() {
            $("#datetime_save_<?php echo $contract->cnt_id ?>_xs").attr("disabled", false);
            $("#datetime_save_<?php echo $contract->cnt_id ?>_xs").removeClass("btn-default");
            $("#datetime_save_<?php echo $contract->cnt_id ?>_xs").addClass("btn-info");
            //var a = $('#datetime_<?php echo $contract->cnt_id ?>').val();
            //console.log(a);
            //var b = "<?php echo date('Y-m-j H:i', strtotime('a')) ?>";
            //console.log(b);
         });

         $('#datetime_save_<?php echo $contract->cnt_id ?>_lg').click(function (ev) {
            var contractId = $('#datetime_<?php echo $contract->cnt_id ?>').attr('data-contract-id');
            var a = $('#datetime_<?php echo $contract->cnt_id ?>').val();
            $.ajax({
                url: '/contract/updateContract',
                data:{
                    next: a,
                    contractId: contractId
                },
                success: function(res) {
                    var result = JSON.parse(res);
                    //alert(result.message);
                    //location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert('Sorry, the contract date was not updated');
                 }
            });
            location.reload(true);
         });

         $('#datetime_save_<?php echo $contract->cnt_id ?>_xs').click(function (ev) {
            var contractId = $('#datetime_<?php echo $contract->cnt_id ?>').attr('data-contract-id');
            var a = $('#datetime_<?php echo $contract->cnt_id ?>').val();
            $.ajax({
                url: '/contract/updateContract',
                data:{
                    next: a,
                    contractId: contractId
                },
                success: function(res) {
                    var result = JSON.parse(res);
                    //alert(result.message);
                    //location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert('Sorry, the contract date was not updated');
                 }
            });
            location.reload(true);
         }); 

        <?php endforeach?>
          $('.contract-table tr').click(function(event) {
              if (event.target.type !== 'checkbox') {
                  $(':checkbox', this).trigger('click');
              }
          });

          $("#pendingapply").on('click', function() {
             var action = $('#pendingbulk').val();
             var hasChecked = false;
             var list = [];
             var checkboxes = 0;
             if (action < 1){
                var noAction = 0;
                $.ajax({
                    url: '/contract/noActionReply',
                    type: 'POST',
                    data:{
                        noAction: noAction
                    },
                    success: function(res) {
                        location.reload();
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert('Please select an action to perform.');
                    }
                });

                return false;
             }
             $('.contract_checked').each(function(){
                if ($(this).is(':checked')){
                    hasChecked = true;
                    checkboxes++;
                }
             });
             if (!hasChecked){
                var noSelect = 0;
                $.ajax({
                    url: '/contract/noSelectReply',
                    type: 'POST',
                    data:{
                        noSelect: noSelect
                    },
                    success: function(res) {
                        location.reload();
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert('Please select at least one contract to process.');
                    }
                });

                return false;
             }
             var failed = true;
             var checkAll = 0;
             if (action == 1){
                 $('.contract_checked').each(function(){
                    var state = $(this).attr('data-state');
                    var id = $(this).attr('data-id');
                    if (state == 0 && $(this).is(':checked')){
                        checkAll++;
                        list.push({
                            id:id,
                            state: state
                        });
                    }
                 });
                if (checkAll == checkboxes){
                    listOfContract = list;
                    $('#accept').modal('show');
                    return true;
                }else{
                   var cannot = 0;
                   $.ajax({
                       url: '/contract/cannotAcceptReply',
                       type: 'POST',
                       data:{
                           cannot: cannot
                       },
                       success: function(res) {
                           location.reload();
                       },
                       error: function(xhr, ajaxOptions, thrownError) {
                           alert('Sorry, given current contracts selected, you cannot accept them.');
                       }
                   });

                   return false;
                }
            }  //accept

            if (action == 2){
                 $('.contract_checked').each(function(){
                    var state = $(this).attr('data-state');
                    var id = $(this).attr('data-id');
                    if (state == 0 && $(this).is(':checked')){
                        checkAll++;
                        list.push({
                            id:id,
                            state: state
                        });
                    }
                 });
                if (checkAll == checkboxes){
                    listOfContract = list;
                    $('#decline').modal('show');
                    return true;
                }else{
                    var cannot = 0;
                   $.ajax({
                       url: '/contract/cannotDeclineReply',
                       type: 'POST',
                       data:{
                           cannot: cannot
                       },
                       success: function(res) {
                           location.reload();
                       },
                       error: function(xhr, ajaxOptions, thrownError) {
                           alert('Sorry, given current contracts selected, you cannot decline them.');
                       }
                   });

                   return false;
                }
            }  //declined

             if (action == 3){
                 $('.contract_checked').each(function(index, element){
                    var state = $(this).attr('data-state');
                    var id = $(this).attr('data-id');
                     if (state == 1 && $(this).is(':checked')){
                        checkAll++;
                        list.push({
                            id:id,
                            state: state
                        });
                    }
                 });
                if (checkAll == checkboxes){
                    listOfContract = list;
                    $('#complete').modal('show');
                    return true;
                }else{
                    var cannot = 0;
                    $.ajax({
                        url: '/contract/cannotCompleteReply',
                        type: 'POST',
                        data:{
                            cannot: cannot
                        },
                        success: function(res) {
                            location.reload();
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert('Sorry, given current contracts selected, you cannot complete them.');
                        }
                    });

                    return false;
                }
            }  //completed

            /* if (action == 5){
                 $('.contract_checked').each(function(){
                    var state = $(this).attr('data-state');
                    var id = $(this).attr('data-id');
                    if (state == 1 && $(this).is(':checked')){
                        checkAll++;
                        list.push({
                            id:id,
                            state: state
                        });
                    }
                 });
                if (checkAll == checkboxes){
                    listOfContract = list;
                    $('#deny').modal('show');
                    return true;
                }else{
                    var cannot = 0;
                    $.ajax({
                        url: '/contract/cannotDenyReply',
                        type: 'POST',
                        data:{
                            cannot: cannot
                        },
                        success: function(res) {
                            location.reload();
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert('Sorry, given current contracts selected, you cannot deny them.');
                        }
                    });

                    return false;
                }
            }  //deny */

            if (action == 4){
                 $('.contract_checked').each(function(){
                    var state = $(this).attr('data-state');
                    var id = $(this).attr('data-id');
                    if (state == 1 && $(this).is(':checked') || state == 2 && $(this).is(':checked')){
                        checkAll++;
                        list.push({
                            id:id,
                            state: state
                        });
                    }
                 });
                if (checkAll == checkboxes){
                    listOfContract = list;
                    $('#review-by-admin').modal('show');
                    return true;
                }else{
                    var cannot = 0;
                    $.ajax({
                        url: '/contract/cannotReportReply',
                        type: 'POST',
                        data:{
                            cannot: cannot
                        },
                        success: function(res) {
                            location.reload();
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert('Sorry, given current contracts selected, you cannot put them under review.');
                        }
                    });

                    return false;
                }
            }  //report

            if (action == 6){
                 $('.contract_checked').each(function(){
                    var state = $(this).attr('data-state');
                    var id = $(this).attr('data-id');
                    if (state == 1 && $(this).is(':checked')){
                        checkAll++;
                        list.push({
                            id:id,
                            state: state
                        });
                    }
                 });
                if (checkAll == checkboxes){
                    listOfContract = list;
                    $('#cancel').modal('show');
                    return true;
                }else{
                    var cannot = 0;
                    $.ajax({
                        url: '/contract/cannotCancelReply',
                        type: 'POST',
                        data:{
                            cannot: cannot
                        },
                        success: function(res) {
                            location.reload();
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert('Sorry, given current contracts selected, you cannot cancel them.');
                        }
                    });

                    return false;
                }
            }  //cancel

          });
          $("#contract_pending").click(function(){
            if($(this).is(':checked')){
                window.location = '/contract?status=pending';
            }else{
            window.location = '/contract';
            }
          });
          $("#contract_completed").click(function(){
            if($(this).is(':checked')){
                window.location = '/contract?status=completed';
            }else{
                window.location = '/contract';
            }
          });
          $("#contract_active").click(function(){
            if($(this).is(':checked')){
                window.location = '/contract?status=active';
            }else{
                window.location = '/contract';
            }
          });
          $("#contract_review").click(function(){
            if($(this).is(':checked')){
                window.location = '/contract?status=review';
            }else{
            window.location = '/contract';
            }
          });
          $("#contract_cancel").click(function(){
            if($(this).is(':checked')){
                window.location = '/contract?status=cancel';
            }else{
                window.location = '/contract';
            }
          });
          $("#contract_rejected").click(function(){
            if($(this).is(':checked')){
                window.location = '/contract?status=rejected';
            }else{
                window.location = '/contract';
            }
          });
          $('.confirm-complete').click(function(){
            processContracts('complete');
            $('#complete').modal('hide');
            //window.location.reload();
          });
          $('.confirm-decline').click(function(){
            processContracts('decline');
            $('#decline').modal('hide');
            //window.location.reload();
          });
          /* $('.confirm-deny').click(function(){
            processContracts('deny');
            $('#deny').modal('hide');
          }); */
          $('.confirm-cancel').click(function(){
            processContracts('cancel');
            $('#cancel').modal('hide');
            //window.location.reload();
          });
          $('.confirm-accept').click(function(){
            processContracts('accept');
            $('#accept').modal('hide');
            //window.location.reload();
          });
          $('.confirm-review-by-admin').click(function(){
            //processContracts('review');
            processReportContracts('review');
            $('#review-by-admin').modal('hide');
            window.location.reload();
          });
          $('.review-teacher').click(function(){
            $('#review').modal('show');
            $('#review-contractId').val($(this).attr('data-id'));
            $('#review-teacherId').val($(this).attr('data-teacher'));
          });
          $('.review-teacher-submit').click(function(){
            var title = $('#review-title').val();
            var comment = $('#review-comment').val();
            var isChecked = $('#tnc').is(':checked');
            //var overall = $('input[type="radio"][name="overall"]:checked').val();
            var overall = $('#overallRating').val();
            var punctuality = $('input[type="radio"][name="punctuality"]:checked').val();
            var friendliness = $('input[type="radio"][name="friendliness"]:checked').val();
            var knowledge = $('input[type="radio"][name="knowledge"]:checked').val();
            var teaching = $('input[type="radio"][name="teaching"]:checked').val();
            var contract_date_month_year = $('#contract_date_month_year :selected').val();
            if (!overall || !punctuality || !friendliness || !knowledge || !teaching){
                alert('You must rate the teacher to submit a review.');
                return false;
            }
            if (title.length < 1){
                alert('you must provide a title');
                return false;
            }
            if (comment.length < 1){
                alert('you must provide a comment');
                return false;
            }
            if(!isChecked){
                alert('you must comply with QuikWit review policy.');
                return false;
            }
            saveReview(title, comment, overall, punctuality, friendliness, knowledge,
                teaching, contract_date_month_year,
                $('#review-contractId').val(), $('#review-teacherId').val());
                $('#review').modal('hide');
                //window.location.reload();
          });
      });

   </script>

{% endblock %}
