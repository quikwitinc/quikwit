{% extends "templates/home.volt" %}

{% block title %}
FAQ | QuikWit
{% endblock %}

{% block head %}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h1 class="text-center animation-slideDown"><strong>FAQ</strong></h1>
        <h2 class="h3 text-center animation-slideUp">Here is some information we thought could be helpful</h2>
    </div>
</section>
<!-- END Intro -->

<section class="site-section site-content" style="padding-bottom: 200px;">
	<div class="container">
		<!-- FAQ Content -->
		<div class="row">
		  
		    <div class="col-xs-12 col-md-offset-2 col-md-8">
		    
		        <!-- Intro Content -->
		        <h3 class="sub-header"><strong>General</strong></h3>
		        <div id="faq1" class="panel-group">
			        <div class="panel panel-default">
			            <div class="panel-heading">
			                <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq1" href="#faq1_q1">What is QuikWit?</a></h4>
			            </div>
			            <div id="faq1_q1" class="panel-collapse collapse">
			                <div class="panel-body">
			                    <p>QuikWit is an online directory listing freelancers in Canada. If you are a freelancer providing local services such as photography or catering, we can help you get more exposure in your area.</p>
			                </div>
			            </div>
			        </div>
			        <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq1" href="#faq1_q2">How much does it cost to sign up?</a></h4>
		                </div>
		                <div id="faq1_q2" class="panel-collapse collapse">
		                    <div class="panel-body">It is absolutely free to sign up for a QuikWit account.</div>
		                </div>
		            </div> 
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq1" href="#faq1_q3">Where does QuikWit operate?</a></h4>
		                </div>
		                <div id="faq1_q3" class="panel-collapse collapse">
		                    <div class="panel-body">QuikWit currently only operates within Canada.</div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq1" href="#faq1_q4">How do I leave a review?</a></h4>
		                </div>
		                <div id="faq1_q4" class="panel-collapse collapse">
		                    <div class="panel-body">
		                        <p>Clients can only rate freelancers after the successful completion of a contract. This is to reduce the likelihood of fake reviews. The review button becomes visible on the Manage page once your contract is completed.</p>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <!-- END Intro Content -->

		        <!-- Features Content -->
		        <h3 class="sub-header"><strong>Clients</strong></h3>
		        <div id="faq2" class="panel-group">
		        	 <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq2" href="#faq2_q1">What services can I find on the website?</a></h4>
		                </div>
		                <div id="faq2_q1" class="panel-collapse collapse">
		                    <div class="panel-body">Services fall into a wide range including Guitar lessons, Personal Training, Photography etc. If you can't find a freelancer for a particular service, simply post a gig and we will notify you once freelancers for that service become available in your area.</div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq1" href="#faq2_q2">How do I reschedule or cancel a contract?</a></h4>
		                </div>
		                <div id="faq2_q2" class="panel-collapse collapse">
		                    <div class="panel-body">After a contract is created between a client and a freelancer, the contract will be visible under the Manage tab. The date of the contract can be changed from the contracts table. The other party is notified whenever this happens. Please contact the freelancer before changing the date and time of your contract. This is to avoid scheduling conflicts.</div>
		                </div>
		        	<div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq2" href="#faq2_q3">How safe are the freelancers on the website?</a></h4>
		                </div>
		                <div id="faq2_q3" class="panel-collapse collapse">
		                    <div class="panel-body">Clients have the option to choose freelancers with completed profiles and good reviews. We advise clients to only select freelancers they feel completely comfortable with after messaging or talking on the phone. However we are currently working on providing optional bacKground checks, online payments and other safety measures.</div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq2" href="#faq2_q4">How do I make payments?</a></h4>
		                </div>
		                <div id="faq2_q4" class="panel-collapse collapse">
		                    <div class="panel-body">We are currently working on introducing an online payment system. We advise that users exercise caution when selecting and meeting freelancers. Before creating a contract, clients should discuss with freelancers which payment method they prefer and make the necessary arrangements. </div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq2" href="#faq2_q5">Are there any additional fees?</a></h4>
		                </div>
		                <div id="faq2_q5" class="panel-collapse collapse">
		                    <div class="panel-body">No. Clients only pay the amount agreed upon with the freelancer.</div>
		                </div>
		            </div>
		            
		            </div>
		            
		            
		            {# <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq2" href="#faq2_q4">Are you working on new features?</a></h4>
		                </div>
		                <div id="faq2_q4" class="panel-collapse collapse">
		                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor. Vestibulum ullamcorper, odio sed rhoncus imperdiet, enim elit sollicitudin orci, eget dictum leo mi nec lectus. Nam commodo turpis id lectus scelerisque vulputate. Integer sed dolor erat. Fusce erat ipsum, varius vel euismod sed, tristique et lectus? Etiam egestas fringilla enim, id convallis lectus laoreet at. Fusce purus nisi, gravida sed consectetur ut, interdum quis nisi. Quisque egestas nisl id lectus facilisis scelerisque? Proin rhoncus dui at ligula vestibulum ut facilisis ante sodales! Suspendisse potenti. Aliquam tincidunt sollicitudin sem nec ultrices. Sed at mi velit. Ut egestas tempor est, in cursus enim venenatis eget! Nulla quis ligula ipsum.</div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq2" href="#faq2_q5">What are your best features?</a></h4>
		                </div>
		                <div id="faq2_q5" class="panel-collapse collapse">
		                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor. Vestibulum ullamcorper, odio sed rhoncus imperdiet, enim elit sollicitudin orci, eget dictum leo mi nec lectus. Nam commodo turpis id lectus scelerisque vulputate. Integer sed dolor erat. Fusce erat ipsum, varius vel euismod sed, tristique et lectus? Etiam egestas fringilla enim, id convallis lectus laoreet at. Fusce purus nisi, gravida sed consectetur ut, interdum quis nisi. Quisque egestas nisl id lectus facilisis scelerisque? Proin rhoncus dui at ligula vestibulum ut facilisis ante sodales! Suspendisse potenti. Aliquam tincidunt sollicitudin sem nec ultrices. Sed at mi velit. Ut egestas tempor est, in cursus enim venenatis eget! Nulla quis ligula ipsum.</div>
		                </div>
		            </div> #}
		        </div> 
		        <!-- END Features Content -->

		        <!-- Subscriptions Content -->
		        <h3 class="sub-header"><strong>Freelancers</strong></h3>
		        <div id="faq3" class="panel-group">
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq3" href="#faq3_q1">How do I become a freelancer?</a></h4>
		                </div>
		                <div id="faq3_q1" class="panel-collapse collapse">
		                    <div class="panel-body">The option to switch your account to a "Freelancer Account" can be found in Settings under the "Account Type" panel.</div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq3" href="#faq3_q2">How can I attract more clients?</a></h4>
		                </div>
		                <div id="faq3_q2" class="panel-collapse collapse">
		                    <div class="panel-body">Freelancers with pictures, an introduction video and completed profiles are more likely to be selected by clients. Reviews and ratings left by clients will also encourage other prospective clients to reach out to you. </div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq3" href="#faq3_q3">Will I have to sign a contract with QuikWit?</a></h4>
		                </div>
		                <div id="faq3_q3" class="panel-collapse collapse">
		                    <div class="panel-body">Freelancers are not required to sign any contracts with QuikWit and are not employees of QuikWit. However, all users will have to agree to our Terms of Use and Privacy Policy when creating their account.</div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq3" href="#faq3_q4">How do I get paid?</a></h4>
		                </div>
		                <div id="faq3_q4" class="panel-collapse collapse">
		                    <div class="panel-body">Before creating contracts with clients, freelancers should discuss which payment method they prefer and make the necessary arrangements. </div>
		                </div>
		            </div>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq3" href="#faq3_q5">What does it cost to use QuikWit?</a></h4>
		                </div>
		                <div id="faq3_q5" class="panel-collapse collapse">
		                    <div class="panel-body">Freelancers who sign up for early access will be able to use this service free of charge for the first 3 months. The site will not always be free. Freelancers will be informed if ever they are required to pay fees. You can find more about our payment terms in our <a href="/terms" target="_blank" class="register-terms">Terms of Use</a>.</div>
		                </div>
		            </div>
		            
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title"><i class="fa fa-angle-right"></i> <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq3" href="#faq3_q6">How can I deactivate my freelancer account?</a></h4>
		                </div>
		                <div id="faq3_q6" class="panel-collapse collapse">
		                    <div class="panel-body">You can switch back to a regular account by clicking "Switch to Client Account" in Settings. </div>
		                </div>
		            </div>
		        </div>
		        <!-- END Subscriptions Content -->
		    </div>
		</div>
		<!-- END FAQ Content -->
    </div>
</section>
{% endblock %}

{% block script %}
{% endblock %}
