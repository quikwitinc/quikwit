{% extends "templates/home.volt" %}

{% block title %}
Gigs for You | QuikWit
{% endblock %}


{% block head %}
<style>
.clickable-row:hover{
    background: #e7e7e7 !important; 
}
@media screen and (max-width: 768px) {
    .text-align {
        text-align: left;

    }
}
@media screen and (min-width: 769px) {
    .text-align {
        text-align: center;

    }
}
</style>
{% endblock %}

{% block content %}
<section class="main-content">
    <!-- Inbox Content -->
    <div class="row">
        <!-- Inbox Menu -->
        <!-- leads List -->
        <div class="container">
            {% if userTeach == 1 %}
            <!-- leads List Block -->
            <div class="block">
                <!-- leads List Title -->
                <div class="block-title">
                    <h2>Gigs <strong>({{ unreadGigCount }})</strong></h2>
                    {# <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i></a>
                    </div> #}
                </div>
                <!-- END leads List Title -->

                <!-- leads List Content -->
                <div class="row" >
                    <div class="text-left col-sm-6" style="margin-bottom: 20px;">
                        These are matching gig posts sent to you based on your skillset and location. Click a gig for more details and to contact the client.
                    </div>
                    <div class="text-right col-sm-6" style="margin-bottom: 20px;">
                        <strong>{{ leadOffset }} - {{ leadCountPerPage }}</strong> from <strong>{{ totalLeadCount }}</strong>
                        <div class="btn-group btn-group-sm">
                            <a href="" class="btn btn-sm btn-default"><i class="fa fa-angle-left"></i></a>
                            <a href="" class="btn btn-sm btn-default"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    <!-- Use the first row as a prototype for your column widths -->
                    {% if leads is defined %}
                        {% for lead in leads %}
                        {% if lead['lead_count'] > 0 and lead['req_userid'] != userId %}
                        <div class="clickable-row col-xs-12" data-href="{{ url('gig/viewRequest/' ~ lead['req_id']) }}" style="padding: 20px; border-bottom: 1px solid #D1D1D1; cursor: pointer;">
                            {# <td class="text-center" style="width: 30px;">
                                <input type="checkbox"/>
                            </td> #}
                        {% else %}
                        <div class="clickable-row col-xs-12" data-href="{{ url('gig/viewRequest/' ~ lead['req_id']) }}" style="padding: 20px; border-bottom: 1px solid #D1D1D1; cursor: pointer; background: #F1F1F1;">
                        {% endif %}

                            <div class="col-xs-1 text-align" >

                                {% if lead['lead_count'] > 0 and lead['req_userid'] != userId %}          
                                <span style="padding-top: 10px; margin-left: -20px; color: #52D23A" class="gi gi-folder_plus gi-1.4x"></span>              
                                {% endif %}
                                <?php $messageModel = new Messages(); 
                                      $userSent = $messageModel->checkIfUserReplyRequest($lead['req_id'], $userId);
                                      if($userSent == true){
                                        echo '<span style="padding-top: 10px; margin-left: -20px; color: #52D23A" class="gi gi-message_out gi-1.4x"></span> ';
                                      }
                                ?>
                            </div>
                            <div class="col-sm-2 col-xs-7 text-align">
                                {% if lead['user_avatar'] is defined %}
                                <img src="{{ url("uploads/avatar/" ~ lead['user_avatar']) }}" alt="{{ lead['user_firstname']|capitalize ~ ' ' ~ lead['user_lastname']|capitalize }}" class="img-thumbnail hidden-xs" style="max-width: 60px;">
                                {% else %}
                                <img src="/img/user/blank-avatar" alt="{{ lead['user_firstname']|capitalize ~ ' ' ~ lead['user_lastname']|capitalize }}" class="img-thumbnail hidden-xs" style="max-width: 60px;">
                                {% endif %}

                                {% if lead['lead_count'] > 0 and lead['req_userid'] != userId %} 
                                    <div style="font-size: 16px; color: #1b92ba;">{{ lead['user_firstname'] }} {{ lead['user_lastname'] }}</div>
                                {% elseif lead['lead_count'] > 0 and lead['req_userid'] == 0 %}
                                    <div style="font-size: 16px; color: #1b92ba;">Anonymous Poster</div>
                                {% elseif lead['req_userid'] == 0 %}
                                    <div style="font-size: 16px;">Anonymous Poster</div>
                                {% else %}
                                    <div style="font-size: 16px;">{{ lead['user_firstname'] }} {{ lead['user_lastname'] }}</div>
                                {% endif %}

                                <div class="hidden-sm hidden-md hidden-lg text-align" style="font-size: 16px;">{{ lead['req_title'] }}</div>
                                <span class="text-muted hidden-sm hidden-md hidden-lg text-align" style="color: #808080">
                                    {{ lead['tag'] }}
                                    {% if lead['req_fixed'] == 0 and lead['req_price'] != 0 %}
                                    ${{ lead['req_price'] }} <p class="text-muted">(per hour)</p>
                                    {% elseif lead['req_fixed'] == 1 and lead['req_price'] != 0 %}
                                    ${{ lead['req_price'] }} <p class="text-muted">(flat rate)</p> 
                                    {% elseif  lead['req_fixed'] == 0 and lead['req_price'] == 0 %}
                                    Budget not specified <p class="text-muted">(per hour)</p>
                                    {% elseif  lead['req_fixed'] == 1 and lead['req_price'] == 0 %}
                                    Budget not specified <p class="text-muted">(flat rate)</p>
                                    {% endif %}
                                    {% if lead['req_hour'] != 0 %}
                                    Estimated&nbsp;{{ lead['req_hour'] }}&nbsp;hrs
                                    {% else %}
                                    Hours not specified
                                    {% endif %}
                                </span>        
                            </div>
                            <div class="col-sm-3 hidden-xs text-align" >
                                <div style="font-size: 16px; padding-top: 10px">{{ lead['req_title'] }}</div>
                            </div>
                            <div class="col-sm-3 hidden-xs">
                                <span class="text-muted" style="color: #808080"><div style="padding-top: 10px">{{ lead['tag'] }}<br>
                                    {% if lead['req_fixed'] == 0 and lead['req_price'] != 0 %}
                                    ${{ lead['req_price'] }} <p class="text-muted">(per hour)</p>
                                    {% elseif lead['req_fixed'] == 1 and lead['req_price'] != 0 %}
                                    ${{ lead['req_price'] }} <p class="text-muted">(flat rate)</p> 
                                    {% elseif  lead['req_fixed'] == 0 and lead['req_price'] == 0 %}
                                    Budget not specified <p class="text-muted">(per hour)</p>
                                    {% elseif  lead['req_fixed'] == 1 and lead['req_price'] == 0 %}
                                    Budget not specified <p class="text-muted">(flat rate)</p>
                                    {% endif %}
                                    <br>
                                    {% if lead['req_hour'] != 0 %}
                                    Estimated&nbsp;{{ lead['req_hour'] }}&nbsp;hrs
                                    {% else %}
                                    Hours not specified
                                    {% endif %}
                                </div></span>
                            </div>
                            <div class="col-sm-1 hidden-xs">
                                <span class="text-muted" style="color: #808080"><div style="padding-top: 10px">
                                    {% if lead['req_status'] == 1 %}
                                    Available
                                    {% elseif lead['req_status'] == 2 %}
                                    Assigned
                                    {% elseif lead['req_status'] == 3 %}
                                    Cancelled
                                    {% endif %}
                                </div></span>
                            </div>
                            <div class="col-sm-2 text-right pull-right" style="padding: 0px 0px 0px 0px; ">{#<em><?php echo Component\PrettyPrint::prettyPrint($lead['req_created']); ?></em>#}
                                <?php   if ($timezone) {
                                            $userTimezone = new DateTimeZone($timezone); 
                                            $newDate = new DateTime();
                                            $newDate->setTimestamp($lead['req_created']);
                                            $newDate->setTimeZone($userTimezone);
                                            echo $newDate->format('M jS'); 
                                        }else{
                                            echo date('M jS', $lead['req_created']) . " - UTC";
                                        }

                                        ?>
                            </div>
                        </div>
                        {% endfor %}
                    {% endif %}
                </div>
                {% if leads is empty %}
                <hr>
                <div class="row" style="padding: 20px; color: #1b92ba;">
                There are currently no gigs available based on your skillset and location. If a client post a gig that matches we will let you know ASAP!
                </div>
                {% endif %}
                <!-- END leads List Content -->
            </div>
            <!-- END leads List Block -->
            {% else %}
            <!-- leads List Block -->

            <div class="block">
                <!-- leads List Title -->
                <div class="block-title">
                    <h2>There are ({{ totalLeadCount }}) client Gigs sent to you</h2>
                    {# <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i></a>
                    </div> #}
                </div>
                <!-- END leads List Title -->

                <!-- leads List Content -->
                <div class="row text-center">
                    <h4>Please activate to be a freelancer in Settings to unlock this feature!</h4>
                    <img src="/img/bg7" />
                </div>
                <!-- END leads List Content -->
            </div>
            <!-- END leads List Block -->
            {% endif %}
        </div>
        <!-- END leads List -->
    </div>
    <!-- END Inbox Content -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

</section>

{% endblock %}

{% block footer %}
{% endblock %}

{% block script %}
<script>
$(document).ready(function(){
    $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
});
</script>
{% endblock %}
