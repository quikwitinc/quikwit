{% extends "templates/home.volt" %}

{% block title %}
How to Hire | QuikWit
{% endblock %}

{% block style %}
<style>
  .element {
  position: relative;
  top: 50%;
  transform: translateY(-50%);
}
</style>
{% endblock %}



{% block content %}
<section class="site-section site-section-light site-section-top" style="background: linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4) ), url('img/mplanner'); background-position: 50% 50%; background-repeat: no-repeat; background-size: cover; min-height: 500px;" id="headerPic">

    <div class="container" style="margin-top: 60px;">
        <h1 style="font-size: 50px; font-family: 'Quicksand', sans-serif; padding-top: 20px;" class="text-center animation-slideDown"><strong>How to hire a Freelancer</h1>
    </div>
    <br>
    <br>
    <div class="col-lg-offset-4 col-lg-4 col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-12 text-center push container">
        <a class="btn btn-lg btn-primary" style="font-size: 30px; padding:10px 20px 10px 20px;" href="{{ url('#signup') }}"><strong>Hire Now</strong></a>
    </div>
</section>

{#
<section class="site-section">
    <!-- section title -->
    <div class="title" style="margin-bottom: 60px; text-align: center;">
        <h3><strong>You can start offering some of these services in your neighborhood<strong></h3>
        <hr style="border: none; background-color: #1b92ba; width: 80px; height: 3px;">
    </div>
    <!-- section title end -->

    <div class="container">
        <div>
            {% if tagList is defined %}
            {{ tagList }}
            {% endif %}
        </div>
    </div>
</section>
<!-- END Promo #3 -->
#}
<!-- Promo #2 -->
<section class="site-content site-section site-slide-content">
    <div class="container">
        <!-- section title -->

        <!-- section title end -->
        <div class="row">
            <div class="col-sm-4 col-xs-12 visibility-none pull-right" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <img src="/img/optimized/HQW1" alt="Promo #1" class="img-responsive text-center" style="width: 220px; margin: 20px;">
            </div>
            <div class="col-sm-6 col-sm-offset-1 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <h1 class="h2 site-heading site-heading-promo" style="color: #4C4E50"><strong>The Search</strong></h1>
                <p class="promo-content">Scan through the profiles of our local freelancers to find the right fit. We can also help you reach all the freelancers for a particular service through our <a href="/post">QuikPost</a> form and at no cost.</p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <img src="/img/optimized/HQW2" alt="Promo #2" class="img-responsive text-center" style="width: 230px; margin: 20px;">
            </div>
            <div class="col-sm-6 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <h1 class="h2 site-heading site-heading-promo" style="color: #4C4E50"><strong>Get quotes</strong></h1>
                <p class="promo-content">Freelancers will get back to you with their estimated price and amount of time needed to complete your project.</p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-4 col-xs-12 visibility-none pull-right" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <img src="/img/optimized/HQW3" alt="Promo #3" class="img-responsive" style="width: 230px; margin: 20px;">
            </div>
            <div class="col-sm-6 col-sm-offset-1 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <h1 class="h2 site-heading site-heading-promo" style="color: #4C4E50"><strong>Compare options</strong></h1>
                <p class="promo-content">View the profiles of interested freelancers to learn about their past projects and to read reviews from past clients. Compare their offerings to find the best value.</p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <img src="/img/optimized/HQW4" alt="Promo #4" class="img-responsive" style="width: 230px; margin: 20px;">
            </div>
            <div class="col-sm-6 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <h1 class="h2 site-heading site-heading-promo" style="color: #4C4E50;"><strong>Choose the right fit</strong></h1>
                <p class="promo-content">Choose a freelancer matching your budget, schedule and style. Finalize the details of your project and create a contract with the freelancer you choose.</p>
            </div>
        </div>
        <hr>
        <div class="row" style="padding-bottom: 40px;">
            <div class="col-sm-4 col-xs-12 visibility-none pull-right" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180" style="padding-bottom: 40px">
                <img src="/img/optimized/HQW5" alt="Promo #5" class="img-responsive" style="width: 230px; margin: 20px;">
            </div>
            <div class="col-sm-6 col-sm-offset-1 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <h1 class="h2 site-heading site-heading-promo" style="color: #4C4E50"><strong>You’re done!</strong></h1>
                <p class="promo-content">We care what you think so please share your thoughts with us by reviewing freelancers and providing feedbacks. Reviews help freelancers improve and others to find the right fit. </p>
            </div>
        </div> 
    </div>
</section>

<section class="site-section site-section-light site-section-top themed-background" style="background: #1b92ba">
    <div class="container">
        <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-12 text-center push container">
        <h3><b>Do you want us to help you find a freelancer?</b><h3>
        <br>
        <a class="btn btn-lg btn-success" style="font-size: 24px; padding:10px 20px 10px 20px;" href="{{ url('post') }}"><strong>Yes!</strong></a>
    </div>
        {#<h2 class="h3 text-center animation-slideUp">Fill with your email to receive instructions on resetting your password!</h2>#}
    </div>
</section>
{% endblock %}