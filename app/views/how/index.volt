{% extends "templates/home.volt" %}

{% block head %}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h1 class="text-center animation-slideDown"><i class="fa fa-arrow-right"></i> <strong>How It Works?</strong></h1>
        <h2 class="h3 text-center animation-slideUp">3 Easy Step to Book a Lesson!</h2>
    </div>
</section>
<!-- END Intro -->

<!-- Step 1 Header -->
<section class="site-content site-section site-section-light themed-background-default">
    <div class="container">
        <div class="site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInQuick" data-element-offset="-100">
            <h1 class="site-heading"><i class="fa fa-arrow-right"></i> <strong> Step One</strong></h1>
        </div>
    </div>
</section>
<!-- END Step 1 Header -->

<!-- Step 1 -->
<section class="site-content site-section site-slide-content">
    <div class="container">
        <div class="row visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInQuick" data-element-offset="-100">
            <div class="col-sm-6 site-block">
                <img src="http://lorempixel.com/540/540/sports" alt="Promo #1" class="img-responsive">
            </div>
            <div class="col-sm-6 col-md-5 col-md-offset-1 site-block">
                <h3 class="h2 site-heading site-heading-promo"><strong>Search</strong> our database</h3>
                <p class="promo-content">The first step is to search our database using the searchbar at the top. You can check out the teacher's rating, location etc. and find the perfect teacher!</p>
            </div>
        </div>
    </div>
</section>
<!-- END Step 1 -->

<!-- Step 2 Header -->
<section class="site-content site-section site-section-light themed-background-default">
    <div class="container">
        <div class="site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInQuick" data-element-offset="-100">
            <h1 class="site-heading"><i class="fa fa-arrow-right"></i> <strong> Step Two</strong></h1>
        </div>
    </div>
</section>
<!-- END Step 2 Header -->

<!-- Step 2 -->
<section class="site-content site-section site-slide-content">
    <div class="container">
        <div class="row visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInQuick" data-element-offset="-100">
            <div class="col-sm-6 col-md-5 site-block">
                <h3 class="h2 site-heading site-heading-promo"><strong>Sign up</strong> to our platform</h3>
                <p class="promo-content">After finding your perfect teacher, you should sign up to our platform using our easy and quick to use registration form. You can get then start to message the teacher to find out more.</p>
            </div>
            <div class="col-sm-6 col-md-offset-1 site-block">
                <img src="http://lorempixel.com/540/540/sports" alt="Promo #2" class="img-responsive">
            </div>
        </div>
    </div>
</section>
<!-- END Step 2 -->

<!-- Step 3 Header -->
<section class="site-content site-section site-section-light themed-background-default">
    <div class="container">
        <div class="site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInQuick" data-element-offset="-100">
            <h1 class="site-heading"><i class="fa fa-arrow-right"></i> <strong> Step Three</strong></h1>
        </div>
    </div>
</section>
<!-- END Step 3 Header -->

<!-- Step 3 -->
<section class="site-content site-section site-slide-content">
    <div class="container">
        <div class="row visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInQuick" data-element-offset="-100">
            <div class="col-sm-6 site-block">
                <img src="http://lorempixel.com/540/540/sports" alt="Promo #4" class="img-responsive">
            </div>
            <div class="col-sm-6 col-md-5 col-md-offset-1 site-block">
                <h3 class="h2 site-heading site-heading-promo"><strong>Sit back</strong> and relax!</h3>
                <p class="promo-content">Lastly, you can book the lesson with the teacher and that is it! For more infomration regarding payments and managing your lessons, please visit <a href="../faq">FAQ</a> here.</p>
            </div>          
        </div>
    </div>
</section>
<!-- END Step 3 -->
{% endblock %}

{% block script %}
{% endblock %}
