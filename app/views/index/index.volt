{% extends "templates/home.volt" %}

{% block head %}
{{ stylesheet_link('third-party/slick-1.5.9/slick/slick.css') }}
{{ stylesheet_link('third-party/slick-1.5.9/slick/slick-theme.css') }}
{% endblock %}

{% block style %}
<style>
.site-nav-link {
    color: #ffffff;
}
.ui-autocomplete {
    z-index: 1000;
}
.slick-prev:before, .slick-next:before{
    color: #1b92ba;
    font-size: 30px;
}
</style>
{% endblock %}

{% block navbar %}
<header id="navbar" style="padding-bottom: 10px; background-color: rgba(0,0,0,0);"> 
    <div style="padding-right: 50px;">
        <!-- Site Logo -->
        <a href="{{ url('') }}" id="quikwit-logo" class="site-logo hidden-xs hidden-sm" style="margin-top: -15px; margin-left: 20px;">
            <img alt="Brand" id="logo-md" src="/img/optimized/QW7.png" style="height: 70px;">
        </a>
        <a href="{{ url('') }}" class="site-logo visible-sm" style="margin-top: -15px; margin-left: 20px;">
            <img alt="Brand" id="logo-sm" src="/img/optimized/QW8.png" style="height: 55px;">
        </a>
        <a href="{{ url('') }}" class="site-logo visible-xs" style="margin-top: -15px; margin-left: 5px;">
            <img alt="Brand" id="logo-xs" src="/img/optimized/QW8.png" style="height: 55px;">
        </a>
        <!-- Site Logo -->
        <form role="search" method="get" id="search-validation" class="search-form form-horizontal col-md-4 col-sm-10 col-xs-8" action="search/index" style="padding: 0px;">
            <div class="form-group push-bit">
                <div class="col-xs-12">
                    <div class="input-group">
                        <input type="text" name="q" style="border-color: #1BBAE1;" class="form-control" placeholder="What service are you looking for?" id="searchService"
                               value="<?php if (isset($_GET['q'])) { 
                                                echo $_GET['q']; 
                                            } else {
                                                echo "";
                                            } ?>">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary" style="padding-top: 3px; height: 34px;"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- Site Navigation -->
        <nav >
            <!-- Menu Toggle -->
            <!-- Toggles menu on small screens -->
            <a href="javascript:void(0)" class="btn btn-default site-menu-toggle visible-xs visible-sm" style="margin-right: -30px; margin-top: -2px;">
                <i class="fa fa-bars"></i>
            </a>
            <!-- END Menu Toggle -->

            <!-- Main Menu -->
            <ul class="site-nav">
                <!-- Toggles menu on small screens -->
                <li class="visible-xs visible-sm">
                    <a href="javascript:void(0)" class="site-menu-toggle text-center">
                        <i class="fa fa-times"></i>
                    </a>
                </li>
                <!-- END Menu Toggle -->
                {# <li>
                    <a href="javascript:void(0)" class="site-nav-sub"><i class="fa fa-angle-down site-nav-arrow"></i>Browse Categories</a>
                    <ul>
                        <li>
                            <a href="#">Art & Crafts</a>
                            <a href="#">Health & Fitness</a>
                            <a href="#">Music</a>
                            <a href="#">IT & Software</a>
                            <a href="#">Academic</a>
                            <a href="#">Culinary</a>
                            <a href="#">Language</a>
                        </li>
                    </ul>
                </li> #}
                {# <li>
                    <a href="{{ url('review') }}">Write a Review</a>
                </li> #}             
                <li> 
                    <a href="/post" id="postLink" class="site-nav-link" style="border: solid 1px #FFFFFF;">Post a Gig</a>
                </li>
                <li>
                    <a href="/browse" class="site-nav-link">Browse Gigs</a>
                </li>
                <li>
                    <a href="/how-to-hire" id="proLink" class="site-nav-link">How to Hire</a>
                </li>
                {% if user is defined %}  
                <li>
                    <a href="javascript:void(0)" class="site-nav-link" data-target="#feedback" data-toggle="modal">Give Feedback</a>
                </li>
                {#<li>
                    <a href="javascript:void(0)" id="startTourBtn">Take a tour</a>
                </li>#}
                {% endif %}
                {% if user is empty %}
                <li>
                    <a href="{{ url('#signup') }}" class="site-nav-link">Sign Up</a>
                </li>
                {% endif %}
                {% if user is defined %}
                <li>
                    <a href="/overview" id="myAccount" class="site-nav-sub site-nav-link"><i class="fa fa-angle-down site-nav-arrow"></i>Hi, {{ firstname }} {% if unreadMessageCount > 0 %} <span class="badge pull-down" style="background: #30D611; padding: 5px 1px;"> </span>{% endif %}</a> 

                    {# <a href="javascript:void(0)" id="myAccount" class="site-nav-sub"><i class="fa fa-angle-down site-nav-arrow">  </i>Hi, {{ firstname }} <span id="user_messages_count">{% if unreadMessageCount > 0 %} <span class="badge pull-down" style="background: rgb(36, 232, 0); padding: 5px 1px;"> </span>{% endif %}</span></a> #}

                    <ul style="border: solid 1px; border-color: #1BBAE1">
                        <li>
                            <a href="/message" {% if unreadMessageCount > 0 %} style="color: #30D611" {% endif %}>Messages <i class="fa fa-envelope-o fa-fw pull-right" {% if unreadMessageCount > 0 %} style="color: #30D611" {% endif %}></i>       
                            </a>
                        </li>
                        {% if userTeach == 1 %}
                        <li>
                            <a href="/gig" {% if unreadGigCount > 0 %} style="color: #30D611" {% endif %}>Matching Gigs
                                <i class="fa fa-folder-open-o fa-fw pull-right" {% if unreadGigCount > 0 %} style="color: #30D611" {% endif %}></i>
                            </a>
                        </li>
                        {% endif %}
                        <li>
                            <a href="/post/postingList">My Posts
                                <i class="fa fa-spinner fa-fw pull-right"></i>
                            </a>
                        </li>
                        <li>
                            <a href="/contacts">Contacts
                                <i class="fa fa-user-plus fa-fw pull-right"></i>
                            </a>
                        </li>
                        <li>
                            <a href="/contract">Manage
                                <i class="fa fa-tasks fa-fw pull-right"></i>
                            </a>
                        </li>
                        <li>
                            <a href="/overview">Profile
                            <i class="fa fa-pencil fa-fw pull-right"></i>
                            </a>
                        </li>
                        <li>
                            <a href="/settings" id="settings">Settings
                                <i class="fa fa-cog fa-fw pull-right"></i>
                            </a>
                        </li>
                        <li>
                            <a href="/faq">FAQ
                                <i class="fa fa-question fa-fw pull-right"></i>
                            </a>
                        </li>
                        <li>
                            {{ link_to("signout", "Logout") }}
                        </li>
                    </ul>
                </li>
                {% else %}
                <li>                               
                    <a href="{{ url('signin')}}" class="site-nav-link">Login</a>
                </li>
                {% endif %}
            </ul>
            <!-- END Main Menu -->
        </nav>
   
        <!-- END Site Navigation -->
    </div>
</header>
{% endblock %}

{% block content %}
{# <!-- Intro -->
<section class="site-section site-section-light site-section-top" style="background: linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4) ), url('/img/optimized/mscreen'); background-position: 50% 50%; background-repeat: no-repeat; background-size: cover; min-height: 700px;" id="headerPic">

    <div class="col-lg-offset-2 col-lg-8 text-center push container" style="padding-top: 150px;">
        <h1 style="font-size: 50px; font-weight: 900px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;" class="text-center animation-slideDown"><strong>Discover freelancers providing local services around you</strong></h1>
        <hr style="width: 50%;">
        <h2 class="text-center animation-slideUp push" style="font-size: 22px">an online directory listing local service providing freelancers in Canada
        </h2>
    </div>
    <br>
    {% if user is empty %}
    <div class="text-center container">
        <a class="btn btn-lg btn-success" style="font-size: 30px; padding:10px 20px 10px 20px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;" href="{{ url('#signup') }}"><strong>Sign Up for Free</strong></a>
    </div>
    {% endif %}
</section>
<!-- END Intro --> #}

<!-- Gallery Carousel Block -->
<section class="site-section-light" id="headerPic" style="min-height: 700px;">
    <!-- Gallery Carousel Content -->
    <div id="example-carousel4" class="carousel slide" style="margin: 0px;">
        <div style="padding-top: 120px; position: absolute; z-index: 12; width: 100%;">
            <div class="col-lg-offset-2 col-lg-8 text-center push container">
               <h1 style="padding-top: 80px; font-size: 60px; font-weight: 900px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;" class="text-center animation-slideDown"><strong>Find Clients. Get Gigs.</strong></h1>
               <hr style="width: 70%;">
               <h2 class="text-center animation-slideUp push" style="font-size: 22px"><b>QuikWit is an online platform connecting freelancers to clients locally in Canada 
               <b></h2>
            </div>
            <br>
            {% if user is empty %}
            <div class="text-center container">
                <a class="btn btn-lg btn-success" style="font-size: 30px; padding:10px 20px 10px 20px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;" href="{{ url('#signup') }}"><strong>Sign Up for Free</strong></a>
            </div>
            {% endif %}
        </div>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="active item gallery" style="background: linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4) ), url('/img/optimized/fitness'); background-position: 50% 50%; background-repeat: no-repeat; background-size: cover; min-height: 700px;"></div>
            <div class="item gallery" style="background: linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4) ), url('/img/optimized/cleaner'); background-position: 50% 50%; background-repeat: no-repeat; background-size: cover; min-height: 700px;"></div>
            <div class="item gallery" style="background: linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4) ), url('/img/optimized/photographer'); background-position: 50% 50%; background-repeat: no-repeat; background-size: cover; min-height: 700px;"></div>
            <div class="item gallery" style="background: linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4) ), url('/img/optimized/music'); background-position: 50% 50%; background-repeat: no-repeat; background-size: cover; min-height: 700px;"></div>
            <div class="item gallery" style="background: linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4) ), url('/img/optimized/catering'); background-position: 50% 50%; background-repeat: no-repeat; background-size: cover; min-height: 700px;"></div>
        </div>
        <!-- END Wrapper for slides -->

        {# <!-- Controls -->
        <a class="left carousel-control no-hover" href="#example-carousel4" data-slide="prev">
            <span><i class="fa fa-chevron-left"></i></span>
        </a>
        <a class="right carousel-control no-hover" href="#example-carousel4" data-slide="next">
            <span><i class="fa fa-chevron-right"></i></span>
        </a>
        <!-- END Controls --> #}
    </div>
    <!-- END Gallery Carousel Content -->
</section>
<!-- END Gallery Carousel Block -->

<section class="site-content site-section site-slide-content" style="padding: 30px">
    <div class="container">
        <!-- section title -->
        <div class="title" style="margin-bottom: 60px; text-align: center;">
        {#<h1><strong>What is QuikWit?<strong></h1>
        <hr style="border: none; background-color: #1b92ba; width: 80px; height: 3px;">#}
    </div>
    <div class="row text-center" style="margin-bottom: 80px; font-size: 20px;">
        If you are a freelancer, <a href="/#signup">get gigs</a> today or check out our <a href="/browse">new posts</a> from clients. If you are looking a freelancer, <a href="/post">post a Gig</a>.
    </div>
</section>

<!-- Section : Category -->
<section id="categories" class="site-content site-section site-slide-content" style="padding-top: 0px;">
    <div class="container">
        <!-- section title -->
        <div class="title" style="margin-bottom: 40px; text-align: center;">
            <h2><strong>Popular Categories<strong></h2>
            <hr style="border: none; background-color: #1b92ba; width: 80px; height: 3px;">
        </div>
        <!-- section title end -->
        <div class="row">
            <a href="/categories/#indoor-installations-and-remodels"><div class="btn service-item col-xs-12 col-sm-3" style="margin-bottom: 0px;">
                <img src="{{ url('/img/optimized/home') }}" style="width: 250px;">
                <h4><strong>Home</strong></h4>                              
            </div></a>

            <a href="/categories/#sports-&-fitness"><div class="btn service-item col-xs-12 col-sm-3" style="margin-bottom: 0px;">
                <img src="{{ url('/img/optimized/boxing') }}" style="width: 250px;">
                <h4><strong>Lessons</strong></h4>                          
            </div></a>

            <a href="/categories/#music"><div class="btn service-item col-xs-12 col-sm-3" style="margin-bottom: 0px;">
                <img src="{{ url('/img/optimized/dj') }}" style="width: 250px; height: 167px;">
                <h4><strong>Events</strong></h4>                 
            </div></a>

            <a href="/categories/#wellness"><div class="btn service-item col-xs-12 col-sm-3" style="margin-bottom: 0px;">
                <img src="{{ url('/img/optimized/chiropractor') }}" style="width: 250px; height: 167px;">
                <h4><strong>Wellness</strong></h4>                
            </div></a> 
        </div>
        <div class="text-center" style="margin: 50px;">
            <a href="/categories"><button class="btn btn-md btn-info">See more Categories</button></a>
        </div>
    </div>
</section>
<!-- Section : Category end --> 

<!-- Promo #3 -->
{#<section class="site-section site-section-light site-section-top" style="background: white;">
    <div class="container">
        <div class="text-center" style="padding-bottom: 20px">
            <h3 style="color: #333"><b>Use QuikWit to offer your services locally for FREE</b></h3>
            <br>
            <a class="btn btn-lg btn-success text-center" style="font-size: 32px; padding:10px 20px 10px 20px;" href="{{ url('become-a-pro') }}"><strong>Get Listed</strong></a>
        </div>
    </div>
</section>#}
<!-- END Promo #3 -->

<section id='requests' class="site-section">
    <div class="container">
        <!-- section title -->
        <div class="title" style="margin-bottom: 60px; text-align: center;">
        <h2><strong>Check out some of our recent posts from clients<strong></h2>
        <hr style="border: none; background-color: #1b92ba; width: 80px; height: 3px;">
    </div>
    <div class="container slide-widget" id="post-widgets">
        <?php if(count($requests) > 0): ?>
        <?php foreach($requests as $idx => $request): ?>
            <div class="col-md-6">
            <!-- START widget-->
            <div class="widget widget-hover-effect1" data-id="<?php echo $request['req_id'];?>">
                <div class="widget-simple themed-background">
                    {# <div class="col-sm-2 hidden-xs" style="padding: 0px;">
                        {% set profile = "user/profile/" ~ request['req_userid'] %}
                        {% if request['req_userid'] != 0 %}
                        <a href="{{ url(profile) }}" target="_blank">
                        {% endif %}
                        {% if request['user_avatar'] is defined %}
                        <img src="{{ url("uploads/avatar/" ~ request['user_avatar']) }}" alt="avatar" class="widget-image pull-left">
                        {% else %} 
                        <img src="/img/user/blank-avatar" style="width: 64px; max-width: 100%" alt="avatar" class="widget-image pull-left">
                        {% endif %}
                        {% if request['req_userid'] != 0 %}
                        </a>
                        {% endif %}
                    </div> #}
                    <div class="col-sm-8 col-xs-12" style="padding: 0px; height: 60px;">
                        <h5 class="widget-content-light">
                            {# <strong><?php if(!empty($request['user_firstname']) && !empty($request['user_lastname'])) {
                                                echo ucwords($request['user_firstname']." ".$request['user_lastname']);
                                             }else{
                                                echo 'Posted by an unregistered user';
                                             }
                                        ?></strong> #}

                        </h5>  
                        <h4 class="widget-content widget-content-light">
                            <strong>{{ request['req_title'] }}</strong>
                        </h4>
                    </div> 
                    <div class="col-sm-4 col-xs-12" style="padding: 0px;">
                        <a class="btn btn-m pull-right" style="border: solid 2px white; background: #1bbae1; color: white;" href="{% if loginUser is defined and request['req_userid'] == userId %} {{ url('post/viewPosting/' ~ request['req_id']) }} {% elseif loginUser is defined %} {{ url('browse/viewDetail/' ~ request['req_id']) }} {% endif %}" {% if loginUser is empty %}data-toggle="modal" data-target="#register" {% else %} target="_blank" {% endif %}><h5 style="color: white;"><b>Contact Details</b></h5></a>
                    </div>       
                </div>
                <div class="widget-extra">
                    <div class="row text-center" style="color: #1b92ba;">
                        <div class="col-xs-4">
                            <h5>
                                <strong>{{request['tag']}}</strong><br>
                            </h5>
                        </div>
                        <div class="col-xs-4">
                            <h5>
                                <strong>
                                    <?php   if ($request['req_fixed'] == 0 && $request['req_price'] != 0) {
                                                echo '$' .  number_format($request['req_price'], 2) . ' <p class="text-muted">(per hour)</p>'; 
                                            }elseif ($request['req_fixed'] == 1 && $request['req_price'] != 0) {
                                                echo '$' .  number_format($request['req_price'], 2) . ' <p class="text-muted">(flate rate)</p>'; 
                                            }elseif ($request['req_fixed'] == 0 && $request['req_price'] == 0) {
                                                echo 'Budget not specified <p class="text-muted">(per hour)</p>';
                                            }elseif ($request['req_fixed'] == 1 && $request['req_price'] == 0) {
                                                echo 'Budget not specified <p class="text-muted">(flat rate)</p>';
                                            }
                                    ?>
                                </strong><br>
                            </h5>
                        </div>
                        <div class="col-xs-4">
                            <h5>
                                <strong>{{ request['city'] }}</strong><br>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END widget-->
            </div>
        <?php endforeach; ?> 
        <?php endif; ?>    
    </div>         
    <div class="container text-center">
        <a href="{{ url('browse') }}" class="btn btn-lg btn-success">See More Gigs</a>
    </div>

</section>

<!-- Section : Service -->
<section id="what" class="site-content site-section">

    <div class="container text-center" style="font-size:16px">
        <div class="row row-items"> 
            <div class="col-sm-4 ">
                {# <span class="icon" style="color:#1b92ba;">
                    <i class="fa fa-search"></i>
                </span> #}

                <div class="circle themed-background-flatie animation-fadeIn360" data-toggle="animation-appear" data-animation-class="animation-fadeIn360" data-element-offset="-100" style="margin-bottom: 25px;">
                <i class="gi gi-eye_open"></i>
                </div>
            
                    <h4 class="site-heading" style="font-size:20px"><b>Get Noticed</b></h4>
                    <p class="text-left remove-margin">Potential clients get to learn about you through your profile and reviews when deciding to hire a freelancer locally.</p>
                             
            </div>

         <div class="col-sm-4">
                {# <span class="icon" style="color:#1b92ba;">
                    <i class="fa fa-search"></i>
                </span> #}

                <div class="circle themed-background animation-fadeIn360" data-toggle="animation-appear" data-animation-class="animation-fadeIn360" data-element-offset="-100" style="margin-bottom: 25px;">
                <i class="gi gi-message_plus"></i>
                </div>
            
                    <h4 class="site-heading " style="font-size:20px"><b>Get Offers<b></h4>
                    <p class="text-left remove-margin">Our QuikPost system alerts you any time a client in your city posts a gig matching your service.</p>
                             
            </div>
            <div class="col-sm-4 ">
                {# <span class="icon" style="color:#1b92ba;">
                    <i class="fa fa-search"></i>
                </span> #}

                <div class="circle themed-background-spring animation-fadeIn360" data-toggle="animation-appear" data-animation-class="animation-fadeIn360" data-element-offset="-100" style="margin-bottom: 25px;">
                <i class="gi gi-settings"></i>
                </div>
            
                    <h4 class="site-heading" style="font-size:20px"><b>Get Contracts</b></h4>
                    <p class="text-left remove-margin">Manage your contracts and communications in one place and reschedule and track changes with ease.</p>
                             
            </div>
            {#
            <div class="col-xs-12 col-md-3 col-sm-6 text-center">
                    {# <span class="icon" style="color:#1b92ba;">
                        <i class="fa fa-comments"></i>
                    </span> #
                    <img src="/img/landing/eye" style="margin-bottom: 35px; margin-top: 7px;">
                    <div class="text-center">
                        <h4><strong>User Profiles</strong></h4>
                        <p>To allow you to find a great fit, Teachers put up profiles with past work experiences and reviews from past clients.</p>  
                    </div>             
             </div>

            <div class="col-xs-12 col-md-3 col-sm-6 text-center">
                {# <span class="icon" style="color:#1b92ba;">
                    <i class="fa fa-tasks"></i>
                </span> #
                <img src="/img/landing/settings" style="margin-bottom: 30px;">
                <div class="text-center">
                    <h4><strong>Manage Your Lessons</strong></h4>
                    <p>Manage all of your lessons and communication in one place and reschedule and track your sessions with ease.</p>     
                </div>        
            </div>

            <div class="col-xs-12 col-md-3 col-sm-6 text-center">
                {# <span class="icon" style="color:#1b92ba;">
                    <i class="fa fa-dollar"></i>
                </span> #
                <img src="/img/landing/wallet" style="margin-bottom: 30px; margin-top: 5px;">
                <div class="text-center">
                    <h4><strong>Make Quick Bucks</strong></h4>
                    <p>You’re the sensei of your passion and your expertise is enviable. Start making money now while doing what you love.</p>
                </div>              
            </div>
                    #} 

        </div>
        <hr> 
    </div>
</section>
<!-- Section : Service end --> 

<!-- Promo #3 -->
{#<section class="site-section">
    <!-- section title -->
    <div class="title" style="margin-bottom: 60px; text-align: center;">
        <h3><strong>You can start offering some of these services in your neighborhood<strong></h3>
        <hr style="border: none; background-color: #1b92ba; width: 80px; height: 3px;">
    </div>
    <!-- section title end -->

    <div class="container">
        <div>
            {% if tagList is defined %}
            {{ tagList }}
            {% endif %}
        </div>
    </div>
</section>
<!-- END Promo #3 -->

<!-- Promo #2 -->
{<section class="site-content site-section site-slide-content">
    <div class="container">
        <!-- section title -->
        <div class="title" style="margin-bottom: 40px; text-align: center;">
            <h1 ><strong>How QuikWit Works<strong></h1>
            <hr style="border: none; background-color: #1b92ba; width: 80px; height: 3px;">
        </div>
        <!-- section title end -->
        <div class="row">
            <div class="col-sm-4 col-xs-12 visibility-none pull-right" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <img src="/img/optimized/HQW1" alt="Promo #1" class="img-responsive text-center" style="width: 220px; margin: 20px;">
            </div>
            <div class="col-sm-6 col-sm-offset-1 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <h1 class="h2 site-heading site-heading-promo" style="color: #4C4E50"><strong>Tell us what you need</strong></h1>
                <p class="promo-content">Fill out a post form with the details of the service you want provided and we will alert freelancers in your area matching your post specifications.</p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <img src="/img/optimized/HQW2" alt="Promo #2" class="img-responsive text-center" style="width: 230px; margin: 20px;">
            </div>
            <div class="col-sm-6 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <h1 class="h2 site-heading site-heading-promo" style="color: #4C4E50"><strong>Get quotes</strong></h1>
                <p class="promo-content">Qualified freelancers will get back to you with their estimated price and amount of time needed to complete your project.</p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-4 col-xs-12 visibility-none pull-right" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <img src="/img/optimized/HQW3" alt="Promo #3" class="img-responsive" style="width: 230px; margin: 20px;">
            </div>
            <div class="col-sm-6 col-sm-offset-1 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <h1 class="h2 site-heading site-heading-promo" style="color: #4C4E50"><strong>Compare options</strong></h1>
                <p class="promo-content">View the profiles of interested freelancers to learn about their past projects and to read reviews from past clients. Compare quotes from freelancers and their offerings to find the best value.</p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <img src="/img/optimized/HQW4" alt="Promo #4" class="img-responsive" style="width: 230px; margin: 20px;">
            </div>
            <div class="col-sm-6 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <h1 class="h2 site-heading site-heading-promo" style="color: #4C4E50;"><strong>Choose the right fit</strong></h1>
                <p class="promo-content">Choose a freelancer matching your budget, schedule and style. Finalize the details of your project and create a contract with the freelancer you choose.</p>
            </div>
        </div>
        <hr>
        <div class="row" style="padding-bottom: 40px;">
            <div class="col-sm-4 col-xs-12 visibility-none pull-right" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180" style="padding-bottom: 40px">
                <img src="/img/optimized/HQW5" alt="Promo #5" class="img-responsive" style="width: 230px; margin: 20px;">
            </div>
            <div class="col-sm-6 col-sm-offset-1 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <h1 class="h2 site-heading site-heading-promo" style="color: #4C4E50"><strong>You’re done!</strong></h1>
                <p class="promo-content">We care what you think so please share your thoughts with us by reviewing freelancers and providing feedbacks. Reviews help freelancers improve and others to find the right fit. </p>
            </div>
        </div> 
    </div>
</section>#}
<section class="site-content site-section site-slide-content" id="become-a-pro">
    <div class="container">
        <!-- section title -->

        <!-- section title end -->
        <div class="row" style="margin: 0 0 20px 0;">
            <div class="col-sm-6 col-md-3 col-md-offset-2 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <img src="/img/optimized/womantyping" alt="Promo #1" class="img-responsive" style="margin: 40px 0 20px 0; width: 220px;">
            </div>
            <div class="col-sm-6 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <h1 class="h2 site-heading site-heading-promo" style="color: #4C4E50"><strong>Create a profile</strong></h1>
                <p class="promo-content">Fill out your profile with the services you offer, location, work experiences, photos, videos of past projects and any information that helps clients learn more about you.</p>
            </div>
        </div>
        <hr>
        <div class="row" style="margin: 20px 0 20px 0;">
            <div class="col-xs-12 col-sm-6 col-md-4 visibility-none pull-right" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <img src="/img/optimized/manwithphone" alt="Promo #2" class="img-responsive" style="margin: 40px 0 20px 0; width: 220px;">
            </div>
            <div class="col-sm-6 col-md-6 col-md-offset-2 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <h1 class="h2 site-heading site-heading-promo" style="color: #4C4E50"><strong>View gigs</strong></h1>
                <p class="promo-content">When clients create posts matching your services, city and other specifications, you will receive a message in your inbox informing you of the new gig.</p>
            </div>
        </div>
        <hr>
        <div class="row" style="margin: 20px 0 20px 0;">
            <div class="col-sm-6 col-md-3 col-md-offset-2 visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <img src="/img/optimized/potentialcontact" alt="Promo #3" class="img-responsive" style="margin: 40px 0 20px 0; width: 220px;">
            </div>
            <div class="col-sm-6 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <h1 class="h2 site-heading site-heading-promo" style="color: #4C4E50;"><strong>Contact clients</strong></h1>
                <p class="promo-content">If you see a gig you are interested in, contact the client for details and give the client your pitch.</p>
            </div>
        </div>
        <hr>
        <div class="row" style="margin: 20px 0 20px 0;">
            <div class="col-xs-12 col-sm-6 col-md-4 visibility-none pull-right" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <img src="/img/optimized/completedeal" alt="Promo #4" class="img-responsive" style="margin: 40px 0 20px 0; width: 220px;">
            </div>
            <div class="col-sm-6 col-md-6 col-md-offset-2 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <h1 class="h2 site-heading site-heading-promo" style="color: #4C4E50"><strong>Create a contract</strong></h1>
                <p class="promo-content">If a client decides to work with you, finalize the details of the gig and create a contract using the contract system on the website. Remember to ask your clients to leave a review on your profile when the project is done.</p>
            </div>
        </div>
        <hr>
        <div class="row site-section" style="margin-top: 20px;">
            <div class="container">
                <div class="text-center">
                    <h3 style="color: #333"><b>To find out more about pricing, click <a href="{{ url('pricing') }}">here</a></b></h3>
                    <br>
                </div>
            </div>
        </div>
    </div>
</section>

{% if user is empty %}
<!-- Sign Up -->
<section class="site-content site-section" id="signup" style="background: linear-gradient( rgba(255, 255, 255, 0.65), rgba(255, 255, 255, 0.65) ), url('img/optimized/meeting'); background-position: 50% 50%; background-repeat: no-repeat; background-size: cover;">

    <div class="container">
        <!-- section title -->
        <div class="title" style="margin-bottom: 40px; text-align: center;">
            <h1><strong>Sign up<strong></h1>
            <hr style="border: none; background-color: #1b92ba; width: 80px; height: 3px;">
        </div>
        <!-- section title end -->
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 site-block">
                <!-- Sign Up Form -->
                {{ form("register/doRegister", "id" : "form-validation", "method" : "post", "class" : "form-horizontal") }}
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                {{ text_field('firstname', "placeholder" : "First Name", "class" : "form-control input-lg") }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                {{ text_field('lastname', "id" : "rform-lastname", "placeholder" : "Last Name", "class" : "form-control input-lg") }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                {{ email_field('email', "id" : "rform-email", "placeholder" : "Email", "class" : "form-control input-lg") }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-lock"></i></span>
                                {{ password_field('password', "id" : "rform-password", "placeholder" : "Password", "class" : "form-control input-lg") }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-lock"></i></span>
                                {{ password_field('rpassword', "id" : "rform-repassword", "placeholder" : "Retype Password", "class" : "form-control input-lg") }}
                                
                            </div>
                        </div>
                    </div>
                    </br>
                    <div class="form-group form-actions">
                        <div class="checkbox c-checkbox">
                             <label class="text-center">  
                                {{ check_field("confirmation", "id" : "rform-confirmation") }}
                                <span class="fa fa-check"></span>
                                <strong class="text-right"> Yes, I understand and agree to the QuikWit <a href="/terms" target="_blank" class="register-terms"> Terms of Use</a> and <a href="/terms/privacy" target="_blank" class="register-terms">Privacy Policy</a></strong>
                            </label>         
                        </div>
                        <br>
                        <div class="col-xs-12 text-center">
                            <button type="submit" class="btn btn-lg btn-primary" id="signUpButton"><strong>Sign Up</strong></button>
                        </div>
                    </div>
                </div>
                    {# <input id="rform-csrf" type="hidden" name="<?php echo $this->security->getTokenKey() ?>" value="<?php echo $this->security->getTokenKey() ?>"/> 
                    <input id="rform-csrf" type="hidden" name="{{ tokenKey }}" value="{{ token }}"/> #}
                    {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }} 
                {{ end_form() }}
                <!-- END Sign Up Form -->
            </div>
        </div>
    </div>
</section>
<!-- END Sign Up -->
{% endif %}
{% endblock %}

{% block modal %}
<!-- START modal-->
<div id="register" tabindex="-1" role="dialog"
    aria-labelledby="reportLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="reportLabel" class="modal-title">New to our site? Please register below to continue.</h4>
            </div>
            <div class="modal-body">
              <!-- Sign Up Form -->
              {{ form("register/doRegister", "id" : "form-validation-2", "method" : "post", "class" : "form-horizontal") }}
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-user"></i></span>
                              {{ text_field('firstname', "placeholder" : "First Name", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-user"></i></span>
                              {{ text_field('lastname', "id" : "rform-lastname", "placeholder" : "Last Name", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                              {{ email_field('email', "id" : "rform-email", "placeholder" : "Email", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-lock"></i></span>
                              {{ password_field('password', "id" : "rform-password", "placeholder" : "Password", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-lock"></i></span>
                              {{ password_field('rpassword', "id" : "rform-repassword", "placeholder" : "Retype Password", "class" : "form-control input-lg") }}
                              
                          </div>
                      </div>
                  </div>
                  </br>
                  <div class="form-group form-actions">
                      <div class="checkbox c-checkbox">
                           <label class="text-center">  
                              {{ check_field("confirmation", "id" : "rform-confirmation") }}
                              <span class="fa fa-check"></span>
                              <strong class="text-right"> Yes, I understand and agree to the QuikWit <a href="/terms" target="_blank" class="register-terms"> Terms of Use</a> and <a href="/terms/privacy" target="_blank" class="register-terms">Privacy Policy</a></strong>
                          </label>         
                      </div>
                      <br>
                      <div class="col-xs-12 text-center">
                          <button type="submit" class="btn btn-lg btn-primary" id="signUpButton"><strong>Sign Up</strong></button>
                      </div>
                  </div>
                  {# <input id="rform-csrf" type="hidden" name="<?php echo $this->security->getTokenKey() ?>" value="<?php echo $this->security->getTokenKey() ?>"/> 
                  <input id="rform-csrf" type="hidden" name="{{ tokenKey }}" value="{{ token }}"/> #}
                  {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }} 
              {{ end_form() }}
              <!-- END Sign Up Form -->
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
{% endblock %}

{% block script %}
{{ javascript_include('third-party/slick-1.5.9/slick/slick.min.js') }}

<script type="text/javascript">
$(document).ready(function() {
    var mCarouselTO = setTimeout(function(){
        $('.carousel').carousel({
            interval: 3000,
            cycle: true,
        }).trigger('slide');
        }, 2000);
        var q = mCarouselTO;

    $('.slide-widget').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
    });

}); 

$(function(){ FormsValidation.init(); });

$(window).scroll(function () {
    var height = $('#headerPic').height() - 10;
    var y_scroll_pos = window.pageYOffset;
    if(y_scroll_pos > height) {
        $("#navbar").css('border-bottom', "solid 1px #1BBAE1");
        $("#navbar").css('background-color', "rgba(255,255,255,.98)");
        $('#logo-md').attr('src', '/img/landing/QW10.png');
        $('#logo-sm').attr('src', '/img/landing/QW2.png');
        $('#logo-xs').attr('src', '/img/landing/QW2.png');
        $('#postLink').css('border', 'solid 1px #1b92ba');
        $('.site-nav-link').css('color', '#1b92ba');
    }else{
        $("#navbar").css('border-bottom', "");
        $("#navbar").css('background-color', "rgba(255,255,255,0)");
        $('#logo-md').attr('src', '/img/optimized/QW7.png');
        $('#logo-sm').attr('src', '/img/optimized/QW8.png');
        $('#logo-xs').attr('src', '/img/optimized/QW8.png');
        $('#postLink').css('border', 'solid 1px #FFFFFF');
        $('.site-nav-link').css('color', '#FFFFFF');
    }

});
</script>

<script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "url": "https://www.quikwit.co",
      "logo": "https://www.quikwit.co/img/favicon/favicon-96x96.png",
      "sameAs" : [
          "https://www.facebook.com/quikwit",
          "https://twitter.com/quikwithq"
        ]
    }
</script>

{% endblock %}



