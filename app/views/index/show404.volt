{% extends "templates/home.volt" %}

{% block title %}
Error | QuikWit
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top">
	<div class="error-options col-sm-offset-1 col-sm-8 col-xs-10">
	    <h3><i class="fa fa-chevron-circle-left text-muted"></i> <a href="javascript:void(0);" onclick="window.history.go(-1)">Go Back</a></h3>
	</div>
</section>
<!-- END Intro -->

<!-- Error Container -->
<div id="error-container">
    
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 text-center">
            <h1 class="animation-pulse" style="color: #1b92ba;"><i class="fa fa-exclamation-circle text-warning"></i> 404</h1>
            <h2 class="h3" style="color: #1b92ba;">*crack* Nut is too hard and broke our teeth.<br>But do not worry, we will take a crack at it again soon...</h2>
        </div>
    </div>
</div>
<!-- END Error Container -->
{% endblock %}