{% extends "templates/home.volt" %}

{% block head %}
<style>
.clickable-row:hover{
    background: #E7E7E7 !important; 
}
@media screen and (max-width: 768px) {
    .text-align {
        text-align: left;

    }
}
@media screen and (min-width: 769px) {
    .text-align {
        text-align: center;

    }
}
</style>
{% endblock %}

{% block title %}
Conversations | QuikWit
{% endblock %}

{% block content %}
<section class="main-content">
    <!-- Inbox Content -->
    <div class="row">
        <!-- Inbox Menu -->
        <div class="col-md-4 col-lg-3">
            {{ partial("partials/message/left-menu") }}
        </div>
        <!-- Messages List -->
        <div class="col-md-8 col-lg-9">
            <!-- Messages List Block -->
            <div class="block">
                <!-- Messages List Title -->
                <div class="block-title">
                    <h2>Conversations <strong>({{ totalMessageCount }})</strong></h2>
                    {# <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i></a>
                    </div> #}
                </div>
                <!-- END Messages List Title -->

                <!-- Messages List Content -->
                <div class="row">
                    <div class="text-right col-sm-12" style="margin-bottom: 20px;">
                        <strong>{{ messageOffset }} - {{ messageCountPerPage }}</strong> from <strong>{{ totalMessageCount }}</strong>
                        <div class="btn-group btn-group-sm">
                            <a href="message?page={{prevPage}}" class="btn btn-sm btn-default"><i class="fa fa-angle-left"></i></a>
                            <a href="message?page={{nextPage}}" class="btn btn-sm btn-default"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    <!-- Use the first row as a prototype for your column widths -->
                    {% if messages is defined %}
                        {% for message in messages %}
                        {% if message['message_count'] > 0 %} 
                        <div class="clickable-row col-xs-12" data-href="{{ url('message/viewMessage/' ~ message['ref_id']) }}" style="padding: 20px; border-bottom: 1px solid #D1D1D1; cursor: pointer;">
                            {# <td class="text-center" style="width: 30px;">
                                <input type="checkbox"/>
                            </td> #}
                        {% else %} 
                        <div class="clickable-row col-xs-12" data-href="{{ url('message/viewMessage/' ~ message['ref_id']) }}" style="padding: 20px; border-bottom: 1px solid #D1D1D1; cursor: pointer; background: #F1F1F1;">
                        {% endif %} 

                            <div class="col-xs-1 text-align" >
                                {% if message['message_count'] > 0 %} 
                                    <span style="padding-top: 10px; margin-left: -20px; color:#52D23A" class="gi gi-message_plus gi-1.4x"></span>                      
                                {% endif %}
                            </div>
                            <div class="col-sm-2 col-xs-8 text-align">
                                {% if message['user_avatar'] is defined %}
                                <img src="{{ url("uploads/avatar/" ~ message['user_avatar']) }}" alt="{{ message['user_firstname']|capitalize ~ ' ' ~ message['user_lastname']|capitalize }}" class="img-thumbnail hidden-xs" style="max-width: 60px;">
                                {% else %}
                                <img src="/img/user/blank-avatar" alt="{{ message['user_firstname']|capitalize ~ ' ' ~ message['user_lastname']|capitalize }}" class="img-thumbnail hidden-xs" style="max-width: 60px;">
                                {% endif %}

                                {% if message['message_count'] > 0 %} 
                                    <div style="font-size: 16px; color: #1b92ba;">{{ message['user_firstname'] }} {{ message['user_lastname'] }}</div>
                                {% else %}
                                    <div style="font-size: 16px;">{{ message['user_firstname'] }} {{ message['user_lastname'] }}</div>
                                {% endif %}

                                <span class="text-muted hidden-sm hidden-md hidden-lg text-align" style="color: #808080"><div style="font-size: 16px;">{{ message['title'] }}</div><em><?php echo Component\PrettyPrint::excerpt($message['body']) . ' ...'; ?></em> {% if message['message_count'] > 0 %} ({{ message['message_count'] }}){% endif %}</span>        
                            </div>
                            <div class="col-sm-3 hidden-xs text-align" >
                                <div style="font-size: 16px; padding-top: 10px">{{ message['title'] }}</div>
                            </div>
                            <div class="col-sm-3 hidden-xs">
                                <span class="text-muted" style="color: #808080"><div style="padding-top: 10px"><?php echo Component\PrettyPrint::excerpt($message['body']) . ' ...'; ?> {% if message['message_count'] > 0 %} ({{ message['message_count'] }}){% endif %}</div></span>
                            </div>
                            <div class="col-sm-2 text-right pull-right" style="padding: 0px 0px 0px 0px; ">{#<em><?php echo Component\PrettyPrint::prettyPrint($message['created']); ?></em>#}
                                <?php   if ($timezone) {
                                            $userTimezone = new DateTimeZone($timezone); 
                                            $newDate = new DateTime();
                                            $newDate->setTimestamp($message['created']);
                                            $newDate->setTimeZone($userTimezone);
                                            echo $newDate->format('M jS'); 
                                        }else{
                                            echo date('M jS', $message['created']) . " - UTC";
                                        }

                                        ?>
                            </div>
                        </div>
                        {% endfor %}
                    {% endif %}
                </div>
                <!-- END Messages List Content -->
            </div>
            <!-- END Messages List Block -->
        </div>
        <!-- END Messages List -->
    </div>
    <!-- END Inbox Content -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

</section>

{% endblock %}

{% block modal %}
{% endblock %}

{% block footer %}
{% endblock %}


{% block script %}
<!-- Load and execute javascript code used only in this page -->
{{ javascript_include("third-party/proui/backend/js/pages/readyInbox.js") }}
<script>$(function(){ ReadyInbox.init(); });</script>
{{ javascript_include("third-party/proui/backend/js/pages/readyChat.js") }}
<script>$(function(){ ReadyChat.init(); });</script>

<script>
$(document).ready(function(){

    $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });

    function updateMessageCount(){
        $.getJSON("/message/count", function(data){
            if(data.success){
                var el = $("#message-count");
                if(data.content.count!==el.data("count")){
                    el.data("get", true);
                    el.html("&nbsp;("+data.content.count+" unread messages)").data("count", data.content.count);
                }else{
                    el.data("get", false);
                }
            };
            //  console.log(data);
        });
    };

    function retrieveMessages(){
        var offset = $("#message-container").children("p").size();
        var count = $("#message-count").data("count") || 0;
        $.getJSON("/message/get", {offset: offset, count: count}, function(data){
            if(data.success){
                var msgs = data.content.messages;
                $.each(msgs, function(idx, msg){
                    var el = $("<p>"+msg.firstname+" "+msg.lastname+" - "+msg.title+"<br/>"+msg.body+"</p>");
                    el.data("id", msg.id);
                    $("#message-container").append(el);
                });
                //data.content.messages); 
            }
            console.log(data);
        });
    };

    //updateMessageCount();
    //retrieveMessages();
    
    /*window.setInterval(function(){
        updateMessageCount();
        if($("#message-count").data("get")==true) retrieveMessages();
    }, 5000); */
});
</script>
{% endblock %}