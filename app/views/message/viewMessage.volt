{% extends "templates/home.volt" %}

{% block head %}
{% endblock %}

{% block title %}
Conversations | QuikWit
{% endblock %}

{% block content %}
<section class="main-content">
    <!-- Inbox Content -->
    <div class="row">
        <!-- Inbox Menu -->
        <div class="col-sm-4 col-lg-3">            
            {{ partial("partials/message/left-menu") }}
        </div>
        <!-- Messages List -->
        <div class="col-sm-8 col-lg-9">
            <!-- View Message Block -->
            <div class="block full">
                <!-- View Message Title -->
                <div class="block-title">
                    <div class="block-options pull-right" style="margin-right: 0px;">  
                        {% if messageRequestId is not NULL and userId == requestUserId %}
                        <a href="{{ url('post/viewPosting/' ~ messageRequestId) }}" target="_blank" class="btn btn-success" style="padding: 0px; border-radius: 0px;"><h2><strong>Go to Request</strong> {# <small><span class="label label-info">Conversations</span></small> #} </h2></a>
                        {% elseif messageRequestId is not NULL and userId != requestUserId %}
                        <a href="{{ url('gig/viewRequest/' ~ messageRequestId) }}" target="_blank" class="btn btn-success" style="padding: 0px; border-radius: 0px;"><h2><strong>Go to Request</strong> {# <small><span class="label label-info">Conversations</span></small> #} </h2></a>
                        {% else %}
                        {% endif %}
                    </div>

                    <h2><strong>{{ messageTitle|capitalize }}</strong> {# <small><span class="label label-info">Conversations</span></small> #} </h2>

                </div>
                <!-- END View Message Title -->

                {% for message in messages %}
                <!-- Message Meta -->
                <table class="table table-borderless table-vcenter remove-margin">
                    <tbody>
                        <tr>
                            <td class="text-center" style="width: 200px;">
                                <a href="{{ url('user/profile/' ~ message['user_id']) }}" class="pull-left">
                                    {% if message['user_avatar'] is defined %}
                                    <img src="{{ url('uploads/avatar/' ~ message['user_avatar']) }}" alt="{{ message['user_firstname']|capitalize ~ ' ' ~ message['user_lastname']|capitalize }}" class="img-thumbnail" style="width: 60px;" />
                                    {% else %}
                                    <img src="/img/user/blank-avatar" alt="{{ message['user_firstname']|capitalize ~ ' ' ~ message['user_lastname']|capitalize }}" class="img-thumbnail" style="width: 60px;" />
                                    {% endif %}
                                </a>
                                <span><a href="{{ url('user/profile/' ~ message['user_id']) }}">{{ message['user_firstname']|capitalize ~ ' ' ~ message['user_lastname']|capitalize }}</a></span>
                            </td>
                            {# <td class="text-right"><strong>{{ date('F jS, Y - g:i a', message['created']) }}</strong></td> #}
                            <td class="text-right"><strong><?php    if ($timezone) {
                                                                        $userTimezone = new DateTimeZone($timezone); 
                                                                        $newDate = new DateTime();
                                                                        $newDate->setTimestamp($message['created']);
                                                                        $newDate->setTimeZone($userTimezone);
                                                                        echo $newDate->format('F jS, Y - g:i a'); 
                                                                    }else{
                                                                        echo date('F jS, Y - g:i a', $message['created']) . " UTC";
                                                                    }

                                                                    ?>
                                                                </strong></td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Message Meta -->

                <!-- Message Body -->
                
                <p style="margin-left: 10px;">
                    {{ message['body']|e }}
                </p>
                <hr>
                <!-- END Message Body -->
                
                {% endfor %}
                {% if messageRequestId is not NULL and userId == requestUserId %}
                       <p class="text-center" style="color: #27AE44; font-size: 14px;"><b>To assign a gig to a freelancer, click on their avatar or name to book them from their profile</b></p>
                {% else %}
                {% endif %}
                <!-- Quick Reply Form -->
                <form method="post" action="/message/send">
                    <textarea id="message-quick-reply" name="message[message]" rows="5" class="form-control push-bit" placeholder="Type your message here"></textarea>
                    <input name='message[reply_to]' type='hidden' value='{{ replyTo }}' />
                    <input name='message[uid]' type='hidden' value='{{ uid }}' />
                    <input type="hidden" name="{{ tokenKey }}" value="{{ token }}"/> 
                    <button class="btn btn-sm btn-primary">Quick Reply</button>
                </form>
                <!-- END Quick Reply Form -->

            </div>
            <!-- END View Message Block -->
        </div>
        <!-- END Messages List -->
    </div>
    <!-- END Inbox Content -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

</section>

{% endblock %}

{% block modal %}
{% endblock %}

{% block footer %}
{% endblock %}


{% block script %}
<!-- Load and execute javascript code used only in this page -->
<script src="/third-party/proui/backend/js/pages/readyInbox.js"></script>
<script>$(function(){ ReadyInbox.init(); });</script>

<script>
$(document).ready(function(){
    function updateMessageCount(){
        $.getJSON("/message/count", function(data){
            if(data.success){
                var el = $("#message-count");
                if(data.content.count!==el.data("count")){
                    el.data("get", true);
                    el.html("&nbsp;("+data.content.count+" unread messages)").data("count", data.content.count);
                }else{
                    el.data("get", false);
                }
            };
            //  console.log(data);
        });
    };

    function retrieveMessages(){
        var offset = $("#message-container").children("p").size();
        var count = $("#message-count").data("count") || 0;
        $.getJSON("/message/get", {offset: offset, count: count}, function(data){
            if(data.success){
                var msgs = data.content.messages;
                $.each(msgs, function(idx, msg){
                    var el = $("<p>"+msg.firstname+" "+msg.lastname+" - "+msg.title+"<br/>"+msg.body+"</p>");
                    el.data("id", msg.id);
                    $("#message-container").append(el);
                });
                //data.content.messages); 
            }
            console.log(data);
        });
    };

    updateMessageCount();
    retrieveMessages();
    
    window.setInterval(function(){
        updateMessageCount();
        if($("#message-count").data("get")==true) retrieveMessages();
    }, 10000);
});
</script>
{% endblock %}