{% extends "templates/home.volt" %}

{% block head %}
{% endblock %}

{% block title %}
Notifications | QuikWit
{% endblock %}

{% block content %}
<section class="main-content">
    <!-- Inbox Content -->
    <div class="row">
        <!-- Inbox Menu -->
        <div class="col-sm-4 col-lg-3">            
            {{ partial("partials/message/left-menu") }}
        </div>
        <!-- Messages List -->
        <div class="col-sm-8 col-lg-9">
            <!-- View Message Block -->
            <div class="block full">
                <!-- View Message Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                    </div>
                    <h2><strong>{{ notificationTitle|capitalize }}</strong></h2>
                </div>
                <!-- END View Message Title -->

                {% for notification in notifications %}
                <!-- Message Meta -->
                <table class="table table-borderless table-vcenter remove-margin">
                    <tbody>
                        <tr>
                            <td class="text-center" style="width: 200px;">
                                <div class="pull-left">
                                    <img src="/img/landing/QW2.png" alt="QuikWit Team" class="img-thumbnail" style="width: 50px;"/>
                                </div>
                                <span>QuikWit Team</span>
                            </td>
                            {# <td class="text-right"><strong>{{ date('F jS, Y - g:i a', notification['created']) }}</strong></td> #}
                            <td class="text-right"><strong><?php if ($timezone) {
                                                                 $userTimezone = new DateTimeZone($timezone); 
                                                                 $newDate = new DateTime();
                                                                 $newDate->setTimestamp($notification['created']);
                                                                 $newDate->setTimeZone($userTimezone);
                                                                 echo $newDate->format('F jS, Y - g:i a'); 

                                                                 }else{
                                                                     echo date('F jS, Y - g:i a', $notification['created']) . " UTC";
                                                                 }

                                                                 ?></strong></td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Message Meta -->

                <!-- Message Body -->
                <p>
                    <?php echo htmlspecialchars_decode($notification['body']); ?>
                </p>
                <!-- END Message Body -->
                <hr>
                {% endfor %}
            </div>
            <!-- END View Message Block -->
        </div>
        <!-- END Messages List -->
    </div>
    <!-- END Inbox Content -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

</section>

{% endblock %}

{% block modal %}
{% endblock %}

{% block footer %}
{% endblock %}


{% block script %}
<!-- Load and execute javascript code used only in this page -->
<script src="/third-party/proui/backend/js/pages/readyInbox.js"></script>
<script>$(function(){ ReadyInbox.init(); });</script>

<script>
$(document).ready(function(){
    function updateMessageCount(){
        $.getJSON("/message/count", function(data){
            if(data.success){
                var el = $("#message-count");
                if(data.content.count!==el.data("count")){
                    el.data("get", true);
                    el.html("&nbsp;("+data.content.count+" unread messages)").data("count", data.content.count);
                }else{
                    el.data("get", false);
                }
            };
            //  console.log(data);
        });
    };

    function retrieveMessages(){
        var offset = $("#message-container").children("p").size();
        var count = $("#message-count").data("count") || 0;
        $.getJSON("/message/get", {offset: offset, count: count}, function(data){
            if(data.success){
                var msgs = data.content.messages;
                $.each(msgs, function(idx, msg){
                    var el = $("<p>"+msg.firstname+" "+msg.lastname+" - "+msg.title+"<br/>"+msg.body+"</p>");
                    el.data("id", msg.id);
                    $("#message-container").append(el);
                });
                //data.content.messages); 
            }
            console.log(data);
        });
    };

    updateMessageCount();
    retrieveMessages();
    
    window.setInterval(function(){
        updateMessageCount();
        if($("#message-count").data("get")==true) retrieveMessages();
    }, 10000);
});
</script>
{% endblock %}