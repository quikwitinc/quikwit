{% extends "templates/home.volt" %}

{% block content %}
<?php $this->flashSession->output() ?>
<section class="main-content">
<h3>OAuth Token Management</h3>
<div class="table-responsive panel panel-default">
	<table class="table table-bordered">
	
	{% if accessToken is defined %}
		<tr>
			<td width="15%" class="text-center">Token Status</td>
			{% if tokenExpired %}
			<td class="danger">Expired</td>
			<td width="10%"><a class="btn btn-success" href="{{ url('o/ytAuth?callback=') }}{{ callback }}">Renew</a></td>
			{% else %}
			<td class="success">Active</td>
			<td width="10%"><a class="btn btn-danger" href="{{ url('o/ytRevokeToken') }}">Revoke</a></td>
			{% endif %}
		</tr>

		<tr>
			<td width="15%" class="text-center">Access Token</td>
			<td>{{ accessToken }}</td>
			<td width="10%"></td>
		</tr>
		<tr>
			<td width="15%" class="text-center">Token Type</td>
			<td>{{ tokenType }}</td>
			<td width="10%"></td>
		</tr>
		<tr>
			<td width="15%" class="text-center">Token Created</td>
			<td>{{ created }}</td>
			<td width="10%"></td>
		</tr>
		<tr>
			<td width="15%" class="text-center">Token Expires</td>
			<td>{{ expires }}</td>
			<td width="10%"></td>
		</tr>
		{% if refreshToken %}
		<tr>
			<td width="15%" class="text-center">Refresh Token</td>
			<td>{{ refreshToken }}</td>
			<td width="10%"><a class="btn btn-success" href="{{ url('o/ytRefreshToken') }}">Refresh</a></td>
		</tr>
		{% endif %}
	{% else %}
		<tr>
			<td width="15%" class="text-center">Token Status</td>
			<td class="warning">Not found!</td>
			<td width="10%"><a class="btn btn-success" href="{{ url('o/ytAuth?callback=') }}{{ callback }}">Authorize</a></td>
		</tr>
	{% endif %}
	</table>
</div>
</section>
{% endblock %}