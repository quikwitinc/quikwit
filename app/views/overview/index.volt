{% extends "templates/home.volt" %}

{% block title %}
Edit Your Profile | QuikWit
{% endblock %}

{% block head %}
<!-- Star Rating -->
<link rel="stylesheet" href="/third-party/star-rating/jquery.rating.css">
<link rel="stylesheet" href="/css/overview.css">
<!-- Jcrop -->
<link rel="stylesheet" href="/third-party/jcrop/css/jquery.Jcrop.min.css">
<style>
.your-special-css-class {
    width: 320px !important;
    height: 50%;
}
.btn-overview {
    padding: 2px;
}
/* @media screen and (max-width: 480px) {
    .avatar {
        margin-top:10px;
        margin-bottom:10px;
        max-height: 40vh;
        max-width: 40vh;
    }
    .cameraWidth {
        max-width: 350px;
    }
}
@media screen and (min-width: 481px) and (max-width: 992px) {
    .avatar {
        margin-top:10px;
        margin-bottom:10px;
        max-height: 70vh;
        max-width: 70vh;
    }
    .cameraWidth {
        max-width: 600px;
    }
}
@media screen and (min-width: 993px) {
    .avatar {
        margin-top:10px;
        margin-bottom:10px;
        max-height: 100vh;
        max-width: 100vh;
    }
    .cameraWidth {
        max-width: 900px;
    }
} */
</style>
{% endblock %}

{% block content %}
<section class="main-content">
    <!-- User Profile Content -->
    <div class="row">   

        <!-- First Column -->
        {% if userInfo.usr_teach %}
        <div class="col-md-5 col-lg-4 col-lg-offset-1">
        {% else %}
        <div class="col-md-8 col-md-offset-2">
        {% endif %}
            <!-- Info Block -->
            <div class="block">
                <!-- Info Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        {% if userInfo.usr_teach %}
                        <span data-toggle="tooltip" title="Add Services" id="addSkillsDiv">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#addSkill" class="btn btn-sm btn-alt btn-default btn-overview" ><i class="fa fa-pencil"></i></a>
                        </span>
                        {% endif %}
                        <span data-toggle="tooltip" title="Add Languages" id="addLanguagesDiv">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#addLanguage" class="btn btn-sm btn-alt btn-default btn-overview" ><i class="fa fa-language"></i></a>
                        </span>
                        {% if userInfo.usr_teach %}
                        <span data-toggle="tooltip" title="Add Hourly Rate" id="addRatesDiv">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#addRate" class="btn btn-sm btn-alt btn-default btn-overview" ><i class="fa fa-dollar"></i></a>
                        </span>
                        {% endif %}
                    </div>
                    <h2>My Info</h2>
                </div>
                <!-- END Info Title -->

                <!-- Info Content -->
                <div class="block-section text-center">
                    {% if userInfo.usr_avatar is defined %}
                    <img title="profile image" src="{{ url("uploads/avatar/" ~ userInfo.usr_avatar) }}" alt="Avatar" class="img-thumbnail" style="width: 175px;">
                    {% else %}
                    <img title="profile image" src="{{ url("img/user/blank-avatar") }}" alt="Avatar" class="img-thumbnail" style="width: 175px;">
                    {% endif %}
                    <h3>
                        <strong>{{ userInfo.usr_firstname|capitalize }}&nbsp;{{ userInfo.usr_lastname|capitalize }}</strong><br><small></small>
                    </h3>
                    {% if userInfo.usr_teach %}
                    <div id="overallRating" style="display: block; width: 160px; padding-left: 35px;" class="container">
                        <input class="star {split:2}" type="radio" name="overall" value="0.5" disabled="disabled"  <?php echo ($stats[0]['overall'] <=0.5  && $stats[0]['overall'] != null)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="overall" value="1.0" disabled="disabled" <?php echo ($stats[0]['overall'] <1.5 && $stats[0]['overall'] >= 1)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="overall" value="1.5" disabled="disabled" <?php echo ($stats[0]['overall'] <2 && $stats[0]['overall'] >=1.5 )?' checked="checked"':'';?> />
                        <input class="star {split:2}" type="radio" name="overall" value="2.0" disabled="disabled" <?php echo ($stats[0]['overall'] < 2.5  && $stats[0]['overall'] >= 2)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="overall" value="2.5" disabled="disabled"  <?php echo ($stats[0]['overall'] < 3  && $stats[0]['overall'] >= 2.5)?' checked="checked"':'';?>/> 

                        <input class="star {split:2}" type="radio" name="overall" value="3.0" disabled="disabled"  <?php echo ($stats[0]['overall'] <3.5  && $stats[0]['overall'] >= 3)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="overall" value="3.5" disabled="disabled" <?php echo ($stats[0]['overall'] <4 && $stats[0]['overall'] >= 3.5)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="overall" value="4.0" disabled="disabled" <?php echo ($stats[0]['overall'] <4.5 && $stats[0]['overall'] >= 4)?' checked="checked"':'';?> />
                        <input class="star {split:2}" type="radio" name="overall" value="4.5" disabled="disabled" <?php echo ($stats[0]['overall'] < 5  && $stats[0]['overall'] >= 4.5)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="overall" value="5.0" disabled="disabled"  <?php echo ($stats[0]['overall'] >=5)?' checked="checked"':'';?>/>
                        <br>
                    </div>
                    <?php echo $stats[0]['num_contract'];?> Review(s)
                    <br>
                    <br>
                    {% endif %}
                    {# <button class="btn btn-info" data-target="#coverPhoto" data-toggle="modal">Set Cover Photo</button> #}
                    <button id="setAvatar" class="btn btn-info" data-target="#camera" data-toggle="modal">Set Profile Picture</button>
                    {# <span data-toggle="tooltip" title="Edit Name">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#editName" class="btn btn btn-primary" style="size: 10%;">Change Name</a>
                    </span> #}
                </div> 
               
                <hr>
                <br>

                <table class="table table-borderless table-striped">
                    <tbody>
                        <tr>
                            <td style="width: 20%;"><strong>Joined</strong></td>
                            <td style="font-size:14px">{{ date('F jS, Y', userInfo.usr_created) }}</td>
                        </tr>
                        {# <tr>
                            <td><strong>Verification</strong></td>
                            <td>Not Verified</td>
                        </tr> #}
                        <tr>
                            <td><strong>Languages</strong></td>
                            <td>{{ userLangs }}</td>
                        </tr>
                        {% if userInfo.usr_teach %}
                        <tr>  
                            <td><strong>Service(s) offered</strong></td>               
                           
                            <td><a href="javascript:void(0)" class="label label-info">{{ userSkillTags }}</a></td>
                        </tr>
                        <tr>
                            <td><strong>Hourly Rate</strong></td>
                            <td><?php if($userInfo->usr_rate == 0) {
                                    echo 'Please contact me for rate.';
                                }else{
                                    echo '$' .  number_format($userInfo->usr_rate, 2);
                                } ?>
                            </td>
                        </tr>
                        {% endif %}
                        {# <tr>
                            <td><strong>Social Media</strong></td>
                            <td>{{connections}}</td>
                        </tr> #}
                    </tbody>
                </table>
                <!-- END Info Content -->
            </div>
            <!-- END Info Block -->

            <!-- Bio Block -->
            <div class="block">
                <!-- Bio Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#about" class="btn btn-alt btn-sm btn-default btn-overview" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                    </div>
                    <h2>About Me </h2>
                </div>
                <!-- END Bio Title -->
                {% if userInfo.usr_about is defined %}
                <div id="my-bio-content">{{ userInfo.usr_about }}</div>
                {% else %}
                <div id="my-bio-content">Please write a little about yourself.</div>
                {% endif %}
                {# <div id="my-bio-edit" class="clearfix">
                    <textarea class="form-control ckeditor" id="my-bio-edit-textarea" name="my-bio"></textarea>
                    <hr>
                    <button type="button" class="btn btn-sm pull-right" id="my-bio-close" style="margin-left:10px">Close</button>
                    <button type="button" class="btn btn-info btn-sm pull-right" id="my-bio-save">Save</button>
                </div> #}
            </div>
            <!-- END Bio Block -->

            {# {% if userInfo.usr_teach %}
            <!-- Availability Block -->
            <div class="block">
                <!-- Availability Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <span data-toggle="tooltip" title="Available">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#availability" class="btn btn-sm btn-alt btn-default" ><i class="fa fa-pencil"></i></a>
                        </span>
                    </div>
                    <h2>My Availability</h2>
                </div>
                <!-- END Availability Title -->

                <!-- Availability Content -->
                <table class="table table-borderless table-striped">
                    <tbody>
                        <tr>
                            <td style="width: 20%;"><strong>Monday</strong></td>
                            <td>
                                
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Availability Content -->
            </div>
            <!-- END Availability Block -->
            {% endif %} #}

            <!-- Location Block -->
            <div class="block">
                <!-- Location Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <span data-toggle="tooltip" title="Edit" id="myLocation">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#addPostal" class="btn btn-alt btn-sm btn-default btn-overview"><i class="fa fa-pencil"></i></a>
                        </span>
                    </div>
                    <h2>My Location</h2>
                </div>
                <!-- END Location Title -->

                <!-- Location Content -->
                <div class="block-content-full">
                    <div data-toggle="gmap" id="gmap"

                     class="gmap"></div>
                </div>
                <!-- END Location Content -->
            </div>
            <!-- END Location Block -->

        </div>
        <!-- END First Column -->

        {% if userInfo.usr_teach %}
        <!-- Second Column -->
        <div class="col-md-7 col-lg-6">

            <!-- Info Block -->
            <div class="block">
                <!-- Info Title -->
                <div class="block-title" id="myStats">
                    <h2>My Stats</h2>
                </div>
                <!-- END Info Title -->

                <!-- Info Content -->
                <table class="table table-borderless table-striped">
                    <tbody>
                        <tr>
                            <td style="width: 20%;"><strong>Punctuality</strong></td>
                            <td><input class="star" type="radio" name="punctuality" value="1" title="Worst" disabled="disabled" <?php echo ($stats[0]['puntuality'] <2 && $stats[0]['puntuality']!=null)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="punctuality" value="2" title="Bad" disabled="disabled" <?php echo ($stats[0]['puntuality'] <3 && $stats[0]['puntuality'] >= 2)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="punctuality" value="3" title="OK" disabled="disabled" <?php echo ($stats[0]['puntuality'] <4 && $stats[0]['puntuality'] >= 3)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="punctuality" value="4" title="Good" disabled="disabled" <?php echo ($stats[0]['puntuality'] < 5  && $stats[0]['puntuality'] >= 4)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="punctuality" value="5" title="Best" disabled="disabled" <?php echo ($stats[0]['puntuality'] >=5)?' checked="checked"':'';?>/></td>
                            <td class="hidden-xs"><strong>Number of Contracts<strong></td>
                            <td class="hidden-xs"><?php echo $contractCompleted[0]['num_contract'];?></td>
                        </tr>
                        <tr>
                            <td><strong>Friendliness</strong></td>
                            <td><input class="star" type="radio" name="friendliness" value="1" title="Worst" disabled="disabled" <?php echo ($stats[0]['friendness'] <2 && $stats[0]['friendness']!=null)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="friendliness" value="2" title="Bad" disabled="disabled" <?php echo ($stats[0]['friendness'] <3 && $stats[0]['friendness'] >= 2)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="friendliness" value="3" title="OK" disabled="disabled" <?php echo ($stats[0]['friendness'] <4 && $stats[0]['friendness'] >= 3)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="friendliness" value="4" title="Good" disabled="disabled" <?php echo ($stats[0]['friendness'] < 5  && $stats[0]['friendness'] >= 4)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="friendliness" value="5" title="Best" disabled="disabled" <?php echo ($stats[0]['friendness'] >=5)?' checked="checked"':'';?>/></td>
                        </tr>
                        <tr>
                            <td><strong>Knowledge</strong></td>
                            <td><input class="star" type="radio" name="knowledge" value="1" title="Worst" disabled="disabled" <?php echo ($stats[0]['knowledge'] <2   && $stats[0]['knowledge']!=null)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="knowledge" value="2" title="Bad" disabled="disabled" <?php echo ($stats[0]['knowledge'] <3 && $stats[0]['knowledge'] >= 2)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="knowledge" value="3" title="OK" disabled="disabled" <?php echo ($stats[0]['knowledge'] <4 && $stats[0]['knowledge'] >= 3)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="knowledge" value="4" title="Good" disabled="disabled"<?php echo ($stats[0]['knowledge'] < 5 && $stats[0]['knowledge'] >= 4)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="knowledge" value="5" title="Best" disabled="disabled" <?php echo ($stats[0]['knowledge'] >=5)?' checked="checked"':'';?>/></td>
                            <td class="hidden-xs"><strong>Number of Reviews</strong></td>
                            <td class="hidden-xs"><?php echo $stats[0]['num_contract'];?></td>
                        </tr>
                        <tr>
                            <td><strong>Service Quality</strong></td>
                            <td><input class="star" type="radio" name="teaching" value="1" title="Worst" disabled="disabled" <?php echo ($stats[0]['teaching_skill'] <2  && $stats[0]['teaching_skill']!=null)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="teaching" value="2" title="Bad" disabled="disabled" <?php echo ($stats[0]['teaching_skill'] <3 && $stats[0]['teaching_skill'] >= 2)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="teaching" value="3" title="OK" disabled="disabled" <?php echo ($stats[0]['teaching_skill'] <4 && $stats[0]['teaching_skill'] >= 3)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="teaching" value="4" title="Good" disabled="disabled" <?php echo ($stats[0]['teaching_skill'] < 5  && $stats[0]['teaching_skill'] >= 4)?' checked="checked"':'';?> />
                                <input class="star" type="radio" name="teaching" value="5" title="Best" disabled="disabled"  <?php echo ($stats[0]['teaching_skill'] >=5)?' checked="checked"':'';?> /></td>
                        </tr>
                        <tr class="hidden-sm hidden-md hidden-lg">
                            <td><strong>Number of Contracts<strong></td>
                            <td><?php echo $contractCompleted[0]['num_contract'];?></td>
                        </tr>
                        <tr class="hidden-sm hidden-md hidden-lg">
                            <td><strong>Number of Reviews</strong></td>
                            <td><?php echo $stats[0]['num_contract'];?></td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Info Content -->
            </div>
            <!-- END Info Block -->

            <!-- Timeline Style Block -->
            <div class="block full">
                <!-- Timeline Style Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <span data-toggle="tooltip" title="Add" id="myExperience">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#addExp" class="btn btn-sm btn-alt btn-default btn-overview"><i class="fa fa-pencil"></i></a>
                        </span>
                    </div>
                    <h2>My Experience</h2>
                </div>
                <!-- END Timeline Style Title -->

                <!-- Timeline Style Content -->
                <!-- You can remove the class .block-content-full if you want the block to have its regular padding -->
                <div class="timeline">
                    {# <h3 class="timeline-header">Updated <small> on <strong> June 14, 2014</strong></small></h3> #}
                    <!-- You can remove the class .timeline-hover if you don't want each event to be highlighted on mouse hover -->
                    <ul class="timeline-list timeline-hover">
                    <?php
                    if (count($userExperiences) > 0){
                        foreach ($userExperiences as $key => $value) {
                        ?><li <?php echo ($key == 0)?'class="active"':'';?>>
                            <div class="btn-group btn-group-sm pull-right">
                                <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default dropdown-toggle enable-tooltip" style="border-style: solid; border-width: 1px" data-toggle="dropdown" title="Options"><i class="fa fa-cog"></i></a>
                                <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                    <li class="">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#expEdit" data-id="<?php echo $value['id'];?>" onclick="editExperience(<?php echo $value['id'];?>);">Edit</a>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#expDelete" onclick="deleteExperience(<?php echo $value['id'];?>);">Delete</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="timeline-icon"><i class="fa fa-file-text"></i></div>
                            <div class="timeline-time"><?php
                             if ($value['end_year'] < 9999){
                                echo  date('F', mktime(0, 0, 0, $value['end_month'], 1,$value['end_year'])) . " " . date('Y', mktime(0, 0, 1, 1,1, $value['end_year'])) . " - <br>" . date('F', mktime(0, 0, 0, $value['start_month'], 1,$value['start_year'])) . " " . date('Y', mktime(0, 0, 1, 1,1, $value['start_year']));
                             }else{
                                echo '<strong>Present</strong> - <br>' . date('F', mktime(0, 0, 0, $value['start_month'], 1,$value['start_year'])) . " " . date('Y', mktime(0, 0, 1, 1,1, $value['start_year']));
                             }
                             ?> 
                            </div>
                            <div class="timeline-content">
                                <p class="push-bit"><strong><?php echo $value['title'];?></strong></p>
                                <?php echo $value['content'];?>
                            </div>
                        </li>
                        <?php
                        }
                    }else{
                    ?>
                    <li>
                        What have you done in the last couple of years? Putting accurate information here raises your authenticity.
                    </li>
                    <?php
                    }
                    ?>
                    </ul>
                </div>
                <!-- END Timeline Style Content -->

            </div>
            <!-- END Timeline Style Block -->

            <!-- Intro Block -->
            <div class="block">
                <!-- Intro Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <span data-toggle="tooltip" title="Upload" id="myIntro">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#uploadIntro" class="btn btn-sm btn-alt btn-default btn-overview" ><i class="fa fa-pencil"></i></a>
                        </span>
                    </div>
                    <h2>My Intro Video</h2>
                </div>
                <!-- END Intro Title -->

                <!-- New Media Content -->
                <div class="gallery" data-toggle="lightbox-gallery">
                    <div class="row">
                       {# <div class="col-xs-6 col-sm-3">
                            <a href="" data-toggle="modal" data-target="#deleteMedia" style="text-decoration:none;">X</a>
                            <a href="http://lorempixel.com/720/480/" class="gallery-link" title="Image Info">
                                <img src="http://lorempixel.com/720/480/" alt="image">
                            </a>
                        </div> #}

                        <div class="col-xs-6 col-sm-12 text-center">
                            <?php if($videos){ ?>
                            	<video width="300" height="" controls>
        						  <source src="<?php if($videos){ echo $videos[0]['vid_url']; } ?>" type="video/mp4">
        						  Your browser does not support the video tag.
        						</video>
        					<?php
        					}
        					else {
        					?>
        						This user did not upload an introductory video.
        					<?php
        					}
        					?>
                        </div>
                    </div>
                </div>
                <!-- END Media Content -->
            </div>
            <!-- END Intro Block -->

            <!-- Media Block -->
            <div class="block">
                <!-- Media Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <span data-toggle="tooltip" title="Upload" id="myMedia">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#uploadMedia" class="btn btn-sm btn-alt btn-default btn-overview" ><i class="fa fa-pencil"></i></a>
                        </span>
                    </div>
                    <h2>My Media</h2>
                </div>
                <!-- END Media Title -->

                <!-- New Media Content -->
                <div class="gallery" data-toggle="lightbox-gallery">
                    <div class="row">
                    	<?php 
                    	if($medias){
                    		foreach($medias as $media) {
                    			if($media['mid_type']==1) {
                    	?>
		                        <div class="col-xs-6 col-sm-3">
		                            <a href="javascript:void(0);" style="text-decoration:none;" onclick="mediaDeleteModal('<?php echo $media['mid_id']; ?>')">X</a>
		                            <a href="<?php echo $media['mid_url']; ?>" class="gallery-link mfp-image" title="<?php echo $media['mid_name']; ?>" data-type="image">
		                                <img src="<?php echo $media['mid_url']; ?>" alt="<?php echo $media['mid_name']; ?>">
		                            </a>
		                        </div>
                        <?php
                        		}
                        		else {
                        ?>
                        		<div class="col-xs-6 col-sm-3">
		                            <a href="javascript:void(0);" style="text-decoration:none;" onclick="mediaDeleteModal('<?php echo $media['mid_id']; ?>')">X</a>
		                            <a href="https://www.youtube.com/watch?v=<?php echo $media['mid_url']; ?>" class="gallery-link mfp-iframe" title="<?php echo $media['mid_name']; ?>" data-type="video">
		                                <img src="https://img.youtube.com/vi/<?php echo $media['mid_url']; ?>/0.jpg" alt="<?php echo $media['mid_name']; ?>">
		                            </a>
		                        </div>		
                        <?php
                        		}
                        	}
                        }else{
                        ?>
                            <div class="col-xs-6 col-sm-6">
                                Here you can upload some pictures and videos to show to others!
                            </div>  
                        <?php
                            }
                        ?>
                    </div>
                </div>
                <!-- END Media Content -->
            </div>
            <!-- END Media Block -->

            <!-- Reviews Block -->
            <div class="block">
                <!-- Reviews Title -->
                <div class="block-title" id="myReviews">
                    <h2>My Reviews</h2>
                </div>
                <!-- END Reviews Title -->

                <!-- Reviews Content -->
                <ul class="media-list">

                </ul>
                <div class="more-review">[ More Reviews ]</div>
                <div class="clear"></div>
                <!-- END Reviews Content -->
            </div>
            <!-- END Reviews Block -->

            {# <!-- Likes Block -->
            <div class="block">
                <!-- Likes Title -->
                <div class="block-title">
                    <h2>Recently Liked Media <small>&bull; <a href="javascript:void(0)">450</a></small></h2>
                </div>
                <!-- END Likes Title -->

                <!-- Likes Content -->
                <div class="row text-center">
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar15.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar7.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar1.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar14.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar3.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar3.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar14.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar2.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar6.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar16.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar3.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar13.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                </div>
                <!-- END Likess Content -->
            </div>
            <!-- END Likes Block --> #}
        </div>
        <!-- END Second Column -->
        {% endif %}
    </div>
    <!-- END User Profile Content -->
</section>
{% endblock %}

{% block modal %}
<!-- START modal-->
<div id="camera" tabindex="-1" role="dialog" aria-labelledby="cameraLabel" aria-hidden="true" class="modal fade" style="overflow-x: scroll;">
    <div class="modal-dialog cameraWidth">
        <div class="modal-content" style="margin-left: 10px; margin-top: 10px;">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="cameraLabel" class="modal-title">Upload a photo of yourself (only JPG/JPEG supported)</h4>
            </div>
            <div class="modal-body">
               <form action="/overview/setprofileimage" method="POST" enctype="multipart/form-data" id="coords">
               	  <div class ="form-group">
                    <span id="err_coords" style="color: #ff0000; float: left; text-align: center; width: 100%;"></span>
                  </div><br>
                  	<input type='file' id="imgAvatar" name="image" onchange="readURLAvatar(this, 'avatar');" />
        	      <img id="avatar" src="#" alt="your image" class="avatar"/>
                  <div class="inline-labels">
                  	<input type="hidden" id="x" name="crop[x]" />
        			<input type="hidden" id="y" name="crop[y]" />
        			<input type="hidden" id="w" name="crop[w]" />
        			<input type="hidden" id="h" name="crop[h]" />
        		  </div>
                  <br>
                  <input type="hidden" size="4" id="uplloadAvatar" name="uplloadAvatar" value=""/>
                  <button type="button" class="btn btn-info btn-sm" onclick="checkCoordsImg(1);">Crop & Upload</button>
                  {# <button type="button" class="btn btn-success btn-sm" onclick="checkCoordsImg(2);">Upload</button> #}
               </form>
               <br>
            </div>
            <div class="modal-footer">
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->

<!-- START modal-->
<div id="about" tabindex="-1" role="dialog" aria-labelledby="cameraLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="cameraLabel" class="modal-title">Please write a little about yourself.</h4>
            </div>
            <div class="modal-body">
               <form action="/settings/updatebio" method="POST">
                  <div class ="form-group">
                    <textarea class="form-control ckeditor" name="about">{{ userInfo.usr_about }}</textarea>
                  </div>
            </div>
            <div class="modal-footer">
               <input type="submit" value="Submit" class="btn btn-info">
               </form>             
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->

<!-- START modal-->
   <div id="addExp" tabindex="-1" role="dialog" aria-labelledby="expLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="expLabel" class="modal-title">Add experiences to display on your profile</h4>
            </div>
            <div class="modal-body">
               <div class = "form-horizontal">
                  <div class ="form-group">
                    <label class = "col-lg-2 control-label">Job Title:</label>
                    <div class = "col-lg-9">
                        <input class = "form-control" type="text" name="title" id="job-title"><br/>
                    </div>
                  </div>
                  <div class = "form-group">
                    <label class = "col-lg-2 control-label">Description:</label>
                    <div class = "col-lg-9">
                        <textarea class = "form-control" name="content" id="job-content"></textarea>
                    </div>
                  </div>
                  <div class ="form-group">
                    <label class = "col-lg-2 control-label">Time Period:</label>
                    <div class="col-lg-10" style="margin-top: 10px;">
                        <div class="col-lg-3" style="padding: 0px;">
                            <select name="startDateMonth" class="form-control" id="startDateMonth">
                                <option value selected="">Month</option>
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                        <div class="col-lg-2" style="padding: 0px;">
                            <input type="text" class="form-control" name="startYear" placeholder="Year" id="startYear">
                        </div>
                        <div class="col-lg-1" style="padding: 0px; text-align: center;">
                            -
                        </div>
                        <div class="endDateMonthYear">
                            <div class="col-lg-3" style="padding: 0px;">
                                <select name="endDateMonth" class="form-control" id="endDateMonth">
                                    <option value selected="">Month</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </div>
                            <div class="col-lg-2" style="padding: 0px;">
                                <input class="form-control" type="text" name="endYear" placeholder="Year" id="endYear">
                            </div>
                        </div>
                        <div class="col-lg-3" id="add-present-work" style="display: none;">Present</div>
                    </div>
                  </div>
                  <div class="form-group">
                       <div class="col-lg-offset-2 col-lg-10">
                          <div class="checkbox c-checkbox">
                             <label>
                                <input type="checkbox" class="current" id="current" >
                                <span class="fa fa-check"></span>I currently work here</label>
                          </div>
                       </div>
                    </div>
               </div>
            </div>
            <div class="modal-footer">
               <input type="submit" value="Submit" class="btn btn-info" id="addExpSubmit">
               <button type="button" id="closeAdd" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
         </div>
      </div>
   </div>
<!-- END modal-->

<!-- START modal-->
   <div id="expEdit" tabindex="-1" role="dialog" aria-labelledby="expLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="expLabel" class="modal-title">Edit this experience</h4>
            </div>
            <div class="modal-body">
               <div class = "form-horizontal" action="" method="post" enctype="multipart/form-data">
                  <div class ="form-group">
                    <label class = "col-lg-2 control-label">Job Title:</label>
                    <div class = "col-lg-9">
                        <input class = "form-control" type="text" name="title" id="editExpTitle"><br/>
                    </div>
                  </div>
                  <div class = "form-group">
                    <label class = "col-lg-2 control-label">Description:</label>
                    <div class = "col-lg-9">
                        <textarea class = "form-control" name="content" id="editExpContent"></textarea>
                    </div>
                  </div>
                  <div class ="form-group">
                    <label class = "col-lg-2 control-label">Time Period:</label>
                    <div class="col-lg-10" style="margin-top: 10px;">
                        <div class="col-lg-3" style="padding: 0px;">
                            <select name="startDateMonth" class="form-control" id="editStartDateMonth">
                                <option value selected="selected">Month</option>
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                        <div class="col-lg-2" style="padding: 0px;">
                            <input class = "form-control" type="text" name="startYear" placeholder="Year" id="editStartYear">
                        </div>
                        <div class="col-lg-1" style="padding: 0px; text-align: center;">
                            -
                        </div>
                        <div class="endDateMonthYear">
                            <div class="col-lg-3" style="padding: 0px;">
                                <select name="endDateMonth" class="form-control" id="editEndDateMonth">
                                    <option value selected="selected">Month</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </div>
                            <div class="col-lg-2" style="padding: 0px;">
                                <input class = "form-control" type="text" name="endYear" placeholder="Year" id="editEndYear">
                            </div>
                        </div>
                        <div class="col-lg-3" id="present-work" style="display: none;">Present</div>
                    </div>
                  </div>
                  <div class="form-group">
                       <div class="col-lg-offset-2 col-lg-10">
                          <div class="checkbox c-checkbox">
                             <label>
                                <input type="checkbox" class="current" id="editCurrent">
                                <span class="fa fa-check"></span>I currently work here</label>
                          </div>
                       </div>
                    </div>
                  <input type="hidden" id="editId" />
               </div>
            </div>
            <div class="modal-footer">
               <input type="submit" value="Submit" class="btn btn-info" id="editExperienceForm">
               <button type="button" id="closeEdit" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
         </div>
      </div>
   </div>
<!-- END modal-->

{# <!-- START modal-->
<div id="availability" tabindex="-1" role="dialog" aria-labelledby="expLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
                <h4 id="expLabel" class="modal-title">Edit Availability</h4>
            </div>
            <div class="modal-body">
                <div class = "form-horizontal" action="" method="post" enctype="multipart/form-data">
                    <div class ="form-group">
                        <label class = "col-lg-2 control-label">Monday</label>
                        <div class="col-lg-10">
                            <div class="col-xs-5" style="padding: 0px;">
                                <select class="form-control">
                                    <option value="0">12 am</option>
                                    <option value="1">1 am</option>
                                    <option value="2">2 am</option>
                                    <option value="3">3 am</option>
                                    <option value="4">4 am</option>
                                    <option value="5">5 am</option>
                                    <option value="6">6 am</option>
                                    <option value="7">7 am </option>
                                    <option value="8">8 am</option>
                                    <option value="9">9 am</option>
                                    <option value="10">10 am</option>
                                    <option value="11">11 am</option>
                                    <option value="12">12 pm</option>
                                    <option value="13">1 pm</option>
                                    <option value="14">2 pm</option>
                                    <option value="15">3 pm</option>
                                    <option value="16">4 pm</option>
                                    <option value="17">5 pm</option>
                                    <option value="18">6 pm</option>
                                    <option value="19">7 pm </option>
                                    <option value="20">8 pm</option>
                                    <option value="21">9 pm</option>
                                    <option value="22">10 pm</option>
                                    <option value="23">11 pm</option>
                                </select>
                            </div>
                            <div class="col-xs-2" style="padding: 0px; text-align: center;">
                                -
                            </div>
                            <div class="col-xs-5" style="padding: 0px;">
                                <select class="form-control">
                                    <option value="0">12 am</option>
                                    <option value="1">1 am</option>
                                    <option value="2">2 am</option>
                                    <option value="3">3 am</option>
                                    <option value="4">4 am</option>
                                    <option value="5">5 am</option>
                                    <option value="6">6 am</option>
                                    <option value="7">7 am </option>
                                    <option value="8">8 am</option>
                                    <option value="9">9 am</option>
                                    <option value="10">10 am</option>
                                    <option value="11">11 am</option>
                                    <option value="12">12 pm</option>
                                    <option value="13">1 pm</option>
                                    <option value="14">2 pm</option>
                                    <option value="15">3 pm</option>
                                    <option value="16">4 pm</option>
                                    <option value="17">5 pm</option>
                                    <option value="18">6 pm</option>
                                    <option value="19">7 pm </option>
                                    <option value="20">8 pm</option>
                                    <option value="21">9 pm</option>
                                    <option value="22">10 pm</option>
                                    <option value="23">11 pm</option>
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class = "col-lg-2 control-label">Tuesday</label>
                        <div class="col-lg-10">
                            <div class="col-xs-5" style="padding: 0px;">
                                <select class="form-control">
                                    <option value="0">12 am</option>
                                    <option value="1">1 am</option>
                                    <option value="2">2 am</option>
                                    <option value="3">3 am</option>
                                    <option value="4">4 am</option>
                                    <option value="5">5 am</option>
                                    <option value="6">6 am</option>
                                    <option value="7">7 am </option>
                                    <option value="8">8 am</option>
                                    <option value="9">9 am</option>
                                    <option value="10">10 am</option>
                                    <option value="11">11 am</option>
                                    <option value="12">12 pm</option>
                                    <option value="13">1 pm</option>
                                    <option value="14">2 pm</option>
                                    <option value="15">3 pm</option>
                                    <option value="16">4 pm</option>
                                    <option value="17">5 pm</option>
                                    <option value="18">6 pm</option>
                                    <option value="19">7 pm </option>
                                    <option value="20">8 pm</option>
                                    <option value="21">9 pm</option>
                                    <option value="22">10 pm</option>
                                    <option value="23">11 pm</option>
                                </select>
                            </div>
                            <div class="col-xs-2" style="padding: 0px; text-align: center;">
                                -
                            </div>
                            <div class="col-xs-5" style="padding: 0px;">
                                <select class="form-control">
                                    <option value="0">12 am</option>
                                    <option value="1">1 am</option>
                                    <option value="2">2 am</option>
                                    <option value="3">3 am</option>
                                    <option value="4">4 am</option>
                                    <option value="5">5 am</option>
                                    <option value="6">6 am</option>
                                    <option value="7">7 am </option>
                                    <option value="8">8 am</option>
                                    <option value="9">9 am</option>
                                    <option value="10">10 am</option>
                                    <option value="11">11 am</option>
                                    <option value="12">12 pm</option>
                                    <option value="13">1 pm</option>
                                    <option value="14">2 pm</option>
                                    <option value="15">3 pm</option>
                                    <option value="16">4 pm</option>
                                    <option value="17">5 pm</option>
                                    <option value="18">6 pm</option>
                                    <option value="19">7 pm </option>
                                    <option value="20">8 pm</option>
                                    <option value="21">9 pm</option>
                                    <option value="22">10 pm</option>
                                    <option value="23">11 pm</option>
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class = "col-lg-2 control-label">Wednesday</label>
                        <div class="col-lg-10">
                            <div class="col-xs-5" style="padding: 0px;">
                                <select class="form-control">
                                    <option value="0">12 am</option>
                                    <option value="1">1 am</option>
                                    <option value="2">2 am</option>
                                    <option value="3">3 am</option>
                                    <option value="4">4 am</option>
                                    <option value="5">5 am</option>
                                    <option value="6">6 am</option>
                                    <option value="7">7 am </option>
                                    <option value="8">8 am</option>
                                    <option value="9">9 am</option>
                                    <option value="10">10 am</option>
                                    <option value="11">11 am</option>
                                    <option value="12">12 pm</option>
                                    <option value="13">1 pm</option>
                                    <option value="14">2 pm</option>
                                    <option value="15">3 pm</option>
                                    <option value="16">4 pm</option>
                                    <option value="17">5 pm</option>
                                    <option value="18">6 pm</option>
                                    <option value="19">7 pm </option>
                                    <option value="20">8 pm</option>
                                    <option value="21">9 pm</option>
                                    <option value="22">10 pm</option>
                                    <option value="23">11 pm</option>
                                </select>
                            </div>
                            <div class="col-xs-2" style="padding: 0px; text-align: center;">
                                -
                            </div>
                            <div class="col-xs-5" style="padding: 0px;">
                                <select class="form-control">
                                    <option value="0">12 am</option>
                                    <option value="1">1 am</option>
                                    <option value="2">2 am</option>
                                    <option value="3">3 am</option>
                                    <option value="4">4 am</option>
                                    <option value="5">5 am</option>
                                    <option value="6">6 am</option>
                                    <option value="7">7 am </option>
                                    <option value="8">8 am</option>
                                    <option value="9">9 am</option>
                                    <option value="10">10 am</option>
                                    <option value="11">11 am</option>
                                    <option value="12">12 pm</option>
                                    <option value="13">1 pm</option>
                                    <option value="14">2 pm</option>
                                    <option value="15">3 pm</option>
                                    <option value="16">4 pm</option>
                                    <option value="17">5 pm</option>
                                    <option value="18">6 pm</option>
                                    <option value="19">7 pm </option>
                                    <option value="20">8 pm</option>
                                    <option value="21">9 pm</option>
                                    <option value="22">10 pm</option>
                                    <option value="23">11 pm</option>
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class = "col-lg-2 control-label">Thursday</label>
                        <div class="col-lg-10">
                            <div class="col-xs-5" style="padding: 0px;">
                                <select class="form-control">
                                    <option value="0">12 am</option>
                                    <option value="1">1 am</option>
                                    <option value="2">2 am</option>
                                    <option value="3">3 am</option>
                                    <option value="4">4 am</option>
                                    <option value="5">5 am</option>
                                    <option value="6">6 am</option>
                                    <option value="7">7 am </option>
                                    <option value="8">8 am</option>
                                    <option value="9">9 am</option>
                                    <option value="10">10 am</option>
                                    <option value="11">11 am</option>
                                    <option value="12">12 pm</option>
                                    <option value="13">1 pm</option>
                                    <option value="14">2 pm</option>
                                    <option value="15">3 pm</option>
                                    <option value="16">4 pm</option>
                                    <option value="17">5 pm</option>
                                    <option value="18">6 pm</option>
                                    <option value="19">7 pm </option>
                                    <option value="20">8 pm</option>
                                    <option value="21">9 pm</option>
                                    <option value="22">10 pm</option>
                                    <option value="23">11 pm</option>
                                </select>
                            </div>
                            <div class="col-xs-2" style="padding: 0px; text-align: center;">
                                -
                            </div>
                            <div class="col-xs-5" style="padding: 0px;">
                                <select class="form-control">
                                    <option value="0">12 am</option>
                                    <option value="1">1 am</option>
                                    <option value="2">2 am</option>
                                    <option value="3">3 am</option>
                                    <option value="4">4 am</option>
                                    <option value="5">5 am</option>
                                    <option value="6">6 am</option>
                                    <option value="7">7 am </option>
                                    <option value="8">8 am</option>
                                    <option value="9">9 am</option>
                                    <option value="10">10 am</option>
                                    <option value="11">11 am</option>
                                    <option value="12">12 pm</option>
                                    <option value="13">1 pm</option>
                                    <option value="14">2 pm</option>
                                    <option value="15">3 pm</option>
                                    <option value="16">4 pm</option>
                                    <option value="17">5 pm</option>
                                    <option value="18">6 pm</option>
                                    <option value="19">7 pm </option>
                                    <option value="20">8 pm</option>
                                    <option value="21">9 pm</option>
                                    <option value="22">10 pm</option>
                                    <option value="23">11 pm</option>
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class = "col-lg-2 control-label">Friday</label>
                        <div class="col-lg-10">
                            <div class="col-xs-5" style="padding: 0px;">
                                <select class="form-control">
                                    <option value="0">12 am</option>
                                    <option value="1">1 am</option>
                                    <option value="2">2 am</option>
                                    <option value="3">3 am</option>
                                    <option value="4">4 am</option>
                                    <option value="5">5 am</option>
                                    <option value="6">6 am</option>
                                    <option value="7">7 am </option>
                                    <option value="8">8 am</option>
                                    <option value="9">9 am</option>
                                    <option value="10">10 am</option>
                                    <option value="11">11 am</option>
                                    <option value="12">12 pm</option>
                                    <option value="13">1 pm</option>
                                    <option value="14">2 pm</option>
                                    <option value="15">3 pm</option>
                                    <option value="16">4 pm</option>
                                    <option value="17">5 pm</option>
                                    <option value="18">6 pm</option>
                                    <option value="19">7 pm </option>
                                    <option value="20">8 pm</option>
                                    <option value="21">9 pm</option>
                                    <option value="22">10 pm</option>
                                    <option value="23">11 pm</option>
                                </select>
                            </div>
                            <div class="col-xs-2" style="padding: 0px; text-align: center;">
                                -
                            </div>
                            <div class="col-xs-5" style="padding: 0px;">
                                <select class="form-control">
                                    <option value="0">12 am</option>
                                    <option value="1">1 am</option>
                                    <option value="2">2 am</option>
                                    <option value="3">3 am</option>
                                    <option value="4">4 am</option>
                                    <option value="5">5 am</option>
                                    <option value="6">6 am</option>
                                    <option value="7">7 am </option>
                                    <option value="8">8 am</option>
                                    <option value="9">9 am</option>
                                    <option value="10">10 am</option>
                                    <option value="11">11 am</option>
                                    <option value="12">12 pm</option>
                                    <option value="13">1 pm</option>
                                    <option value="14">2 pm</option>
                                    <option value="15">3 pm</option>
                                    <option value="16">4 pm</option>
                                    <option value="17">5 pm</option>
                                    <option value="18">6 pm</option>
                                    <option value="19">7 pm </option>
                                    <option value="20">8 pm</option>
                                    <option value="21">9 pm</option>
                                    <option value="22">10 pm</option>
                                    <option value="23">11 pm</option>
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class = "col-lg-2 control-label">Saturday</label>
                        <div class="col-lg-10">
                            <div class="col-xs-5" style="padding: 0px;">
                                <select class="form-control">
                                    <option value="0">12 am</option>
                                    <option value="1">1 am</option>
                                    <option value="2">2 am</option>
                                    <option value="3">3 am</option>
                                    <option value="4">4 am</option>
                                    <option value="5">5 am</option>
                                    <option value="6">6 am</option>
                                    <option value="7">7 am </option>
                                    <option value="8">8 am</option>
                                    <option value="9">9 am</option>
                                    <option value="10">10 am</option>
                                    <option value="11">11 am</option>
                                    <option value="12">12 pm</option>
                                    <option value="13">1 pm</option>
                                    <option value="14">2 pm</option>
                                    <option value="15">3 pm</option>
                                    <option value="16">4 pm</option>
                                    <option value="17">5 pm</option>
                                    <option value="18">6 pm</option>
                                    <option value="19">7 pm </option>
                                    <option value="20">8 pm</option>
                                    <option value="21">9 pm</option>
                                    <option value="22">10 pm</option>
                                    <option value="23">11 pm</option>
                                </select>
                            </div>
                            <div class="col-xs-2" style="padding: 0px; text-align: center;">
                                -
                            </div>
                            <div class="col-xs-5" style="padding: 0px;">
                                <select class="form-control">
                                    <option value="0">12 am</option>
                                    <option value="1">1 am</option>
                                    <option value="2">2 am</option>
                                    <option value="3">3 am</option>
                                    <option value="4">4 am</option>
                                    <option value="5">5 am</option>
                                    <option value="6">6 am</option>
                                    <option value="7">7 am </option>
                                    <option value="8">8 am</option>
                                    <option value="9">9 am</option>
                                    <option value="10">10 am</option>
                                    <option value="11">11 am</option>
                                    <option value="12">12 pm</option>
                                    <option value="13">1 pm</option>
                                    <option value="14">2 pm</option>
                                    <option value="15">3 pm</option>
                                    <option value="16">4 pm</option>
                                    <option value="17">5 pm</option>
                                    <option value="18">6 pm</option>
                                    <option value="19">7 pm </option>
                                    <option value="20">8 pm</option>
                                    <option value="21">9 pm</option>
                                    <option value="22">10 pm</option>
                                    <option value="23">11 pm</option>
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class ="form-group">
                        <label class = "col-lg-2 control-label">Sunday</label>
                        <div class="col-lg-10">
                            <div class="col-xs-5" style="padding: 0px;">
                                <select class="form-control">
                                    <option value="0">12 am</option>
                                    <option value="1">1 am</option>
                                    <option value="2">2 am</option>
                                    <option value="3">3 am</option>
                                    <option value="4">4 am</option>
                                    <option value="5">5 am</option>
                                    <option value="6">6 am</option>
                                    <option value="7">7 am </option>
                                    <option value="8">8 am</option>
                                    <option value="9">9 am</option>
                                    <option value="10">10 am</option>
                                    <option value="11">11 am</option>
                                    <option value="12">12 pm</option>
                                    <option value="13">1 pm</option>
                                    <option value="14">2 pm</option>
                                    <option value="15">3 pm</option>
                                    <option value="16">4 pm</option>
                                    <option value="17">5 pm</option>
                                    <option value="18">6 pm</option>
                                    <option value="19">7 pm </option>
                                    <option value="20">8 pm</option>
                                    <option value="21">9 pm</option>
                                    <option value="22">10 pm</option>
                                    <option value="23">11 pm</option>
                                </select>
                            </div>
                            <div class="col-xs-2" style="padding: 0px; text-align: center;">
                                -
                            </div>
                            <div class="col-xs-5" style="padding: 0px;">
                                <select class="form-control">
                                    <option value="0">12 am</option>
                                    <option value="1">1 am</option>
                                    <option value="2">2 am</option>
                                    <option value="3">3 am</option>
                                    <option value="4">4 am</option>
                                    <option value="5">5 am</option>
                                    <option value="6">6 am</option>
                                    <option value="7">7 am </option>
                                    <option value="8">8 am</option>
                                    <option value="9">9 am</option>
                                    <option value="10">10 am</option>
                                    <option value="11">11 am</option>
                                    <option value="12">12 pm</option>
                                    <option value="13">1 pm</option>
                                    <option value="14">2 pm</option>
                                    <option value="15">3 pm</option>
                                    <option value="16">4 pm</option>
                                    <option value="17">5 pm</option>
                                    <option value="18">6 pm</option>
                                    <option value="19">7 pm </option>
                                    <option value="20">8 pm</option>
                                    <option value="21">9 pm</option>
                                    <option value="22">10 pm</option>
                                    <option value="23">11 pm</option>
                                </select> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
               <input type="submit" class="btn btn-info">
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal--> #}

<!-- START modal-->
<div id="deleteMedia" tabindex="-1" role="dialog"
    aria-labelledby="deleteLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="deleteLabel" class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
            	Are you sure you want to remove this media from your profile?
            	<input type="hidden" id="mediaDeleteId" value="mediaDeleteId" value="" >
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" onclick="mediaDelete();">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->

<!-- START modal-->
<div id="expDelete" tabindex="-1" role="dialog"
    aria-labelledby="deleteLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="deleteLabel" class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">Are you sure you want to remove this
                experience?</div>
            <div class="modal-footer">
              <input type="hidden" id="deleteExp" />
                <button class="btn btn-danger" id="deleteButton">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->

<!-- START modal-->
<div id="reportReview" tabindex="-1" role="dialog"
    aria-labelledby="reportReviewLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="reportReviewLabel" class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">Are you sure you want to report this
                review?</div>
            <div class="modal-footer">
              <input type="hidden" id="reportRev" />
                <button class="btn btn-danger" id="reportReviewButton">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->

<!-- START modal-->
   <div id="addSkill" tabindex="-1" role="dialog" aria-labelledby="reportLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="skillLabel" class="modal-title">Add your services to display on your profile</h4>
            </div>
            <div class="modal-body">
               <form id="user-skills-form" class="form-horizontal">
                  <div class = "form-group">
                     <label for = "sub-skill" class = "col-lg-3 control-label">Skill:</label>
                     <div class = "col-lg-8">
                        <?php 
                        echo Phalcon\Tag::select(array('user-skills-input', 
                                                            TagsStandard::find(array(
                                                               "status = 1",
                                                               "order" => "name")), 
                                                            "using" => array("name", "name"),
                                                            "class" => "select-chosen form-control",
                                                            "type" => "text",
                                                            "data-role" => "tagsinput",
                                                            "value" => $userTagsComma,
                                                            "multiple" => "multiple"
                                                            )); ?>
                     </div>
                  </div>
               </form>
            </div>
            <div class="modal-footer">
               <a href="/suggest" class="pull-left">Can't find your skill?</a> 
               <button id="user-skills-submit" type="button" class="btn btn-info">Submit</button>
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
         </div>
      </div>
   </div>
<!-- END modal-->

<!-- START modal-->
   <div id="addLanguage" tabindex="-1" role="dialog" aria-labelledby="reportLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="languageLabel" class="modal-title">Add your skills to display on your profile</h4>
            </div>
            <div class="modal-body">
               <form id="user-languages-form" class="form-horizontal">
                  <div class = "form-group">
                     <label for = "sub-lang" class = "col-lg-3 control-label">Languages:</label>
                     <div class = "col-lg-8">
                        <?php 
                        echo Phalcon\Tag::select(array('user-languages-input', 
                                                            Languages::find("status = 1"), 
                                                            "using" => array("name", "name"),
                                                            "class" => "select-chosen form-control",
                                                            "type" => "text",
                                                            "data-role" => "tagsinput",
                                                            "value" => $userLangsComma,
                                                            "multiple" => "multiple"
                                                            )); ?>
                     </div>
                  </div>
               </form>
            </div>
            <div class="modal-footer">
               <button id="user-languages-submit" type="button" class="btn btn-info">Submit</button>
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
         </div>
      </div>
   </div>
<!-- END modal-->

<!-- START modal-->
   <div id="addRate" tabindex="-1" role="dialog" aria-labelledby="rateLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="RateLabel" class="modal-title">Add your hourly rate to display on your profile</h4>
            </div>
            <div class="modal-body">
               <form class="form-horizontal">
                 <div class = "form-group">
                    <label for = "rate-body" class = "col-xs-3 control-label">Hourly Rate:</label>
                    <div class="input-group m-b col-xs-8">
                         <span class="input-group-addon">$</span>
                            <input type="text" class="form-control" id="teachrate" placeholder="10" value="<?php echo $userInfo->usr_rate;?>">
                         <span class="input-group-addon">.00</span>
                      </div>
                 </div>
               </form>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-info" id="addRateForm">Submit</button>
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
         </div>
      </div>
   </div>
<!-- END modal-->

{# <!-- START modal-->
   <div id="editName" tabindex="-1" role="dialog" aria-labelledby="nameLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="nameLabel" class="modal-title">Edit your name</h4>
            </div>
            <div class="modal-body">
               <form class="form-horizontal">
                 <div class = "form-group">
                    <label for = "name-body" class = "col-lg-3 control-label">First Name:</label>
                    <div class="input-group m-b col-lg-8">
                        <input type="text" class="form-control" id="firstName" value="<?php echo $userInfo->usr_firstname;?>">
                    </div>
                 </div>
                 <div class = "form-group">
                    <label for = "name-body" class = "col-lg-3 control-label">Last Name:</label>
                    <div class="input-group m-b col-lg-8">
                        <input type="text" class="form-control" id="lastName" value="<?php echo $userInfo->usr_lastname;?>">
                    </div>
                 </div>
               </form>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-info" id="editNameForm">Submit</button>
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
         </div>
      </div>
   </div>
<!-- END modal--> #}


<!-- START modal-->
   <div id="addPostal" tabindex="-1" role="dialog" aria-labelledby="postalLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="postalLabel" class="modal-title">Add your postal code and city to display your location</h4>
            </div>
            <div class="modal-body">
               {{ form("user/updatePostal", "id" : "postal-form", "method" : "post", "class" : "form-horizontal") }}
                 <div class = "form-group">
                    <label for = "postal-body" class = "col-xs-3 control-label">Postal Code:</label>
                    <div class="input-group m-b col-xs-8">
                        {{   text_field("postalcode", "id" : "postal-code", "placeholder" : "Postal Code", "class": "form-control", "value" : userInfo.usr_postalcode) }}
                            {{   hidden_field("lat", "id" : "lat", "class": "form-control", "value" : userInfo.lat) }}
                            {{   hidden_field("lng", "id" : "lng", "class": "form-control", "value" : userInfo.lng) }}
                    </div>
                 </div>
                 <div class = "form-group">
                    <label for = "postal-body" class = "col-xs-3 control-label">City:</label>
                    <div class="input-group m-b col-xs-8">
                        <?php 
                        echo Phalcon\Tag::select(array('cityId', 
                                                   City::find(), 
                                                   "using" => array("id", "name"),
                                                   "class" => "select-chosen form-control",
                                                   "type" => "text",
                                                   "data-role" => "tagsinput",
                                                   "value" => $userCity
                                                   )); ?> 
                    </div>
                 </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-info" id="addPostalForm">Submit</button>
               {{ end_form() }}
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
         </div>
      </div>
   </div>
<!-- END modal-->

<!-- START modal-->
   <div id="uploadIntro" tabindex="-1" role="dialog" aria-labelledby="reportLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="introLabel" class="modal-title text-center">Add/Edit an introductory video to display on your profile</h4>
            </div>
            <form class = "form-horizontal" id="new-intro-form" name="newintroform" action="/videoupload/doVideoUpload" method="post" enctype="multipart/form-data">
            <div class="modal-body">
            	  <input class = "form-control" type="hidden" name="intro_video_id" id="intro_video_id" value="">
            	  <div class ="form-group">
                    <span id="intro_frm_err" style="color: #ff0000; float: left; text-align: center; width: 100%;"></span>
                  </div>
                  <div class ="form-group">
                    <label class = "col-lg-3 control-label">Title:</label>
                    <div class="col-lg-8">
                        <input class = "form-control" type="text" name="title" id="video_title" value="">
                    </div>
                  </div>
                  <div class ="form-group">
                    <label class = "col-lg-3 control-label">Video:</label>
                    <div class="col-lg-8">
                        <div id="message" style="display: none;"></div>
                        <video id="videoPreview" width="350" controls autoplay></video>
                    </div>
                  </div>
                  <div class ="form-group">
                     <label for = "upload-body" class = "col-lg-3 control-label">Upload:</label>
                     <div class="box-placeholder text-center col-lg-8">
                        <em class="fa fa-cloud-upload fa-2x"></em>
                        <div class="col-xs-offset-3">
                            <input id="upload-select-video" type="file" name="fileInput" accept="video/*" />                           
                        </div>
                     </div>
                  </div>
                  <div class = "form-group" >
                     <label for = "upload-tag" class = "col-lg-3 control-label">Skills:</label>
                     <div class = "col-lg-8">
                        <?php
                        $tagArr = array();
                        foreach ($userTags as $userTag) {
                        	array_push($tagArr, $userTag['tag_id']);
                        }
                        $tag = new Phalcon\Forms\Element\Select('user-skills-input[]', TagsStandard::find("status = 1"), array(
                            "using" => array("id", "name"),
				            "useEmpty" => false,
				            "multiple" => true,
				            "class" => "select-chosen form-control",
                            "type" => "text",
                            "data-role" => "tagsinput"
				        ));				
				        $tag->setDefault($tagArr);
				        echo $tag;
				      	?>
                     </div>
                  </div>
            </div>
            <div class="modal-footer">
               <a href="/suggest" class="pull-left">Can't find your skill?</a>
               <input type="button" value="Submit" class="btn btn-info" onclick="uploadVideo();">
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
            </form>
         </div>
      </div>
   </div>
<!-- END modal-->

<!-- START modal-->
   <div id="uploadMedia" tabindex="-1" role="dialog" aria-labelledby="reportLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="mediaLabel" class="modal-title">Add medias to display on your profile</h4>
            </div>
            <div class="modal-body">
               <form class = "form-horizontal" id="new-media-form"  name="newform" action="/media/doMediaUpload" method="post" enctype="multipart/form-data">
               	  <div class ="form-group">
                    <span id="media_frm_err" style="color: #ff0000; float: left; text-align: center; width: 100%;"></span>
                  </div>
                  <div class ="form-group">
                    <label class = "col-lg-3 control-label">Title:</label>
                    <div class="col-lg-8">
                        <input class = "form-control" type="text" name="title" id="media_title">
                    </div>
                  </div>
                  <div class ="form-group">
                    <label class = "col-lg-3 control-label">Type:</label>
                    <div class="col-lg-8">
                        <select class = "form-control" name="media_type" id="media_type">
                        	<option value="1">IMAGE</option>
                        	<option value="2">VIDEO</option>
                        </select>
                    </div>
                  </div>
                  <div class ="form-group">
                     <label for = "upload-body" class = "col-lg-3 control-label">Upload:</label>
                     <div id="upload-drop" class="box-placeholder text-center col-lg-8">
                        <em class="fa fa-cloud-upload fa-2x"></em>
                        <div class="col-xs-offset-3">
                            <input id="upload-select-media" type="file" name="file">
                        </div>
                     </div>
                  </div>
                  <div class = "form-group">
                    <label class = "col-lg-3 control-label">Body:</label>
                    <div class="col-lg-8">
                        <textarea class = "form-control" id="media_content" name="media_content"></textarea>
                    </div>
                  </div>
                  <div class = "form-group">
                     <label for = "sub-skill" class = "col-lg-3 control-label">Skill:</label>
                     <div class = "col-lg-8">
                        <?php
                        $tag = new Phalcon\Forms\Element\Select('user-skills-input[]', TagsStandard::find("status = 1"), array(
                            "using" => array("id", "name"),
				            "useEmpty" => false,
				            "multiple" => true,
				            "class" => "select-chosen form-control",
                            "type" => "text",
                            "data-role" => "tagsinput"
				        ));				
				        echo $tag;
				      	?>
                     </div>
                  </div>
               </form>
            </div>
            <div class="modal-footer">
               <a href="/suggest" class="pull-left">Can't find your skill?</a> 
               <input type="button" value="Submit" class="btn btn-info" onclick="uploadMedia();">
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
         </div>
      </div>
   </div>
<!-- END modal-->
<!-- START modal-->
<div id="coverPhoto" tabindex="-1" role="dialog" aria-labelledby="coverLabel" aria-hidden="true" class="modal fade">
    <div class="modal-content" style="margin-left: 10px; margin-top: 10px;">
        <div class="modal-header">
           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
           <h4 id="coverLabel" class="modal-title">Set Your Cover Photo</h4>
        </div>
        <div class="modal-body">
           <form action="/overview/setcoverimage" method="POST" enctype="multipart/form-data">
              <input type='file' id="imgInp" name="image" />
              <img id="cover" src="#" alt="your image" style="margin-top:10px; margin-bottom:10px;">
              <input type="hidden" id="x1" name="crop[x1]" />
              <input type="hidden" id="y1" name="crop[y1]" />
              <input type="hidden" id="w1" name="crop[w1]" />
              <input type="hidden" id="h1" name="crop[h1]" />
              <br>
              <button type="button" class="btn btn-danger btn-sm">Crop</button>
              <button type="submit" class="btn btn-info btn-sm">Upload</button>
           </form>
           <br>
        </div>
        <div class="modal-footer">
           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
        </div>
    </div>
</div>
<!-- END modal-->
{% endblock %}

{% block footer %}
{% endblock %}

{% block script %}
<!-- Load and execute javascript code used only in this page -->
{{ javascript_include("third-party/proui/backend/js/pages/readyTimeline.js") }}

<!-- ckeditor.js, load it only in the page you would like to use CKEditor (it's a heavy plugin to include it with the others!) -->
{{ javascript_include('third-party/proui/backend/js/helpers/ckeditor/ckeditor.js') }}

<!-- Star Rating -->
{{ javascript_include("third-party/star-rating/jquery.rating.js") }}
{{ javascript_include("third-party/star-rating/jquery.MetaData.js") }}
{{ javascript_include('third-party/async/async.js') }}
<script>           
var jcrop_api;
function readURLAvatar(input, img_prv_id) {
	if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        	$('#'+img_prv_id).show();
            $('#'+img_prv_id).attr('src', e.target.result);
			var image = new Image();
		    image.src = e.target.result;		
		    image.onload = function() {
		        if(this.width > 600) {
		        	var wd = this.width+20;
		        	$('.modal-dialog').width(wd);
		        }
		        $('#avatar').Jcrop({
	               aspectRatio: 1,        
			       onSelect:   updateCoords,
			       onRelease:  clearCoords
	            });
		    };
            
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function updateCoords(c)
{
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
};
function clearCoords() {
    $('#x').val('');
    $('#y').val('');
    $('#w').val('');
    $('#h').val('');
};
  
function checkCoordsImg(type) {
	$("#uplloadAvatar").val(type);
	var fileInput = document.getElementById ("imgAvatar");
	var msg = '';
	//var allowType = ["png", "gif", "jpg", "jpeg"];
    var allowType = ["jpg", "jpeg"];    	
	var fileExt = fileInput.files[0].name.split(".");
	
	if (fileInput.value == "") {
        msg += "Please browse for file<br>";
    }    
    if(fileInput.files[0].size>10000000) {
        msg += "File size should not be more than 10MB<br>";        
    }
    if(!inArray(fileExt[fileExt.length-1], allowType)) {
        msg += "File type should be "+ allowType.join(", ") +"<br>";        
    }
    if(type==1) {
	    if (!parseInt($('#w').val())) {
	    	msg += "Please select a crop region then press Crop & Upload.";
	    }
    }    
    if(msg=='') {
    	$("#coords").submit();
    }
    else {
    	$("#err_coords").html(msg);
    }
}  
  
var page = 0;
var userId = {{userInfo.usr_id}};
$(document).ready(function(){
    /* var map =$(".gmap").gMap({
        latitude: {{userInfo.lat}},
        longitude: {{userInfo.lng}},
        zoom: 16,
        markers: [
            {
                latitude:  {{userInfo.lat}},
                longitude: {{userInfo.lng}},
            }
        ],
    }); */
    if(document.referrer == '<?php echo $this->url->get("register/tour"); ?>'){
        var width = $(window).width();
        if(width >= 768) {
            hopscotch.startTour(tour);
        }
    }
    
    processReview();
    $('.more-review').click(function(e){
        processReview();
    });
    /* $("#my-bio-edit").hide();
    $("#bioEdit").click(function() {
        $('#my-bio-edit').find("textarea").val($('#my-bio-content').html());
        $('#my-bio-content').hide();
        $('#my-bio-edit').show();
        return false;
    }); */
    $('#addExpSubmit').click(function(){
        var title = $('#job-title').val();
        var content = $('#job-content').val();
        var startMonth = $('#startDateMonth :selected').val();
        var endMonth = $('#endDateMonth :selected').val();
        var startYear = $('#startYear').val();
        var endYear = $('#endYear').val();
        var isChecked = $('#current').is(':checked');
        var condition = true;
        if (title.length < 1){
            condition = false;
            alert('Please fill in your job title');
        }
        if (content.length < 1){
            condition = false;
            alert('Please fill in your job content');
        }
        if (startMonth == '' || startYear == '' || Number(startYear).toString() == 'NaN' || startYear.length != 4 || startYear > 3000 && startYear < 9999){
            condition = false;
            alert('Please be sure that the start date is completely filled and correct.');
        }
        if (isChecked){
            endYear = 9999;
            endMonth = 9;
        }else{
            if (endMonth == '' || endYear == '' || Number(endYear).toString() == 'NaN' || endYear.length != 4 || endYear > 3000 && endYear < 9999){
                condition = false;
                alert('Please be sure that the end date is completely filled and correct.');
            }
            if (endYear < startYear){
                condition = false;
                alert('Please be sure the start date is not after the end date.')
            }
        }
        // var jobTag = $('#job-tag').val();
        if (condition){
            $.ajax('/user/addExperience', {
            data: {
                title: title,
                content: content,
                startMonth: startMonth,
                startYear: startYear,
                endMonth: endMonth,
                endYear: endYear
            },
            type: 'POST',
            dataType: 'json',
            success: function(data){
                //alert("Your experience has been added successfully.")
                window.location.reload();
            },
            error: function(xhr){
                //alert('Sorry your experience was not saved');
                window.location.reload();
            }
            });
        }
    });

(function localFileVideoPlayerInit(win) {
    var URL = win.URL || win.webkitURL,
        displayMessage = (function displayMessageInit() {
            var node = document.querySelector('#message');

            return function displayMessage(message, isError) {
                node.innerHTML = message;
                node.className = isError ? 'error' : 'info';
            };
        }()),
        playSelectedFile = function playSelectedFileInit(event) {
            var file = this.files[0];

            var type = file.type;

            var videoNode = document.querySelector('#videoPreview');

            var canPlay = videoNode.canPlayType(type);

            canPlay = (canPlay === '' ? 'no' : canPlay);

            var message = 'Can play type "' + type + '": ' + canPlay;

            var isError = canPlay === 'no';

            displayMessage(message, isError);

            if (isError) {
                return;
            }

            var fileURL = URL.createObjectURL(file);

            videoNode.src = fileURL;
        },
        inputNode = document.querySelector('#upload-select-video');
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
    if (!URL) {
        displayMessage('Your browser is not ' + 
           '<a href="http://caniuse.com/bloburls">supported</a>!', true);
        
        return;
    }                
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
    inputNode.addEventListener('change', playSelectedFile, false);
}(window));


$('#editExperienceForm').click(function(){
        var title = $('#editExpTitle').val();
        var content = $('#editExpContent').val();
        var startMonth = $('#editStartDateMonth :selected').val();
        var endMonth = $('#editEndDateMonth :selected').val();
        var startYear = $('#editStartYear').val();
        var endYear = $('#editEndYear').val();
        var isChecked = $('#editCurrent').is(':checked');
        var condition = true;
        if (title.length < 1){
            condition = false;
            alert('Please fill in your job title');
        }
        if (content.length < 1){
            condition = false;
            alert('Please fill in your job content');
        }
        if (startMonth == ''){
            condition = false;
            alert('Please specify a start Month');
        }
        if (startYear == '' || Number(startYear).toString() == 'NaN' || startYear.length != 4){
            condition = false;
            alert('Please input a proper year');
        }
        if (endMonth == ''){
            condition = false;
            alert('Please specify a end month');
        }
        if (isChecked){
            endYear = 9999;
            endMonth = 9;
        }else{
            if (endYear == '' || Number(endYear).toString() == 'NaN' || endYear.length != 4){
                condition = false;
                alert('Please input a proper year');
            }
            if (endYear < startYear){
                condition = false;
                alert('Please be sure the start date is not after the end date.')
            }
        }
        var id = $('#editId').val();
        if (condition){
            $.ajax('/user/editExperience/'+id, {
            data: {
                title: title,
                content: content,
                startMonth: startMonth,
                startYear: startYear,
                endMonth: endMonth,
                endYear: endYear
            },
            type: 'POST',
            dataType: 'json',
            success: function(data){
                if(!data.success){
                    alert('Sorry, your experience was not saved');
                }else{
                    window.location.reload();
                }
            },
            error: function(xhr){
                alert('Sorry your experience was not saved');
            }
            });
        }
    });

    $('#editCurrent').click(function(){
        $('#editEndYear').val('');
    })
    $('#closeAdd').click(function(){
        window.location.reload();
    })
    $('#closeEdit').click(function(){
        window.location.reload();
    })
    /* $("#my-bio-close").click(function() {
            $('#my-bio-edit').hide();
            $('#my-bio-content').show();
            return false;
    });
    $("#my-bio-save").click(function() {
        $.ajax("/settings/updatebio", {
            type: "POST",
            data: {about:$('#my-bio-edit-textarea').val() },
            success: function(data){
                if(data.status){
                    $('#my-bio-content').html($('#my-bio-edit-textarea').val());
                    $('#my-bio-content').show();
                    $('#my-bio-edit').hide();
                    $('#my-bio-edit-textarea').val('');
                }else{
                    alert(data.message);
                }
            },
            dataType: "json"});
    }); */

    $("#user-skills-submit").click(function(){
        $.ajax('/user/updateSkill', {
            data: {
                skills : $("#user-skills-input").val()
            },
            success: function(data){
                //alert("Your skills have been updated successfully.")
                window.location.reload();
            }
            });
    });
    $("#user-languages-submit").click(function(){
        $.ajax('/user/updateLanguage', {
            data: {
                languages : $("#user-languages-input").val()
            },
            success: function(data){
                //alert("Your skills have been updated successfully.")
                window.location.reload();
            }
            });
    });
    $("#addRateForm").click(function(){
        $.ajax('/user/updateRate', {
            data: {
                rate : $("#teachrate").val()
            },
            success: function(data){
                console.log(data);
                window.location.reload();
            }
            });
    });
    $("#editNameForm").click(function(){
        $.ajax('/user/updateName', {
            data: {
                firstname : $("#firstName").val(),
                lastname : $("#lastName").val()
            },
            success: function(data){
                console.log(data);
                window.location.reload();
            }
        });
    });
    $("#deleteButton").click(function(){
        var id = $('#deleteExp').val();
        console.log(id);
        $.ajax('/user/deleteExperience/' + id , {
            data: {
            },
            success: function(data){
                console.log(data);
                window.location.reload();
            }
            });
    });

    $("#reportReviewButton").click(function(){
        var id = $('#reportRev').val();
        $.ajax('/contractreview/reportReview/' + id , {
            data: {
            },
            success: function(data){
                console.log(data);
                window.location.reload();
            }
        });
    });

    $('.current').click(function(){
         $('.endDateMonthYear').toggle();
         $('#present-work').toggle();
         $('#add-present-work').toggle();
    });

    function updateCompleteCount(){
        $.getJSON("/contract/countComplete", function(data){
            if(data.success){
                var el = $("#complete-count");
                if(data.content.count!==el.data("count")){
                    el.data("get", true);
                    el.html("&nbsp;"+data.content.count).data("count", data.content.count);
                }else{
                    el.data("get", false);
                }
            };
            //  console.log(data);
        });
    };

    function updateOngoingCount(){
        $.getJSON("/message/countOngoing", function(data){
            if(data.success){
                var el = $("#ongoing-count");
                if(data.content.count!==el.data("count")){
                    el.data("get", true);
                    el.html("&nbsp;"+data.content.count).data("count", data.content.count);
                }else{
                    el.data("get", false);
                }
            };
            //  console.log(data);
        });
    };
});
function processReview(){
  page++;
  async.waterfall([
    function(cb){
        $.ajax({
            url: '/user/getReview',
            data:{
                page: page,
                userId: userId
            },
            success: function(res) {
                var result = JSON.parse(res);
                if (!result.status){
                    return cb(null, result.total, result.numPage, result.list);
                }else{
                    return cb(null, 0, 0,  []);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                cb(null, 0, 0, []);
             }
    });
    },
    function(total,  numPage,list, cb){
        if (page <= total){
        var self = this;
         $.ajax({
            url: '/js/user/review.js',
            cache: false,
            dataType: 'text',
            success: function(data) {
             var source  = data;
             template  = Handlebars.compile(source);
             list.map(function(oneReview){
                    $('.media-list').append(template({
                       id: oneReview.id,
                       userId: oneReview.userId,
                       avatar: oneReview.avatar,
                       createDate: oneReview.createDate,
                       firstname: oneReview.firstname,
                       lastname: oneReview.lastname,
                       title: oneReview.title,
                       comment: oneReview.comment,
                    }));
             return oneReview;
            });
             if (page == numPage){
                $('.more-review').remove();
             }
             return cb(null, true);
            }, error: function(xhr, ajaxOptions, thrownError) {
            }
        });
        } else if (page == 1 && total == 0){
         $.ajax({
            url: '/js/user/noreview.js',
            cache: false,
            dataType: 'text',
            success: function(data) {
             var source   = data;
             template  = Handlebars.compile(source);
            $('ul.media-list').append(template({}));
            $('.more-review').remove();
            return cb(null, true);
            }, error: function(xhr, ajaxOptions, thrownError) {
            }
        });
        }else{
            $('.more-review').remove();
        }
    }
  ], function(e, result){

  });
}

    function editExperience(id){
        $.getJSON("/user/getExperience/" + id, function(data){
        var title = $('#editExpTitle').val(data.title);
        var content = $('#editExpContent').val(data.content);
        var startMonth = $('#editStartDateMonth').val(data.start_month);
        var endMonth = $('#editEndDateMonth').val(data.end_month);
        var startYear = $('#editStartYear').val(data.start_year);
        var endYear = $('#editEndYear').val(data.end_year);
        $('#editId').val(id);
        if ((data.end_year > 9998)){
        $('#editCurrent').prop('checked', true);
         $('.endDateMonthYear').toggle();
         $('#present-work').toggle();
         $('#add-present-work').toggle();
        }
            //  console.log(data);
        });
    }

    function deleteExperience(id){
        $('#deleteExp').val(id);
    }

    function reportReview(id){
        $('#reportRev').val(id);
    }

    function readURL(input) {
       if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
             $('#cover').attr('src', e.target.result);
             $('#cover').Jcrop({
               aspectRatio: 1,
               onSelect: updateCoords
             });
          }

          function updateCoords(c)
          {
            $('#x').val(c.x);
            $('#y').val(c.y);
            $('#w').val(c.w);
            $('#h').val(c.h);
          };

          function checkCoords()
          {
            if (parseInt($('#w').val())) return true;
            alert('Please select a crop region then press submit.');
            return false;
          };

          reader.readAsDataURL(input.files[0]);
       }
    }

    $("#imgInp").change(function(){
       readURL(this);
    });
    
    function uploadVideo() {
    	var fileInput = document.getElementById ("upload-select-video");
		var msg = '';
		var allowType = ["mp4", "webm", "ogv"];
		if($("#video_title").val()==''){
            msg += "Title should not be empty<br>";		
		}
		if($("#intro_video_id").val() == ''){
			if (fileInput.value == "") {
	            msg += "Please browse for file<br>";
	        }
	        else {
				var fileExt = fileInput.files[0].name.split(".");
		        if(fileInput.files[0].size>10000000) {
		            msg += "File size should not be more than 10MB<br>";        
		        }
		        if(!inArray(fileExt[fileExt.length-1], allowType)) {
		            msg += "File type should be "+ allowType.join(", ") +"<br>";        
		        }
	        }
        }
        if(msg=='') {
        	$("#new-intro-form").submit();
        }
        else {
        	$("#intro_frm_err").html(msg);
        }
    }
    
    function uploadMedia() {
    	var fileInput = document.getElementById ("upload-select-media");
		var msg = '';
		if($("#media_type").val() == 1) {
			var allowType = ["png", "gif", "jpg", "jpeg"];
		}
		else {
			var allowType = ["mp4", "webm", "ogv"];
		}
		if($("#media_title").val()==''){
            msg += "Title should not be empty<br>";		
		}
		if (fileInput.value == "") {
            msg += "Please browse for file<br>";
        }
        else {
			var fileExt = fileInput.files[0].name.split(".");
	        if(fileInput.files[0].size>10000000) {
	            msg += "File size should not be more than 10MB<br>";        
	        }
	        if(!inArray(fileExt[fileExt.length-1], allowType)) {
	            msg += "File type should be "+ allowType.join(", ") +"<br>";        
	        }
        }
        if(msg=='') {
        	$("#new-media-form").submit();
        }
        else {
        	$("#media_frm_err").html(msg);
        }
    }
    function inArray(needle, haystack) {
	    var length = haystack.length;
	    for(var i = 0; i < length; i++) {
	        if(haystack[i] == needle) return true;
	    }
	    return false;
	}
	function mediaDeleteModal(mediaId) {
		$('#mediaDeleteId').val(mediaId);	
		$('#deleteMedia').modal('show');
	}
	function mediaDelete() {
		location.href="/media/doDelete?id="+$('#mediaDeleteId').val();
	}

    function geoLookUp(q, callback){
        var geo = new google.maps.Geocoder();
        geo.geocode(q, callback);
    };

    $("#addPostalForm").click(function(e){
        var f = $("#postal-form");
        var q = $("#postal-code").val().trim();
        //var c = $("#countrySelect", this.id).val().trim();

        if(q){
            geoLookUp({address: q}, function(results, status){
                if (status == google.maps.GeocoderStatus.OK) {
                    console.info(results);
                    var result = results[0];
                    var p = result.geometry.location;
                    var lat = p.lat();
                    var lng = p.lng();
                    console.log(lat, lng);
                    document.getElementById('lat').value = lat;
                    document.getElementById('lng').value = lng;
                    f.submit();
                }
                else{
                    alert("Please check your postal code.");
                }
            });
        }
    });

    function initMap() {

        var myLatLng = {lat: {{userInfo.lat}}, lng: {{userInfo.lng}} };

        var map = new google.maps.Map(document.getElementById('gmap'), {
            zoom: 12,
            center: myLatLng
        });

        // Add circle overlay and bind to marker
        var circle = new google.maps.Circle({
            strokeColor: '#1b92ba',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#1b92ba',
            fillOpacity: 0.35,
            map: map,
            center: myLatLng,
            radius: 2000    // 3000 meters
        });

    }
    google.maps.event.addDomListener(window, 'load', initMap); 
</script>
<!-- Jcrop -->
<script src="/third-party/jcrop/js/jquery.Jcrop.min.js"></script>
<script src="/third-party/jcrop/js/jquery.color.js"></script>
{% endblock %}