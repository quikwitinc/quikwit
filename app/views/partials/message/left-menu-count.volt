<!-- Tags Content -->
<ul class="nav nav-pills nav-stacked">
    <li>
        <a href="{{ url("message/announcements") }}">
           <span class="pull-right" style="color:#1bbae1; width: 25px;">
                <strong>({{ totalUnreadAnnouncementCount }})</strong></span>
                
            <i class="gi gi-bullhorn gi-1.4x" style="color:#1bbae1; padding-right:10px"></i> <strong>Announcements</strong>
        </a>
    </li>
    <li>
        <a href="{{ url("message") }}">
          <span class="pull-right" style="color:#1bbae1; width: 25px;">
                <strong>({{ totalUnreadMessageCount }})</strong></span>
                
            <i class="gi gi-conversation gi-1.4x" style="color:#1bbae1; padding-right:10px"></i> <strong>Conversations</strong>
        </a>
    </li>
    <li>
        <a href="{{ url("message/notifications") }}">
           <span class="pull-right" style="color:#1bbae1; width: 25px;">
                <strong>({{ totalUnreadNotificationCount }})</strong></span>
            
            <i class="gi gi-bell gi-1.4x" style="color:#1bbae1; padding-right:10px"></i> <strong>Notifications</strong>
        </a>
    </li>
</ul>