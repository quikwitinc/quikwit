<!-- Tags Block -->
<div class="block full">
    <!-- Tags Title -->
    <div class="block-title clearfix">
        {# <div class="block-options pull-right">
            <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Add Tag"><i class="fa fa-plus"></i></a>
        </div> #}
        {# <div class="block-options pull-left" style="margin-top: 5px;">
            <div class="input-group">
                <input type="text" name="" class="form-control" placeholder="Search your messages here...">
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div> #}
        <h4>Messages Type</h4>
    </div>
    <!-- END Tags Title -->

    {{ partial("partials/message/left-menu-count") }}
</div>
<!-- END Tags Block -->