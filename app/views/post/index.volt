{% extends "templates/home.volt" %}

{% block title %}
Find a Freelancer | QuikWit
{% endblock %}

{% block content %}
<section class="main-content">
   <div class="container">
      
   	<div class="col-sm-7">
         <!-- START panel-->
         <div class="panel panel-default">
            <div class="panel-heading" style="background:#1b92ba; color:white">QuikPost
               <a href="#" data-toggle="tooltip" title="Collapse Panel" class="pull-right">  
               </a>
            </div>
            <div class="panel-body">
               <div class="response-message" style="font-size: 16px;"></div>
               <br>
               <form id="request-form">
                  <div class="form-group">
                     <label style="font-size:16px">What do you need help with today?</label>  
                     <?php 
                     echo Phalcon\Tag::select(array('tagId', 
                                                   TagsStandard::find(array(
                                                      "status = 1",
                                                      "order" => "name")), 
                                                   "using" => array("id", "name"),
                                                   "class" => "select-chosen form-control",
                                                   "type" => "text",
                                                   "data-role" => "tagsinput",
                                                   "useEmpty" => "true",
                                                   "emptyText" => "Select a category"
                                                   )); ?>                    
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">Please provide a title for this gig.</label>
                     {{ text_field('title', 'class' : 'form-control') }}
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">Please provide the details of this gig.</label>
                     {{ text_area('task', 'class' : 'form-control', 'rows' : '8') }}
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">Do you prefer to pay by the hour or a flat rate?</label>
                     <select class="form-control" name="fixed" style="font-size:14px">
                        <option value="0" >Hourly - pay by the hour</option>
                        <option value="1">Flat rate - pay fixed amount</option>
                     </select>
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">Is this gig nonrecurring or recurring?</label>
                     <select class="form-control" name="multiple" style="font-size:14px">
                        <option value="0">Nonrecurring</option>
                        <option value="1">Recurring</option>
                     </select>
                  </div> 
                  <br> 
                  <div class="form-group">
                     <label style="font-size:16px">What is your budget? Please put the amount here in Canadian Dollars.</label>
                     <div class="input-group m-b">
                        <span class="input-group-addon">$</span>
                        {{ text_field('price', 'class' : 'form-control') }}
                     </div>
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">What is your estimation of the hours needed to complete this gig?</label>
                     <div class="input-group m-b">
                        {{ text_field('hour', 'class' : 'form-control') }}
                        <span class="input-group-addon">Hours</span>
                     </div>
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">What sort of quality are you looking for?</label>
                     <select class="form-control" name="quality" style="font-size:14px">
                        <option value="0">As cheap as possible.</option>
                        <option value="1">A mix of value and experience.</option>
                        <option value="2">Price is not the biggest factor. I want the highest quality.</option>
                     </select>
                  </div>
                  <br>
                  <div class="form-group">
                     <label for="datetime-title" style="font-size:16px">When do you need this task done?</label>
                     <div class='input-group date datetimepicker mb-lg'>
                        <input type='text' name="datetime" id="datetimepickers" class="form-control" readOnly />
                        <span class="input-group-addon">
                           <span class="fa fa-calendar"></span>
                        </span>
                     </div>
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">Where do you need this gig done? Please put in the postal code and city.</label>
                     <div class="col-lg-12" style="margin-bottom: 20px; padding-left: 0px;">
                        <div class="col-lg-5">
                           {{ text_field('postal', 'class' : 'form-control', 'placeholder' : 'Enter postal code') }}
                        </div>
                        <div class="col-lg-7">
                           <?php 
                           echo Phalcon\Tag::select(array('cityId', 
                                                      City::find(), 
                                                      "using" => array("id", "name"),
                                                      "class" => "form-control",
                                                      "type" => "text"
                                                      )); ?> 
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="form-group">
                     <label style="font-size:16px">Do you require the freelancer to travel to your location?</label>
                     <select class="form-control" name="travel" style="font-size:14px">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                     </select>
                  </div> 
                  {#<div class="form-group">
                     <label>Within hours, you'll be introduced to interested and available appliance installation specialists who will send you custom quotes based on your answers to the previous questions.</label>
                  </div>#}
                  {% if user is empty %}
                  <div class="form-group">
                     <label style="font-size:16px">To wrap things up, please put in your details.</label>
                  </div>
                  <div class="form-group">
                     {#<label style="font-size:16px">First Name</label>#}
                     {{ text_field('firstname', "placeholder" : "First Name", "class" : "form-control") }}
                  </div>
                  <div class="form-group">
                     {#<label style="font-size:16px">Last Name</label>#}
                     {{ text_field('lastname', "id" : "rform-lastname", "placeholder" : "Last Name", "class" : "form-control") }}
                  </div>
                  <div class="form-group">
                     {#<label style="font-size:16px">Email</label>#}
                     {{ email_field('email', "id" : "rform-email", "placeholder" : "Email", "class" : "form-control") }}         
                  </div>
                  <div class="form-group">
                     {{ text_field('phone', "id" : "rform-phone", "placeholder" : "Phone Number", "class" : "form-control") }}
                  </div>
                  {% endif %}
                  <br>
                  <div class="form-group">    
                     <div class="checkbox c-checkbox">
                        <label>
                           {{ check_field("confirmation", "id" : "rform-confirmation") }}
                           <span class="fa fa-check"></span>
                           <strong class="text-right"> Yes, I understand and agree to the QuikWit <a href="/terms" target="_blank" class="register-terms"> Terms of Use</a> and <a href="/terms/privacy" target="_blank" class="register-terms">Privacy Policy</strong></a>
                        </label>    
                     </div>
                  </div>
                  <div class="col-xs-12 text-center" style="padding-top: 20px">
                     <button type="button" id="requestButton" class="btn btn-success">Get Quotes!</button>
                  </div>
                  {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }} 
               </form>
            </div>

         </div>
         <!-- END panel-->

      </div>
      <div class="col-sm-5" >
         <h4 style="font-size:18px; color:#1b92ba"><strong>How does QuikPost Work?<strong></h4>
         <br>
         <div style="font-size:16px; font-weight: 500;">
            <strong>If you are a Client</strong>, once the gig is posted, we’ll send the details to interested and available freelancers in your area. You’ll be able to compare quotes and review the profiles of freelancers who respond. Message them on our platform to finalize the details. When you’re ready, hire the right freelancer for the gig.
            <br><br>
            <strong>If you are a Freelancer</strong> ready to work, great news for you! We are currently a <strong>free platform</strong> for now and there is <strong>no hidden cost</strong> for you.  Please <a href="/#signup">Sign Up</a> now to become a freelancer or learn more about <a href="index/#become-a-pro">Becoming a Freelancer</a> or <a href="/pricing">Pricing</a>.
         </div>
         <br>
      </div>
   </div>
</section>
{% endblock %}

{% block script %}
<script type="text/javascript">
   $("#requestButton").click(function(e) {
         /* var confirmation = $('#rform-confirmation').is(':checked');
         console.log(confirmation);
         if(!confirmation) {
            alert('Please agree to our Terms of Service and Privacy Policy.');
            return false;
         } */

         e.preventDefault();
         var form = $("#request-form");
         if(!form.valid()){
            return false;
         }else{
            $('.response-message').html('Posting...');
            window.scrollTo(0, 0);
         }
         
         $.ajax({
            type : 'POST',
            url : '/post/doRequest',
            data : form.serialize(),
            success : function(response) {
               if(response.success == true){
                  $('.response-message').css('color', 'rgb(30, 192, 80)');
               }else{
                  $('.response-message').css('color', 'red');
               }
               $('.response-message').html(response.message); 
               //if (response.success) {
                  //form.fadeOut();
               //} 
               window.scrollTo(0, 0);

               if(response.content){
                  var cityId = response.content[0];
                  var tagId = response.content[1];

                  $.ajax({
                     type: 'POST',
                     url : '/post/sendRequest',
                     data : {
                        cityId : cityId,
                        tagId : tagId
                     }
                  });  
               }  
            }
         });

   });
</script>
{% endblock %}
