{% extends "templates/home.volt" %}

{% block title %}
My Posts | QuikWit
{% endblock %}


{% block head %}
<style>
.clickable-row:hover{
    background: #f1f1f1; 
}
@media screen and (max-width: 768px) {
    .text-align {
        text-align: left;

    }
}
@media screen and (min-width: 769px) {
    .text-align {
        text-align: center;

    }
}
</style>
{% endblock %}

{% block content %}
<section class="main-content">
    <!-- Requests Content -->
    <div class="row">
        <!-- Requests Menu -->
        <!-- Requests List -->
        <div class="container">
            <!-- Requests List Block -->
            <div class="block">
                <!-- Requests List Title -->
                <div class="block-title">
                    <h2>My Posts <strong>({{ totalRequestCount }})</strong></h2>
                    {# <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i></a>
                    </div> #}
                </div>
                <!-- END Requests List Title -->

                <!-- Requests List Content -->
                <div class="row">
                    <div class="text-left col-sm-6" style="margin-bottom: 20px; ">
                        Assign your gigs to freelancers by booking them through their profile page. View created contracts on your <a href="/contract">Manage</a> tab.
                    </div>
                    <div class="text-right col-sm-6" style="margin-bottom: 20px;">
                        <strong>{{ requestOffset }} - {{ requestCountPerPage }}</strong> from <strong>{{ totalRequestCount }}</strong>
                        <div class="btn-group btn-group-sm">
                            <a href="" class="btn btn-sm btn-default"><i class="fa fa-angle-left"></i></a>
                            <a href="" class="btn btn-sm btn-default"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    <!-- Use the first row as a prototype for your column widths -->
                    {% if requests is defined %}
                        {% for request in requests %}
                        <div class="clickable-row col-xs-12" data-href="{{ url('post/viewPosting/' ~ request['req_id']) }}" style="padding: 20px; border-bottom: 1px solid #f1f1f1; cursor: pointer;">
                            {# <td class="text-center" style="width: 30px;">
                                <input type="checkbox"/>
                            </td> #}
                            <div class="col-xs-1 text-align">
                            </div>
                            <div class="hidden-sm hidden-md hidden-lg col-xs-7 text-align">      
                                <div style="font-size: 16px;">{{ request['req_title'] }}</div>
                                <span class="text-muted text-align" style="color: #808080">
                                    {{ request['tag'] }}
                                    {% if request['req_fixed'] == 0 and request['req_price'] != 0 %}
                                    ${{ request['req_price'] }} <p class="text-muted">(per hour)</p>
                                    {% elseif request['req_fixed'] == 1 and request['req_price'] != 0 %}
                                    ${{ request['req_price'] }} <p class="text-muted">(flat rate)</p> 
                                    {% elseif  request['req_fixed'] == 0 and request['req_price'] == 0 %}
                                    Budget not specified <p class="text-muted">(per hour)</p>
                                    {% elseif  request['req_fixed'] == 1 and request['req_price'] == 0 %}
                                    Budget not specified <p class="text-muted">(flat rate)</p>
                                    {% endif %}
                                    {% if request['req_hour'] != 0 %}
                                    Estimated&nbsp;{{ request['req_hour'] }}&nbsp;hrs
                                    {% else %}
                                    Hours not specified
                                    {% endif %}
                                </span>        
                            </div>
                            <div class="col-sm-3 hidden-xs text-align" >
                                <div style="font-size: 16px; padding-top: 10px">{{ request['req_title'] }}</div>
                            </div>
                            <div class="col-sm-3 hidden-xs">
                                <span class="text-muted" style="color: #808080"><div style="padding-top: 10px">{{ request['tag'] }}<br>
                                    {% if request['req_fixed'] == 0 and request['req_price'] != 0 %}
                                    ${{ request['req_price'] }} <p class="text-muted">(per hour)</p>
                                    {% elseif request['req_fixed'] == 1 and request['req_price'] != 0 %}
                                    ${{ request['req_price'] }} <p class="text-muted">(flat rate)</p> 
                                    {% elseif  request['req_fixed'] == 0 and request['req_price'] == 0 %}
                                    Budget not specified <p class="text-muted">(per hour)</p>
                                    {% elseif  request['req_fixed'] == 1 and request['req_price'] == 0 %}
                                    Budget not specified <p class="text-muted">(flat rate)</p>
                                    {% endif %}
                                    <br>
                                    {% if request['req_hour'] != 0 %}
                                    Estimated&nbsp;{{ request['req_hour'] }}&nbsp;hrs
                                    {% else %}
                                    Hours not specified
                                    {% endif %}
                                </div></span>
                            </div>
                            <div class="col-sm-1 hidden-xs">
                                <span class="text-muted" style="color: #808080"><div style="padding-top: 10px">
                                    {% if request['req_status'] == 1 %}
                                    Available
                                    {% elseif request['req_status'] == 2 %}
                                    Assigned
                                    {% elseif request['req_status'] == 3 %}
                                    Cancelled
                                    {% endif %}
                                </div></span>
                            </div>
                            <div class="col-sm-2 text-right pull-right" style="padding: 0px 0px 0px 0px; ">{#<em><?php echo Component\PrettyPrint::prettyPrint($request['req_created']); ?></em>#}
                                <?php   if ($timezone) {
                                            $userTimezone = new DateTimeZone($timezone); 
                                            $newDate = new DateTime();
                                            $newDate->setTimestamp($request['req_created']);
                                            $newDate->setTimeZone($userTimezone);
                                            echo $newDate->format('M jS'); 
                                        }else{
                                            echo date('M jS', $request['req_created']) . " - UTC";
                                        }

                                        ?>
                            </div>
                        </div>
                        {% endfor %}
                        {% if requests is empty %}
                        <hr>
                        <div style="padding-left:40px; padding-top:20px; padding-bottom:40px; color: #1b92ba">
                        There are no posts from you. You can post a new gig by going to <a href="{{ url('post') }}">Post a Gig</a>.
                        </div>
                        {% endif %}
                    {% endif %}
                </div>
                <!-- END Requests List Content -->
            </div>
            <!-- END Requests List Block -->
        </div>
        <!-- END Requests List -->
    </div>
    <!-- END Request Content -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

</section>

{% endblock %}

{% block footer %}
{% endblock %}

{% block script %}
<script>
$(document).ready(function(){
    $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
});
</script>
{% endblock %}
