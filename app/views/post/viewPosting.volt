{% extends "templates/home.volt" %}

{% block head %}
{% endblock %}

{% block title %}
My Post | QuikWit
{% endblock %}

{% block content %}
<section class="main-content">
    <!-- Inbox Content -->
    <div class="row">
        <div class="container">
            <!-- View Request Block -->
            <div class="block full">
                <!-- View Request Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                    </div>
                    <h2><strong>{{ requestTitle|capitalize }}</strong></h2>
                    <h2 class="pull-right"><i class="fa fa-chevron-circle-left text-muted" style="color:white"></i> <a href="javascript:void(0);" style="color:white" onclick="window.history.go(-1)">Go Back</a></h2>
                </div>
                <!-- END View Request Title -->

                {% for request in requests %}
                <!-- Request Meta -->
                <table class="table table-borderless table-vcenter remove-margin">
                    <tbody>
                        <tr>
                            <td>
                            {% if request['req_status'] == 1 %}
                            Gig Status: Available
                            {% elseif request['req_status'] == 2 %}
                            Gig Status: Assigned
                            {% elseif request['req_status'] == 3 %}
                            Gig Status: Cancelled
                            {% endif %}
                            </td>
                            <td class="text-right"><strong><?php if ($timezone) {
                                                                 $userTimezone = new DateTimeZone($timezone); 
                                                                 $newDate = new DateTime();
                                                                 $newDate->setTimestamp($request['req_created']);
                                                                 $newDate->setTimeZone($userTimezone);
                                                                 echo $newDate->format('F jS, Y - g:i a'); 

                                                                 }else{
                                                                     echo date('F jS, Y - g:i a', $request['req_created']) . " UTC";
                                                                 }

                                                                 ?></strong></td>
                        </tr>
                        <tr>
                         <td>
                            {% if request['req_status'] == 1 or request['req_status'] == 2  %}
                              <button class="btn btn-info" data-toggle="modal" data-target="#cancel">Cancel Posting</button>
                            {% endif %}
                        </td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Request Meta -->

                <form>
                <br>
                    {% if request['state'] is defined %}
                    <div class="form-group">
                       <label style="font-size:16px">Last assigned to:</label>
                       <div> 
                          <a href="{{ url('user/profile/' ~ request['req_userid']) }}">
                            {% if request['user_avatar'] is defined %}
                            <img src="{{ url('uploads/avatar/' ~ request['user_avatar']) }}" alt="{{ request['user_firstname']|capitalize ~ ' ' ~ request['user_lastname']|capitalize }}" class="img-thumbnail" style="width: 60px;" />
                            {% else %}
                            <img src="/img/user/blank-avatar" alt="{{ request['user_firstname']|capitalize ~ ' ' ~ request['user_lastname']|capitalize }}" class="img-thumbnail" style="width: 60px;" />
                            {% endif %}
                          </a>
                          <span><a href="{{ url('user/profile/' ~ request['req_userid']) }}">{{ request['user_firstname']|capitalize ~ ' ' ~ request['user_lastname']|capitalize }}</a></span>
                       </div>
                       <hr>       
                    </div>
                    {% endif %}
                    <div class="form-group">
                       <label style="font-size:16px; color: #1b92ba">Category:</label>
                       <div>  
                         {{ requestTag|capitalize }} 
                       </div>   
                       <hr>     
                    </div>
                   <div class="form-group">
                      <label style="font-size:16px; color: #1b92ba">Description of Your Task:</label>
                      <div>  
                        <?php echo htmlspecialchars_decode($request['req_text']); ?>   
                      </div>
                      <hr>        
                   </div>
                   <div class="form-group">
                      <label style="font-size:16px; color: #1b92ba">Budget:</label>
                      <div>
                        {% if request['req_fixed'] == 0 and request['req_price'] != 0 %}
                        ${{ request['req_price'] }} <p class="text-muted">(per hour)</p>
                        {% elseif request['req_fixed'] == 1 and request['req_price'] != 0 %}
                        ${{ request['req_price'] }} <p class="text-muted">(flat rate)</p> 
                        {% elseif  request['req_fixed'] == 0 and request['req_price'] == 0 %}
                        Budget not specified <p class="text-muted">(per hour)</p>
                        {% elseif  request['req_fixed'] == 1 and request['req_price'] == 0 %}
                        Budget not specified <p class="text-muted">(flat rate)</p>
                        {% endif %}
                      </div>
                      <hr>
                   </div> 
                   <div class="form-group">
                      <label style="font-size:16px; color: #1b92ba">This gig is:</label>
                      <div style="font-size:14px">
                          {% if request['req_multiple'] == 0 %}
                          Nonrecurring
                          {% elseif request['req_multiple'] == 1 %}
                          Recurring
                          {% endif %}
                      </div>
                      <hr>
                   </div>                                                                                                                                    
                   <div class="form-group">
                      <label style="font-size:16px; color: #1b92ba">Estimated number of hours required to complete this gig:</label>
                      <div>
                        {% if request['req_hour'] != 0 %}
                        {{ request['req_hour'] }}&nbsp;hrs
                        {% else %}
                        Hours not specified
                        {% endif %}
                      </div>
                      <hr>
                   </div>
                   <div class="form-group">
                      <label style="font-size:16px; color: #1b92ba">Quality preference:</label>
                      <div style="font-size:14px">
                          {% if request['req_quality'] == 0 %}
                          As cheap as possible
                          {% elseif request['req_quality'] == 1 %}
                          A mix of value and experience
                          {% elseif request['req_quality'] == 2 %}
                          Price is not the biggest factor. I want the highest quality.
                          {% endif %}
                      </div>
                      <hr>
                   </div> 
                  {#<div class="form-group">
                      <label for="datetime-title">Date and Time you want this task to be completed on:</label>
                      <div>
                          <?php 
                          if ($timezone) {
                              $userTimezone = new DateTimeZone($timezone); 
                              $newDate = new DateTime();
                              if ($request['req_datetime'] != Null) {
                                  $newDate->setTimestamp(strtotime($request['req_datetime']));
                                  $newDate->setTimeZone($userTimezone);
                                  echo $newDate->format('F jS, Y - g:i a'); 
                              }else{
                                  echo 'Null';
                              }
                          }else if ($request['req_datetime'] != Null) {
                              echo $request['req_datetime'] . " UTC";
                          }else{
                              echo 'Null';
                          }  

                          ?>
                      </div>
                      <hr>
                   </div>
                   #}
                   <div class="form-group">
                      <label style="font-size:16px; color: #1b92ba">Location of this gig:</label>
                      <div>
                        {{ request['city'] }}
                      </div>
                      <hr>
                   </div>
                   <div class="form-group">
                      <label style="font-size:16px; color: #1b92ba">Travel preference:</label>
                      <div>
                        {% if request['req_travel'] == 0 %}
                        You noted that you are not willing to travel
                        {% elseif request['req_travel'] == 1 %}
                        You noted that you are willing to travel
                        {% endif %}
                      </div>
                   </div>
                </form>
                {% endfor %}       
            </div>

            <!-- END View Request Block -->
        </div>
        <!-- END Requests List -->
    </div>
    <!-- END Inbox Content -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

</section>

{% endblock %}

{% block modal %}
<!-- START modal-->
<div id="cancel" tabindex="-1" role="dialog"
    aria-labelledby="acceptLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="denyLabel" class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">Are you sure you want to cancel this posting?</div>
            <div class="modal-footer">
                <button class="btn btn-info" id="btnCancel">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
{% endblock %}

{% block footer %}
{% endblock %}

{% block script %}
<script>
$(document).ready(function(){
  $('#btnCancel').click(function(){
    var reqId = <?php echo $request['req_id']; ?>;
    $.ajax({
        url: '/post/cancelPost',
        type: 'POST',
        data:{
            reqId: reqId
        }, 
        success: function(res) {
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('Sorry, please try again later');
        }
    });
    $('#cancel').modal('hide');
    //window.location.reload();
  });
});
</script>
{% endblock %}
