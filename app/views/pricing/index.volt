{% extends "templates/home.volt" %}

{% block title %}
Pricing | QuikWit
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h1 class="h3 text-center animation-slideUp" style="font-size: 200%; padding-top:20px"><strong>How does QuikWit pricing work?</strong></h1>
        {#<h2 class="h3 text-center animation-slideUp">Fill with your email to receive instructions on resetting your password!</h2>#}
    </div>
</section>
<!-- END Intro -->

<section class="site-content site-section" style="padding-bottom: 200px;">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-8 col-lg-offset-2 site-block" >
              <div style="font-size: 18px; padding-top:25px"><b>QuikWit is FREE for now because we want to make sure that freelancers are actually getting clients before we think about revenue. If you are still curious to know, this is how pricing is going to work...</b></div>
              <br>
              <br>

              <hr>
              <h3 style="font-size: 20px; color:#1b92ba"><strong>For Clients</strong></h3>
              <hr>
              <div style="font-size: 18px; padding-top:25px"><b>For clients, the website is completely free. The only fee clients are responsible for are the fees owed to a freelancer after the completion of a contract.</b></div>
              <br>
              <hr>
              <h3 style="font-size: 20px; color:#1b92ba"><strong>For Freelancers</strong></h3>
              <hr>

              <div style="font-size: 18px; padding-top:25px"><b>Creating and setting up your account is free. However, the website might implement a reasonable pricing model in the future to sustain operation. When this change happens, users will be informed and this Pricing page and Terms of Use will be updated.</b></div>            
            </div>
       </div>
       <!-- END panel-->
   </div>
</section>
{% endblock %}
