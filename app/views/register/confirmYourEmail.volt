{% extends "templates/home.volt" %}

{% block title %}
Please confirm your email | QuikWit
{% endblock %}

{% block head %}
<!-- Countdown -->
{{ javascript_include('third-party/countdown/countdown_v5.0/countdown.js') }}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h1 class="h3 text-center animation-slideUp" style="font-size: 200%"><strong>Processing...</strong></h1>
        {#<h2 class="h3 text-center animation-slideUp">Fill with your email to receive instructions on resetting your password!</h2>#}
    </div>
</section>
<!-- END Intro -->

<section class="site-content site-section" style="padding-bottom: 200px;">
    <div class="container">
        <div class="row" style="margin-top: 40px; padding-bottom: 40px;">           
            <div class="col-sm-8 col-sm-offset-2 site-block">
                <div class="row">
                <div class="col-sm-8" style="text-alignment: center">
                <img style="padding-left: 40px" src="{{ url('img/squirrel650') }}" >
                </div>
                <div class="col-sm-4" style="padding-top: 60px;">
                <script style="color: #1b92ba"> 
                var myCountdown2 = new Countdown({
                                    time: 300, 
                                    width: 150, 
                                    height: 80, 
                                    rangeHi: "minute"    // <- no comma on last item!
                                    });
                </script>
                </div>
                </div>
                <div style="font-size: 20px; padding-top:25px">QuikWit is sad because you haven't yet confirmed your email! Simply <strong>click the link in the confirmation email</strong> we sent you to receive updates when new gigs near you are posted.</div>
                <br>
                <div style="font-size: 20px; padding-top:25px; padding-bottom:50px"><strong style="color: #1b92ba">80% of all email subscribers confirm their email within the first five minutes, the others miss out on the new gigs</strong> – so go to your inbox and click that link now!</div>
                {#<div style="font-size: 20px; padding-top:25px">If you did not get one, you can click below to receive another.</div>#}
             
            	<div class="form-group form-actions text-center">
            	    <div class="col-xs-12">                    
                        <button id="resend" class="btn btn-md btn-success"><b>Resend Email Confirmation</b></button>
            	    </div>
            	</div>	               
            </div>
	    </div>
	    <!-- END panel-->
	</div>
</section>
{% endblock %}

{% block script %}
<!-- Custom script for pages-->
<script>
$(document).ready(function(){
    $("#resend").click(function() {

        var email = "<?php echo $email; ?>";

        $.ajax({
            url: '/user/resend',
            type: 'POST',
            data:{
                email: email
            },
            success: function(res) {
                location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                location.reload();
            }
        });
    });
});
</script>
{% endblock %}