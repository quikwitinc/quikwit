{% block head %}
{% endblock %} 

{% block content %}

<div id="message">{{ flash.output() }}</div>

{{ form("register/doRegister", "method" : "post") }}
<div class="row text-center"><h1>Registration</h1></div>
<div class="row">
	<div class="small-4 large-3 columns">
		<label for="firstname" class="text-center inline">Firstname: </label>
	</div>
	<div class="small-8 large-9 columns">
		{{ text_field('firstname') }}
	</div>
</div>
<div class="row">
	<div class="small-4 large-3 columns">
        	<label for="password" class="text-center inline">Last Name: </label>                    
      	</div>
       	<div class="small-8 large-9 columns">
              	{{ text_field('lastname') }}
       	</div>    
</div>
<div class="row">
	<div class="small-4 large-3 columns">
        	<label for="email" class="text-center inline">Email: </label>                    
      	</div>
       	<div class="small-8 large-9 columns">
              	{{ text_field('email') }}
       	</div>    
</div>
<div class="row">
	<div class="small-4 large-3 columns">
        	<label for="password" class="text-center inline">Password: </label>                    
      	</div>
       	<div class="small-8 large-9 columns">
              	{{ password_field('password') }}
       	</div>    
</div>

<div class="row">
	<div class="small-4 large-3 columns">
        	<label for="rpassword" class="text-center inline">Confirm Pasword: </label>                    
      	</div>
       	<div class="small-8 large-9 columns">
              	{{ password_field('rpassword') }}
       	</div>    
</div>
<div class="row">
	<input type="hidden" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}" />
</div>
<div class="row">
	<div class="small-4 large-3 columns">&nbsp;</div>
   	<div class="small-8 large-9 columns">{{ submit_button("Let's Go", 'class' : 'button') }}</div>    
</div>

{{ end_form() }}
{% endblock %}