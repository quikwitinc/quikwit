{% extends "templates/home.volt" %}

{% block title %}
Do you want to become a Client or Freelancer? | QuikWit
{% endblock %}

{% block head %}
<style>
@media (max-width: 750px) {
    .line {
        border-top: medium solid #1b92ba;
        margin-top: 70px;
        padding-top: 50px;
    }
}
@media (min-width: 751px) {
    .line {
        border-left: medium solid #1b92ba;
    }
}

</style>
{% endblock %}

{% block fbTrack %}
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1634884940101252');
fbq('track', "PageView");

    fbq('track', 'CompleteRegistration'); 

</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1634884940101252&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h2 class="h3 text-center animation-slideUp" style="font-size: 36px"><strong>Let's set up your account...</strong></h2>
    </div>
</section>
<!-- END Intro -->

<section class="site-section site-content" style="padding-bottom: 200px">
	<div class="container" style="margin-top: 80px">
		<div class="row" >
            <div class="col-sm-offset-1 col-sm-10 site-block text-center">
                <div class="col-sm-6 col-sm-offset-0 col-xs-6 col-xs-offset-3">
                    <h2 style="color: #1b92ba">I want to find a freelancer</h2>
                    <br>
                    <p style="font-size:16px">Find the right freelancer for your project.</p>
                    <br>
                    <button type="submit" id="clientBtn" class="btn btn-lg btn-info"><strong>Hire a freelancer</strong></button>
                </div>
                <div class="col-sm-6 col-sm-offset-0 col-xs-6 col-xs-offset-3 line">          
                    <h2 style="color: #1b92ba">I want to find more clients</h2>
                    <br>
                    <p style="font-size:16px">Get listed on our site and get clients.</p>
                    <br>
                    <button type="submit" id="teachBtn" class="btn btn-lg btn-info"><strong>Work as a freelancer</strong></button>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 60px;">
            {#<div class="col-sm-offset-2 col-sm-8 site-block text-center">
                <div class="col-sm-12">
                    <button type="submit" id="resend" class="btn btn-md btn-success">Resend Email Confirmation</button>
                </div>
            </div>#}
        </div>
    </div>
</section>
{% endblock %}

{% block script %}
<script>
$(document).ready(function(){
    $("#teachBtn").click(function() {
        var becomeTeacher = true;
        $.getJSON("/user/updateTeach", {teach: becomeTeacher},
            function(data){
                if (typeof data.status == 'string'){
                    //alert('Your status has been updated successfully.');
                    window.location.replace("../register/tour");
                    //hopscotch.startTour(tour);
                }else{
                    //alert('Sorry, you could not change your teacher status');
                    window.location.reload();
                }
            }
        );
    });

    $("#clientBtn").click(function() {
        var client = 0;
        $.ajax({
            url: '/user/client',
            type: 'POST',
            data:{
                client: client
            },
            success: function(res) {
                window.location.replace("../register/tour");
                //hopscotch.startTour(tour);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                window.location.replace("../register/tour");
            }
        });
    });

    $("#resend").click(function() {

        var email = "<?php echo $email; ?>";

        $.ajax({
            url: '/user/resend',
            type: 'POST',
            data:{
                email: email
            },
            success: function(res) {
                location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                location.reload();
            }
        });
    });      
});
</script>
{% endblock %}