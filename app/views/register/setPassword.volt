{% extends "templates/home.volt" %}

{% block title %}
Set Your Password | QuikWit
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h2 class="text-center animation-slideUp"><strong>Please finish your registration with setting your password</strong></h2>
        
    </div>
</section>
<!-- END Intro -->

<section class="site-content site-section" style="padding-bottom: 200px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 site-block">
                <span class="response-message" style="font-size: 16px;"></span>
            </div>
            <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 site-block">
                <form id="password-reset-form" class="form-horizontal">
	               <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                                {{ password_field('password', "id" : "form-password", "placeholder" : "Password", "class": "form-control input-lg") }}
                            </div>
                        </div>
                    </div>
                   <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                                {{ password_field('cpassword', "id" : "form-password-confirmation", "placeholder" : "Password Confirmation", "class": "form-control input-lg") }}
                            </div>
                        </div>
                    </div>
                    <br>
	            	<div class="form-group form-actions">
	            	    <div class="col-xs-12 text-center">
	            	        <button id="form-submit" type="button" class="btn btn-lg btn-primary"><strong>Set Password</strong></button>
	            	    </div>
	            	</div>
	            </form>
	         </div>
	    </div>
	    <!-- END panel-->
	</div>
</section>
{% endblock %}

{% block script %}
<script>
$("#form-submit").click(function(e) {

    e.preventDefault();
    var form = $("#request-form");
    var password = $('#form-password').val();
    var cPassword = $('#form-password-confirmation').val();

    $.ajax({
        type : 'POST',
        url : '/register/insertPassword',
        data : {
            password: password,
            cPassword: cPassword
        },
        success : function(response) {
            if(response.success == true){
               $('.response-message').css('color', 'green');
            }else{
               $('.response-message').css('color', 'red');
            }
            $('.response-message').html(response.message); 
            //if (response.success) {
              //form.fadeOut();
            //} 
            $.ajax({
                type: 'POST',
                url : '/post/sendRequest'
            }); 
        }
    });

});
</script>
{% endblock %}