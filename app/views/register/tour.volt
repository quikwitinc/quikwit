{% extends "templates/home.volt" %}

{% block title %}
Let's Get You Started | QuikWit
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h1 class="h3 text-center animation-slideUp" style="font-size: 200%"><strong>Great! You're almost done...</strong></h1>
        {#<h2 class="h3 text-center animation-slideUp">Fill with your email to receive instructions on resetting your password!</h2>#}
    </div>
</section>
<!-- END Intro -->

<section class="site-content site-section" style="padding-bottom: 200px;">
    <div class="container">
        <div class="row" style="margin-top: 40px">
            
            <div class="col-sm-6 col-sm-offset-3 col-lg-6  site-block">
                    <div style="font-size: 20px; padding-top:25px"><b>Thank you for signing up! Now let's help you set up your personalized profile. Click the below button to get started.</b></div>
	               <br>
                   <br>
	            	<div class="form-group form-actions">
	            	    <div class="col-xs-12 text-center">
	            	        {#<a class="btn btn-lg btn-primary" style="font-size: 20px" href="/overview"><strong>Next</strong>#}
                        
                            <button type="submit" id="tourBtn" class="btn btn-lg btn-primary"><strong>Next</strong>

                            </button>
                            
                            
	            	    </div>
	            	</div>
	           
	               
            </div>
	    </div>
	    <!-- END panel-->
	</div>
</section>
{% endblock %}

{% block script %}
<!-- Custom script for pages-->

<script>
$(document).ready(function(){
    $("#tourBtn").click(function() {      
        window.location.replace("../overview");                
    });
});
</script>

{{ javascript_include("js/pages.js") }}
    {# {{ javascript_include("js/signin/recover.js") }} #}
	
{% endblock %}