{% extends "templates/home.volt" %}

{% block head %}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background-default">
    <div class="container">
        <h1 class="text-center animation-slideDown"><i class="fa fa-arrow-right"></i> <strong>Write a Review</strong></h1>
        <h2 class="h3 text-center animation-slideUp">Give your valuable feedback for the instructor!</h2>
    </div>
</section>
<!-- END Intro -->

<section class="site-section site-content">
	<div class="container">
		<div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-lg-5 col-lg-offset-3 site-block">
		      <legend>Get Started. <h5>Review a instructor or a location </h5></legend>
		      <div class="col-xs-10">
		      	What would you like to review?
		      	<div class="radio">
		      	   <label>
		      	      <input id="rdb1" type="radio" name="toggler" value="1" checked="">Instructor</label>
		      	</div>
		      	<div class="radio">
		      	   <label>
		      	      <input id="rdb2" type="radio" name="toggler" value="2">Location</label>
		      	</div>
		      	<br>
		      </div>
		      <div class="col-xs-10 toHide" id="blk-1">
		      	Enter instructor name and city
		      	<!-- Search Form -->
		      	<form role="search" method="post" class="search-form form-horizontal">
		      	    <div class="form-group push-bit">
		      	        <div class="col-xs-12">
		      	            <div class="input-group">
		      	                <input type="text" id="" name="" class="form-control" placeholder="eg. John Doe">
		      	                <div class="input-group-btn">
		      	                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
		      	                </div>
		      	            </div>
		      	        </div>
		      	    </div>
		      	</form>
		      	<!-- END Search Form -->
		      	<a href="../review/addTeacher"><button class="btn btn-sm btn-primary">Write Review</button></a>
		      </div>
		      <div class="col-xs-10 toHide" id="blk-2" style="display:none">
		      	{# Enter Location name and city
		      	<!-- Search Form -->
		      	<form>
		      	    <div class="form-group">
		      	        <input type="text" class="form-control">
		      	        <button type="submit" class="btn btn-primary">Search...</button>
		      	    </div>
		      	</form>
		      	<!-- END Search Form --> #}
		      	<h3>Coming soon...</h3>
		      </div>
		    </div>
		</div>
    </div>
</section>
{% endblock %}

{% block script %}
<script>
$(function() {
    $("[name=toggler]").click(function(){
            $('.toHide').hide();
            $("#blk-"+$(this).val()).show('slow');
    });
 });
</script>
{% endblock %}

