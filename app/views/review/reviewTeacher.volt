{% extends "templates/home.volt" %}

{% block head %}
<!-- Star Rating -->
<link rel="stylesheet" href="/third-party/star-rating/jquery.rating.css">
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background-default">
    <div class="container">
        <h1 class="text-center animation-slideDown"> <strong>Add Review</strong></h1>
        <h2 class="h3 text-center animation-slideUp">Give your valuable feedback for the instructor!</h2>
    </div>
</section>
<!-- END Intro -->

<section class="site-section site-content">
	<div class="container">
		<div class="row">
            <div class="col-sm-12 col-sm-offset-3 col-lg-6 col-lg-offset-3 site-block">
		          <div class="form-horizontal">
		              <div class="form-group">
		                  <label class="col-lg-3 control-label">Ratings:</label>
		                  <div class="col-lg-9">
		                      <table width="100%" cellspacing="10"> <tr>
		                        <td valign="top" width="">
		                         <table width="100%">
		                          <tr>
		                           <td valign="top" width="50%">
		                              <div class="Clear">
		                                  Punctuality
		                                  <input class="star required" type="radio" name="punctuality" value="1" title="Worst"/>
		                                  <input class="star" type="radio" name="punctuality" value="2" title="Bad"/>
		                                  <input class="star" type="radio" name="punctuality" value="3" title="OK"/>
		                                  <input class="star" type="radio" name="punctuality" value="4" title="Good"/>
		                                  <input class="star" type="radio" name="punctuality" value="5" title="Best"/>
		                              </div>
		                              <br/>
		                             <div class="Clear">
		                                  Friendliness
		                                  <input class="star required" type="radio" name="friendliness" value="1" title="Worst"/>
		                                  <input class="star" type="radio" name="friendliness" value="2" title="Bad"/>
		                                  <input class="star" type="radio" name="friendliness" value="3" title="OK"/>
		                                  <input class="star" type="radio" name="friendliness" value="4" title="Good"/>
		                                  <input class="star" type="radio" name="friendliness" value="5" title="Best"/>
		                              </div>
		                              <br/>
		                           </td>
		                           <td valign="top" width="50%">
		                             <div class="Clear">
		                                  Knowledge
		                                  <input class="star required" type="radio" name="knowledge" value="1" title="Worst"/>
		                                  <input class="star" type="radio" name="knowledge" value="2" title="Bad"/>
		                                  <input class="star" type="radio" name="knowledge" value="3" title="OK"/>
		                                  <input class="star" type="radio" name="knowledge" value="4" title="Good"/>
		                                  <input class="star" type="radio" name="knowledge" value="5" title="Best"/>
		                             </div>
		                              <br/>
		                             <div class="Clear">
		                                  Teaching
		                                  <input class="star required" type="radio" name="teaching" value="1" title="Worst"/>
		                                  <input class="star" type="radio" name="teaching" value="2" title="Bad"/>
		                                  <input class="star" type="radio" name="teaching" value="3" title="OK"/>
		                                  <input class="star" type="radio" name="teaching" value="4" title="Good"/>
		                                  <input class="star" type="radio" name="teaching" value="5" title="Best"/>
		                              </div>
		                              <br/>
		                           </td>
		                          </tr>
		                         </table>
		                        </td>
		                       </tr>
		                      </table>
		                      <br />
		                      <div class="Clear" style="margin-left: 30%;">
		                           Overall Rating
		                           <input class="star required" type="radio" name="overall" value="1" title="Worst" {split:2} disabled="disabled"/>
		                           <input class="star" type="radio" name="overall" value="2" title="Bad" {split:2} disabled="disabled"/>
		                           <input class="star" type="radio" name="overall" value="3" title="OK" {split:2} disabled="disabled"/>
		                           <input class="star" type="radio" name="overall" value="4" title="Good" {split:2} disabled="disabled"/>
		                           <input class="star" type="radio" name="overall" value="5" title="Best" {split:2} disabled="disabled"/>
		                       </div>
		                  </div>
		              </div>
		              <div class="form-group">
		                  <label class="col-sm-3 control-label">When did you take this lesson?</label>
		                  <div class="col-sm-9">
		                      <select name="lesson_date_month_year" id="lesson_date_month_year" class="form-control" size="1">
		                          <?php
		                          $date = new DateTime();
		                          $interval = new DateInterval('P1M');
		                          echo '<option value="' . $date->format('m') . ',' . $date->format('Y') . '" selected>' .
		                          $date->format('F') . ' ' . $date->format('Y') . '</option>';
		                          for ($i=0; $i < 12 ; $i++) {
		                              $date->sub($interval);
		                              echo '<option value="' . $date->format('m') . ',' . $date->format('Y') . '">' .
		                              $date->format('F') . ' ' . $date->format('Y') . '</option>';
		                          }
		                          ?>
		                      </select>
		                  </div>
		              </div>
		              <div class="form-group">
		                  <label for="review-title" class="col-lg-3 control-label">Title:</label>
		                  <div class="col-lg-9">
		                      <input type="text" class="form-control" id="review-title"
		                          placeholder="[Type Reason Here]" required>
		                  </div>
		              </div>
		              <div class="form-group">
		                  <label for="review-body" class="col-lg-3 control-label">Comment:</label>
		                  <div class="col-lg-9">
		                      <textarea class="form-control" rows="8" id="review-comment"
		                          placeholder="[Type Review Here]" required></textarea>
		                  </div>
		              </div>
		              <div class="form-group">
		                 <div class="col-lg-offset-3 col-lg-9">
		                    <div class="checkbox c-checkbox">
		                       <label>
		                          <input type="checkbox" id="tnc">
		                          <span class="fa fa-check"></span>I certify that this review is based on my own experience and is my genuine opinion of this instructor and that I have no personal or business relationship with this instructor, and have not been offered any incentive or payment originating from the instructor to write this review. I understand that QuikWit has a zero-tolerance policy on fake reviews. <a href="#">Learn more</a></label>
		                    </div>
		                 </div>
		              </div>
		              <div class="form-group">
		              	<input type="hidden" name="review-teacherId" id="review-teacherId" />
		              </div>
		              <div class="form-group form-actions">
			              <div class="col-xs-offset-1 col-xs-6 text-left">
			                  <button class="btn btn-sm btn-primary"> Previous</button>
			              </div>
		                  <div class="col-xs-5 text-right">
		                      <button type="submit" class="btn btn-sm btn-info"> Submit Review</button>
		                  </div>
		              </div>
		          </div>
		      </div>
		      </form>
		    </div>
		</div>
    </div>
</section>
{% endblock %}

{% block script %}
<!-- Star Rating -->
{{ javascript_include("third-party/star-rating/jquery.rating.js") }}
{{ javascript_include("third-party/star-rating/jquery.MetaData.js") }}
{% endblock %}
