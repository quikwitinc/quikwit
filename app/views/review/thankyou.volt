{% extends "templates/home.volt" %}

{% block head %}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background-default">
    <div class="container">
        <h1 class="text-center animation-slideDown"> <strong>Congrats!</strong></h1>
        <h2 class="h3 text-center animation-slideUp">Give your valuable feedback for the instructor!</h2>
    </div>
</section>
<!-- END Intro -->

<section class="site-section site-content">
	<div class="container">
		<div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-lg-6 col-lg-offset-3 site-block">
		    	<h3><strong>Congrats! </strong> The QuikWit Team will review your request to create a new instrcutor profile and post it in a few days.</h3>
		    	<h3>Thank you for contributing to our community.</h3>
		    </div>
		</div>
    </div>
</section>
{% endblock %}