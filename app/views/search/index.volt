{% extends "templates/home.volt" %}

{% block title %}
Search for the best freelancers around you | QuikWit
{% endblock %}

{% block head %}
<!-- Star Rating -->
{{ stylesheet_link("third-party/star-rating/jquery.rating.css") }}

{% endblock %}

{% block content %}

<section class="main-content">
  <div class="col-lg-offset-1 col-lg-4 col-md-5 col-sm-12">
    {# {% if user is empty %}
    <form role="search" method="get" class="search-form form-horizontal" action="search/index" id="search-validation">
        <div class="form-group push-bit">
            <div class="col-xs-12">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Enter a skill" id="searchSkill"
                           value="<?php if (isset($_GET['q'])) { 
                                            echo $_GET['q']; 
                                        } else {
                                            echo "";
                                        } ?>">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary" style="padding-top: 3px; height: 34px;"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    {% endif %} #}
    {# <!-- Basic Form Elements Block -->
    <div class="panel panel-default">
        <!-- Basic Form Elements Title -->
        <div class="panel-heading"><strong>Filter</strong>
          <a href="#" data-perform="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
             <em class="fa fa-minus"></em>
          </a>
        </div>
        <!-- END Form Elements Title -->

        <div class="panel-body">
          <!-- Basic Form Elements Content -->
          <form action="page_forms_general.html" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return false;">
              {# <div class="form-group">
                  <label class="col-md-3 control-label" for="example-select">Availability</label>
                  <div class="col-md-8">
                    <div class="col-md-10">
                      <select id="example-select" name="example-select" class="form-control" size="1">
                        <option value="0">From</option>
                        <option value="1">12 am</option>
                        <option value="2">1 am</option>
                        <option value="3">2 am</option>
                        <option value="4">3 am</option>
                        <option value="5">4 am</option>
                        <option value="6">5 am</option>
                        <option value="7">6 am</option>
                        <option value="8">7 am</option>
                        <option value="9">8 am</option>
                        <option value="10">9 am</option>
                        <option value="11">10 am</option>
                        <option value="12">11 am</option>
                        <option value="13">12 pm</option>
                        <option value="14">1 pm</option>
                        <option value="15">2 pm</option>
                        <option value="16">3 pm</option>
                        <option value="17">4 pm</option>
                        <option value="18">5 pm</option>
                        <option value="19">6 pm</option>
                        <option value="20">7 pm</option>
                        <option value="21">8 pm</option>
                        <option value="22">9 pm</option>
                        <option value="23">10 pm</option>
                        <option value="24">11 pm</option>
                        <option value="25">12 pm</option>
                      </select>
                    </div>
                    <div class="col-md-10">
                      <select id="example-select" name="example-select" class="form-control" size="1">
                        <option value="0">To</option>
                        <option value="1">12 am</option>
                        <option value="2">1 am</option>
                        <option value="3">2 am</option>
                        <option value="4">3 am</option>
                        <option value="5">4 am</option>
                        <option value="6">5 am</option>
                        <option value="7">6 am</option>
                        <option value="8">7 am</option>
                        <option value="9">8 am</option>
                        <option value="10">9 am</option>
                        <option value="11">10 am</option>
                        <option value="12">11 am</option>
                        <option value="13">12 pm</option>
                        <option value="14">1 pm</option>
                        <option value="15">2 pm</option>
                        <option value="16">3 pm</option>
                        <option value="17">4 pm</option>
                        <option value="18">5 pm</option>
                        <option value="19">6 pm</option>
                        <option value="20">7 pm</option>
                        <option value="21">8 pm</option>
                        <option value="22">9 pm</option>
                        <option value="23">10 pm</option>
                        <option value="24">11 pm</option>
                        <option value="25">12 pm</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-offset-3 col-md-9 col-sm-6">
                      <div class="checkbox">
                          <label for="example-checkbox1">
                              <input type="checkbox" id="mon" name="monday " value="mon"> Monday
                          </label>
                      </div>
                      <div class="checkbox">
                          <label for="example-checkbox2">
                              <input type="checkbox" id="tue" name="tuesday" value="tue"> Tuesday
                          </label>
                      </div>
                      <div class="checkbox">
                          <label for="example-checkbox3">
                              <input type="checkbox" id="wed" name="wednesday" value="wed"> Wednesday
                          </label>
                      </div>
                      <div class="checkbox">
                          <label for="example-checkbox1">
                              <input type="checkbox" id="thurds" name="thursday" value="thurs"> Thursday
                          </label>
                      </div>
                      <div class="checkbox">
                          <label for="example-checkbox2">
                              <input type="checkbox" id="fri" name="friday" value="fri"> Friday
                          </label>
                      </div>
                      <div class="checkbox">
                          <label for="example-checkbox3">
                              <input type="checkbox" id="sat" name="saturday" value="sat"> Saturday
                          </label>
                      </div>
                      <div class="checkbox">
                          <label for="example-checkbox3">
                              <input type="checkbox" id="sun" name="sunday" value="sun"> Sunday
                          </label>
                      </div>
                  </div>
              </div> 
              <div class="form-group">
                  <label class="col-md-3 control-label" for="example-text-input">Price Range ($CDN)</label>
                  <div class="col-md-9">
                    <div class="col-md-6">
                        Min
                        <input type="text" id="minPrice" name="minimum-price" class="form-control" placeholder="">
                    </div>
                    <div class="col-md-6">
                        Max
                        <input type="text" id="maxPrice" name="maximum-price" class="form-control" placeholder="">
                    </div>
                  </div>
                  <button class="btn btn-block btn-primary" style="width:80%;margin-left:auto;margin-right:auto;" id="filterSearch">Search</button>
              </div>
          </form>
          <!-- END Basic Form Elements Content -->
        </div>
      </div> #} 
    <!-- Basic Form Elements Block -->
    <div class="panel panel-default">
      <!-- Basic Form Elements Title -->
      <div class="panel-heading"><strong>Show Location</strong>
        <a href="#" data-perform="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
           <em class="fa fa-minus"></em>
        </a>
      </div>
      <!-- END Form Elements Title -->

      <div class="panel-wrapper">
        <!-- Basic Form Elements Content -->
        <form method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return false;">
            <div class="form-group" style="margin: 15px;">
               <label class="col-sm-3 control-label">Location</label>
               <div class="col-sm-9">
                  <div class="radio">
                    <label style="font-size: 18px;">
                       <input name="location" id="map-current-location" type="radio" value="option1"> Current Location
                    </label>
                  </div>
                  <div class="radio">
                    <label style="font-size: 18px;">
                       <input name="location" id="map-user-address" type="radio" value="option2">My Address
                    </label>
                  </div>
                  <div class="radio">
                    <label style="font-size: 18px;">
                        <input name="location" id="map-postal-code" type="radio" value="option3">
                        <input id="map-user-postalCode" type="text" class="form-control" placeholder="Postal Code">
                    </label>
                  </div>
                  <div class="checkbox">
                    <label style="font-size: 18px;">
                       <input id="map-radius-option" type="checkbox" value="option4">Radius(KM)
                       <input class="form-control" type="text" id="map-radius" value="10">
                    </label>
                  </div>
               </div>
               <div class="col-xs-12" style="margin-top: 15px;">
                  <button class="btn btn-block btn-primary" style="width:80%;margin-left:auto;margin-right:auto;" id="locationSearch">Show</button>
               </div>
            </div>
            <div id="maps-canvas" data-toggle="gmap" class="gmap" style="width:100%; margin-top:10px;"></div>
            {# <div class="form-group col-md-12 col-sm-6">
                <label class="col-md-3 control-label" for="example-text-input">Distance</label>
                <div class="col-md-9">
                  <input type="text" id="example-text-input" name="example-text-input" class="form-control" placeholder="L2Z4B6">
                  <span class="help-block">Postal Code</span>
                </div>
                <div class="col-md-offset-3 col-md-9">
                  <input type="text" id="example-text-input" name="example-text-input" class="form-control" placeholder="10">
                  <span class="help-block">Km</span>
                </div>
            </div> #}
            {# <div class="form-group form-actions">
                <div class="col-md-9 col-md-offset-3 col-xs-12">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Filter</button>
                    <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                </div>
            </div> #}
        </form>
        <!-- END Basic Form Elements Content -->
      </div>
    </div>
    </div>
  <div class="col-lg-6 col-md-7 col-xs-12">
    <div class="row" style="padding-left: 15px;">
        <div class="form-inline push-bit clearfix">
            <select id="results-sort" name="results-sort" class="form-control" size="1">
                <option value="0" disabled selected>SORT BY</option>
                <option value="/search?q={{q}}&sort=relevance">Relevance</option>
                <option value="/search?q={{q}}&sort=distance">Distance (Closest to Farthest)</option>
                <option value="/search?q={{q}}&sort=rating">Rating (Highest to Lowest)</option>
                <option value="/search?q={{q}}&sort=lowprice">Price (Lowest to Highest)</option>
                <option value="/search?q={{q}}&sort=highprice">Price (Highest to Lowest)</option>
            </select>
        </div>
    </div>
  	<?php if(count($users) > 0): ?>
  	<?php foreach($users as $idx => $user): ?>
       <!-- START widget-->
       <div class="widget" data-id="<?php echo $user['id'];?>">
          <div class="widget-simple">
             <div class="row">
                <div class="text-center col-xs-4" style="padding: 0px;">
                   {% set profile = "user/profile/" ~ user['id'] %}
                   <a href="{{ url(profile) }}" target="_blank">
                   {% if user['avatar'] is defined %}
                   <img src="{{ url("uploads/avatar/" ~ user['avatar']) }}"  style="width: 100px; max-width: 100%" alt="Image" class="img-thumbnail img-thumbnail">
                   {% else %} 
                   <img src="/img/user/blank-avatar" style="width: 100px; max-width: 100%" alt="Image" class="img-thumbnail img-thumbnail">
                   {% endif %}
                   <br>
                   <h5><strong><?php echo ucwords($user['firstname'].", ".$user['lastname']); ?></strong></h5>
                   </a>
                </div>
                <div class="col-sm-4 col-xs-8">
                  <table style="border-collapse:separate; border-spacing: 1em; font-weight: 600; margin-top: -15px;">
                    <tr>
                      <td style="padding-left: 20px;">
                      {{user['contract_complete']}} contracts completed
                      </td>
                    </tr>
                    <tr>
                      <td style="padding-left: 20px">             
                      <?php if($user['usr_rate'] == 0) {
                          echo 'Please contact me for rate.';
                      }else{
                          echo '$' .  number_format($user['usr_rate'], 2);
                      } ?>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding-top:10px; padding-left: 20px">
                        <input class="star {split:2}" type="radio" name="{{ url(profile) }}" value="0.5" disabled="disabled"  <?php echo ($user['rating'] <=0.5  && $user['rating'] != null && $user['rating'] > 0)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="{{ url(profile) }}" value="1.0" disabled="disabled" <?php echo ($user['rating'] <1.5 && $user['rating'] >= 1)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="{{ url(profile) }}" value="1.5" disabled="disabled" <?php echo ($user['rating'] <2 && $user['rating'] >=1.5 )?' checked="checked"':'';?> />
                        <input class="star {split:2}" type="radio" name="{{ url(profile) }}" value="2.0" disabled="disabled" <?php echo ($user['rating'] < 2.5  && $user['rating'] >= 2)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="{{ url(profile) }}" value="2.5" disabled="disabled"  <?php echo ($user['rating'] < 3  && $user['rating'] >= 2.5)?' checked="checked"':'';?>/> 

                        <input class="star {split:2}" type="radio" name="{{ url(profile) }}" value="3.0" disabled="disabled"  <?php echo ($user['rating'] <3.5  && $user['rating'] >= 3)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="{{ url(profile) }}" value="3.5" disabled="disabled" <?php echo ($user['rating'] <4 && $user['rating'] >= 3.5)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="{{ url(profile) }}" value="4.0" disabled="disabled" <?php echo ($user['rating'] <4.5 && $user['rating'] >= 4)?' checked="checked"':'';?> />
                        <input class="star {split:2}" type="radio" name="{{ url(profile) }}" value="4.5" disabled="disabled" <?php echo ($user['rating'] < 5  && $user['rating'] >= 4.5)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="{{ url(profile) }}" value="5.0" disabled="disabled"  <?php echo ($user['rating'] >=5)?' checked="checked"':'';?>/>
                        {# <input class="star required" type="radio" name="{{ url(profile) }}" value="1" title="Worst" disabled="disabled" <?php echo ($user['rating'] <2 && $user['rating'] != null && $user['rating'] > 0)?' checked="checked"':'';?>/>
                       <input class="star" type="radio" name="{{ url(profile) }}" value="2" title="Bad" disabled="disabled" <?php echo ($user['rating'] <3 && $user['rating'] >= 2)?' checked="checked"':'';?>/>
                       <input class="star" type="radio" name="{{ url(profile) }}" value="3" title="OK" disabled="disabled" <?php echo ($user['rating'] <4 && $user['rating'] >= 3)?' checked="checked"':'';?>/>
                       <input class="star" type="radio" name="{{ url(profile) }}" value="4" title="Good" disabled="disabled" <?php echo ($user['rating'] <5 && $user['rating'] >= 4)?' checked="checked"':'';?>/>
                       <input class="star" type="radio" name="{{ url(profile) }}" value="5" title="Best" disabled="disabled" <?php echo ($user['rating'] >= 5)?' checked="checked"':'';?>/> #}
                        &nbsp;(<?php echo $user['num_rating']; ?>)
                      </td>
                    </tr>
                    {# <tr>
                      <td>
                        <h5>Share | </h5>
                      </td>
                      <td>
                        <a href=""><em class="fa fa-facebook-official"></em></a>
                        <a href=""><em class="fa fa-linkedin"></em></a>
                      </td>
                    </tr> #}
                  </table>
                </div>
                <!-- START button group-->
                <div class="col-sm-4 hidden-xs">
                   <ul role="menu" style="list-style-type: none; margin-top: -10px;">
                      <li><a href="{{ url(profile) }}"><h5 style="padding-bottom: 5px"><strong>View Profile</strong></h5></a></li>
                      <li><a href="javascript:void(0)" data-toggle="modal" data-target="{% if loginUser is defined %}#message{% else %}#register{% endif %}" class="searchMessage" data-id="<?php echo $user['usr_id']; ?>"><h5 style="padding-bottom: 5px"><strong>Message</strong></h5></a>
                      </li>
                      <li><a href="javascript:void(0)" {% if loginUser is defined %}onclick="showBookingModal('<?php echo $user['usr_id'];?>', '<?php echo $user['usr_rate']; ?>');"{% else %}data-toggle="modal" data-target="#register"{% endif %}><h5 style="padding-bottom: 5px"><strong>Book</strong></h5></a></li>
                      <li><a href="javascript:void(0)" data-toggle="modal" data-target="{% if loginUser is defined %}#report{% else %}#register{% endif %}" class="report-user" data-id="<?php echo $user['usr_id'];?>"><h5><strong>Report</strong></h5></a>
                      </li>
                   </ul>
                </div>
                <!-- END button group-->
                <div class="col-xs-12 text-center">
                  <a href="{{ url(profile) }}" class="label label-info"> 
                    {{user['skills']}} 
                  </a> 
                  &nbsp;
                  {{user['moreskills']}}
                </div>
             </div>
          </div>
       </div>
       <!-- END widget-->
       <?php endforeach; ?>
       <?php else: ?>
          <h4 style="margin-top: 50px">Sorry, currently there are no freelancers offering this service. Please check again later~</h4>
          <br>
          <h4>If it is a new category, you can suggest it <a href="{{ url('suggest') }}">here</a> to us. Thanks!</h4>
       <?php endif; ?>
       <?php if ($numPages > 1){ ?>
       <button class="btn btn-block" id="loadMore">Load More</button>
       <?php } ?>
  </div>
</section>

{% endblock %}

{% block modal %}
<!-- START modal-->
<div id="message" tabindex="-1" role="dialog" aria-labelledby="messageLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="messageLabel" class="modal-title">Send Message</h4>
            </div>
            <div class="modal-body">
            {% if email_verified == 1 %}
               <!-- <form class="form-horizontal"> -->
               {{ form("message/send", "id" : "message-form", "method" : "post", "class" : "form-horizontal") }}
                  <div class="form-group">
                    {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }}
                    {{ hidden_field("message[redirect]", "value" : "http://www.quikwit.co/user/profile/", "id":"searchRedirect", "data-value":"http://www.quikwit.com/user/profile/") }}
                    {{ hidden_field("message[uid]", "value" : 0, "id":"searchUserId") }}
                     <label for = "message-title" class = "col-lg-2 control-label">Title:</label>
                     <div class = "col-lg-10">
                       <!-- <input type = "text" class = "form-control" id = "message-title" placeholder = "[Type Title Here]"> -->
                      {{ text_field('message[title]', "id" : "message-title", "placeholder" : "Give your message a title", "class": "form-control") }}
                     </div>
                  </div>
                  <div class = "form-group">
                     <label for = "message-body" class = "col-lg-2 control-label">Message:</label>
                     <div class = "col-lg-10">
                     <!-- <textarea class="form-control" rows="8" placeholder="[Type Message Here]"></textarea> -->
                     {{ text_area('message[message]', "id" : "message-title", "placeholder" : "Type your message here", "class": "form-control", "rows": 8) }}
                     </div>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-info">Send</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
              </div>
              {{ end_form() }}
              {% else %}
              For security reasons, we require users to confirm their email before messaging other users. Do you want to confirm your email now?
              </div>
              <div class="modal-footer">
                  <button id="resend" class="btn btn-info">Yes</button>
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
              </div>
              {% endif %}
         </div>
      </div>
   </div>
<!-- END modal-->
<!-- START modal-->
   <div id="booking" tabindex="-1" role="dialog" aria-labelledby="bookingLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="bookingLabel" class="modal-title">Contract Booking</h4>
            </div>
            <div class="modal-body">
            {% if email_verified == 1 %}
              <!-- START panel-->
                {{ form("contract/create", "id" : "contract-form", "method" : "post", "class" : "form-horizontal") }}
                   <div class="form-group container">
                      <p>Please message the freelancer first regarding availability before booking a contract.</p>
                   </div>
                   <div class="form-group">
                   {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }}
                    {{ hidden_field("contract[uid]", "value" : userId, "id":"bookId") }}
                      <label for="datetime-title" class="col-lg-3 control-label">Start Date & Time</label>
                      <div class="col-lg-8">
                        <div class='input-group date datetimepicker mb-lg' id='datetimepickers'>
  		                    <input type='text' class="form-control" name="contract[datetime]" readOnly/>
  		                    <span class="input-group-addon">
  		                        <span class="fa fa-calendar"></span>
  		                    </span>
		                    </div>
                    </div>
                   </div>
                   <div class="form-group">
                      <label for="price-title" class="col-lg-3 control-label">Price of contract ($ CDN)</label>
                      <div class="col-lg-8">
                        <div class="input-group m-b">
                           <span class="input-group-addon">$</span>
                           {{ text_field('price', "id" : "contract-price", "class" : "form-control") }}
                        </div>
                      </div>
                   </div>
                   <div class = "form-group">
                      <label for = "sub-skill" class = "col-lg-3 control-label">Skill:</label>
                       <div class = "col-lg-8">

                       		<span id="bookingSkillList"></span>

                       </div>
                    </div>
                    <div class = "form-group">
                      <label for = "req-id" class = "col-lg-3 control-label">Post:</label>
                       <div class = "col-lg-8">
                          <?php 
                          $requestModel = new Requests();
                          $requestsObj = $requestModel->getUserRequests($loginUser->id);
                          $requests = (array_column($requestsObj, 'req_title', 'req_id'));
                          //error_log("<pre>request".print_r($requests,true)."</pre>"); 
                          echo Phalcon\Tag::selectStatic(array('contract[reqId]', 
                                                            $requests, 
                                                            "using" => array("req_id", "req_title"),
                                                            "class" => "select-chosen",
                                                            "type" => "text",
                                                            "id" => "myRequests",
                                                            "data-role" => "tagsinput",
                                                            "useEmpty" => true,
                                                            "emptyText" => "Ignore if this is not related to any post"
                                                            )); ?>
                       </div>
                    </div>
              <!-- END panel-->
            </div>
            <div class="modal-footer">
               <input type="submit" value="Book" class="btn btn-info">
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
            {{ end_form() }}
            {% else %}
            For security reasons, we require users to confirm their email before booking a contract. Do you want to confirm your email now?
            </div>
            <div class="modal-footer">
                <button id="resend" data-dismiss="modal" class="btn btn-info">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
            {% endif %}
         </div>
      </div>
   </div>
<!-- END modal-->
<!-- START modal-->
<div id="report" tabindex="-1" role="dialog"
    aria-labelledby="reportLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="reportLabel" class="modal-title">Report User</h4>
            </div>
            <div class="modal-body">
                {{ form("abuse/create", "class" : "form-horizontal", "method" :
                "post") }}
                <div class="form-group">
                    <label for="report-title" class="col-lg-2 control-label">Description:</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="report-title"   name="abuse[reason]"
                            placeholder="Please tell us the nature of the issue">
                    </div>
                </div>
                <div class="form-group">
                    <label for="report-body" class="col-lg-2 control-label">More Information:</label>
                    <div class="col-lg-10">
                        <textarea class="form-control" id="report-body" rows="8"  name="abuse[extra]"
                            placeholder="Please provide us with more information so we can help"></textarea>
                    </div>
                </div>
                {{ hidden_field("abuse[uid]", "value" : 0, "id": "abuse_user_id") }}
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-info">Submit</button>
                {{ end_form() }}
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
<!-- START modal-->
<div id="register" tabindex="-1" role="dialog"
    aria-labelledby="reportLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="reportLabel" class="modal-title">New to our site? Please register below to continue.</h4>
            </div>
            <div class="modal-body">
              <!-- Sign Up Form -->
              {{ form("register/doRegister", "id" : "form-validation", "method" : "post", "class" : "form-horizontal") }}
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-user"></i></span>
                              {{ text_field('firstname', "placeholder" : "First Name", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-user"></i></span>
                              {{ text_field('lastname', "id" : "rform-lastname", "placeholder" : "Last Name", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                              {{ email_field('email', "id" : "rform-email", "placeholder" : "Email", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-lock"></i></span>
                              {{ password_field('password', "id" : "rform-password", "placeholder" : "Password", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-lock"></i></span>
                              {{ password_field('rpassword', "id" : "rform-repassword", "placeholder" : "Retype Password", "class" : "form-control input-lg") }}
                              
                          </div>
                      </div>
                  </div>
                  </br>
                  <div class="form-group form-actions">
                      <div class="checkbox c-checkbox">
                           <label class="text-center">  
                              {{ check_field("confirmation", "id" : "rform-confirmation") }}
                              <span class="fa fa-check"></span>
                              <strong class="text-right"> Yes, I understand and agree to the QuikWit <a href="/terms" target="_blank" class="register-terms"> Terms of Use</a> and <a href="/terms/privacy" target="_blank" class="register-terms">Privacy Policy</a></strong>
                          </label>         
                      </div>
                      <br>
                      <div class="col-xs-12 text-center">
                          <button type="submit" class="btn btn-lg btn-primary" id="signUpButton"><strong>Sign Up</strong></button>
                      </div>
                  </div>
                  {# <input id="rform-csrf" type="hidden" name="<?php echo $this->security->getTokenKey() ?>" value="<?php echo $this->security->getTokenKey() ?>"/> 
                  <input id="rform-csrf" type="hidden" name="{{ tokenKey }}" value="{{ token }}"/> #}
                  {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }} 
              {{ end_form() }}
              <!-- END Sign Up Form -->
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
{% endblock %}

{% block footer %}{% endblock %}

{% block script %}
<!-- Star Rating -->
{{ javascript_include("third-party/star-rating/jquery.rating.js") }}
{{ javascript_include("third-party/star-rating/jquery.MetaData.js") }}
{{ javascript_include('third-party/async/async.js') }}
{{ javascript_include("third-party/proui/backend/js/pages/formsValidation.js") }}

<script>
$(document).ready(function(){
  Handlebars.registerHelper('equal', function(lvalue, rvalue, options) {
      if (arguments.length < 3)
          throw new Error("Handlebars Helper equal needs 2 parameters");
      if( lvalue!=rvalue ) {
          return options.inverse(this);
      } else {
          return options.fn(this);
      }
  });
  
  $(function(){ FormsValidation.init(); });
  $(document).on('click', '.searchMessage', function(ev){
    var id = $(this).attr('data-id');
    $('#searchRedirect').val('/user/profile/' + id);
    $('#searchUserId').val(id);
  });
  /* $(document).on('click', '.searchBook', function(ev){
    var id = $(this).attr('data-id');
    var price = $(this).attr('data-price');
    $('#bookId').val(id);
    $('#contract-price').val(Number(price).toFixed(2));
  }); */
  $(document).on('click', '.report-user', function(ev){
    var id = $(this).attr('data-id');
    $('#abuse_user_id').val(id);
  });
  $("#resend").click(function() {

      var email = "<?php echo $email; ?>";

      $.ajax({
          url: '/user/resend',
          type: 'POST',
          data:{
              email: email
          },
          success: function(res) {
              location.reload();
          },
          error: function(xhr, ajaxOptions, thrownError) {
              location.reload();
          }
      });
  });
  var map;
  var mapObject = {
      cPos: null,
      users: [],
      circle: null,
      teacherSelected: false,
      currentLocationSelected: false,
      myAddressSelected: false,
      radiusSelected: false,
      radiusSize: 3000,
      myAddress: null,
      postalCode: null,
      postalCodeSelected: false,
      currentLocation: null,
      page: {{page}},
      total: {{total}},
      numPages: {{numPages}},
      q: '{{q}}',
      initialize: function (lat, lng) {
        var mapOptions = {
          center: { lat:lat, lng: lng},
          zoom:10
        };
        map = new google.maps.Map(document.getElementById('maps-canvas'),
            mapOptions);
      },
      placeInitialMarkers: function(){
        <?php foreach ($users as $idx => $user){
            $userObject = json_encode($user);
            if($user['lat'] && $user['lng']): ?>
             this.placeMarker(new google.maps.LatLng(<?php echo $user['lat']; ?>, <?php echo $user['lng']; ?>),"<?php echo ucwords($user['firstname']) . ' ' . ucwords($user['lastname']);?>", <?php echo $userObject; ?>);
        <?php endif; ?>
        <?php } ?>
      },
      placeMarker: function(LatLng, title, user){
         var marker = new google.maps.Marker({
              position: LatLng,
              map: map,
              title: title
          });
         user.marker = marker;
         user.title = title;
         user.tooFar = false;
         user.show = true;
         this.users.push(user);
      },
      filterPrice: function(min, max){
        this.users = this.users.map(function(oneUser){
            oneUser.show = false;
          if (oneUser.usr_rate >= min && oneUser.usr_rate <= max){
            oneUser.show = true;
          }
          if (oneUser.marker){
           oneUser.marker.setMap(null);
           oneUser.marker = null;
          }
          return oneUser;
         //map ends
         });
         //function ends
      },
      returnFilter: function(){
         this.users = this.users.map(function(oneUser){
          oneUser.show = true;
          oneUser.marker = new google.maps.Marker({
            position: new google.maps.LatLng(oneUser.lat, oneUser.lng),
            map: map,
            title: oneUser.title
           });
          return oneUser;
         //map ends
         });
         //function ends
      },
      mapCurrentPosition: function(mapCircle, radius){
       var self = this;
       if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position){
        self.cPos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          console.log("Latitude: " + position.coords.latitude +
          "<br>Longitude: " + position.coords.longitude);
          map.setCenter(self.cPos);
          self.currentLocation = new google.maps.Marker({
              position: self.cPos,
              map: map,
              title: "You are here!",
              label: "C"
              //animation: google.maps.Animation.BOUNCE
          }); 
          self.infowindow = new google.maps.InfoWindow({
              content: "You are here!"
          });
          console.log(self.currentLocation);
          self.currentLocation.addListener('click', function() {
            self.infowindow.open(map, self.currentLocation);
          }); 
          if (mapCircle){
            self.drawRadiusNow(radius);
          }
        }, function(err){
          alert('Sorry, your browser could not retrieve your current location.');
          if (self.currentLocation != null){
            self.currentLocation.setMap(null);
            self.currentLocation = null;
          }
          console.log(err)
        },{
          enableHighAccuracy: true,
          timeout: 5000,
          maximumAge: 0
        });
       } else {
          alert('Sorry, your browser is preventing us from getting your current location.');
          if (self.currentLocation != null){
            self.currentLocation.setMap(null);
            self.currentLocation = null;
          }
          self.currentLocationSelected = false;
          self.radiusSelected = false;
          $("#map-radius-option").attr('checked',self.radiusSelected);
          console.error("Geolocation is not supported by this browser.");
       }
      },
      mapAddress: function(mapCircle, radius){
         var self = this;
         var lat = "{{lat}}";
         var lng = "{{lng}}";
         if (lat != 0 && lng != 0 && this.myAddress == undefined){
          self.cPos = new google.maps.LatLng(lat, lng);
            map.setCenter(self.cPos);
            self.myAddress = new google.maps.Marker({
                position: self.cPos,
                map: map,
                title:"You live Here!",
                label:"A"
            });
            self.infowindow = new google.maps.InfoWindow({
                content: "You live here!"
            });
            self.myAddress.addListener('click', function() {
                self.infowindow.open(map, self.myAddress);
            });
            if (mapCircle){
              self.drawRadiusNow(radius);
            } 
         }else{
            alert('Sorry, we could not plot your address on the map.');
         }
      },
      mapPostalCode: function(postalLat, postalLng, mapCircle, radius){
         var self = this;
         var lat = postalLat;
         var lng = postalLng;
         if (lat != 0 && lng != 0 && this.postalCode == undefined){
          self.cPos = new google.maps.LatLng(lat, lng);
            map.setCenter(self.cPos);
            self.postalCode = new google.maps.Marker({
                position: self.cPos,
                map: map,
                title:"Location is here!",
                label:"P"
            });
            self.infowindow = new google.maps.InfoWindow({
                content: "Location is here!"
            });
            self.postalCode.addListener('click', function() {
              self.infowindow.open(map, self.postalCode);
            });
            if (mapCircle){
              self.drawRadiusNow(radius);
            } 
         }else{
            alert('Sorry, we could not plot your postal code on the map.');
         }
      },
      filterUserTooFar: function(r){
        var self = this;
        this.users = this.users.map(function(oneUser){
          if (oneUser.marker != null){
            var distance = self.distanceGPS(self.cPos.lat(),self.cPos.lng(), parseFloat(oneUser.lat), parseFloat(oneUser.lng));
            if (distance > r){
              oneUser.tooFar = true;
              oneUser.marker.setMap(null);
              oneUser.marker = null;
            }
          }
        return oneUser;
       });
      },
      returnUserTooFar: function(){
        this.users = this.users.map(function(oneUser){
            if (oneUser.tooFar == true){
              oneUser.tooFar = false;
              oneUser.marker = new google.maps.Marker({
               position: new google.maps.LatLng(oneUser.lat, oneUser.lng),
               map: map,
               title: oneUser.title
              });
            }
        return oneUser;
       });
      },
      drawRadius: function(r){
         this.circle = null;
         var circleOpts = {
          strokeColor: '#1b92ba',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#1b92ba',
          fillOpacity: 0.35,
          map: map,
          center: this.cPos,
          radius: r
         };
         this.circle = new google.maps.Circle(circleOpts);
         this.filterUserTooFar(r);
      },
      drawRadiusNow : function(r){
        this.drawRadius(r * 1000);
      },
      removeCircle: function(){
        if (this.circle != null){
          this.circle.setMap(null);
          this.circle = null;
          this.returnUserTooFar();
        }
      },
      distanceGPS: function(lat1, lon1, lat2, lon2) {
        //calculate distance using metres
       var radlat1 = Math.PI * lat1 / 180;
       var radlat2 = Math.PI * lat2 / 180;
       var radlon1 = Math.PI * lon1 / 180;
       var radlon2 = Math.PI * lon2 / 180;
       var theta = lon1 - lon2;
       var radtheta = Math.PI * theta / 180;
       var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
       dist = Math.acos(dist);
       dist = dist * 180 / Math.PI;
       dist = dist * 60 * 1.1515;
       dist = dist * 1.609344 * 1000;
       return dist.toFixed(3);
      },
      clearMap: function(){
        if (this.circle != null){
        this.circle.setMap(null);
        this.circle = null;
        }
        var self = this;
        if (this.currentLocation != null){
          this.currentLocation.setMap(null);
          this.currentLocation = null;
        }
        if (this.myAddress != null){
          this.myAddress.setMap(null);
          this.myAddress = null;
        }
        if (this.postalCode != null){
          this.postalCode.setMap(null);
          this.postalCode = null;
        }
        this.users = this.users.map(function(oneUser){
          if (oneUser.marker){
          oneUser.marker.setMap(null);
          oneUser.marker = null;
          }
          return oneUser;
        });
      },
      resetRows: function(){
        this.users = this.users.map(function(oneUser){
          if (!oneUser.show){
            oneUser.show = true;
            oneUser.tooFar = false;
             $('.widget[data-id="' + oneUser.usr_id + '"]').show();
          }
          return oneUser;
        });
      },
      hideRows: function(){
        this.users = this.users.map(function(oneUser){
          if (!oneUser.show){
             $('.widget[data-id="' + oneUser.usr_id + '"]').hide();
          }else{
            $('.widget[data-id="' + oneUser.usr_id + '"]').show();
          }
          return oneUser;
        });
      },
      drawMap: function(currentLocation,userAddress,postalCode){
        this.users = this.users.map(function(oneUser){
          if (oneUser.show){
          oneUser.marker = new google.maps.Marker({
            position: new google.maps.LatLng(oneUser.lat, oneUser.lng),
            map: map,
            title: oneUser.title
           });
          oneUser.infowindow = new google.maps.InfoWindow({
              content: '<a href="/user/profile/' + oneUser.usr_id + '">' + oneUser.title + '</a>'
          });
          oneUser.marker.addListener('click', function() {
            oneUser.infowindow.open(map, oneUser.marker);
          }); 
          }
          return oneUser;
         //map ends
         });
      }
    };

    mapObject.initialize(43.653226000000000000, -79.383184299999980000);
    mapObject.placeInitialMarkers();
    mapObject.drawMap();
    $('#loadMore').click(function(){
      <?php $loginUserArray = (array) $loginUser;
          if($loginUserArray) {
            $loginUserJSON = json_encode($loginUserArray);
          }else{
            $loginUserJSON = false;
          } 
          
          ?>
      var loginUser_data = '<?php echo $loginUserJSON; ?>';
      if (!loginUser_data) {
        $('#register').modal('show');
      } else {
        processSearch(mapObject);
      }
    });
    $('#filterSearch').click(function(){
      $('.widget-simple').remove();
      mapObject.page = 0;
      processSearch(mapObject);
    });
    //click events
    $('input[type="radio"]').click(function(){
     if($(this).attr("value")=="local"){
        $("#map").show();
        $("#tabs").removeClass('col-md-10');
        $("#tabs").addClass('col-md-5');
     }
     if($(this).attr("value")=="global"){
        $("#map").hide();
        $("#tabs").removeClass('col-md-5');
        $("#tabs").addClass('col-md-10');
     }
   });
   //toggle teachers
   /* $("#teachers").click(function(){
    mapObject.teacherSelected = !mapObject.teacherSelected;
    if( mapObject.teacherSelected == true){
     mapObject.filterTeachers();
    } else{
     mapObject.returnTeachers();
    }
   }); */
   //toggle current location
   $("#map-current-location").click(function(){
     mapObject.currentLocationSelected = !mapObject.currentLocationSelected;
   });
   //toggle your address
   $("#map-user-address").click(function(){
    mapObject.myAddressSelected = !mapObject.myAddressSelected;
   });
   //toggle postal code
   $("#map-user-postalCode").click(function(){
    mapObject.postalCodeSelected = !mapObject.postalCodeSelected;
   });
   //radius changes
   $("#map-radius").change(function(){
      var radius = Number($("#map-radius").val());
      if (radius.toString() == 'NaN'){
        alert('Radius must be numeric');
        return false;
      }
   });
   $("#results-sort").bind('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
    });
   //Toggle to draw the circle
   $("#map-radius-option").click(function(){
     mapObject.radiusSelected = !mapObject.radiusSelected;
   });

    $(document).on('click', '.report-user', function(ev){
      var id = $(this).attr('data-id');
      $('#abuse_user_id').val(id);
    });

     $('#locationSearch').click(function(){
      //var min = $('#minPrice').val();
      //var max = $('#maxPrice').val();
      var currentLocation = $('#map-current-location').is(':checked');
      var userAddress = $('#map-user-address').is(':checked');
      var postalCode = $('#map-postal-code').is(':checked');
      var radius = $('#map-radius-option').is(':checked');
      var mapRadius = Number($('#map-radius').val());
      mapObject.resetRows();
      /* if (min && max){
        mapObject.filterPrice(min,max);
      } */
      mapObject.clearMap();
      if (currentLocation){
        mapObject.mapCurrentPosition((radius && mapRadius > 0), mapRadius);
      }
      if (userAddress){
        mapObject.mapAddress();
        if (radius && mapRadius > 0){
          mapObject.drawRadiusNow(mapRadius);
        }
      }
      if (postalCode){
        var q = $("#map-user-postalCode").val().trim();
        if(q){
          geoLookUp({address: q}, function(results, status){
            if (status == google.maps.GeocoderStatus.OK) {
              console.info(results);
              var result = results[0];
              var p = result.geometry.location;
              var lat = p.lat();
              var lng = p.lng();
              console.log(lat, lng);
              mapObject.mapPostalCode(lat, lng);
            }
            if (radius && mapRadius > 0){
              mapObject.drawRadiusNow(mapRadius);
            }
          });
        }
      }
      mapObject.hideRows();
      mapObject.drawMap(currentLocation,userAddress,postalCode);
   });
});

function geoLookUp(q, callback){
  var geo = new google.maps.Geocoder();
  geo.geocode(q, callback);
};

function resetFilter(mapObject){
  mapObject.resetRows();
  mapObject.clearMap();
  $('input[type="radio"]').attr('checked', false);
  $('input[type="checkbox"]').attr('checked', false);
  $('#minPrice').val('');
  $('#maxPrice').val('');
  $('#map-radius').val('3');
}

function processSearch(mapObject){
  mapObject.page = mapObject.page + 1;
  //mapObject.minPrice = $('#minPrice').val();
  //mapObject.maxPrice = $('#maxPrice').val();
  if($('#map-radius-option').is(":checked")){
  	mapObject.mapRadiusOption = 1;
  }
  else {
  	mapObject.mapRadiusOption = 0;
  }
  mapObject.mapRadius = $('#map-radius').val();
 
  async.waterfall([
    function(cb){
      $.ajax({
          url: '/search/search?q=' + mapObject.q + '&sort=<?php echo $sort;?>' +  '&page=' + mapObject.page,
          //data: 'minPrice='+mapObject.minPrice+'&maxPrice='+mapObject.maxPrice+'&mapRadiusOption='+mapObject.mapRadiusOption+'&mapRadius='+mapObject.mapRadius,
          data: '&mapRadiusOption='+mapObject.mapRadiusOption+'&mapRadius='+mapObject.mapRadius,
          success: function(res) {
              var result = JSON.parse(res);
              if (result.status){              	  
               	$('#loadMore').show();
                  return cb(null, result.list);
              }else{
                  return cb(null, []);
              }
          },
          error: function(xhr, ajaxOptions, thrownError) {
              cb(null, []);
           }
      });
    },
    function(list, cb){
        if (list.length < 1){
            $('#loadMore').hide();
        }
        var self = this;
         $.ajax({
            url: '/js/user/search.js',
            cache: false,
            dataType: 'text',
            success: function(data) {
             var source   = data;
             template  = Handlebars.compile(source);
             var queue = [];
             list.map(function(oneUser){
                    var rate = Number(oneUser.usr_rate);
                    $('#loadMore').before(template({
                       userId: oneUser.usr_id,
                       avatar: oneUser.avatar,
                       usr_rate: rate.toFixed(2),
                       skills: oneUser.skills,
                       moreskills: oneUser.moreskills,
                       name: capitalizeFirstLetter(oneUser.firstname + ' ' + oneUser.lastname),
                       contract_complete: oneUser.contract_complete,
                       check1: (oneUser.rating <= 0.5 && oneUser.rating != null  && oneUser.rating > 0)?' checked="checked"':'',
                       check2: (oneUser.rating < 1.5 && oneUser.rating >= 1)?' checked="checked"':'',
                       check3: (oneUser.rating < 2 && oneUser.rating >= 1.5)?' checked="checked"':'',
                       check4: (oneUser.rating < 2.5 && oneUser.rating >= 2)?' checked="checked"':'',
                       check5: (oneUser.rating < 3 && oneUser.rating >= 2.5)?' checked="checked"':'',
                       check6: (oneUser.rating < 3.5 && oneUser.rating >= 3)?' checked="checked"':'',
                       check7: (oneUser.rating < 4 && oneUser.rating >= 3.5)?' checked="checked"':'',
                       check8: (oneUser.rating < 4.5 && oneUser.rating >= 4)?' checked="checked"':'',
                       check9: (oneUser.rating < 5 && oneUser.rating >= 4.5)?' checked="checked"':'',
                       check10:(oneUser.rating >= 5 )?' checked="checked"':'',
                       num_rating: oneUser.num_rating
                    }));
                    $('.star[data-id="' + oneUser.usr_id + '"]').rating();
                    if (oneUser.lat != 0 && oneUser.lat != null &&
                        oneUser.lat != 0 && oneUser.lat != null){
                      mapObject.placeMarker(
                        new google.maps.LatLng(oneUser.lat, oneUser.lng),
                          capitalizeFirstLetter(oneUser.firstname + ' ' + oneUser.lastname),
                          oneUser
                      );
                      mapObject.drawMap();
                    }
                    return oneUser;
            });

            //resetFilter(mapObject);
            if (mapObject.page == mapObject.numPages){
               $('#loadMore').hide();
            }
             return cb(null, true);
            }, 
            error: function(xhr, ajaxOptions, thrownError) {
              return cb(null, true);
            }
        });

    }
  ], function(e, result){

  });
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function showBookingModal(usr_id, usr_price) {
	$('#contract-price').val(usr_price);
	$('#bookId').val(usr_id);
	var postData = 'usr_id='+usr_id;	
	$.ajax({
      url: '/search/userskilltags',
      type: "POST",
	    data: postData,
      dataType: 'text',
      success: function(data) {         	
      	$('#bookingSkillList').html(data);
      	$("#usersTags").chosen();
      	$('.chosen-container.chosen-container-single').css('width', '360px');
      	$('#booking').modal('show');
      }, 
      	error: function(xhr, ajaxOptions, thrownError) {
      }
  });
}
</script>
{% endblock %}
