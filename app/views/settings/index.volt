{% extends "templates/home.volt" %}

{% block title %}
Account Settings | QuikWit
{% endblock %}

{% block head %}
   <link rel="stylesheet" href="/third-party/jcrop/css/jquery.Jcrop.min.css">
{% endblock %}

{% block content %}
 <section class="main-content">
    {# <div class="panel panel-default col-sm-6 col-xs-12" style="margin-right: 30px;">
       <div class="panel-body">
       <legend> Account Settings</legend>
          <form id="qw-usettings-form" method="post" action="settings/changeSettings" class="form-horizontal">
             <fieldset>
                <div class="form-group">
                   <label class="col-sm-3 control-label">First Name</label>
                   <div class="col-sm-8">
                        {{  text_field("firstname", "placeholder" : "First name", "class": "form-control", "value" : profile.usr_firstname) }}
                      <!-- <input type="text" placeholder="" class="form-control"> -->
                   </div>
                </div>
             </fieldset>
             <fieldset>
                <div class="form-group">
                   <label class="col-sm-3 control-label">Last Name</label>
                   <div class="col-sm-8">
                   {{  text_field("lastname", "placeholder" : "Last name", "class": "form-control", "value" : profile.usr_lastname) }}
                    <!--  <input type="text" placeholder="placeholder" class="form-control"> -->
                   </div>
                </div>
             </fieldset>
             <fieldset>
                <div class="form-group">
                   <label class="col-sm-3 control-label">Email</label>
                   <div class="col-sm-8">
                   {{   email_field("email", "placeholder" : "Email", "class": "form-control", "value" : profile.usr_email) }}
                    <!--  <input type="email" placeholder="placeholder" class="form-control"> -->
                   </div>
                </div>
             </fieldset>
             <fieldset>
                <div class="form-group">
                   <label class="col-sm-3 control-label">Languages</label>
                   <div class="col-sm-8">
                      <select name="language" class="select-chosen form-control" data-placeholder = "Languages">
                        <option>{{ profile.usr_lang }}</option>
                        <option value="English">English</option>
                        <option value="Afrikaans">Afrikaans</option>
                        <option value="Arabic">Arabic</option>
                        <option value="Armenian">Armenian</option>
                        <option value="Belarusian">Belarusian</option>
                        <option value="Bulgarian">Bulgarian</option>
                        <option value="Catalan">Catalan</option>
                        <option value="Mandarin">Mandarin</option>
                        <option value="Cantonese">Cantonese</option>
                        <option value="Croatian">Croatian</option>
                        <option value="Czech">Czech</option>
                        <option value="Dutch">Danish</option>
                        <option value="Dutch">Dutch</option>
                        <option value="Esperanto">Esperanto</option>
                        <option value="Estonian">Estonian</option>
                        <option value="Filipino">Filipino</option>
                        <option value="Finnish">Finnish</option>
                        <option value="French">French</option>
                        <option value="German">German</option>
                        <option value="Greek">Greek</option>
                        <option value="Hebrew">Hebrew</option>
                        <option value="Hindi">Hindi</option>
                        <option value="Hungarian">Hungarian</option>
                        <option value="Icelandic">Icelandic</option>
                        <option value="Indonesian">Indonesian</option>
                        <option value="Italian">Italian</option>
                        <option value="Japanese">Japanese</option>
                        <option value="Korean">Korean</option>
                        <option value="Latvian">Latvian</option>
                        <option value="Lithuanian">Lithuanian</option>
                        <option value="Norwegian">Norwegian</option>
                        <option value="Persian">Persian</option>
                        <option value="Polish">Polish</option>
                        <option value="Portuguese">Portuguese</option>
                        <option value="Romanian">Romanian</option>
                        <option value="Russian">Russian</option>
                        <option value="Serbian">Serbian</option>
                        <option value="Slovak">Slovak</option>
                        <option value="Slovenian">Slovenian</option>
                        <option value="Spanish">Spanish</option>
                        <option value="Swahili">Swahili</option>
                        <option value="Swedish">Swedish</option>
                        <option value="Thai">Thai</option>
                        <option value="Turkish">Turkish</option>
                        <option value="Ukrainian">Ukrainian</option>
                        <option value="Vietnamese">Vietnamese</option>
                      </select>
                   </div>
                </div>
             </fieldset>
             {# <fieldset>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="country">Country</label>
                    <div class="col-sm-8">
                        <select id="countrySelect" name="profile[country]" class="select-chosen form-control" data-placeholder="Country">
                            <option>{{ profile.usr_country }}</option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                            <option value="Canada">Canada</option>
                            <option value="United States">United States</option>
                            <option value="Afghanistan">Afghanistan</option>
                            <option value="Aland Islands">Aland Islands</option>
                            <option value="Albania">Albania</option>
                            <option value="Algeria">Algeria</option>
                            <option value="American Samoa">American Samoa</option>
                            <option value="Andorra">Andorra</option>
                            <option value="Angola">Angola</option>
                            <option value="Anguilla">Anguilla</option>
                            <option value="Antarctica">Antarctica</option>
                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                            <option value="Argentina">Argentina</option>
                            <option value="Armenia">Armenia</option>
                            <option value="Aruba">Aruba</option>
                            <option value="Australia">Australia</option>
                            <option value="Austria">Austria</option>
                            <option value="Azerbaijan">Azerbaijan</option>
                            <option value="Bahamas">Bahamas</option>
                            <option value="Bahrain">Bahrain</option>
                            <option value="Bangladesh">Bangladesh</option>
                            <option value="Barbados">Barbados</option>
                            <option value="Belarus">Belarus</option>
                            <option value="Belgium">Belgium</option>
                            <option value="Belize">Belize</option>
                            <option value="Benin">Benin</option>
                            <option value="Bermuda">Bermuda</option>
                            <option value="Bhutan">Bhutan</option>
                            <option value="Bolivia, Plurinational State of">Bolivia, Plurinational State of</option>
                            <option value="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option>
                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                            <option value="Botswana">Botswana</option>
                            <option value="Bouvet Island">Bouvet Island</option>
                            <option value="Brazil">Brazil</option>
                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                            <option value="Bulgaria">Bulgaria</option>
                            <option value="Burkina Faso">Burkina Faso</option>
                            <option value="Burundi">Burundi</option>
                            <option value="Cambodia">Cambodia</option>
                            <option value="Cameroon">Cameroon</option>
                            <option value="Cape Verde">Cape Verde</option>
                            <option value="Cayman Islands">Cayman Islands</option>
                            <option value="Central African Republic">Central African Republic</option>
                            <option value="Chad">Chad</option>
                            <option value="Chile">Chile</option>
                            <option value="China">China</option>
                            <option value="Christmas Island">Christmas Island</option>
                            <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                            <option value="Colombia">Colombia</option>
                            <option value="Comoros">Comoros</option>
                            <option value="Congo">Congo</option>
                            <option value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option>
                            <option value="Cook Islands">Cook Islands</option>
                            <option value="Costa Rica">Costa Rica</option>
                            <option value="Ivory Coast">Ivory Coast</option>
                            <option value="Croatia">Croatia</option>
                            <option value="Cuba">Cuba</option>
                            <option value="Curaçao">Curaçao</option>
                            <option value="Cyprus">Cyprus</option>
                            <option value="Czech Republic">Czech Republic</option>
                            <option value="Denmark">Denmark</option>
                            <option value="Djibouti">Djibouti</option>
                            <option value="Dominica">Dominica</option>
                            <option value="Dominican Republic">Dominican Republic</option>
                            <option value="Ecuador">Ecuador</option>
                            <option value="Egypt">Egypt</option>
                            <option value="El Salvador">El Salvador</option>
                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                            <option value="Eritrea">Eritrea</option>
                            <option value="Estonia">Estonia</option>
                            <option value="Ethiopia">Ethiopia</option>
                            <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                            <option value="Faroe Islands">Faroe Islands</option>
                            <option value="Fiji">Fiji</option>
                            <option value="Finland">Finland</option>
                            <option value="France">France</option>
                            <option value="French Guiana">French Guiana</option>
                            <option value="French Polynesia">French Polynesia</option>
                            <option value="French Southern Territories">French Southern Territories</option>
                            <option value="Gabon">Gabon</option>
                            <option value="Gambia">Gambia</option>
                            <option value="Georgia">Georgia</option>
                            <option value="Germany">Germany</option>
                            <option value="Ghana">Ghana</option>
                            <option value="Gibraltar">Gibraltar</option>
                            <option value="Greece">Greece</option>
                            <option value="Greenland">Greenland</option>
                            <option value="Grenada">Grenada</option>
                            <option value="Guadeloupe">Guadeloupe</option>
                            <option value="Guam">Guam</option>
                            <option value="Guatemala">Guatemala</option>
                            <option value="Guernsey">Guernsey</option>
                            <option value="Guinea">Guinea</option>
                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                            <option value="Guyana">Guyana</option>
                            <option value="Haiti">Haiti</option>
                            <option value="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>
                            <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                            <option value="Honduras">Honduras</option>
                            <option value="Hong Kong">Hong Kong</option>
                            <option value="Hungary">Hungary</option>
                            <option value="Iceland">Iceland</option>
                            <option value="India">India</option>
                            <option value="Indonesia">Indonesia</option>
                            <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                            <option value="Iraq">Iraq</option>
                            <option value="Ireland">Ireland</option>
                            <option value="Isle of Man">Isle of Man</option>
                            <option value="Israel">Israel</option>
                            <option value="Italy">Italy</option>
                            <option value="Jamaica">Jamaica</option>
                            <option value="Japan">Japan</option>
                            <option value="Jersey">Jersey</option>
                            <option value="Jordan">Jordan</option>
                            <option value="Kazakhstan">Kazakhstan</option>
                            <option value="Kenya">Kenya</option>
                            <option value="Kiribati">Kiribati</option>
                            <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                            <option value="Korea, Republic of">Korea, Republic of</option>
                            <option value="Kuwait">Kuwait</option>
                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                            <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                            <option value="Latvia">Latvia</option>
                            <option value="Lebanon">Lebanon</option>
                            <option value="Lesotho">Lesotho</option>
                            <option value="Liberia">Liberia</option>
                            <option value="Libya">Libya</option>
                            <option value="Liechtenstein">Liechtenstein</option>
                            <option value="Lithuania">Lithuania</option>
                            <option value="Luxembourg">Luxembourg</option>
                            <option value="Macao">Macao</option>
                            <option value="Macdonia, the former Yugoslav Republic of">Macedonia, the former Yugoslav Republic of</option>
                            <option value="Madagascar">Madagascar</option>
                            <option value="Malawi">Malawi</option>
                            <option value="Malaysia">Malaysia</option>
                            <option value="Maldives">Maldives</option>
                            <option value="Mali">Mali</option>
                            <option value="Malta">Malta</option>
                            <option value="Marshall Islands">Marshall Islands</option>
                            <option value="Martinique">Martinique</option>
                            <option value="Mauritania">Mauritania</option>
                            <option value="Mauritius">Mauritius</option>
                            <option value="Mayotte">Mayotte</option>
                            <option value="Mexico">Mexico</option>
                            <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                            <option value="Moldova, Republic of">Moldova, Republic of</option>
                            <option value="Manaco">Monaco</option>
                            <option value="Mongolia">Mongolia</option>
                            <option value="Montenegro">Montenegro</option>
                            <option value="Montserrat">Montserrat</option>
                            <option value="Morocco">Morocco</option>
                            <option value="Mozambique">Mozambique</option>
                            <option value="Myanmar">Myanmar</option>
                            <option value="Namibia">Namibia</option>
                            <option value="Nauru">Nauru</option>
                            <option value="Nepal">Nepal</option>
                            <option value="Netherlands">Netherlands</option>
                            <option value="New Caledonia">New Caledonia</option>
                            <option value="New Zealand">New Zealand</option>
                            <option value="Nicaragua">Nicaragua</option>
                            <option value="Niger">Niger</option>
                            <option value="Nigeria">Nigeria</option>
                            <option value="Niue">Niue</option>
                            <option value="Norfolk Island">Norfolk Island</option>
                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                            <option value="Norway">Norway</option>
                            <option value="Oman">Oman</option>
                            <option value="Pakistan">Pakistan</option>
                            <option value="Palau">Palau</option>
                            <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                            <option value="Panama">Panama</option>
                            <option value="Papua New Guinea">Papua New Guinea</option>
                            <option value="Paraguay">Paraguay</option>
                            <option value="Peru">Peru</option>
                            <option value="Philippines">Philippines</option>
                            <option value="Pitcairn">Pitcairn</option>
                            <option value="Poland">Poland</option>
                            <option value="Portugal">Portugal</option>
                            <option value="Puerto Rico">Puerto Rico</option>
                            <option value="Qatar">Qatar</option>
                            <option value="Réunion">Réunion</option>
                            <option value="Romania">Romania</option>
                            <option value="Russian Federation">Russian Federation</option>
                            <option value="Rwanda">Rwanda</option>
                            <option value="Saint-Barthélemy">Saint-Barthélemy</option>
                            <option value="Saint Helena, Ascension and Tristan da Cunha">Saint Helena, Ascension and Tristan da Cunha</option>
                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                            <option value="Saint Lucia">Saint Lucia</option>
                            <option value="Saint Martin (French part)">Saint Martin (French part)</option>
                            <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                            <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                            <option value="Samoa">Samoa</option>
                            <option value="San Marino">San Marino</option>
                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                            <option value="Saudi Arabia">Saudi Arabia</option>
                            <option value="Senegal">Senegal</option>
                            <option value="Serbia">Serbia</option>
                            <option value="Seychelles">Seychelles</option>
                            <option value="Sierra Leone">Sierra Leone</option>
                            <option value="Singapore">Singapore</option>
                            <option value="Sint Maarten (Dutch part)">Sint Maarten (Dutch part)</option>
                            <option value="Slovakia">Slovakia</option>
                            <option value="Slovenia">Slovenia</option>
                            <option value="Solomon Islands">Solomon Islands</option>
                            <option value="Somalia">Somalia</option>
                            <option value="South Africa">South Africa</option>
                            <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                            <option value="South Sudan">South Sudan</option>
                            <option value="Spain">Spain</option>
                            <option value="Sri Lanka">Sri Lanka</option>
                            <option value="Sudan">Sudan</option>
                            <option value="Suriname">Suriname</option>
                            <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                            <option value="Swaziland">Swaziland</option>
                            <option value="Sweden">Sweden</option>
                            <option value="Switzerland">Switzerland</option>
                            <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                            <option value="Taiwan">Taiwan</option>
                            <option value="Tajikistan">Tajikistan</option>
                            <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                            <option value="Thailand">Thailand</option>
                            <option value="Timor-Leste">Timor-Leste</option>
                            <option value="Togo">Togo</option>
                            <option value="Tokelau">Tokelau</option>
                            <option value="Tonga">Tonga</option>
                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                            <option value="Tunisia">Tunisia</option>
                            <option value="Turkey">Turkey</option>
                            <option value="Turkmenistan">Turkmenistan</option>
                            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                            <option value="Tuvalu">Tuvalu</option>
                            <option value="Uganda">Uganda</option>
                            <option value="Ukraine">Ukraine</option>
                            <option value="United Arab Emirates">United Arab Emirates</option>
                            <option value="United Kingdom">United Kingdom</option>
                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                            <option value="Uruguay">Uruguay</option>
                            <option value="Uzbekistan">Uzbekistan</option>
                            <option value="Vanuatu">Vanuatu</option>
                            <option value="Venezuela, Bolivarian Republic of">Venezuela, Bolivarian Republic of</option>
                            <option value="Vietnam">Vietnam</option>
                            <option value="Virgin Islands, British">Virgin Islands, British</option>
                            <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                            <option value="Wallis and Futuna">Wallis and Futuna</option>
                            <option value="Western Sahara">Western Sahara</option>
                            <option value="Yemen">Yemen</option>
                            <option value="Zambia">Zambia</option>
                            <option value="Zimbabwe">Zimbabwe</option>
                        </select>
                    </div>
                </div>
            </fieldset> 
             <fieldset class="last-child">
                <div class="form-group">
                   <label class="col-sm-3 control-label">Postal Code</label>
                   <div class="col-sm-8">
                   {{   text_field("postalcode", "id" : "postal-code", "placeholder" : "Postal Code", "class": "form-control", "value" : profile.usr_postalcode) }}
                      <!-- <input type="text" placeholder="placeholder" class="form-control"> -->
                       {{   hidden_field("lat", "id" : "lat", "class": "form-control", "value" : profile.lat) }}
                       {{   hidden_field("lng", "id" : "lng", "class": "form-control", "value" : profile.lng) }}
                   </div>
                </div>
             </fieldset>
             <br />
             <button class="btn btn-primary" type="submit">Update</button>
          </form>
       </div>
    </div> #}
    <div class="col-md-5 col-md-offset-1">
        <div class="panel panel-default">
           <div class="panel-body">
           <legend> Account Settings</legend>
              {{ form("settings/updateEmail", "method" : "post", "class" : "form-horizontal") }}
                 <fieldset class="last-child">
                    <div class="form-group">
                       <label class="col-sm-3 control-label">Email</label>
                       <div class="col-sm-8">
                       {# {{    email_field("profile[email]", "placeholder" : "Email", "class": "form-control", "value" : profile.usr_email) }} #}
                       {{   email_field("email", "placeholder" : "Email", "class": "form-control", "value" : profile.usr_email) }}
                        <!--  <input type="email" placeholder="placeholder" class="form-control"> -->
                       </div>
                    </div>
                 </fieldset>
                 <br />
                 <div style="display: inline;">
                    <button class="btn btn-primary" type="submit">Update</button>
              {{ end_form() }}
                {% if email_verified == 0 %}
                    <button id="resend" class="btn btn-md btn-success">Resend Email Confirmation</button>
                {% endif %}
                 </div>
           </div>
        </div>
        <div class="panel panel-default">
           <div class="panel-body">
           <legend> Change Password </legend>
               {{ form("settings/changePassword", "id" : "change-password-form", "method" : "post", "class" : "form-horizontal") }}
                  <fieldset>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Current Password</label>
                        <div class="col-sm-8">
                           {{ password_field ("cpassword", "id" : "current_password", "class": "form-control" ) }}             
                        </div>
                     </div>
                  </fieldset>
                  <fieldset>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">New Password</label>
                        <div class="col-sm-8">
                           {{ password_field ("password", "id" : "new_password", "class": "form-control" ) }}             
                        </div>
                     </div>
                  </fieldset>
                  <fieldset class="last-child">
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Retype New Password</label>
                        <div class="col-sm-8">
                           {{ password_field ("rpassword", "id" : "confirm_new_password", "class": "form-control" ) }}  
                        </div>
                     </div>
                  </fieldset>
                  <br />
                    <button class="btn btn-primary" type="submit">Update</button>
                {{ end_form() }}
           </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="panel panel-default" id="activateTeacher">
           <div class="panel-body">
              <legend> Account Type </legend>
              {# <fieldset>
                 <div>
                    <em class="fa fa-4x fa-envelope" data-toggle="modal" data-target="#email" style="margin:20px;"></em>
                    <em class="fa fa-4x fa-camera" data-toggle="modal" data-target="#camera" style="margin:20px;"></em>
                    <em class="fa fa-4x fa-phone-square" data-toggle="modal" data-target="#phone" style="margin:20px;"></em>
                    <em class="fa fa-4x fa-group" data-toggle="modal" data-target="#social" style="margin:20px;"></em>
                    <em class="fa fa-4x fa-paypal" data-toggle="modal" data-target="#paypal" style="margin:20px;"></em>
                 </div>
              </fieldset> #}
              <fieldset>
                {% if userInfo.usr_teach %}
                <input class="btn btn-danger" type="submit" value="Switch to Client account" id="checkTeach" style="margin:20px;">
                {% else %}
                <input class="btn btn-primary" type="submit" value="Become a Freelancer" id="checkTeach" style="margin:20px;">
                {% endif %}
              </fieldset>
           </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <legend> Email Preferences </legend>
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-7 control-label">Do you want to receive our newsletter?</label>
                        <div class="col-sm-5">
                            <label class="switch">
                                {% if subscribeStatus == 1 %}
                                {{ check_field('newsletter', 'id' : 'subscribeBtn', 'checked' : 'true') }}
                                {% else %}
                                {{ check_field('newsletter', 'id' : 'subscribeBtn') }}
                                {% endif %}
                                <span></span>
                            </label>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="last-child">
                    <div class="form-group">
                        <label class="col-sm-7 control-label">Do you want us to forward all notifications to your email?</label>
                        <div class="col-sm-5">
                            <label class="switch">
                                {% if forwardStatus == 1 %}
                                {{ check_field('forwardEmail', 'id' : 'forwardBtn', 'checked' : 'true') }}
                                {% else %}
                                {{ check_field('forwardEmail', 'id' : 'forwardBtn') }}
                                {% endif %}
                                <span></span>
                            </label>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div> 
        <div class="panel panel-default">
            <div class="panel-body">
                <legend>Account Deactivation</legend>
                <fieldset>
                    <div class="col-sm-6">
                        <button class="btn btn-danger" data-toggle="modal" data-target="#deactivate" type="button">Deactivate</button>
                    </div>
                    <div class="col-sm-6">
                        Note: Your Account will reactivate when you log in
                    </div>
                </fieldset>
                {# <fieldset>
                    <div class="col-sm-4">
                        <button class="btn btn-danger" type="button">Delete Account</button>
                    </div>
                    <div class="col-sm-8">
                        Note: Account will be permanently deleted
                    </div>
                </fieldset> #}
            </div>
        </div>
    </div>
</section>
{% endblock %}

{% block modal %}
<!-- END modal-->
<div id="deactivate" tabindex="-1" role="dialog"
    aria-labelledby="deactivateLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="deactivateLabel" class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">Are you sure you want to deactivate your account?</div>
            <div class="modal-footer">
                <button class="btn btn-info" type="submit" id="btnDeactive">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
{# <!-- START modal-->
<div id="email" tabindex="-1" role="dialog" aria-labelledby="emailLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="emailLabel" class="modal-title">Verify your email</h4>
            </div>
            <div class="modal-body">
                <form action="/settings/sendConfirmation" method="POST" enctype="multipart/form-data">
                    <fieldset>
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-8">
                        {{    email_field("profile[email]", "placeholder" : "Email", "class": "form-control", "value" : profile.usr_email) }}
                        </div>
                    </fieldset>
                    <button type="submit" class="btn btn-success btn-sm">Confirm now!</button>
                </form>
                <br>
            </div>
            <div class="modal-footer">
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
         </div>
      </div>
   </div>
<!-- END modal-->
<!-- START modal-->
<div id="camera" tabindex="-1" role="dialog" aria-labelledby="cameraLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="cameraLabel" class="modal-title">Upload a photo of yourself</h4>
            </div>
            <div class="modal-body">
               <form action="/settings/setprofileimage" method="POST" enctype="multipart/form-data">
                  <input type='file' id="imgInp" name="image" />
                  <img id="avatar" src="#" alt="your image" style="margin-top:10px; margin-bottom:10px;">
                  <input type="hidden" id="x" name="crop[x]" />
                  <input type="hidden" id="y" name="crop[y]" />
                  <input type="hidden" id="w" name="crop[w]" />
                  <input type="hidden" id="h" name="crop[h]" />
                  <br>
                  <button type="button" class="btn btn-danger btn-sm">Crop</button>
                  <button type="submit" class="btn btn-success btn-sm">Upload</button>
               </form>
               <br>
            </div>
            <div class="modal-footer">
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
         </div>
      </div>
   </div>
<!-- END modal-->
<!-- START modal-->
<div id="phone" tabindex="-1" role="dialog" aria-labelledby="phoneLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="phoneLabel" class="modal-title">Verify Your Phone Number</h4>
            </div>
            <div class="modal-body">
            {{ form("/", "id" : "enter_number", "method" : "post", "class" : "form-horizontal") }}
               <fieldset>
                  <!-- START row -->
                  <div class="row">
                     <div class="col-md-6">
                        <div class="">
                           <label>Phone Number:</label>
                           {{ text_field('phone[phone_number]', "id" : "phone-number", "placeholder" : "5555555555", "class" : "form-control") }}
                        </div>
                     </div>
                     <div class="col-md-2">
                        <div class="form-group">
                           <input type="submit" class="btn btn-success btn-lg" value="Verify" style="margin:20px">
                        </div>
                     </div>
                  </div>
                  <div id="verify_code" style="display: none;">
                    <p>Calling you now.</p>
                    <p>When prompted, enter the verification code:</p>
                    <h1 id="verification_code"></h1>
                    <p><strong id="status">Waiting...</strong></p>
                  </div>
                  <!-- END row -->
               </fieldset>
            {{ end_form() }}
            </div>
            <div class="modal-footer">
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
         </div>
      </div>
   </div>
<!-- END modal-->
<!-- START modal-->
<div id="social" tabindex="-1" role="dialog" aria-labelledby="phoneLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="socialLabel" class="modal-title">Connect with your other profiles</h4>
            </div>
            <div class="modal-body">
              {% if not hasLinkedIn and not hasFacebook %}
              You have no Social Media connected. Please connect at least one of them<br/>
               <a href="/signin/auth/linkedin">Connect to LinkedIn</a><br/>
               <a href="/signin/auth/facebook">Connect to Facebook</a>
              {% endif %}
              {% if hasLinkedIn %}
              Your Account is connected to your linkedIn.<br/>
              {% endif %}
              {% if hasFacebook %}
              Your Account is connected to your Facebook.<br/>
              {% endif %}
              {% if not hasFacebook  and hasLinkedIn %}
              Your Account is  not connected with Facebook.<br/>
              <a href="/signin/auth/facebook">Connect to Facebook by clicking here</a>
              {% endif %}
             {% if not hasLinkedIn  and hasFacebook %}
              Your Account is  not connected with LinkedIn.<br/>
              <a href="/signin/auth/linkedin">Connect to LinkedIn by clicking here</a>
              {% endif %}
            </div>
            <div class="modal-footer">
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
         </div>
      </div>
   </div>
<!-- END modal--> #}
{% endblock %}

{% block footer %}
{% endblock %}

{% block script %}
<script>
$(function(){ FormsValidation.init(); });
$(document).ready(function(){
    $("#checkTeach").click(function() {
        var self = this;
        var becomeTeacher = ($(self).val() == 'Become a Freelancer')?true:false;
        $.getJSON("/user/updateTeach", {teach: becomeTeacher},
            function(data){
                /* if (typeof data.status == 'string'){
                    $(self).val(data.status);
                    if (becomeTeacher){
                        $(self).removeClass('btn-info');
                        $(self).addClass('btn-danger');
                    }else{
                        $(self).removeClass('btn-danger');
                        $(self).addClass('btn-info');
                    }
                    //alert('Your status has been updated successfully.');
                    window.location.reload();
                }else{
                    //alert('Sorry, you could not change your teacher status');
                    window.location.reload();
                } */
                window.location.reload();
            }
        );
    });

    $("#btnDeactive").click(function(){
        $.ajax({
            url: '/user/deactivate',
            type: 'POST',
            success: function(){
                window.location = "/user/bye";
            }
        });
    }); 
    
    $('#subscribeBtn').click(function() {
        $(this).prop("checked") ? $(this).val("1") : $(this).val("0");
        if ($(this).val() == 1) {
            var value = 1;
        }else if ($(this).val() == 0) {
            var value = 0;
        }
        $.ajax({
            url: '/user/updateSubscribe',
            type: 'POST',
            data: {
                value : value
            },
            success: function(res) {
                location.reload();
            }
        });
    });

    $('#forwardBtn').click(function() {
        $(this).prop("checked") ? $(this).val("1") : $(this).val("0");
        if ($(this).val() == 1) {
            var value = 1;
        }else if ($(this).val() == 0) {
            var value = 0;
        }
        $.ajax({
            url: '/user/updateForward',
            type: 'POST',
            data: {
                value : value
            },
            success: function(res) {
                location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(value);
            }
        });
    });

    $("#resend").click(function() {

        var email = "<?php echo $email; ?>";

        $.ajax({
            url: '/user/resend',
            type: 'POST',
            data:{
                email: email
            },
            success: function(res) {
                location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                location.reload();
            }
        });
    });
    

    /* $("#enter_number").submit(function(e) {
        e.preventDefault();
        initiateCall();
    }); */
});

/* function initiateCall() {
    $.post("settings/call", { phone_number : $("#phone_number").val() },
    function(data) { showCodeForm(data.verification_code); }, "json");
        checkStatus();
} 

function showCodeForm(code) {
    $("#verification_code").text(code);
    $("#verify_code").fadeIn();
    $("#enter_number").fadeOut();
} */

function geoLookUp(q, callback){
	var geo = new google.maps.Geocoder();
	geo.geocode(q, callback);
};

/* $("#qw-usettings-form").find("button").click(function(e){
	var f = $("#qw-usettings-form");
	var q = $("#postal-code", this.id).val().trim();
	//var c = $("#countrySelect", this.id).val().trim();

	if(q){
		geoLookUp({address: q}, function(results, status){
			if (status == google.maps.GeocoderStatus.OK) {
				console.info(results);
				var result = results[0];
				var p = result.geometry.location;
				var lat = p.lat();
				var lng = p.lng();
				console.log(lat, lng);
				$("#lat", f.id).val(lat);
				$("#lng", f.id).val(lng);
				f.submit();
			}
			else{
				alert("Unable to retrieve map information");
			}
		});
	}
}); */
</script>

{{ javascript_include("js/pages.js") }}
{# javascript_include("js/settings/phone.js") #}
{{ javascript_include("js/settings/facebook.js") }}
<script src="/third-party/47admin/formwizard/js/bwizard.min.js"></script>
<!-- Form Validation-->
<script src="/third-party/47admin/parsley/parsley.min.js"></script>
{% endblock %}
