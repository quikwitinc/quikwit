{% extends "templates/home.volt" %}

{% block title %}
Log In | QuikWit
{% endblock %}

{% block head %}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        {#<h1 class="text-center animation-slideDown"><strong>Log In</strong></h1>#}
        <h1 class="h3 text-center animation-slideUp" style="font-size: 200%"><strong>Log in and let's get started.</strong></h1>
    </div>
</section>
<!-- END Intro -->

<!-- Log In -->
<section class="site-content site-section" style="padding-bottom: 200px;">
    <div class="container">
        <div class="row" style="margin-top: 50px">
            <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 site-block">
                <!-- Log In Form -->
                {{ form("signin/doSignin", "id" : "signin-validation", "method" : "post", "class" : "form-horizontal") }}
                    <div class="form-group" style="text-align:center;">
                      <div class="col-xs-6">
                        <a class="btn btn-m" style="background-color: #4e69a2; color: white;" href="/signin/auth/facebook">Facebook Login</a>
                      </div>
                      <div class="col-xs-6">
                        <a class="btn btn-m" style="background-color: #069; color: white;" href="/signin/auth/linkedin">Linkedin Login</a>
                      </div> 
                    </div> 
                    <h3 class="text-center">or</h3>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                {{ email_field('username', "id" : "form-username", "placeholder" : "Email", "class": "form-control input-lg", "style" : "font-size:130%") }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-lock"></i></span>
                                {{ password_field('password', "id" : "form-password", "placeholder": "Password", "class": "form-control input-lg", "style" : "font-size:130%") }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                     <!-- <input type="hidden" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}" /> -->
                     {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }}
                     <!-- {{ router.getRewriteUri() }} -->
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-xs-6">
                            {# <label class="switch switch-primary">
                                <input type="checkbox" id="login-remember-me" name="login-remember-me" checked><span></span>
                            </label>
                            <small>Remember me</small> #}
                        </div> 
                        <div class="col-xs-12 text-center">
                            <button type="submit" class="btn btn-lg btn-primary"><strong>Log In</strong></button>

                        </div>
                    </div><br>
                {{ end_form() }}
                <div class="text-center" style="font-size: 110%">
                    <a href="/signin/recover">Forgot password?</a>
                </div>
                <!-- END Log In Form -->
            </div>
        </div>
    </div>
</section>
<!-- END Log In -->

{#	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-8"  id="signinPanel">
         <!-- START panel-->
         <div data-toggle="play-animation" data-play="fadeInUp" data-offset="0" class="panel panel-default panel-flat">
            <p class="text-center mb-lg">
               <br>
               <a href="#">
                  <img src="img/landing/QW1.png" alt="QuikWit" class="block-center img-responsive" style="width:50%; height:50%">
               </a>
            </p>
            <p class="text-center mb-lg">
               <strong>SIGN IN TO CONTINUE.</strong>
            </p>
            <div class="panel-body">

            {{ form("signin/doSignin", "id" : "signin-form", "method" : "post", "class" : "mb-lg") }}
              <!--  <form role="form" class="mb-lg" action=""> -->
                  <div class="text-right mb-sm"><a href="/index" class="text-muted">Need to Signup?</a>
                  </div>
                  <div class="form-group has-feedback">
                     <!--  <input id="exampleInputEmail1" type="email" placeholder="Enter email" class="form-control"> -->
                     {{ email_field('username', "id" : "form-username", "placeholder" : "Enter email", "class": "form-control") }}
                     <span class="fa fa-envelope form-control-feedback text-muted"></span>
                  </div>
                  <div class="form-group has-feedback">
                     <!-- <input id="exampleInputPassword1" type="password" placeholder="Password" class="form-control"> -->
                     {{ password_field('password', "id" : "form-password", "placeholder": "Password", "class": "form-control") }}
                     <span class="fa fa-lock form-control-feedback text-muted"</span>
                  </div>
                   <div class="clearfix" style="text-align:center;">
                     <a href="/signin/auth/linkedin">Login with Linkin</a> <a href="/signin/auth/facebook">Login with Facebook</a>
                  </div>
                  <div class="form-group">
					 <!-- <input type="hidden" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}" /> -->
					 {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }}
					 <!-- {{ router.getRewriteUri() }} -->
				  </div>
                  <div class="clearfix">
                     <div class="checkbox c-checkbox pull-left mt0" style="left:30px;">
                        <label><input type="checkbox" value=""><span class="fa fa-check"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Remember Me</label>
                     </div>
                  </div>
                  <button type="submit" class="btn btn-block btn-primary">Login</button>
                  <div class="text-center"><a href="/signin/recover" class="text-muted">Forgot your password?</a></div>
               {{ end_form() }}
            </div>
         </div>
         <!-- END panel-->
	</div> #}
{% endblock %}

{% block script %}
{{ javascript_include("js/pages.js") }}
{% endblock %}