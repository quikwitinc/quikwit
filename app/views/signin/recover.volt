{% extends "templates/home.volt" %}

{% block title %}
Recover Password | QuikWit
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h1 class="h3 text-center animation-slideUp" style="font-size: 200%"><strong>Let's help you retrieve your password.</strong></h1>
        {#<h2 class="h3 text-center animation-slideUp">Fill with your email to receive instructions on resetting your password!</h2>#}
    </div>
</section>
<!-- END Intro -->

<section class="site-content site-section" style="padding-bottom: 200px;">
    <div class="container">
        <div class="row" style="margin-top: 40px">
            <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 site-block">
                <span class="response-message" style="font-size: 16px;"></span>
            </div>
            <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 site-block">
                {{ form("signin/recover", "id" : "password-recover-form", "method" : "post", "class" : "form-horizontal") }}
	               <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                {{ email_field('email', "id" : "form-email", "placeholder" : "Email", "class": "form-control input-lg") }}
                            </div>
                        </div>
                    </div><br>
	            	<div class="form-group form-actions">
	            	    <div class="col-xs-12 text-center">
	            	        <button id="form-submit" type="submit" class="btn btn-lg btn-primary"><strong>Reset Password</strong></button>
	            	    </div>
	            	</div>
	            {{ end_form() }}
	         </div>
	    </div>
	    <!-- END panel-->
	</div>
</section>
{% endblock %}

{% block script %}
<!-- Custom script for pages-->
	{{ javascript_include("js/pages.js") }}
	{# {{ javascript_include("js/signin/recover.js") }} #}
{% endblock %}