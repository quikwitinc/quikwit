{% extends "templates/home.volt" %}

{% block title %}
Reset Password | QuikWit
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h2 class="text-center animation-slideUp"><strong>A few more steps...</strong></h2>
        
    </div>
</section>
<!-- END Intro -->

<section class="site-content site-section" style="padding-bottom: 200px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 site-block">
                <span class="response-message" style="font-size: 16px;"></span>
            </div>
            <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 site-block">
            {% if isValid %}
                {{ form("signin/resetPassword", "id" : "password-reset-form", "method" : "post", "class" : "form-horizontal") }}
	               <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                                {{ password_field('password', "id" : "form-password", "placeholder" : "Password", "class": "form-control input-lg") }}
                            </div>
                        </div>
                    </div>
                   <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                                {{ password_field('password_confirmation', "id" : "form-password-confirmation", "placeholder" : "Password Confirmation", "class": "form-control input-lg") }}
                            </div>
                        </div>
                    </div>
                    <br>
	            	<div class="form-group form-actions">
	            	    <div class="col-xs-12 text-center">
	            	        <button id="form-submit" type="submit" class="btn btn-lg btn-primary"><strong>Change Password</strong></button>
	            	    </div>
	            	</div>
	            {{ end_form() }}
	        {% elseif isExpired %}

            <div class="text-center">
                <span style="font-size: 18px;">The Url is expired. Please try <a href="/signin/recover">recovering</a> your password again.</span>
            </div>

            {% elseif isCompleted %}
                <div class="text-center">
                    <span style="font-size: 16px;">
                        You have successfully changed your password. Please sign in or <a href="/signin/recover">request </a> another password change.
                    </span>
                </div>
                <br>
                <!-- Log In Form -->
                {{ form("signin/doSignin", "id" : "signin-validation", "method" : "post", "class" : "form-horizontal") }}
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                {{ email_field('username', "id" : "form-username", "placeholder" : "Email", "class": "form-control input-lg", "style" : "font-size:130%") }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-lock"></i></span>
                                {{ password_field('password', "id" : "form-password", "placeholder": "Password", "class": "form-control input-lg", "style" : "font-size:130%") }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                     <!-- <input type="hidden" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}" /> -->
                     {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }}
                     <!-- {{ router.getRewriteUri() }} -->
                    </div>

                    <div class="form-group form-actions">
                        <div class="col-xs-6">
                            {# <label class="switch switch-primary">
                                <input type="checkbox" id="login-remember-me" name="login-remember-me" checked><span></span>
                            </label>
                            <small>Remember me</small> #}
                        </div> 

                        <div class="col-xs-12 text-center">
                            <button type="submit" class="btn btn-lg btn-primary"><strong>Log In</strong></button>
                        </div>
                    </div><br>
                {{ end_form() }}
             {% endif %}
	         </div>
	    </div>
	    <!-- END panel-->
	</div>
</section>
{% endblock %}

{% block script %}
<!-- Custom script for pages-->
	{{ javascript_include("js/pages.js") }}
	{{ javascript_include("js/signin/resetPassword.js") }}
{% endblock %}