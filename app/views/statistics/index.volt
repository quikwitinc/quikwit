{% extends "templates/home.volt" %}

{% block head %}
{% endblock %}

{% block content %}
<section class="main-content">
   <h3>Hi John,
      <br>
      <small>This is your stats.</small>
   </h3>
   <div class="row">
      <!-- START dashboard main content-->
      <div class="col-md-9">
         <!-- START summary widgets-->
         <div class="row">
            <div class="col-lg-3 col-sm-6">
               <!-- START widget-->
               <div data-toggle="play-animation" data-play="fadeInDown" data-offset="0" data-delay="100" class="panel widget">
                  <div class="panel-body bg-primary">
                     <div class="row row-table row-flush">
                        <div class="col-xs-8">
                           <p class="mb0">Total Contacts</p>
                           <h3 class="m0">150</h3>
                        </div>
                        <div class="col-xs-4 text-center">
                           <em class="fa fa-user fa-2x"><sup class="fa fa-plus"></sup>
                           </em>
                        </div>
                     </div>
                  </div>
                  <div class="panel-body">
                     <!-- Bar chart-->
                     <div class="text-center">
                        <div data-bar-color="primary" data-height="30" data-bar-width="6" data-bar-spacing="6" class="inlinesparkline inline">5,3,4,6,5,9,4,4,10,5,9,6,4</div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-3 col-sm-6">
               <!-- START widget-->
               <div data-toggle="play-animation" data-play="fadeInDown" data-offset="0" data-delay="500" class="panel widget">
                  <div class="panel-body bg-warning">
                     <div class="row row-table row-flush">
                        <div class="col-xs-8">
                           <p class="mb0">Bounce Rate</p>
                           <h3 class="m0">80%</h3>
                        </div>
                        <div class="col-xs-4 text-center">
                           <em class="fa fa-users fa-2x"></em>
                        </div>
                     </div>
                  </div>
                  <div class="panel-body">
                     <!-- Bar chart-->
                     <div class="text-center">
                        <div data-bar-color="warning" data-height="30" data-bar-width="6" data-bar-spacing="6" class="inlinesparkline inline">10,30,40,70,50,90,70,50,90,40,40,60,40</div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-3 col-sm-6">
               <!-- START widget-->
               <div data-toggle="play-animation" data-play="fadeInDown" data-offset="0" data-delay="1000" class="panel widget">
                  <div class="panel-body bg-danger">
                     <div class="row row-table row-flush">
                        <div class="col-xs-8">
                           <p class="mb0">Appear in Searches</p>
                           <h3 class="m0">1000</h3>
                        </div>
                        <div class="col-xs-4 text-center">
                           <em class="fa fa-search fa-2x"></em>
                        </div>
                     </div>
                  </div>
                  <div class="panel-body">
                     <!-- Bar chart-->
                     <div class="text-center">
                        <div data-bar-color="danger" data-height="30" data-bar-width="6" data-bar-spacing="6" class="inlinesparkline inline">2,7,5,9,4,2,7,5,7,5,9,6,4</div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-3 col-sm-6">
               <!-- START widget-->
               <div data-toggle="play-animation" data-play="fadeInDown" data-offset="0" data-delay="1500" class="panel widget">
                  <div class="panel-body bg-success">
                     <div class="row row-table row-flush">
                        <div class="col-xs-8">
                           <p class="mb0">Total Visitors</p>
                           <h3 class="m0">1.5k</h3>
                        </div>
                        <div class="col-xs-4 text-center">
                           <em class="fa fa-globe fa-2x"></em>
                        </div>
                     </div>
                  </div>
                  <div class="panel-body">
                     <!-- Bar chart-->
                     <div class="text-center">
                        <div data-bar-color="success" data-height="30" data-bar-width="6" data-bar-spacing="6" class="inlinesparkline inline">4,7,5,9,6,4,8,6,3,4,7,5,9</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- START Secondary Widgets-->
         <div class="row">
            <div class="col-md-4">
               <!-- START widget-->
               <div data-toggle="play-animation" data-play="fadeInLeft" data-offset="0" data-delay="1400" class="panel widget">
                  <div class="panel-body">
                     <div class="text-right text-muted">
                        <em class="fa fa-users fa-2x"></em>
                     </div>
                     <h3 class="mt0">4</h3>
                     <p class="text-muted">Total Contracts Completed</p>
                     <div class="progress progress-striped progress-xs">
                        <div role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;" class="progress-bar progress-bar-success">
                           <span class="sr-only">80% Complete</span>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END widget-->
            </div>
            <div class="col-md-4">
               <!-- START widget-->
               <div data-toggle="play-animation" data-play="fadeInLeft" data-offset="0" data-delay="1400" class="panel widget">
                  <div class="panel-body">
                     <div class="text-right text-muted">
                        <em class="fa fa-bar-chart-o fa-2x"></em>
                     </div>
                     <h3 class="mt0">$ 1250</h3>
                     <p class="text-muted">Total Income</p>
                     <div class="progress progress-striped progress-xs">
                        <div role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;" class="progress-bar progress-bar-info">
                           <span class="sr-only">70% Complete</span>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END widget-->
            </div>
            <div class="col-md-4">
               <!-- START widget-->
               <div data-toggle="play-animation" data-play="fadeInLeft" data-offset="0" data-delay="1400" class="panel widget">
                  <div class="panel-body">
                     <div class="text-right text-muted">
                        <em class="fa fa-trophy fa-2x"></em>
                     </div>
                     <h3 class="mt0">260</h3>
                     <p class="text-muted">Reward Points</p>
                     <div class="progress progress-striped progress-xs">
                        <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" class="progress-bar progress-bar-warning">
                           <span class="sr-only">60% Complete</span>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END widget-->
            </div>
         </div>
         <!-- END Secondary Widgets-->
         <!-- END summary widgets-->
         <!-- START chart-->
         <div class="row">
            <div class="col-lg-12">
               <div class="panel panel-default">
                  <div class="panel-collapse">
                     <div class="panel-body">
                        <div style="height: 350px;" data-source="/server/chart-data.php?type=area" class="chart-area flot-chart"></div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <a href="#" data-perform="panel-dismiss" data-toggle="tooltip" title="Close Panel" class="pull-right">
                     <em class="fa fa-times"></em>
                  </a>
                  <a href="#" data-perform="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
                     <em class="fa fa-minus"></em>
                  </a>
                  <div class="panel-title">Ratings</div>
               </div>
               <div class="panel-body">
                  <div style="height: 200px;" data-source="/server/chart-data.php?type=pie" class="chart-pie flot-chart"></div>
               </div>
            </div>
            </div>
         </div>
         <!-- END chart-->                 
      </div>
      <!-- END dashboard main content-->
      <!-- START dashboard sidebar-->
      <div class="col-md-3">
         <!-- START messages-->
         <div class="panel panel-default">
            <div class="panel-heading">
               <div class="panel-title">Latest Visitors</div>
            </div>
            <!-- START list group-->
            <div class="list-group">
               <!-- START list group item-->
               <a href="#" class="list-group-item">
                  <div class="media">
                     <div class="pull-left">
                        <img style="width: 48px; height: 48px;" src="/img/user/01.jpg" alt="Image" class="media-object img-rounded">
                     </div>
                     <div class="media-body clearfix">
                        <small class="pull-right">2h</small>
                        <strong class="media-heading text-primary">
                           <div class="point point-success point-lg text-left"></div>Sheila Carter</strong>
                        <p class="mb-sm">
                           <small>Artist</small>
                        </p>
                     </div>
                  </div>
               </a>
               <!-- END list group item-->
               <!-- START list group item-->
               <a href="#" class="list-group-item">
                  <div class="media">
                     <div class="pull-left">
                        <img style="width: 48px; height: 48px;" src="/img/user/04.jpg" alt="Image" class="media-object img-rounded">
                     </div>
                     <div class="media-body clearfix">
                        <small class="pull-right">3h</small>
                        <strong class="media-heading text-primary">
                           <div class="point point-success point-lg text-left"></div>Rich Reynolds</strong>
                        <p class="mb-sm">
                           <small>Programmer</small>
                        </p>
                     </div>
                  </div>
               </a>
               <!-- END list group item-->
               <!-- START list group item-->
               <a href="#" class="list-group-item">
                  <div class="media">
                     <div class="pull-left">
                        <img style="width: 48px; height: 48px;" src="/img/user/03.jpg" alt="Image" class="media-object img-rounded">
                     </div>
                     <div class="media-body clearfix">
                        <small class="pull-right">4h</small>
                        <strong class="media-heading text-primary">
                           <div class="point point-danger point-lg text-left"></div>Beverley Pierce</strong>
                        <p class="mb-sm">
                           <small>Lifecoach</small>
                        </p>
                     </div>
                  </div>
               </a>
               <!-- END list group item-->
               <!-- START list group item-->
               <a href="#" class="list-group-item">
                  <div class="media">
                     <div class="pull-left">
                        <img style="width: 48px; height: 48px;" src="/img/user/06.jpg" alt="Image" class="media-object img-rounded">
                     </div>
                     <div class="media-body clearfix">
                        <small class="pull-right">4h</small>
                        <strong class="media-heading text-primary">
                           <div class="point point-danger point-lg text-left"></div>Alex Somar</strong>
                        <p class="mb-sm">
                           <small>Marketer</small>
                        </p>
                     </div>
                  </div>
               </a>
               <!-- END list group item-->
            </div>
            <!-- END list group-->
            <!-- START panel footer-->
            <div class="panel-footer clearfix">
               <a href="#" class="pull-left">
                  <small>More</small>
               </a>
            </div>
            <!-- END panel-footer-->
         </div>
         <!-- END messages-->
      </div>
      <!-- END dashboard sidebar-->
   </div>
   </section>
{% endblock %}

{% block script %}
	<!--  Flot Charts-->
   <script src="/third-party/47admin/flot/jquery.flot.min.js"></script>
   <script src="/third-party/47admin/flot/jquery.flot.tooltip.min.js"></script>
   <script src="/third-party/47admin/flot/jquery.flot.resize.min.js"></script>
   <script src="/third-party/47admin/flot/jquery.flot.pie.min.js"></script>
   <script src="/third-party/47admin/flot/jquery.flot.time.min.js"></script>
   <script src="/third-party/47admin/flot/jquery.flot.categories.min.js"></script>
   <!--[if lt IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
{% endblock %}