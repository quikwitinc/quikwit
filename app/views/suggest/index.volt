{% extends "templates/home.volt" %}

{% block title %}
Suggest a service | QuikWit
{% endblock %}

{% block head %}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
       {#<h1 class="text-center animation-slideDown"><i class="fa fa-arrow-right"></i> <strong>Suggest a Skill</strong></h1>#}
        <h1 class="h3 text-center animation-slideUp" style="font-size: 200%"><strong>Let us know which service you want us to add.</strong></h1>
    </div>
</section>
<!-- END Intro -->

<section class="site-section site-content" style="padding-bottom: 200px;">
	<div class="container">
		<div class="row" style="margin-top: 40px">
            <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 site-block">
            
            	<p class="text-center" style="font-size: 130%"> Thanks for contributing! We will add the new categories as soon as possible after reviewing it.</p>
            	<br>
			    <form id="suggest-skills-form" class="form-horizontal">
			        <div class = "form-group">
			           {# <label for = "sub-skill" class = "col-lg-3 control-label">Skills:</label>#}
			            <div class = "col-xs-12 text-center">
			               <input id='suggest-skills-input' class="input-tags form-control input-lg" type="text" data-role="tagsinput">
			            </div>
			        </div><br>
			        <div class="form-group form-actions">
			            <div class="col-xs-12 text-center">
			                <button id="suggest-skills-submit" class="btn btn-lg btn-primary"><strong>Suggest Service</strong></button>
			            </div>
			        </div>
			    </form>
		    </div>
		</div>
    </div>
</section>
{% endblock %}

{% block script %}
<script>
$("#suggest-skills-submit").click(function(){
    $.ajax('/suggest/suggestSkill', {
        data: {
            skills : $("#suggest-skills-input").val()
        },
        success: function(data){
        	console.log(data);
        }
    });
});
</script> 
{% endblock %}