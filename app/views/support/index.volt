{% extends "templates/home.volt" %}

{% block title %}
Support | QuikWit
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h1 class="text-center animation-slideDown"><i class="fa fa-arrow-right"></i> <strong>Support</strong></h1>
        <h2 class="h3 text-center animation-slideUp">Let us help you!</h2>
    </div>
</section>
<!-- END Intro -->

<!-- Log In -->
<section class="site-content site-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 site-block">
                <!-- Log In Form -->
                <form id="support-form" class="form-horizontal" method="post" enctype="text/plain">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-pen"></i></span>
                                <input type="text" id="support-title" placeholder="Title" class="form-control input-lg">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                       <div class="col-xs-12">
                           <div class="input-group">
                             <span class="input-group-addon"><i class="gi gi-search"></i></span>
                             <select class="form-control input-lg" id="support-category">
                                 <option>-- Categories --</option>
                                 <option>Creating & Managing Your Account</option>
                                 <option>Account Permission</option>
                                 <option>Contracts</option>
                                 <option>Postings</option>
                                 <option>Transaction</option>
                                 <option>General</option>
                             </select>
                           </div>
                       </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                <input type="text" id="support-email" placeholder="Email" class="form-control input-lg">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-edit"></i></span>
                                <textarea type="text" id="support-problem" placeholder="Details of the problem" class="form-control input-lg" rows="8"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        {# <div class="col-xs-6">
                            <label class="switch switch-primary">
                                <input type="checkbox" name="redirect-follow-up" checked><span></span>
                            </label>
                            <br>
                            <small>Follow-ups to your email</small>
                        </div> #}
                        <div class="col-xs-12 text-center">
                            <button type="submit" id="sendButton" class="btn btn-m btn-primary">Send</button>
                        </div>
                    </div>
                </form>
                <div class="text-center">
                    <a href="/signin/recover"><small>Forgot password?</small></a>
                </div>
                <!-- END Log In Form -->
            </div>
        </div>
    </div>
</section>
<!-- END Log In -->
{% endblock %}

{% block script %}
{{ javascript_include("js/pages.js") }}
{{ javascript_include("js/signin/index.js") }}
{{ javascript_include("third-party/proui/backend/js/pages/formsValidation.js") }}

<script type="text/javascript">
$(function(){ FormsValidation.init(); });
$("#sendButton").click(function() {
    var title = $("#support-title").val();
    var category = $('#support-category').val();
    var email = $('#support-email').val();
    var problem = $('#support-problem').val();

    $.ajax({
        url: '/support/send',
        type: 'POST',
        data:{
            title: title,
            category: category,
            email: email,
            problem: problem
        },
        success: function(res) {
            location.reload();
        }
    });
});
</script>
{% endblock %}