<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">

<title>{% block title %}User Dashboard{% endblock %}</title>

<meta name="description" content="">
<meta name="author" content="">
<meta name="robots" content="noindex, nofollow">

<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

<!-- Stylesheets -->
<!-- Bootstrap is included in its original form, unaltered -->
{{ stylesheet_link("third-party/proui/backend/css/bootstrap.min.css") }}

<!-- Related styles of various icon packs and plugins -->
{{ stylesheet_link("third-party/proui/backend/css/plugins.css") }}

<!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
{{ stylesheet_link("third-party/proui/backend/css/main.css") }}

<!-- App CSS-->
{{ stylesheet_link('css/app.css') }}

<!-- START Page Custom CSS-->
{% block head %}
{% endblock %}
<!-- END Page Custom CSS-->

<!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

<!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
{{ stylesheet_link("third-party/proui/backend/css/themes.css") }}
<!-- END Stylesheets -->

<!-- Modernizr (browser feature detection library) & Respond.js (enables responsive CSS code on browsers that don't support it, eg IE8) -->
{{ javascript_include('third-party/proui/backend/js/vendors/modernizr-respond.min.js') }}

<style>
{# .padding220 {padding-top: +220px;}
.padding50 {padding-top: +50px;} #}
</style>

</head>
<body style="background-color: #eaedf1;">
<!-- Page Wrapper -->
<!-- In the PHP version you can set the following options from inc/config file -->
<!--
    Available classes:

    'page-loading'      enables page preloader
-->
<div id="page-wrapper">
    {# <!-- Preloader -->
    <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
    <!-- Used only if page preloader is enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
    <div class="preloader themed-background">
        <h1 class="push-top-bottom text-light text-center"><strong>Pro</strong>UI</h1>
        <div class="inner">
            <h3 class="text-light visible-lt-ie9 visible-lt-ie10"><strong>Loading..</strong></h3>
            <div class="preloader-spinner hidden-lt-ie9 hidden-lt-ie10"></div>
        </div>
    </div>
    <!-- END Preloader --> #}

    <div id="page-container" class="sidebar-visible-lg sidebar-no-animations">
        <!-- Main Sidebar -->
        <div id="sidebar">
            <!-- Wrapper for scrolling functionality -->
            <div id="sidebar-scroll">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Brand -->
                    <a href="javascript:void(0)" class="sidebar-brand">
                        <i class="gi gi-book"></i><span class="sidebar-nav-mini-hide"><strong>Features </strong></span>
                    </a>
                    <!-- END Brand -->

                    <!-- Sidebar Navigation -->
                    <ul class="sidebar-nav">
                        <li class="sidebar-header">
                            <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Learn anything locally!"><i class="gi gi-lightbulb"></i></a></span>
                            <span class="sidebar-header-title">Browse Categories</span>
                        </li>
                        <li>
                            <a href="page_widgets_stats.html"><i class="gi gi-charts sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Statistics</span></a>
                        </li>
                        <li>
                            <a href="page_widgets_social.html"><i class="gi gi-share_alt sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Social</span></a>
                        </li>
                        <li>
                            <a href="page_widgets_media.html"><i class="gi gi-film sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Media</span></a>
                        </li>
                        <li>
                            <a href="page_widgets_links.html"><i class="gi gi-link sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Links</span></a>
                        </li>
                    </ul>
                    <!-- END Sidebar Navigation -->                       
                </div>
                <!-- END Sidebar Content -->
            </div>
            <!-- END Wrapper for scrolling functionality -->
        </div>
        <!-- END Main Sidebar -->

        <!-- Main Container -->
        <div id="main-container">
            <!-- Header -->
            <!-- In the PHP version you can set the following options from inc/config file -->
            <!--
                Available header.navbar classes:

                'navbar-default'            for the default light header
                'navbar-inverse'            for an alternative dark header

                'navbar-fixed-top'          for a top fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                    'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

                'navbar-fixed-bottom'       for a bottom fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                    'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
            -->
            <header class="navbar navbar-default">
                <!-- Left Header Navigation -->
                <ul class="nav navbar-nav-custom">
                    <!-- Main Sidebar Toggle Button -->
                    <li>
                        <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                            <i class="fa fa-bars fa-fw"></i>
                        </a>
                    </li>
                    <li>
                        <a href="/index">
                            <img src="img/landing/QW2.png" style="height: 40px; width: 40px;">
                        </a>
                    </li>
                    <!-- END Main Sidebar Toggle Button -->

                    {# <!-- Template Options -->
                    <!-- Change Options functionality can be found in js/app.js - templateOptions() -->
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="gi gi-settings"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-custom dropdown-options">
                            <li class="dropdown-header text-center">Header Style</li>
                            <li>
                                <div class="btn-group btn-group-justified btn-group-sm">
                                    <a href="javascript:void(0)" class="btn btn-primary" id="options-header-default">Light</a>
                                    <a href="javascript:void(0)" class="btn btn-primary" id="options-header-inverse">Dark</a>
                                </div>
                            </li>
                            <li class="dropdown-header text-center">Page Style</li>
                            <li>
                                <div class="btn-group btn-group-justified btn-group-sm">
                                    <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style">Default</a>
                                    <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style-alt">Alternative</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- END Template Options --> #}
                </ul>
                <!-- END Left Header Navigation -->

                <!-- Search Form -->
                <form class="navbar-form-custom" role="search">
                    <div class="form-group">
                        <input type="text" id="top-search" class="form-control" placeholder="Search here">
                    </div>
                    <button type="submit" class="hidden btn btn-default">Submit</button>
                </form>
                <!-- END Search Form -->

                <!-- Right Header Navigation -->
                <ul class="nav navbar-nav-custom pull-right">

                    <li><a href="/coming-soon">Feature</a>
                    {% if user is defined %}
                    <!-- User Dropdown -->
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ user.usr_avatar }}" alt="Avatar"> <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                            <li class="dropdown-header text-center">Welcome, {{ user.firstName|capitalize }}</li>
                            <li>
                                <a href="/message">
                                    <i class="fa fa-envelope-o fa-fw pull-right"></i>
                                    <span class="badge pull-right">5</span>
                                    Messages
                                </a>
                                <a href="/contacts"><i class="fa fa-user-plus fa-fw pull-right"></i> 
                                    Contacts
                                </a>
                                <a href="/lesson"><i class="fa fa-tasks fa-fw pull-right"></i> 
                                    Lessons
                                </a>
                                <a href="#"><i class="fa fa-question fa-fw pull-right"></i>
                                    FAQ
                                </a>
                                {# <a href="/support"><i class="fa fa-question fa-fw pull-right"></i>
                                    Support
                                </a> #}

                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="/overview">
                                    <i class="fa fa-user fa-fw pull-right"></i>
                                    Profile
                                </a>
                                <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                                <a href="/settings" data-toggle="modal">
                                    <i class="fa fa-cog fa-fw pull-right"></i>
                                    Settings
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                {{ link_to("signout", "Logout") }}
                            </li>
                        </ul>
                    </li>
                    <!-- END User Dropdown -->
                    {% else %}
                    <li><a href="{{ url('signin') }} "> <strong>Login</strong>
                    </a>
                    </li> {% endif %}
                </ul>
                <!-- END Right Header Navigation -->

                {# <!-- Dashboard 2 Header -->
                {% if user is defined %}
                <div class="content-header" id="dashboard" style="margin-top: 50px; margin-bottom: 0px; background-color: #1bbae1;">
                    <ul class="nav-horizontal text-center">
                        <li>
                            <a href="/dashboard"><i class="fa fa-home"></i> Home</a>
                        </li>
                        <li>
                            <a href="/message"><i class="fa fa-comment"></i> Message</a>
                        </li>
                        <li>
                            <a href="/contacts"><i class="fa fa-user-plus"></i> Contacts</a>
                        </li>
                        <li>
                            <a href="/lesson"><i class="fa fa-tasks"></i> Lessons</a>
                        </li>
                        <li>
                            <a href="/posting"><i class="fa fa-gavel"></i> Postings</a>
                        </li> 
                        <li>
                            <a href="/overview"><i class="fa fa-pencil"></i> Profile</a>
                        </li>
                        <li>
                            <a href="/settings"><i class="fa fa-cogs"></i> Settings</a>
                        </li>
                         <?php if(!empty($userInfo) && $userInfo->usr_level === user::ROLE_ADMIN): ?>
                         <li><a href="{{ url('o') }}">Manage Oauth Tokens</a></li>
                         <?php endif; ?>
                    </ul>
                </div>
                {% endif %}
                <!-- END Dashboard 2 Header --> #}

            </header>
            <!-- END Header -->

            <!-- Page content -->
            <div id="page-content">

              <!-- Visible Main Sidebar Header -->
              <div class="content-header">
                  <div class="header-section">
                      <h1 style="margin-left: 20px;">
                          {% block subheader %}{% endblock %}<br><small>{% block description %}{% endblock %}</small>
                      </h1>
                  </div>
              </div>
              {# <ul class="breadcrumb breadcrumb-top">
                  <li>Page Layouts</li>
                  <li><a href="">Visible Main Sidebar</a></li>
              </ul> #}
              <!-- END Visible Main Sidebar Header -->

                <!-- Dashboard 2 Content -->
                <div class="row">
                    {% block content %} {% endblock %}
                </div>
                <!-- END Dashboard 2 Content -->

            </div>
            <!-- END Page Content -->

            {% block footer %}
            {# <footer class="bg-inverse navbar navbar-fixed-bottom" style="position: relative; z-index: 1">
              <div class="row" style="margin-left: 50px; margin-right: 50px;">
                 <div class="col-sm-2 col-xs-5">
                    <nav>
                       <h5>Overview</h5>
                       <ul class="list-unstyled">
                          <li><a href="/feature">Feature</a>
                          </li>
                       </ul>
                    </nav>
                 </div>
                 <div class="col-sm-2 col-xs-5">
                    <nav>
                       <h5>CONTACT</h5>
                       <ul class="list-unstyled">
                          <li><a href="#">Support</a>
                          </li>
                          <li><a href="#">Sales</a>
                          </li>
                       </ul>
                    </nav>
                 </div>
                 <div class="col-sm-2 col-xs-5">
                    <nav>
                       <h5>COMPANY</h5>
                       <ul class="list-unstyled">
                          <li><a href="#">About</a>
                          </li>
                          <li><a href="#">Press Kit</a>
                          </li>
                          <li><a href="#">Education</a>
                          </li>
                          <li><a href="#">Non-profits</a>
                          </li>
                       </ul>
                    </nav>
                 </div>
                 <div class="col-sm-2 col-xs-5">
                    <nav>
                       <h5>Say HELLO</h5>
                       <ul class="list-unstyled">
                          <li><a href="#">Facebook</a>
                          </li>
                          <li><a href="#">Twitter</a>
                          </li>
                          <li><a href="#">Google+</a>
                          </li>
                       </ul>
                    </nav>
                 </div>
                 <div class="col-sm-4 col-xs-10">
                    <form action="" method="post">
                       <h5>SUBSCRIBE TO OUR MONTHLY NEWSLETTER!</h5>
                       <div class="input-group">
                          <input type="email" name="email" placeholder="mail@example.com" required="true" class="form-control">
                          <span class="input-group-btn">
                             <button type="submit" class="btn btn-success">Join</button>
                          </span>
                       </div>
                       <!--/input-group -->
                    </form>
                    <p class="text-muted">
                       <small>We will never spam and you can unsubscribe at any time</small>
                    </p>
                 </div>
                 <div class="col-xs-12">
                    <p style="text-align: center;">Copyright 2015 | <a href="#">Terms of Service </a>| <a href="#">Privacy Policy</a>
                    </p>
                 </div>
              </div>
            </footer> #}
            {% endblock %}
            <!-- END Footer -->
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

{# <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
<div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"><i class="fa fa-pencil"></i> Settings</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                <form action="index.html" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" onsubmit="return false;">
                    <fieldset>
                        <legend>Vital Info</legend>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Username</label>
                            <div class="col-md-8">
                                <p class="form-control-static">Admin</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-email">Email</label>
                            <div class="col-md-8">
                                <input type="email" id="user-settings-email" name="user-settings-email" class="form-control" value="admin@example.com">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-notifications">Email Notifications</label>
                            <div class="col-md-8">
                                <label class="switch switch-primary">
                                    <input type="checkbox" id="user-settings-notifications" name="user-settings-notifications" value="1" checked>
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Password Update</legend>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-password">New Password</label>
                            <div class="col-md-8">
                                <input type="password" id="user-settings-password" name="user-settings-password" class="form-control" placeholder="Please choose a complex one..">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
                            <div class="col-md-8">
                                <input type="password" id="user-settings-repassword" name="user-settings-repassword" class="form-control" placeholder="..and confirm it!">
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group form-actions">
                        <div class="col-xs-12 text-right">
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-sm btn-primary">Save Changes</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!-- END User Settings --> #}

{% block modal %}
{% endblock %}

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="js/helpers/excanvas.min.js"></script><![endif]-->

<!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
{{ javascript_include('//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js') }}
<script>!window.jQuery && document.write(decodeURI('%3Cscript src="third-party/proui/backend/js/vendor/jquery-1.11.2.min.js"%3E%3C/script%3E'));</script>

<!-- Bootstrap.js, Jquery plugins and Custom JS code -->
{{ javascript_include("js/contact/handlebars-v2.0.0.js") }}
{{ javascript_include('third-party/proui/backend/js/vendors/bootstrap.min.js') }}
{{ javascript_include("third-party/47admin/moment/min/moment.min.js") }}
{{ javascript_include('third-party/proui/backend/js/plugins.js') }}
{{ javascript_include('third-party/47admin/moment/min/moment-with-langs.min.js')}}
<!-- {{ javascript_include("third-party/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")}} -->
{{ javascript_include("third-party/47admin/datetimepicker/js/bootstrap-datetimepicker.min.js")}}
{{ javascript_include('third-party/proui/backend/js/app.js') }}

{{ javascript_include('third-party/proui/backend/js/animo.js') }}
{{ javascript_include('third-party/proui/backend/js/fastclick.js') }}

<!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps -->
{{ javascript_include('//maps.google.com/maps/api/js?sensor=true') }}
{{ javascript_include("third-party/47admin/gmap/jquery.gmap.min.js") }}

<!-- App Main-->

{{ javascript_include("js/app.js") }}

<script>
/* $(document).ready(function(){
  $('#content').addClass("padding220");
  $('#togDashboard').on('click', null, function(event) {
    $('#dashboard').toggle();
    $('#content').toggleClass("padding50");
  });

});

$(function() {
    $('#dashboard li').on('click', null, function(event) {
        $('li.active').removeClass('active');
        $(this).addClass('active');
    });
}); */
</script>

 <!-- START Page Custom Script-->
 {% block script %}
 {% endblock %}
 <!-- END Page Custom Script-->

</html>