{% extends "templates/home.volt" %}

{% block head %}
{% endblock %}

{% block content %}
<section class="main-content">
    <!-- Inbox Content -->
    <div class="row">
        <!-- Inbox Menu -->
        <div class="col-sm-4 col-lg-3">

            <!-- Tags Block -->
            <div class="block full">
                <!-- Tags Title -->
                <div class="block-title clearfix">
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Add Tag"><i class="fa fa-plus"></i></a>
                    </div>
                    {# <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i></a>
                    </div>
                    <div class="block-options pull-left">
                        <a href="../message/compose" class="btn btn-alt btn-sm btn-default"><i class="fa fa-pencil"></i> Compose Message</a>
                    </div> #}
                </div>
                <!-- END Tags Title -->

                <!-- Tags Content -->
                <ul class="nav nav-pills nav-stacked">
                    <li>
                        <a href="../message/announce">
                            <span class="badge pull-right">{{ totalUnreadAnnouncements }}</span>
                            <i class="fa fa-tag fa-fw text-success"></i> <strong>Announcements</strong>
                        </a>
                    </li>
                    <li>
                        <a href="../message/index">
                            <span class="badge pull-right">{{ totalUnreadMessageCount }}</span>
                            <i class="fa fa-tag fa-fw text-info"></i> <strong>Contacts</strong>
                        </a>
                    </li>
                    <li>
                        <a href="../message/notification">
                            <span class="badge pull-right">{{ totalUnreadNotificationCount }}</span>
                            <i class="fa fa-tag fa-fw text-danger"></i> <strong>Notifications</strong>
                        </a>
                    </li>
                </ul>
                <!-- END Tags Content -->
            </div>
            <!-- END Tags Block -->
        </div>
        <!-- Messages List -->
        <div class="col-sm-8 col-lg-9">
            <!-- Chat Block -->
             <div class="block">
                 <!-- Title -->
                 <div class="block-title">
                     <div class="block-options pull-right">
                         <div class="btn-group btn-group-sm">
                             <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default dropdown-toggle enable-tooltip" data-toggle="dropdown" title="Options"><i class="fa fa-cog"></i></a>
                             <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                 <li class="">
                                     <a href="javascript:void(0)">Report User</a>
                                 </li>
                             </ul>
                         </div>
                     </div>
                     <h2><i class="fa fa-pencil animation-pulse"></i> <strong>Shirley</strong> Wells</h2>
                 </div>
                 <!-- END Title -->

                 <!-- Content -->
                 <div class="chatui-container block-content">

                     <!-- Talk -->
                     <div class="chatui-talk" style="width:100%">
                         <div class="chatui-talk-scroll">
                             <ul>
                                 <li class="text-center"><small>yesterday, 23:10</small></li>
                                 <li class="chatui-talk-msg">
                                     <img src="img/placeholders/avatars/avatar6.jpg" alt="Avatar" class="img-circle chatui-talk-msg-avatar"> Hey admin?
                                 </li>
                                 <li class="chatui-talk-msg">
                                     <img src="img/placeholders/avatars/avatar6.jpg" alt="Avatar" class="img-circle chatui-talk-msg-avatar"> How are you?
                                 </li>
                                 <li class="text-center"><small>just now</small></li>
                                 <li class="chatui-talk-msg chatui-talk-msg-highlight themed-border">
                                     <img src="img/placeholders/avatars/avatar2.jpg" alt="Avatar" class="img-circle chatui-talk-msg-avatar"> I'm fine, thanks!
                                 </li>
                             </ul>
                         </div>
                     </div>
                     <!-- END Talk -->

                     <!-- Input -->
                     <div class="chatui-input" style="width:100%">
                         <form action="#" method="post" class="form-horizontal form-control-borderless remove-margin">
                             <div class="input-group">
                                 <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>
                                 <textarea type="text" id="chatui-message" name="chatui-message" class="form-control input-lg" placeholder="Type a message and hit enter.."></textarea>
                             </div>
                         </form>
                     </div>
                     <!-- END Input -->
                 </div>
                 <!-- END Content -->
             </div>
             <!-- END Chat Block -->
        </div>
        <!-- END Messages List -->
    </div>
    <!-- END Inbox Content -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

</section>

{% endblock %}

{% block modal %}
{% endblock %}

{% block footer %}
{% endblock %}


{% block script %}
<!-- Load and execute javascript code used only in this page -->
{{ javascript_include ('third-party/proui/backend/js/pages/readyInbox.js') }}
<script>$(function(){ ReadyInbox.init(); });</script>
{{ javascript_include ('third-party/proui/backend/js/pages/readyChat.js') }}
<script>$(function(){ ReadyChat.init(); });</script>

<script>
$(document).ready(function(){
    function updateMessageCount(){
        $.getJSON("/message/count", function(data){
            if(data.success){
                var el = $("#message-count");
                if(data.content.count!==el.data("count")){
                    el.data("get", true);
                    el.html("&nbsp;("+data.content.count+" unread messages)").data("count", data.content.count);
                }else{
                    el.data("get", false);
                }
            };
            //  console.log(data);
        });
    };

    function retrieveMessages(){
        var offset = $("#message-container").children("p").size();
        var count = $("#message-count").data("count") || 0;
        $.getJSON("/message/get", {offset: offset, count: count}, function(data){
            if(data.success){
                var msgs = data.content.messages;
                $.each(msgs, function(idx, msg){
                    var el = $("<p>"+msg.firstname+" "+msg.lastname+" - "+msg.title+"<br/>"+msg.body+"</p>");
                    el.data("id", msg.id);
                    $("#message-container").append(el);
                });
                //data.content.messages); 
            }
            console.log(data);
        });
    };

    updateMessageCount();
    retrieveMessages();
    
    window.setInterval(function(){
        updateMessageCount();
        if($("#message-count").data("get")==true) retrieveMessages();
    }, 5000);
});
</script>
{% endblock %}