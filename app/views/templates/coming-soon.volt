<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="not-ie" lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="An online platform connecting teachers with students who need their services">
<meta name="keywords" content="QuikWit, skill, teach, learn">
<meta name="google-site-verification" content="cFsMhkd83JMCqyKiH8IvlChkmS5XJA1rXtxtvrzYcfs" />

{% block title %}
<title>QuikWit coming soon!</title>
{% endblock %}

{% block head %}
{% endblock %}

 <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--style sheet-->
{{ stylesheet_link("optimized/css/bootstrap.css") }}
{{ stylesheet_link("third-party/me/css/font-awesome/css/font-awesome.css") }}
{{ stylesheet_link("third-party/me/css/fontello/css/fontello.css") }}
{{ stylesheet_link("third-party/me/css/styles.css") }}
{{ stylesheet_link("optimized/css/animate.css") }}
{{ stylesheet_link("http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Montserrat:400,700") }}
{{ stylesheet_link('http://fonts.googleapis.com/css?family=Pacifico') }}

<!--color stylesheet -->
{{ stylesheet_link("optimized/css/custom.css") }}

</head>

<body>

<!-- Preloader -->
   <div class="mask">
      <div class="bubblingG">
         <span id="bubblingG_1">
         </span>
         <span id="bubblingG_2">
         </span>
         <span id="bubblingG_3">
         </span>
      </div>
   </div>
<!-- Preloader end -->

<!-- START PAGE WRAPPER -->
<div id="page-wrapper">
<!-- Section : Home -->
   <section id="home" class="parallax" {% block header %}style="background-image: url('/img/coming-soon/mpushup.jpg'); height: 90%; min-height: 600px"{% endblock %} data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
      <div class="parallax-overlay">   
         <div class="container">
            <div class="col-md-12">

            <!-- Personal Info -->  
             <div class="info">              
                {% block top %}
                <div class="row">
                  <div class="col-md-5 col-sm-4"></div>
                  <div class="col-md-5 col-sm-6">
                    {# <h3>Sign up below!</h3> #}
                    <!-- contact from -->
                    <div id="messages">{{ flash.output() }}</div>
                    <h3 style="color: white;">Link Students & Teachers | Local</h3>

                    {{ form("coming-soon/register", "id" : "fmnewsletter", "method" : "post", "class" : "form") }}
                      {# {{ text_field('name', 'required' : true, 'placeholder' : 'Name') }}<br/> #}
                      {{ email_field('email', 'required' : true, 'placeholder' : 'Pre-register with your email') }}
                      <button type="submit" class="btn btn-info form-control"  id="submit">Get Notified!</button>
                    {{ end_form() }}
                    
                    <h2>Share it. Live it.</h2>
                    <h4 style="color:white;">Coming Soon In Fall 2015!</h4>
                    <!-- View more -->                 
                    <div class="gotosection">
                      <a href="/coming-soon#what"><i class="fa fa-angle-double-down "></i></a>
                    </div>
                    <!-- View more end -->
                  </div>
                </div>                
                {% endblock %}
                {% block signup %}
                {% endblock %}
             </div>
            <!-- Personal Info end -->     
            </div>
         </div>
      </div>
   </section>
<!-- Section : Home end -->

<!-- Navigation -->
<div class="main-nav" id="nav">

   <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
     <div class="container-fluid">                
       <!-- Brand and toggle get grouped for better mobile display -->
       <div class="navbar-header">
         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
           <span class="sr-only">Toggle navigation</span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
         </button>
         <a class="nav-brand" href="/coming-soon">
            <img alt="Brand" src="/optimized/image/QW5.png" style="height: 57px; width: 164px; margin-left: 70px; margin-top: 5px;">
         </a>
       </div>

       <!-- Collect the nav links, forms, and other content for toggling -->
       <div class="collapse navbar-collapse menu" id="bs-example-navbar-collapse-1">
         <ul class="nav navbar-nav">
            <li><a href="{{ url('coming-soon') }}" class="colapse-menu1" style="color:#000000;"><strong>Home</strong></a></li>
            <li><a href="{{ url('coming-soon/about') }}" class="colapse-menu1" style="color:#000000;"><strong>About Us</strong></a></li>
            <li><a href="{{ url('coming-soon/timeline') }}" class="colapse-menu1" style="color:#000000;"><strong>Timeline</strong></a></li>
            <li><a href="{{ url('coming-soon/teacher') }}" class="colapse-menu1" style="color:#000000;"><strong><i>For Teachers</i></strong></a></li>
            {# <li><a href="{{ url('coming-soon/contact')}}" class="colapse-menu1" style="color:#000000;">Contact</a></li>
            <li><a href="{{ url('coming-soon/blog')}}" class="colapse-menu1" style="color:#000000;">Blog</a></li> #}
         </ul>
       </div><!-- /.navbar-collapse -->
     </div><!-- /.container-fluid -->
   </nav>

</div>
<!-- End of Navigation -->

{% block section %}
{% endblock %}

<!-- Footer -->
<footer>
   <div class="container">
    <div class="row">
      <div class="col-md-5">
        {# <nav class="navbar" role="navigation">
          <ul class="navbar-nav" id="navs">
            <li><a href="{{ url('coming-soon/press')}}">Press</a></li> 
            <li><a href="{{ url('coming-soon/faq')}}">FAQs</a></li>
            <li><a href="{{ url('coming-soon/job')}}">Jobs</a></li> 
          </ul>
        </nav> #}
        <h5 style="color: white; margin-top: 40px;">Email: general@quikwit.co</h5>
      </div> 
      <div class="col-md-3">
      </div>
      <div class="col-md-3">
        <!--<ul class="get_in">
          <li><i class="fa fa-envelope-o"></i><p>hello@metrothemes.me</p></li>
          <li><i class="fa fa-phone"></i><p>+0123 456 789</p></li>        
          <li><i class="fa fa-map-marker"></i><p>21 Cool Street, Melbourne Victoria 3000 Australia</p></li>
          <li><a href="#"><i class="fa fa-cloud-download"></i><p>Download Vcard</p></a></li>  
        </ul>-->
        <!-- social Icons -->
            <ul class="social">
                <li><a class="facebook" href="https://www.facebook.com/quikwit"></a></li>
                <li><a class="twitter" href="https://twitter.com/quikwithq"></a></li>
                {# <li><a class="pinterest" href="https://pinterest.com/quikwithq"></a></li>
                <li><a class="google" href="google.com/+Quikwit"></a></li> #}
            </ul>
        <!-- social Icons end -->
      </div>
      {# <div class="col-md-2">
        <nav>
          <ul class="nav">
            <li><a href="/">Privacy Policy</a></li>
            <li><a href="/">Terms Of Service</a></li>
          </ul>
        </nav>
      </div> #}
      <div class="copyright col-md-12"> &copy; 2015 Quikwit Inc. All rights reserved.</div>            
   </div>
</footer>
<!-- Footer end -->
</div>
<!-- END PAGE WRAPPER -->
<!--JavaScript-->

{{ javascript_include("optimized/js/jquery.js") }}
{{ javascript_include("optimized/js/bootstrap.min.js") }}
{{ javascript_include("optimized/js/scripts.js") }}
{{ javascript_include("optimized/js/jquery.form.js") }}

{% block script %}
{% endblock %}

</body>
</html>
