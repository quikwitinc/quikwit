{% extends "templates/home.volt" %}

{% block head %}
{% endblock %}

{% block content %}
<section class="main-content">
    <!-- Inbox Content -->
    <div class="row">
        <!-- Inbox Menu -->
        <div class="col-sm-4 col-lg-3">

            <!-- Tags Block -->
            <div class="block full">
                <!-- Tags Title -->
                <div class="block-title clearfix">
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Add Tag"><i class="fa fa-plus"></i></a>
                    </div>
                    {# <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i></a>
                    </div>
                    <div class="block-options pull-left">
                        <a href="../message/compose" class="btn btn-alt btn-sm btn-default"><i class="fa fa-pencil"></i> Compose Message</a>
                    </div> #}
                </div>
                <!-- END Tags Title -->

                <!-- Tags Content -->
                <ul class="nav nav-pills nav-stacked">
                    <li>
                        <a href="../message/announce">
                            <span class="badge pull-right">1680</span>
                            <i class="fa fa-tag fa-fw text-success"></i> <strong>Announcements</strong>
                        </a>
                    </li>
                    <li>
                        <a href="../message/index">
                            <span class="badge pull-right">350</span>
                            <i class="fa fa-tag fa-fw text-info"></i> <strong>Contacts</strong>
                        </a>
                    </li>
                    <li>
                        <a href="../message/notification">
                            <span class="badge pull-right">651</span>
                            <i class="fa fa-tag fa-fw text-danger"></i> <strong>Notifications</strong>
                        </a>
                    </li>
                </ul>
                <!-- END Tags Content -->
            </div>
            <!-- END Tags Block -->
        </div>
        <!-- Messages List -->
        <div class="col-sm-8 col-lg-9">
            <!-- Chat Block -->
            <div class="panel panel-default">
                <!-- Title -->
                <div class="panel-heading">
                    <i class="fa fa-pencil"></i> <strong>To:</strong> &nbsp
                    <input class="form-control-borderless" placeholder="Username">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-share"></i>Send</button> &nbsp
                    <a href="#" data-perform="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
                       <em class="fa fa-minus"></em>
                    </a>
                </div>
                <!-- END Title -->

                <!-- Content -->
                <div class="panel-body" style="padding: 0px;">
                    <!-- Textarea -->
                    <textarea name="message" class="form-control" placeholder="Type your message here" style="height: 400px;"></textarea>
                    <!-- END Textarea -->
                </div>
                <!-- END Content -->
            </div>
            <!-- END Chat Block -->
        </div>
        <!-- END Messages List -->
    </div>
    <!-- END Inbox Content -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

</section>

{% endblock %}

{% block modal %}
<!-- START modal-->
<div id="messageRoom" tabindex="-1" role="dialog" aria-labelledby="messageLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="messageLabel" class="modal-title">Message</h4>
            </div>
            <div class="modal-body">
               <!-- Chat Block -->
               <div class="block">
                   <!-- Title -->
                   <div class="block-title">
                       <div class="block-options pull-right">
                           <div class="btn-group btn-group-sm">
                               <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default dropdown-toggle enable-tooltip" data-toggle="dropdown" title="Options"><i class="fa fa-cog"></i></a>
                               <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                   <li class="">
                                       <a href="javascript:void(0)">Report User</a>
                                   </li>
                               </ul>
                           </div>
                       </div>
                       <h2><i class="fa fa-pencil animation-pulse"></i> <strong>Shirley</strong> Wells</h2>
                   </div>
                   <!-- END Title -->

                   <!-- Content -->
                   <div class="chatui-container block-content-full">

                       <!-- Talk -->
                       <div class="chatui-talk" style="width: 550px;">
                           <div class="chatui-talk-scroll">
                               <ul>
                                   <li class="text-center"><small>yesterday, 23:10</small></li>
                                   <li class="chatui-talk-msg">
                                       <img src="img/placeholders/avatars/avatar6.jpg" alt="Avatar" class="img-circle chatui-talk-msg-avatar"> Hey admin?
                                   </li>
                                   <li class="chatui-talk-msg">
                                       <img src="img/placeholders/avatars/avatar6.jpg" alt="Avatar" class="img-circle chatui-talk-msg-avatar"> How are you?
                                   </li>
                                   <li class="text-center"><small>just now</small></li>
                                   <li class="chatui-talk-msg chatui-talk-msg-highlight themed-border">
                                       <img src="img/placeholders/avatars/avatar2.jpg" alt="Avatar" class="img-circle chatui-talk-msg-avatar"> I'm fine, thanks!
                                   </li>
                               </ul>
                           </div>
                       </div>
                       <!-- END Talk -->

                       <!-- Input -->
                       <div class="chatui-input">
                           <form action="#" method="post" class="form-horizontal form-control-borderless remove-margin">
                               <div class="input-group">
                                   <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>
                                   <input type="text" id="chatui-message" name="chatui-message" class="form-control input-lg" placeholder="Type a message and hit enter.." style="width: 400px;">
                               </div>
                           </form>
                       </div>
                       <!-- END Input -->
                   </div>
                   <!-- END Content -->
               </div>
               <!-- END Chat Block -->
            </div>
         </div>
      </div>
   </div>
<!-- END modal-->
{% endblock %}

{% block footer %}
{% endblock %}


{% block script %}
<!-- Load and execute javascript code used only in this page -->
<script src="third-party/proui/backend/js/pages/readyInbox.js"></script>
<script>$(function(){ ReadyInbox.init(); });</script>
{{ javascript_include ('js/message/compose.js') }}

{% endblock %}