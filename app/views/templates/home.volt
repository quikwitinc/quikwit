<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>{% block title %}Welcome to QuikWit - Login or Sign up{% endblock %}</title>

        <meta name="description" content="QuikWit is an online directory listing freelancers providing local services in Canada.">
        <meta name="robots" content="NOODP">

        <!-- Twitter Card data -->
        <meta name="twitter:card" value="QuikWit is an online directory listing freelancers providing local services in Canada.">

        <!-- Open Graph data -->
        <meta property="og:title" content="Welcome to QuikWit - Login or Sign up" />
        <meta property="og:type" content="Web Application" />
        <meta property="og:url" content="https://www.quikwit.co" />
        <meta property="og:image" content="{{ url('img/optimized/mscreen') }}" />
        <meta property="og:description" content="QuikWit is an online directory listing freelancers providing local services in Canada." />

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <meta property="fb:app_id" content="770147199741530" />

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ url('img/favicon/favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ url('img/favicon/favicon.ico') }}" type="image/x-icon">
        <link rel="apple-touch-icon" sizes="57x57" href="{{ url('img/favicon/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ url('img/favicon/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ url('img/favicon/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ url('img/favicon/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ url('img/favicon/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ url('img/favicon/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ url('img/favicon/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ url('img/favicon/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ url('img/favicon/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ url('img/favicon/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ url('img/favicon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ url('img/favicon/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ url('img/favicon/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ url('img/favicon/manifest.json') }}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ url('img/favicon/ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        {{ stylesheet_link("third-party/proui/frontend/css/bootstrap.min.css") }}

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        {{ stylesheet_link("third-party/proui/frontend/css/frontmain.css") }}

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        {{ stylesheet_link("third-party/proui/backend/css/backmain.css") }}

        <!-- Related styles of various icon packs and plugins -->
        {{ stylesheet_link("third-party/proui/frontend/css/frontplugins.css") }}

        <!-- Related styles of various icon packs and plugins -->
        {{ stylesheet_link("third-party/proui/backend/css/backplugins.css") }}

        <!-- Datetimepicker -->
        {{ stylesheet_link("third-party/47admin/datetimepicker/css/bootstrap-datetimepicker.min.css") }}

        <!-- App CSS-->
        {{ stylesheet_link('css/app.css') }}
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <!-- END Stylesheets -->

        <!-- Hopscotch -->
        {{ stylesheet_link("third-party/hopscotch/dist/css/hopscotch.css") }}
        <!-- END Hopscotch -->

        <!-- START Page Custom CSS-->
        {% block head %}
        {% endblock %}
        {% block style %}
        <style>
        .site-nav-link {
            color: #1b92ba;
        }
        .ui-autocomplete {
            z-index: 1000;
        }
        .gi-1.4x{font-size: 1.4em;}
        .gi-3x{font-size: 3em;}
        .gi-4x{font-size: 4em;}
        .gi-5x{font-size: 5em;}
        </style>
        {% endblock %}
        <!-- END Page Custom CSS-->

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        {{ stylesheet_link("third-party/proui/backend/css/backthemes.css") }}
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) & Respond.js (enables responsive CSS code on browsers that don't support it, eg IE8) -->
        {{ javascript_include("third-party/proui/backend/js/vendors/modernizr-respond.min.js") }}

        {% block fbTrack %}
        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '1634884940101252');
        fbq('track', "PageView");
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1634884940101252&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
        {% endblock %}

    </head>
    <body style="background-color: #f7f7f7;" id="page-wrapper" class="page-loading">
        <!-- Page Container -->
        <!-- In the PHP version you can set the following options from inc/config file -->
        <!-- 'boxed' class for a boxed layout -->

        <div class="preloader themed-background">
            <h1 class="push-top-bottom text-light text-center"><strong>Nutcracking...</strong></h1>
            <div class="inner">
                <h3 class="text-light visible-lt-ie9 visible-lt-ie10"><strong>In progress..</strong></h3>
                <div class="preloader-spinner hidden-lt-ie9 hidden-lt-ie10"></div>
            </div>
        </div>

        <?php $this->flash->output(); ?>
        <div id="page-container" style="position: relative;">
            <!-- Site Header -->
            {% block navbar %}<header id="navbar" style="padding-bottom: 10px; background-color: rgb(255,255,255); border-bottom: solid 1px; border-color: #1BBAE1">
                <div style="padding-right: 50px;">
                    <!-- Site Logo -->
                    <a href="{{ url('') }}" id="quikwit-logo" class="site-logo hidden-xs hidden-sm" style="margin-top: -15px; margin-left: 20px;">
                        <img alt="Brand" id="logo-md" src="/img/landing/QW10.png" style="height: 70px;">
                    </a>
                    <a href="{{ url('') }}" class="site-logo visible-sm" style="margin-top: -15px; margin-left: 20px;">
                        <img alt="Brand" id="logo-sm" src="/img/landing/QW2.png" style="height: 55px;">
                    </a>
                    <a href="{{ url('') }}" class="site-logo visible-xs" style="margin-top: -15px; margin-left: 5px;">
                        <img alt="Brand" id="logo-xs" src="/img/landing/QW2.png" style="height: 55px;">
                    </a>
                    <!-- Site Logo -->
                    <form role="search" method="get" id="search-validation" class="search-form form-horizontal col-md-4 col-sm-10 col-xs-8" action="search/index" style="padding: 0px;">
                        <div class="form-group push-bit">
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <input type="text" name="q" style="border-color: #1BBAE1;" class="form-control" placeholder="What service are you looking for?" id="searchService"
                                           value="<?php if (isset($_GET['q'])) { 
                                                            echo $_GET['q']; 
                                                        } else {
                                                            echo "";
                                                        } ?>">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-primary" style="padding-top: 3px; height: 34px;"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- Site Navigation -->
                  
                    <nav >
                        <!-- Menu Toggle -->
                        <!-- Toggles menu on small screens -->
                        <a href="javascript:void(0)" class="btn btn-default site-menu-toggle visible-xs visible-sm" style="margin-right: -30px; margin-top: -2px;">
                            <i class="fa fa-bars"></i>
                        </a>
                        <!-- END Menu Toggle -->

                        <!-- Main Menu -->
                        <ul class="site-nav"  >
                            <!-- Toggles menu on small screens -->
                            <li class="visible-xs visible-sm">
                                <a href="javascript:void(0)" class="site-menu-toggle text-center">
                                    <i class="fa fa-times"></i>
                                </a>
                            </li>
                            <!-- END Menu Toggle -->
                            {# <li>
                                <a href="javascript:void(0)" class="site-nav-sub"><i class="fa fa-angle-down site-nav-arrow"></i>Browse Categories</a>
                                <ul>
                                    <li>
                                        <a href="#">Art & Crafts</a>
                                        <a href="#">Health & Fitness</a>
                                        <a href="#">Music</a>
                                        <a href="#">IT & Software</a>
                                        <a href="#">Academic</a>
                                        <a href="#">Culinary</a>
                                        <a href="#">Language</a>
                                    </li>
                                </ul>
                            </li> #}
                            {# <li>
                                <a href="{{ url('review') }}">Write a Review</a>
                            </li> #}
                            
                            <li>
                                <a href="/post" id="postLink" class="site-nav-link" style="border: solid 1px #1bbae1;">Post a Gig</a>
                            </li>
                            <li>
                                <a href="/browse" class="site-nav-link">Browse Gigs</a>
                            </li>
                            <li>
                                <a href="/how-to-hire" id="proLink" class="site-nav-link">How to Hire</a>
                            </li>
                            {% if user is defined %}
                            <li>
                                <a href="javascript:void(0)" class="site-nav-link" data-target="#feedback" data-toggle="modal">Give Feedback</a>
                            </li>
                            {#<li>
                                <a href="javascript:void(0)" id="startTourBtn">Take a tour</a>
                            </li>#}
                            {% endif %}
                            {% if user is empty %}
                            <li>
                                <a href="{{ url('#signup') }}" class="site-nav-link">Sign Up</a>
                            </li>
                            {% endif %}
                            {% if user is defined %}
                            <li>
                                <a href="/overview" id="myAccount" class="site-nav-sub site-nav-link"><i class="fa fa-angle-down site-nav-arrow"></i>Hi, {{ firstname }} {% if unreadMessageCount > 0 %} <span class="badge pull-down" style="background: #30D611; padding: 5px 1px;"> </span>{% endif %}</a> 

                                {# <a href="javascript:void(0)" id="myAccount" class="site-nav-sub"><i class="fa fa-angle-down site-nav-arrow">  </i>Hi, {{ firstname }} <span id="user_messages_count">{% if unreadMessageCount > 0 %} <span class="badge pull-down" style="background: rgb(36, 232, 0); padding: 5px 1px;"> </span>{% endif %}</span></a> #}

                                <ul style="border: solid 1px; border-color: #1BBAE1">
                                    <li>
                                        <a href="/message" {% if unreadMessageCount > 0 %} style="color: #30D611" {% endif %}>Messages 
                                            <i class="fa fa-envelope-o fa-fw pull-right" {% if unreadMessageCount > 0 %} style="color: #30D611" {% endif %}></i>       
                                        </a>
                                    </li>
                                    {% if userTeach == 1 %}
                                    <li>
                                        <a href="/gig" {% if unreadGigCount > 0 %} style="color: #30D611" {% endif %}>Matching Gigs
                                            <i class="fa fa-folder-open-o fa-fw pull-right" {% if unreadGigCount > 0 %} style="color: #30D611" {% endif %}></i>
                                        </a>
                                    </li>
                                    {% endif %}
                                    <li>
                                        <a href="/post/postingList">My Posts
                                            <i class="fa fa-spinner fa-fw pull-right"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/contacts">Contacts
                                            <i class="fa fa-user-plus fa-fw pull-right"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/contract">Manage
                                            <i class="fa fa-tasks fa-fw pull-right"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/overview">Profile
                                        <i class="fa fa-pencil fa-fw pull-right"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/settings" id="settings">Settings
                                            <i class="fa fa-cog fa-fw pull-right"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/faq">FAQ
                                            <i class="fa fa-question fa-fw pull-right"></i>
                                        </a>
                                    </li>
                                    <li>
                                        {{ link_to("signout", "Logout") }}
                                    </li>
                                </ul>
                            </li>
                            {% else %}
                            <li>                               
                                <a href="{{ url('signin')}}" class="site-nav-link">Login</a>
                            </li>
                            {% endif %}
                        </ul>
                        <!-- END Main Menu -->
                    </nav>
               
                    <!-- END Site Navigation -->
                </div>
            </header>
            {% endblock %}
            <!-- END Site Header -->

            {% block content %}
            {% endblock %}

            {% block footer %}
            <!-- Footer -->
            <footer class="site-footer site-section" style="padding-bottom: 0px; bottom: 0px; left: 0px; width: 100%;">
                <div class="container">
                    <!-- Footer Links -->
                    <div class="row text-center">
                        <div class="col-sm-3 col-xs-12">
                            <h4 class="footer-heading">QuikWit</h4>
                            <ul class="footer-nav list-unstyled">
                                <li><a href="/about">Company</a></li>
                                {# <li><a href="/blog">Blog</a></li> #}
                                <li><a href="/contact-us">Contact</a></li>
                                {# <li><a href="/support">Support</a></li> #}
                                <li><a href="/faq">FAQ</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <h4 class="footer-heading">Additional Info</h4>
                            <ul class="footer-nav list-unstyled">
                                <li><a href="/how-to-hire">How to Hire</a></li>
                                <li><a href="/pricing">Pricing</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <h4 class="footer-heading">Legal</h4>
                            <ul class="footer-nav list-unstyled">
                                <li><a href="/terms" target="_blank">Terms of Use</a></li>
                                <li><a href="/terms/privacy" target="_blank">Privacy Policy</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <h4 class="footer-heading">Follow Us</h4>
                            <ul class="footer-nav footer-nav-social list-inline">
                                <li><a href="https://www.facebook.com/quikwit" target="_blank"><i class="fa fa-2x fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/quikwithq" target="_blank"><i class="fa fa-2x fa-twitter"></i></a></li>
                                {# <li><a href="https://pinterest.com/quikwithq)"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="https://plus.google.com/117235860072830632821"><i class="fa fa-google-plus"></i></a></li> #}
                            </ul>
                        </div>
                    </div>
                    <br>
                    <!-- END Footer Links -->
                    <div class="col-xs-12 text-center">                    
                        <ul class="footer-nav list-inline">
                            <li style="font-size: 18px;">&copy; 2015 Quikwit Inc.</li>
                            <li>All rights reserved.</li>
                        </ul>
                    </div>
                </div>
            </footer>
            <!-- END Footer -->
            {% endblock %}

        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-up"></i></a>

        <!-- START modal-->
           <div id="feedback" tabindex="-1" role="dialog" aria-labelledby="reportLabel" aria-hidden="true" class="modal fade">
              <div class="modal-dialog">
                 <div class="modal-content">
                    <div class="modal-header">
                       <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
                       <h4 id="RateLabel" class="modal-title">How can we give you a more awesome service?</h4>
                    </div>
                    <div class="modal-body">
                       <form class="form-horizontal">
                         <div class = "form-group">
                            <div class="col-md-12">
                                <textarea id="feedbackText" class="form-control" rows="8" placeholder="Tell us your thoughts. We want to hear it all."></textarea>
                            </div>
                         </div>
                       </form>
                    </div>
                    <div class="modal-footer">
                       <button type="button" class="btn btn-info" id="giveFeedback">Submit</button>
                       <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                    </div>
                 </div>
              </div>
           </div>
        <!-- END modal-->

        {% block modal %}
        {% endblock %}

        <!-- Include jQuery library from Google's CDN but if something goes wrong get jQuery from local file -->
        {{ javascript_include("//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js") }}
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="third-party/proui/frontend/js/vendors/jquery-1.11.2.min.js"%3E%3C/script%3E'));</script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

        <!-- Bootstrap.js, jQuery plugins and Custom JS code -->
        {{ javascript_include("third-party/proui/frontend/js/vendors/bootstrap.min.js") }}
        {{ javascript_include('third-party/proui/frontend/js/frontplugins.js') }}
        {{ javascript_include('third-party/proui/backend/js/backplugins.js') }}
        {{ javascript_include('third-party/proui/backend/js/backapp.js') }}


        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        {{ javascript_include("js/contact/handlebars-v2.0.0.js") }}
        {{ javascript_include("third-party/47admin/moment/min/moment.min.js") }}
        {{ javascript_include('third-party/47admin/moment/min/moment-with-langs.min.js')}}
        {{ javascript_include("third-party/47admin/datetimepicker/js/bootstrap-datetimepicker.min.js")}}

        {{ javascript_include('third-party/proui/backend/js/animo.js') }}
        {{ javascript_include('third-party/proui/backend/js/fastclick.js') }}

        <!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps -->
        {{ javascript_include("third-party/47admin/gmap/jquery.gmap.min.js") }}
        {{ javascript_include("https://maps.googleapis.com/maps/api/js?key=AIzaSyDt0ToOkmDQsw9jaGXFrR0oMW6lhqqi088", false) }}

        <!-- App Main-->
        {{ javascript_include("js/app.js") }}
        <!-- START Page Custom Script-->

        {{ javascript_include("third-party/proui/backend/js/pages/formsValidation.js") }}

         <!-- Hopscotch -->
        {{ javascript_include("third-party/hopscotch/dist/js/hopscotch.js") }}
        {{ javascript_include("js/user_onboard.js") }}
        <!-- END Hopscotch --> 

        {{ javascript_include("third-party/jsTimezoneDetect/jstz-1.0.4.min.js")}}

        {% block script %}
        {% endblock %}

        <script>
        $(document).ready(function(){

            $(function(){ FormsValidation.init(); });
            
            var tz = jstz.determine(); // Determines the time zone of the browser client
            var timezone = tz.name(); //'Asia/Kolhata' for Indian Time.
            /* $.post("/timezone/getTimezone", {tz: timezone}, function(data) {
               //Preocess the timezone in the controller function and get
               //the confirmation value here. On success, refresh the page.
               console.log(data);
            }); */

            $.ajax({
                url: '/timezone/getTimezone', 
                type: 'POST',
                data: {
                    tz : timezone
                },
                success: function(data){
                    console.log(timezone);
                }
            });

            <?php $tagsJSON = json_encode($tagsColumn); ?>
            var tags = <?php echo $tagsJSON; ?>;
            $("#searchService").autocomplete({
              source: tags
            }); 

            $("#giveFeedback").click(function(){
                var feedback = $("#feedbackText").val();
                $.ajax({
                    url: '/user/giveFeedback', 
                    type: 'POST',
                    data: {
                        feedback : feedback
                    },
                    success: function(data){
                        console.log(data);
                        window.location.reload();
                    }
                });
            });

            /* window.setInterval(function(){
                $.ajax({ 
                    url: '/index/retrieveUnreadCount',
                    success: function(data){
                        if (data.newMessages > 0)  {
                            var $cont = $('#user_messages_count');

                            $cont.html( data.newMessages + '<span class="badge pull-down" style="background: rgb(36, 232, 0); padding: 5px 1px;"> </span>');
                        }
                    }
                });
            }, 30000); */

        });
        </script>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-58890184-1', 'auto');
          ga('send', 'pageview');

        </script>
        <!-- END Page Custom Script-->

    </body>
</html>