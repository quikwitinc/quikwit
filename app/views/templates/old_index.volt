{% extends "templates/base.volt" %}

{% block head %}
<style>
body { 
  background: url("/img/coming-soon/smsnowboarding.jpg") no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
@media screen and (min-width: 1000px) {
    /* Styles go here */
    #signupPanel {
      position: relative;
      z-index: 1;
    }
}
</style>
{% endblock %}

{% block content %}
   <div class="col-lg-2 col-md-3 col-sm-4 col-xs-10" id="signupPanel" style="margin-left:50px; margin-bottom: 100px;">
   {% if user is empty %}
      <!-- START panel-->
      <div data-toggle="play-animation" data-play="fadeInUp" data-offset="0" class="panel panel-default panel-flat">
         <p class="text-center mb-lg">
            <br>
            <a href="#">
               <img src="/img/landing/QW1.png" alt="Image" class="block-center img-responsive" style="width:50%; height:50%">
            </a>
         </p>
         <div class="panel-body" style="margin-top:-20px;">
        		{{ form("register/doRegister", "id" : "register-form", "method" : "post") }}
               <div class="text-right mb-sm">Already have an account? <a href="{{ static_url('signin') }}" class="text-muted"> Sign in</a>
               </div>
               <hr>      
               <div class="form-group has-feedback">
                <label for="firstname" class="text-muted">First Name</label>
                	{{ text_field('firstname', "id" : "rform-firstname",  "type": "text", "placeholder" : "First Name", "class" : "form-control") }}
               </div>

               <div class="form-group has-feedback">
                 <label for="lastname" class="text-muted">Last Name</label>
                	{{ text_field('lastname', "id" : "rform-lastname",  "type": "text", "placeholder" : "Last Name", "class" : "form-control") }}
               </div>

                 <!--  <input id="email" type="email" placeholder="Enter email" class="form-control"> -->
               <div class="form-group has-feedback">
                	 <label for="email" class="text-muted">Email address</label>
                	{{ text_field('email', "id" : "rform-email",  "type": "email", "placeholder" : "Enter email", "class" : "form-control") }}
                  <span class="fa fa-envelope form-control-feedback text-muted"></span>
               </div>
               <div class="form-group has-feedback">
                	 <label for="username" class="text-muted">User Name:</label>
                	{{ text_field('username', "id" : "rform-username", "placeholder" : "User name", "class" : "form-control") }}
                  <span class="fa fa-smile-o form-control-feedback text-muted"></span>
               </div>
               <div class="form-group has-feedback">
                  <label for="password" class="text-muted">Password</label>
                  <!--  <input id="signupInputPassword1" type="password" placeholder="Password" class="form-control"> -->
                  {{ password_field('password', "id" : "rform-password", "placeholder" : "Password", "class" : "form-control") }}
                  <span class="fa fa-lock form-control-feedback text-muted"></span>
               </div>
               <div class="form-group has-feedback">
                  <label for="signupInputRePassword1" class="text-muted">Retype Password</label>
                  <!--  <input id="signupInputRePassword1" type="password" placeholder="Retype Password" class="form-control"> -->
                  {{ password_field('rpassword', "id" : "rform-repassword", "placeholder" : "Retype Password", "class" : "form-control") }}
                  <span class="fa fa-lock form-control-feedback text-muted"></span>
               </div>
               <div class="clearfix">
                  <div class="checkbox c-checkbox pull-left mt0">
                     <label>
                       <!-- <input type="checkbox" value="">  -->
                        {{ check_field("confirmation", "id" : "rform-confirmation") }}
                        <span class="fa fa-check"></span>I agree with the <a href="#">terms</a>
                     </label>
                  </div>
               </div>
               <button type="submit" class="btn btn-block btn-primary">Register</button>
           	{{ end_form() }}
         </div>
   	</div>
    <!-- END panel-->
  {% endif %}
  </div>
  {# <div class="col-md-4">
    {% if user is defined %}
    <div class="panel panel-default">
       <div class="panel-body">
          <legend>Welcome back, {{ user.firstName|capitalize }}</legend>
          <div class="col-xs-3">
            <img src="{{ user.usr_avatar }}" alt="Avatar">
          </div>
          <div class="col-xs-8">
            <h4>Where have you been lately?</h4>
            <a href="/review" class="btn btn-info">Write your review</a>
          </div>
       </div>
    </div>
    {% endif %}
  </div> #}
{% endblock %}

{% block script %}
	{{ javascript_include("third-party/jquery-validation/jquery.validate.min.js") }}
  {{ javascript_include("js/pages.js") }}
  {{ javascript_include("js/index/index.js") }}
{% endblock %}
