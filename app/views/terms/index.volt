{% extends "templates/home.volt" %}

{% block title %}
Terms of Service | QuikWit
{% endblock %}

{% block head %}

{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h1 class="text-center animation-slideDown"><strong>Terms of Use</strong></h1>

    </div>
</section>
<!-- END Intro -->

<!-- Terms -->
<section class="site-content site-section">
    <div class="container">
        <div class="site-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-weight: 400;">
			<h4><strong>Effective Date: September 21, 2015</strong></h4>
		
			<hr>
			Thank you for visiting <a href="https://quikwit.co" target="_blank"> www.quikwit.co</a> (<b>“Website”</b>). This site is owned and operated by QuikWit Inc. (<b>“QuikWit”, “us” or “we”</b>). These Terms of Use, the Privacy Policy and all policies posted on the Website set out the terms on which we offer all users access and use of our site, services, applications and tools (Collectively <b>“Services”</b>). These Terms of Use may be amended or updated by QuikWit from time to time without notice. It is your responsibility to review these Terms of Use for any changes. Using our services after any amendments of these Terms of Use shall signify your acceptance of the revised terms. Any new features added to the Service will be subject to the latest Terms of Use, unless stated otherwise.
			<br><br>
			<ul>
				<li>1.	<u>ABOUT THE SERVICE</u>: QuikWit is a platform that helps users looking for local services, such as legal help or installation of appliances, (<b>“Clients”</b>) find people who can fulfill that service (<b>“Freelancers”</b>) through the Website. Users interested in accessing the Service and connecting with Clients or Freelancers must create an account (“registered user”) on the Website. Information provided by registered users when creating an account will be available as a public, searchable profile on the Website. Clients and Freelancers are collectively referred to as “Users”. Users can register on behalf of a corporate entity, however QuikWit reserves the right to verify the corporation’s existence and the user’s affiliation with it. You acknowledge that QuikWit does not hire or employ Freelancers through the Website to fulfill services. Rather, QuikWit is merely a platform that connects Clients and Freelancers. It is the responsibility of Clients and Freelancers to verify any information provided by Users they connect with and to ensure that they meet their needs and requirements. QuikWit does not endorse, validate of verify the qualifications, skills or licenses of any Freelancer nor will be responsible for the performance, actions or inactions of any Freelancer.
				</li>
				<br>
				<li>2.	<u>AGE RESTRICTION</u>: If you are under the age of 18, your legal guardian or parent must consent to your use of the services provided through the website and must accept these Terms of Use on your behalf.
				</li>
				<br>
				<li>3.	<u>USING THE SERVICE</u>: QuikWit reserves the right (but will have no obligation) to remove or refuse to distribute any User Content and to terminate users or reclaim usernames. We can also choose to access, read, preserve and disclose any information as we believe is necessary to 
					<ul>
						<li>(i) satisfy any applicable law, regulation, legal process or governmental request,</li>
						<li>(ii) enforce these Terms of Use, including investigation of potential violations,</li> 
						<li>(iii) detect, prevent, or otherwise address fraud, security or technical issues,</li> 
						<li>(iv) respond to user support requests, or (v) protect the rights, property or safety of our users and the public.</li>
					</ul>
				</li>
				<br>
				<li>4.	<u>PROHIBITED ACTIONS</u>: When using the Service, you agree to respect all Users of the Website and will not request, offer or participate in any of the following prohibited activities:
					<ul>
						<li>a)	services that violates the law and/or legal rights of any person or entity in the jurisdiction of the User;</li>
						<li>b)	services related to spam, unsolicited services or products</li>
						<li>c)	pornographic or obscene services including sexual and escort services;</li>
						<li>d)	services that are false, misleading or fraudulent</li>
						<li>e)	infringe any third-party right</li>
						<li>f)	distribute viruses or any other computer code or technologies that will interrupt, destroy or limit the functionality of any computer software or hardware of the Website or any of the Users;</li>
						<li>g)	lottery, raffles, sweepstakes entries;</li>
						<li>h)	affiliate marketing, chain letters or pyramid schemes;</li>
						<li>i)	copy, modify, or distribute any other person’s content without explicit permission from the owner of the content;</li>
						<li>j)	pretend to be any person or entity or misrepresent your affiliation with a person or entity; </li>
						<li>k)	impose an unreasonable load on our infrastructure or interfere with the proper working of the Website;</li>
						<li>l)	use any robot, spider, scraper or other automated means to access QuikWit and collect content for any purpose without our express written permission;</li>
						<li>m)	harvest or otherwise collect information about others without their consent; or</li>
						<li>n)	interfere with or disobey any regulations of servers or networks connected to the Website or probe, scan or test the vulnerability of any system or network, or breach or circumvent any security or authentication measures.</li>
					</ul>
				</li>
				<br>
				<li>5.	<u>PAYMENT TERMS</u>:
					<ul>
						<li>a.	Contract Fees: Freelancers will select an hourly rate for the successful completion of each contract they accept. By choosing to create a contract with a Freelancer, the Client agrees that the amount to be paid after the completion of the contract will be the rate selected by the Freelancer. If another price is negotiated, both parties agree that the amount stated by the contract represents the new negotiated price and accepting the contract means you acknowledge and accept this new price.</li>
						<li>b.	Taxes and Chargebacks: Each Freelancer (not QuikWit) is responsible for determining local indirect taxes, including but not limited to, goods and services tax or harmonized sales tax, and for including any applicable taxes, such as income tax, to be collected or obligations related to applicable taxes in the Contract Fees. In the event of any chargebacks, the Freelancer will be liable for the same, meaning that if a Client initiates a chargeback on PayPal, a credit card or any other payment method, the Freelancer will be charged the full transaction cost.</li>
						<li>c.	QuikWit Fees: Browsing the Website and creating an account are all free. In the first couple of weeks after the launch of the Website, all users can use all features of the Website free of charge. Users will be notified if this changes, through an announcement on the Website and an amendment made to this Terms of Use and the <a href="/pricing">Pricing</a> page.</li>
						<li>d.	QuikWit Fee Changes: QuikWit reserves the right to implement changes to features of the Website. Any revised fees will take effect 7 days from the date of posting on the Website or notification to your account.</li>
					</ul>
				</li>
				<br>
				<li>6.	<u>CONTENT</u>: All information, data, text, software, music, sound, photographs, graphics, video, messages or other materials, whether publicly posted or privately transmitted to the Website by Users, is the sole responsibility of such Users. In other words, the viewer or user, not QuikWit, will bare full responsibility for all such material made available by using the Service. We do not control or actively manage content posted by Users and therefore do not guarantee the veracity or quality of contents uploaded by users. All Users acknowledge that by using the Website, they may be exposed to inappropriate, offensive or questionable materials. By no means will QuikWit be held responsible for any such materials, including errors or omissions in any materials or defects or errors in any printing or manufacturing, or for any loss or damage of any kind incurred as a result of the viewing or use of any materials posted, emailed, transmitted or otherwise made available through the Website or any loss or damage arising from a third party security breach or cyberattack on QuikWit servers or databases.
				</li>
				<br>
				<li>7.	<u>LICENSE OF CONTENT</u>: By submitting, posting or displaying User Content on or through the Website, you grant us (and our agents) a non-exclusive, royalty-free license (with the right to sublicense) to use, copy, modify, transmit, display and distribute such User Content. QuikWit will not be responsible or liable for any use of content posted by Users in accordance with these Terms. You represent and warrant that you have all the rights, power and authority necessary to grant the rights granted herein to any User Content that you submit.
				</li>
				<br>
				<li>8.	<u>END USER LICENSE</u>: All content on the Website (except for contents posted by Users) belongs to QuikWit and its licensors, and such retains all rights, title and interest in and to all Intellectual Property relating to the Website and its Services. QuikWit grants you a limited license to access and use the Website for the purpose of using its Services. You must not access (or attempt to access) the Website or Services by any means either than the interface provided, and you will not use information from the Website for any Services for any purposes either than the purpose for which it was made available. You must not attempt to reverse engineer, modify, adapt, translate, prepare derivative works from, decompile, attempt to interfere with the operation of, or otherwise attempt to derive source code from any part of the Site or its Services unless explicitly permitted by applicable law. Nothing in these Terms of Use gives you a right to use the QuikWit names, trademarks, logos, domain names, and other distinctive brand features without our written permission. You shall not override or circumvent any of the usage riles or restrictions on the Website. Any future release, update, or other addition to functionality of the Website shall be subject to the Terms of Use.
				</li>
				<br>
				<li>9.	<u>REVIEWS AND FEEDBACK</u>: After a contract is completed, Clients can provide feedback on their experience with the respective Freelancer using our star rating and/or comment systems. This feedback will be displayed on the Freelancer’s profile and made available to all Users of the site.
				Clients and Freelancers can also provide feedback to us. Any suggestions, testimonials, comments or other feedback relating to the Website or its Service, may be used in the Website and all other QuikWit products and services (“QuikWit Offerings”). Accordingly, you agree that: 
					<ul>
						<li>(a) QuikWit is not subject to any confidentiality obligations in respect to the provided feedback,</li>
						<li>(b) the feedback is not confidential or proprietary information of you or any third party and you have all of the necessary rights to disclose the feedback to QuikWit,</li>
						<li>(c) QuikWit (including all the successors and assigns or QuikWit and all QuikWit Offerings) may freely use, reproduce, publicise, license, distribute, and otherwise commercialize the feedback in any QuikWit offerings, and</li>
						<li>(d) You are not entitled to receive any compensation or re-imbursement of any kind from QuikWit or any of the other users of the Website with respect to the feedback.</li>
					</ul>
				</li>
				<br>
				<li>10.	<u>DISPUTE RESOLUTION</u>: Users may file complaints if displeased with other Users. We will investigate each dispute and respond to the User as soon as possible. We will notify Users of disputes pertaining to them and will attempt to resolve any dispute. However, QuikWit is not responsible for refunding any User for fees paid to another party or for fees paid for a premium account (if any). If QuikWit considers a complaint valid and such complaint relates to non-attendance, abusive or unFreelancer conduct by you, QuikWit reserves the right to terminate or suspend your access to the Service, hold back your payments, and to exercise any other rights and remedies that QuikWit may have. Although we will make attempts to solve disputes peacefully, we make no guarantees that we will be able to solve any dispute between Users.
				</li>
				<br>
				<li>11.	<u>ADVERTISEMENTS, LINKS AND THIRD PARTY WEBSITES</u>: The Website may contain advertisements. The Website (including User Content) may also contain links to other websites we do not own, manage or in any way endorse. If you access any of these third party websites or services from the Website, it is your responsibility to determine their terms of use and comply with them. If you elect to have any business dealings with anyone whose products or services may be advertised on the Website or any third party websites or services, you acknowledge and agree that such dealings are solely between you and such advertiser or website and that QuikWit is not responsible or liable for any losses or damages you incurred as a result of such dealings with them.
				</li>
				<br>
				<li>12.	<u>DISCLAIMER OF REPRESENTATIONS, WARRANTIES AND CONDITIONS</u>: THE WEBSITE, AND ALL ITS MATERIALS ARE PROVIDED “AS IS” AND ON AN “AS AVAILABLE” BASIS. QUIKWIT SPECIFICALLY DISCLAIMS ALL REPRESENTATIONS, WARRANTIES AND CONDITIONS, EITHER EXPRESS, IMPLIED, STATUTORY, BY USAGE OF TRADE, COURSE OF DEALING OR OTHERWISE INCLUDING BUT NOT LIMITED TO ANY IMPLIED WARRANTIES OF MERCHANTABILITY, MERCHANTABLE QUALITY, OR FITNESS FOR A PARTICULAR PURPOSE. ANY INFORMATION OR MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SERVICE IS AT YOUR OWN DISCRETION AND RISK AND YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM, LOSS OF DATA, OR ANY OTHER LOSS THAT RESULTS FROM DOWNLOADING OR USING ANY SUCH MATERIAL. QUIKWIT DOES NOT WARRANT, ENDORSE, GUARANTEE, PROVIDE ANY CONDITIONS OR REPRESENTATIONS, OR ASSUME ANY RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY ANY USER OR THIRD PARTY THROUGH THE WEBSITE OR IN RESPECT TO ANY WEBSITE THAT CAN BE REACHED FROM A LINK ON THE WEBSITE OR FEATURED IN ANY BANNER OR OTHER ADVERTISING ON THE WEBSITE, AND QUIKWIT SHALL NOT BE A PARTY TO ANY TRANSACTION THAT YOU MAY ENTER INTO WITH ANY SUCH USER OR THIRD PARTY. QUIKWIT WILL NOT BE LIABLE FOR ANY TYPE OF CONTENT EXCHANGED BY MEANS OF THE SERVICE.
				</li>
				<br>
				<li>13.	<u>LIMITATION OF LIABILITY</u>: You agree not to hold QuikWit responsible or liable for any damages or losses whatsoever, including, but not limited to:
					<ul>
						<li>a.	your use of or your inability to use the Website or Services;</li>
						<li>b.	delays or disruptions in our Website or Services;</li>
						<li>c.	viruses or other malicious software obtained by accessing, or linking to, our Website or Services;</li>
						<li>d.	glitches, bugs, errors, or inaccuracies of any kind in our Website or Services;</li>
						<li>e.	personal injury or property damage, including damage to your hardware device from the use of the Website or Services;</li>
						<li>f.	the content, actions, or inactions of third parties’ use of the Website or Services;</li>
						<li>g.	a suspension or other action take with respect to your account;</li>
						<li>h.	your reliance on the quality, accuracy, or reliability of User profiles, ratings and comments, or metrics found on, used on, 		 or made available through the Website or its Services;</li>
						<li>i.	The cost of procurement of substitute goods, data, information or services; or</li>
						<li>j.	your need to modify practices, content, or behaviour or your loss of or inability to do business, as a result of changes to the Terms of Use.</li>
					</ul>
				<br>
				ADDITIONALLY, IN NO EVENT WILL QUIKWIT, OUR AFFILIATES, OUR LICENSORS, OR OUR THIRD-PARTY SERVICE PROVIDERS BE LIABLE FOR ANY SPECIAL, CONSEQUENTIAL, INCIDENTAL, PUNITIVE, EXEMPLARY, OR INDIRECT COSTS OR DAMAGES, INCLUDING, BUT NOT LIMITED TO, LITIGATION COSTS, INSTALLATION AND REMOVAL COSTS, OR LOSS OF DATA, PRODUCTION, PROFIT, OR BUSINESS OPPORTUNITIES. THE LIABILITY OF QUIKWIT, OUR AFFILIATES, OUR LICENSORS, AND OUR THIRD-PARTY SERVICE PROVIDERS TO ANY USER FOR ANY CLAIM ARISING OUT OF OR IN CONNECTION WITH THESE TERMS WILL BE THE GREATER OF: (A) HUNDRED CANADIAN DOLLARS (CAD 100); OR (B) AMOUNTS YOU’VE PAID QUIKWIT IN THE PRIOR 12 MONTHS (IF ANY). THESE LIMITATIONS WILL APPLY TO ANY LIABILITY, ARISING FROM ANY CAUSE OF ACTION WHATSOEVER ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY, OR OTHERWISE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH COSTS OR DAMAGES AND EVEN IF THE LIMITED REMEDIES PROVIDED HEREIN FAIL OF THEIR ESSENTIAL PURPOSE. THE FOREGOING LIMITATIONS SHALL APPLY TO THE FULLEST EXTENSION PERMITTED BY LAW IN THE APPLICABLE JURSIDICTION.
				</li>
				<br>
				<li>14.	<u>RELEASE</u>: In addition to the recognition that QuikWit is not a party to any contract between Client and Freelancer, you hereby release QuikWit, our Affiliates, and our respective officers, directors, agents, subsidiaries, joint ventures, and employees from claims, demands, and damages (actual and consequential) of every kind and nature, known and unknown, arising out of or in any way connected with any dispute you have with another User, whether it be at law or in equity. This release includes, for example and without limitation, any disputes regarding the performance, functions, and quality of the Services provided to a Client by a Freelancer and requests for refunds based upon disputes. Procedures regarding the handling of certain disputes between Users are discussed under section 10 (Dispute Resolution). You further waive any and all rights and benefits otherwise conferred by any statutory law of any jurisdiction that would purport to limit the scope of a release or waiver. Clients and Freelancers are solely responsible for verifying any information provided by Users of the Website and should take necessary precautions to ensure their safety and security when engaging with such Users.
				</li>
				<br>
				<li>15.	<u>INDEMNIFICATION</u>: YOU SHALL INDEMNIFY AND HOLD QUIKWIT AND ITS SUBSIDIARIES, AFFILIATES, OFFICERS, AGENTS AND EMPLOYEES, HARMLESS FROM ALL CLAIMS, ACTIONS, PROCEEDING, DEMANDS, DAMAGES, LOSSES, COSTS, AND EXPENSES (INCLUDING REASONABLE ATTORNEYS’ FEES), INCURRED IN THE CONNECTION WITH ANY MATERIAL SUBMITTED, POSTED, TRANSMITTED OR MADE AVAILABLE BY YOU THROUGH THE SERVICE AND/OR ANY VIOLATION BY YOU OF THESE TERMS OF USE OR OF ANY APPLICABLE LAW OR REGULATION.
				</li>
				<br>
				<li>16.	<u>TERMINATION</u>: QuikWit may immediately terminate your ability to access the Website or aspects of it without notice if we deem it necessary to do so. Reason for such termination will include, but not be to limited to:
					<ul>
						<li>a.	Breaches or violations of these Terms of Use or any other agreement that you may have with QuikWit;</li>
						<li>b.	Requests by law enforcement or other government agencies;</li>
						<li>c.	a request by you;</li>
						<li>d.	discontinuance or modification to the Website; and</li>
						<li>e.	unexpected technical, security or legal issues.</li>
					</ul>
				Termination of your access to the Website may also include removal of some or all of the materials you uploaded to the Website. You acknowledge and agree that all terminations may be made by QuikWit is in its sole discretion and that QuikWit will not be made liable to you or any third-party for any termination of your access to this Website or for the removal of any of the materials uploaded by you to the Website. Any termination of these terms of use by QuikWit will be in addition to any and all other rights and remedies that QuikWit may have.
				</li>
				<br>
				<li>17.	<u>SERVICE AVAILABILITY AND UPDATES</u>: QuikWit may alter, suspend, or discontinue this Website or Service at any time and for any reason or no reason, without notice. The Website may be unavailable from time to time due to maintenance or malfunction of computer network equipment or other reasons. QuikWit may periodically add or update the information and materials on the Website without notice.
				</li>
				<br>
				<li>18.	<u>SECURITY</u>: Information sent or received over the internet is generally insecure and QuikWit cannot and does not make any representation or warranty concerning security of any communication to or from the Website or any representation or warranty regarding the interception by third parties of personal or other information. You are responsible for safeguarding the password that you use to access the Website and you are responsible for any activities or actions under your password. You agree to keep your password secure. QuikWit will not be liable for any loss or damage arising from your failure to comply with these requirements.
				</li>
				<br>
				<li>19.	<u>GENERAL</u>: These Terms of Use, together with our Privacy Policy constitutes the entire agreement between the parties relating to the Website and all related activities. These Terms of Use will not be modified except in writing signed by both parties or by a new posing of these Terms of Use issued by QuikWit. If any of these Terms of Use is held to be unlawful, void, or unenforceable, that part will be deemed severed and will not affect the validity and enforceability of the remaining provisions. The failure of QuikWit to exercise or enforce any right or provision under these Terms of Use shall not constitute a waiver of such right or provision. Any waiver of any right or provision by QuikWit must be in writing and will only apply to the specific instance identified in such writing. You may not assign the Terms of Use, or any right or licenses granted hereunder, whether voluntarily, by operation of law, or otherwise without QuikWit’s written consent. QuikWit may assign these Terms of Use to anyone for any reason. 
				</li>
			</ul>
        </div>
    </div>
</section>
<!-- END Terms -->
{% endblock %}


