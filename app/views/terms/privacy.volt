{% extends "templates/home.volt" %}

{% block title %}
Privacy Policy | QuikWit
{% endblock %}

{% block head %}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h1 class="text-center animation-slideUp"><strong>Privacy Policy</strong></h1>
        
</section>
<!-- END Intro -->

<!-- Terms -->
<section class="site-content site-section">
    <div class="container">
        <div class="site-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-weight: 400;">
        	<h4><strong>Effective Date: September 21, 2015</strong></h4>
        	
        	<hr>
        	QuikWit Inc. (<b>“QuikWit”, “we” or “us”</b>) provides this Privacy Policy to let you know our policies and procedures regarding the collection, use and disclosure of information through www.quikwit.co (the <b>“Website”</b>), and any other websites, applications or services that are owned or controlled by QuikWit, as well as any information QuikWit collects offline in connection with the Service.
        	<br><br>
        	<ul> 
        		<li>1.	DEFINITIONS: While using the Service, we will collect two types of information:
        			<ul>
        				<li>a.	Personal Information: this refers to information used in identifying a user of the Services, such as (but not limited to) name, postal code, phone number, email address, credit card information, birth date, gender and past work experiences. We also collect location data at the time of collection, we will clearly identify the information being collected and the purpose for which it will be used. We may collect Personal Information with regards to the Service through registration processes, communication with you, information downloads, service purchases, customer support and surveys.</li>
        				<li>b.	Non-Personal Information:  this refers to information that does not identify the user. Examples are demographic statistics of our users (such as gender or age range or our users), number of visitors, what pages users access or visit and the average time spent on a page.</li>
        			</ul>
        		</li>
        		<br>
        		<li>2.	CONSENT: By using the Service, you give consent to our usage of your Personal Information as described in this Privacy Policy. Unless otherwise stated by this Privacy Policy, your Personal Information will not be used for any other purpose without your consent. Your Personal Information will not be actively collected for sale or marketing in a way that specifically identifies you (unless we have your consent, like in the case of reviews and feedback) and we do not sell customer lists.</li>
        		<br>
        		<li>3.	INFORMATION COLLECTION: Collecting, using or disclosing information will be for the purpose of managing your account, processing payments, maintaining customer/visitor lists, responding to your inquiries, for identification or activities that will help us continue to provide and/or improve the Service. Any contact information you provide in signing up for our Services or contacting us will be kept confidential and will be used only for the purpose for which it was provided/collected, unless you agree that we may disclose it to other third parties.</li>
        		<br>
        		<li>4.	PURPOSE FOR INFORMATION COLLECTION:
        			<ul>
        				<li>a.	Registration: In order to use some features of the Service, you must provide certain Personal Information to us such as your name and email address. This information will be used in setting up your account. You also have the option to register using your Facebook or LinkedIn account in which case we will have access to certain information that is visible through your Facebook or LinkedIn settings. We may post to your Facebook wall according to your Facebook settings which apply when you approve QuikWit to post on your behalf.</li>
        				<li>b.	Verification: We may use Personal Information submitted by a Client or Freelancer to verify their credentials. This may include background checks or verification of government IDs. We may also contact Freelancers through the phone number or email provided to verify their Personal Information.</li>
        				<li>c.	User Profiles: Profiles created by Clients and Freelancers on the Website will be available to all Users of the Service. This will allow Users to identify other Users, view and verify previous work experiences of such Users and allow Clients to provide or view reviews on a completed Contract with a Client or for any other relevant aspect QuikWit may deem relevant within the scope of the Website.</li>
        				<li>d.	Payment Information: If you use the Service to make or receive payments, we will also collect certain payment information, such as credit card, PayPal or other financial account information, and billing address.</li>
        				<li>e.	Announcements and Notifications: Notifications are provided for certain activities relating to your use of our Service despite your indicated email preferences, for example we may send you notices of any updates to our <a href="/terms" target="_blank" class="register-terms"> Terms of Use</a> or Privacy Policy even if you have opted not receive general communications from us. We will also send emails and/or texts to Users regarding pending, ongoing or past contracts, such as (but not limited to) in the event of rescheduling, cancellation, or other changes made to the contract by one or both parties involved in the contract.</li>
        				<li>f.	Marketing Communications: If you opt-in to receive marketing communications from us, we will keep you up to date on our products and services. You may withdraw consent to receiving communications from us at any time by following the opt-out instructions in each communication.</li>
        				<li>g.	Location Data: The location you provide in Settings or when searching for a Freelancer is used as your default location for all activities on the Website requiring the use of your location. You can change this location at any time in Settings. If you do not provide us with your location, it may be automatically acquired for the above mentioned use, unless you deny us permission to acquire your location automatically.</li>
        				<li>h.	Statistics: We collect statistics regarding the use of our Service. This information will be kept confidential, however, aggregate statistics that do not personally identify an individual will be kept by us and such aggregate statistics may be made available to other members or third parties.</li>
        				<li>i.	Reviews and Feedback: By providing reviews or feedback regarding the Service or its Users (see further “Reviews and Feedback” in the <a href="/terms" target="_blank" class="register-terms"> Terms of Use</a>), you agree that we may use your name in connection with our publication and promotion of that review or feedback.</li>
        				<li>j.	System Logs and Cookies: Cookies are used by us to track content usage and traffic through the Service. A cookies is a feature of your web browser that consists of a text file that is placed on your hard disk by a web browser. The Website uses cookies to help it compile aggregate statistics about usage on this Website, such us how many users visited the Website, how log users spend on pages of the Website, and what pages are most frequently viewed. The information is used to improve the content of the Website. You can set your browser to notify you when you are sent a cookie. This gives you the change to decide whether or not to accept it. If you disable cookies, you may not be able to take advantage of all the features of the Service. Your IP address is reported by your browser whenever you visit a page on the Website. This information is recorded together with your registration information on our databases.</li>
        				<li>k.	Advertisements: Advertisements appearing on the Service may be delivered by us or one or more third-party web advertisers. These third party web advertisers may set cookies. These cookies allow the advertisement server operated by that third party to recognise your computer each time they send you an online advertisement. Accordingly, advertisement servers may compile information about where or whether you viewed their advertisements and which advertisements you clicked on. This information allows web advertisers to deliver targeted advertisements that they believe will be of most interest to you. The Privacy Policy applies to cookies placed on your computer buy us, but does not cover the use of cookies by any third-party web advertisers. For the privacy practices of such third-party web advertisers, you should consult the applicable privacy policy for the relevant third-party web advertiser(s).</li>
        				<li>l.	Partner Websites: QuikWit may also deliver targeted advertisements that may be relevant to you based on the information you have provided to us when you visit our partner websites.</li>
        			</ul>
        			If we decide to use your Personal Information for anything either than the above mentioned reason, we will inform you first by updating this Privacy Policy (elaborated more in section 8, “Amendments to the Privacy Policy”)
        		</li>
        		<br>
        		<li>5.	Disclosures and Transfers: We have put in place safefuards to ensure a proper level of protection of your Personal Information (more details in section 6, "Security"). We will not disclose or transfer your Personal Information to third parties without your permission except as specified in this Privacy Policy.
        		As at the date of this Privacy Policy, we share Personal Information about you in respect of the Service with, but not limited to, Amazon Web Services (AWS), PayPal, Facebook, LinkedIn, Google and any other technology companies that we partner with to provide the Service. Our data and servers are hosted through cloud infrastructure provided by AWS.
        		We may periodically use the services of third parties to help improve our technology and Service. These parties may be given some access to our databases containing user information. However, they will be made to sign contracts which prohibits them from using these information for any other purpose other than that which is specified by the contract.</li>
        		<br>
        		<li>6.	Security: We use commercially reasonable efforts to store and protect your Personal Information. We have designed procedures to limit the dissemination of your Personal Information to only such designated staff as are reasonable necessary to provide the Service.
        		During transmission, we help protect your credit card and other payment option through the use of industry standard Secure Sockets Layer (SSL) encryption technology.
        		Users of the Service are also responsible for protecting their Personal Information. For example, Users should never give out their email account information or their passwords to third parties.</li>
        		<br>
        		<li>7.	Retention of Information: Your Personal Information will be kept for as long as it remains necessary for the identified purpose or as required by law, which may extend beyond the termination of our relationship with you. We may keep some data to prevent fraud or future abuse, or for business purposes (like statistics), account recovery or if required by law. Information kept will be subject to this Privacy Policy. If you request that we remove all your Personal Information, we may not be fully be able to comply with this request dues to restrictions by technology and/or law.</li>
        		<br>
        		<li>8.	Amendments to the Privacy Policy: QuikWit reserves the right to change this Privacy Policy from time to time. In the event that we do, we will send a notification to you to inform you of the update. Changes made will take effect 7 days after users are informed of it. Your continued use of the Service after the effective date of changes to this Privacy Policy signifies your acceptance of all changes.</li>
        		<br>
        		<li>9.	Access and Accuracy: You have the right to request Personal Information we have on you to verify the Personal Information we have collected on you and to have a general account of how we have used your information. Upon receiving your request, we will send you a copy of your Personal Information. However, in some cases, such as when the information you are requesting pertains to another user (even if it is your Personal Information), we may not be able to provide such information and we will explain to you why we must deny your request. We will make every effort to keep your Personal Information accurate and current, and will provide you with tools and features to check, modify and delete your Personal Information. Updated Personal Information will be made available for all Users of the Service to see.</li>
        	</ul>
        </div>
    </div>
</section>
<!-- END Terms -->
{% endblock %}
