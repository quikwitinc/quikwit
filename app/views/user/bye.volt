{% extends "templates/home.volt" %}

{% block title %}
Bye! | QuikWit
{% endblock %}

{% block head %}
{% endblock %}

{% block content %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h1 class="text-center animation-slideUp"><strong>We hate to see you leave!</strong></h1>
        {# <h2 class="h3 text-center animation-slideUp">Please let us know how we can improve...</h2> #}
    </div>
</section>
<!-- END Intro -->

<section class="site-section site-content" style="padding-bottom: 200px;">
	<div class="container">
		<div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-lg-5 col-lg-offset-4 site-block">
                <h4 class="h2 site-heading">Please let us know how we can improve</h4>
                {{ form("contact-us/email", "id" : "", "method" : "post", "class" : "form-horizontal") }}
                    <div class="form-group">
                        {{ text_field('name', "placeholder" : "Name", "class" : "form-control input-lg") }}
                    </div>
                    <div class="form-group">
                        {{ email_field('email', "placeholder" : "Email", "class" : "form-control input-lg") }}
                    </div>
                    <div class="form-group">
                        {{ text_area('message', "placeholder" : "Message", "class" : "form-control input-lg", "rows" : "10") }}
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-primary">Send Message</button>
                    </div>
                {{ end_form() }}
		    </div>
		</div>
    </div>
</section>
{% endblock %}

{% block script %}
{% endblock %}

