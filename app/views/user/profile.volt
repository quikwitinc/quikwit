{% extends "templates/home.volt" %}

{% block title %}
User Profiles | QuikWit
{% endblock %}

{% block head %}
{{ stylesheet_link("third-party/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") }}
{{ stylesheet_link("css/overview.css") }}
<!-- Star Rating -->
{{ stylesheet_link("third-party/star-rating/jquery.rating.css") }}
{% endblock %}

{% block content %}
{# {{ flash.output() }} #}
{% if userInfo is defined %}
<section class="main-content">
    <!-- User Profile Content -->
    <div class="row">
        {% if userInfo.usr_teach %}
        <div class="col-md-5 col-lg-4 col-lg-offset-1">
        {% else %}
        <div class="col-md-8 col-md-offset-2">
        {% endif %}
            <!-- Info Block -->
            <div class="block">
                <!-- Info Title -->
                <div class="block-title">
                    <h2>Info</h2>
                </div>
                <!-- END Info Title -->

                <!-- Info Content -->
                <div class="block-section text-center">
                    {% if userInfo.usr_avatar is defined %}
                    <img title="profile image" src="{{ url("uploads/avatar/" ~ userInfo.usr_avatar) }}" alt="Avatar" class="img-thumbnail" style="width: 175px;">
                    {% else %}
                    <img title="profile image" src="{{ url("img/user/blank-avatar") }}" alt="Avatar" class="img-circle" style="width: 175px;">
                    {% endif %}
                    <h3>
                        <strong>{{ userInfo.usr_firstname|capitalize }}&nbsp;{{ userInfo.usr_lastname|capitalize }}</strong><br><small></small>
                    </h3>
                    {% if userInfo.usr_teach %}
                    <div id="overallRating" style="display: block; width: 160px; padding-left: 35px;" class="container">
                        <input class="star {split:2}" type="radio" name="overall" value="0.5" disabled="disabled"  <?php echo ($stats[0]['overall'] <=0.5  && $stats[0]['overall'] != null)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="overall" value="1.0" disabled="disabled" <?php echo ($stats[0]['overall'] <1.5 && $stats[0]['overall'] >= 1)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="overall" value="1.5" disabled="disabled" <?php echo ($stats[0]['overall'] <2 && $stats[0]['overall'] >=1.5 )?' checked="checked"':'';?> />
                        <input class="star {split:2}" type="radio" name="overall" value="2.0" disabled="disabled" <?php echo ($stats[0]['overall'] < 2.5  && $stats[0]['overall'] >= 2)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="overall" value="2.5" disabled="disabled"  <?php echo ($stats[0]['overall'] < 3  && $stats[0]['overall'] >= 2.5)?' checked="checked"':'';?>/> 

                        <input class="star {split:2}" type="radio" name="overall" value="3.0" disabled="disabled"  <?php echo ($stats[0]['overall'] <3.5  && $stats[0]['overall'] >= 3)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="overall" value="3.5" disabled="disabled" <?php echo ($stats[0]['overall'] <4 && $stats[0]['overall'] >= 3.5)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="overall" value="4.0" disabled="disabled" <?php echo ($stats[0]['overall'] <4.5 && $stats[0]['overall'] >= 4)?' checked="checked"':'';?> />
                        <input class="star {split:2}" type="radio" name="overall" value="4.5" disabled="disabled" <?php echo ($stats[0]['overall'] < 5  && $stats[0]['overall'] >= 4.5)?' checked="checked"':'';?>/>
                        <input class="star {split:2}" type="radio" name="overall" value="5.0" disabled="disabled"  <?php echo ($stats[0]['overall'] >=5)?' checked="checked"':'';?>/>
                        <br>
                    </div>
                    <?php echo $stats[0]['num_contract'];?> Review(s)
                    <br>
                    <br>
                    {% endif %}
                    <button type="button" data-toggle="modal" data-target="{% if loginUser is defined %}#message{% else %}#register{% endif %}" id="msgbutton" class="btn btn-info"><strong>Message</strong></button>
                    {% if userInfo.usr_teach %}
                        <button type="button" data-toggle="modal" data-target="{% if loginUser is defined %}#booking{% else %}#register{% endif %}" data-price="{{ userInfo.usr_rate }}" id="bookbutton" class="btn btn-info searchBook"><strong>Book Me</strong></button>
                    {% endif %}
                    <div class="btn-group btn-group-sm">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default dropdown-toggle enable-tooltip" data-toggle="dropdown" title="Options"><i class="fa fa-cog"></i></a>
                        <ul class="dropdown-menu dropdown-custom dropdown-menu-left">
                            <li class="">
                                {# <a href="javascript:void(0)">Block</a> #}
                                <a href="javascript:void(0)" data-toggle="modal" class="report-user" data-target="{% if loginUser is defined %}#report{% else %}#register{% endif %}">Report User</a>
                            </li>
                        </ul>
                    </div>
                </div> 

                <hr>
                <br>

                <table class="table table-borderless table-striped">
                    <tbody>
                        <tr>
                            <td style="width: 20%;"><strong>Joined</strong></td>
                            <td style="font-size:14px">{{ date('F jS, Y', userInfo.usr_created) }}</td>
                        </tr>
                        {# <tr>
                            <td><strong>Verification</strong></td>
                            <td></td>
                        </tr> #}
                        <tr>
                            <td><strong>Languages</strong></td>
                            <td>{{ userLangs }}</td>
                        </tr>
                        {% if userInfo.usr_teach %}
                        <tr>
                            <td><strong>Service(s) offered</strong></td>
                           
                            <td><a href="javascript:void(0)" class="label label-info">{{ userTags }}</a></td>
                        </tr>
                        <tr>
                            <td><strong>Hourly Rate</strong></td>
                            <td><?php if($userInfo->usr_rate == 0) {
                                    echo 'Please contact me for rate.';
                                }else{
                                    echo '$' .  number_format($userInfo->usr_rate, 2);
                                } ?>
                            </td>
                        </tr> 
                        {% endif %}
                        {# <tr>
                            <td><strong>Social Media</strong></td>
                            <td>{{connections}}</td>
                        </tr> #}
                    </tbody>
                </table>
                <!-- END Info Content -->
            </div>
            <!-- END Info Block -->

            <!-- Bio Block -->
            <div class="block">
                <!-- Bio Title -->
                <div class="block-title">
                    <h2>About {{ userInfo.usr_firstname|capitalize }}</h2>
                </div>
                <!-- END Bio Title -->

                {% if userInfo.usr_about is defined %}
                <div>{{ userInfo.usr_about }}</div>
                {% else %}
                <div>About me...</div>
                {% endif %}
            </div>
            <!-- END Bio Block -->

            <!-- Location Block -->
            <div class="block">
                <!-- Location Title -->
                <div class="block-title">
                    <h2>Location</h2>
                </div>
                <!-- END Location Title -->

                <!-- Location Content -->
                <div class="block-content-full">
                <div data-toggle="gmap"

                     class="gmap" id="gmap"></div>
                </div>
                <!-- END Location Content -->
            </div>
            <!-- END Location Block -->

        </div>
        <!-- END First Column -->

        {% if userInfo.usr_teach %}
        <!-- Second Column -->
        <div class="col-md-7 col-lg-6">

                <!-- Info Block -->
                <div class="block">
                    <!-- Info Title -->
                    <div class="block-title">
                        <h2>Stats</h2>
                    </div>
                    <!-- END Info Title -->

                    <!-- Info Content -->
                    <table class="table table-borderless table-striped">
                        <tbody>
                            <tr>
                                <td style="width: 20%;"><strong>Punctuality</strong></td>
                                <td><input class="star" type="radio" name="punctuality" value="1" title="Worst" disabled="disabled" <?php echo ($stats[0]['puntuality'] <2  && $stats[0]['puntuality'] != null)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="punctuality" value="2" title="Bad" disabled="disabled" <?php echo ($stats[0]['puntuality'] <3 && $stats[0]['puntuality'] >= 2)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="punctuality" value="3" title="OK" disabled="disabled" <?php echo ($stats[0]['puntuality'] <4 && $stats[0]['puntuality'] >= 3)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="punctuality" value="4" title="Good" disabled="disabled" <?php echo ($stats[0]['puntuality'] < 5  && $stats[0]['puntuality'] >= 4)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="punctuality" value="5" title="Best" disabled="disabled" <?php echo ($stats[0]['puntuality'] >=5)?' checked="checked"':'';?>/></td>
                                <td class="hidden-xs"><strong>Number of Contracts<strong></td>
                                <td class="hidden-xs"><?php echo $contractCompleted[0]['num_contract'];?></td>
                            </tr>
                            <tr>
                                <td><strong>Friendliness</strong></td>
                                <td><input class="star" type="radio" name="friendliness" value="1" title="Worst" disabled="disabled" <?php echo ($stats[0]['friendness'] <2  && $stats[0]['friendness'] != null)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="friendliness" value="2" title="Bad" disabled="disabled" <?php echo ($stats[0]['friendness'] <3 && $stats[0]['friendness'] >= 2)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="friendliness" value="3" title="OK" disabled="disabled" <?php echo ($stats[0]['friendness'] <4 && $stats[0]['friendness'] >= 3)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="friendliness" value="4" title="Good" disabled="disabled" <?php echo ($stats[0]['friendness'] < 5  && $stats[0]['friendness'] >= 4)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="friendliness" value="5" title="Best" disabled="disabled" <?php echo ($stats[0]['friendness'] >=5)?' checked="checked"':'';?>/></td>
                            </tr>
                            <tr>
                                <td><strong>Knowledge</strong></td>
                                <td><input class="star" type="radio" name="knowledge" value="1" title="Worst" disabled="disabled" <?php echo ($stats[0]['knowledge'] <2  && $stats[0]['knowledge'] != null)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="knowledge" value="2" title="Bad" disabled="disabled" <?php echo ($stats[0]['knowledge'] <3 && $stats[0]['knowledge'] >= 2)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="knowledge" value="3" title="OK" disabled="disabled" <?php echo ($stats[0]['knowledge'] <4 && $stats[0]['knowledge'] >= 3)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="knowledge" value="4" title="Good" disabled="disabled"<?php echo ($stats[0]['knowledge'] < 5 && $stats[0]['knowledge'] >= 4)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="knowledge" value="5" title="Best" disabled="disabled" <?php echo ($stats[0]['knowledge'] >=5)?' checked="checked"':'';?>/></td>
                                <td class="hidden-xs"><strong>Number of Reviews</strong></td>
                                <td class="hidden-xs"><?php echo $stats[0]['num_contract'];?></td>
                            </tr>
                            <tr>
                                <td><strong>Service Quality</strong></td>
                                <td><input class="star" type="radio" name="teaching" value="1" title="Worst" disabled="disabled" <?php echo ($stats[0]['teaching_skill'] <2  && $stats[0]['teaching_skill'] != null)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="teaching" value="2" title="Bad" disabled="disabled" <?php echo ($stats[0]['teaching_skill'] <3 && $stats[0]['teaching_skill'] >= 2)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="teaching" value="3" title="OK" disabled="disabled" <?php echo ($stats[0]['teaching_skill'] <4 && $stats[0]['teaching_skill'] >= 3)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="teaching" value="4" title="Good" disabled="disabled" <?php echo ($stats[0]['teaching_skill'] < 5  && $stats[0]['teaching_skill'] >= 4)?' checked="checked"':'';?> />
                                    <input class="star" type="radio" name="teaching" value="5" title="Best" disabled="disabled"  <?php echo ($stats[0]['teaching_skill'] >=5)?' checked="checked"':'';?> /></td>
                            </tr>
                            <tr class="hidden-sm hidden-md hidden-lg">
                                <td><strong>Number of Contracts<strong></td>
                                <td><?php echo $contractCompleted[0]['num_contract'];?></td>
                            </tr>
                            <tr class="hidden-sm hidden-md hidden-lg">
                                <td><strong>Number of Reviews</strong></td>
                                <td><?php echo $stats[0]['num_contract'];?></td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- END Info Content -->
                </div>
                <!-- END Info Block -->

            <!-- Intro Block -->
            <div class="block">
                <!-- Intro Title -->
                <div class="block-title">
                    <h2>Intro Video</h2>
                </div>
                <!-- END Intro Title -->

                <!-- New Media Content -->
                <div class="gallery" data-toggle="lightbox-gallery">
                    <div class="row">
                       {# <div class="col-xs-6 col-sm-3">
                            <a href="" data-toggle="modal" data-target="#deleteMedia" style="text-decoration:none;">X</a>
                            <a href="http://lorempixel.com/720/480/" class="gallery-link" title="Image Info">
                                <img src="http://lorempixel.com/720/480/" alt="image">
                            </a>
                        </div> #}

                        <div class="col-xs-6 col-sm-12 text-center">
                            <?php if($videos){ ?>
                                <video width="300" height="" controls>
                                  <source src="<?php if($videos){ echo $videos[0]['vid_url']; } ?>" type="video/mp4">
                                  Your browser does not support the video tag.
                                </video>
                            <?php
                            }
                            else {
                            ?>
                                No video uploaded yet.
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- END Media Content -->
            </div>
            <!-- END Intro Block -->

            <!-- Media Block -->
            <div class="block">
                <!-- Media Title -->
                <div class="block-title">
                    <h2><strong>Media</strong></h2>
                </div>
                <!-- END Media Title -->

                <!-- New Media Content -->
                <div class="gallery" data-toggle="lightbox-gallery">
                    <div class="row">
                        <?php 
                        if($medias){
                            foreach($medias as $media) {
                                if($media['mid_type']==1) {
                        ?>
                                <div class="col-xs-6 col-sm-3">
                                    <a href="javascript:void(0);" style="text-decoration:none;" onclick="mediaDeleteModal('<?php echo $media['mid_id']; ?>')">X</a>
                                    <a href="<?php echo $media['mid_url']; ?>" class="gallery-link mfp-image" title="<?php echo $media['mid_name']; ?>" data-type="image">
                                        <img src="<?php echo $media['mid_url']; ?>" alt="<?php echo $media['mid_name']; ?>">
                                    </a>
                                </div>
                        <?php
                                }
                                else {
                        ?>
                                <div class="col-xs-6 col-sm-3">
                                    <a href="javascript:void(0);" style="text-decoration:none;" onclick="mediaDeleteModal('<?php echo $media['mid_id']; ?>')">X</a>
                                    <a href="https://www.youtube.com/watch?v=<?php echo $media['mid_url']; ?>" class="gallery-link mfp-iframe" title="<?php echo $media['mid_name']; ?>" data-type="video">
                                        <img src="http://img.youtube.com/vi/<?php echo $media['mid_url']; ?>/0.jpg" alt="<?php echo $media['mid_name']; ?>">
                                    </a>
                                </div>      
                        <?php
                                }
                            }
                        }else{
                        ?>
                            <div class="col-xs-6 col-sm-6">
                                This user did not upload any media yet! 
                            </div> 
                        <?php
                            }
                        ?>
                    </div>
                </div>
                <!-- END Media Content -->
            </div>
            <!-- END Media Block -->

            <!-- Reviews Block -->
            <div class="block">
                <!-- Reviews Title -->
                <div class="block-title">
                    <h2>Reviews</h2>
                </div>
                <!-- END Reviews Title -->

                <!-- Reviews Content -->
                <ul class="media-list">

                </ul>
                <div class="more-review">[ More Reviews ]</div>
                <div class="clear"></div>
                <!-- END Reviews Content -->
            </div>
            <!-- END Reviews Block -->

            <!-- Timeline Style Block -->
            <div class="block full">
                <!-- Timeline Style Title -->
                <div class="block-title">
                    <h2>Experience</h2>
                </div>
                <!-- END Timeline Style Title -->

                <!-- Timeline Style Content -->
                <!-- You can remove the class .block-content-full if you want the block to have its regular padding -->
                <div class="timeline">
                    {# <h3 class="timeline-header">Updated <small> on <strong> June 14, 2014</strong></small></h3> #}
                    <!-- You can remove the class .timeline-hover if you don't want each event to be highlighted on mouse hover -->
                    <ul class="timeline-list timeline-hover">

                    <?php
                    if (count($userExperiences) > 0){
                        foreach ($userExperiences as $key => $value) {
                        ?><li <?php echo ($key == 0)?'class="active"':'';?>>
                            <div class="btn-group btn-group-sm pull-right">
                            </div>
                            <div class="timeline-icon"><i class="fa fa-file-text"></i></div>
                            <div class="timeline-time"><?php
                             if ($value['end_year'] < 9999){
                                echo  date('F', mktime(0, 0, 0, $value['end_month'], 1,$value['end_year'])) . " " . date('Y', mktime(0, 0, 1, 1,1, $value['end_year'])) . " - <br>" . date('F', mktime(0, 0, 0, $value['start_month'], 1,$value['start_year'])) . " " . date('Y', mktime(0, 0, 1, 1,1, $value['start_year']));
                             }else{
                                echo '<strong>Present</strong> - <br>' . date('F', mktime(0, 0, 0, $value['start_month'], 1,$value['start_year'])) . " " . date('Y', mktime(0, 0, 1, 1,1, $value['start_year']));
                             }
                             ?> 
                            </div>
                            <div class="timeline-content">
                                <p class="push-bit"><strong><?php echo $value['title'];?></strong></p>
                                <?php echo $value['content'];?>
                            </div>
                        </li>
                        <?php
                        }
                    }else{
                    ?>
                    <li>
                        This user did not put in any experience.                   
                    </li>
                    <?php
                    }
                    ?>

                    </ul>
                </div>
                <!-- END Timeline Style Content -->

            </div>
            <!-- END Timeline Style Block -->

            {# <!-- Likes Block -->
            <div class="block">
                <!-- Likes Title -->
                <div class="block-title">
                    <h2>Recently Liked Media <small>&bull; <a href="javascript:void(0)">450</a></small></h2>
                </div>
                <!-- END Likes Title -->

                <!-- Likes Content -->
                <div class="row text-center">
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar15.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar7.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar1.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar14.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar3.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar3.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar14.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar2.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar6.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar16.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar3.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="img/placeholders/avatars/avatar13.jpg" alt="image" class="img-circle" data-toggle="tooltip" title="Username">
                        </a>
                    </div>
                </div>
                <!-- END Likess Content -->
            </div>
            <!-- END Likes Block --> #}
        </div>
        <!-- END Second Column -->
        {% endif %}
    </div>
    <!-- END User Profile Content -->
</section>
{% else %}
<!-- Intro -->
<section class="site-section site-section-light site-section-top themed-background">
    <div class="container">
        <h1 class="h3 text-center animation-slideUp" style="font-size: 200%"><strong>Sorry, user is not available.</strong></h1>
        {#<h2 class="h3 text-center animation-slideUp">Fill with your email to receive instructions on resetting your password!</h2>#}
    </div>
</section>
<!-- END Intro -->

<section class="site-content site-section" style="padding-bottom: 200px;">
    <div class="container">
        <div class="row" style="margin-top: 40px">
            <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 site-block">
                <h4>This user account is either not found in our database or has been deactivated at the request of the account owner.</h4>
                <br>
                <h4>If you think this is a mistake, please contact <a href="/contact">us</a> here.</h4>
            </div>
        </div>
        <!-- END panel-->
    </div>
</section>
{% endif %}

{% endblock %}

{% block modal %}
<!-- START modal-->
   <div id="booking" tabindex="-1" role="dialog" aria-labelledby="bookingLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="bookingLabel" class="modal-title">Create Contract</h4>
            </div>
            <div class="modal-body">
            {% if email_verified == 1 %}
              <!-- START panel-->
                {{ form("contract/create", "id" : "contract-form", "method" : "post", "class" : "form-horizontal") }}
                    <div class="form-group container">
                        <p style="color:#1b92ba">Please message the freelancer first regarding availability before booking.</p>
                    </div>
                   <div class="form-group">
                   {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }}
                    {{ hidden_field("contract[uid]", "value" : userId) }}
                      <label for="datetime-title" class="col-lg-3 control-label">Start Date & Time</label>
                      <div class="col-lg-8">
                        <div class='input-group date datetimepicker mb-lg'>
                          <input type='text' name="contract[datetime]" id="datetimepickers" class="form-control" readOnly />
                          <span class="input-group-addon">
                              <span class="fa fa-calendar"></span>
                          </span>
                        </div>
                      </div>
                   </div>
                   <div class="form-group">
                      <label for="price-title" class="col-lg-3 control-label">Price of contract ($ CDN)</label>
                      <div class="col-lg-8">
                        <div class="input-group m-b">
                           <span class="input-group-addon">$</span>
                           {{ text_field('price', "id" : "contract-price", "class" : "form-control") }}
                        </div>
                      </div>
                   </div>
                   <div class = "form-group">
                      <label for = "sub-skill" class = "col-lg-3 control-label">Skill:</label>
                       <div class = "col-lg-8">
                          <?php   $usertagsmodel = new UserTags();
                                  $userTagsObj = $usertagsmodel->getUserTags($userId);
                                  $userTags = (array_column($userTagsObj, 'name', 'tag_id'));
                                  echo Phalcon\Tag::selectStatic(array('contract[tag]', 
                                          $userTags, 
                                          "using" => array("tag_id", "name"),
                                          "class" => "select-chosen form-control",
                                          "type" => "text",
                                          "id" => "usersTags",
                                          "data-role" => "tagsinput"
                                          )); ?>
                       </div>
                    </div>
                    <div class = "form-group">
                      <label for = "req-id" class = "col-lg-3 control-label">Post:</label>
                       <div class = "col-lg-8">
                          <?php 
                          $requestModel = new Requests();
                          $requestsObj = $requestModel->getUserRequests($loginUserId);
                          $requests = (array_column($requestsObj, 'req_title', 'req_id'));
                          //error_log("<pre>request".print_r($requests,true)."</pre>"); 
                          echo Phalcon\Tag::selectStatic(array('contract[reqId]', 
                                                            $requests, 
                                                            "using" => array("req_id", "req_title"),
                                                            "class" => "select-chosen form-control",
                                                            "type" => "text",
                                                            "id" => "myRequests",
                                                            "data-role" => "tagsinput",
                                                            "useEmpty" => true,
                                                            "emptyText" => "Ignore if this is not related to any post"
                                                            )); ?>
                       </div>
              <!-- END panel-->
            </div>
            <div class="modal-footer">
               <input type="submit" value=" Book " class="btn btn-info">
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
            {{ end_form() }}
            {% else %}
            For security reasons, we require users to confirm their email before booking a contract. Do you want to confirm your email now?
            </div>
            <div class="modal-footer">
                <button id="resend" data-dismiss="modal" class="btn btn-info">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
            {% endif %}
         </div>
      </div>
   </div>
<!-- END modal-->
<!-- START modal-->
<div id="message" tabindex="-1" role="dialog" aria-labelledby="messageLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
               <h4 id="messageLabel" class="modal-title">Send Message</h4>
            </div>
            <div class="modal-body">
                {% if email_verified == 1 %}
               <!-- <form class="form-horizontal"> -->
               {{ form("message/send", "id" : "message-form", "method" : "post", "class" : "form-horizontal") }}
                  <div class="form-group">
                  	{{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }}
					{# hidden_field("message[redirect]", "value" : userLink) #}
					{{ hidden_field("message[uid]", "value" : userId) }}
                    <label for = "message-title" class = "col-lg-2 control-label">Title:</label>
                     <div class = "col-lg-10">
                       <!-- <input type = "text" class = "form-control" id = "message-title" placeholder = "[Type Title Here]"> -->
                       {{ text_field('message[title]', "id" : "message-title", "placeholder" : "Give your message a title", "class" : "form-control") }}
                     </div>
                  </div>
                  <div class = "form-group">
                     <label for = "review-body" class = "col-lg-2 control-label">Message:</label>
                     <div class = "col-lg-10">
                     <!-- <textarea class="form-control" rows="8" placeholder="[Type Message Here]"></textarea> -->
                     {{ text_area('message[message]', "id" : "message-title", "placeholder" : "Type your message here", "class" : "form-control", "rows": 8) }}
                     </div>
                  </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-info">Send</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
            {{ end_form() }}
            {% else %}
            For security reasons, we require users to confirm their email before messaging other users. Do you want to confirm your email now?
            </div>
            <div class="modal-footer">
                <button id="resend" class="btn btn-info">Yes</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            </div>
            {% endif %}
        </div>
    </div>
</div>
<!-- END modal-->
<!-- START modal-->
<div id="report" tabindex="-1" role="dialog"
    aria-labelledby="reportLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="reportLabel" class="modal-title">Report User</h4>
            </div>
            <div class="modal-body">
                {{ form("abuse/create", "class" : "form-horizontal", "method" :
                "post") }}
                <div class="form-group">
                    <label for="report-title" class="col-lg-2 control-label">Description:</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="report-title" name="abuse[reason]"
                            placeholder="Please tell us the nature of the issue">
                    </div>
                </div>
                <div class="form-group">
                    <label for="report-body" class="col-lg-2 control-label">More Information:</label>
                    <div class="col-lg-10">
                        <textarea class="form-control" id="report-body" rows="8" name="abuse[extra]"
                            placeholder="Please provide us with more information so we can help"></textarea>
                    </div>
                </div>
                {{ hidden_field("abuse[uid]", "value" : userId) }}
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-info">Submit</button>
                {{ end_form() }}
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
<!-- START modal-->
<div id="register" tabindex="-1" role="dialog"
    aria-labelledby="reportLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                    class="close">x</button>
                <h4 id="reportLabel" class="modal-title">New to our site? Please register below to continue.</h4>
            </div>
            <div class="modal-body">
              <!-- Sign Up Form -->
              {{ form("register/doRegister", "id" : "form-validation", "method" : "post", "class" : "form-horizontal") }}
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-user"></i></span>
                              {{ text_field('firstname', "placeholder" : "First Name", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-user"></i></span>
                              {{ text_field('lastname', "id" : "rform-lastname", "placeholder" : "Last Name", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                              {{ email_field('email', "id" : "rform-email", "placeholder" : "Email", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-lock"></i></span>
                              {{ password_field('password', "id" : "rform-password", "placeholder" : "Password", "class" : "form-control input-lg") }}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="gi gi-lock"></i></span>
                              {{ password_field('rpassword', "id" : "rform-repassword", "placeholder" : "Retype Password", "class" : "form-control input-lg") }}
                              
                          </div>
                      </div>
                  </div>
                  </br>
                  <div class="form-group form-actions">
                      <div class="checkbox c-checkbox">
                           <label class="text-center">  
                              {{ check_field("confirmation", "id" : "rform-confirmation") }}
                              <span class="fa fa-check"></span>
                              <strong class="text-right"> Yes, I understand and agree to the QuikWit <a href="/terms" target="_blank" class="register-terms"> Terms of Use</a> and <a href="/terms/privacy" target="_blank" class="register-terms">Privacy Policy</a></strong>
                          </label>         
                      </div>
                      <br>
                      <div class="col-xs-12 text-center">
                          <button type="submit" class="btn btn-lg btn-primary" id="signUpButton"><strong>Sign Up</strong></button>
                      </div>
                  </div>
                  {# <input id="rform-csrf" type="hidden" name="<?php echo $this->security->getTokenKey() ?>" value="<?php echo $this->security->getTokenKey() ?>"/> 
                  <input id="rform-csrf" type="hidden" name="{{ tokenKey }}" value="{{ token }}"/> #}
                  {{ hidden_field(security.getTokenKey(), "value" : security.getToken()) }} 
              {{ end_form() }}
              <!-- END Sign Up Form -->
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal-->
{% endblock %}

{% block footer %}
{% if userInfo is empty %}
<!-- Footer -->
<footer class="site-footer site-section" style="padding-bottom: 0px; bottom: 0px; left: 0px; width: 100%;">
    <div class="container">
        <!-- Footer Links -->
        <div class="row text-center">
            <div class="col-sm-3 col-xs-12">
                <h4 class="footer-heading">QuikWit</h4>
                <ul class="footer-nav list-unstyled">
                    <li><a href="/about">Company</a></li>
                    <li><a href="/contact-us">Contact</a></li>
                    {# <li><a href="/support">Support</a></li> #}
                    {# <li><a href="/blog">Blog</a></li> #}
                    <li><a href="/faq">FAQ</a></li>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-12">
                <h4 class="footer-heading">For Freelancers</h4>
                <ul class="footer-nav list-unstyled">
                    <li><a href="/how-to-hire">How to Hire</a></li>
                    <li><a href="/pricing">Pricing</a></li>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-12">
                <h4 class="footer-heading">Legal</h4>
                <ul class="footer-nav list-unstyled">
                    <li><a href="/terms" target="_blank">Terms of Use</a></li>
                    <li><a href="/terms/privacy" target="_blank">Privacy Policy</a></li>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-12">
                <h4 class="footer-heading">Follow Us</h4>
                <ul class="footer-nav footer-nav-social list-inline">
                    <li><a href="https://www.facebook.com/quikwit" target="_blank"><i class="fa fa-2x fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/quikwithq" target="_blank"><i class="fa fa-2x fa-twitter"></i></a></li>
                    {# <li><a href="https://pinterest.com/quikwithq)"><i class="fa fa-pinterest"></i></a></li>
                    <li><a href="https://plus.google.com/117235860072830632821"><i class="fa fa-google-plus"></i></a></li> #}
                </ul>
            </div>
        </div>
        <br>
        <!-- END Footer Links -->
        <div class="col-xs-12 text-center">                    
            <ul class="footer-nav list-inline">
                <li style="font-size: 18px;">&copy; 2015 Quikwit Inc.</li>
                <li>All rights reserved.</li>
            </ul>
        </div>
    </div>
</footer>
<!-- END Footer -->
{% else %}
{% endif %}
{% endblock %}

{% block script %}
<!-- Load and execute javascript code used only in this page -->
{{ javascript_include("third-party/proui/backend/js/pages/readyTimeline.js") }}

<!-- Star Rating -->
{{ javascript_include("third-party/star-rating/jquery.rating.js") }}
{{ javascript_include("third-party/star-rating/jquery.MetaData.js") }}

{{ javascript_include('third-party/async/async.js') }}

<script>
var page = 0;
var userId = {{userId}};
$(document).ready(function(){
    /* var latitude = {{userInfo.lat}};
    var longitude = {{userInfo.lng}};
    if (latitude && longitude){
        var map =$(".gmap").gMap({
            latitude: latitude,
            longitude: longitude,
            zoom: 16,
            markers: [
                {
                    latitude:  {{userInfo.lat}},
                    longitude: {{userInfo.lng}},
                },
                {
                    address: "",
                    html: "Location",
                    icon: {
                        image: "images/gmap_pin_grey.png",
                        iconsize: [26, 46],
                        iconanchor: [12,46]
                    }
                }
            ],
        });      
    } */
    $("#resend").click(function() {

        var email = "<?php echo $email; ?>";

        $.ajax({
            url: '/user/resend',
            type: 'POST',
            data:{
                email: email
            },
            success: function(res) {
                location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                location.reload();
            }
        });
    });
    processReview();
    $('.more-review').click(function(e){
        processReview();
    });
    $(document).on('click', '.searchBook', function(ev){
      var id = $(this).attr('data-id');
      var price = $(this).attr('data-price');
      $('#contract-price').val(Number(price).toFixed(2));
    });
    $(document).on('click', '.report-user', function(ev){
      var id = $(this).attr('data-id');
      $('#abuse_user_id').val(id);
    });
});

function processReview(){
  page++;
  async.waterfall([
    function(cb){
        $.ajax({
            url: '/user/getReview',
            data:{
                page: page,
                userId: userId
            },
            success: function(res) {
                var result = JSON.parse(res);
                if (!result.status){
                    return cb(null, result.total,  result.numPage, result.list);
                }else{
                    return cb(null, 0, 0, []);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                cb(null, 0, 0, []);
             }
    });
    },
    function(total,  numPage, list, cb){
        if (page <= total){
        var self = this;
         $.ajax({
            url: '/js/user/review.js',
            cache: false,
            dataType: 'text',
            success: function(data) {
             var source   = data;
             template  = Handlebars.compile(source);
             list.map(function(oneReview){
                    $('.media-list').append(template({
                       userId: oneReview.userId,
                       avatar: oneReview.avatar,
                       createDate: oneReview.createDate,
                       firstname: oneReview.firstname,
                       lastname: oneReview.lastname,
                       title: oneReview.title,
                       comment: oneReview.comment,
                    }));
             return oneReview;
            });
             if (page == numPage){
                $('.more-review').remove();
             }
             return cb(null, true);
            }, error: function(xhr, ajaxOptions, thrownError) {
            }
        });
        } else if (page == 1 && total == 0){
         $.ajax({
            url: '/js/user/noreview.js',
            cache: false,
            dataType: 'text',
            success: function(data) {
             var source   = data;
             template  = Handlebars.compile(source);
            $('ul.media-list').append(template({}));
            $('.more-review').remove();
            return cb(null, true);
            }, error: function(xhr, ajaxOptions, thrownError) {
            }
        });
        }else{
            $('.more-review').remove();
        }
    }
  ], function(e, result){

  });
}

function initMap() {

    var UserLatLng = {lat: {{userInfo.lat}}, lng: {{userInfo.lng}} };

    var map = new google.maps.Map(document.getElementById('gmap'), {
        zoom: 12,
        center: UserLatLng
    });

    // Add circle overlay and bind to marker
    var circle = new google.maps.Circle({
        strokeColor: '#1b92ba',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#1b92ba',
        fillOpacity: 0.35,
        map: map,
        center: UserLatLng,
        radius: 2000    // 3000 meters
    });

}
google.maps.event.addDomListener(window, 'load', initMap); 

</script>
{% endblock %}