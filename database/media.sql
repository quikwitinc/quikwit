/*
MySQL Data Transfer
Source Host: localhost
Source Database: tute-user
Target Host: localhost
Target Database: tute-user
Date: 8/12/2015 11:37:27 PM
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for media
-- ----------------------------
DROP TABLE IF EXISTS `media`;
CREATE TABLE `media` (
  `mid_id` int(11) NOT NULL AUTO_INCREMENT,
  `mid_user` int(11) DEFAULT NULL,
  `mid_type` tinyint(1) DEFAULT '1' COMMENT '1 = image; 2 = video',
  `mid_name` varchar(255) DEFAULT NULL,
  `mid_url` varchar(255) DEFAULT NULL,
  `mid_content` text,
  `mid_created` int(11) DEFAULT NULL,
  PRIMARY KEY (`mid_id`),
  KEY `vid_user` (`mid_user`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for media_tags
-- ----------------------------
DROP TABLE IF EXISTS `media_tags`;
CREATE TABLE `media_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
