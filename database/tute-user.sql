-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2015 at 11:24 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tute-user`
--

-- --------------------------------------------------------

--
-- Table structure for table `abuses`
--

CREATE TABLE IF NOT EXISTS `abuses` (
`abs_id` int(11) NOT NULL,
  `abs_user` int(11) DEFAULT NULL,
  `abs_target` int(11) DEFAULT NULL,
  `abs_created` datetime DEFAULT NULL,
  `abs_reason` tinytext,
  `abs_text` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bookmarks`
--

CREATE TABLE IF NOT EXISTS `bookmarks` (
`bmk_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `usr_id` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `bmk_deleted` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Art & Crafts'),
(2, 'Sport & Health'),
(3, 'Music'),
(4, 'IT & Software'),
(5, 'Academic'),
(6, 'Culinary'),
(7, 'Language'),
(8, 'Beauty & Fashion'),
(9, 'Pet Care & Training'),
(10, 'Games & Hobbies');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `user_id` int(11) NOT NULL COMMENT 'Contact user id',
  `contact_id` int(11) NOT NULL COMMENT 'User''s contact''s id',
  `created` int(11) unsigned DEFAULT NULL COMMENT 'Timestamp of when this contact is created'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`user_id`, `contact_id`, `created`) VALUES
(4, 6, 1431885752),
(4, 8, 1432173144),
(6, 4, 1431832364),
(7, 12, 1433105230),
(12, 7, 1422044126),
(12, 21, 1433100071),
(12, 23, 1435199864),
(13, 4, 1421985872),
(18, 18, 1423109126),
(23, 12, 1435543032);

-- --------------------------------------------------------

--
-- Table structure for table `contracthistory`
--

CREATE TABLE IF NOT EXISTS `contracthistory` (
`history_id` int(11) NOT NULL,
  `cnt_id` int(11) NOT NULL,
  `cnt_next` datetime NOT NULL,
  `prompter` int(11) NOT NULL,
  `reciever` int(11) NOT NULL,
  `sub_state` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contracthistory`
--

INSERT INTO `contracthistory` (`history_id`, `cnt_id`, `cnt_next`, `prompter`, `reciever`, `sub_state`) VALUES
(111, 22, '2015-06-25 22:08:00', 12, 23, 0),
(112, 22, '2015-06-25 22:08:00', 23, 12, 4),
(113, 22, '2015-06-25 22:08:00', 12, 23, 3),
(114, 22, '2015-06-25 22:08:00', 12, 23, 5),
(115, 22, '2015-06-25 22:08:00', 23, 12, 6),
(116, 23, '0000-00-00 00:00:00', 12, 23, 0),
(117, 23, '0000-00-00 00:00:00', 12, 23, 12),
(118, 24, '0000-00-00 00:00:00', 12, 23, 0),
(119, 24, '1971-01-06 00:00:00', 12, 23, 1),
(120, 24, '1971-01-06 00:00:00', 12, 23, 12),
(121, 25, '0000-00-00 00:00:00', 12, 23, 0),
(122, 26, '0000-00-00 00:00:00', 12, 23, 0),
(123, 27, '0000-00-00 00:00:00', 12, 23, 0),
(124, 28, '0000-00-00 00:00:00', 12, 23, 0),
(125, 29, '2015-06-17 22:42:00', 12, 23, 0),
(126, 30, '0000-00-00 00:00:00', 12, 23, 0),
(127, 23, '1974-07-18 00:00:00', 12, 23, 1),
(128, 25, '0000-00-00 00:00:00', 23, 12, 13),
(129, 26, '0000-00-00 00:00:00', 23, 12, 4),
(130, 26, '0000-00-00 00:00:00', 12, 23, 3),
(131, 26, '1970-01-23 00:00:00', 12, 23, 1),
(132, 26, '1970-01-23 00:00:00', 12, 23, 5),
(133, 26, '1970-01-23 00:00:00', 12, 23, 15),
(134, 27, '0000-00-00 00:00:00', 12, 23, 3),
(135, 27, '0000-00-00 00:00:00', 23, 12, 4),
(136, 27, '0000-00-00 00:00:00', 23, 12, 15),
(137, 28, '0000-00-00 00:00:00', 23, 12, 4),
(138, 29, '2015-06-17 22:42:00', 23, 12, 4),
(139, 30, '0000-00-00 00:00:00', 23, 12, 4),
(140, 28, '0000-00-00 00:00:00', 12, 23, 3),
(141, 30, '0000-00-00 00:00:00', 12, 23, 3),
(142, 29, '2015-06-17 22:42:00', 12, 23, 3),
(144, 28, '0000-00-00 00:00:00', 23, 12, 9),
(145, 28, '0000-00-00 00:00:00', 23, 12, 14),
(147, 29, '2015-06-17 22:42:00', 12, 23, 10),
(148, 29, '2015-06-17 22:42:00', 23, 12, 14),
(149, 23, '1974-07-17 23:00:00', 12, 23, 1),
(150, 23, '1974-07-17 22:00:00', 12, 23, 1),
(151, 23, '1974-07-17 21:00:00', 12, 23, 1),
(152, 23, '1974-07-17 20:00:00', 12, 23, 1),
(153, 23, '1974-07-17 19:00:00', 12, 23, 1),
(154, 23, '1974-07-17 18:00:00', 12, 23, 1),
(155, 23, '1974-07-17 17:00:00', 12, 23, 1),
(156, 23, '1974-07-17 16:59:00', 12, 23, 1),
(157, 23, '1974-07-17 16:58:00', 12, 23, 1),
(158, 23, '1974-07-17 16:57:00', 12, 23, 1),
(159, 23, '1974-07-17 17:00:00', 12, 23, 1),
(160, 23, '1974-07-17 17:01:00', 12, 23, 1),
(161, 23, '1974-07-17 16:59:00', 12, 23, 1),
(162, 23, '1974-07-17 16:58:00', 12, 23, 1),
(163, 23, '1974-07-17 17:50:00', 12, 23, 1),
(164, 31, '2015-06-28 13:13:00', 12, 23, 0),
(165, 32, '2015-06-28 15:43:00', 12, 23, 0),
(166, 30, '0000-00-00 00:00:00', 12, 23, 5),
(167, 30, '0000-00-00 00:00:00', 23, 12, 6),
(168, 33, '2015-06-28 21:57:00', 23, 12, 0),
(169, 33, '2015-06-28 21:57:00', 23, 12, 3),
(170, 33, '2015-06-28 21:57:00', 12, 23, 4),
(171, 33, '2015-06-28 21:57:00', 12, 23, 6),
(172, 33, '2015-06-28 21:57:00', 23, 12, 5),
(173, 34, '2015-06-28 22:06:00', 23, 12, 0),
(174, 34, '2015-06-28 22:06:00', 23, 12, 3),
(175, 34, '2015-06-28 22:06:00', 12, 23, 4),
(176, 34, '2015-06-28 22:06:00', 12, 23, 6),
(177, 34, '2015-06-28 22:06:00', 23, 12, 5),
(178, 35, '2015-06-28 23:30:00', 12, 23, 0),
(179, 35, '2015-06-28 23:30:00', 12, 23, 3),
(180, 35, '2015-06-28 23:30:00', 23, 12, 4),
(181, 35, '2015-06-28 23:30:00', 23, 12, 6),
(182, 35, '2015-06-28 23:30:00', 12, 23, 5),
(183, 31, '2015-06-28 13:13:00', 23, 12, 4),
(184, 31, '2015-06-28 13:13:00', 12, 23, 3),
(185, 31, '2015-06-28 13:13:00', 12, 23, 5),
(186, 31, '2015-06-28 13:13:00', 23, 12, 6),
(187, 36, '0000-00-00 00:00:00', 12, 23, 0),
(188, 37, '0000-00-00 00:00:00', 12, 23, 0),
(189, 38, '0000-00-00 00:00:00', 12, 23, 0),
(190, 39, '0000-00-00 00:00:00', 12, 23, 0),
(191, 40, '2015-07-08 11:50:00', 12, 23, 0),
(192, 40, '2015-07-24 11:50:00', 12, 23, 1),
(193, 40, '2015-07-13 11:50:00', 12, 23, 1),
(194, 40, '2015-07-16 11:50:00', 12, 23, 1),
(195, 40, '2015-07-16 11:50:00', 12, 23, 1),
(196, 40, '2015-07-24 11:50:00', 12, 23, 1),
(197, 40, '2015-07-25 11:50:00', 12, 23, 1),
(198, 40, '2015-07-28 11:50:00', 12, 23, 1),
(199, 40, '2015-07-10 11:50:00', 12, 23, 1),
(200, 39, '1970-01-07 00:00:00', 12, 23, 1),
(201, 38, '1970-01-13 00:00:00', 12, 23, 1),
(202, 40, '2015-07-24 11:50:00', 12, 23, 1),
(203, 34, '2015-06-11 22:06:00', 12, 23, 2),
(204, 41, '2015-07-17 15:37:00', 12, 23, 0),
(205, 42, '2015-07-25 16:09:00', 12, 23, 0);

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE IF NOT EXISTS `contracts` (
`cnt_id` int(11) NOT NULL,
  `cnt_user` int(11) DEFAULT NULL,
  `cnt_state` tinyint(4) DEFAULT '0',
  `cnt_unitprice` decimal(10,0) DEFAULT NULL,
  `cnt_tagid` int(11) unsigned DEFAULT NULL,
  `cnt_teachid` int(11) DEFAULT NULL,
  `cnt_created` datetime NOT NULL,
  `cnt_next` datetime DEFAULT NULL,
  `cnt_started` datetime DEFAULT NULL,
  `cnt_completed` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contracts`
--

INSERT INTO `contracts` (`cnt_id`, `cnt_user`, `cnt_state`, `cnt_unitprice`, `cnt_tagid`, `cnt_teachid`, `cnt_created`, `cnt_next`, `cnt_started`, `cnt_completed`) VALUES
(22, 12, 2, '100', 23, 23, '2015-06-26 02:09:00', '2015-06-25 22:08:00', '2015-06-26 02:10:00', '2015-06-26 02:11:00'),
(23, 12, 3, '10', 6, 23, '2015-06-26 02:14:00', '1974-07-17 17:50:00', NULL, NULL),
(24, 12, 3, '20', 9, 23, '2015-06-26 02:23:00', '1971-01-06 00:00:00', NULL, NULL),
(25, 12, 3, '30', 30, 23, '2015-06-26 02:25:00', '0000-00-00 00:00:00', NULL, NULL),
(26, 12, 5, '50', 50, 23, '2015-06-26 02:35:00', '1970-01-23 00:00:00', '2015-06-26 03:08:00', NULL),
(27, 12, 5, '10', 20, 23, '2015-06-26 02:37:00', '0000-00-00 00:00:00', '2015-06-26 03:11:00', NULL),
(28, 12, 4, '100', 20, 23, '2015-06-26 02:38:00', '0000-00-00 00:00:00', '2015-06-26 03:12:00', NULL),
(29, 12, 4, '90', 24, 23, '2015-06-26 02:39:00', '2015-06-17 22:42:00', '2015-06-26 03:12:00', NULL),
(30, 12, 2, '50', 80, 23, '2015-06-26 02:40:00', '0000-00-00 00:00:00', '2015-06-26 03:12:00', '2015-06-28 21:29:00'),
(31, 12, 2, '30', 81, 23, '2015-06-28 17:13:00', '2015-06-28 13:13:00', '2015-06-29 04:03:00', '2015-06-29 04:05:00'),
(32, 12, 0, '50', 3, 23, '2015-06-28 19:44:00', '2015-06-28 15:43:00', NULL, NULL),
(33, 23, 2, '20', 45, 12, '2015-06-29 02:00:00', '2015-06-28 21:57:00', '2015-06-29 02:01:00', '2015-06-29 02:02:00'),
(34, 23, 2, '0', 4, 12, '2015-06-29 02:06:00', '2015-06-11 22:06:00', '2015-06-29 02:08:00', '2015-06-29 02:09:00'),
(35, 12, 2, '30', 5, 23, '2015-06-29 03:30:00', '2015-06-28 23:30:00', '2015-06-29 03:34:00', '2015-06-29 03:35:00'),
(36, 12, 0, '30', 6, 23, '2015-07-08 15:00:00', '0000-00-00 00:00:00', NULL, NULL),
(37, 12, 0, '30', 7, 23, '2015-07-08 15:05:00', '0000-00-00 00:00:00', NULL, NULL),
(38, 12, 0, '30', 8, 23, '2015-07-08 15:10:00', '1970-01-13 00:00:00', NULL, NULL),
(39, 12, 0, '30', 9, 23, '2015-07-08 15:11:00', '1970-01-07 00:00:00', NULL, NULL),
(40, 12, 0, '30', 10, 23, '2015-07-08 15:51:00', '2015-07-24 11:50:00', NULL, NULL),
(41, 12, 0, '30', 5, 23, '2015-07-17 19:46:00', '2015-07-17 15:37:00', NULL, NULL),
(42, 12, 0, '50', 1, 23, '2015-07-17 20:14:00', '2015-07-25 16:09:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_confirmations`
--

CREATE TABLE IF NOT EXISTS `email_confirmations` (
`id` int(10) unsigned NOT NULL,
  `usersId` int(10) unsigned NOT NULL,
  `code` char(32) NOT NULL,
  `createdAt` int(10) unsigned NOT NULL,
  `modifiedAt` int(10) unsigned DEFAULT NULL,
  `confirmed` char(1) DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
`fls_id` tinyint(4) NOT NULL,
  `fls_deleted` tinyint(1) DEFAULT '0',
  `fls_type` tinyint(1) DEFAULT '0',
  `fls_uploader` int(11) DEFAULT NULL,
  `fls_uploaded` datetime DEFAULT NULL,
  `fls_name` varchar(255) DEFAULT NULL,
  `fls_desc` varchar(255) DEFAULT NULL,
  `fls_ext` varchar(255) DEFAULT NULL,
  `fls_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
`id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `title` varchar(256) NOT NULL,
  `body` text,
  `status` tinyint(4) NOT NULL COMMENT 'Global message status',
  `created_by` int(11) NOT NULL,
  `created` int(11) DEFAULT NULL,
  `ref_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `type`, `title`, `body`, `status`, `created_by`, `created`, `ref_id`) VALUES
(1, 2, 'Hi', 'Hi', 1, 12, 1436195300, '4c1deef2'),
(2, 2, 'Hi', 'Yo sup.', 1, 23, 1436196009, '4c1deef2'),
(3, 2, 'second message', 'testing second', 1, 12, 1436219206, '2b66da47'),
(4, 2, 'second message', 'OK good.', 1, 23, 1436219633, '2b66da47'),
(18, 3, 'Welcome to QuikWit!', '/', 1, 0, 1436718527, '9d99151e'),
(19, 3, 'Welcome to QuikWit!', '/', 1, 0, 1436986663, 'b491d3fd');

-- --------------------------------------------------------

--
-- Table structure for table `messages_contact`
--

CREATE TABLE IF NOT EXISTS `messages_contact` (
`id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` tinyint(4) NOT NULL,
  `target_id` int(11) NOT NULL COMMENT 'Target user id.',
  `read_on` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages_contact`
--

INSERT INTO `messages_contact` (`id`, `message_id`, `user_id`, `user_type`, `target_id`, `read_on`, `status`, `modified`) VALUES
(1, 1, 12, 1, 23, 1436218241, 1, 1436218241),
(2, 1, 23, 2, 12, 1436196011, 1, 1436196011),
(3, 2, 23, 1, 12, 1436196010, 1, 1436196010),
(4, 2, 12, 2, 23, 1436218241, 1, 1436218241),
(5, 3, 12, 1, 23, NULL, 2, 1436219206),
(6, 3, 23, 2, 12, 1436219635, 1, 1436219635),
(7, 4, 23, 1, 12, 1436219635, 1, 1436219635),
(8, 4, 12, 2, 23, NULL, 0, 1436219633);

-- --------------------------------------------------------

--
-- Table structure for table `messages_notification`
--

CREATE TABLE IF NOT EXISTS `messages_notification` (
`id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `target_id` int(11) NOT NULL COMMENT 'Target user id.',
  `read_on` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages_notification`
--

INSERT INTO `messages_notification` (`id`, `message_id`, `user_id`, `target_id`, `read_on`, `status`, `modified`) VALUES
(1, 18, 0, 66, 1436808748, 1, 1436808748);

-- --------------------------------------------------------

--
-- Table structure for table `messages_system`
--

CREATE TABLE IF NOT EXISTS `messages_system` (
`id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL COMMENT 'Target user id.',
  `read_on` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE IF NOT EXISTS `newsletters` (
`id` int(10) unsigned NOT NULL,
  `email` varchar(112) NOT NULL,
  `created` int(32) unsigned DEFAULT NULL COMMENT 'Timestamp of when the entry is added',
  `status` tinyint(4) DEFAULT '0' COMMENT 'Current subscription status of the user',
  `modified` int(32) unsigned DEFAULT NULL COMMENT 'Timestamp of when the entry is modified'
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `email`, `created`, `status`, `modified`) VALUES
(56, 'carlinjazmin@gmail.com', 1432513449, 1, 1432513449),
(57, 'lin8682@gmail.com', 1432521077, 1, 1433351054),
(58, 'larefin@gmail.com', 1432523997, 1, 1432523997),
(59, 'iraarto@gmail.com', 1432554692, 1, 1432554692),
(60, 'davidburiev@gmail.com', 1432558832, 1, 1432558832),
(61, 'phonejeenee@gmail.com', 1432562493, 1, 1432562493),
(62, 'tanyakastl@gmail.com', 1432563958, 1, 1432563958),
(63, 'Trs.sports@outlook.com', 1432567208, 1, 1432567208),
(64, 'karenf.qm@gmail.com', 1432571094, 1, 1432571094),
(65, 'mah_academi@yahoo.com', 1432571778, 1, 1432571778),
(66, 'julianchung@dominus.ca', 1432576932, 1, 1432576932),
(67, 'lesclarke2000@yahoo.com', 1432578422, 1, 1432578422),
(68, 'info@perrychow.net', 1432578673, 1, 1432578757),
(69, 'valphonso@hotmail.com', 1432579778, 1, 1432579778),
(70, 'pintoian005@hotmail.com', 1432581393, 1, 1432581393),
(71, 'kevg83@hotmail.com', 1432581802, 1, 1432581802),
(72, 'indusharma19@hotmail.com', 1432586138, 1, 1432586138),
(73, 'maryjanehenry@gmail.com', 1432592917, 1, 1432592917),
(74, 'gennablaney67@gmail.com', 1432602542, 1, 1432602542),
(75, 'mmmchefservices@gmail.com', 1432603924, 1, 1432603924),
(76, 'pshafabakhsh@gmail.com', 1432604153, 1, 1432695705),
(77, 'omosamem@gmail.com', 1432607943, 1, 1432607943),
(78, 'contact@indeanaunderhill.com', 1432615355, 1, 1432615355),
(79, 'trinalangthorne@rogers.com', 1432653806, 1, 1432653806),
(80, 'zeeshan_sarani@yahoo.com', 1432657043, 1, 1432657070),
(81, 'alejandrowe@gmail.com', 1432661789, 1, 1432661789),
(82, 'svetlana.piano@outlook.com', 1432664503, 1, 1432664503),
(83, 'yyy@rrr.com', 1432664561, 1, 1432664561),
(84, 'bassbabe_65@hotmail.com', 1432675369, 1, 1432675369),
(85, 'diana@dianajkline.com', 1432676098, 1, 1432676098),
(86, 'mr.rushabh.shah@gmail.com', 1432677522, 1, 1432677522),
(87, 'hackneymanor@gmail.com', 1432680282, 1, 1432680282),
(88, 'adimandja80@gmail.com', 1432685227, 1, 1432685227),
(89, 'brianna.mastromatteo@utoronto.ca', 1432727811, 1, 1432727811),
(90, 'julie@communicationsthatclick.com', 1432747103, 1, 1432747103),
(91, 'tntutorings@gmail.com', 1432748256, 1, 1432748256),
(92, 'saraholuyomi@yahoo.ca', NULL, 1, NULL),
(93, 'tanya_prosser@ymail.com', 1432756866, 1, 1432756866),
(94, 'parithefairy@hotmail.com', 1432761916, 1, 1432761916),
(95, 'aaronkehyeng@yahoo.com', 1432769375, 1, 1432769375),
(97, 'halimah1901@hotmail.com', 1432836523, 1, 1432836523),
(98, 'ulyssus2001@yahoo.com', 1432840383, 1, 1432840383),
(99, 'richard.vetro@gmail.com', 1432847662, 1, 1432847662),
(100, 'calianadances@gmail.com', 1432858431, 1, 1432858431),
(101, 'susanewertfineart@gmail.com', 1432860473, 1, 1432860473),
(102, 'moffat1@sympatico.ca', 1432862734, 1, 1432862734),
(103, 'padmavati.yogi@gmail.com', 1432866441, 1, 1432866441),
(104, 'jamilhaidari@hotmail.com', 1432914329, 1, 1432914329),
(106, 'miaadamouzadeh@gmail.com', 1432927881, 1, 1432927881),
(107, 'michael.kukura@me.com', 1432930808, 1, 1432930808),
(108, 'andrewyounes@live.com', 1432931111, 1, 1432931111),
(109, 'shahdilbutt@gmail.com', 1432935465, 1, 1432935465),
(110, 'wilsons1@live.ca', 1432936826, 1, 1432936826),
(111, 'Katerina.Shimanski@gmail.com', 1432955100, 1, 1432955100),
(112, 'androidtucra@gmail.com', 1433003252, 1, 1433003252),
(113, 'quincy.madiam@gmail.com', 1433005883, 1, 1433005883),
(114, 'coop.saturday@gmail.com', 1433006815, 1, 1433006815),
(115, 'noahkoffman@gmail.com', 1433036965, 1, 1433036965),
(116, 's4dsch@yahoo.ca', 1433040796, 1, 1433040796),
(117, 'leedansoup@gmail.com', 1433077250, 1, 1433077250),
(118, 'quinn_little@hotmail.com', 1433096117, 1, 1433096117),
(120, 'westley.j.wong@gmail.com', 1433196383, 1, 1433196383),
(121, 'portnoii@hotmail.com', 1433210143, 1, 1433210143),
(122, 'comp_soft_train@hotmail.com', 1433266686, 1, 1433266826),
(123, 'united-athletics@outlook.com', 1433294359, 1, 1433294359),
(124, 'hermanchow_023@hotmail.com', 1433340424, 1, 1433860725),
(125, 'chinaram9@gmail.com', 1433373268, 1, 1433373268),
(126, 'farafathy@gmail.com', 1433380102, 1, 1433380102),
(127, 'jessetubbsfitness@gmail.org', 1433385097, 1, 1433385097),
(128, 'krmfiesel@gmail.com', 1433417869, 1, 1433417895),
(129, 'jake.hock19@yahoo.ca', 1433441328, 1, 1433441328),
(130, 'helgapple@gmail.com', 1433444632, 1, 1433444632),
(131, 'info@destinywellness.org', 1433462281, 1, 1433462281),
(132, 'earllingasin@gmail.com', 1433462948, 1, 1433462948),
(133, 'annaisaeva2609@gmail.com', 1433491250, 1, 1433491250),
(134, 'james@elationnation.ca', 1433525198, 1, 1433525198),
(135, 'torontotennispro@gmail.com', 1433527410, 1, 1433527410),
(136, 'shae@pearlfitness.ca', 1433529318, 1, 1433529318),
(137, 'mayank_capoor@hotmail.com', 1433533138, 1, 1433533138),
(138, 'sr.maverick@gmail.com', 1433536556, 1, 1433536556),
(139, 'breakawaytherapyandtraining@gmail.com', 1433605144, 1, 1433605144),
(140, 'tanksconditioning@gmail.com', 1433647313, 1, 1433647313),
(141, 'cxsisback@hotmail.com', 1433788384, 1, 1433788384),
(142, 'f_agueci@hotmail.com', 1433810542, 1, 1433810542),
(143, 'raosmosis@hotmail.com', 1433811850, 1, 1433811850),
(144, 'torontocatsitting@yahoo.ca', 1434046438, 1, 1434046438),
(145, 'shariq.salam@hotmail.com', 1434383449, 1, 1434383449),
(146, 'teletune@hotmail.com', 1434385203, 1, 1434385203),
(147, 'heyteredelila@gmail.com', 1434418157, 1, 1434418157),
(148, 'ami.j.modi@gmail.com', 1434421361, 1, 1434421361),
(150, 'annaflood123@hotmail.com', 1434766571, 1, 1434766571),
(151, 'daniel.tossos@gmail.com', 1435087899, 1, 1435087899),
(152, 'celiacanta@gmail.com', 1435167710, 1, 1435167710),
(153, 'rachel_lindo_49@hotmail.com', 1435250618, 1, 1435250736);

-- --------------------------------------------------------

--
-- Table structure for table `numbers`
--

CREATE TABLE IF NOT EXISTS `numbers` (
`id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `verification_code` int(11) DEFAULT NULL,
  `verified` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oauth`
--

CREATE TABLE IF NOT EXISTS `oauth` (
`auth_id` int(11) NOT NULL COMMENT 'OAuth Table ID',
  `auth_type` int(11) NOT NULL COMMENT 'OAuth Type',
  `auth_access_token` tinyblob COMMENT 'OAuth Access Credentials',
  `auth_refresh_token` varchar(255) DEFAULT NULL COMMENT 'OAuth RefreshToken for Renewing Access Token'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oauth`
--

INSERT INTO `oauth` (`auth_id`, `auth_type`, `auth_access_token`, `auth_refresh_token`) VALUES
(1, 1, 0x7b226163636573735f746f6b656e223a22796132392e4467483938564e50626f55377037736c6f4831442d39763374786e4175654757326b76784d3250765f4a4d2d785f416939637430592d6a7759366e56726e5336363070686871712d4b58786e7777222c22657870697265735f696e223a333630302c2263726561746564223a313432323834313335337d, '1/LZCoDl_YkEMmrpix7AA7DQ_fbliUnpnpEW8XUnnhnUsMEudVrK5jSpoR30zcRFq6');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`ord_id` int(11) NOT NULL,
  `ord_contract` int(11) DEFAULT NULL,
  `ord_progress` int(11) DEFAULT NULL,
  `ord_creator` int(11) DEFAULT NULL,
  `ord_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
`pst_id` int(11) NOT NULL,
  `pst_parent` int(11) DEFAULT NULL,
  `pst_user` int(11) DEFAULT NULL,
  `pst_creator` int(11) DEFAULT NULL,
  `pst_created` datetime DEFAULT NULL,
  `pst_text` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
`rvs_id` int(11) NOT NULL,
  `rvs_user` int(11) NOT NULL,
  `rvs_target` int(11) NOT NULL,
  `rvs_overall` int(11) DEFAULT NULL,
  `rvs_month` int(11) DEFAULT NULL,
  `rvs_year` int(11) DEFAULT NULL,
  `rvs_created` datetime DEFAULT NULL,
  `rvs_title` text NOT NULL,
  `rvs_trust` tinyint(1) DEFAULT NULL,
  `rvs_friend` tinyint(1) DEFAULT NULL,
  `rvs_quality` tinyint(1) DEFAULT NULL,
  `rvs_time` tinyint(1) DEFAULT NULL,
  `rvs_comment` mediumtext,
  `rvs_edited` datetime DEFAULT NULL,
  `rvs_deleted` datetime DEFAULT NULL,
  `rvs_contractId` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`rvs_id`, `rvs_user`, `rvs_target`, `rvs_overall`, `rvs_month`, `rvs_year`, `rvs_created`, `rvs_title`, `rvs_trust`, `rvs_friend`, `rvs_quality`, `rvs_time`, `rvs_comment`, `rvs_edited`, `rvs_deleted`, `rvs_contractId`) VALUES
(28, 12, 23, 3, 6, 2015, '2015-06-27 16:23:00', 'Hi', 3, 4, 2, 3, 'Troll', NULL, NULL, 22),
(29, 12, 23, 4, 3, 2015, '2015-06-28 21:30:00', 'Good', 4, 5, 4, 3, 'Good Work', NULL, NULL, 30),
(30, 23, 12, 4, 6, 2015, '2015-06-29 02:02:00', 'Good ', 4, 4, 3, 3, 'Thanks.', NULL, NULL, 33),
(31, 23, 12, 4, 6, 2015, '2015-06-29 02:09:00', 'Hey', 4, 4, 3, 3, 'Hey', NULL, NULL, 34),
(32, 12, 23, 3, 6, 2015, '2015-06-29 03:42:00', 'hi', 3, 4, 2, 3, 'hi', NULL, NULL, 35),
(33, 12, 23, 5, 6, 2015, '2015-06-29 04:08:00', 'yolo', 5, 5, 4, 5, 'yolo', NULL, NULL, 31);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
`id` int(11) unsigned NOT NULL COMMENT 'ID of the tag',
  `name` varchar(255) NOT NULL COMMENT 'Name of the tag (unqiue)',
  `status` tinyint(4) DEFAULT '0' COMMENT 'Current Status of this tag',
  `created` int(11) unsigned DEFAULT NULL COMMENT 'Timestamp of when this tag is created'
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `status`, `created`) VALUES
(1, 'Basketball', 1, 1421992037),
(2, 'Table Tennis', 1, 1421992173),
(3, 'Swimming', 1, 1421992175),
(4, 'Tennis', 1, 1421992177),
(5, 'Badminton', 1, 1421993655),
(6, 'Soccer', 1, 1421994306),
(7, 'Football', 1, 1421994447),
(8, 'Water Polo', 1, 1421994604),
(9, 'Hiking', 1, 1421994743),
(10, ' Soccer', 1, 1421995220),
(11, ' Badminton', 1, 1421995220),
(12, ' Tennis', 1, 1421995220),
(13, ' Hiking', 1, 1421995220),
(15, 'Dance', 1, 1422299956),
(16, 'BOUNCING', 1, 1422300130),
(17, 'ROLLING', 1, 1422300130),
(18, 'COOKING', 1, 1422300130),
(19, 'Kickboxing ', 1, 1422300165),
(20, 'Rugby', 1, 1422849957),
(21, 'Ice Hockey', 1, 1435525871),
(22, 'Cycling', 1, 1437014459),
(23, 'Dancing', 0, 1437097582),
(24, 'Kicking', 0, 1437097582),
(26, 'Pewpew', 0, 1437097761),
(27, 'Diedie', 0, 1437097761),
(28, 'Heyhey', 0, 1437097929),
(29, 'Yoyo', 0, 1437097992);

-- --------------------------------------------------------

--
-- Table structure for table `tags_standard`
--

CREATE TABLE IF NOT EXISTS `tags_standard` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(4) DEFAULT '1',
  `category_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=526 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags_standard`
--

INSERT INTO `tags_standard` (`id`, `name`, `status`, `category_id`) VALUES
(1, 'Asian Cooking', 1, 6),
(2, 'Baking', 1, 6),
(3, 'Cake Decorating', 1, 6),
(4, 'Cooking', 1, 6),
(5, 'Desserts', 1, 6),
(6, 'French Cooking', 1, 6),
(7, 'Healthy Cooking', 1, 6),
(8, 'Indian Cooking', 1, 6),
(9, 'Italian Cooking', 1, 6),
(10, 'Knife Skills', 1, 6),
(11, 'Middle Eastern Cooking', 1, 6),
(12, 'Mixology', 1, 6),
(13, 'Pasta Making', 1, 6),
(14, 'Accent Reduction', 1, 7),
(15, 'Albanian', 1, 7),
(16, 'American Sign Language', 1, 7),
(17, 'Arabic', 1, 7),
(18, 'Belarusian', 1, 7),
(19, 'Braille', 1, 7),
(20, 'Bulgarian', 1, 7),
(21, 'Cantonese', 1, 7),
(22, 'Catalan', 1, 7),
(23, 'Chinese', 1, 7),
(24, 'Croatian', 1, 7),
(25, 'Czech', 1, 7),
(26, 'Danish', 1, 7),
(27, 'Dialect Coaching', 1, 7),
(28, 'Dutch', 1, 7),
(29, 'English', 1, 7),
(30, 'ESL', 1, 7),
(31, 'Farsi', 1, 7),
(32, 'French', 1, 7),
(33, 'German', 1, 7),
(34, 'Greek', 1, 7),
(35, 'Hebrew', 1, 7),
(36, 'Hindi', 1, 7),
(37, 'Hungarian', 1, 7),
(38, 'Indonesian', 1, 7),
(39, 'Italian', 1, 7),
(40, 'Japanese', 1, 7),
(41, 'Korean', 1, 7),
(42, 'Latin', 1, 7),
(43, 'Mandarin', 1, 7),
(44, 'Norwegian', 1, 7),
(45, 'Polish', 1, 7),
(46, 'Portuguese', 1, 7),
(47, 'Romanian', 1, 7),
(48, 'Russian', 1, 7),
(49, 'Serbian', 1, 7),
(50, 'Sign Language', 1, 7),
(51, 'Spanish', 1, 7),
(52, 'Swedish', 1, 7),
(53, 'Tagalog', 1, 7),
(54, 'Thai', 1, 7),
(55, 'Turkish', 1, 7),
(56, 'Ukranian', 1, 7),
(57, 'Vietnamese', 1, 7),
(58, 'Ableton', 1, 3),
(59, 'Accompaniment', 1, 3),
(60, 'Accordian', 1, 3),
(61, 'Acoustic Guitar', 1, 3),
(62, 'Arrangement and Composition', 1, 3),
(63, 'Audio Engineering', 1, 3),
(64, 'Audition Prep', 1, 3),
(65, 'Bagpipes', 1, 3),
(66, 'Banjo', 1, 3),
(67, 'Baroque Flute', 1, 3),
(68, 'Bass Clarinet', 1, 3),
(69, 'Bass Guitar', 1, 3),
(70, 'Bassoon', 1, 3),
(71, 'Bass Trombone', 1, 3),
(72, 'Beatbox', 1, 3),
(73, 'Bluegrass Guitar', 1, 3),
(74, 'Blues Guitar', 1, 3),
(75, 'Bodhran', 1, 3),
(76, 'Bongo', 1, 3),
(77, 'Broadway Singing', 1, 3),
(78, 'Bugle', 1, 3),
(79, 'Cello', 1, 3),
(80, 'Celtic Flute', 1, 3),
(81, 'Christian Gospel', 1, 3),
(82, 'Clarinet', 1, 3),
(83, 'Classical Guitar', 1, 3),
(84, 'Classical Piano', 1, 3),
(85, 'Classical Voice', 1, 3),
(86, 'Composition', 1, 3),
(87, 'Concertina', 1, 3),
(88, 'Conducting', 1, 3),
(89, 'Conga Drums', 1, 3),
(90, 'Country Guitar', 1, 3),
(91, 'Country Singing', 1, 3),
(92, 'Didgeridoo', 1, 3),
(93, 'Djembe', 1, 3),
(94, 'DJing', 1, 3),
(95, 'Dobro Guitar', 1, 3),
(96, 'Drum', 1, 3),
(97, 'Dulcimer', 1, 3),
(98, 'Ear Training', 1, 3),
(99, 'Erhu', 1, 3),
(100, 'Euphonium', 1, 3),
(101, 'Fiddle', 1, 3),
(102, 'Flamenco Guitar', 1, 3),
(103, 'Flugelhorn', 1, 3),
(104, 'Flute', 1, 3),
(105, 'French Horn', 1, 3),
(106, 'Gospel Singing', 1, 3),
(107, 'Guitar', 1, 3),
(108, 'Hand Drums', 1, 3),
(109, 'Harmonica', 1, 3),
(110, 'Harp', 1, 3),
(111, 'Jazz Voice', 1, 3),
(112, 'Kazoo', 1, 3),
(113, 'Logic Pro Software', 1, 4),
(114, 'Lute', 1, 3),
(115, 'Mandolin', 1, 3),
(116, 'Mariachi', 1, 3),
(117, 'Marimba', 1, 3),
(118, 'Musical Theater', 1, 3),
(119, 'Music Performance', 1, 3),
(120, 'Music Recording', 1, 3),
(121, 'Music Theory', 1, 3),
(122, 'Music Therapy', 1, 3),
(123, 'Oboe', 1, 3),
(124, 'Ocarina', 1, 3),
(125, 'Opera Voice', 1, 3),
(126, 'Organ', 1, 3),
(127, 'Percussion', 1, 3),
(128, 'Piano', 1, 3),
(129, 'Piccolo', 1, 3),
(130, 'Pro Tools Software', 1, 4),
(131, 'Rapping', 1, 3),
(132, 'Saxophone', 1, 3),
(133, 'Singing', 1, 3),
(134, 'Sitar', 1, 3),
(135, 'Songwriting', 1, 3),
(136, 'Steel Drums', 1, 3),
(137, 'Suzuki Method', 1, 3),
(138, 'Synthesizer', 1, 3),
(139, 'Tabla', 1, 3),
(140, 'Tin Whistle', 1, 3),
(141, 'Trombone', 1, 3),
(142, 'Trumpet', 1, 3),
(143, 'Tuba', 1, 3),
(144, 'Ukulele', 1, 3),
(145, 'Upright Bass', 1, 3),
(146, 'Viola', 1, 3),
(147, 'Violin', 1, 3),
(148, 'Vocal Training', 1, 3),
(149, 'Xylophone', 1, 3),
(150, 'Acupuncture', 1, 2),
(151, 'Alexander Technique', 1, 2),
(152, 'Alternative Medicine', 1, 2),
(153, 'Flexibility Training', 1, 2),
(154, 'Health Coaching', 1, 2),
(155, 'Life Coaching', 1, 2),
(156, 'Meditation', 1, 2),
(157, 'Nutrition', 1, 2),
(158, 'Pilates', 1, 2),
(159, 'Qigong', 1, 2),
(160, 'Reiki', 1, 2),
(161, 'Weight Loss', 1, 2),
(162, 'Yoga', 1, 2),
(163, 'Aikido', 1, 2),
(164, 'Archery', 1, 2),
(165, 'Badminton', 1, 2),
(166, 'Baseball', 1, 2),
(167, 'Basketball', 1, 2),
(168, 'Bodybuilding', 1, 2),
(169, 'Bowling', 1, 2),
(170, 'Boxing', 1, 2),
(171, 'Canoeing', 1, 2),
(172, 'Capoeira', 1, 2),
(173, 'Cheerleading', 1, 2),
(174, 'Climbing', 1, 2),
(175, 'Cricket', 1, 2),
(176, 'Cross Country', 1, 2),
(177, 'Curling', 1, 2),
(178, 'Cycling', 1, 2),
(179, 'Dance Fitness', 1, 2),
(180, 'Disc Golf', 1, 2),
(181, 'Driving', 1, 2),
(182, 'Fencing', 1, 2),
(183, 'Field Hockey', 1, 2),
(184, 'Figure Skating', 1, 2),
(185, 'Fishing', 1, 2),
(186, 'Fitness', 1, 2),
(187, 'Football', 1, 2),
(188, 'Golf', 1, 2),
(189, 'Gymnastics', 1, 2),
(190, 'Handball', 1, 2),
(191, 'Hapkido', 1, 2),
(192, 'Horseback Riding', 1, 2),
(193, 'Ice Hockey', 1, 2),
(194, 'Ice Skating', 1, 2),
(195, 'Inline Skating', 1, 2),
(196, 'Jeet Kune Do', 1, 2),
(197, 'Jiu Jitsu', 1, 2),
(198, 'Judo', 1, 2),
(199, 'Kajukenbo', 1, 2),
(200, 'Karate', 1, 2),
(201, 'Kayaking', 1, 2),
(202, 'Kendo', 1, 2),
(203, 'Kenpo', 1, 2),
(204, 'Kickboxing', 1, 2),
(205, 'Krav Maga', 1, 2),
(206, 'Kung Fu', 1, 2),
(207, 'Lacrosse', 1, 2),
(208, 'Mixed Martial Arts', 1, 2),
(209, 'Muay Thai', 1, 2),
(210, 'Ninjutsu', 1, 2),
(211, 'Parkour', 1, 2),
(212, 'Personal Training', 1, 2),
(213, 'Ping Pong', 1, 2),
(214, 'Pole Vaulting', 1, 2),
(215, 'Racquetball', 1, 2),
(216, 'Rock Climbing', 1, 2),
(217, 'Rollerskating', 1, 2),
(218, 'Rugby', 1, 2),
(219, 'Running', 1, 2),
(220, 'Sailing', 1, 2),
(221, 'Scuba Diving', 1, 2),
(222, 'Self Defense', 1, 2),
(223, 'Shot Put', 1, 2),
(224, 'Skateboarding', 1, 2),
(225, 'Skiing', 1, 2),
(226, 'Snowboarding', 1, 2),
(227, 'Soccer', 1, 2),
(228, 'Softball', 1, 2),
(229, 'Spinning', 1, 2),
(230, 'Sports Nutrition', 1, 2),
(231, 'Squash', 1, 2),
(232, 'Strength and Conditioning', 1, 2),
(233, 'Stretching', 1, 2),
(234, 'Surfing', 1, 2),
(235, 'Swimming', 1, 2),
(236, 'Table Tennis', 1, 2),
(237, 'Taekwondo', 1, 2),
(238, 'Tai Chi', 1, 2),
(239, 'Tennis', 1, 2),
(240, 'Track and Field', 1, 2),
(241, 'Triathlon', 1, 2),
(242, 'Tumbling', 1, 2),
(243, 'Volleyball', 1, 2),
(244, 'Wakeboarding', 1, 2),
(245, 'Water Polo', 1, 2),
(246, 'Wing Chun', 1, 2),
(247, 'Wrestling', 1, 2),
(248, 'Zumba', 1, 2),
(249, 'Abstract Painting', 1, 1),
(250, 'Acrylic Painting', 1, 1),
(251, 'Calligraphy', 1, 1),
(252, 'Cartooning', 1, 1),
(253, 'Drawing', 1, 1),
(254, 'Graffiti', 1, 1),
(255, 'Illustration', 1, 1),
(256, 'Oil Painting', 1, 1),
(257, 'Painting', 1, 1),
(258, 'Photography', 1, 1),
(259, 'Pottery', 1, 1),
(260, 'Sculpture', 1, 1),
(261, 'Watercolor Painting', 1, 1),
(262, 'Acrobatics', 1, 1),
(263, 'Acting', 1, 1),
(264, 'Aerial Tissue Silks', 1, 1),
(265, 'African Dance', 1, 1),
(266, 'Ballet', 1, 1),
(267, 'Ballroom Dancing', 1, 1),
(268, 'Bollywood Dancing', 1, 1),
(269, 'Brazilian Samba', 1, 1),
(270, 'Breakdancing', 1, 1),
(271, 'Burlesque', 1, 1),
(272, 'Chachacha', 1, 1),
(273, 'Child Acting', 1, 1),
(274, 'Choir', 1, 1),
(275, 'Choreography', 1, 1),
(276, 'Clowning', 1, 1),
(277, 'Contemporary Art Theory', 1, 1),
(278, 'Contemporary Dance', 1, 1),
(279, 'Dance', 1, 1),
(280, 'Dance Choreography', 1, 1),
(281, 'Film and TV Acting', 1, 1),
(282, 'Filmmaking', 1, 1),
(283, 'Film Production', 1, 1),
(284, 'Film Scoring', 1, 1),
(285, 'Flamenco', 1, 1),
(286, 'Folkloric Dancing', 1, 1),
(287, 'Hip Hop Dance', 1, 1),
(288, 'Improv Acting', 1, 1),
(289, 'Improv Comedy', 1, 1),
(290, 'Indian Dance', 1, 1),
(291, 'Irish Dance', 1, 1),
(292, 'Jazz Dancing', 1, 1),
(293, 'Juggling', 1, 1),
(294, 'Latin Ballroom Dance', 1, 1),
(295, 'Magic', 1, 1),
(296, 'Meisner Technique', 1, 1),
(297, 'Method Acting', 1, 1),
(298, 'Modelling', 1, 1),
(299, 'Modern Dance', 1, 1),
(300, 'Pageantry', 1, 1),
(301, 'Podcasting', 1, 1),
(302, 'Poi', 1, 1),
(303, 'Salsa Dancing', 1, 1),
(304, 'Screenwriting', 1, 1),
(305, 'Shakespearean Acting', 1, 1),
(306, 'Speaking Voice', 1, 1),
(307, 'Spoken Word', 1, 1),
(308, 'Stage Performance', 1, 1),
(309, 'Stand Up Comedy', 1, 1),
(310, 'Swing Dance', 1, 1),
(311, 'Tango', 1, 1),
(312, 'Tap Dance', 1, 1),
(313, 'Theater Acting', 1, 1),
(314, 'Theatrical Dance', 1, 1),
(315, 'Voice Acting', 1, 1),
(316, 'Beading', 1, 1),
(317, 'Carpentry', 1, 1),
(318, 'Ceramics', 1, 1),
(319, 'Crocheting', 1, 1),
(320, 'Embroidery', 1, 1),
(321, 'Floral Arrangement', 1, 1),
(322, 'Gardening', 1, 1),
(323, 'Interior Design', 1, 1),
(324, 'Jewelry Design', 1, 1),
(325, 'Knitting', 1, 1),
(326, 'Needlework', 1, 1),
(327, 'Origami', 1, 1),
(328, 'Paper Crafts', 1, 1),
(329, 'Quilting', 1, 1),
(330, 'Scrapbooking', 1, 1),
(331, 'Sewing', 1, 1),
(332, 'Stained Glass', 1, 1),
(333, 'Weaving', 1, 1),
(334, 'Welding', 1, 1),
(335, 'Woodworking', 1, 1),
(336, 'Manga Drawing', 1, 1),
(337, 'ActivInspire', 1, 4),
(338, 'Adobe Creative Suite', 1, 4),
(339, 'Adobe Dreamweaver', 1, 4),
(340, 'Adobe Flash', 1, 4),
(341, 'Adobe Illustrator', 1, 4),
(342, 'Adobe InDesign', 1, 4),
(343, 'Adobe Lightroom', 1, 4),
(344, 'Adobe Photoshop', 1, 4),
(345, 'Animation', 1, 4),
(346, 'ASP.NET', 1, 4),
(347, 'Autodesk Suite', 1, 4),
(348, 'Autodesk AutoCAD', 1, 4),
(349, 'Autodesk 3ds Max', 1, 4),
(350, 'Autodesk Building Design Suite', 1, 4),
(351, 'Autodesk Inventor', 1, 4),
(352, 'Autodesk Maya', 1, 4),
(353, 'Autodesk Navisworks', 1, 4),
(354, 'Autodesk Product Design Suite', 1, 4),
(355, 'Autodesk Revit', 1, 4),
(356, 'Autodesk SketchBook Pro', 1, 4),
(357, 'Building Information Modeling', 1, 4),
(358, 'CAD', 1, 4),
(359, 'COBOL', 1, 4),
(360, 'Computer Programming', 1, 4),
(361, 'C Programming', 1, 4),
(362, 'CSS', 1, 4),
(363, 'DOS', 1, 4),
(364, 'DSLR Video', 1, 4),
(365, 'Graphic Design', 1, 4),
(366, 'HTML', 1, 4),
(367, 'Java', 1, 4),
(368, 'Javascript', 1, 4),
(369, 'LInux', 1, 4),
(370, 'Lua', 1, 4),
(371, 'Microsoft Access', 1, 4),
(372, 'Microsoft Excel', 1, 4),
(373, 'Microsoft Office', 1, 4),
(374, 'Microsoft PowerPoint', 1, 4),
(375, 'Microsoft Project', 1, 4),
(376, 'Microsoft Publisher', 1, 4),
(377, 'Microsoft Windows', 1, 4),
(378, 'Microsoft Word', 1, 4),
(379, 'Pascal', 1, 4),
(380, 'Perl', 1, 4),
(381, 'PHP', 1, 4),
(382, 'Python', 1, 4),
(383, 'QuickBooks', 1, 4),
(384, 'Responsive Design', 1, 4),
(385, 'Ruby on Rails', 1, 4),
(386, 'SEO', 1, 4),
(387, 'SQL', 1, 4),
(388, 'STATA', 1, 4),
(389, 'Twitter Bootstrap', 1, 4),
(390, 'Typing', 1, 4),
(391, 'Video Production', 1, 4),
(392, 'Visual Basic', 1, 4),
(393, 'Web Analytics', 1, 4),
(394, 'Web Design', 1, 4),
(395, 'WordPress', 1, 4),
(396, 'Zurb Foundation', 1, 4),
(397, 'Facial Exercise', 1, 8),
(398, 'Cosmetology', 1, 8),
(399, 'Fashion Design', 1, 8),
(400, 'Fashion Stylist', 1, 8),
(401, 'Hair Braiding', 1, 8),
(402, 'Hair Coloring', 1, 8),
(403, 'Hair Cutting', 1, 8),
(404, 'Hair Styling', 1, 8),
(405, 'Makeup', 1, 8),
(406, 'Nail Design', 1, 8),
(407, 'Skin Care', 1, 8),
(408, 'Accounting', 1, 5),
(409, 'Algebra', 1, 5),
(410, 'Analytical Chemistry', 1, 5),
(411, 'Analytical Geometry', 1, 5),
(412, 'Anatomy', 1, 5),
(413, 'Anatomy and Physiology', 1, 5),
(414, 'Anthropology', 1, 5),
(415, 'Archaeology', 1, 5),
(416, 'Architecture', 1, 5),
(417, 'Arithmetic', 1, 5),
(418, 'Art History', 1, 5),
(419, 'Astronomy', 1, 5),
(420, 'Basic Math', 1, 5),
(421, 'Bible Studies', 1, 5),
(422, 'Biochemistry', 1, 5),
(423, 'Biology', 1, 5),
(424, 'Botany', 1, 5),
(425, 'Business', 1, 5),
(426, 'Calculus', 1, 5),
(427, 'Cell Biology', 1, 5),
(428, 'Chemical Engineering', 1, 5),
(429, 'Chemistry', 1, 5),
(430, 'Civil Engineering', 1, 5),
(431, 'Creative Writing', 1, 5),
(432, 'Discrete Math', 1, 5),
(434, 'Earth Science', 1, 5),
(435, 'Ecology', 1, 5),
(436, 'Econometrics', 1, 5),
(437, 'Economics', 1, 5),
(438, 'Entrepreneurship', 1, 5),
(439, 'Electrical Engineering', 1, 5),
(440, 'Electronics', 1, 5),
(441, 'Elementary Math', 1, 5),
(442, 'Energy and Earth Sciences', 1, 5),
(443, 'Engineering', 1, 5),
(444, 'English Literature', 1, 5),
(445, 'Environmental Chemistry', 1, 5),
(446, 'Environmental Biology', 1, 5),
(447, 'Environmental Physics', 1, 5),
(448, 'Environmental Geography', 1, 5),
(449, 'Essay Writing', 1, 5),
(450, 'European History', 1, 5),
(451, 'Film Studies', 1, 5),
(452, 'Finance', 1, 5),
(453, 'Fluid Dynamics', 1, 5),
(454, 'Genetics', 1, 5),
(455, 'Geography', 1, 5),
(456, 'Geology', 1, 5),
(457, 'Geometry', 1, 5),
(458, 'GMAT', 1, 5),
(459, 'Grammar', 1, 5),
(460, 'GRE', 1, 5),
(461, 'History', 1, 5),
(462, 'Humanities', 1, 5),
(463, 'International Business', 1, 5),
(464, 'Language', 1, 5),
(465, 'Life Science', 1, 5),
(466, 'Linear Algebra', 1, 5),
(467, 'Linguistics', 1, 5),
(468, 'Literature', 1, 5),
(469, 'LSAT', 1, 5),
(470, 'Macroeconomics', 1, 5),
(471, 'Math', 1, 5),
(472, 'MCAT', 1, 5),
(473, 'Mechanical Engineering', 1, 5),
(474, 'Microbiology', 1, 5),
(475, 'Microeconomics', 1, 5),
(476, 'Molecular Biology', 1, 5),
(477, 'Nursing', 1, 5),
(478, 'Organic Chemistry', 1, 5),
(479, 'PCAT', 1, 5),
(480, 'Philosophy', 1, 5),
(481, 'Phonics', 1, 5),
(482, 'Physical and Earth Sciences', 1, 5),
(483, 'Physical Sciences', 1, 5),
(484, 'Physics', 1, 5),
(485, 'Physiology', 1, 5),
(486, 'Poetry', 1, 5),
(487, 'Pre Algebra', 1, 5),
(488, 'Pre Calculus', 1, 5),
(489, 'Psychology', 1, 5),
(490, 'Public Speaking', 1, 5),
(491, 'Reading', 1, 5),
(492, 'Religious Studies', 1, 5),
(493, 'Robotics', 1, 5),
(494, 'SAT', 1, 5),
(495, 'Science', 1, 5),
(496, 'Social Sciences', 1, 5),
(497, 'Social Studies', 1, 5),
(498, 'Sociology', 1, 5),
(499, 'Speech', 1, 5),
(500, 'Spelling', 1, 5),
(501, 'SPSS', 1, 5),
(502, 'SSAT', 1, 5),
(503, 'Statistics', 1, 5),
(504, 'Study Skills', 1, 5),
(505, 'Test Prep', 1, 5),
(506, 'Thermodynamics', 1, 5),
(507, 'TOEFL', 1, 5),
(508, 'Trigonometry', 1, 5),
(509, 'Vocabulary', 1, 5),
(510, 'World History', 1, 5),
(511, 'Writing', 1, 5),
(512, 'Zoology', 1, 5),
(513, 'G Driving Test', 1, 5),
(514, 'M Driving Test', 1, 5),
(515, 'A Driving Test', 1, 5),
(516, 'D Driving Test', 1, 5),
(517, 'C Driving Test', 1, 5),
(518, 'F Driving Test', 1, 5),
(519, 'B Driving Test', 1, 5),
(520, 'E Driving Test', 1, 5),
(521, 'Pet Grooming', 1, 9),
(522, 'Pet Training', 1, 9),
(523, 'Billiards', 1, 10),
(524, 'Chess', 1, 10),
(525, 'Poker', 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`usr_id` int(11) NOT NULL,
  `usr_deleted` tinyint(1) DEFAULT '0',
  `usr_level` varchar(12) DEFAULT '0',
  `usr_handle` varchar(255) DEFAULT NULL,
  `usr_password` varchar(255) NOT NULL,
  `usr_salt` varchar(255) DEFAULT NULL,
  `usr_email` varchar(255) NOT NULL,
  `usr_firstname` varchar(255) DEFAULT NULL,
  `usr_lastname` varchar(255) DEFAULT NULL,
  `usr_avatar` varchar(225) DEFAULT NULL,
  `usr_cover` varchar(225) DEFAULT NULL,
  `usr_organization` varchar(255) DEFAULT NULL,
  `usr_address` varchar(255) DEFAULT NULL,
  `usr_city` varchar(255) DEFAULT NULL,
  `usr_country` varchar(255) DEFAULT NULL,
  `usr_postalcode` varchar(128) DEFAULT NULL,
  `lat` float(10,6) DEFAULT NULL,
  `lng` float(10,6) DEFAULT NULL,
  `usr_lang` varchar(255) DEFAULT 'English',
  `usr_created` int(11) unsigned DEFAULT NULL,
  `usr_about` mediumtext,
  `usr_teach` tinyint(1) DEFAULT '0',
  `usr_verification` tinyint(4) unsigned DEFAULT '0',
  `usr_verified` int(1) NOT NULL DEFAULT '0' COMMENT 'Flag to indicate if the user has been verified their email address.',
  `usr_reported` int(11) NOT NULL DEFAULT '0',
  `usr_rate` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`usr_id`, `usr_deleted`, `usr_level`, `usr_handle`, `usr_password`, `usr_salt`, `usr_email`, `usr_firstname`, `usr_lastname`, `usr_avatar`, `usr_cover`, `usr_organization`, `usr_address`, `usr_city`, `usr_country`, `usr_postalcode`, `lat`, `lng`, `usr_lang`, `usr_created`, `usr_about`, `usr_teach`, `usr_verification`, `usr_verified`, `usr_reported`, `usr_rate`) VALUES
(4, 0, 'admin', NULL, '$2a$14$jm3htDdQaovDfS1MiEXR6e7nq0Q0HMMy9j/8RCMFRcg2plucUF8cu', NULL, 'jie.q@live.ca', 'Jie', 'Qiu', 'http://www.tute.com/uploads/4/avatar/avatar.jpg', NULL, NULL, NULL, NULL, 'CAN', 'M2H 2W5', 43.798653, -79.335815, 'English', 1422850769, 'abc abc', 1, 0, 0, 0, 0),
(6, 0, 'user', NULL, '$2a$14$FSvy6cs.nLyWpo3hpwvS2OjKj6NL/tNPjxF3xYDdv9JKEHrpxQAaS', NULL, 'jie.q@tute.com', 'jack', 'jones', NULL, NULL, NULL, NULL, NULL, NULL, ' ', 0.000000, 0.000000, 'English', 2147483647, NULL, 0, 0, 0, 0, 0),
(7, 0, 'user', NULL, '$2a$14$TM5lmyx16lUNdgdZwkrTfuervme0ajy9HVC9P.iD5zBGjo56S6yMu', NULL, 'p_nels@yahoo.com', 'pedro', 'nelson', NULL, NULL, NULL, NULL, NULL, '', 'm1h 3g6', 43.780907, -79.249695, 'lang_bg', 1422851300, 'Hey I am cool', 0, 0, 0, 0, 25),
(8, 0, 'user', NULL, '$2a$14$6ogvpnNNxYQjxsjGre9WruI.92inCEfxg7HfBLsQOUS2hAM44rjR6', NULL, 'jie.qiu@tute.com', 'jack', 'jones', 'http://www.tute.com/uploads/8/avatar/avatar.jpg', NULL, NULL, NULL, NULL, 'CAN', ' M2H 2W5', 0.000000, 0.000000, 'FranÃ§ois', 1416145678, '<javascript>alert(''hi'');</javascript>', 0, 0, 0, 0, 0),
(9, 0, 'user', NULL, '$2a$14$FKZRdyE95stNzA8CmIfsiO1x2T82QvQYQe5g3OslDhrip5uzdAtkS', NULL, 'jackie.c@tute.com', 'Jackie', 'Chung', NULL, NULL, NULL, NULL, NULL, NULL, ' ', 0.000000, 0.000000, 'English', 1422851300, NULL, 0, 0, 0, 0, 0),
(10, 0, 'user', NULL, '$2a$14$HGl5N6ET/pTXGleI8VMpXuMkGCLfVaKWz.Cryecddgrj3tXftbPb2', NULL, 'j.c@tute.com', 'Joe', 'Doe', NULL, NULL, NULL, NULL, NULL, NULL, ' ', 0.000000, 0.000000, 'English', 1422851300, NULL, 0, 0, 0, 0, 0),
(11, 0, 'user', NULL, '$2a$14$DCa.OrgOqF1SuCp4k7EwsuaKucSRgmdyr0BrDbaH2e8pVMgIH6BeK', NULL, 'blkclkc@hotmail.com', 'Bryan', 'Leung', NULL, NULL, NULL, NULL, NULL, NULL, ' ', 0.000000, 0.000000, 'English', 1422851300, NULL, 0, 0, 0, 0, 0),
(12, 0, 'user', NULL, '$2a$14$Tn3re588BfG/MH41W4/T3e9Orav9K25tp2pmW1mtAkLf8y1Hn8p/K', NULL, 'test@test.com', 'test', 'test', 'http://localhost/12/avatar/avatar.jpg', NULL, NULL, NULL, NULL, 'IRL', 'L3R7V9', 43.867241, -79.342506, 'Cantonese', 1422851300, 'Hi I am Bryan.', 1, 0, 0, 0, 100),
(13, 0, 'user', NULL, '$2a$14$22.sls2m43lGQ19FAjSeou7d5u0RR.87vmyIQ8jk3ebhteyT2YTRe', NULL, 'tute@tute.com', 'GG', 'QQ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1422851300, NULL, 0, NULL, 0, 0, 0),
(15, 0, 'user', NULL, '$2a$14$ngveUVwLH.5.xaesEo7RKu7K4/hG0euAREQ9/cv/vu.Hbm5MfKN2C', NULL, 'test@quikwit.com', 'Tester', 'QuikWit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1422851300, NULL, 0, NULL, 0, 0, 0),
(16, 0, 'user', NULL, '$2a$14$Wy1dWWae.6487B1rW0nQJ.arQudDCuLkPg5s6ZuzLEj3mCJCiAPYK', NULL, 'tester2@quikwit.com', 'Tute', 'Tester', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1422851300, NULL, 0, NULL, 0, 0, 0),
(18, 0, 'user', NULL, '$2a$14$2ivuWAAwIzIRnvvmiedJbOEGSgnWaFYNJw3HE4mpwGVrpdru86DCi', NULL, 'justindasilva@gmail.com', 'Justin', 'Da Silva', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1423108782, NULL, 0, NULL, 0, 0, 0),
(19, 0, 'user', NULL, '$2a$14$zBAmRLtbya.ZVF7Q/rV91O7dk1FhsUE689TvlcL41T2OSM3bwNKCe', NULL, 'wongryan2001@gmail.com', 'Ryan', 'Wong', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1424668100, NULL, 0, NULL, 0, 0, 0),
(20, 0, 'user', NULL, '$2a$14$B8yejhnUk54fbgndDf.bX.FWr7G2uWLhAmvKhySeTTwqMjxUF5Wnm', NULL, 'wongryan2009@gmail.com', 'Ryan', 'Wong', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1425155672, NULL, 0, NULL, 0, 0, 0),
(21, 0, 'user', NULL, '$2a$14$ntXmplXKL6v6d4/WRHB/wezbc/G6IRgZu9brMgj7QLKXmCMZYe/Bu', NULL, 'ryanwongnascent@gmail.com', 'Ryan Puikin', 'Wong', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1431907508, NULL, 1, NULL, 0, 0, 0),
(22, 0, 'user', NULL, '$2a$14$Ivww8.7QNuagZJl2EouNiebi3W3/UJBF/K55klfOxzWrXO3V8a9HK', NULL, 'javierching@hotmail.com', 'Javier', 'Ching', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1434815930, NULL, 0, NULL, 0, 0, 0),
(23, 0, 'user', NULL, '$2a$14$wzMdS6uFwqa6tokBOdmY1up3LXyPi4584Nh2X1WyJjpeGaX458Xhq', NULL, 'yolo@hotmail.com', 'Yolo', 'yolo', NULL, NULL, NULL, NULL, NULL, '', 'M1P4P5', 43.776714, -79.258507, 'lang_zh-CN', 1434816339, NULL, 1, NULL, 0, 0, 30),
(31, 0, 'user', NULL, '$2a$14$Bpf3GXRxIOm9M2nL65/7ieMPUTWCuwnrLk6VHz7gQiD7McXgptswW', NULL, 'david@david.com', 'David', 'david', NULL, NULL, NULL, NULL, NULL, NULL, 'M1H 3G6', 43.780907, -79.249695, 'English', 1435795436, NULL, 1, NULL, 0, 0, 0),
(32, 0, 'user', NULL, '$2a$14$1jIehcT94p1KGxMTT6NNoePjX5WMT.gcTHMwgH73odSA4IYIOz3Vy', NULL, 'javier.ching@gmail.com', 'Javier', 'Ching', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1436023577, NULL, 0, 0, 0, 0, 0),
(33, 0, 'user', NULL, '$2a$14$ntglaop2aWr8elRnAP38z.QKBAVyORCDzRhAkhi2bVBghCuc4/xoK', NULL, 'jiddas@hotmail.com', 'Javier', 'Ching', NULL, NULL, NULL, NULL, NULL, NULL, 'L3R7V9', 43.867241, -79.342506, 'English', 1436103820, NULL, 0, 0, 0, 0, 0),
(35, 0, 'user', NULL, '$2a$14$2oTaOWdssauQxltqgPe5m.7K4gj37OyKWjMFSStlXZKVMojTiShVi', NULL, 'jhnching15@gmail.com', 'Javier', 'Ching', NULL, NULL, NULL, NULL, NULL, NULL, 'L3R7V9', 43.867241, -79.342506, 'English', 1436120042, 'Ding lae goh faeg', 0, 0, 0, 0, 0),
(36, 0, 'user', NULL, '$2a$14$w6luCvYRboNSILB8KY4VFeVMqc5imFwIVI621rabe2Mk5..r1jhgm', NULL, 'bla@bla.com', 'bla', 'bla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1436126445, NULL, 0, 0, 0, 0, 0),
(37, 0, 'user', NULL, '$2a$14$DpEnHirmg8B4uQaGXVxqduUhWIK0pFz3b1Ewu2zk9UmnY1JAaHAKe', NULL, 'bal@bla.com', 'bla', 'bla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1436126542, NULL, 0, 0, 0, 0, 0),
(38, 0, 'user', NULL, '$2a$14$fKgbgHaec60Q0j2ZI7X6W.Xs4HhzM3apaBO99a2bBDdeYq3ZCynkS', NULL, 'mango@mango.com', 'mango', 'mango', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1436126601, NULL, 0, NULL, 0, 0, 0),
(39, 0, 'user', NULL, '$2a$14$MLXSXbqRnBaVNN003w2WXerowQ/jxaEXr3WP1TaC4oghA9bsmmXPC', NULL, 'gah@gah.com', 'gah', 'gah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1436126632, NULL, 0, 0, 0, 0, 0),
(40, 0, 'user', NULL, '$2a$14$lguIwOSwnfuFeIn7lNNOfO/PWh/nqYTHPzmNuhS7oPBZvA41QgFHG', NULL, 'pok@gai.com', 'pok', 'pok', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1436126757, NULL, 0, 0, 0, 0, 0),
(66, 0, 'user', NULL, '$2a$14$On11HP14f9RDF3JoZc2TLe4H07l4h82u8TAPNOzZ1QYC3Zdd0IMP2', NULL, 'water@water.com', 'Water', 'Water', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1436718527, NULL, 0, NULL, 0, 0, 0),
(67, 0, 'user', NULL, '$2a$14$AqjzdomKuakqjZbkpQ3dBOqit1Sk5ahaQ6R67Np8B8zA0mRiozRsK', NULL, 'hi@hi.com', 'hi', 'hi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'English', 1436986663, NULL, 0, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_connections`
--

CREATE TABLE IF NOT EXISTS `users_connections` (
`usr_id` int(11) NOT NULL,
  `usr_connection_user` int(11) NOT NULL COMMENT 'refer to your user id on your application',
  `usr_provider` varchar(50) NOT NULL,
  `usr_hybridauth_session` text NOT NULL COMMENT 'will contain the hybridauth session data',
  `usr_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usr_profile` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_connections`
--

INSERT INTO `users_connections` (`usr_id`, `usr_connection_user`, `usr_provider`, `usr_hybridauth_session`, `usr_updated_at`, `usr_profile`) VALUES
(7, 33, 'facebook', 'a:2:{s:35:"hauth_session.facebook.is_logged_in";s:4:"i:1;";s:41:"hauth_session.facebook.token.access_token";s:205:"s:196:"CAAK8cd5D7loBAA2wJmWifXIGxOj4wEb4TqAqoDltZCpjhZCp9BWLjvfCTQXf26tS2lvRpO9oYC96XNJkAFZBXXCroimHMhFMiaNk3ovmfcDjI2hZBWmR1irF21IogA1USJTXcEEyUKDeyEGU3H6kkmYaiR462b90BZCkvTzZAiihEYyGevHFbKRNKqlFVbD4QZD";";}', '2015-07-11 01:01:20', 'https://www.facebook.com/app_scoped_user_id/10153155940548962/');

-- --------------------------------------------------------

--
-- Table structure for table `users_experience`
--

CREATE TABLE IF NOT EXISTS `users_experience` (
`id` int(11) NOT NULL,
  `title` varchar(512) NOT NULL,
  `content` text NOT NULL,
  `start_month` int(11) NOT NULL,
  `start_year` int(11) NOT NULL,
  `end_month` int(11) NOT NULL,
  `end_year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_experience`
--

INSERT INTO `users_experience` (`id`, `title`, `content`, `start_month`, `start_year`, `end_month`, `end_year`, `user_id`, `create_date`) VALUES
(1, 'Head Chef', 'Cooked', 1, 2014, 1, 1111, 7, '2015-05-04 14:39:49'),
(2, 'Developer', 'Coding', 3, 2012, 1, 1111, 21, '2015-05-18 02:01:39'),
(3, 'Badminton Player', 'Badminton', 9, 2014, 1, 1111, 12, '2015-06-23 17:20:37'),
(8, 'yolo', 'yolo', 2, 2014, 1, 1111, 23, '2015-06-29 03:40:02');

-- --------------------------------------------------------

--
-- Table structure for table `users_password_recovery`
--

CREATE TABLE IF NOT EXISTS `users_password_recovery` (
`id` int(11) unsigned NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `old_password` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `created` int(32) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `modified` int(32) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_password_recovery`
--

INSERT INTO `users_password_recovery` (`id`, `user_id`, `old_password`, `hash`, `created`, `status`, `modified`) VALUES
(1, 8, '$2a$14$tuXcZed2bbZcxF1ohfvPH.ukfcbijOO1CslYpa1P3sOInmdx6DhIm', '6de8347aa64b8e0467bf986f3f6a28fa', 1430631389, 1, 1430636670),
(2, 12, '$2a$14$Tn3re588BfG/MH41W4/T3e9Orav9K25tp2pmW1mtAkLf8y1Hn8p/K', 'fca4d52e6368b1e353910823335fd155', 1433985941, 0, NULL),
(3, 12, '$2a$14$Tn3re588BfG/MH41W4/T3e9Orav9K25tp2pmW1mtAkLf8y1Hn8p/K', '689388ceb23056a9b6935b04cda1ef1b', 1433985942, 0, NULL),
(4, 12, '$2a$14$Tn3re588BfG/MH41W4/T3e9Orav9K25tp2pmW1mtAkLf8y1Hn8p/K', '0dae7a1f8700bbc1881320001cb1c15c', 1433985943, 0, NULL),
(5, 12, '$2a$14$Tn3re588BfG/MH41W4/T3e9Orav9K25tp2pmW1mtAkLf8y1Hn8p/K', '975ace9312bbf8f03984cbb04633a290', 1433985944, 0, NULL),
(6, 12, '$2a$14$Tn3re588BfG/MH41W4/T3e9Orav9K25tp2pmW1mtAkLf8y1Hn8p/K', 'c2b22bbab4eb653c79cfe89825089030', 1433985944, 0, NULL),
(7, 12, '$2a$14$Tn3re588BfG/MH41W4/T3e9Orav9K25tp2pmW1mtAkLf8y1Hn8p/K', 'fa3804291895350b12a68030fff68eab', 1433985944, 0, NULL),
(8, 12, '$2a$14$Tn3re588BfG/MH41W4/T3e9Orav9K25tp2pmW1mtAkLf8y1Hn8p/K', 'cd990d15d5f462b08db2eac1ba4a4608', 1433985945, 0, NULL),
(9, 12, '$2a$14$Tn3re588BfG/MH41W4/T3e9Orav9K25tp2pmW1mtAkLf8y1Hn8p/K', '1ac3af2d7008dc0d2df431652369d95c', 1433985946, 0, NULL),
(10, 12, '$2a$14$Tn3re588BfG/MH41W4/T3e9Orav9K25tp2pmW1mtAkLf8y1Hn8p/K', 'ed9529b1bdae179e9152a2503d461431', 1433985952, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_search`
--

CREATE TABLE IF NOT EXISTS `users_search` (
`id` int(11) NOT NULL,
  `lesson_complete` int(11) NOT NULL DEFAULT '0',
  `num_rating` int(11) NOT NULL DEFAULT '0',
  `rating` float NOT NULL DEFAULT '0',
  `intro_video` int(11) NOT NULL DEFAULT '0',
  `uploads` float NOT NULL DEFAULT '0',
  `experience_filled` int(11) NOT NULL DEFAULT '0',
  `lat` float NOT NULL DEFAULT '0',
  `lng` float NOT NULL DEFAULT '0',
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `price` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_search`
--

INSERT INTO `users_search` (`id`, `lesson_complete`, `num_rating`, `rating`, `intro_video`, `uploads`, `experience_filled`, `lat`, `lng`, `usr_id`, `price`) VALUES
(12, 0, 0, 0, 0, 0, 0, 0, 0, 21, 0),
(16, 2, 2, 4, 0, 0, 1, 43.8672, -79.3425, 12, 100),
(18, 4, 4, 3.75, 0, 0, 1, 9999, 9999, 23, 30),
(19, 0, 0, 0, 0, 0, 0, 9999, 9999, 31, 0),
(20, 2, 5, 0, 0, 0, 0, 1234, 1234, 36, 10000),
(22, 4, 1, 0, 0, 0, 0, 1234, 1234, 38, 9000);

-- --------------------------------------------------------

--
-- Table structure for table `user_tags`
--

CREATE TABLE IF NOT EXISTS `user_tags` (
  `user_id` int(11) NOT NULL,
  `tag_id` int(11) unsigned NOT NULL,
  `created` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_tags`
--

INSERT INTO `user_tags` (`user_id`, `tag_id`, `created`) VALUES
(4, 1, 1422849957),
(4, 7, 1422849957),
(4, 10, 1422849957),
(7, 17, 1422300130),
(7, 18, 1422300130),
(12, 2, 1437077204),
(13, 1, 1421995443),
(13, 10, 1421995444),
(23, 21, 1435525871),
(31, 21, 1435950135),
(36, 21, 1435950136),
(38, 21, 1435950136),
(66, 5, 1436831992);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
`vid_id` int(11) NOT NULL,
  `vid_user` int(11) DEFAULT NULL,
  `vid_type` tinyint(1) DEFAULT '0',
  `vid_name` varchar(255) DEFAULT NULL,
  `vid_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wall`
--

CREATE TABLE IF NOT EXISTS `wall` (
`id` int(11) unsigned NOT NULL COMMENT 'ID of the media wall',
  `media` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL COMMENT 'Media wall title',
  `content` text COMMENT 'Media wall body',
  `tag_id` int(10) unsigned NOT NULL,
  `created` int(11) unsigned DEFAULT NULL COMMENT 'Timestamp of when this media wall is created',
  `createdby` int(11) DEFAULT NULL COMMENT 'ID of the user that created this media wall',
  `modified` int(11) unsigned DEFAULT NULL COMMENT 'Last modified timestamp of this media wall'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wall`
--

INSERT INTO `wall` (`id`, `media`, `title`, `content`, `tag_id`, `created`, `createdby`, `modified`) VALUES
(1, 'http://www.quikwit.co/uploads/4/videoviewdemo.mp4', 'Test Making new Post', 'This is a test', 0, 1419691651, 4, 1419691651),
(2, 'http://www.quikwit.co/uploads/4/videoviewdemo.mp4', 'Test 123', '123213213213', 0, 1419691837, 4, 1419691837),
(3, 'http://www.quikwit.co/uploads/4/videoviewdemo.mp4', 'THis is a demo', 'this is a demo video', 0, 1419692271, 4, 1419692271);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abuses`
--
ALTER TABLE `abuses`
 ADD PRIMARY KEY (`abs_id`), ADD KEY `abs_user` (`abs_user`), ADD KEY `abs_creator` (`abs_target`);

--
-- Indexes for table `bookmarks`
--
ALTER TABLE `bookmarks`
 ADD PRIMARY KEY (`bmk_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
 ADD PRIMARY KEY (`user_id`,`contact_id`), ADD KEY `contact_id` (`contact_id`);

--
-- Indexes for table `contracthistory`
--
ALTER TABLE `contracthistory`
 ADD PRIMARY KEY (`history_id`);

--
-- Indexes for table `contracts`
--
ALTER TABLE `contracts`
 ADD PRIMARY KEY (`cnt_id`), ADD KEY `cnt_user` (`cnt_user`), ADD KEY `cnt_tagid` (`cnt_tagid`);

--
-- Indexes for table `email_confirmations`
--
ALTER TABLE `email_confirmations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
 ADD PRIMARY KEY (`fls_id`), ADD KEY `fls_uploader` (`fls_uploader`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`id`), ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `messages_contact`
--
ALTER TABLE `messages_contact`
 ADD PRIMARY KEY (`id`), ADD KEY `Message To User Index` (`message_id`,`user_id`), ADD KEY `User ID Foreign Key Constraint` (`user_id`), ADD KEY `target_id` (`target_id`);

--
-- Indexes for table `messages_notification`
--
ALTER TABLE `messages_notification`
 ADD PRIMARY KEY (`id`), ADD KEY `target_id to user_id` (`target_id`), ADD KEY `message_id to  message id` (`message_id`,`user_id`);

--
-- Indexes for table `messages_system`
--
ALTER TABLE `messages_system`
 ADD PRIMARY KEY (`id`), ADD KEY `Message To User Index` (`message_id`,`user_id`), ADD KEY `User ID Foreign Key Constraint` (`user_id`), ADD KEY `target_id` (`target_id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `Email Index` (`email`) COMMENT 'Use User Email as INDEX';

--
-- Indexes for table `numbers`
--
ALTER TABLE `numbers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth`
--
ALTER TABLE `oauth`
 ADD PRIMARY KEY (`auth_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`ord_id`), ADD KEY `ord_contract` (`ord_contract`), ADD KEY `ord_creator` (`ord_creator`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
 ADD PRIMARY KEY (`pst_id`), ADD KEY `pst_user` (`pst_user`), ADD KEY `pst_creator` (`pst_creator`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
 ADD PRIMARY KEY (`rvs_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `tags_standard`
--
ALTER TABLE `tags_standard`
 ADD PRIMARY KEY (`id`), ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`usr_id`), ADD UNIQUE KEY `email` (`usr_email`);

--
-- Indexes for table `users_connections`
--
ALTER TABLE `users_connections`
 ADD PRIMARY KEY (`usr_id`);

--
-- Indexes for table `users_experience`
--
ALTER TABLE `users_experience`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_password_recovery`
--
ALTER TABLE `users_password_recovery`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users_search`
--
ALTER TABLE `users_search`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `usr_id` (`usr_id`);

--
-- Indexes for table `user_tags`
--
ALTER TABLE `user_tags`
 ADD PRIMARY KEY (`user_id`,`tag_id`), ADD KEY `user_tags_ibfk_2` (`tag_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
 ADD PRIMARY KEY (`vid_id`), ADD KEY `vid_user` (`vid_user`);

--
-- Indexes for table `wall`
--
ALTER TABLE `wall`
 ADD PRIMARY KEY (`id`), ADD KEY `createdby` (`createdby`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abuses`
--
ALTER TABLE `abuses`
MODIFY `abs_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bookmarks`
--
ALTER TABLE `bookmarks`
MODIFY `bmk_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `contracthistory`
--
ALTER TABLE `contracthistory`
MODIFY `history_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=206;
--
-- AUTO_INCREMENT for table `contracts`
--
ALTER TABLE `contracts`
MODIFY `cnt_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `email_confirmations`
--
ALTER TABLE `email_confirmations`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
MODIFY `fls_id` tinyint(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `messages_contact`
--
ALTER TABLE `messages_contact`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `messages_notification`
--
ALTER TABLE `messages_notification`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `messages_system`
--
ALTER TABLE `messages_system`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=154;
--
-- AUTO_INCREMENT for table `numbers`
--
ALTER TABLE `numbers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth`
--
ALTER TABLE `oauth`
MODIFY `auth_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'OAuth Table ID',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `ord_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
MODIFY `pst_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
MODIFY `rvs_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID of the tag',AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tags_standard`
--
ALTER TABLE `tags_standard`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=526;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `usr_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `users_connections`
--
ALTER TABLE `users_connections`
MODIFY `usr_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users_experience`
--
ALTER TABLE `users_experience`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users_password_recovery`
--
ALTER TABLE `users_password_recovery`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users_search`
--
ALTER TABLE `users_search`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
MODIFY `vid_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wall`
--
ALTER TABLE `wall`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID of the media wall',AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `abuses`
--
ALTER TABLE `abuses`
ADD CONSTRAINT `abuses_ibfk_1` FOREIGN KEY (`abs_user`) REFERENCES `users` (`usr_id`);

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`usr_id`),
ADD CONSTRAINT `contacts_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `users` (`usr_id`);

--
-- Constraints for table `contracts`
--
ALTER TABLE `contracts`
ADD CONSTRAINT `contracts_tags_1` FOREIGN KEY (`cnt_tagid`) REFERENCES `tags_standard` (`id`),
ADD CONSTRAINT `contracts_ibfk_1` FOREIGN KEY (`cnt_user`) REFERENCES `users` (`usr_id`);

--
-- Constraints for table `files`
--
ALTER TABLE `files`
ADD CONSTRAINT `files_ibfk_1` FOREIGN KEY (`fls_uploader`) REFERENCES `users` (`usr_id`);

--
-- Constraints for table `messages_contact`
--
ALTER TABLE `messages_contact`
ADD CONSTRAINT `Message ID Foreign Key` FOREIGN KEY (`message_id`) REFERENCES `messages` (`id`),
ADD CONSTRAINT `Target ID Foreign Key Constraint` FOREIGN KEY (`target_id`) REFERENCES `users` (`usr_id`),
ADD CONSTRAINT `User ID Foreign Key Constraint` FOREIGN KEY (`user_id`) REFERENCES `users` (`usr_id`);

--
-- Constraints for table `messages_notification`
--
ALTER TABLE `messages_notification`
ADD CONSTRAINT `message to message id` FOREIGN KEY (`message_id`) REFERENCES `messages` (`id`),
ADD CONSTRAINT `target to user id` FOREIGN KEY (`target_id`) REFERENCES `users` (`usr_id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`ord_contract`) REFERENCES `contracts` (`cnt_id`),
ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`ord_creator`) REFERENCES `users` (`usr_id`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`pst_user`) REFERENCES `users` (`usr_id`),
ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`pst_creator`) REFERENCES `users` (`usr_id`);

--
-- Constraints for table `tags_standard`
--
ALTER TABLE `tags_standard`
ADD CONSTRAINT `tags_categories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `users_password_recovery`
--
ALTER TABLE `users_password_recovery`
ADD CONSTRAINT `users_password_recovery_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`usr_id`);

--
-- Constraints for table `users_search`
--
ALTER TABLE `users_search`
ADD CONSTRAINT `users_search_ibfk_1` FOREIGN KEY (`usr_id`) REFERENCES `users` (`usr_id`);

--
-- Constraints for table `user_tags`
--
ALTER TABLE `user_tags`
ADD CONSTRAINT `user_tags_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`usr_id`),
ADD CONSTRAINT `user_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tags_standard` (`id`);

--
-- Constraints for table `videos`
--
ALTER TABLE `videos`
ADD CONSTRAINT `videos_ibfk_1` FOREIGN KEY (`vid_user`) REFERENCES `users` (`usr_id`);

--
-- Constraints for table `wall`
--
ALTER TABLE `wall`
ADD CONSTRAINT `wall_ibfk_1` FOREIGN KEY (`createdby`) REFERENCES `users` (`usr_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
