<?php

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);

chdir(dirname(__DIR__));

$cwd = getcwd();
DEFINE('DS', DIRECTORY_SEPARATOR);
defined('APPLICATION_ROOT') || define('APPLICATION_ROOT', $cwd);
defined('APPLICATION_PATH') || define('APPLICATION_PATH', $cwd . '/app');
defined('APPLICATION_PUBLIC') || define('APPLICATION_PUBLIC', $cwd . '/public');
defined('APPLICATION_VENDOR') || define('APPLICATION_VENDOR', $cwd . '/vendor');
defined('APPLICATION_URL') || define('APPLICATION_URL', $_SERVER["SERVER_NAME"] . (!in_array($_SERVER['SERVER_PORT'], [80, 443]) ? ':' . $_SERVER['SERVER_PORT'] : ''));

include_once(APPLICATION_PATH . '/config/constants.php');
defined('APPLICATION_ENV') || define('APPLICATION_ENV', getenv('APPLICATION_ENV')? getenv('APPLICATION_ENV') : ENV_DEVELOPMENT);

unset($cwd);

if (APPLICATION_ENV === ENV_DEVELOPMENT || APPLICATION_ENV === ENV_STAGING) {
    $debug = new \Phalcon\Debug();
    $debug->listen();
    ini_set('display_startup_errors',1);
    ini_set('display_errors',1);
    ini_set('log_errors', 1);
    error_reporting(E_ALL);
} else {
    ini_set('display_startup_errors', 0);
    ini_set('display_errors', 0);
    ini_set('log_errors', 1);
    error_reporting(0);
}

//try {

    # Default to utc time.
    date_default_timezone_set('UTC');

    // Autoloader
    $loader = new \Phalcon\Loader();
    $loader->registerDirs([
        APPLICATION_PATH . '/controllers/',
        APPLICATION_PATH . '/models/',
        APPLICATION_PATH . '/config/',
        APPLICATION_PATH . '/forms/', //GQ - autoload user forms.
        APPLICATION_PATH . ''
    ]);

    /* GQ 2014-08-01 : register Google API client */

    $loader->registerNamespaces(
        [
            'Component' => APPLICATION_PATH . '/components',
            'PrettyDateTime' => APPLICATION_VENDOR . '/danielstjules/php-pretty-datetime/src'
        ]
    )->register();

    $loader->register();

    require_once APPLICATION_VENDOR . '/autoload.php';

    // Dependency Injection
    $di = new \Phalcon\DI\FactoryDefault();

    // Return API Config
    $di->setShared('config', function() {
        $config = require_once APPLICATION_PATH . '/config/environment/' . APPLICATION_ENV . '/config.php';
        return $config;
    });

    //  Return API Config
    $di->setShared('api', function() {
        return include APPLICATION_PATH . '/config/api.php';
    });

    $di->setShared('SwiftMailer', function() {
        return include APPLICATION_VENDOR . '/swiftmailer/swiftmailer/lib/swift_required.php';
    });

    $di->setShared('hybridauth', function() use ($di) {
        $authConfig = include APPLICATION_PATH . '/config/hybridauth.php';
        require_once( APPLICATION_VENDOR . '/hybridauth/hybridauth/hybridauth/Hybrid/Auth.php' );
        $hybridauth = new Hybrid_Auth($authConfig);
        return $hybridauth;
    });
    
    // Database
    $di->set('db', function() use ($di) {
        $dbConfig = (array)$di->get('config')->get('db');
        $db = new \Phalcon\Db\Adapter\Pdo\Mysql($dbConfig);
        return $db;
    });

    // View
    $di->set('view', function() use ($di) {
        $view = new \Phalcon\Mvc\View();
        $view->setViewsDir(APPLICATION_PATH . '/views/');
        $view->registerEngines(array(
            '.volt' => function($view, $di) {
            $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
            $volt->setOptions(array(
              'compiledPath' => APPLICATION_PATH . '/cache/',
              'stat' => true,
              'compileAlways' => true
            ));
            return $volt;
            }
        ));
        $view->di = $di;
        return $view;
    });

    /**
     * Setup response caching values
     */
    /* $di->set('response', function(){
        $response = new \Phalcon\Http\Response();

        // we want the cache to expire at midnight Phx time every night
        $s = strtotime('midnight +1 day') - time();

        $ExpireDate = new \DateTime();
        $ExpireDate->modify('+' . $s . ' seconds');

        $response->setExpires($ExpireDate);

        $response->setHeader('Pragma', 'cache');
        $response->setHeader('Cache-Control', 'public, max-age=' . $s . ', s-max=' . $s . ', must-revalidate, proxy-revalidate');
        return $response;
    }); */

    // Email Template
    $di->setShared('emailTemplate', function() {
        $view = new \Phalcon\Mvc\View\Simple();
        $view->setViewsDir(APPLICATION_PATH . '/components/Mailer/templates/');
        $view->registerEngines(array(
            '.volt' => function($view, $di) {
                $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
                $volt->setOptions(array(
                    'compiledPath' => APPLICATION_PATH . '/cache/email',
                    'stat' => true,
                    'compileAlways' => true
                ));
                return $volt;
            }
        ));
        return $view;
    });

    // Mailer Manager
    $di->set('mailerManager', function() {
        $mailerManager = new \Component\Mailer\Manager();
        return $mailerManager;
    });

    // Setup a base URI so that all generated URI
    $di->set('url', function () use ($di) {
        $url = new \Phalcon\Mvc\Url();
        $dispatcher = $di->getShared('dispatcher');
        $url->setBaseUri('https://'.APPLICATION_URL."/");
        $url->setStaticBaseUri('/');
        $url->setBasePath(APPLICATION_PATH);
        return $url;
    });

    // Setup a base URI so that all generated URI
    //$di->set('secured-url', function () use ($di) {
        //$url = new \Phalcon\Mvc\Url();
        //$dispatcher = $di->getShared('dispatcher');
        //$url->setBaseUri('https://'.APPLICATION_URL."/");
        //$url->setStaticBaseUri('/');
        //$url->setBasePath(APPLICATION_PATH);
        //return $url;
    //});

    // Router
    //$di->set('router', function() {
    //    $router = new \Phalcon\Mvc\Router();
    //    $router->mount(new GlobalRoutes());
    //    return $router;
    //});

    // Security
    $di->set('security', function() {
        $security = new Security();
        return $security;
    });

    // Session
    $di->setShared('session', function() {
        $session = new \Phalcon\Session\Adapter\Files();
        $session->start();
        return $session;
    });

    // Return custom components
    $di->setShared('component', function() {
        $obj = new stdClass();
        $obj->helper = new \Component\Helper;
        $obj->user = new \Component\User;
        return $obj;
    });

    // Meta-Data
    // $di['modelsMetadata'] = function() {
    //     $metaData = new \Phalcon\Mvc\Model\MetaData\Apc([
    //         "lifetime" => 86400,
    //         "prefix"   => "metaData"
    //     ]);
    //     return $metaData;
    // };

    $di->set('flash', function() {
        // There is a Direct, and a Session
        $flash = new \Phalcon\Flash\Session(array(
            'error' => 'alert-danger',
            'success' => 'alert-success',
            'notice' => 'alert-info',
            'warning' => 'alert-warning',
        ));
        return $flash;
    });

    // Custom Dispatcher (Overrides the default)
    $di->set('dispatcher', function() use ($di) {
        $eventsManager = $di->getShared('eventsManager');

        // Custom ACL Class
        $permission = new Permission();

        // Listen for events from the permission class
        $eventsManager->attach('dispatch', $permission);

        // Handles exceptions
        // $eventsManager->attach("dispatch:beforeDispatchLoop", function($event, $dispatcher, $exception){
        $eventsManager->attach("dispatch:beforeException", function($event, $dispatcher, $exception){
            if ($exception) {
                $dispatcher->forward(array(
                        'controller' => 'index',
                        'action' => 'show404'
                ));
                return false;
            }

            if ($event->getType() == 'beforeException') {
                switch ($exception->getCode()) {
                    case \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                    case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                        $dispatcher->forward(array(
                            'controller' => 'error',
                            'action' => 'show404'
                        ));
                    return false;
                }
            }
        });

        $dispatcher = new \Phalcon\Mvc\Dispatcher();
        $dispatcher->setEventsManager($eventsManager);

        return $dispatcher;
    }, true);

    $di->set('modelsManager', function() {
        return new Phalcon\Mvc\Model\Manager();
    });

    /* GQ 2014-08-01 : register Google API library as part of the include path. */
    set_include_path(get_include_path().PATH_SEPARATOR.APPLICATION_PATH."/../app/library");


    // Deploy the App
    $app = new \Phalcon\Mvc\Application($di);
    echo $app->handle()->getContent();



/* } catch(\Phalcon\Exception $e) {

    if (APPLICATION_ENV === ENV_DEVELOPMENT || APPLICATION_ENV === ENV_STAGING) {
        echo '<pre>';
        echo get_class($e), ": ", $e->getMessage(), "\n";
        echo " File=", $e->getFile(), "\n";
        echo " Line=", $e->getLine(), "\n";
        echo $e->getTraceAsString();
        echo '</pre>';
        exit;
    }
} */
