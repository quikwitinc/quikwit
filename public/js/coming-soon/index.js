$(document).ready(function(){

    $('input#submit', 'form#fmnewsletter').click(function(){
        ga('send', 'event', 'button', 'click', 'Newsletter Signup');
        $.getJSON('/coming-soon/register',
            {email : $('#email', 'form#fmnewsletter').val()},
            function (data) {
                $('div#messages').html('');
                if(data.success) {
                    ga('send', 'event', 'response', 'Newsletter Signup Response Success');
                    $('div#messages').append('<p style="color:green;">'+data.message+'</p>');
                } else {
                    ga('send', 'event', 'response', 'Newsletter Signup Response Failed');
                    for (var i=0; i<data.content.length; i++) {
                        $('div#messages').append('<p style="color:red;">'+data.content[i]+'</p>');
                    }
                }
            }
        );
    });
});