    <div class="col-md-4 col-sm-6 col-xs-12">
         <!-- START widget-->
         <div class="widget">
            <div class="widget-simple">
               <div class="row">
                  <div class="text-center col-xs-6">
                     {{#if avatar}}
                     <a href="user/profile/{{id}}" target="_blank"><img src="/uploads/avatar/{{avatar}}" style="width: 100px; height: 100px; max-width: 100%" alt="Image" class="img-thumbnail img-thumbnail"></a> 
                     {{else}}
                     <a href="user/profile/{{id}}" target="_blank"><img src="/img/user/blank-avatar" style="width: 100px; height: 100px; max-width: 100%" alt="Image" class="img-thumbnail img-thumbnail"></a> 
                     {{/if}}
                     <br>
                     <h5><strong>{{ firstname }} {{ lastname }}</strong></h5>
                  </div>
                  <!-- START button group-->
                  <div class="col-xs-6">
                     <ul role="menu" style="list-style-type: none;">
                        <li><a href="user/profile/{{id}}" target="_blank"><h5><strong>View Profile</strong></h5></a></li>
                        <li><a href="#" data-toggle="modal" data-target="#message" data-id="{{id}}" onclick="contactMessage({{id}})"><h5><strong>Message</strong></h5></a>
                        </li>
                        {{#if teach}}
                        <li><a href="#" onclick="showBookingModal('{{id}}', '{{rate}}');"><h5><strong>Book</strong></h5></a></li>
                        {{/if}}
                        <li><a href="#" data-toggle="modal" data-target="#remove" class="remove-contact" data-id="{{id}}"><h5><strong>Remove</strong></h5></a></li>
                        <li><a href="#" data-toggle="modal" data-target="#report" class="report-user" data-id="{{id}}"><h5><strong>Report</strong></h5></a>
                        </li>
                        {{#if notTeach}}
                           <li>&nbsp;</li>
                        {{/if }}
                     </ul>
                  </div>
                  <!-- END button group-->
               </div>
            </div>
         </div>
         <!-- END widget-->
    </div>