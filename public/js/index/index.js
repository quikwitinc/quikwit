$(document).ready(function(){
	var rform = $("form#register-form");
	rform.validate({
		rules :{
			firstname : {
				required: true,
				minlength : 1
			},
			lastname : {
				required: true,
				minlength : 1
			},
			email : {
				required : true,
				email : true,
				remote : {
					url : "/register/check/email",
					type : "post",
					data: {	email : function(){ return rform.find("input[name='email']").val();}, 
							boolean : true,
					},
				}
			}
		},
		messages : {
			email : {
				required : "Email is required",
				email : "Email address is invalid",
				remote : "Email address already exists",
			}
		},
		success: function(label){
			console.log(label);
		}
	});
	/*$("#rform-firstname", rsel).validate({
		debug: true,
		rules : {
			required: true,
			minlength : 1
		},
		messages : {
			required : "First name is required.",
			minlength :  $.validator.format("At least {0} characters required.")
		}
	});
	$("#rform-lastname", rsel).validate({
		debug: true,
		rules : {
			required: true,
			minlength : 1
		},
		messages : {
			required : "First name is required.",
			minlength :  $.validator.format("At least {0} characters required.")
		}
	});
	
	$("#rform-email", rsel).validate({
		debug: true,
		rules : {
			required : true,
			email : true,
			remote : {
				url : "/register/check/email",
				type : "post",
				data: {email : $("#rform-email", rsel).val()},
				success : function(data){
					return data.success;
				}
			}
		},
		messages : {
			required : "Email is required.",
			email : "Email is invalid."
		}
	}); */
	
	//$("#")
});

/* (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-58890184-1', 'auto');
ga('send', 'pageview'); */