<div class="widget widget-hover-effect1" data-id="{{requestId}}">
    <div class="widget-simple themed-background">
        {{!-- <div class="col-sm-2 hidden-xs" style="padding: 0px;">
            {{#equal userId 0 }}
            {{else}}
            <a href="/user/profile/{{userId}}" target="_blank">
            {{/equal}}
            {{# if avatar }}
            <img src="/uploads/avatar/{{avatar}}" alt="avatar" class="widget-image img-circle pull-left">
            {{else}}
            <img src="/img/user/blank-avatar" style="width: 64px; max-width: 100%" alt="avatar" class="widget-image pull-left">
            {{/if}} 
            {{#equal userId 0 }}
            {{else}}
            </a>
            {{/equal}}
        </div> --}}
        <div class="col-sm-8 col-xs-12" style="padding: 0px; height: 60px;">
            {{!-- <h5 class="widget-content-light">
                <strong>{{#if name}}
                        {{ name }}
                        {{else}}
                        This poster wishes to be anonymous.
                        {{/if}}</strong>
            </h5> --}}
            <h4 class="widget-content widget-content-light">
                <strong>{{ title }}</strong>
            </h4>
        </div>
        <div class="col-sm-4 col-xs-12" style="padding: 0px;">
            {{#equal userId requestId }}
            <a href="/request/viewPosting/{{requestId}}" target="_blank" class="btn btn-m pull-right" style="border: solid 2px white; background: #1bbae1; color: white;"><h5 style="color: white;"><strong>Contact Details</strong></h5></a>
            {{else}}
            <a href="/browse/viewDetail/{{requestId}}" target="_blank" class="btn btn-m pull-right" style="border: solid 2px white; background: #1bbae1; color: white;"><h5 style="color: white;"><strong>Contact Details</strong></h5></a>
            {{/equal}}
        </div>
    </div>
    <div class="widget-extra">
        <div class="row text-center" style="color: #1b92ba;">
            <div class="col-xs-4">
                <h5>
                    <strong>{{ skill }}</strong><br>
                </h5>
            </div>
            <div class="col-xs-4">
                <h5>
                    <strong>
                        {{#equal price 0 }}
                        ${{ price }}
                        {{else}}
                        Budget not specified
                        {{/equal}}
                        {{#equal fixed 0 }}
                        &nbsp;(per hour)
                        {{else}}
                        &nbsp;(flate rate)
                        {{/equal}}
                    </strong><br>
                </h5>
            </div>
            <div class="col-xs-4">
                <h5>
                    <strong>{{ city }}</strong><br>
                </h5>
            </div>
        </div>
    </div>
</div>