function checkStatus() {
	$.post("status.php", { phone_number : $("#phone_number").val() },
	function(data) { updateStatus(data.status); }, "json");
}

function updateStatus(current) {
	if (current === "unverified") {
		$("#status").append(".");
		setTimeout(checkStatus, 3000);
	}
	else {
		success();
	}
}

function success() {
	$("#status").text("Verified!");
}