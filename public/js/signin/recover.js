$("#form-submit").click(function(e){
    e.preventDefault();
    var form = $("#password-recover-form");
    $.ajax({
        method : 'POST',
        data : form.serialize(),
        dataType : 'json',
        success : function(response) {
            /* console.log(response.message);
            console.log(form.find('.response-message')); */
            $('.response-message').html(response.message);
            /* if (response.success) {
                form.fadeOut();
            } 
            window.location.replace("../signin/emailsent"); */
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('Sorry, failed to send email instructions. Please try again later.');
            cb(null,true);
        }
    });
});