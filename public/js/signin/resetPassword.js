$("#form-submit").click(function(e){
    e.preventDefault();
    var form = $("#password-reset-form");
    $.ajax({
        method : 'POST',
        data : form.serialize(),
        dataType : 'json',
        success : function(response) {
            /* console.log(response.message); 
            $('.response-message').html(response.message); 
            if (response.success) {
                form.fadeOut();
            } */
            window.location.reload(); 
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('Sorry, failed to reset password. Please try again later.');
            cb(null,true);
        }
    });
});