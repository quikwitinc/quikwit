<li class="media">
    <a href="/user/profile/{{userId}}" class="pull-left" target="_blank">
        {{#if avatar}}
        <img src="/uploads/avatar/{{avatar}}" alt="Avatar" class="img-thumbnail" style="width: 50px;"> 
        {{else}}
        <img src="/img/user/blank-avatar" alt="Avatar" class="img-thumbnail" style="width: 50px;">
        {{/if}}
        <br>
        <strong>{{firstname}} {{lastname}}</strong>
    </a>
    <div class="media-body" style="padding-top: 10px;">
        <p class="push-bit">
            <span class="text-muted pull-right">
                <small>{{createDate}}</small>
            </span>
            <strong>{{title}}</strong>
        </p>
        <div class="btn-group btn-group-sm pull-right">
            <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default enable-tooltip" title="Options" data-toggle="modal" data-target="#reportReview" onclick="reportReview({{ id }});"><i class="fa fa-flag"></i></a>
        </div>
        <p>{{comment}}</p>
    </div>
</li>

