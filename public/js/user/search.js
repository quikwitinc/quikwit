       <div class="widget" data-id="{{userId}}">
          <div class="widget-simple">
             <div class="row">
                <div class="text-center col-xs-4" style="padding: 0px;">
                   {{#if avatar}}
                   <a href="/user/profile/{{userId}}" target="_blank">
                   <img src="/uploads/avatar/{{avatar}}" style="width: 100px; max-width: 100%" alt="Image" class="img-thumbnail img-thumbnail"></a>
                   {{else}}
                   <a href="/user/profile/{{userId}}" target="_blank">
                   <img src="/img/user/blank-avatar" style="width: 100px; max-width: 100%" alt="Image" class="img-thumbnail img-thumbnail"></a>
                   {{/if}}  
                   <br>
                   <h5><strong>{{name}}</strong></h5>
                </div>
                <div class="col-sm-4 col-xs-8">
                  <table style="border-collapse:separate; border-spacing: 1em; font-weight: 600; margin-top: -15px;">
                    <tr>
                      <td style="padding-left: 20px;">
                      {{contract_complete}}
                      </td>
                    </tr>
                    <tr>
                      <td style="padding-left: 20px;">
                      {{#equal usr_rate 0}}
                      Please contact me for rate.
                      {{else}}
                      {{usr_rate}}
                      {{/equal}}
                      </td>
                    </tr>
                    <tr>
                      <td style="padding-top: 10px; padding-left: 20px">
                        <input class="star {split:2}" type="radio" name="/user/profile/{{userId}}" value="0.5" disabled="disabled" {{check1}} data-id="{{userId}}"/>
                        <input class="star {split:2}" type="radio" name="/user/profile/{{userId}}" value="1.0" disabled="disabled" {{check2}} data-id="{{userId}}"/>
                        <input class="star {split:2}" type="radio" name="/user/profile/{{userId}}" value="1.5" disabled="disabled" {{check3}} data-id="{{userId}}"/>
                        <input class="star {split:2}" type="radio" name="/user/profile/{{userId}}" value="2.0" disabled="disabled" {{check4}} data-id="{{userId}}"/>
                        <input class="star {split:2}" type="radio" name="/user/profile/{{userId}}" value="2.5" disabled="disabled" {{check5}} data-id="{{userId}}"/> 

                        <input class="star {split:2}" type="radio" name="/user/profile/{{userId}}" value="3.0" disabled="disabled" {{check6}} data-id="{{userId}}"/>
                        <input class="star {split:2}" type="radio" name="/user/profile/{{userId}}" value="3.5" disabled="disabled" {{check7}} data-id="{{userId}}"/>
                        <input class="star {split:2}" type="radio" name="/user/profile/{{userId}}" value="4.0" disabled="disabled" {{check8}} data-id="{{userId}}"/>
                        <input class="star {split:2}" type="radio" name="/user/profile/{{userId}}" value="4.5" disabled="disabled" {{check9}} data-id="{{userId}}"/>
                        <input class="star {split:2}" type="radio" name="/user/profile/{{userId}}" value="5.0" disabled="disabled" {{check10}} data-id="{{userId}}"/> 
                        &nbsp({{num_rating}})
                      </td>
                    </tr>
                  </table>
                </div>
                <!-- START button group-->
                <div class="col-sm-4 hidden-xs">
                   <ul role="menu" style="list-style-type: none;  margin-top: -10px;">
                      <li><a href="/user/profile/{{userId}}" target="_blank"><h5 style="padding-bottom: 5px"><strong>View Profile</strong></h5></a></li>
                      <li><a href="#" data-toggle="modal" data-target="#message" data-id="{{userId}}" class="searchMessage"><h5 style="padding-bottom: 5px"><strong>Message</strong></h5></a>
                      </li>
                      <li><a href="#" onclick="showBookingModal('{{userId}}', '{{usr_rate}}');"><h5 style="padding-bottom: 5px"><strong>Book</strong></h5></a></li>
                      <li><a href="#" data-toggle="modal" data-target="#report" class="report-user" data-id="{{userId}}"><h5 skills><strong>Report</strong></h5></a>
                      </li>
                   </ul>
                </div>
                <!-- END button group-->
                <div class="col-xs-12 text-center">
                  <a href="/user/profile/{{userId}}" class="label label-info"> 
                    {{skills}}
                  </a>
                  &nbsp;
                  {{moreskills}}
                </div>
             </div>
          </div>
       </div>


