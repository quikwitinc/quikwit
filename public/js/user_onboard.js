// Define the tour!
var tour = {
  id: "welcome-to-quikwit",
  steps: [
    /* {
      title: "Welcome to QuikWit!",
      content: "Thank you for signing up! Here we landed you in your profile page to help you understand different aspects of your profile. Your profile will be shown just like this here to the public!",
      target: "quikwit-logo",
      placement: "right"
    },
    {
      title: "Finding the right teacher",
      content: "Here is where you can search for all the teachers available to you. Simply enter what you want to learn and look at our available teachers in that category.",
      target: "search",
      placement: "bottom"
    }, */
    {
      title: "Adding Skills",
      content: "To add the services you want to provide, add skills to your account to allow clients to find you.",
      target: "addSkillsDiv",
      placement: "left",
      yOffset: "-20"
    },
    {
      title: "Adding Languages",
      content: "Add the languages you speak fluently. This could help you find more clients.",
      target: "addLanguagesDiv",
      placement: "bottom",
      xOffset: "-15"
    },
    {
      title: "Adding Hourly Rate",
      content: "Choose the hourly rate you would like to charge your clients on projects.",
      target: "addRatesDiv",
      placement: "bottom",
      xOffset: "-15"

    },
    {
      title: "Overall Rating",
      content: "This is your cumulated overall rating from all of the reviews you have from clients.",
      target: "overallRating",
      yOffset: "-20",
      placement: "right"
    },
   
     {
      title: "Set profile picture",
      content: "This is where you can upload your picture. Using your own photo will increase your credibility!",
      target: "setAvatar",
      yOffset: "-10",
      placement: "right"
    },
    {
      title: "Editing Your Bio",
      content: "Let your clients get to know you. This is a good place to show your personality and describe your style of business.",
      target: "bioEdit",
      placement: "left",
      yOffset: "-20"
    },
    {
      title: "Add Your Location",
      content: "Add your city and postal code by clicking this button. For safety reasons we show users the radius within which your postal code falls rather than the code itself.",
      target: "myLocation",
      placement: "left",
      yOffset: "-20"
    },
    {
      title: "Freelancer Stats",
      content: "You will find your ratings and a few of your statistics, such as how many contracts you have completed, here.",
      target: "myStats",
      placement: "bottom"
    },
    {
      title: "Edit Your Experience",
      content: "This is a great place to tell your clients about your past projects or events and when you did them.",
      target: "myExperience",
      placement: "left",
      yOffset: "-20"
    },
    {
      title: "Your Intro Video",
      content: "Show your clients who you are by uploading a short lengthed video of yourself. This can add a personal touch to your account.",
      target: "myIntro",
      placement: "left",
      yOffset: "-20"
    },
    {
      title: "Adding Media",
      content: "Include pictures and videos of your past projects to show potential clients.",
      target: "myMedia",
      placement: "left",
      yOffset: "-20"

    },
     {
      title: "Reviews",
      content: "This is where all the reviews your clients submit will be displayed.",
      target: "myReviews",
      placement: "top"
    },
    {
      title: "Manage everything here",
      content: "Here is where you have the rest of your options such as Messages, Settings and Management tools. Thank you for signing up and we hope you have a great day!",
      target: "myAccount",
      placement: "left",
      arrowOffset: "-5"
    }
    /*
    {
      title: "Related account settings",
      content: "All of your other settings can be found here. Please click 'Settings' to continue",
      target: "settings",
      placement: "left",
      arrowOffset: "-5",
      nextOnTargetClick: true,
      showNextButton: false,
      /* onNext: function(tour) {
        tour.end();
        var checkExist = setInterval(function() {
          $element = $('#activateTeacher');

          if ($element.is(':visible')) {
            clearInterval(checkExist); 
            tour.start(true);
            tour.goTo(9);
          }
        }
      }, 
      multipage: true,
      //orphan: true
    },
    {
      title: "Activate/Deactivate teacher status",
      content: "You can activate or deactivate your teacher status here! </br></br> Note that by becoming a teacher you can still learn from other teachers on our site.",
      target: "activateTeacher",
      placement: "left",
      //orphan: true
    }  */
  ]
};

/* ========== */
/* TOUR SETUP */
/* ========== */
addClickListener = function(el, fn) {
  if (el.addEventListener) {
    el.addEventListener('click', fn, false);
  }
  else {
    el.attachEvent('onclick', fn);
  }
},

init = function() {
  var startBtnId = 'startTourBtn',
      calloutId = 'startTourCallout',
      mgr = hopscotch.getCalloutManager(),
      state = hopscotch.getState();

  if (state && state.indexOf('welcome-to-quikwit:') === 0) {
    // Already started the tour at some point!
    hopscotch.startTour(tour);
  }
  /* else {
    // Looking at the page for the first(?) time.
    setTimeout(function() {
      mgr.createCallout({
        id: calloutId,
        target: startBtnId,
        placement: 'right',
        title: 'Take an example tour',
        content: 'Start by taking an example tour to see what we offer!',
        yOffset: -25,
        arrowOffset: 20,
        width: 240
      });
    }, 100);
  } */

  addClickListener(document.getElementById(startBtnId), function() {
    if (!hopscotch.isActive) {
      mgr.removeAllCallouts();
      hopscotch.startTour(tour);
    }
  });

};

init();