<?php

$data = array(
  array(
     'label' => '5 Stars',
     'color' => '#b2d767',
     'data' => 120
  ),
  array(
     'label' => '4 Stars',
     'color' => '#4acab4',
     'data' => 60
  ),
  array(
     'label' => '3 Stars',
     'color' => '#ffea88',
     'data' => 20
  ),
  array(
     'label' => '2 Stars',
     'color' => '#ff8153',
     'data' => 30
  ),
  array(
     'label' => '1 Stars',
     'color' => '#878bb6',
     'data' => 15
  )
);

?>