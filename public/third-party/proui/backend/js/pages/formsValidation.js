/*
 *  Document   : formsValidation.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Forms Validation page
 */

jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z]+$/i.test(value);
}, "This input should contain only letters."); 

jQuery.validator.addMethod("notEqual", function(value, element, param) {
    return this.optional(element) || value != $(param).val();
}, "Please specify a different (non-default) value");

jQuery.validator.addMethod("lettersNumbers", function(value, element) {
    return this.optional(element) || /^[a-z0-9\s]+$/i.test(value);
}, "This input should contain only numbers and letters.");

var FormsValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    firstname: {
                        required: true,
                        lettersonly: true
                        //minlength: 3
                    },
                    lastname: {
                        required: true,
                        lettersonly: true
                        //minlength: 3
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    rpassword: {
                        required: true,
                        equalTo: '#rform-password'
                    },
                    confirmation: {
                        required: true
                    }
                },
                messages: {
                    firstname: {
                        required: 'Please enter your first name.',
                        lettersonly: 'Your first name should contain only letters.'
                        //minlength: 'Your username must consist of at least 3 characters.'
                    },
                    lastname: {
                        required: 'Please enter your last name.',
                        lettersonly: 'Your last name should contain only letters.'
                        //minlength: 'Your username must consist of at least 3 characters.'
                    },
                    email: 'Please enter a valid email address.',
                    password: {
                        required: 'Please provide a password.',
                        minlength: 'Your password must be at least 5 characters long.'
                    },
                    rpassword: {
                        required: 'Please provide a password.',
                        equalTo: 'Please enter the same password as above.'
                    },
                    confirmation: {
                        required: 'Please agree to our Terms of Service and Privacy Policy.'
                    }
                }
            });

            $('#form-validation-2').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    firstname: {
                        required: true,
                        lettersonly: true
                        //minlength: 3
                    },
                    lastname: {
                        required: true,
                        lettersonly: true
                        //minlength: 3
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    rpassword: {
                        required: true,
                        equalTo: '#rform-password'
                    },
                    confirmation: {
                        required: true
                    }
                },
                messages: {
                    firstname: {
                        required: 'Please enter your first name.',
                        lettersonly: 'Your first name should contain only letters.'
                        //minlength: 'Your username must consist of at least 3 characters.'
                    },
                    lastname: {
                        required: 'Please enter your last name.',
                        lettersonly: 'Your last name should contain only letters.'
                        //minlength: 'Your username must consist of at least 3 characters.'
                    },
                    email: 'Please enter a valid email address.',
                    password: {
                        required: 'Please provide a password.',
                        minlength: 'Your password must be at least 5 characters long.'
                    },
                    rpassword: {
                        required: 'Please provide a password.',
                        equalTo: 'Please enter the same password as above.'
                    },
                    confirmation: {
                        required: 'Please agree to our Terms of Service and Privacy Policy.'
                    }
                }
            });

            /* $('#search-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    q: {
                        required: true
                    }
                },
                messages: {
                    q: {
                        required: 'Please provide a search term.'
                    }
                }
            }); */

            $('#contract-form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    price: {
                        required: true
                    }
                },
                messages: {
                    price: {
                        required: 'Please provide an amount.'
                    }
                }
            }); 

            $('#signin-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    username: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                },
                messages: {
                    username: 'Please enter a valid email address.',
                    password: {
                        required: 'Please provide a password.'
                    }
                }
            });

            $('#request-form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    title: {
                        required: true
                    },
                    task: {
                        required: true
                    },
                    price: {
                        required: true,
                        number: true
                    },
                    hour: {
                        required: true,
                        number: true
                    },
                    postal: {
                        required: true,
                        lettersNumbers: true
                    },
                    email: {
                        email: true
                    },
                    confirmation: {
                        required: true
                    }
                },
                messages: {
                    title: {
                        required: 'Please provide a title.'
                    },
                    task: {
                        required: 'Please provide a description for this job.'
                    },
                    price: {
                        required: 'Please provide an estimated budget.',
                        number: 'Please enter a numeric value.'
                    },
                    hour: {
                        required: 'Please provide an estimated number of hours.',
                        number: 'Please enter a numeric value.'
                    },
                    postal: {
                        required: 'Please provide the postal code for the job',
                        lettersNumbers: 'Please enter a valid postal code'
                    },
                    email: {
                        email: 'Please enter a valid email address.'
                    },
                    confirmation: {
                        required: 'Please agree to our Terms of Service and Privacy Policy.'
                    }
                }
            }); 

            /* $('#qw-usettings-form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    firstname: {
                        required: true,
                        lettersonly: true
                        //minlength: 3
                    },
                    lastname: {
                        required: true,
                        lettersonly: true
                        //minlength: 3
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    postalcode: {
                        required: true,
                        lettersNumbers: true,
                        minlength: 6,
                        maxlength: 7
                    }                   
                },
                messages: {
                    firstname: {
                        required: 'Please enter your first name.',
                        lettersonly: 'Your first name should contain only letters.'
                        //minlength: 'Your username must consist of at least 3 characters.'
                    },
                    lastname: {
                        required: 'Please enter your last name.',
                        lettersonly: 'Your last name should contain only letters.'
                        //minlength: 'Your username must consist of at least 3 characters.'
                    },
                    email: 'Please enter a valid email address.',
                    postalcode: 'Please enter a valid postal code.'
                }
            }); */

            $('#change-password-form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    password: {
                        required: true,
                        minlength: 5,
                        notEqual: '#current_password'
                    },
                    rpassword: {
                        required: true,
                        equalTo: '#new_password'
                    }
                },
                messages: {
                    password: {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 5 characters long',
                        notEqual: 'Your new password should be different from your current password.'
                    },
                    rpassword: {
                        required: 'Please provide a password',
                        equalTo: 'Please enter the same password as above'
                    }
                }
            });

            $('#password-reset-form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    password: {
                        required: true,
                        minlength: 5
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: '#form-password'
                    }
                },
                messages: {
                    password: {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 5 characters long',
                        notEqual: 'Your new password should be different from your current password.'
                    },
                    password_confirmation: {
                        required: 'Please provide a password',
                        equalTo: 'Please enter the same password as above'
                    }
                }
            });

            /* $('#validation-form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    val_username: {
                        required: true,
                        minlength: 3
                    },
                    val_email: {
                        required: true,
                        email: true
                    },
                    val_password: {
                        required: true,
                        minlength: 5
                    },
                    val_confirm_password: {
                        required: true,
                        equalTo: '#rform-password'
                    },
                    val_bio: {
                        required: true,
                        minlength: 5
                    },
                    val_skill: {
                        required: true
                    },
                    val_website: {
                        required: true,
                        url: true
                    },
                    val_credit_card: {
                        required: true,
                        creditcard: true
                    },
                    val_digits: {
                        required: true,
                        digits: true
                    },
                    val_number: {
                        required: true,
                        number: true
                    },
                    val_range: {
                        required: true,
                        range: [1, 1000]
                    },
                    val_terms: {
                        required: true
                    }
                },
                messages: {
                    val_username: {
                        required: 'Please enter a username',
                        minlength: 'Your username must consist of at least 3 characters'
                    },
                    val_email: 'Please enter a valid email address',
                    val_password: {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 5 characters long'
                    },
                    val_confirm_password: {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 5 characters long',
                        equalTo: 'Please enter the same password as above'
                    },
                    val_bio: 'Don\'t be shy, share something with us :-)',
                    val_skill: 'Please select a skill!',
                    val_website: 'Please enter your website!',
                    val_credit_card: 'Please enter a valid credit card! Try 446-667-651!',
                    val_digits: 'Please enter only digits!',
                    val_number: 'Please enter a number!',
                    val_range: 'Please enter a number between 1 and 1000!',
                    val_terms: 'You must agree to the service terms!'
                }
            }); */

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();